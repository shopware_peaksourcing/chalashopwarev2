<?php

/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */


namespace Shopware\Models\Attribute;
use Shopware\Components\Model\ModelEntity,
    Doctrine\ORM\Mapping AS ORM,
    Symfony\Component\Validator\Constraints as Assert,
    Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity
 * @ORM\Table(name="s_categories_attributes")
 */
class Category extends ModelEntity
{
    

    /**
     * @var integer $id
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="id", type="integer", nullable=false)
     */
     protected $id;


    /**
     * @var integer $categoryId
     *
     * @ORM\Column(name="categoryID", type="integer", nullable=true)
     */
     protected $categoryId;


    /**
     * @var string $attribute1
     *
     * @ORM\Column(name="attribute1", type="text", nullable=true)
     */
     protected $attribute1;


    /**
     * @var string $attribute2
     *
     * @ORM\Column(name="attribute2", type="string", nullable=true)
     */
     protected $attribute2;


    /**
     * @var string $attribute3
     *
     * @ORM\Column(name="attribute3", type="string", nullable=true)
     */
     protected $attribute3;


    /**
     * @var string $attribute4
     *
     * @ORM\Column(name="attribute4", type="string", nullable=true)
     */
     protected $attribute4;


    /**
     * @var string $attribute5
     *
     * @ORM\Column(name="attribute5", type="string", nullable=true)
     */
     protected $attribute5;


    /**
     * @var string $attribute6
     *
     * @ORM\Column(name="attribute6", type="string", nullable=true)
     */
     protected $attribute6;


    /**
     * @var string $productConfigurator
     *
     * @ORM\Column(name="product_configurator", type="string", nullable=true)
     */
     protected $productConfigurator;


    /**
     * @var integer $productCompare
     *
     * @ORM\Column(name="product_compare", type="integer", nullable=true)
     */
     protected $productCompare;


    /**
     * @var string $menuStyle
     *
     * @ORM\Column(name="menu_style", type="text", nullable=true)
     */
     protected $menuStyle;


    /**
     * @var string $productsInMenu
     *
     * @ORM\Column(name="products_in_menu", type="text", nullable=true)
     */
     protected $productsInMenu;


    /**
     * @var integer $service
     *
     * @ORM\Column(name="service", type="integer", nullable=true)
     */
     protected $service;


    /**
     * @var string $additionalDescription
     *
     * @ORM\Column(name="additional_description", type="text", nullable=true)
     */
     protected $additionalDescription;


    /**
     * @var datetime $customDate
     *
     * @ORM\Column(name="custom_date", type="datetime", nullable=true)
     */
     protected $customDate;


    /**
     * @var string $pageTemplate
     *
     * @ORM\Column(name="page_template", type="text", nullable=true)
     */
     protected $pageTemplate;


    /**
     * @var string $additionalPages
     *
     * @ORM\Column(name="additional_pages", type="text", nullable=true)
     */
     protected $additionalPages;


    /**
     * @var string $doubleImages
     *
     * @ORM\Column(name="double_images", type="text", nullable=true)
     */
     protected $doubleImages;


    /**
     * @var string $doubleImagesTitle
     *
     * @ORM\Column(name="double_images_title", type="string", nullable=true)
     */
     protected $doubleImagesTitle;


    /**
     * @var string $doubleImagesText
     *
     * @ORM\Column(name="double_images_text", type="text", nullable=true)
     */
     protected $doubleImagesText;


    /**
     * @var string $contactText
     *
     * @ORM\Column(name="contact_text", type="text", nullable=true)
     */
     protected $contactText;


    /**
     * @var string $contactImage
     *
     * @ORM\Column(name="contact_image", type="string", nullable=true)
     */
     protected $contactImage;


    /**
     * @var string $facebookLink
     *
     * @ORM\Column(name="facebook_link", type="string", nullable=true)
     */
     protected $facebookLink;


    /**
     * @var string $instagramLink
     *
     * @ORM\Column(name="instagram_link", type="string", nullable=true)
     */
     protected $instagramLink;


    /**
     * @var string $callendarLink
     *
     * @ORM\Column(name="callendar_link", type="string", nullable=true)
     */
     protected $callendarLink;


    /**
     * @var string $images
     *
     * @ORM\Column(name="images", type="text", nullable=true)
     */
     protected $images;


    /**
     * @var string $completeAlbum
     *
     * @ORM\Column(name="complete_album", type="string", nullable=true)
     */
     protected $completeAlbum;


    /**
     * @var string $menuProductStyle
     *
     * @ORM\Column(name="menu_product_style", type="text", nullable=true)
     */
     protected $menuProductStyle;


    /**
     * @var string $menuLabel
     *
     * @ORM\Column(name="menu_label", type="string", nullable=true)
     */
     protected $menuLabel;


    /**
     * @var string $menuBottomLabel
     *
     * @ORM\Column(name="menu_bottom_label", type="string", nullable=true)
     */
     protected $menuBottomLabel;


    /**
     * @var string $topSliderImages
     *
     * @ORM\Column(name="top_slider_images", type="text", nullable=true)
     */
     protected $topSliderImages;


    /**
     * @var string $parentCategoryFilter
     *
     * @ORM\Column(name="parent_category_filter", type="string", nullable=true)
     */
     protected $parentCategoryFilter;


    /**
     * @var integer $hideInMobileMenu
     *
     * @ORM\Column(name="hide_in_mobile_menu", type="integer", nullable=true)
     */
     protected $hideInMobileMenu;


    /**
     * @var string $mobileDescription
     *
     * @ORM\Column(name="mobile_description", type="string", nullable=true)
     */
     protected $mobileDescription;


    /**
     * @var string $relatedBlog
     *
     * @ORM\Column(name="related_blog", type="text", nullable=true)
     */
     protected $relatedBlog;


    /**
     * @var string $configuratorText
     *
     * @ORM\Column(name="configurator_text", type="text", nullable=true)
     */
     protected $configuratorText;


    /**
     * @var string $configuratorImage
     *
     * @ORM\Column(name="configurator_image", type="string", nullable=true)
     */
     protected $configuratorImage;


    /**
     * @var integer $hideImageForMobileMenu
     *
     * @ORM\Column(name="hide_image_for_mobile_menu", type="integer", nullable=true)
     */
     protected $hideImageForMobileMenu;


    /**
     * @var \Shopware\Models\Category\Category
     *
     * @ORM\OneToOne(targetEntity="Shopware\Models\Category\Category", inversedBy="attribute")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categoryID", referencedColumnName="id")
     * })
     */
    protected $category;
    


    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    

    public function getCategoryId()
    {
        return $this->categoryId;
    }

    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;
        return $this;
    }
    

    public function getAttribute1()
    {
        return $this->attribute1;
    }

    public function setAttribute1($attribute1)
    {
        $this->attribute1 = $attribute1;
        return $this;
    }
    

    public function getAttribute2()
    {
        return $this->attribute2;
    }

    public function setAttribute2($attribute2)
    {
        $this->attribute2 = $attribute2;
        return $this;
    }
    

    public function getAttribute3()
    {
        return $this->attribute3;
    }

    public function setAttribute3($attribute3)
    {
        $this->attribute3 = $attribute3;
        return $this;
    }
    

    public function getAttribute4()
    {
        return $this->attribute4;
    }

    public function setAttribute4($attribute4)
    {
        $this->attribute4 = $attribute4;
        return $this;
    }
    

    public function getAttribute5()
    {
        return $this->attribute5;
    }

    public function setAttribute5($attribute5)
    {
        $this->attribute5 = $attribute5;
        return $this;
    }
    

    public function getAttribute6()
    {
        return $this->attribute6;
    }

    public function setAttribute6($attribute6)
    {
        $this->attribute6 = $attribute6;
        return $this;
    }
    

    public function getProductConfigurator()
    {
        return $this->productConfigurator;
    }

    public function setProductConfigurator($productConfigurator)
    {
        $this->productConfigurator = $productConfigurator;
        return $this;
    }
    

    public function getProductCompare()
    {
        return $this->productCompare;
    }

    public function setProductCompare($productCompare)
    {
        $this->productCompare = $productCompare;
        return $this;
    }
    

    public function getMenuStyle()
    {
        return $this->menuStyle;
    }

    public function setMenuStyle($menuStyle)
    {
        $this->menuStyle = $menuStyle;
        return $this;
    }
    

    public function getProductsInMenu()
    {
        return $this->productsInMenu;
    }

    public function setProductsInMenu($productsInMenu)
    {
        $this->productsInMenu = $productsInMenu;
        return $this;
    }
    

    public function getService()
    {
        return $this->service;
    }

    public function setService($service)
    {
        $this->service = $service;
        return $this;
    }
    

    public function getAdditionalDescription()
    {
        return $this->additionalDescription;
    }

    public function setAdditionalDescription($additionalDescription)
    {
        $this->additionalDescription = $additionalDescription;
        return $this;
    }
    

    public function getCustomDate()
    {
        return $this->customDate;
    }

    public function setCustomDate($customDate)
    {
        $this->customDate = $customDate;
        return $this;
    }
    

    public function getPageTemplate()
    {
        return $this->pageTemplate;
    }

    public function setPageTemplate($pageTemplate)
    {
        $this->pageTemplate = $pageTemplate;
        return $this;
    }
    

    public function getAdditionalPages()
    {
        return $this->additionalPages;
    }

    public function setAdditionalPages($additionalPages)
    {
        $this->additionalPages = $additionalPages;
        return $this;
    }
    

    public function getDoubleImages()
    {
        return $this->doubleImages;
    }

    public function setDoubleImages($doubleImages)
    {
        $this->doubleImages = $doubleImages;
        return $this;
    }
    

    public function getDoubleImagesTitle()
    {
        return $this->doubleImagesTitle;
    }

    public function setDoubleImagesTitle($doubleImagesTitle)
    {
        $this->doubleImagesTitle = $doubleImagesTitle;
        return $this;
    }
    

    public function getDoubleImagesText()
    {
        return $this->doubleImagesText;
    }

    public function setDoubleImagesText($doubleImagesText)
    {
        $this->doubleImagesText = $doubleImagesText;
        return $this;
    }
    

    public function getContactText()
    {
        return $this->contactText;
    }

    public function setContactText($contactText)
    {
        $this->contactText = $contactText;
        return $this;
    }
    

    public function getContactImage()
    {
        return $this->contactImage;
    }

    public function setContactImage($contactImage)
    {
        $this->contactImage = $contactImage;
        return $this;
    }
    

    public function getFacebookLink()
    {
        return $this->facebookLink;
    }

    public function setFacebookLink($facebookLink)
    {
        $this->facebookLink = $facebookLink;
        return $this;
    }
    

    public function getInstagramLink()
    {
        return $this->instagramLink;
    }

    public function setInstagramLink($instagramLink)
    {
        $this->instagramLink = $instagramLink;
        return $this;
    }
    

    public function getCallendarLink()
    {
        return $this->callendarLink;
    }

    public function setCallendarLink($callendarLink)
    {
        $this->callendarLink = $callendarLink;
        return $this;
    }
    

    public function getImages()
    {
        return $this->images;
    }

    public function setImages($images)
    {
        $this->images = $images;
        return $this;
    }
    

    public function getCompleteAlbum()
    {
        return $this->completeAlbum;
    }

    public function setCompleteAlbum($completeAlbum)
    {
        $this->completeAlbum = $completeAlbum;
        return $this;
    }
    

    public function getMenuProductStyle()
    {
        return $this->menuProductStyle;
    }

    public function setMenuProductStyle($menuProductStyle)
    {
        $this->menuProductStyle = $menuProductStyle;
        return $this;
    }
    

    public function getMenuLabel()
    {
        return $this->menuLabel;
    }

    public function setMenuLabel($menuLabel)
    {
        $this->menuLabel = $menuLabel;
        return $this;
    }
    

    public function getMenuBottomLabel()
    {
        return $this->menuBottomLabel;
    }

    public function setMenuBottomLabel($menuBottomLabel)
    {
        $this->menuBottomLabel = $menuBottomLabel;
        return $this;
    }
    

    public function getTopSliderImages()
    {
        return $this->topSliderImages;
    }

    public function setTopSliderImages($topSliderImages)
    {
        $this->topSliderImages = $topSliderImages;
        return $this;
    }
    

    public function getParentCategoryFilter()
    {
        return $this->parentCategoryFilter;
    }

    public function setParentCategoryFilter($parentCategoryFilter)
    {
        $this->parentCategoryFilter = $parentCategoryFilter;
        return $this;
    }
    

    public function getHideInMobileMenu()
    {
        return $this->hideInMobileMenu;
    }

    public function setHideInMobileMenu($hideInMobileMenu)
    {
        $this->hideInMobileMenu = $hideInMobileMenu;
        return $this;
    }
    

    public function getMobileDescription()
    {
        return $this->mobileDescription;
    }

    public function setMobileDescription($mobileDescription)
    {
        $this->mobileDescription = $mobileDescription;
        return $this;
    }
    

    public function getRelatedBlog()
    {
        return $this->relatedBlog;
    }

    public function setRelatedBlog($relatedBlog)
    {
        $this->relatedBlog = $relatedBlog;
        return $this;
    }
    

    public function getConfiguratorText()
    {
        return $this->configuratorText;
    }

    public function setConfiguratorText($configuratorText)
    {
        $this->configuratorText = $configuratorText;
        return $this;
    }
    

    public function getConfiguratorImage()
    {
        return $this->configuratorImage;
    }

    public function setConfiguratorImage($configuratorImage)
    {
        $this->configuratorImage = $configuratorImage;
        return $this;
    }
    

    public function getHideImageForMobileMenu()
    {
        return $this->hideImageForMobileMenu;
    }

    public function setHideImageForMobileMenu($hideImageForMobileMenu)
    {
        $this->hideImageForMobileMenu = $hideImageForMobileMenu;
        return $this;
    }
    

    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }
    
}