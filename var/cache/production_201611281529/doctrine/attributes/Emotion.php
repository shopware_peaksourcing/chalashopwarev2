<?php

/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */


namespace Shopware\Models\Attribute;
use Shopware\Components\Model\ModelEntity,
    Doctrine\ORM\Mapping AS ORM,
    Symfony\Component\Validator\Constraints as Assert,
    Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity
 * @ORM\Table(name="s_emotion_attributes")
 */
class Emotion extends ModelEntity
{
    

    /**
     * @var integer $id
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="id", type="integer", nullable=false)
     */
     protected $id;


    /**
     * @var integer $emotionId
     *
     * @ORM\Column(name="emotionID", type="integer", nullable=true)
     */
     protected $emotionId;


    /**
     * @var string $imagePosition
     *
     * @ORM\Column(name="image_position", type="text", nullable=true)
     */
     protected $imagePosition;


    /**
     * @var string $videoText
     *
     * @ORM\Column(name="video_text", type="text", nullable=true)
     */
     protected $videoText;


    /**
     * @var integer $videoImage
     *
     * @ORM\Column(name="video_image", type="integer", nullable=true)
     */
     protected $videoImage;


    /**
     * @var string $videoTextTitle
     *
     * @ORM\Column(name="video_text_title", type="string", nullable=true)
     */
     protected $videoTextTitle;


    /**
     * @var string $productRelation
     *
     * @ORM\Column(name="product_relation", type="text", nullable=true)
     */
     protected $productRelation;


    /**
     * @var integer $swagShopTemplate
     *
     * @ORM\Column(name="swag_shop_template", type="integer", nullable=true)
     */
     protected $swagShopTemplate;


    /**
     * @var integer $chalaStories
     *
     * @ORM\Column(name="chala_stories", type="integer", nullable=true)
     */
     protected $chalaStories;


    /**
     * @var \Shopware\Models\Emotion\Emotion
     *
     * @ORM\OneToOne(targetEntity="Shopware\Models\Emotion\Emotion", inversedBy="attribute")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="emotionID", referencedColumnName="id")
     * })
     */
    protected $emotion;
    


    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    

    public function getEmotionId()
    {
        return $this->emotionId;
    }

    public function setEmotionId($emotionId)
    {
        $this->emotionId = $emotionId;
        return $this;
    }
    

    public function getImagePosition()
    {
        return $this->imagePosition;
    }

    public function setImagePosition($imagePosition)
    {
        $this->imagePosition = $imagePosition;
        return $this;
    }
    

    public function getVideoText()
    {
        return $this->videoText;
    }

    public function setVideoText($videoText)
    {
        $this->videoText = $videoText;
        return $this;
    }
    

    public function getVideoImage()
    {
        return $this->videoImage;
    }

    public function setVideoImage($videoImage)
    {
        $this->videoImage = $videoImage;
        return $this;
    }
    

    public function getVideoTextTitle()
    {
        return $this->videoTextTitle;
    }

    public function setVideoTextTitle($videoTextTitle)
    {
        $this->videoTextTitle = $videoTextTitle;
        return $this;
    }
    

    public function getProductRelation()
    {
        return $this->productRelation;
    }

    public function setProductRelation($productRelation)
    {
        $this->productRelation = $productRelation;
        return $this;
    }
    

    public function getSwagShopTemplate()
    {
        return $this->swagShopTemplate;
    }

    public function setSwagShopTemplate($swagShopTemplate)
    {
        $this->swagShopTemplate = $swagShopTemplate;
        return $this;
    }
    

    public function getChalaStories()
    {
        return $this->chalaStories;
    }

    public function setChalaStories($chalaStories)
    {
        $this->chalaStories = $chalaStories;
        return $this;
    }
    

    public function getEmotion()
    {
        return $this->emotion;
    }

    public function setEmotion($emotion)
    {
        $this->emotion = $emotion;
        return $this;
    }
    
}