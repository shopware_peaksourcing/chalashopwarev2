/*! jQuery v2.1.4 | (c) 2005, 2015 jQuery Foundation, Inc. | jquery.org/license */
!function(a,b){"object"==typeof module&&"object"==typeof module.exports?module.exports=a.document?b(a,!0):function(a){if(!a.document)throw new Error("jQuery requires a window with a document");return b(a)}:b(a)}("undefined"!=typeof window?window:this,function(a,b){var c=[],d=c.slice,e=c.concat,f=c.push,g=c.indexOf,h={},i=h.toString,j=h.hasOwnProperty,k={},l=a.document,m="2.1.4",n=function(a,b){return new n.fn.init(a,b)},o=/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,p=/^-ms-/,q=/-([\da-z])/gi,r=function(a,b){return b.toUpperCase()};n.fn=n.prototype={jquery:m,constructor:n,selector:"",length:0,toArray:function(){return d.call(this)},get:function(a){return null!=a?0>a?this[a+this.length]:this[a]:d.call(this)},pushStack:function(a){var b=n.merge(this.constructor(),a);return b.prevObject=this,b.context=this.context,b},each:function(a,b){return n.each(this,a,b)},map:function(a){return this.pushStack(n.map(this,function(b,c){return a.call(b,c,b)}))},slice:function(){return this.pushStack(d.apply(this,arguments))},first:function(){return this.eq(0)},last:function(){return this.eq(-1)},eq:function(a){var b=this.length,c=+a+(0>a?b:0);return this.pushStack(c>=0&&b>c?[this[c]]:[])},end:function(){return this.prevObject||this.constructor(null)},push:f,sort:c.sort,splice:c.splice},n.extend=n.fn.extend=function(){var a,b,c,d,e,f,g=arguments[0]||{},h=1,i=arguments.length,j=!1;for("boolean"==typeof g&&(j=g,g=arguments[h]||{},h++),"object"==typeof g||n.isFunction(g)||(g={}),h===i&&(g=this,h--);i>h;h++)if(null!=(a=arguments[h]))for(b in a)c=g[b],d=a[b],g!==d&&(j&&d&&(n.isPlainObject(d)||(e=n.isArray(d)))?(e?(e=!1,f=c&&n.isArray(c)?c:[]):f=c&&n.isPlainObject(c)?c:{},g[b]=n.extend(j,f,d)):void 0!==d&&(g[b]=d));return g},n.extend({expando:"jQuery"+(m+Math.random()).replace(/\D/g,""),isReady:!0,error:function(a){throw new Error(a)},noop:function(){},isFunction:function(a){return"function"===n.type(a)},isArray:Array.isArray,isWindow:function(a){return null!=a&&a===a.window},isNumeric:function(a){return!n.isArray(a)&&a-parseFloat(a)+1>=0},isPlainObject:function(a){return"object"!==n.type(a)||a.nodeType||n.isWindow(a)?!1:a.constructor&&!j.call(a.constructor.prototype,"isPrototypeOf")?!1:!0},isEmptyObject:function(a){var b;for(b in a)return!1;return!0},type:function(a){return null==a?a+"":"object"==typeof a||"function"==typeof a?h[i.call(a)]||"object":typeof a},globalEval:function(a){var b,c=eval;a=n.trim(a),a&&(1===a.indexOf("use strict")?(b=l.createElement("script"),b.text=a,l.head.appendChild(b).parentNode.removeChild(b)):c(a))},camelCase:function(a){return a.replace(p,"ms-").replace(q,r)},nodeName:function(a,b){return a.nodeName&&a.nodeName.toLowerCase()===b.toLowerCase()},each:function(a,b,c){var d,e=0,f=a.length,g=s(a);if(c){if(g){for(;f>e;e++)if(d=b.apply(a[e],c),d===!1)break}else for(e in a)if(d=b.apply(a[e],c),d===!1)break}else if(g){for(;f>e;e++)if(d=b.call(a[e],e,a[e]),d===!1)break}else for(e in a)if(d=b.call(a[e],e,a[e]),d===!1)break;return a},trim:function(a){return null==a?"":(a+"").replace(o,"")},makeArray:function(a,b){var c=b||[];return null!=a&&(s(Object(a))?n.merge(c,"string"==typeof a?[a]:a):f.call(c,a)),c},inArray:function(a,b,c){return null==b?-1:g.call(b,a,c)},merge:function(a,b){for(var c=+b.length,d=0,e=a.length;c>d;d++)a[e++]=b[d];return a.length=e,a},grep:function(a,b,c){for(var d,e=[],f=0,g=a.length,h=!c;g>f;f++)d=!b(a[f],f),d!==h&&e.push(a[f]);return e},map:function(a,b,c){var d,f=0,g=a.length,h=s(a),i=[];if(h)for(;g>f;f++)d=b(a[f],f,c),null!=d&&i.push(d);else for(f in a)d=b(a[f],f,c),null!=d&&i.push(d);return e.apply([],i)},guid:1,proxy:function(a,b){var c,e,f;return"string"==typeof b&&(c=a[b],b=a,a=c),n.isFunction(a)?(e=d.call(arguments,2),f=function(){return a.apply(b||this,e.concat(d.call(arguments)))},f.guid=a.guid=a.guid||n.guid++,f):void 0},now:Date.now,support:k}),n.each("Boolean Number String Function Array Date RegExp Object Error".split(" "),function(a,b){h["[object "+b+"]"]=b.toLowerCase()});function s(a){var b="length"in a&&a.length,c=n.type(a);return"function"===c||n.isWindow(a)?!1:1===a.nodeType&&b?!0:"array"===c||0===b||"number"==typeof b&&b>0&&b-1 in a}var t=function(a){var b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u="sizzle"+1*new Date,v=a.document,w=0,x=0,y=ha(),z=ha(),A=ha(),B=function(a,b){return a===b&&(l=!0),0},C=1<<31,D={}.hasOwnProperty,E=[],F=E.pop,G=E.push,H=E.push,I=E.slice,J=function(a,b){for(var c=0,d=a.length;d>c;c++)if(a[c]===b)return c;return-1},K="checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",L="[\\x20\\t\\r\\n\\f]",M="(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",N=M.replace("w","w#"),O="\\["+L+"*("+M+")(?:"+L+"*([*^$|!~]?=)"+L+"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|("+N+"))|)"+L+"*\\]",P=":("+M+")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|"+O+")*)|.*)\\)|)",Q=new RegExp(L+"+","g"),R=new RegExp("^"+L+"+|((?:^|[^\\\\])(?:\\\\.)*)"+L+"+$","g"),S=new RegExp("^"+L+"*,"+L+"*"),T=new RegExp("^"+L+"*([>+~]|"+L+")"+L+"*"),U=new RegExp("="+L+"*([^\\]'\"]*?)"+L+"*\\]","g"),V=new RegExp(P),W=new RegExp("^"+N+"$"),X={ID:new RegExp("^#("+M+")"),CLASS:new RegExp("^\\.("+M+")"),TAG:new RegExp("^("+M.replace("w","w*")+")"),ATTR:new RegExp("^"+O),PSEUDO:new RegExp("^"+P),CHILD:new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\("+L+"*(even|odd|(([+-]|)(\\d*)n|)"+L+"*(?:([+-]|)"+L+"*(\\d+)|))"+L+"*\\)|)","i"),bool:new RegExp("^(?:"+K+")$","i"),needsContext:new RegExp("^"+L+"*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\("+L+"*((?:-\\d)?\\d*)"+L+"*\\)|)(?=[^-]|$)","i")},Y=/^(?:input|select|textarea|button)$/i,Z=/^h\d$/i,$=/^[^{]+\{\s*\[native \w/,_=/^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,aa=/[+~]/,ba=/'|\\/g,ca=new RegExp("\\\\([\\da-f]{1,6}"+L+"?|("+L+")|.)","ig"),da=function(a,b,c){var d="0x"+b-65536;return d!==d||c?b:0>d?String.fromCharCode(d+65536):String.fromCharCode(d>>10|55296,1023&d|56320)},ea=function(){m()};try{H.apply(E=I.call(v.childNodes),v.childNodes),E[v.childNodes.length].nodeType}catch(fa){H={apply:E.length?function(a,b){G.apply(a,I.call(b))}:function(a,b){var c=a.length,d=0;while(a[c++]=b[d++]);a.length=c-1}}}function ga(a,b,d,e){var f,h,j,k,l,o,r,s,w,x;if((b?b.ownerDocument||b:v)!==n&&m(b),b=b||n,d=d||[],k=b.nodeType,"string"!=typeof a||!a||1!==k&&9!==k&&11!==k)return d;if(!e&&p){if(11!==k&&(f=_.exec(a)))if(j=f[1]){if(9===k){if(h=b.getElementById(j),!h||!h.parentNode)return d;if(h.id===j)return d.push(h),d}else if(b.ownerDocument&&(h=b.ownerDocument.getElementById(j))&&t(b,h)&&h.id===j)return d.push(h),d}else{if(f[2])return H.apply(d,b.getElementsByTagName(a)),d;if((j=f[3])&&c.getElementsByClassName)return H.apply(d,b.getElementsByClassName(j)),d}if(c.qsa&&(!q||!q.test(a))){if(s=r=u,w=b,x=1!==k&&a,1===k&&"object"!==b.nodeName.toLowerCase()){o=g(a),(r=b.getAttribute("id"))?s=r.replace(ba,"\\$&"):b.setAttribute("id",s),s="[id='"+s+"'] ",l=o.length;while(l--)o[l]=s+ra(o[l]);w=aa.test(a)&&pa(b.parentNode)||b,x=o.join(",")}if(x)try{return H.apply(d,w.querySelectorAll(x)),d}catch(y){}finally{r||b.removeAttribute("id")}}}return i(a.replace(R,"$1"),b,d,e)}function ha(){var a=[];function b(c,e){return a.push(c+" ")>d.cacheLength&&delete b[a.shift()],b[c+" "]=e}return b}function ia(a){return a[u]=!0,a}function ja(a){var b=n.createElement("div");try{return!!a(b)}catch(c){return!1}finally{b.parentNode&&b.parentNode.removeChild(b),b=null}}function ka(a,b){var c=a.split("|"),e=a.length;while(e--)d.attrHandle[c[e]]=b}function la(a,b){var c=b&&a,d=c&&1===a.nodeType&&1===b.nodeType&&(~b.sourceIndex||C)-(~a.sourceIndex||C);if(d)return d;if(c)while(c=c.nextSibling)if(c===b)return-1;return a?1:-1}function ma(a){return function(b){var c=b.nodeName.toLowerCase();return"input"===c&&b.type===a}}function na(a){return function(b){var c=b.nodeName.toLowerCase();return("input"===c||"button"===c)&&b.type===a}}function oa(a){return ia(function(b){return b=+b,ia(function(c,d){var e,f=a([],c.length,b),g=f.length;while(g--)c[e=f[g]]&&(c[e]=!(d[e]=c[e]))})})}function pa(a){return a&&"undefined"!=typeof a.getElementsByTagName&&a}c=ga.support={},f=ga.isXML=function(a){var b=a&&(a.ownerDocument||a).documentElement;return b?"HTML"!==b.nodeName:!1},m=ga.setDocument=function(a){var b,e,g=a?a.ownerDocument||a:v;return g!==n&&9===g.nodeType&&g.documentElement?(n=g,o=g.documentElement,e=g.defaultView,e&&e!==e.top&&(e.addEventListener?e.addEventListener("unload",ea,!1):e.attachEvent&&e.attachEvent("onunload",ea)),p=!f(g),c.attributes=ja(function(a){return a.className="i",!a.getAttribute("className")}),c.getElementsByTagName=ja(function(a){return a.appendChild(g.createComment("")),!a.getElementsByTagName("*").length}),c.getElementsByClassName=$.test(g.getElementsByClassName),c.getById=ja(function(a){return o.appendChild(a).id=u,!g.getElementsByName||!g.getElementsByName(u).length}),c.getById?(d.find.ID=function(a,b){if("undefined"!=typeof b.getElementById&&p){var c=b.getElementById(a);return c&&c.parentNode?[c]:[]}},d.filter.ID=function(a){var b=a.replace(ca,da);return function(a){return a.getAttribute("id")===b}}):(delete d.find.ID,d.filter.ID=function(a){var b=a.replace(ca,da);return function(a){var c="undefined"!=typeof a.getAttributeNode&&a.getAttributeNode("id");return c&&c.value===b}}),d.find.TAG=c.getElementsByTagName?function(a,b){return"undefined"!=typeof b.getElementsByTagName?b.getElementsByTagName(a):c.qsa?b.querySelectorAll(a):void 0}:function(a,b){var c,d=[],e=0,f=b.getElementsByTagName(a);if("*"===a){while(c=f[e++])1===c.nodeType&&d.push(c);return d}return f},d.find.CLASS=c.getElementsByClassName&&function(a,b){return p?b.getElementsByClassName(a):void 0},r=[],q=[],(c.qsa=$.test(g.querySelectorAll))&&(ja(function(a){o.appendChild(a).innerHTML="<a id='"+u+"'></a><select id='"+u+"-\f]' msallowcapture=''><option selected=''></option></select>",a.querySelectorAll("[msallowcapture^='']").length&&q.push("[*^$]="+L+"*(?:''|\"\")"),a.querySelectorAll("[selected]").length||q.push("\\["+L+"*(?:value|"+K+")"),a.querySelectorAll("[id~="+u+"-]").length||q.push("~="),a.querySelectorAll(":checked").length||q.push(":checked"),a.querySelectorAll("a#"+u+"+*").length||q.push(".#.+[+~]")}),ja(function(a){var b=g.createElement("input");b.setAttribute("type","hidden"),a.appendChild(b).setAttribute("name","D"),a.querySelectorAll("[name=d]").length&&q.push("name"+L+"*[*^$|!~]?="),a.querySelectorAll(":enabled").length||q.push(":enabled",":disabled"),a.querySelectorAll("*,:x"),q.push(",.*:")})),(c.matchesSelector=$.test(s=o.matches||o.webkitMatchesSelector||o.mozMatchesSelector||o.oMatchesSelector||o.msMatchesSelector))&&ja(function(a){c.disconnectedMatch=s.call(a,"div"),s.call(a,"[s!='']:x"),r.push("!=",P)}),q=q.length&&new RegExp(q.join("|")),r=r.length&&new RegExp(r.join("|")),b=$.test(o.compareDocumentPosition),t=b||$.test(o.contains)?function(a,b){var c=9===a.nodeType?a.documentElement:a,d=b&&b.parentNode;return a===d||!(!d||1!==d.nodeType||!(c.contains?c.contains(d):a.compareDocumentPosition&&16&a.compareDocumentPosition(d)))}:function(a,b){if(b)while(b=b.parentNode)if(b===a)return!0;return!1},B=b?function(a,b){if(a===b)return l=!0,0;var d=!a.compareDocumentPosition-!b.compareDocumentPosition;return d?d:(d=(a.ownerDocument||a)===(b.ownerDocument||b)?a.compareDocumentPosition(b):1,1&d||!c.sortDetached&&b.compareDocumentPosition(a)===d?a===g||a.ownerDocument===v&&t(v,a)?-1:b===g||b.ownerDocument===v&&t(v,b)?1:k?J(k,a)-J(k,b):0:4&d?-1:1)}:function(a,b){if(a===b)return l=!0,0;var c,d=0,e=a.parentNode,f=b.parentNode,h=[a],i=[b];if(!e||!f)return a===g?-1:b===g?1:e?-1:f?1:k?J(k,a)-J(k,b):0;if(e===f)return la(a,b);c=a;while(c=c.parentNode)h.unshift(c);c=b;while(c=c.parentNode)i.unshift(c);while(h[d]===i[d])d++;return d?la(h[d],i[d]):h[d]===v?-1:i[d]===v?1:0},g):n},ga.matches=function(a,b){return ga(a,null,null,b)},ga.matchesSelector=function(a,b){if((a.ownerDocument||a)!==n&&m(a),b=b.replace(U,"='$1']"),!(!c.matchesSelector||!p||r&&r.test(b)||q&&q.test(b)))try{var d=s.call(a,b);if(d||c.disconnectedMatch||a.document&&11!==a.document.nodeType)return d}catch(e){}return ga(b,n,null,[a]).length>0},ga.contains=function(a,b){return(a.ownerDocument||a)!==n&&m(a),t(a,b)},ga.attr=function(a,b){(a.ownerDocument||a)!==n&&m(a);var e=d.attrHandle[b.toLowerCase()],f=e&&D.call(d.attrHandle,b.toLowerCase())?e(a,b,!p):void 0;return void 0!==f?f:c.attributes||!p?a.getAttribute(b):(f=a.getAttributeNode(b))&&f.specified?f.value:null},ga.error=function(a){throw new Error("Syntax error, unrecognized expression: "+a)},ga.uniqueSort=function(a){var b,d=[],e=0,f=0;if(l=!c.detectDuplicates,k=!c.sortStable&&a.slice(0),a.sort(B),l){while(b=a[f++])b===a[f]&&(e=d.push(f));while(e--)a.splice(d[e],1)}return k=null,a},e=ga.getText=function(a){var b,c="",d=0,f=a.nodeType;if(f){if(1===f||9===f||11===f){if("string"==typeof a.textContent)return a.textContent;for(a=a.firstChild;a;a=a.nextSibling)c+=e(a)}else if(3===f||4===f)return a.nodeValue}else while(b=a[d++])c+=e(b);return c},d=ga.selectors={cacheLength:50,createPseudo:ia,match:X,attrHandle:{},find:{},relative:{">":{dir:"parentNode",first:!0}," ":{dir:"parentNode"},"+":{dir:"previousSibling",first:!0},"~":{dir:"previousSibling"}},preFilter:{ATTR:function(a){return a[1]=a[1].replace(ca,da),a[3]=(a[3]||a[4]||a[5]||"").replace(ca,da),"~="===a[2]&&(a[3]=" "+a[3]+" "),a.slice(0,4)},CHILD:function(a){return a[1]=a[1].toLowerCase(),"nth"===a[1].slice(0,3)?(a[3]||ga.error(a[0]),a[4]=+(a[4]?a[5]+(a[6]||1):2*("even"===a[3]||"odd"===a[3])),a[5]=+(a[7]+a[8]||"odd"===a[3])):a[3]&&ga.error(a[0]),a},PSEUDO:function(a){var b,c=!a[6]&&a[2];return X.CHILD.test(a[0])?null:(a[3]?a[2]=a[4]||a[5]||"":c&&V.test(c)&&(b=g(c,!0))&&(b=c.indexOf(")",c.length-b)-c.length)&&(a[0]=a[0].slice(0,b),a[2]=c.slice(0,b)),a.slice(0,3))}},filter:{TAG:function(a){var b=a.replace(ca,da).toLowerCase();return"*"===a?function(){return!0}:function(a){return a.nodeName&&a.nodeName.toLowerCase()===b}},CLASS:function(a){var b=y[a+" "];return b||(b=new RegExp("(^|"+L+")"+a+"("+L+"|$)"))&&y(a,function(a){return b.test("string"==typeof a.className&&a.className||"undefined"!=typeof a.getAttribute&&a.getAttribute("class")||"")})},ATTR:function(a,b,c){return function(d){var e=ga.attr(d,a);return null==e?"!="===b:b?(e+="","="===b?e===c:"!="===b?e!==c:"^="===b?c&&0===e.indexOf(c):"*="===b?c&&e.indexOf(c)>-1:"$="===b?c&&e.slice(-c.length)===c:"~="===b?(" "+e.replace(Q," ")+" ").indexOf(c)>-1:"|="===b?e===c||e.slice(0,c.length+1)===c+"-":!1):!0}},CHILD:function(a,b,c,d,e){var f="nth"!==a.slice(0,3),g="last"!==a.slice(-4),h="of-type"===b;return 1===d&&0===e?function(a){return!!a.parentNode}:function(b,c,i){var j,k,l,m,n,o,p=f!==g?"nextSibling":"previousSibling",q=b.parentNode,r=h&&b.nodeName.toLowerCase(),s=!i&&!h;if(q){if(f){while(p){l=b;while(l=l[p])if(h?l.nodeName.toLowerCase()===r:1===l.nodeType)return!1;o=p="only"===a&&!o&&"nextSibling"}return!0}if(o=[g?q.firstChild:q.lastChild],g&&s){k=q[u]||(q[u]={}),j=k[a]||[],n=j[0]===w&&j[1],m=j[0]===w&&j[2],l=n&&q.childNodes[n];while(l=++n&&l&&l[p]||(m=n=0)||o.pop())if(1===l.nodeType&&++m&&l===b){k[a]=[w,n,m];break}}else if(s&&(j=(b[u]||(b[u]={}))[a])&&j[0]===w)m=j[1];else while(l=++n&&l&&l[p]||(m=n=0)||o.pop())if((h?l.nodeName.toLowerCase()===r:1===l.nodeType)&&++m&&(s&&((l[u]||(l[u]={}))[a]=[w,m]),l===b))break;return m-=e,m===d||m%d===0&&m/d>=0}}},PSEUDO:function(a,b){var c,e=d.pseudos[a]||d.setFilters[a.toLowerCase()]||ga.error("unsupported pseudo: "+a);return e[u]?e(b):e.length>1?(c=[a,a,"",b],d.setFilters.hasOwnProperty(a.toLowerCase())?ia(function(a,c){var d,f=e(a,b),g=f.length;while(g--)d=J(a,f[g]),a[d]=!(c[d]=f[g])}):function(a){return e(a,0,c)}):e}},pseudos:{not:ia(function(a){var b=[],c=[],d=h(a.replace(R,"$1"));return d[u]?ia(function(a,b,c,e){var f,g=d(a,null,e,[]),h=a.length;while(h--)(f=g[h])&&(a[h]=!(b[h]=f))}):function(a,e,f){return b[0]=a,d(b,null,f,c),b[0]=null,!c.pop()}}),has:ia(function(a){return function(b){return ga(a,b).length>0}}),contains:ia(function(a){return a=a.replace(ca,da),function(b){return(b.textContent||b.innerText||e(b)).indexOf(a)>-1}}),lang:ia(function(a){return W.test(a||"")||ga.error("unsupported lang: "+a),a=a.replace(ca,da).toLowerCase(),function(b){var c;do if(c=p?b.lang:b.getAttribute("xml:lang")||b.getAttribute("lang"))return c=c.toLowerCase(),c===a||0===c.indexOf(a+"-");while((b=b.parentNode)&&1===b.nodeType);return!1}}),target:function(b){var c=a.location&&a.location.hash;return c&&c.slice(1)===b.id},root:function(a){return a===o},focus:function(a){return a===n.activeElement&&(!n.hasFocus||n.hasFocus())&&!!(a.type||a.href||~a.tabIndex)},enabled:function(a){return a.disabled===!1},disabled:function(a){return a.disabled===!0},checked:function(a){var b=a.nodeName.toLowerCase();return"input"===b&&!!a.checked||"option"===b&&!!a.selected},selected:function(a){return a.parentNode&&a.parentNode.selectedIndex,a.selected===!0},empty:function(a){for(a=a.firstChild;a;a=a.nextSibling)if(a.nodeType<6)return!1;return!0},parent:function(a){return!d.pseudos.empty(a)},header:function(a){return Z.test(a.nodeName)},input:function(a){return Y.test(a.nodeName)},button:function(a){var b=a.nodeName.toLowerCase();return"input"===b&&"button"===a.type||"button"===b},text:function(a){var b;return"input"===a.nodeName.toLowerCase()&&"text"===a.type&&(null==(b=a.getAttribute("type"))||"text"===b.toLowerCase())},first:oa(function(){return[0]}),last:oa(function(a,b){return[b-1]}),eq:oa(function(a,b,c){return[0>c?c+b:c]}),even:oa(function(a,b){for(var c=0;b>c;c+=2)a.push(c);return a}),odd:oa(function(a,b){for(var c=1;b>c;c+=2)a.push(c);return a}),lt:oa(function(a,b,c){for(var d=0>c?c+b:c;--d>=0;)a.push(d);return a}),gt:oa(function(a,b,c){for(var d=0>c?c+b:c;++d<b;)a.push(d);return a})}},d.pseudos.nth=d.pseudos.eq;for(b in{radio:!0,checkbox:!0,file:!0,password:!0,image:!0})d.pseudos[b]=ma(b);for(b in{submit:!0,reset:!0})d.pseudos[b]=na(b);function qa(){}qa.prototype=d.filters=d.pseudos,d.setFilters=new qa,g=ga.tokenize=function(a,b){var c,e,f,g,h,i,j,k=z[a+" "];if(k)return b?0:k.slice(0);h=a,i=[],j=d.preFilter;while(h){(!c||(e=S.exec(h)))&&(e&&(h=h.slice(e[0].length)||h),i.push(f=[])),c=!1,(e=T.exec(h))&&(c=e.shift(),f.push({value:c,type:e[0].replace(R," ")}),h=h.slice(c.length));for(g in d.filter)!(e=X[g].exec(h))||j[g]&&!(e=j[g](e))||(c=e.shift(),f.push({value:c,type:g,matches:e}),h=h.slice(c.length));if(!c)break}return b?h.length:h?ga.error(a):z(a,i).slice(0)};function ra(a){for(var b=0,c=a.length,d="";c>b;b++)d+=a[b].value;return d}function sa(a,b,c){var d=b.dir,e=c&&"parentNode"===d,f=x++;return b.first?function(b,c,f){while(b=b[d])if(1===b.nodeType||e)return a(b,c,f)}:function(b,c,g){var h,i,j=[w,f];if(g){while(b=b[d])if((1===b.nodeType||e)&&a(b,c,g))return!0}else while(b=b[d])if(1===b.nodeType||e){if(i=b[u]||(b[u]={}),(h=i[d])&&h[0]===w&&h[1]===f)return j[2]=h[2];if(i[d]=j,j[2]=a(b,c,g))return!0}}}function ta(a){return a.length>1?function(b,c,d){var e=a.length;while(e--)if(!a[e](b,c,d))return!1;return!0}:a[0]}function ua(a,b,c){for(var d=0,e=b.length;e>d;d++)ga(a,b[d],c);return c}function va(a,b,c,d,e){for(var f,g=[],h=0,i=a.length,j=null!=b;i>h;h++)(f=a[h])&&(!c||c(f,d,e))&&(g.push(f),j&&b.push(h));return g}function wa(a,b,c,d,e,f){return d&&!d[u]&&(d=wa(d)),e&&!e[u]&&(e=wa(e,f)),ia(function(f,g,h,i){var j,k,l,m=[],n=[],o=g.length,p=f||ua(b||"*",h.nodeType?[h]:h,[]),q=!a||!f&&b?p:va(p,m,a,h,i),r=c?e||(f?a:o||d)?[]:g:q;if(c&&c(q,r,h,i),d){j=va(r,n),d(j,[],h,i),k=j.length;while(k--)(l=j[k])&&(r[n[k]]=!(q[n[k]]=l))}if(f){if(e||a){if(e){j=[],k=r.length;while(k--)(l=r[k])&&j.push(q[k]=l);e(null,r=[],j,i)}k=r.length;while(k--)(l=r[k])&&(j=e?J(f,l):m[k])>-1&&(f[j]=!(g[j]=l))}}else r=va(r===g?r.splice(o,r.length):r),e?e(null,g,r,i):H.apply(g,r)})}function xa(a){for(var b,c,e,f=a.length,g=d.relative[a[0].type],h=g||d.relative[" "],i=g?1:0,k=sa(function(a){return a===b},h,!0),l=sa(function(a){return J(b,a)>-1},h,!0),m=[function(a,c,d){var e=!g&&(d||c!==j)||((b=c).nodeType?k(a,c,d):l(a,c,d));return b=null,e}];f>i;i++)if(c=d.relative[a[i].type])m=[sa(ta(m),c)];else{if(c=d.filter[a[i].type].apply(null,a[i].matches),c[u]){for(e=++i;f>e;e++)if(d.relative[a[e].type])break;return wa(i>1&&ta(m),i>1&&ra(a.slice(0,i-1).concat({value:" "===a[i-2].type?"*":""})).replace(R,"$1"),c,e>i&&xa(a.slice(i,e)),f>e&&xa(a=a.slice(e)),f>e&&ra(a))}m.push(c)}return ta(m)}function ya(a,b){var c=b.length>0,e=a.length>0,f=function(f,g,h,i,k){var l,m,o,p=0,q="0",r=f&&[],s=[],t=j,u=f||e&&d.find.TAG("*",k),v=w+=null==t?1:Math.random()||.1,x=u.length;for(k&&(j=g!==n&&g);q!==x&&null!=(l=u[q]);q++){if(e&&l){m=0;while(o=a[m++])if(o(l,g,h)){i.push(l);break}k&&(w=v)}c&&((l=!o&&l)&&p--,f&&r.push(l))}if(p+=q,c&&q!==p){m=0;while(o=b[m++])o(r,s,g,h);if(f){if(p>0)while(q--)r[q]||s[q]||(s[q]=F.call(i));s=va(s)}H.apply(i,s),k&&!f&&s.length>0&&p+b.length>1&&ga.uniqueSort(i)}return k&&(w=v,j=t),r};return c?ia(f):f}return h=ga.compile=function(a,b){var c,d=[],e=[],f=A[a+" "];if(!f){b||(b=g(a)),c=b.length;while(c--)f=xa(b[c]),f[u]?d.push(f):e.push(f);f=A(a,ya(e,d)),f.selector=a}return f},i=ga.select=function(a,b,e,f){var i,j,k,l,m,n="function"==typeof a&&a,o=!f&&g(a=n.selector||a);if(e=e||[],1===o.length){if(j=o[0]=o[0].slice(0),j.length>2&&"ID"===(k=j[0]).type&&c.getById&&9===b.nodeType&&p&&d.relative[j[1].type]){if(b=(d.find.ID(k.matches[0].replace(ca,da),b)||[])[0],!b)return e;n&&(b=b.parentNode),a=a.slice(j.shift().value.length)}i=X.needsContext.test(a)?0:j.length;while(i--){if(k=j[i],d.relative[l=k.type])break;if((m=d.find[l])&&(f=m(k.matches[0].replace(ca,da),aa.test(j[0].type)&&pa(b.parentNode)||b))){if(j.splice(i,1),a=f.length&&ra(j),!a)return H.apply(e,f),e;break}}}return(n||h(a,o))(f,b,!p,e,aa.test(a)&&pa(b.parentNode)||b),e},c.sortStable=u.split("").sort(B).join("")===u,c.detectDuplicates=!!l,m(),c.sortDetached=ja(function(a){return 1&a.compareDocumentPosition(n.createElement("div"))}),ja(function(a){return a.innerHTML="<a href='#'></a>","#"===a.firstChild.getAttribute("href")})||ka("type|href|height|width",function(a,b,c){return c?void 0:a.getAttribute(b,"type"===b.toLowerCase()?1:2)}),c.attributes&&ja(function(a){return a.innerHTML="<input/>",a.firstChild.setAttribute("value",""),""===a.firstChild.getAttribute("value")})||ka("value",function(a,b,c){return c||"input"!==a.nodeName.toLowerCase()?void 0:a.defaultValue}),ja(function(a){return null==a.getAttribute("disabled")})||ka(K,function(a,b,c){var d;return c?void 0:a[b]===!0?b.toLowerCase():(d=a.getAttributeNode(b))&&d.specified?d.value:null}),ga}(a);n.find=t,n.expr=t.selectors,n.expr[":"]=n.expr.pseudos,n.unique=t.uniqueSort,n.text=t.getText,n.isXMLDoc=t.isXML,n.contains=t.contains;var u=n.expr.match.needsContext,v=/^<(\w+)\s*\/?>(?:<\/\1>|)$/,w=/^.[^:#\[\.,]*$/;function x(a,b,c){if(n.isFunction(b))return n.grep(a,function(a,d){return!!b.call(a,d,a)!==c});if(b.nodeType)return n.grep(a,function(a){return a===b!==c});if("string"==typeof b){if(w.test(b))return n.filter(b,a,c);b=n.filter(b,a)}return n.grep(a,function(a){return g.call(b,a)>=0!==c})}n.filter=function(a,b,c){var d=b[0];return c&&(a=":not("+a+")"),1===b.length&&1===d.nodeType?n.find.matchesSelector(d,a)?[d]:[]:n.find.matches(a,n.grep(b,function(a){return 1===a.nodeType}))},n.fn.extend({find:function(a){var b,c=this.length,d=[],e=this;if("string"!=typeof a)return this.pushStack(n(a).filter(function(){for(b=0;c>b;b++)if(n.contains(e[b],this))return!0}));for(b=0;c>b;b++)n.find(a,e[b],d);return d=this.pushStack(c>1?n.unique(d):d),d.selector=this.selector?this.selector+" "+a:a,d},filter:function(a){return this.pushStack(x(this,a||[],!1))},not:function(a){return this.pushStack(x(this,a||[],!0))},is:function(a){return!!x(this,"string"==typeof a&&u.test(a)?n(a):a||[],!1).length}});var y,z=/^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,A=n.fn.init=function(a,b){var c,d;if(!a)return this;if("string"==typeof a){if(c="<"===a[0]&&">"===a[a.length-1]&&a.length>=3?[null,a,null]:z.exec(a),!c||!c[1]&&b)return!b||b.jquery?(b||y).find(a):this.constructor(b).find(a);if(c[1]){if(b=b instanceof n?b[0]:b,n.merge(this,n.parseHTML(c[1],b&&b.nodeType?b.ownerDocument||b:l,!0)),v.test(c[1])&&n.isPlainObject(b))for(c in b)n.isFunction(this[c])?this[c](b[c]):this.attr(c,b[c]);return this}return d=l.getElementById(c[2]),d&&d.parentNode&&(this.length=1,this[0]=d),this.context=l,this.selector=a,this}return a.nodeType?(this.context=this[0]=a,this.length=1,this):n.isFunction(a)?"undefined"!=typeof y.ready?y.ready(a):a(n):(void 0!==a.selector&&(this.selector=a.selector,this.context=a.context),n.makeArray(a,this))};A.prototype=n.fn,y=n(l);var B=/^(?:parents|prev(?:Until|All))/,C={children:!0,contents:!0,next:!0,prev:!0};n.extend({dir:function(a,b,c){var d=[],e=void 0!==c;while((a=a[b])&&9!==a.nodeType)if(1===a.nodeType){if(e&&n(a).is(c))break;d.push(a)}return d},sibling:function(a,b){for(var c=[];a;a=a.nextSibling)1===a.nodeType&&a!==b&&c.push(a);return c}}),n.fn.extend({has:function(a){var b=n(a,this),c=b.length;return this.filter(function(){for(var a=0;c>a;a++)if(n.contains(this,b[a]))return!0})},closest:function(a,b){for(var c,d=0,e=this.length,f=[],g=u.test(a)||"string"!=typeof a?n(a,b||this.context):0;e>d;d++)for(c=this[d];c&&c!==b;c=c.parentNode)if(c.nodeType<11&&(g?g.index(c)>-1:1===c.nodeType&&n.find.matchesSelector(c,a))){f.push(c);break}return this.pushStack(f.length>1?n.unique(f):f)},index:function(a){return a?"string"==typeof a?g.call(n(a),this[0]):g.call(this,a.jquery?a[0]:a):this[0]&&this[0].parentNode?this.first().prevAll().length:-1},add:function(a,b){return this.pushStack(n.unique(n.merge(this.get(),n(a,b))))},addBack:function(a){return this.add(null==a?this.prevObject:this.prevObject.filter(a))}});function D(a,b){while((a=a[b])&&1!==a.nodeType);return a}n.each({parent:function(a){var b=a.parentNode;return b&&11!==b.nodeType?b:null},parents:function(a){return n.dir(a,"parentNode")},parentsUntil:function(a,b,c){return n.dir(a,"parentNode",c)},next:function(a){return D(a,"nextSibling")},prev:function(a){return D(a,"previousSibling")},nextAll:function(a){return n.dir(a,"nextSibling")},prevAll:function(a){return n.dir(a,"previousSibling")},nextUntil:function(a,b,c){return n.dir(a,"nextSibling",c)},prevUntil:function(a,b,c){return n.dir(a,"previousSibling",c)},siblings:function(a){return n.sibling((a.parentNode||{}).firstChild,a)},children:function(a){return n.sibling(a.firstChild)},contents:function(a){return a.contentDocument||n.merge([],a.childNodes)}},function(a,b){n.fn[a]=function(c,d){var e=n.map(this,b,c);return"Until"!==a.slice(-5)&&(d=c),d&&"string"==typeof d&&(e=n.filter(d,e)),this.length>1&&(C[a]||n.unique(e),B.test(a)&&e.reverse()),this.pushStack(e)}});var E=/\S+/g,F={};function G(a){var b=F[a]={};return n.each(a.match(E)||[],function(a,c){b[c]=!0}),b}n.Callbacks=function(a){a="string"==typeof a?F[a]||G(a):n.extend({},a);var b,c,d,e,f,g,h=[],i=!a.once&&[],j=function(l){for(b=a.memory&&l,c=!0,g=e||0,e=0,f=h.length,d=!0;h&&f>g;g++)if(h[g].apply(l[0],l[1])===!1&&a.stopOnFalse){b=!1;break}d=!1,h&&(i?i.length&&j(i.shift()):b?h=[]:k.disable())},k={add:function(){if(h){var c=h.length;!function g(b){n.each(b,function(b,c){var d=n.type(c);"function"===d?a.unique&&k.has(c)||h.push(c):c&&c.length&&"string"!==d&&g(c)})}(arguments),d?f=h.length:b&&(e=c,j(b))}return this},remove:function(){return h&&n.each(arguments,function(a,b){var c;while((c=n.inArray(b,h,c))>-1)h.splice(c,1),d&&(f>=c&&f--,g>=c&&g--)}),this},has:function(a){return a?n.inArray(a,h)>-1:!(!h||!h.length)},empty:function(){return h=[],f=0,this},disable:function(){return h=i=b=void 0,this},disabled:function(){return!h},lock:function(){return i=void 0,b||k.disable(),this},locked:function(){return!i},fireWith:function(a,b){return!h||c&&!i||(b=b||[],b=[a,b.slice?b.slice():b],d?i.push(b):j(b)),this},fire:function(){return k.fireWith(this,arguments),this},fired:function(){return!!c}};return k},n.extend({Deferred:function(a){var b=[["resolve","done",n.Callbacks("once memory"),"resolved"],["reject","fail",n.Callbacks("once memory"),"rejected"],["notify","progress",n.Callbacks("memory")]],c="pending",d={state:function(){return c},always:function(){return e.done(arguments).fail(arguments),this},then:function(){var a=arguments;return n.Deferred(function(c){n.each(b,function(b,f){var g=n.isFunction(a[b])&&a[b];e[f[1]](function(){var a=g&&g.apply(this,arguments);a&&n.isFunction(a.promise)?a.promise().done(c.resolve).fail(c.reject).progress(c.notify):c[f[0]+"With"](this===d?c.promise():this,g?[a]:arguments)})}),a=null}).promise()},promise:function(a){return null!=a?n.extend(a,d):d}},e={};return d.pipe=d.then,n.each(b,function(a,f){var g=f[2],h=f[3];d[f[1]]=g.add,h&&g.add(function(){c=h},b[1^a][2].disable,b[2][2].lock),e[f[0]]=function(){return e[f[0]+"With"](this===e?d:this,arguments),this},e[f[0]+"With"]=g.fireWith}),d.promise(e),a&&a.call(e,e),e},when:function(a){var b=0,c=d.call(arguments),e=c.length,f=1!==e||a&&n.isFunction(a.promise)?e:0,g=1===f?a:n.Deferred(),h=function(a,b,c){return function(e){b[a]=this,c[a]=arguments.length>1?d.call(arguments):e,c===i?g.notifyWith(b,c):--f||g.resolveWith(b,c)}},i,j,k;if(e>1)for(i=new Array(e),j=new Array(e),k=new Array(e);e>b;b++)c[b]&&n.isFunction(c[b].promise)?c[b].promise().done(h(b,k,c)).fail(g.reject).progress(h(b,j,i)):--f;return f||g.resolveWith(k,c),g.promise()}});var H;n.fn.ready=function(a){return n.ready.promise().done(a),this},n.extend({isReady:!1,readyWait:1,holdReady:function(a){a?n.readyWait++:n.ready(!0)},ready:function(a){(a===!0?--n.readyWait:n.isReady)||(n.isReady=!0,a!==!0&&--n.readyWait>0||(H.resolveWith(l,[n]),n.fn.triggerHandler&&(n(l).triggerHandler("ready"),n(l).off("ready"))))}});function I(){l.removeEventListener("DOMContentLoaded",I,!1),a.removeEventListener("load",I,!1),n.ready()}n.ready.promise=function(b){return H||(H=n.Deferred(),"complete"===l.readyState?setTimeout(n.ready):(l.addEventListener("DOMContentLoaded",I,!1),a.addEventListener("load",I,!1))),H.promise(b)},n.ready.promise();var J=n.access=function(a,b,c,d,e,f,g){var h=0,i=a.length,j=null==c;if("object"===n.type(c)){e=!0;for(h in c)n.access(a,b,h,c[h],!0,f,g)}else if(void 0!==d&&(e=!0,n.isFunction(d)||(g=!0),j&&(g?(b.call(a,d),b=null):(j=b,b=function(a,b,c){return j.call(n(a),c)})),b))for(;i>h;h++)b(a[h],c,g?d:d.call(a[h],h,b(a[h],c)));return e?a:j?b.call(a):i?b(a[0],c):f};n.acceptData=function(a){return 1===a.nodeType||9===a.nodeType||!+a.nodeType};function K(){Object.defineProperty(this.cache={},0,{get:function(){return{}}}),this.expando=n.expando+K.uid++}K.uid=1,K.accepts=n.acceptData,K.prototype={key:function(a){if(!K.accepts(a))return 0;var b={},c=a[this.expando];if(!c){c=K.uid++;try{b[this.expando]={value:c},Object.defineProperties(a,b)}catch(d){b[this.expando]=c,n.extend(a,b)}}return this.cache[c]||(this.cache[c]={}),c},set:function(a,b,c){var d,e=this.key(a),f=this.cache[e];if("string"==typeof b)f[b]=c;else if(n.isEmptyObject(f))n.extend(this.cache[e],b);else for(d in b)f[d]=b[d];return f},get:function(a,b){var c=this.cache[this.key(a)];return void 0===b?c:c[b]},access:function(a,b,c){var d;return void 0===b||b&&"string"==typeof b&&void 0===c?(d=this.get(a,b),void 0!==d?d:this.get(a,n.camelCase(b))):(this.set(a,b,c),void 0!==c?c:b)},remove:function(a,b){var c,d,e,f=this.key(a),g=this.cache[f];if(void 0===b)this.cache[f]={};else{n.isArray(b)?d=b.concat(b.map(n.camelCase)):(e=n.camelCase(b),b in g?d=[b,e]:(d=e,d=d in g?[d]:d.match(E)||[])),c=d.length;while(c--)delete g[d[c]]}},hasData:function(a){return!n.isEmptyObject(this.cache[a[this.expando]]||{})},discard:function(a){a[this.expando]&&delete this.cache[a[this.expando]]}};var L=new K,M=new K,N=/^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,O=/([A-Z])/g;function P(a,b,c){var d;if(void 0===c&&1===a.nodeType)if(d="data-"+b.replace(O,"-$1").toLowerCase(),c=a.getAttribute(d),"string"==typeof c){try{c="true"===c?!0:"false"===c?!1:"null"===c?null:+c+""===c?+c:N.test(c)?n.parseJSON(c):c}catch(e){}M.set(a,b,c)}else c=void 0;return c}n.extend({hasData:function(a){return M.hasData(a)||L.hasData(a)},data:function(a,b,c){
return M.access(a,b,c)},removeData:function(a,b){M.remove(a,b)},_data:function(a,b,c){return L.access(a,b,c)},_removeData:function(a,b){L.remove(a,b)}}),n.fn.extend({data:function(a,b){var c,d,e,f=this[0],g=f&&f.attributes;if(void 0===a){if(this.length&&(e=M.get(f),1===f.nodeType&&!L.get(f,"hasDataAttrs"))){c=g.length;while(c--)g[c]&&(d=g[c].name,0===d.indexOf("data-")&&(d=n.camelCase(d.slice(5)),P(f,d,e[d])));L.set(f,"hasDataAttrs",!0)}return e}return"object"==typeof a?this.each(function(){M.set(this,a)}):J(this,function(b){var c,d=n.camelCase(a);if(f&&void 0===b){if(c=M.get(f,a),void 0!==c)return c;if(c=M.get(f,d),void 0!==c)return c;if(c=P(f,d,void 0),void 0!==c)return c}else this.each(function(){var c=M.get(this,d);M.set(this,d,b),-1!==a.indexOf("-")&&void 0!==c&&M.set(this,a,b)})},null,b,arguments.length>1,null,!0)},removeData:function(a){return this.each(function(){M.remove(this,a)})}}),n.extend({queue:function(a,b,c){var d;return a?(b=(b||"fx")+"queue",d=L.get(a,b),c&&(!d||n.isArray(c)?d=L.access(a,b,n.makeArray(c)):d.push(c)),d||[]):void 0},dequeue:function(a,b){b=b||"fx";var c=n.queue(a,b),d=c.length,e=c.shift(),f=n._queueHooks(a,b),g=function(){n.dequeue(a,b)};"inprogress"===e&&(e=c.shift(),d--),e&&("fx"===b&&c.unshift("inprogress"),delete f.stop,e.call(a,g,f)),!d&&f&&f.empty.fire()},_queueHooks:function(a,b){var c=b+"queueHooks";return L.get(a,c)||L.access(a,c,{empty:n.Callbacks("once memory").add(function(){L.remove(a,[b+"queue",c])})})}}),n.fn.extend({queue:function(a,b){var c=2;return"string"!=typeof a&&(b=a,a="fx",c--),arguments.length<c?n.queue(this[0],a):void 0===b?this:this.each(function(){var c=n.queue(this,a,b);n._queueHooks(this,a),"fx"===a&&"inprogress"!==c[0]&&n.dequeue(this,a)})},dequeue:function(a){return this.each(function(){n.dequeue(this,a)})},clearQueue:function(a){return this.queue(a||"fx",[])},promise:function(a,b){var c,d=1,e=n.Deferred(),f=this,g=this.length,h=function(){--d||e.resolveWith(f,[f])};"string"!=typeof a&&(b=a,a=void 0),a=a||"fx";while(g--)c=L.get(f[g],a+"queueHooks"),c&&c.empty&&(d++,c.empty.add(h));return h(),e.promise(b)}});var Q=/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,R=["Top","Right","Bottom","Left"],S=function(a,b){return a=b||a,"none"===n.css(a,"display")||!n.contains(a.ownerDocument,a)},T=/^(?:checkbox|radio)$/i;!function(){var a=l.createDocumentFragment(),b=a.appendChild(l.createElement("div")),c=l.createElement("input");c.setAttribute("type","radio"),c.setAttribute("checked","checked"),c.setAttribute("name","t"),b.appendChild(c),k.checkClone=b.cloneNode(!0).cloneNode(!0).lastChild.checked,b.innerHTML="<textarea>x</textarea>",k.noCloneChecked=!!b.cloneNode(!0).lastChild.defaultValue}();var U="undefined";k.focusinBubbles="onfocusin"in a;var V=/^key/,W=/^(?:mouse|pointer|contextmenu)|click/,X=/^(?:focusinfocus|focusoutblur)$/,Y=/^([^.]*)(?:\.(.+)|)$/;function Z(){return!0}function $(){return!1}function _(){try{return l.activeElement}catch(a){}}n.event={global:{},add:function(a,b,c,d,e){var f,g,h,i,j,k,l,m,o,p,q,r=L.get(a);if(r){c.handler&&(f=c,c=f.handler,e=f.selector),c.guid||(c.guid=n.guid++),(i=r.events)||(i=r.events={}),(g=r.handle)||(g=r.handle=function(b){return typeof n!==U&&n.event.triggered!==b.type?n.event.dispatch.apply(a,arguments):void 0}),b=(b||"").match(E)||[""],j=b.length;while(j--)h=Y.exec(b[j])||[],o=q=h[1],p=(h[2]||"").split(".").sort(),o&&(l=n.event.special[o]||{},o=(e?l.delegateType:l.bindType)||o,l=n.event.special[o]||{},k=n.extend({type:o,origType:q,data:d,handler:c,guid:c.guid,selector:e,needsContext:e&&n.expr.match.needsContext.test(e),namespace:p.join(".")},f),(m=i[o])||(m=i[o]=[],m.delegateCount=0,l.setup&&l.setup.call(a,d,p,g)!==!1||a.addEventListener&&a.addEventListener(o,g,!1)),l.add&&(l.add.call(a,k),k.handler.guid||(k.handler.guid=c.guid)),e?m.splice(m.delegateCount++,0,k):m.push(k),n.event.global[o]=!0)}},remove:function(a,b,c,d,e){var f,g,h,i,j,k,l,m,o,p,q,r=L.hasData(a)&&L.get(a);if(r&&(i=r.events)){b=(b||"").match(E)||[""],j=b.length;while(j--)if(h=Y.exec(b[j])||[],o=q=h[1],p=(h[2]||"").split(".").sort(),o){l=n.event.special[o]||{},o=(d?l.delegateType:l.bindType)||o,m=i[o]||[],h=h[2]&&new RegExp("(^|\\.)"+p.join("\\.(?:.*\\.|)")+"(\\.|$)"),g=f=m.length;while(f--)k=m[f],!e&&q!==k.origType||c&&c.guid!==k.guid||h&&!h.test(k.namespace)||d&&d!==k.selector&&("**"!==d||!k.selector)||(m.splice(f,1),k.selector&&m.delegateCount--,l.remove&&l.remove.call(a,k));g&&!m.length&&(l.teardown&&l.teardown.call(a,p,r.handle)!==!1||n.removeEvent(a,o,r.handle),delete i[o])}else for(o in i)n.event.remove(a,o+b[j],c,d,!0);n.isEmptyObject(i)&&(delete r.handle,L.remove(a,"events"))}},trigger:function(b,c,d,e){var f,g,h,i,k,m,o,p=[d||l],q=j.call(b,"type")?b.type:b,r=j.call(b,"namespace")?b.namespace.split("."):[];if(g=h=d=d||l,3!==d.nodeType&&8!==d.nodeType&&!X.test(q+n.event.triggered)&&(q.indexOf(".")>=0&&(r=q.split("."),q=r.shift(),r.sort()),k=q.indexOf(":")<0&&"on"+q,b=b[n.expando]?b:new n.Event(q,"object"==typeof b&&b),b.isTrigger=e?2:3,b.namespace=r.join("."),b.namespace_re=b.namespace?new RegExp("(^|\\.)"+r.join("\\.(?:.*\\.|)")+"(\\.|$)"):null,b.result=void 0,b.target||(b.target=d),c=null==c?[b]:n.makeArray(c,[b]),o=n.event.special[q]||{},e||!o.trigger||o.trigger.apply(d,c)!==!1)){if(!e&&!o.noBubble&&!n.isWindow(d)){for(i=o.delegateType||q,X.test(i+q)||(g=g.parentNode);g;g=g.parentNode)p.push(g),h=g;h===(d.ownerDocument||l)&&p.push(h.defaultView||h.parentWindow||a)}f=0;while((g=p[f++])&&!b.isPropagationStopped())b.type=f>1?i:o.bindType||q,m=(L.get(g,"events")||{})[b.type]&&L.get(g,"handle"),m&&m.apply(g,c),m=k&&g[k],m&&m.apply&&n.acceptData(g)&&(b.result=m.apply(g,c),b.result===!1&&b.preventDefault());return b.type=q,e||b.isDefaultPrevented()||o._default&&o._default.apply(p.pop(),c)!==!1||!n.acceptData(d)||k&&n.isFunction(d[q])&&!n.isWindow(d)&&(h=d[k],h&&(d[k]=null),n.event.triggered=q,d[q](),n.event.triggered=void 0,h&&(d[k]=h)),b.result}},dispatch:function(a){a=n.event.fix(a);var b,c,e,f,g,h=[],i=d.call(arguments),j=(L.get(this,"events")||{})[a.type]||[],k=n.event.special[a.type]||{};if(i[0]=a,a.delegateTarget=this,!k.preDispatch||k.preDispatch.call(this,a)!==!1){h=n.event.handlers.call(this,a,j),b=0;while((f=h[b++])&&!a.isPropagationStopped()){a.currentTarget=f.elem,c=0;while((g=f.handlers[c++])&&!a.isImmediatePropagationStopped())(!a.namespace_re||a.namespace_re.test(g.namespace))&&(a.handleObj=g,a.data=g.data,e=((n.event.special[g.origType]||{}).handle||g.handler).apply(f.elem,i),void 0!==e&&(a.result=e)===!1&&(a.preventDefault(),a.stopPropagation()))}return k.postDispatch&&k.postDispatch.call(this,a),a.result}},handlers:function(a,b){var c,d,e,f,g=[],h=b.delegateCount,i=a.target;if(h&&i.nodeType&&(!a.button||"click"!==a.type))for(;i!==this;i=i.parentNode||this)if(i.disabled!==!0||"click"!==a.type){for(d=[],c=0;h>c;c++)f=b[c],e=f.selector+" ",void 0===d[e]&&(d[e]=f.needsContext?n(e,this).index(i)>=0:n.find(e,this,null,[i]).length),d[e]&&d.push(f);d.length&&g.push({elem:i,handlers:d})}return h<b.length&&g.push({elem:this,handlers:b.slice(h)}),g},props:"altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),fixHooks:{},keyHooks:{props:"char charCode key keyCode".split(" "),filter:function(a,b){return null==a.which&&(a.which=null!=b.charCode?b.charCode:b.keyCode),a}},mouseHooks:{props:"button buttons clientX clientY offsetX offsetY pageX pageY screenX screenY toElement".split(" "),filter:function(a,b){var c,d,e,f=b.button;return null==a.pageX&&null!=b.clientX&&(c=a.target.ownerDocument||l,d=c.documentElement,e=c.body,a.pageX=b.clientX+(d&&d.scrollLeft||e&&e.scrollLeft||0)-(d&&d.clientLeft||e&&e.clientLeft||0),a.pageY=b.clientY+(d&&d.scrollTop||e&&e.scrollTop||0)-(d&&d.clientTop||e&&e.clientTop||0)),a.which||void 0===f||(a.which=1&f?1:2&f?3:4&f?2:0),a}},fix:function(a){if(a[n.expando])return a;var b,c,d,e=a.type,f=a,g=this.fixHooks[e];g||(this.fixHooks[e]=g=W.test(e)?this.mouseHooks:V.test(e)?this.keyHooks:{}),d=g.props?this.props.concat(g.props):this.props,a=new n.Event(f),b=d.length;while(b--)c=d[b],a[c]=f[c];return a.target||(a.target=l),3===a.target.nodeType&&(a.target=a.target.parentNode),g.filter?g.filter(a,f):a},special:{load:{noBubble:!0},focus:{trigger:function(){return this!==_()&&this.focus?(this.focus(),!1):void 0},delegateType:"focusin"},blur:{trigger:function(){return this===_()&&this.blur?(this.blur(),!1):void 0},delegateType:"focusout"},click:{trigger:function(){return"checkbox"===this.type&&this.click&&n.nodeName(this,"input")?(this.click(),!1):void 0},_default:function(a){return n.nodeName(a.target,"a")}},beforeunload:{postDispatch:function(a){void 0!==a.result&&a.originalEvent&&(a.originalEvent.returnValue=a.result)}}},simulate:function(a,b,c,d){var e=n.extend(new n.Event,c,{type:a,isSimulated:!0,originalEvent:{}});d?n.event.trigger(e,null,b):n.event.dispatch.call(b,e),e.isDefaultPrevented()&&c.preventDefault()}},n.removeEvent=function(a,b,c){a.removeEventListener&&a.removeEventListener(b,c,!1)},n.Event=function(a,b){return this instanceof n.Event?(a&&a.type?(this.originalEvent=a,this.type=a.type,this.isDefaultPrevented=a.defaultPrevented||void 0===a.defaultPrevented&&a.returnValue===!1?Z:$):this.type=a,b&&n.extend(this,b),this.timeStamp=a&&a.timeStamp||n.now(),void(this[n.expando]=!0)):new n.Event(a,b)},n.Event.prototype={isDefaultPrevented:$,isPropagationStopped:$,isImmediatePropagationStopped:$,preventDefault:function(){var a=this.originalEvent;this.isDefaultPrevented=Z,a&&a.preventDefault&&a.preventDefault()},stopPropagation:function(){var a=this.originalEvent;this.isPropagationStopped=Z,a&&a.stopPropagation&&a.stopPropagation()},stopImmediatePropagation:function(){var a=this.originalEvent;this.isImmediatePropagationStopped=Z,a&&a.stopImmediatePropagation&&a.stopImmediatePropagation(),this.stopPropagation()}},n.each({mouseenter:"mouseover",mouseleave:"mouseout",pointerenter:"pointerover",pointerleave:"pointerout"},function(a,b){n.event.special[a]={delegateType:b,bindType:b,handle:function(a){var c,d=this,e=a.relatedTarget,f=a.handleObj;return(!e||e!==d&&!n.contains(d,e))&&(a.type=f.origType,c=f.handler.apply(this,arguments),a.type=b),c}}}),k.focusinBubbles||n.each({focus:"focusin",blur:"focusout"},function(a,b){var c=function(a){n.event.simulate(b,a.target,n.event.fix(a),!0)};n.event.special[b]={setup:function(){var d=this.ownerDocument||this,e=L.access(d,b);e||d.addEventListener(a,c,!0),L.access(d,b,(e||0)+1)},teardown:function(){var d=this.ownerDocument||this,e=L.access(d,b)-1;e?L.access(d,b,e):(d.removeEventListener(a,c,!0),L.remove(d,b))}}}),n.fn.extend({on:function(a,b,c,d,e){var f,g;if("object"==typeof a){"string"!=typeof b&&(c=c||b,b=void 0);for(g in a)this.on(g,b,c,a[g],e);return this}if(null==c&&null==d?(d=b,c=b=void 0):null==d&&("string"==typeof b?(d=c,c=void 0):(d=c,c=b,b=void 0)),d===!1)d=$;else if(!d)return this;return 1===e&&(f=d,d=function(a){return n().off(a),f.apply(this,arguments)},d.guid=f.guid||(f.guid=n.guid++)),this.each(function(){n.event.add(this,a,d,c,b)})},one:function(a,b,c,d){return this.on(a,b,c,d,1)},off:function(a,b,c){var d,e;if(a&&a.preventDefault&&a.handleObj)return d=a.handleObj,n(a.delegateTarget).off(d.namespace?d.origType+"."+d.namespace:d.origType,d.selector,d.handler),this;if("object"==typeof a){for(e in a)this.off(e,b,a[e]);return this}return(b===!1||"function"==typeof b)&&(c=b,b=void 0),c===!1&&(c=$),this.each(function(){n.event.remove(this,a,c,b)})},trigger:function(a,b){return this.each(function(){n.event.trigger(a,b,this)})},triggerHandler:function(a,b){var c=this[0];return c?n.event.trigger(a,b,c,!0):void 0}});var aa=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,ba=/<([\w:]+)/,ca=/<|&#?\w+;/,da=/<(?:script|style|link)/i,ea=/checked\s*(?:[^=]|=\s*.checked.)/i,fa=/^$|\/(?:java|ecma)script/i,ga=/^true\/(.*)/,ha=/^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,ia={option:[1,"<select multiple='multiple'>","</select>"],thead:[1,"<table>","</table>"],col:[2,"<table><colgroup>","</colgroup></table>"],tr:[2,"<table><tbody>","</tbody></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],_default:[0,"",""]};ia.optgroup=ia.option,ia.tbody=ia.tfoot=ia.colgroup=ia.caption=ia.thead,ia.th=ia.td;function ja(a,b){return n.nodeName(a,"table")&&n.nodeName(11!==b.nodeType?b:b.firstChild,"tr")?a.getElementsByTagName("tbody")[0]||a.appendChild(a.ownerDocument.createElement("tbody")):a}function ka(a){return a.type=(null!==a.getAttribute("type"))+"/"+a.type,a}function la(a){var b=ga.exec(a.type);return b?a.type=b[1]:a.removeAttribute("type"),a}function ma(a,b){for(var c=0,d=a.length;d>c;c++)L.set(a[c],"globalEval",!b||L.get(b[c],"globalEval"))}function na(a,b){var c,d,e,f,g,h,i,j;if(1===b.nodeType){if(L.hasData(a)&&(f=L.access(a),g=L.set(b,f),j=f.events)){delete g.handle,g.events={};for(e in j)for(c=0,d=j[e].length;d>c;c++)n.event.add(b,e,j[e][c])}M.hasData(a)&&(h=M.access(a),i=n.extend({},h),M.set(b,i))}}function oa(a,b){var c=a.getElementsByTagName?a.getElementsByTagName(b||"*"):a.querySelectorAll?a.querySelectorAll(b||"*"):[];return void 0===b||b&&n.nodeName(a,b)?n.merge([a],c):c}function pa(a,b){var c=b.nodeName.toLowerCase();"input"===c&&T.test(a.type)?b.checked=a.checked:("input"===c||"textarea"===c)&&(b.defaultValue=a.defaultValue)}n.extend({clone:function(a,b,c){var d,e,f,g,h=a.cloneNode(!0),i=n.contains(a.ownerDocument,a);if(!(k.noCloneChecked||1!==a.nodeType&&11!==a.nodeType||n.isXMLDoc(a)))for(g=oa(h),f=oa(a),d=0,e=f.length;e>d;d++)pa(f[d],g[d]);if(b)if(c)for(f=f||oa(a),g=g||oa(h),d=0,e=f.length;e>d;d++)na(f[d],g[d]);else na(a,h);return g=oa(h,"script"),g.length>0&&ma(g,!i&&oa(a,"script")),h},buildFragment:function(a,b,c,d){for(var e,f,g,h,i,j,k=b.createDocumentFragment(),l=[],m=0,o=a.length;o>m;m++)if(e=a[m],e||0===e)if("object"===n.type(e))n.merge(l,e.nodeType?[e]:e);else if(ca.test(e)){f=f||k.appendChild(b.createElement("div")),g=(ba.exec(e)||["",""])[1].toLowerCase(),h=ia[g]||ia._default,f.innerHTML=h[1]+e.replace(aa,"<$1></$2>")+h[2],j=h[0];while(j--)f=f.lastChild;n.merge(l,f.childNodes),f=k.firstChild,f.textContent=""}else l.push(b.createTextNode(e));k.textContent="",m=0;while(e=l[m++])if((!d||-1===n.inArray(e,d))&&(i=n.contains(e.ownerDocument,e),f=oa(k.appendChild(e),"script"),i&&ma(f),c)){j=0;while(e=f[j++])fa.test(e.type||"")&&c.push(e)}return k},cleanData:function(a){for(var b,c,d,e,f=n.event.special,g=0;void 0!==(c=a[g]);g++){if(n.acceptData(c)&&(e=c[L.expando],e&&(b=L.cache[e]))){if(b.events)for(d in b.events)f[d]?n.event.remove(c,d):n.removeEvent(c,d,b.handle);L.cache[e]&&delete L.cache[e]}delete M.cache[c[M.expando]]}}}),n.fn.extend({text:function(a){return J(this,function(a){return void 0===a?n.text(this):this.empty().each(function(){(1===this.nodeType||11===this.nodeType||9===this.nodeType)&&(this.textContent=a)})},null,a,arguments.length)},append:function(){return this.domManip(arguments,function(a){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var b=ja(this,a);b.appendChild(a)}})},prepend:function(){return this.domManip(arguments,function(a){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var b=ja(this,a);b.insertBefore(a,b.firstChild)}})},before:function(){return this.domManip(arguments,function(a){this.parentNode&&this.parentNode.insertBefore(a,this)})},after:function(){return this.domManip(arguments,function(a){this.parentNode&&this.parentNode.insertBefore(a,this.nextSibling)})},remove:function(a,b){for(var c,d=a?n.filter(a,this):this,e=0;null!=(c=d[e]);e++)b||1!==c.nodeType||n.cleanData(oa(c)),c.parentNode&&(b&&n.contains(c.ownerDocument,c)&&ma(oa(c,"script")),c.parentNode.removeChild(c));return this},empty:function(){for(var a,b=0;null!=(a=this[b]);b++)1===a.nodeType&&(n.cleanData(oa(a,!1)),a.textContent="");return this},clone:function(a,b){return a=null==a?!1:a,b=null==b?a:b,this.map(function(){return n.clone(this,a,b)})},html:function(a){return J(this,function(a){var b=this[0]||{},c=0,d=this.length;if(void 0===a&&1===b.nodeType)return b.innerHTML;if("string"==typeof a&&!da.test(a)&&!ia[(ba.exec(a)||["",""])[1].toLowerCase()]){a=a.replace(aa,"<$1></$2>");try{for(;d>c;c++)b=this[c]||{},1===b.nodeType&&(n.cleanData(oa(b,!1)),b.innerHTML=a);b=0}catch(e){}}b&&this.empty().append(a)},null,a,arguments.length)},replaceWith:function(){var a=arguments[0];return this.domManip(arguments,function(b){a=this.parentNode,n.cleanData(oa(this)),a&&a.replaceChild(b,this)}),a&&(a.length||a.nodeType)?this:this.remove()},detach:function(a){return this.remove(a,!0)},domManip:function(a,b){a=e.apply([],a);var c,d,f,g,h,i,j=0,l=this.length,m=this,o=l-1,p=a[0],q=n.isFunction(p);if(q||l>1&&"string"==typeof p&&!k.checkClone&&ea.test(p))return this.each(function(c){var d=m.eq(c);q&&(a[0]=p.call(this,c,d.html())),d.domManip(a,b)});if(l&&(c=n.buildFragment(a,this[0].ownerDocument,!1,this),d=c.firstChild,1===c.childNodes.length&&(c=d),d)){for(f=n.map(oa(c,"script"),ka),g=f.length;l>j;j++)h=c,j!==o&&(h=n.clone(h,!0,!0),g&&n.merge(f,oa(h,"script"))),b.call(this[j],h,j);if(g)for(i=f[f.length-1].ownerDocument,n.map(f,la),j=0;g>j;j++)h=f[j],fa.test(h.type||"")&&!L.access(h,"globalEval")&&n.contains(i,h)&&(h.src?n._evalUrl&&n._evalUrl(h.src):n.globalEval(h.textContent.replace(ha,"")))}return this}}),n.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(a,b){n.fn[a]=function(a){for(var c,d=[],e=n(a),g=e.length-1,h=0;g>=h;h++)c=h===g?this:this.clone(!0),n(e[h])[b](c),f.apply(d,c.get());return this.pushStack(d)}});var qa,ra={};function sa(b,c){var d,e=n(c.createElement(b)).appendTo(c.body),f=a.getDefaultComputedStyle&&(d=a.getDefaultComputedStyle(e[0]))?d.display:n.css(e[0],"display");return e.detach(),f}function ta(a){var b=l,c=ra[a];return c||(c=sa(a,b),"none"!==c&&c||(qa=(qa||n("<iframe frameborder='0' width='0' height='0'/>")).appendTo(b.documentElement),b=qa[0].contentDocument,b.write(),b.close(),c=sa(a,b),qa.detach()),ra[a]=c),c}var ua=/^margin/,va=new RegExp("^("+Q+")(?!px)[a-z%]+$","i"),wa=function(b){return b.ownerDocument.defaultView.opener?b.ownerDocument.defaultView.getComputedStyle(b,null):a.getComputedStyle(b,null)};function xa(a,b,c){var d,e,f,g,h=a.style;return c=c||wa(a),c&&(g=c.getPropertyValue(b)||c[b]),c&&(""!==g||n.contains(a.ownerDocument,a)||(g=n.style(a,b)),va.test(g)&&ua.test(b)&&(d=h.width,e=h.minWidth,f=h.maxWidth,h.minWidth=h.maxWidth=h.width=g,g=c.width,h.width=d,h.minWidth=e,h.maxWidth=f)),void 0!==g?g+"":g}function ya(a,b){return{get:function(){return a()?void delete this.get:(this.get=b).apply(this,arguments)}}}!function(){var b,c,d=l.documentElement,e=l.createElement("div"),f=l.createElement("div");if(f.style){f.style.backgroundClip="content-box",f.cloneNode(!0).style.backgroundClip="",k.clearCloneStyle="content-box"===f.style.backgroundClip,e.style.cssText="border:0;width:0;height:0;top:0;left:-9999px;margin-top:1px;position:absolute",e.appendChild(f);function g(){f.style.cssText="-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:block;margin-top:1%;top:1%;border:1px;padding:1px;width:4px;position:absolute",f.innerHTML="",d.appendChild(e);var g=a.getComputedStyle(f,null);b="1%"!==g.top,c="4px"===g.width,d.removeChild(e)}a.getComputedStyle&&n.extend(k,{pixelPosition:function(){return g(),b},boxSizingReliable:function(){return null==c&&g(),c},reliableMarginRight:function(){var b,c=f.appendChild(l.createElement("div"));return c.style.cssText=f.style.cssText="-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0",c.style.marginRight=c.style.width="0",f.style.width="1px",d.appendChild(e),b=!parseFloat(a.getComputedStyle(c,null).marginRight),d.removeChild(e),f.removeChild(c),b}})}}(),n.swap=function(a,b,c,d){var e,f,g={};for(f in b)g[f]=a.style[f],a.style[f]=b[f];e=c.apply(a,d||[]);for(f in b)a.style[f]=g[f];return e};var za=/^(none|table(?!-c[ea]).+)/,Aa=new RegExp("^("+Q+")(.*)$","i"),Ba=new RegExp("^([+-])=("+Q+")","i"),Ca={position:"absolute",visibility:"hidden",display:"block"},Da={letterSpacing:"0",fontWeight:"400"},Ea=["Webkit","O","Moz","ms"];function Fa(a,b){if(b in a)return b;var c=b[0].toUpperCase()+b.slice(1),d=b,e=Ea.length;while(e--)if(b=Ea[e]+c,b in a)return b;return d}function Ga(a,b,c){var d=Aa.exec(b);return d?Math.max(0,d[1]-(c||0))+(d[2]||"px"):b}function Ha(a,b,c,d,e){for(var f=c===(d?"border":"content")?4:"width"===b?1:0,g=0;4>f;f+=2)"margin"===c&&(g+=n.css(a,c+R[f],!0,e)),d?("content"===c&&(g-=n.css(a,"padding"+R[f],!0,e)),"margin"!==c&&(g-=n.css(a,"border"+R[f]+"Width",!0,e))):(g+=n.css(a,"padding"+R[f],!0,e),"padding"!==c&&(g+=n.css(a,"border"+R[f]+"Width",!0,e)));return g}function Ia(a,b,c){var d=!0,e="width"===b?a.offsetWidth:a.offsetHeight,f=wa(a),g="border-box"===n.css(a,"boxSizing",!1,f);if(0>=e||null==e){if(e=xa(a,b,f),(0>e||null==e)&&(e=a.style[b]),va.test(e))return e;d=g&&(k.boxSizingReliable()||e===a.style[b]),e=parseFloat(e)||0}return e+Ha(a,b,c||(g?"border":"content"),d,f)+"px"}function Ja(a,b){for(var c,d,e,f=[],g=0,h=a.length;h>g;g++)d=a[g],d.style&&(f[g]=L.get(d,"olddisplay"),c=d.style.display,b?(f[g]||"none"!==c||(d.style.display=""),""===d.style.display&&S(d)&&(f[g]=L.access(d,"olddisplay",ta(d.nodeName)))):(e=S(d),"none"===c&&e||L.set(d,"olddisplay",e?c:n.css(d,"display"))));for(g=0;h>g;g++)d=a[g],d.style&&(b&&"none"!==d.style.display&&""!==d.style.display||(d.style.display=b?f[g]||"":"none"));return a}n.extend({cssHooks:{opacity:{get:function(a,b){if(b){var c=xa(a,"opacity");return""===c?"1":c}}}},cssNumber:{columnCount:!0,fillOpacity:!0,flexGrow:!0,flexShrink:!0,fontWeight:!0,lineHeight:!0,opacity:!0,order:!0,orphans:!0,widows:!0,zIndex:!0,zoom:!0},cssProps:{"float":"cssFloat"},style:function(a,b,c,d){if(a&&3!==a.nodeType&&8!==a.nodeType&&a.style){var e,f,g,h=n.camelCase(b),i=a.style;return b=n.cssProps[h]||(n.cssProps[h]=Fa(i,h)),g=n.cssHooks[b]||n.cssHooks[h],void 0===c?g&&"get"in g&&void 0!==(e=g.get(a,!1,d))?e:i[b]:(f=typeof c,"string"===f&&(e=Ba.exec(c))&&(c=(e[1]+1)*e[2]+parseFloat(n.css(a,b)),f="number"),null!=c&&c===c&&("number"!==f||n.cssNumber[h]||(c+="px"),k.clearCloneStyle||""!==c||0!==b.indexOf("background")||(i[b]="inherit"),g&&"set"in g&&void 0===(c=g.set(a,c,d))||(i[b]=c)),void 0)}},css:function(a,b,c,d){var e,f,g,h=n.camelCase(b);return b=n.cssProps[h]||(n.cssProps[h]=Fa(a.style,h)),g=n.cssHooks[b]||n.cssHooks[h],g&&"get"in g&&(e=g.get(a,!0,c)),void 0===e&&(e=xa(a,b,d)),"normal"===e&&b in Da&&(e=Da[b]),""===c||c?(f=parseFloat(e),c===!0||n.isNumeric(f)?f||0:e):e}}),n.each(["height","width"],function(a,b){n.cssHooks[b]={get:function(a,c,d){return c?za.test(n.css(a,"display"))&&0===a.offsetWidth?n.swap(a,Ca,function(){return Ia(a,b,d)}):Ia(a,b,d):void 0},set:function(a,c,d){var e=d&&wa(a);return Ga(a,c,d?Ha(a,b,d,"border-box"===n.css(a,"boxSizing",!1,e),e):0)}}}),n.cssHooks.marginRight=ya(k.reliableMarginRight,function(a,b){return b?n.swap(a,{display:"inline-block"},xa,[a,"marginRight"]):void 0}),n.each({margin:"",padding:"",border:"Width"},function(a,b){n.cssHooks[a+b]={expand:function(c){for(var d=0,e={},f="string"==typeof c?c.split(" "):[c];4>d;d++)e[a+R[d]+b]=f[d]||f[d-2]||f[0];return e}},ua.test(a)||(n.cssHooks[a+b].set=Ga)}),n.fn.extend({css:function(a,b){return J(this,function(a,b,c){var d,e,f={},g=0;if(n.isArray(b)){for(d=wa(a),e=b.length;e>g;g++)f[b[g]]=n.css(a,b[g],!1,d);return f}return void 0!==c?n.style(a,b,c):n.css(a,b)},a,b,arguments.length>1)},show:function(){return Ja(this,!0)},hide:function(){return Ja(this)},toggle:function(a){return"boolean"==typeof a?a?this.show():this.hide():this.each(function(){S(this)?n(this).show():n(this).hide()})}});function Ka(a,b,c,d,e){return new Ka.prototype.init(a,b,c,d,e)}n.Tween=Ka,Ka.prototype={constructor:Ka,init:function(a,b,c,d,e,f){this.elem=a,this.prop=c,this.easing=e||"swing",this.options=b,this.start=this.now=this.cur(),this.end=d,this.unit=f||(n.cssNumber[c]?"":"px")},cur:function(){var a=Ka.propHooks[this.prop];return a&&a.get?a.get(this):Ka.propHooks._default.get(this)},run:function(a){var b,c=Ka.propHooks[this.prop];return this.options.duration?this.pos=b=n.easing[this.easing](a,this.options.duration*a,0,1,this.options.duration):this.pos=b=a,this.now=(this.end-this.start)*b+this.start,this.options.step&&this.options.step.call(this.elem,this.now,this),c&&c.set?c.set(this):Ka.propHooks._default.set(this),this}},Ka.prototype.init.prototype=Ka.prototype,Ka.propHooks={_default:{get:function(a){var b;return null==a.elem[a.prop]||a.elem.style&&null!=a.elem.style[a.prop]?(b=n.css(a.elem,a.prop,""),b&&"auto"!==b?b:0):a.elem[a.prop]},set:function(a){n.fx.step[a.prop]?n.fx.step[a.prop](a):a.elem.style&&(null!=a.elem.style[n.cssProps[a.prop]]||n.cssHooks[a.prop])?n.style(a.elem,a.prop,a.now+a.unit):a.elem[a.prop]=a.now}}},Ka.propHooks.scrollTop=Ka.propHooks.scrollLeft={set:function(a){a.elem.nodeType&&a.elem.parentNode&&(a.elem[a.prop]=a.now)}},n.easing={linear:function(a){return a},swing:function(a){return.5-Math.cos(a*Math.PI)/2}},n.fx=Ka.prototype.init,n.fx.step={};var La,Ma,Na=/^(?:toggle|show|hide)$/,Oa=new RegExp("^(?:([+-])=|)("+Q+")([a-z%]*)$","i"),Pa=/queueHooks$/,Qa=[Va],Ra={"*":[function(a,b){var c=this.createTween(a,b),d=c.cur(),e=Oa.exec(b),f=e&&e[3]||(n.cssNumber[a]?"":"px"),g=(n.cssNumber[a]||"px"!==f&&+d)&&Oa.exec(n.css(c.elem,a)),h=1,i=20;if(g&&g[3]!==f){f=f||g[3],e=e||[],g=+d||1;do h=h||".5",g/=h,n.style(c.elem,a,g+f);while(h!==(h=c.cur()/d)&&1!==h&&--i)}return e&&(g=c.start=+g||+d||0,c.unit=f,c.end=e[1]?g+(e[1]+1)*e[2]:+e[2]),c}]};function Sa(){return setTimeout(function(){La=void 0}),La=n.now()}function Ta(a,b){var c,d=0,e={height:a};for(b=b?1:0;4>d;d+=2-b)c=R[d],e["margin"+c]=e["padding"+c]=a;return b&&(e.opacity=e.width=a),e}function Ua(a,b,c){for(var d,e=(Ra[b]||[]).concat(Ra["*"]),f=0,g=e.length;g>f;f++)if(d=e[f].call(c,b,a))return d}function Va(a,b,c){var d,e,f,g,h,i,j,k,l=this,m={},o=a.style,p=a.nodeType&&S(a),q=L.get(a,"fxshow");c.queue||(h=n._queueHooks(a,"fx"),null==h.unqueued&&(h.unqueued=0,i=h.empty.fire,h.empty.fire=function(){h.unqueued||i()}),h.unqueued++,l.always(function(){l.always(function(){h.unqueued--,n.queue(a,"fx").length||h.empty.fire()})})),1===a.nodeType&&("height"in b||"width"in b)&&(c.overflow=[o.overflow,o.overflowX,o.overflowY],j=n.css(a,"display"),k="none"===j?L.get(a,"olddisplay")||ta(a.nodeName):j,"inline"===k&&"none"===n.css(a,"float")&&(o.display="inline-block")),c.overflow&&(o.overflow="hidden",l.always(function(){o.overflow=c.overflow[0],o.overflowX=c.overflow[1],o.overflowY=c.overflow[2]}));for(d in b)if(e=b[d],Na.exec(e)){if(delete b[d],f=f||"toggle"===e,e===(p?"hide":"show")){if("show"!==e||!q||void 0===q[d])continue;p=!0}m[d]=q&&q[d]||n.style(a,d)}else j=void 0;if(n.isEmptyObject(m))"inline"===("none"===j?ta(a.nodeName):j)&&(o.display=j);else{q?"hidden"in q&&(p=q.hidden):q=L.access(a,"fxshow",{}),f&&(q.hidden=!p),p?n(a).show():l.done(function(){n(a).hide()}),l.done(function(){var b;L.remove(a,"fxshow");for(b in m)n.style(a,b,m[b])});for(d in m)g=Ua(p?q[d]:0,d,l),d in q||(q[d]=g.start,p&&(g.end=g.start,g.start="width"===d||"height"===d?1:0))}}function Wa(a,b){var c,d,e,f,g;for(c in a)if(d=n.camelCase(c),e=b[d],f=a[c],n.isArray(f)&&(e=f[1],f=a[c]=f[0]),c!==d&&(a[d]=f,delete a[c]),g=n.cssHooks[d],g&&"expand"in g){f=g.expand(f),delete a[d];for(c in f)c in a||(a[c]=f[c],b[c]=e)}else b[d]=e}function Xa(a,b,c){var d,e,f=0,g=Qa.length,h=n.Deferred().always(function(){delete i.elem}),i=function(){if(e)return!1;for(var b=La||Sa(),c=Math.max(0,j.startTime+j.duration-b),d=c/j.duration||0,f=1-d,g=0,i=j.tweens.length;i>g;g++)j.tweens[g].run(f);return h.notifyWith(a,[j,f,c]),1>f&&i?c:(h.resolveWith(a,[j]),!1)},j=h.promise({elem:a,props:n.extend({},b),opts:n.extend(!0,{specialEasing:{}},c),originalProperties:b,originalOptions:c,startTime:La||Sa(),duration:c.duration,tweens:[],createTween:function(b,c){var d=n.Tween(a,j.opts,b,c,j.opts.specialEasing[b]||j.opts.easing);return j.tweens.push(d),d},stop:function(b){var c=0,d=b?j.tweens.length:0;if(e)return this;for(e=!0;d>c;c++)j.tweens[c].run(1);return b?h.resolveWith(a,[j,b]):h.rejectWith(a,[j,b]),this}}),k=j.props;for(Wa(k,j.opts.specialEasing);g>f;f++)if(d=Qa[f].call(j,a,k,j.opts))return d;return n.map(k,Ua,j),n.isFunction(j.opts.start)&&j.opts.start.call(a,j),n.fx.timer(n.extend(i,{elem:a,anim:j,queue:j.opts.queue})),j.progress(j.opts.progress).done(j.opts.done,j.opts.complete).fail(j.opts.fail).always(j.opts.always)}n.Animation=n.extend(Xa,{tweener:function(a,b){n.isFunction(a)?(b=a,a=["*"]):a=a.split(" ");for(var c,d=0,e=a.length;e>d;d++)c=a[d],Ra[c]=Ra[c]||[],Ra[c].unshift(b)},prefilter:function(a,b){b?Qa.unshift(a):Qa.push(a)}}),n.speed=function(a,b,c){var d=a&&"object"==typeof a?n.extend({},a):{complete:c||!c&&b||n.isFunction(a)&&a,duration:a,easing:c&&b||b&&!n.isFunction(b)&&b};return d.duration=n.fx.off?0:"number"==typeof d.duration?d.duration:d.duration in n.fx.speeds?n.fx.speeds[d.duration]:n.fx.speeds._default,(null==d.queue||d.queue===!0)&&(d.queue="fx"),d.old=d.complete,d.complete=function(){n.isFunction(d.old)&&d.old.call(this),d.queue&&n.dequeue(this,d.queue)},d},n.fn.extend({fadeTo:function(a,b,c,d){return this.filter(S).css("opacity",0).show().end().animate({opacity:b},a,c,d)},animate:function(a,b,c,d){var e=n.isEmptyObject(a),f=n.speed(b,c,d),g=function(){var b=Xa(this,n.extend({},a),f);(e||L.get(this,"finish"))&&b.stop(!0)};return g.finish=g,e||f.queue===!1?this.each(g):this.queue(f.queue,g)},stop:function(a,b,c){var d=function(a){var b=a.stop;delete a.stop,b(c)};return"string"!=typeof a&&(c=b,b=a,a=void 0),b&&a!==!1&&this.queue(a||"fx",[]),this.each(function(){var b=!0,e=null!=a&&a+"queueHooks",f=n.timers,g=L.get(this);if(e)g[e]&&g[e].stop&&d(g[e]);else for(e in g)g[e]&&g[e].stop&&Pa.test(e)&&d(g[e]);for(e=f.length;e--;)f[e].elem!==this||null!=a&&f[e].queue!==a||(f[e].anim.stop(c),b=!1,f.splice(e,1));(b||!c)&&n.dequeue(this,a)})},finish:function(a){return a!==!1&&(a=a||"fx"),this.each(function(){var b,c=L.get(this),d=c[a+"queue"],e=c[a+"queueHooks"],f=n.timers,g=d?d.length:0;for(c.finish=!0,n.queue(this,a,[]),e&&e.stop&&e.stop.call(this,!0),b=f.length;b--;)f[b].elem===this&&f[b].queue===a&&(f[b].anim.stop(!0),f.splice(b,1));for(b=0;g>b;b++)d[b]&&d[b].finish&&d[b].finish.call(this);delete c.finish})}}),n.each(["toggle","show","hide"],function(a,b){var c=n.fn[b];n.fn[b]=function(a,d,e){return null==a||"boolean"==typeof a?c.apply(this,arguments):this.animate(Ta(b,!0),a,d,e)}}),n.each({slideDown:Ta("show"),slideUp:Ta("hide"),slideToggle:Ta("toggle"),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"},fadeToggle:{opacity:"toggle"}},function(a,b){n.fn[a]=function(a,c,d){return this.animate(b,a,c,d)}}),n.timers=[],n.fx.tick=function(){var a,b=0,c=n.timers;for(La=n.now();b<c.length;b++)a=c[b],a()||c[b]!==a||c.splice(b--,1);c.length||n.fx.stop(),La=void 0},n.fx.timer=function(a){n.timers.push(a),a()?n.fx.start():n.timers.pop()},n.fx.interval=13,n.fx.start=function(){Ma||(Ma=setInterval(n.fx.tick,n.fx.interval))},n.fx.stop=function(){clearInterval(Ma),Ma=null},n.fx.speeds={slow:600,fast:200,_default:400},n.fn.delay=function(a,b){return a=n.fx?n.fx.speeds[a]||a:a,b=b||"fx",this.queue(b,function(b,c){var d=setTimeout(b,a);c.stop=function(){clearTimeout(d)}})},function(){var a=l.createElement("input"),b=l.createElement("select"),c=b.appendChild(l.createElement("option"));a.type="checkbox",k.checkOn=""!==a.value,k.optSelected=c.selected,b.disabled=!0,k.optDisabled=!c.disabled,a=l.createElement("input"),a.value="t",a.type="radio",k.radioValue="t"===a.value}();var Ya,Za,$a=n.expr.attrHandle;n.fn.extend({attr:function(a,b){return J(this,n.attr,a,b,arguments.length>1)},removeAttr:function(a){return this.each(function(){n.removeAttr(this,a)})}}),n.extend({attr:function(a,b,c){var d,e,f=a.nodeType;if(a&&3!==f&&8!==f&&2!==f)return typeof a.getAttribute===U?n.prop(a,b,c):(1===f&&n.isXMLDoc(a)||(b=b.toLowerCase(),d=n.attrHooks[b]||(n.expr.match.bool.test(b)?Za:Ya)),
void 0===c?d&&"get"in d&&null!==(e=d.get(a,b))?e:(e=n.find.attr(a,b),null==e?void 0:e):null!==c?d&&"set"in d&&void 0!==(e=d.set(a,c,b))?e:(a.setAttribute(b,c+""),c):void n.removeAttr(a,b))},removeAttr:function(a,b){var c,d,e=0,f=b&&b.match(E);if(f&&1===a.nodeType)while(c=f[e++])d=n.propFix[c]||c,n.expr.match.bool.test(c)&&(a[d]=!1),a.removeAttribute(c)},attrHooks:{type:{set:function(a,b){if(!k.radioValue&&"radio"===b&&n.nodeName(a,"input")){var c=a.value;return a.setAttribute("type",b),c&&(a.value=c),b}}}}}),Za={set:function(a,b,c){return b===!1?n.removeAttr(a,c):a.setAttribute(c,c),c}},n.each(n.expr.match.bool.source.match(/\w+/g),function(a,b){var c=$a[b]||n.find.attr;$a[b]=function(a,b,d){var e,f;return d||(f=$a[b],$a[b]=e,e=null!=c(a,b,d)?b.toLowerCase():null,$a[b]=f),e}});var _a=/^(?:input|select|textarea|button)$/i;n.fn.extend({prop:function(a,b){return J(this,n.prop,a,b,arguments.length>1)},removeProp:function(a){return this.each(function(){delete this[n.propFix[a]||a]})}}),n.extend({propFix:{"for":"htmlFor","class":"className"},prop:function(a,b,c){var d,e,f,g=a.nodeType;if(a&&3!==g&&8!==g&&2!==g)return f=1!==g||!n.isXMLDoc(a),f&&(b=n.propFix[b]||b,e=n.propHooks[b]),void 0!==c?e&&"set"in e&&void 0!==(d=e.set(a,c,b))?d:a[b]=c:e&&"get"in e&&null!==(d=e.get(a,b))?d:a[b]},propHooks:{tabIndex:{get:function(a){return a.hasAttribute("tabindex")||_a.test(a.nodeName)||a.href?a.tabIndex:-1}}}}),k.optSelected||(n.propHooks.selected={get:function(a){var b=a.parentNode;return b&&b.parentNode&&b.parentNode.selectedIndex,null}}),n.each(["tabIndex","readOnly","maxLength","cellSpacing","cellPadding","rowSpan","colSpan","useMap","frameBorder","contentEditable"],function(){n.propFix[this.toLowerCase()]=this});var ab=/[\t\r\n\f]/g;n.fn.extend({addClass:function(a){var b,c,d,e,f,g,h="string"==typeof a&&a,i=0,j=this.length;if(n.isFunction(a))return this.each(function(b){n(this).addClass(a.call(this,b,this.className))});if(h)for(b=(a||"").match(E)||[];j>i;i++)if(c=this[i],d=1===c.nodeType&&(c.className?(" "+c.className+" ").replace(ab," "):" ")){f=0;while(e=b[f++])d.indexOf(" "+e+" ")<0&&(d+=e+" ");g=n.trim(d),c.className!==g&&(c.className=g)}return this},removeClass:function(a){var b,c,d,e,f,g,h=0===arguments.length||"string"==typeof a&&a,i=0,j=this.length;if(n.isFunction(a))return this.each(function(b){n(this).removeClass(a.call(this,b,this.className))});if(h)for(b=(a||"").match(E)||[];j>i;i++)if(c=this[i],d=1===c.nodeType&&(c.className?(" "+c.className+" ").replace(ab," "):"")){f=0;while(e=b[f++])while(d.indexOf(" "+e+" ")>=0)d=d.replace(" "+e+" "," ");g=a?n.trim(d):"",c.className!==g&&(c.className=g)}return this},toggleClass:function(a,b){var c=typeof a;return"boolean"==typeof b&&"string"===c?b?this.addClass(a):this.removeClass(a):this.each(n.isFunction(a)?function(c){n(this).toggleClass(a.call(this,c,this.className,b),b)}:function(){if("string"===c){var b,d=0,e=n(this),f=a.match(E)||[];while(b=f[d++])e.hasClass(b)?e.removeClass(b):e.addClass(b)}else(c===U||"boolean"===c)&&(this.className&&L.set(this,"__className__",this.className),this.className=this.className||a===!1?"":L.get(this,"__className__")||"")})},hasClass:function(a){for(var b=" "+a+" ",c=0,d=this.length;d>c;c++)if(1===this[c].nodeType&&(" "+this[c].className+" ").replace(ab," ").indexOf(b)>=0)return!0;return!1}});var bb=/\r/g;n.fn.extend({val:function(a){var b,c,d,e=this[0];{if(arguments.length)return d=n.isFunction(a),this.each(function(c){var e;1===this.nodeType&&(e=d?a.call(this,c,n(this).val()):a,null==e?e="":"number"==typeof e?e+="":n.isArray(e)&&(e=n.map(e,function(a){return null==a?"":a+""})),b=n.valHooks[this.type]||n.valHooks[this.nodeName.toLowerCase()],b&&"set"in b&&void 0!==b.set(this,e,"value")||(this.value=e))});if(e)return b=n.valHooks[e.type]||n.valHooks[e.nodeName.toLowerCase()],b&&"get"in b&&void 0!==(c=b.get(e,"value"))?c:(c=e.value,"string"==typeof c?c.replace(bb,""):null==c?"":c)}}}),n.extend({valHooks:{option:{get:function(a){var b=n.find.attr(a,"value");return null!=b?b:n.trim(n.text(a))}},select:{get:function(a){for(var b,c,d=a.options,e=a.selectedIndex,f="select-one"===a.type||0>e,g=f?null:[],h=f?e+1:d.length,i=0>e?h:f?e:0;h>i;i++)if(c=d[i],!(!c.selected&&i!==e||(k.optDisabled?c.disabled:null!==c.getAttribute("disabled"))||c.parentNode.disabled&&n.nodeName(c.parentNode,"optgroup"))){if(b=n(c).val(),f)return b;g.push(b)}return g},set:function(a,b){var c,d,e=a.options,f=n.makeArray(b),g=e.length;while(g--)d=e[g],(d.selected=n.inArray(d.value,f)>=0)&&(c=!0);return c||(a.selectedIndex=-1),f}}}}),n.each(["radio","checkbox"],function(){n.valHooks[this]={set:function(a,b){return n.isArray(b)?a.checked=n.inArray(n(a).val(),b)>=0:void 0}},k.checkOn||(n.valHooks[this].get=function(a){return null===a.getAttribute("value")?"on":a.value})}),n.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "),function(a,b){n.fn[b]=function(a,c){return arguments.length>0?this.on(b,null,a,c):this.trigger(b)}}),n.fn.extend({hover:function(a,b){return this.mouseenter(a).mouseleave(b||a)},bind:function(a,b,c){return this.on(a,null,b,c)},unbind:function(a,b){return this.off(a,null,b)},delegate:function(a,b,c,d){return this.on(b,a,c,d)},undelegate:function(a,b,c){return 1===arguments.length?this.off(a,"**"):this.off(b,a||"**",c)}});var cb=n.now(),db=/\?/;n.parseJSON=function(a){return JSON.parse(a+"")},n.parseXML=function(a){var b,c;if(!a||"string"!=typeof a)return null;try{c=new DOMParser,b=c.parseFromString(a,"text/xml")}catch(d){b=void 0}return(!b||b.getElementsByTagName("parsererror").length)&&n.error("Invalid XML: "+a),b};var eb=/#.*$/,fb=/([?&])_=[^&]*/,gb=/^(.*?):[ \t]*([^\r\n]*)$/gm,hb=/^(?:about|app|app-storage|.+-extension|file|res|widget):$/,ib=/^(?:GET|HEAD)$/,jb=/^\/\//,kb=/^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,lb={},mb={},nb="*/".concat("*"),ob=a.location.href,pb=kb.exec(ob.toLowerCase())||[];function qb(a){return function(b,c){"string"!=typeof b&&(c=b,b="*");var d,e=0,f=b.toLowerCase().match(E)||[];if(n.isFunction(c))while(d=f[e++])"+"===d[0]?(d=d.slice(1)||"*",(a[d]=a[d]||[]).unshift(c)):(a[d]=a[d]||[]).push(c)}}function rb(a,b,c,d){var e={},f=a===mb;function g(h){var i;return e[h]=!0,n.each(a[h]||[],function(a,h){var j=h(b,c,d);return"string"!=typeof j||f||e[j]?f?!(i=j):void 0:(b.dataTypes.unshift(j),g(j),!1)}),i}return g(b.dataTypes[0])||!e["*"]&&g("*")}function sb(a,b){var c,d,e=n.ajaxSettings.flatOptions||{};for(c in b)void 0!==b[c]&&((e[c]?a:d||(d={}))[c]=b[c]);return d&&n.extend(!0,a,d),a}function tb(a,b,c){var d,e,f,g,h=a.contents,i=a.dataTypes;while("*"===i[0])i.shift(),void 0===d&&(d=a.mimeType||b.getResponseHeader("Content-Type"));if(d)for(e in h)if(h[e]&&h[e].test(d)){i.unshift(e);break}if(i[0]in c)f=i[0];else{for(e in c){if(!i[0]||a.converters[e+" "+i[0]]){f=e;break}g||(g=e)}f=f||g}return f?(f!==i[0]&&i.unshift(f),c[f]):void 0}function ub(a,b,c,d){var e,f,g,h,i,j={},k=a.dataTypes.slice();if(k[1])for(g in a.converters)j[g.toLowerCase()]=a.converters[g];f=k.shift();while(f)if(a.responseFields[f]&&(c[a.responseFields[f]]=b),!i&&d&&a.dataFilter&&(b=a.dataFilter(b,a.dataType)),i=f,f=k.shift())if("*"===f)f=i;else if("*"!==i&&i!==f){if(g=j[i+" "+f]||j["* "+f],!g)for(e in j)if(h=e.split(" "),h[1]===f&&(g=j[i+" "+h[0]]||j["* "+h[0]])){g===!0?g=j[e]:j[e]!==!0&&(f=h[0],k.unshift(h[1]));break}if(g!==!0)if(g&&a["throws"])b=g(b);else try{b=g(b)}catch(l){return{state:"parsererror",error:g?l:"No conversion from "+i+" to "+f}}}return{state:"success",data:b}}n.extend({active:0,lastModified:{},etag:{},ajaxSettings:{url:ob,type:"GET",isLocal:hb.test(pb[1]),global:!0,processData:!0,async:!0,contentType:"application/x-www-form-urlencoded; charset=UTF-8",accepts:{"*":nb,text:"text/plain",html:"text/html",xml:"application/xml, text/xml",json:"application/json, text/javascript"},contents:{xml:/xml/,html:/html/,json:/json/},responseFields:{xml:"responseXML",text:"responseText",json:"responseJSON"},converters:{"* text":String,"text html":!0,"text json":n.parseJSON,"text xml":n.parseXML},flatOptions:{url:!0,context:!0}},ajaxSetup:function(a,b){return b?sb(sb(a,n.ajaxSettings),b):sb(n.ajaxSettings,a)},ajaxPrefilter:qb(lb),ajaxTransport:qb(mb),ajax:function(a,b){"object"==typeof a&&(b=a,a=void 0),b=b||{};var c,d,e,f,g,h,i,j,k=n.ajaxSetup({},b),l=k.context||k,m=k.context&&(l.nodeType||l.jquery)?n(l):n.event,o=n.Deferred(),p=n.Callbacks("once memory"),q=k.statusCode||{},r={},s={},t=0,u="canceled",v={readyState:0,getResponseHeader:function(a){var b;if(2===t){if(!f){f={};while(b=gb.exec(e))f[b[1].toLowerCase()]=b[2]}b=f[a.toLowerCase()]}return null==b?null:b},getAllResponseHeaders:function(){return 2===t?e:null},setRequestHeader:function(a,b){var c=a.toLowerCase();return t||(a=s[c]=s[c]||a,r[a]=b),this},overrideMimeType:function(a){return t||(k.mimeType=a),this},statusCode:function(a){var b;if(a)if(2>t)for(b in a)q[b]=[q[b],a[b]];else v.always(a[v.status]);return this},abort:function(a){var b=a||u;return c&&c.abort(b),x(0,b),this}};if(o.promise(v).complete=p.add,v.success=v.done,v.error=v.fail,k.url=((a||k.url||ob)+"").replace(eb,"").replace(jb,pb[1]+"//"),k.type=b.method||b.type||k.method||k.type,k.dataTypes=n.trim(k.dataType||"*").toLowerCase().match(E)||[""],null==k.crossDomain&&(h=kb.exec(k.url.toLowerCase()),k.crossDomain=!(!h||h[1]===pb[1]&&h[2]===pb[2]&&(h[3]||("http:"===h[1]?"80":"443"))===(pb[3]||("http:"===pb[1]?"80":"443")))),k.data&&k.processData&&"string"!=typeof k.data&&(k.data=n.param(k.data,k.traditional)),rb(lb,k,b,v),2===t)return v;i=n.event&&k.global,i&&0===n.active++&&n.event.trigger("ajaxStart"),k.type=k.type.toUpperCase(),k.hasContent=!ib.test(k.type),d=k.url,k.hasContent||(k.data&&(d=k.url+=(db.test(d)?"&":"?")+k.data,delete k.data),k.cache===!1&&(k.url=fb.test(d)?d.replace(fb,"$1_="+cb++):d+(db.test(d)?"&":"?")+"_="+cb++)),k.ifModified&&(n.lastModified[d]&&v.setRequestHeader("If-Modified-Since",n.lastModified[d]),n.etag[d]&&v.setRequestHeader("If-None-Match",n.etag[d])),(k.data&&k.hasContent&&k.contentType!==!1||b.contentType)&&v.setRequestHeader("Content-Type",k.contentType),v.setRequestHeader("Accept",k.dataTypes[0]&&k.accepts[k.dataTypes[0]]?k.accepts[k.dataTypes[0]]+("*"!==k.dataTypes[0]?", "+nb+"; q=0.01":""):k.accepts["*"]);for(j in k.headers)v.setRequestHeader(j,k.headers[j]);if(k.beforeSend&&(k.beforeSend.call(l,v,k)===!1||2===t))return v.abort();u="abort";for(j in{success:1,error:1,complete:1})v[j](k[j]);if(c=rb(mb,k,b,v)){v.readyState=1,i&&m.trigger("ajaxSend",[v,k]),k.async&&k.timeout>0&&(g=setTimeout(function(){v.abort("timeout")},k.timeout));try{t=1,c.send(r,x)}catch(w){if(!(2>t))throw w;x(-1,w)}}else x(-1,"No Transport");function x(a,b,f,h){var j,r,s,u,w,x=b;2!==t&&(t=2,g&&clearTimeout(g),c=void 0,e=h||"",v.readyState=a>0?4:0,j=a>=200&&300>a||304===a,f&&(u=tb(k,v,f)),u=ub(k,u,v,j),j?(k.ifModified&&(w=v.getResponseHeader("Last-Modified"),w&&(n.lastModified[d]=w),w=v.getResponseHeader("etag"),w&&(n.etag[d]=w)),204===a||"HEAD"===k.type?x="nocontent":304===a?x="notmodified":(x=u.state,r=u.data,s=u.error,j=!s)):(s=x,(a||!x)&&(x="error",0>a&&(a=0))),v.status=a,v.statusText=(b||x)+"",j?o.resolveWith(l,[r,x,v]):o.rejectWith(l,[v,x,s]),v.statusCode(q),q=void 0,i&&m.trigger(j?"ajaxSuccess":"ajaxError",[v,k,j?r:s]),p.fireWith(l,[v,x]),i&&(m.trigger("ajaxComplete",[v,k]),--n.active||n.event.trigger("ajaxStop")))}return v},getJSON:function(a,b,c){return n.get(a,b,c,"json")},getScript:function(a,b){return n.get(a,void 0,b,"script")}}),n.each(["get","post"],function(a,b){n[b]=function(a,c,d,e){return n.isFunction(c)&&(e=e||d,d=c,c=void 0),n.ajax({url:a,type:b,dataType:e,data:c,success:d})}}),n._evalUrl=function(a){return n.ajax({url:a,type:"GET",dataType:"script",async:!1,global:!1,"throws":!0})},n.fn.extend({wrapAll:function(a){var b;return n.isFunction(a)?this.each(function(b){n(this).wrapAll(a.call(this,b))}):(this[0]&&(b=n(a,this[0].ownerDocument).eq(0).clone(!0),this[0].parentNode&&b.insertBefore(this[0]),b.map(function(){var a=this;while(a.firstElementChild)a=a.firstElementChild;return a}).append(this)),this)},wrapInner:function(a){return this.each(n.isFunction(a)?function(b){n(this).wrapInner(a.call(this,b))}:function(){var b=n(this),c=b.contents();c.length?c.wrapAll(a):b.append(a)})},wrap:function(a){var b=n.isFunction(a);return this.each(function(c){n(this).wrapAll(b?a.call(this,c):a)})},unwrap:function(){return this.parent().each(function(){n.nodeName(this,"body")||n(this).replaceWith(this.childNodes)}).end()}}),n.expr.filters.hidden=function(a){return a.offsetWidth<=0&&a.offsetHeight<=0},n.expr.filters.visible=function(a){return!n.expr.filters.hidden(a)};var vb=/%20/g,wb=/\[\]$/,xb=/\r?\n/g,yb=/^(?:submit|button|image|reset|file)$/i,zb=/^(?:input|select|textarea|keygen)/i;function Ab(a,b,c,d){var e;if(n.isArray(b))n.each(b,function(b,e){c||wb.test(a)?d(a,e):Ab(a+"["+("object"==typeof e?b:"")+"]",e,c,d)});else if(c||"object"!==n.type(b))d(a,b);else for(e in b)Ab(a+"["+e+"]",b[e],c,d)}n.param=function(a,b){var c,d=[],e=function(a,b){b=n.isFunction(b)?b():null==b?"":b,d[d.length]=encodeURIComponent(a)+"="+encodeURIComponent(b)};if(void 0===b&&(b=n.ajaxSettings&&n.ajaxSettings.traditional),n.isArray(a)||a.jquery&&!n.isPlainObject(a))n.each(a,function(){e(this.name,this.value)});else for(c in a)Ab(c,a[c],b,e);return d.join("&").replace(vb,"+")},n.fn.extend({serialize:function(){return n.param(this.serializeArray())},serializeArray:function(){return this.map(function(){var a=n.prop(this,"elements");return a?n.makeArray(a):this}).filter(function(){var a=this.type;return this.name&&!n(this).is(":disabled")&&zb.test(this.nodeName)&&!yb.test(a)&&(this.checked||!T.test(a))}).map(function(a,b){var c=n(this).val();return null==c?null:n.isArray(c)?n.map(c,function(a){return{name:b.name,value:a.replace(xb,"\r\n")}}):{name:b.name,value:c.replace(xb,"\r\n")}}).get()}}),n.ajaxSettings.xhr=function(){try{return new XMLHttpRequest}catch(a){}};var Bb=0,Cb={},Db={0:200,1223:204},Eb=n.ajaxSettings.xhr();a.attachEvent&&a.attachEvent("onunload",function(){for(var a in Cb)Cb[a]()}),k.cors=!!Eb&&"withCredentials"in Eb,k.ajax=Eb=!!Eb,n.ajaxTransport(function(a){var b;return k.cors||Eb&&!a.crossDomain?{send:function(c,d){var e,f=a.xhr(),g=++Bb;if(f.open(a.type,a.url,a.async,a.username,a.password),a.xhrFields)for(e in a.xhrFields)f[e]=a.xhrFields[e];a.mimeType&&f.overrideMimeType&&f.overrideMimeType(a.mimeType),a.crossDomain||c["X-Requested-With"]||(c["X-Requested-With"]="XMLHttpRequest");for(e in c)f.setRequestHeader(e,c[e]);b=function(a){return function(){b&&(delete Cb[g],b=f.onload=f.onerror=null,"abort"===a?f.abort():"error"===a?d(f.status,f.statusText):d(Db[f.status]||f.status,f.statusText,"string"==typeof f.responseText?{text:f.responseText}:void 0,f.getAllResponseHeaders()))}},f.onload=b(),f.onerror=b("error"),b=Cb[g]=b("abort");try{f.send(a.hasContent&&a.data||null)}catch(h){if(b)throw h}},abort:function(){b&&b()}}:void 0}),n.ajaxSetup({accepts:{script:"text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},contents:{script:/(?:java|ecma)script/},converters:{"text script":function(a){return n.globalEval(a),a}}}),n.ajaxPrefilter("script",function(a){void 0===a.cache&&(a.cache=!1),a.crossDomain&&(a.type="GET")}),n.ajaxTransport("script",function(a){if(a.crossDomain){var b,c;return{send:function(d,e){b=n("<script>").prop({async:!0,charset:a.scriptCharset,src:a.url}).on("load error",c=function(a){b.remove(),c=null,a&&e("error"===a.type?404:200,a.type)}),l.head.appendChild(b[0])},abort:function(){c&&c()}}}});var Fb=[],Gb=/(=)\?(?=&|$)|\?\?/;n.ajaxSetup({jsonp:"callback",jsonpCallback:function(){var a=Fb.pop()||n.expando+"_"+cb++;return this[a]=!0,a}}),n.ajaxPrefilter("json jsonp",function(b,c,d){var e,f,g,h=b.jsonp!==!1&&(Gb.test(b.url)?"url":"string"==typeof b.data&&!(b.contentType||"").indexOf("application/x-www-form-urlencoded")&&Gb.test(b.data)&&"data");return h||"jsonp"===b.dataTypes[0]?(e=b.jsonpCallback=n.isFunction(b.jsonpCallback)?b.jsonpCallback():b.jsonpCallback,h?b[h]=b[h].replace(Gb,"$1"+e):b.jsonp!==!1&&(b.url+=(db.test(b.url)?"&":"?")+b.jsonp+"="+e),b.converters["script json"]=function(){return g||n.error(e+" was not called"),g[0]},b.dataTypes[0]="json",f=a[e],a[e]=function(){g=arguments},d.always(function(){a[e]=f,b[e]&&(b.jsonpCallback=c.jsonpCallback,Fb.push(e)),g&&n.isFunction(f)&&f(g[0]),g=f=void 0}),"script"):void 0}),n.parseHTML=function(a,b,c){if(!a||"string"!=typeof a)return null;"boolean"==typeof b&&(c=b,b=!1),b=b||l;var d=v.exec(a),e=!c&&[];return d?[b.createElement(d[1])]:(d=n.buildFragment([a],b,e),e&&e.length&&n(e).remove(),n.merge([],d.childNodes))};var Hb=n.fn.load;n.fn.load=function(a,b,c){if("string"!=typeof a&&Hb)return Hb.apply(this,arguments);var d,e,f,g=this,h=a.indexOf(" ");return h>=0&&(d=n.trim(a.slice(h)),a=a.slice(0,h)),n.isFunction(b)?(c=b,b=void 0):b&&"object"==typeof b&&(e="POST"),g.length>0&&n.ajax({url:a,type:e,dataType:"html",data:b}).done(function(a){f=arguments,g.html(d?n("<div>").append(n.parseHTML(a)).find(d):a)}).complete(c&&function(a,b){g.each(c,f||[a.responseText,b,a])}),this},n.each(["ajaxStart","ajaxStop","ajaxComplete","ajaxError","ajaxSuccess","ajaxSend"],function(a,b){n.fn[b]=function(a){return this.on(b,a)}}),n.expr.filters.animated=function(a){return n.grep(n.timers,function(b){return a===b.elem}).length};var Ib=a.document.documentElement;function Jb(a){return n.isWindow(a)?a:9===a.nodeType&&a.defaultView}n.offset={setOffset:function(a,b,c){var d,e,f,g,h,i,j,k=n.css(a,"position"),l=n(a),m={};"static"===k&&(a.style.position="relative"),h=l.offset(),f=n.css(a,"top"),i=n.css(a,"left"),j=("absolute"===k||"fixed"===k)&&(f+i).indexOf("auto")>-1,j?(d=l.position(),g=d.top,e=d.left):(g=parseFloat(f)||0,e=parseFloat(i)||0),n.isFunction(b)&&(b=b.call(a,c,h)),null!=b.top&&(m.top=b.top-h.top+g),null!=b.left&&(m.left=b.left-h.left+e),"using"in b?b.using.call(a,m):l.css(m)}},n.fn.extend({offset:function(a){if(arguments.length)return void 0===a?this:this.each(function(b){n.offset.setOffset(this,a,b)});var b,c,d=this[0],e={top:0,left:0},f=d&&d.ownerDocument;if(f)return b=f.documentElement,n.contains(b,d)?(typeof d.getBoundingClientRect!==U&&(e=d.getBoundingClientRect()),c=Jb(f),{top:e.top+c.pageYOffset-b.clientTop,left:e.left+c.pageXOffset-b.clientLeft}):e},position:function(){if(this[0]){var a,b,c=this[0],d={top:0,left:0};return"fixed"===n.css(c,"position")?b=c.getBoundingClientRect():(a=this.offsetParent(),b=this.offset(),n.nodeName(a[0],"html")||(d=a.offset()),d.top+=n.css(a[0],"borderTopWidth",!0),d.left+=n.css(a[0],"borderLeftWidth",!0)),{top:b.top-d.top-n.css(c,"marginTop",!0),left:b.left-d.left-n.css(c,"marginLeft",!0)}}},offsetParent:function(){return this.map(function(){var a=this.offsetParent||Ib;while(a&&!n.nodeName(a,"html")&&"static"===n.css(a,"position"))a=a.offsetParent;return a||Ib})}}),n.each({scrollLeft:"pageXOffset",scrollTop:"pageYOffset"},function(b,c){var d="pageYOffset"===c;n.fn[b]=function(e){return J(this,function(b,e,f){var g=Jb(b);return void 0===f?g?g[c]:b[e]:void(g?g.scrollTo(d?a.pageXOffset:f,d?f:a.pageYOffset):b[e]=f)},b,e,arguments.length,null)}}),n.each(["top","left"],function(a,b){n.cssHooks[b]=ya(k.pixelPosition,function(a,c){return c?(c=xa(a,b),va.test(c)?n(a).position()[b]+"px":c):void 0})}),n.each({Height:"height",Width:"width"},function(a,b){n.each({padding:"inner"+a,content:b,"":"outer"+a},function(c,d){n.fn[d]=function(d,e){var f=arguments.length&&(c||"boolean"!=typeof d),g=c||(d===!0||e===!0?"margin":"border");return J(this,function(b,c,d){var e;return n.isWindow(b)?b.document.documentElement["client"+a]:9===b.nodeType?(e=b.documentElement,Math.max(b.body["scroll"+a],e["scroll"+a],b.body["offset"+a],e["offset"+a],e["client"+a])):void 0===d?n.css(b,c,g):n.style(b,c,d,g)},b,f?d:void 0,f,null)}})}),n.fn.size=function(){return this.length},n.fn.andSelf=n.fn.addBack,"function"==typeof define&&define.amd&&define("jquery",[],function(){return n});var Kb=a.jQuery,Lb=a.$;return n.noConflict=function(b){return a.$===n&&(a.$=Lb),b&&a.jQuery===n&&(a.jQuery=Kb),n},typeof b===U&&(a.jQuery=a.$=n),n});
//# sourceMappingURL=jquery.min.map
/*! Picturefill - v3.0.1 - 2015-09-30
 * http://scottjehl.github.io/picturefill
 * Copyright (c) 2015 https://github.com/scottjehl/picturefill/blob/master/Authors.txt; Licensed MIT
 */
!function(a){var b=navigator.userAgent;a.HTMLPictureElement&&/ecko/.test(b)&&b.match(/rv\:(\d+)/)&&RegExp.$1<41&&addEventListener("resize",function(){var b,c=document.createElement("source"),d=function(a){var b,d,e=a.parentNode;"PICTURE"===e.nodeName.toUpperCase()?(b=c.cloneNode(),e.insertBefore(b,e.firstElementChild),setTimeout(function(){e.removeChild(b)})):(!a._pfLastSize||a.offsetWidth>a._pfLastSize)&&(a._pfLastSize=a.offsetWidth,d=a.sizes,a.sizes+=",100vw",setTimeout(function(){a.sizes=d}))},e=function(){var a,b=document.querySelectorAll("picture > img, img[srcset][sizes]");for(a=0;a<b.length;a++)d(b[a])},f=function(){clearTimeout(b),b=setTimeout(e,99)},g=a.matchMedia&&matchMedia("(orientation: landscape)"),h=function(){f(),g&&g.addListener&&g.addListener(f)};return c.srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==",/^[c|i]|d$/.test(document.readyState||"")?h():document.addEventListener("DOMContentLoaded",h),f}())}(window),function(a,b,c){"use strict";function d(a){return" "===a||"	"===a||"\n"===a||"\f"===a||"\r"===a}function e(b,c){var d=new a.Image;return d.onerror=function(){z[b]=!1,aa()},d.onload=function(){z[b]=1===d.width,aa()},d.src=c,"pending"}function f(){L=!1,O=a.devicePixelRatio,M={},N={},s.DPR=O||1,P.width=Math.max(a.innerWidth||0,y.clientWidth),P.height=Math.max(a.innerHeight||0,y.clientHeight),P.vw=P.width/100,P.vh=P.height/100,r=[P.height,P.width,O].join("-"),P.em=s.getEmValue(),P.rem=P.em}function g(a,b,c,d){var e,f,g,h;return"saveData"===A.algorithm?a>2.7?h=c+1:(f=b-c,e=Math.pow(a-.6,1.5),g=f*e,d&&(g+=.1*e),h=a+g):h=c>1?Math.sqrt(a*b):a,h>c}function h(a){var b,c=s.getSet(a),d=!1;"pending"!==c&&(d=r,c&&(b=s.setRes(c),s.applySetCandidate(b,a))),a[s.ns].evaled=d}function i(a,b){return a.res-b.res}function j(a,b,c){var d;return!c&&b&&(c=a[s.ns].sets,c=c&&c[c.length-1]),d=k(b,c),d&&(b=s.makeUrl(b),a[s.ns].curSrc=b,a[s.ns].curCan=d,d.res||_(d,d.set.sizes)),d}function k(a,b){var c,d,e;if(a&&b)for(e=s.parseSet(b),a=s.makeUrl(a),c=0;c<e.length;c++)if(a===s.makeUrl(e[c].url)){d=e[c];break}return d}function l(a,b){var c,d,e,f,g=a.getElementsByTagName("source");for(c=0,d=g.length;d>c;c++)e=g[c],e[s.ns]=!0,f=e.getAttribute("srcset"),f&&b.push({srcset:f,media:e.getAttribute("media"),type:e.getAttribute("type"),sizes:e.getAttribute("sizes")})}function m(a,b){function c(b){var c,d=b.exec(a.substring(m));return d?(c=d[0],m+=c.length,c):void 0}function e(){var a,c,d,e,f,i,j,k,l,m=!1,o={};for(e=0;e<h.length;e++)f=h[e],i=f[f.length-1],j=f.substring(0,f.length-1),k=parseInt(j,10),l=parseFloat(j),W.test(j)&&"w"===i?((a||c)&&(m=!0),0===k?m=!0:a=k):X.test(j)&&"x"===i?((a||c||d)&&(m=!0),0>l?m=!0:c=l):W.test(j)&&"h"===i?((d||c)&&(m=!0),0===k?m=!0:d=k):m=!0;m||(o.url=g,a&&(o.w=a),c&&(o.d=c),d&&(o.h=d),d||c||a||(o.d=1),1===o.d&&(b.has1x=!0),o.set=b,n.push(o))}function f(){for(c(S),i="",j="in descriptor";;){if(k=a.charAt(m),"in descriptor"===j)if(d(k))i&&(h.push(i),i="",j="after descriptor");else{if(","===k)return m+=1,i&&h.push(i),void e();if("("===k)i+=k,j="in parens";else{if(""===k)return i&&h.push(i),void e();i+=k}}else if("in parens"===j)if(")"===k)i+=k,j="in descriptor";else{if(""===k)return h.push(i),void e();i+=k}else if("after descriptor"===j)if(d(k));else{if(""===k)return void e();j="in descriptor",m-=1}m+=1}}for(var g,h,i,j,k,l=a.length,m=0,n=[];;){if(c(T),m>=l)return n;g=c(U),h=[],","===g.slice(-1)?(g=g.replace(V,""),e()):f()}}function n(a){function b(a){function b(){f&&(g.push(f),f="")}function c(){g[0]&&(h.push(g),g=[])}for(var e,f="",g=[],h=[],i=0,j=0,k=!1;;){if(e=a.charAt(j),""===e)return b(),c(),h;if(k){if("*"===e&&"/"===a[j+1]){k=!1,j+=2,b();continue}j+=1}else{if(d(e)){if(a.charAt(j-1)&&d(a.charAt(j-1))||!f){j+=1;continue}if(0===i){b(),j+=1;continue}e=" "}else if("("===e)i+=1;else if(")"===e)i-=1;else{if(","===e){b(),c(),j+=1;continue}if("/"===e&&"*"===a.charAt(j+1)){k=!0,j+=2;continue}}f+=e,j+=1}}}function c(a){return k.test(a)&&parseFloat(a)>=0?!0:l.test(a)?!0:"0"===a||"-0"===a||"+0"===a?!0:!1}var e,f,g,h,i,j,k=/^(?:[+-]?[0-9]+|[0-9]*\.[0-9]+)(?:[eE][+-]?[0-9]+)?(?:ch|cm|em|ex|in|mm|pc|pt|px|rem|vh|vmin|vmax|vw)$/i,l=/^calc\((?:[0-9a-z \.\+\-\*\/\(\)]+)\)$/i;for(f=b(a),g=f.length,e=0;g>e;e++)if(h=f[e],i=h[h.length-1],c(i)){if(j=i,h.pop(),0===h.length)return j;if(h=h.join(" "),s.matchesMedia(h))return j}return"100vw"}b.createElement("picture");var o,p,q,r,s={},t=function(){},u=b.createElement("img"),v=u.getAttribute,w=u.setAttribute,x=u.removeAttribute,y=b.documentElement,z={},A={algorithm:""},B="data-pfsrc",C=B+"set",D=navigator.userAgent,E=/rident/.test(D)||/ecko/.test(D)&&D.match(/rv\:(\d+)/)&&RegExp.$1>35,F="currentSrc",G=/\s+\+?\d+(e\d+)?w/,H=/(\([^)]+\))?\s*(.+)/,I=a.picturefillCFG,J="position:absolute;left:0;visibility:hidden;display:block;padding:0;border:none;font-size:1em;width:1em;overflow:hidden;clip:rect(0px, 0px, 0px, 0px)",K="font-size:100%!important;",L=!0,M={},N={},O=a.devicePixelRatio,P={px:1,"in":96},Q=b.createElement("a"),R=!1,S=/^[ \t\n\r\u000c]+/,T=/^[, \t\n\r\u000c]+/,U=/^[^ \t\n\r\u000c]+/,V=/[,]+$/,W=/^\d+$/,X=/^-?(?:[0-9]+|[0-9]*\.[0-9]+)(?:[eE][+-]?[0-9]+)?$/,Y=function(a,b,c,d){a.addEventListener?a.addEventListener(b,c,d||!1):a.attachEvent&&a.attachEvent("on"+b,c)},Z=function(a){var b={};return function(c){return c in b||(b[c]=a(c)),b[c]}},$=function(){var a=/^([\d\.]+)(em|vw|px)$/,b=function(){for(var a=arguments,b=0,c=a[0];++b in a;)c=c.replace(a[b],a[++b]);return c},c=Z(function(a){return"return "+b((a||"").toLowerCase(),/\band\b/g,"&&",/,/g,"||",/min-([a-z-\s]+):/g,"e.$1>=",/max-([a-z-\s]+):/g,"e.$1<=",/calc([^)]+)/g,"($1)",/(\d+[\.]*[\d]*)([a-z]+)/g,"($1 * e.$2)",/^(?!(e.[a-z]|[0-9\.&=|><\+\-\*\(\)\/])).*/gi,"")+";"});return function(b,d){var e;if(!(b in M))if(M[b]=!1,d&&(e=b.match(a)))M[b]=e[1]*P[e[2]];else try{M[b]=new Function("e",c(b))(P)}catch(f){}return M[b]}}(),_=function(a,b){return a.w?(a.cWidth=s.calcListLength(b||"100vw"),a.res=a.w/a.cWidth):a.res=a.d,a},aa=function(a){var c,d,e,f=a||{};if(f.elements&&1===f.elements.nodeType&&("IMG"===f.elements.nodeName.toUpperCase()?f.elements=[f.elements]:(f.context=f.elements,f.elements=null)),c=f.elements||s.qsa(f.context||b,f.reevaluate||f.reselect?s.sel:s.selShort),e=c.length){for(s.setupRun(f),R=!0,d=0;e>d;d++)s.fillImg(c[d],f);s.teardownRun(f)}};o=a.console&&console.warn?function(a){console.warn(a)}:t,F in u||(F="src"),z["image/jpeg"]=!0,z["image/gif"]=!0,z["image/png"]=!0,z["image/svg+xml"]=b.implementation.hasFeature("http://wwwindow.w3.org/TR/SVG11/feature#Image","1.1"),s.ns=("pf"+(new Date).getTime()).substr(0,9),s.supSrcset="srcset"in u,s.supSizes="sizes"in u,s.supPicture=!!a.HTMLPictureElement,s.supSrcset&&s.supPicture&&!s.supSizes&&!function(a){u.srcset="data:,a",a.src="data:,a",s.supSrcset=u.complete===a.complete,s.supPicture=s.supSrcset&&s.supPicture}(b.createElement("img")),s.selShort="picture>img,img[srcset]",s.sel=s.selShort,s.cfg=A,s.supSrcset&&(s.sel+=",img["+C+"]"),s.DPR=O||1,s.u=P,s.types=z,q=s.supSrcset&&!s.supSizes,s.setSize=t,s.makeUrl=Z(function(a){return Q.href=a,Q.href}),s.qsa=function(a,b){return a.querySelectorAll(b)},s.matchesMedia=function(){return a.matchMedia&&(matchMedia("(min-width: 0.1em)")||{}).matches?s.matchesMedia=function(a){return!a||matchMedia(a).matches}:s.matchesMedia=s.mMQ,s.matchesMedia.apply(this,arguments)},s.mMQ=function(a){return a?$(a):!0},s.calcLength=function(a){var b=$(a,!0)||!1;return 0>b&&(b=!1),b},s.supportsType=function(a){return a?z[a]:!0},s.parseSize=Z(function(a){var b=(a||"").match(H);return{media:b&&b[1],length:b&&b[2]}}),s.parseSet=function(a){return a.cands||(a.cands=m(a.srcset,a)),a.cands},s.getEmValue=function(){var a;if(!p&&(a=b.body)){var c=b.createElement("div"),d=y.style.cssText,e=a.style.cssText;c.style.cssText=J,y.style.cssText=K,a.style.cssText=K,a.appendChild(c),p=c.offsetWidth,a.removeChild(c),p=parseFloat(p,10),y.style.cssText=d,a.style.cssText=e}return p||16},s.calcListLength=function(a){if(!(a in N)||A.uT){var b=s.calcLength(n(a));N[a]=b?b:P.width}return N[a]},s.setRes=function(a){var b;if(a){b=s.parseSet(a);for(var c=0,d=b.length;d>c;c++)_(b[c],a.sizes)}return b},s.setRes.res=_,s.applySetCandidate=function(a,b){if(a.length){var c,d,e,f,h,k,l,m,n,o=b[s.ns],p=s.DPR;if(k=o.curSrc||b[F],l=o.curCan||j(b,k,a[0].set),l&&l.set===a[0].set&&(n=E&&!b.complete&&l.res-.1>p,n||(l.cached=!0,l.res>=p&&(h=l))),!h)for(a.sort(i),f=a.length,h=a[f-1],d=0;f>d;d++)if(c=a[d],c.res>=p){e=d-1,h=a[e]&&(n||k!==s.makeUrl(c.url))&&g(a[e].res,c.res,p,a[e].cached)?a[e]:c;break}h&&(m=s.makeUrl(h.url),o.curSrc=m,o.curCan=h,m!==k&&s.setSrc(b,h),s.setSize(b))}},s.setSrc=function(a,b){var c;a.src=b.url,"image/svg+xml"===b.set.type&&(c=a.style.width,a.style.width=a.offsetWidth+1+"px",a.offsetWidth+1&&(a.style.width=c))},s.getSet=function(a){var b,c,d,e=!1,f=a[s.ns].sets;for(b=0;b<f.length&&!e;b++)if(c=f[b],c.srcset&&s.matchesMedia(c.media)&&(d=s.supportsType(c.type))){"pending"===d&&(c=d),e=c;break}return e},s.parseSets=function(a,b,d){var e,f,g,h,i=b&&"PICTURE"===b.nodeName.toUpperCase(),j=a[s.ns];(j.src===c||d.src)&&(j.src=v.call(a,"src"),j.src?w.call(a,B,j.src):x.call(a,B)),(j.srcset===c||d.srcset||!s.supSrcset||a.srcset)&&(e=v.call(a,"srcset"),j.srcset=e,h=!0),j.sets=[],i&&(j.pic=!0,l(b,j.sets)),j.srcset?(f={srcset:j.srcset,sizes:v.call(a,"sizes")},j.sets.push(f),g=(q||j.src)&&G.test(j.srcset||""),g||!j.src||k(j.src,f)||f.has1x||(f.srcset+=", "+j.src,f.cands.push({url:j.src,d:1,set:f}))):j.src&&j.sets.push({srcset:j.src,sizes:null}),j.curCan=null,j.curSrc=c,j.supported=!(i||f&&!s.supSrcset||g),h&&s.supSrcset&&!j.supported&&(e?(w.call(a,C,e),a.srcset=""):x.call(a,C)),j.supported&&!j.srcset&&(!j.src&&a.src||a.src!==s.makeUrl(j.src))&&(null===j.src?a.removeAttribute("src"):a.src=j.src),j.parsed=!0},s.fillImg=function(a,b){var c,d=b.reselect||b.reevaluate;a[s.ns]||(a[s.ns]={}),c=a[s.ns],(d||c.evaled!==r)&&((!c.parsed||b.reevaluate)&&s.parseSets(a,a.parentNode,b),c.supported?c.evaled=r:h(a))},s.setupRun=function(){(!R||L||O!==a.devicePixelRatio)&&f()},s.supPicture?(aa=t,s.fillImg=t):!function(){var c,d=a.attachEvent?/d$|^c/:/d$|^c|^i/,e=function(){var a=b.readyState||"";f=setTimeout(e,"loading"===a?200:999),b.body&&(s.fillImgs(),c=c||d.test(a),c&&clearTimeout(f))},f=setTimeout(e,b.body?9:99),g=function(a,b){var c,d,e=function(){var f=new Date-d;b>f?c=setTimeout(e,b-f):(c=null,a())};return function(){d=new Date,c||(c=setTimeout(e,b))}},h=y.clientHeight,i=function(){L=Math.max(a.innerWidth||0,y.clientWidth)!==P.width||y.clientHeight!==h,h=y.clientHeight,L&&s.fillImgs()};Y(a,"resize",g(i,99)),Y(b,"readystatechange",e)}(),s.picturefill=aa,s.fillImgs=aa,s.teardownRun=t,aa._=s,a.picturefillCFG={pf:s,push:function(a){var b=a.shift();"function"==typeof s[b]?s[b].apply(s,a):(A[b]=a[0],R&&s.fillImgs({reselect:!0}))}};for(;I&&I.length;)a.picturefillCFG.push(I.shift());a.picturefill=aa,"object"==typeof module&&"object"==typeof module.exports?module.exports=aa:"function"==typeof define&&define.amd&&define("picturefill",function(){return aa}),s.supPicture||(z["image/webp"]=e("image/webp","data:image/webp;base64,UklGRkoAAABXRUJQVlA4WAoAAAAQAAAAAAAAAAAAQUxQSAwAAAABBxAR/Q9ERP8DAABWUDggGAAAADABAJ0BKgEAAQADADQlpAADcAD++/1QAA=="))}(window,document);
/*!
 * jQuery Transit - CSS3 transitions and transformations
 * (c) 2011-2014 Rico Sta. Cruz
 * MIT Licensed.
 *
 * http://ricostacruz.com/jquery.transit
 * http://github.com/rstacruz/jquery.transit
 */

/* jshint expr: true */

;(function (root, factory) {

  if (typeof define === 'function' && define.amd) {
    define(['jquery'], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(require('jquery'));
  } else {
    factory(root.jQuery);
  }

}(this, function($) {

  $.transit = {
    version: "0.9.12",

    // Map of $.css() keys to values for 'transitionProperty'.
    // See https://developer.mozilla.org/en/CSS/CSS_transitions#Properties_that_can_be_animated
    propertyMap: {
      marginLeft    : 'margin',
      marginRight   : 'margin',
      marginBottom  : 'margin',
      marginTop     : 'margin',
      paddingLeft   : 'padding',
      paddingRight  : 'padding',
      paddingBottom : 'padding',
      paddingTop    : 'padding'
    },

    // Will simply transition "instantly" if false
    enabled: true,

    // Set this to false if you don't want to use the transition end property.
    useTransitionEnd: false
  };

  var div = document.createElement('div');
  var support = {};

  // Helper function to get the proper vendor property name.
  // (`transition` => `WebkitTransition`)
  function getVendorPropertyName(prop) {
    // Handle unprefixed versions (FF16+, for example)
    if (prop in div.style) return prop;

    var prefixes = ['Moz', 'Webkit', 'O', 'ms'];
    var prop_ = prop.charAt(0).toUpperCase() + prop.substr(1);

    for (var i=0; i<prefixes.length; ++i) {
      var vendorProp = prefixes[i] + prop_;
      if (vendorProp in div.style) { return vendorProp; }
    }
  }

  // Helper function to check if transform3D is supported.
  // Should return true for Webkits and Firefox 10+.
  function checkTransform3dSupport() {
    div.style[support.transform] = '';
    div.style[support.transform] = 'rotateY(90deg)';
    return div.style[support.transform] !== '';
  }

  var isChrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;

  // Check for the browser's transitions support.
  support.transition      = getVendorPropertyName('transition');
  support.transitionDelay = getVendorPropertyName('transitionDelay');
  support.transform       = getVendorPropertyName('transform');
  support.transformOrigin = getVendorPropertyName('transformOrigin');
  support.filter          = getVendorPropertyName('Filter');
  support.transform3d     = checkTransform3dSupport();

  var eventNames = {
    'transition':       'transitionend',
    'MozTransition':    'transitionend',
    'OTransition':      'oTransitionEnd',
    'WebkitTransition': 'webkitTransitionEnd',
    'msTransition':     'MSTransitionEnd'
  };

  // Detect the 'transitionend' event needed.
  var transitionEnd = support.transitionEnd = eventNames[support.transition] || null;

  // Populate jQuery's `$.support` with the vendor prefixes we know.
  // As per [jQuery's cssHooks documentation](http://api.jquery.com/jQuery.cssHooks/),
  // we set $.support.transition to a string of the actual property name used.
  for (var key in support) {
    if (support.hasOwnProperty(key) && typeof $.support[key] === 'undefined') {
      $.support[key] = support[key];
    }
  }

  // Avoid memory leak in IE.
  div = null;

  // ## $.cssEase
  // List of easing aliases that you can use with `$.fn.transition`.
  $.cssEase = {
    '_default':       'ease',
    'in':             'ease-in',
    'out':            'ease-out',
    'in-out':         'ease-in-out',
    'snap':           'cubic-bezier(0,1,.5,1)',
    // Penner equations
    'easeInCubic':    'cubic-bezier(.550,.055,.675,.190)',
    'easeOutCubic':   'cubic-bezier(.215,.61,.355,1)',
    'easeInOutCubic': 'cubic-bezier(.645,.045,.355,1)',
    'easeInCirc':     'cubic-bezier(.6,.04,.98,.335)',
    'easeOutCirc':    'cubic-bezier(.075,.82,.165,1)',
    'easeInOutCirc':  'cubic-bezier(.785,.135,.15,.86)',
    'easeInExpo':     'cubic-bezier(.95,.05,.795,.035)',
    'easeOutExpo':    'cubic-bezier(.19,1,.22,1)',
    'easeInOutExpo':  'cubic-bezier(1,0,0,1)',
    'easeInQuad':     'cubic-bezier(.55,.085,.68,.53)',
    'easeOutQuad':    'cubic-bezier(.25,.46,.45,.94)',
    'easeInOutQuad':  'cubic-bezier(.455,.03,.515,.955)',
    'easeInQuart':    'cubic-bezier(.895,.03,.685,.22)',
    'easeOutQuart':   'cubic-bezier(.165,.84,.44,1)',
    'easeInOutQuart': 'cubic-bezier(.77,0,.175,1)',
    'easeInQuint':    'cubic-bezier(.755,.05,.855,.06)',
    'easeOutQuint':   'cubic-bezier(.23,1,.32,1)',
    'easeInOutQuint': 'cubic-bezier(.86,0,.07,1)',
    'easeInSine':     'cubic-bezier(.47,0,.745,.715)',
    'easeOutSine':    'cubic-bezier(.39,.575,.565,1)',
    'easeInOutSine':  'cubic-bezier(.445,.05,.55,.95)',
    'easeInBack':     'cubic-bezier(.6,-.28,.735,.045)',
    'easeOutBack':    'cubic-bezier(.175, .885,.32,1.275)',
    'easeInOutBack':  'cubic-bezier(.68,-.55,.265,1.55)'
  };

  // ## 'transform' CSS hook
  // Allows you to use the `transform` property in CSS.
  //
  //     $("#hello").css({ transform: "rotate(90deg)" });
  //
  //     $("#hello").css('transform');
  //     //=> { rotate: '90deg' }
  //
  $.cssHooks['transit:transform'] = {
    // The getter returns a `Transform` object.
    get: function(elem) {
      return $(elem).data('transform') || new Transform();
    },

    // The setter accepts a `Transform` object or a string.
    set: function(elem, v) {
      var value = v;

      if (!(value instanceof Transform)) {
        value = new Transform(value);
      }

      // We've seen the 3D version of Scale() not work in Chrome when the
      // element being scaled extends outside of the viewport.  Thus, we're
      // forcing Chrome to not use the 3d transforms as well.  Not sure if
      // translate is affectede, but not risking it.  Detection code from
      // http://davidwalsh.name/detecting-google-chrome-javascript
      if (support.transform === 'WebkitTransform' && !isChrome) {
        elem.style[support.transform] = value.toString(true);
      } else {
        elem.style[support.transform] = value.toString();
      }

      $(elem).data('transform', value);
    }
  };

  // Add a CSS hook for `.css({ transform: '...' })`.
  // In jQuery 1.8+, this will intentionally override the default `transform`
  // CSS hook so it'll play well with Transit. (see issue #62)
  $.cssHooks.transform = {
    set: $.cssHooks['transit:transform'].set
  };

  // ## 'filter' CSS hook
  // Allows you to use the `filter` property in CSS.
  //
  //     $("#hello").css({ filter: 'blur(10px)' });
  //
  $.cssHooks.filter = {
    get: function(elem) {
      return elem.style[support.filter];
    },
    set: function(elem, value) {
      elem.style[support.filter] = value;
    }
  };

  // jQuery 1.8+ supports prefix-free transitions, so these polyfills will not
  // be necessary.
  if ($.fn.jquery < "1.8") {
    // ## 'transformOrigin' CSS hook
    // Allows the use for `transformOrigin` to define where scaling and rotation
    // is pivoted.
    //
    //     $("#hello").css({ transformOrigin: '0 0' });
    //
    $.cssHooks.transformOrigin = {
      get: function(elem) {
        return elem.style[support.transformOrigin];
      },
      set: function(elem, value) {
        elem.style[support.transformOrigin] = value;
      }
    };

    // ## 'transition' CSS hook
    // Allows you to use the `transition` property in CSS.
    //
    //     $("#hello").css({ transition: 'all 0 ease 0' });
    //
    $.cssHooks.transition = {
      get: function(elem) {
        return elem.style[support.transition];
      },
      set: function(elem, value) {
        elem.style[support.transition] = value;
      }
    };
  }

  // ## Other CSS hooks
  // Allows you to rotate, scale and translate.
  registerCssHook('scale');
  registerCssHook('scaleX');
  registerCssHook('scaleY');
  registerCssHook('translate');
  registerCssHook('rotate');
  registerCssHook('rotateX');
  registerCssHook('rotateY');
  registerCssHook('rotate3d');
  registerCssHook('perspective');
  registerCssHook('skewX');
  registerCssHook('skewY');
  registerCssHook('x', true);
  registerCssHook('y', true);

  // ## Transform class
  // This is the main class of a transformation property that powers
  // `$.fn.css({ transform: '...' })`.
  //
  // This is, in essence, a dictionary object with key/values as `-transform`
  // properties.
  //
  //     var t = new Transform("rotate(90) scale(4)");
  //
  //     t.rotate             //=> "90deg"
  //     t.scale              //=> "4,4"
  //
  // Setters are accounted for.
  //
  //     t.set('rotate', 4)
  //     t.rotate             //=> "4deg"
  //
  // Convert it to a CSS string using the `toString()` and `toString(true)` (for WebKit)
  // functions.
  //
  //     t.toString()         //=> "rotate(90deg) scale(4,4)"
  //     t.toString(true)     //=> "rotate(90deg) scale3d(4,4,0)" (WebKit version)
  //
  function Transform(str) {
    if (typeof str === 'string') { this.parse(str); }
    return this;
  }

  Transform.prototype = {
    // ### setFromString()
    // Sets a property from a string.
    //
    //     t.setFromString('scale', '2,4');
    //     // Same as set('scale', '2', '4');
    //
    setFromString: function(prop, val) {
      var args =
        (typeof val === 'string')  ? val.split(',') :
        (val.constructor === Array) ? val :
        [ val ];

      args.unshift(prop);

      Transform.prototype.set.apply(this, args);
    },

    // ### set()
    // Sets a property.
    //
    //     t.set('scale', 2, 4);
    //
    set: function(prop) {
      var args = Array.prototype.slice.apply(arguments, [1]);
      if (this.setter[prop]) {
        this.setter[prop].apply(this, args);
      } else {
        this[prop] = args.join(',');
      }
    },

    get: function(prop) {
      if (this.getter[prop]) {
        return this.getter[prop].apply(this);
      } else {
        return this[prop] || 0;
      }
    },

    setter: {
      // ### rotate
      //
      //     .css({ rotate: 30 })
      //     .css({ rotate: "30" })
      //     .css({ rotate: "30deg" })
      //     .css({ rotate: "30deg" })
      //
      rotate: function(theta) {
        this.rotate = unit(theta, 'deg');
      },

      rotateX: function(theta) {
        this.rotateX = unit(theta, 'deg');
      },

      rotateY: function(theta) {
        this.rotateY = unit(theta, 'deg');
      },

      // ### scale
      //
      //     .css({ scale: 9 })      //=> "scale(9,9)"
      //     .css({ scale: '3,2' })  //=> "scale(3,2)"
      //
      scale: function(x, y) {
        if (y === undefined) { y = x; }
        this.scale = x + "," + y;
      },

      // ### skewX + skewY
      skewX: function(x) {
        this.skewX = unit(x, 'deg');
      },

      skewY: function(y) {
        this.skewY = unit(y, 'deg');
      },

      // ### perspectvie
      perspective: function(dist) {
        this.perspective = unit(dist, 'px');
      },

      // ### x / y
      // Translations. Notice how this keeps the other value.
      //
      //     .css({ x: 4 })       //=> "translate(4px, 0)"
      //     .css({ y: 10 })      //=> "translate(4px, 10px)"
      //
      x: function(x) {
        this.set('translate', x, null);
      },

      y: function(y) {
        this.set('translate', null, y);
      },

      // ### translate
      // Notice how this keeps the other value.
      //
      //     .css({ translate: '2, 5' })    //=> "translate(2px, 5px)"
      //
      translate: function(x, y) {
        if (this._translateX === undefined) { this._translateX = 0; }
        if (this._translateY === undefined) { this._translateY = 0; }

        if (x !== null && x !== undefined) { this._translateX = unit(x, 'px'); }
        if (y !== null && y !== undefined) { this._translateY = unit(y, 'px'); }

        this.translate = this._translateX + "," + this._translateY;
      }
    },

    getter: {
      x: function() {
        return this._translateX || 0;
      },

      y: function() {
        return this._translateY || 0;
      },

      scale: function() {
        var s = (this.scale || "1,1").split(',');
        if (s[0]) { s[0] = parseFloat(s[0]); }
        if (s[1]) { s[1] = parseFloat(s[1]); }

        // "2.5,2.5" => 2.5
        // "2.5,1" => [2.5,1]
        return (s[0] === s[1]) ? s[0] : s;
      },

      rotate3d: function() {
        var s = (this.rotate3d || "0,0,0,0deg").split(',');
        for (var i=0; i<=3; ++i) {
          if (s[i]) { s[i] = parseFloat(s[i]); }
        }
        if (s[3]) { s[3] = unit(s[3], 'deg'); }

        return s;
      }
    },

    // ### parse()
    // Parses from a string. Called on constructor.
    parse: function(str) {
      var self = this;
      str.replace(/([a-zA-Z0-9]+)\((.*?)\)/g, function(x, prop, val) {
        self.setFromString(prop, val);
      });
    },

    // ### toString()
    // Converts to a `transition` CSS property string. If `use3d` is given,
    // it converts to a `-webkit-transition` CSS property string instead.
    toString: function(use3d) {
      var re = [];

      for (var i in this) {
        if (this.hasOwnProperty(i)) {
          // Don't use 3D transformations if the browser can't support it.
          if ((!support.transform3d) && (
            (i === 'rotateX') ||
            (i === 'rotateY') ||
            (i === 'perspective') ||
            (i === 'transformOrigin'))) { continue; }

          if (i[0] !== '_') {
            if (use3d && (i === 'scale')) {
              re.push(i + "3d(" + this[i] + ",1)");
            } else if (use3d && (i === 'translate')) {
              re.push(i + "3d(" + this[i] + ",0)");
            } else {
              re.push(i + "(" + this[i] + ")");
            }
          }
        }
      }

      return re.join(" ");
    }
  };

  function callOrQueue(self, queue, fn) {
    if (queue === true) {
      self.queue(fn);
    } else if (queue) {
      self.queue(queue, fn);
    } else {
      self.each(function () {
                fn.call(this);
            });
    }
  }

  // ### getProperties(dict)
  // Returns properties (for `transition-property`) for dictionary `props`. The
  // value of `props` is what you would expect in `$.css(...)`.
  function getProperties(props) {
    var re = [];

    $.each(props, function(key) {
      key = $.camelCase(key); // Convert "text-align" => "textAlign"
      key = $.transit.propertyMap[key] || $.cssProps[key] || key;
      key = uncamel(key); // Convert back to dasherized

      // Get vendor specify propertie
      if (support[key])
        key = uncamel(support[key]);

      if ($.inArray(key, re) === -1) { re.push(key); }
    });

    return re;
  }

  // ### getTransition()
  // Returns the transition string to be used for the `transition` CSS property.
  //
  // Example:
  //
  //     getTransition({ opacity: 1, rotate: 30 }, 500, 'ease');
  //     //=> 'opacity 500ms ease, -webkit-transform 500ms ease'
  //
  function getTransition(properties, duration, easing, delay) {
    // Get the CSS properties needed.
    var props = getProperties(properties);

    // Account for aliases (`in` => `ease-in`).
    if ($.cssEase[easing]) { easing = $.cssEase[easing]; }

    // Build the duration/easing/delay attributes for it.
    var attribs = '' + toMS(duration) + ' ' + easing;
    if (parseInt(delay, 10) > 0) { attribs += ' ' + toMS(delay); }

    // For more properties, add them this way:
    // "margin 200ms ease, padding 200ms ease, ..."
    var transitions = [];
    $.each(props, function(i, name) {
      transitions.push(name + ' ' + attribs);
    });

    return transitions.join(', ');
  }

  // ## $.fn.transition
  // Works like $.fn.animate(), but uses CSS transitions.
  //
  //     $("...").transition({ opacity: 0.1, scale: 0.3 });
  //
  //     // Specific duration
  //     $("...").transition({ opacity: 0.1, scale: 0.3 }, 500);
  //
  //     // With duration and easing
  //     $("...").transition({ opacity: 0.1, scale: 0.3 }, 500, 'in');
  //
  //     // With callback
  //     $("...").transition({ opacity: 0.1, scale: 0.3 }, function() { ... });
  //
  //     // With everything
  //     $("...").transition({ opacity: 0.1, scale: 0.3 }, 500, 'in', function() { ... });
  //
  //     // Alternate syntax
  //     $("...").transition({
  //       opacity: 0.1,
  //       duration: 200,
  //       delay: 40,
  //       easing: 'in',
  //       complete: function() { /* ... */ }
  //      });
  //
  $.fn.transition = $.fn.transit = function(properties, duration, easing, callback) {
    var self  = this;
    var delay = 0;
    var queue = true;

    var theseProperties = $.extend(true, {}, properties);

    // Account for `.transition(properties, callback)`.
    if (typeof duration === 'function') {
      callback = duration;
      duration = undefined;
    }

    // Account for `.transition(properties, options)`.
    if (typeof duration === 'object') {
      easing = duration.easing;
      delay = duration.delay || 0;
      queue = typeof duration.queue === "undefined" ? true : duration.queue;
      callback = duration.complete;
      duration = duration.duration;
    }

    // Account for `.transition(properties, duration, callback)`.
    if (typeof easing === 'function') {
      callback = easing;
      easing = undefined;
    }

    // Alternate syntax.
    if (typeof theseProperties.easing !== 'undefined') {
      easing = theseProperties.easing;
      delete theseProperties.easing;
    }

    if (typeof theseProperties.duration !== 'undefined') {
      duration = theseProperties.duration;
      delete theseProperties.duration;
    }

    if (typeof theseProperties.complete !== 'undefined') {
      callback = theseProperties.complete;
      delete theseProperties.complete;
    }

    if (typeof theseProperties.queue !== 'undefined') {
      queue = theseProperties.queue;
      delete theseProperties.queue;
    }

    if (typeof theseProperties.delay !== 'undefined') {
      delay = theseProperties.delay;
      delete theseProperties.delay;
    }

    // Set defaults. (`400` duration, `ease` easing)
    if (typeof duration === 'undefined') { duration = $.fx.speeds._default; }
    if (typeof easing === 'undefined')   { easing = $.cssEase._default; }

    duration = toMS(duration);

    // Build the `transition` property.
    var transitionValue = getTransition(theseProperties, duration, easing, delay);

    // Compute delay until callback.
    // If this becomes 0, don't bother setting the transition property.
    var work = $.transit.enabled && support.transition;
    var i = work ? (parseInt(duration, 10) + parseInt(delay, 10)) : 0;

    // If there's nothing to do...
    if (i === 0) {
      var fn = function(next) {
        self.css(theseProperties);
        if (callback) { callback.apply(self); }
        if (next) { next(); }
      };

      callOrQueue(self, queue, fn);
      return self;
    }

    // Save the old transitions of each element so we can restore it later.
    var oldTransitions = {};

    var run = function(nextCall) {
      var bound = false;

      // Prepare the callback.
      var cb = function() {
        if (bound) { self.unbind(transitionEnd, cb); }

        if (i > 0) {
          self.each(function() {
            this.style[support.transition] = (oldTransitions[this] || null);
          });
        }

        if (typeof callback === 'function') { callback.apply(self); }
        if (typeof nextCall === 'function') { nextCall(); }
      };

      if ((i > 0) && (transitionEnd) && ($.transit.useTransitionEnd)) {
        // Use the 'transitionend' event if it's available.
        bound = true;
        self.bind(transitionEnd, cb);
      } else {
        // Fallback to timers if the 'transitionend' event isn't supported.
        window.setTimeout(cb, i);
      }

      // Apply transitions.
      self.each(function() {
        if (i > 0) {
          this.style[support.transition] = transitionValue;
        }
        $(this).css(theseProperties);
      });
    };

    // Defer running. This allows the browser to paint any pending CSS it hasn't
    // painted yet before doing the transitions.
    var deferredRun = function(next) {
        this.offsetWidth; // force a repaint
        run(next);
    };

    // Use jQuery's fx queue.
    callOrQueue(self, queue, deferredRun);

    // Chainability.
    return this;
  };

  function registerCssHook(prop, isPixels) {
    // For certain properties, the 'px' should not be implied.
    if (!isPixels) { $.cssNumber[prop] = true; }

    $.transit.propertyMap[prop] = support.transform;

    $.cssHooks[prop] = {
      get: function(elem) {
        var t = $(elem).css('transit:transform');
        return t.get(prop);
      },

      set: function(elem, value) {
        var t = $(elem).css('transit:transform');
        t.setFromString(prop, value);

        $(elem).css({ 'transit:transform': t });
      }
    };

  }

  // ### uncamel(str)
  // Converts a camelcase string to a dasherized string.
  // (`marginLeft` => `margin-left`)
  function uncamel(str) {
    return str.replace(/([A-Z])/g, function(letter) { return '-' + letter.toLowerCase(); });
  }

  // ### unit(number, unit)
  // Ensures that number `number` has a unit. If no unit is found, assume the
  // default is `unit`.
  //
  //     unit(2, 'px')          //=> "2px"
  //     unit("30deg", 'rad')   //=> "30deg"
  //
  function unit(i, units) {
    if ((typeof i === "string") && (!i.match(/^[\-0-9\.]+$/))) {
      return i;
    } else {
      return "" + i + units;
    }
  }

  // ### toMS(duration)
  // Converts given `duration` to a millisecond string.
  //
  // toMS('fast') => $.fx.speeds[i] => "200ms"
  // toMS('normal') //=> $.fx.speeds._default => "400ms"
  // toMS(10) //=> '10ms'
  // toMS('100ms') //=> '100ms'  
  //
  function toMS(duration) {
    var i = duration;

    // Allow string durations like 'fast' and 'slow', without overriding numeric values.
    if (typeof i === 'string' && (!i.match(/^[\-0-9\.]+/))) { i = $.fx.speeds[i] || $.fx.speeds._default; }

    return unit(i, 'ms');
  }

  // Export some functions for testable-ness.
  $.transit.getTransitionValue = getTransition;

  return $;
}));

// jquery.event.move
//
// 1.3.6
//
// Stephen Band
//
// Triggers 'movestart', 'move' and 'moveend' events after
// mousemoves following a mousedown cross a distance threshold,
// similar to the native 'dragstart', 'drag' and 'dragend' events.
// Move events are throttled to animation frames. Move event objects
// have the properties:
//
// pageX:
// pageY:   Page coordinates of pointer.
// startX:
// startY:  Page coordinates of pointer at movestart.
// distX:
// distY:  Distance the pointer has moved since movestart.
// deltaX:
// deltaY:  Distance the finger has moved since last event.
// velocityX:
// velocityY:  Average velocity over last few events.


(function (module) {
	if (typeof define === 'function' && define.amd) {
		// AMD. Register as an anonymous module.
		define(['jquery'], module);
	} else {
		// Browser globals
		module(jQuery);
	}
})(function(jQuery, undefined){

	var // Number of pixels a pressed pointer travels before movestart
	    // event is fired.
	    threshold = 6,
	
	    add = jQuery.event.add,
	
	    remove = jQuery.event.remove,

	    // Just sugar, so we can have arguments in the same order as
	    // add and remove.
	    trigger = function(node, type, data) {
	    	jQuery.event.trigger(type, data, node);
	    },

	    // Shim for requestAnimationFrame, falling back to timer. See:
	    // see http://paulirish.com/2011/requestanimationframe-for-smart-animating/
	    requestFrame = (function(){
	    	return (
	    		window.requestAnimationFrame ||
	    		window.webkitRequestAnimationFrame ||
	    		window.mozRequestAnimationFrame ||
	    		window.oRequestAnimationFrame ||
	    		window.msRequestAnimationFrame ||
	    		function(fn, element){
	    			return window.setTimeout(function(){
	    				fn();
	    			}, 25);
	    		}
	    	);
	    })(),
	    
	    ignoreTags = {
	    	textarea: true,
	    	input: true,
	    	select: true,
	    	button: true
	    },
	    
	    mouseevents = {
	    	move: 'mousemove',
	    	cancel: 'mouseup dragstart',
	    	end: 'mouseup'
	    },
	    
	    touchevents = {
	    	move: 'touchmove',
	    	cancel: 'touchend',
	    	end: 'touchend'
	    };


	// Constructors
	
	function Timer(fn){
		var callback = fn,
		    active = false,
		    running = false;
		
		function trigger(time) {
			if (active){
				callback();
				requestFrame(trigger);
				running = true;
				active = false;
			}
			else {
				running = false;
			}
		}
		
		this.kick = function(fn) {
			active = true;
			if (!running) { trigger(); }
		};
		
		this.end = function(fn) {
			var cb = callback;
			
			if (!fn) { return; }
			
			// If the timer is not running, simply call the end callback.
			if (!running) {
				fn();
			}
			// If the timer is running, and has been kicked lately, then
			// queue up the current callback and the end callback, otherwise
			// just the end callback.
			else {
				callback = active ?
					function(){ cb(); fn(); } : 
					fn ;
				
				active = true;
			}
		};
	}


	// Functions
	
	function returnTrue() {
		return true;
	}
	
	function returnFalse() {
		return false;
	}
	
	function preventDefault(e) {
		e.preventDefault();
	}
	
	function preventIgnoreTags(e) {
		// Don't prevent interaction with form elements.
		if (ignoreTags[ e.target.tagName.toLowerCase() ]) { return; }
		
		e.preventDefault();
	}

	function isLeftButton(e) {
		// Ignore mousedowns on any button other than the left (or primary)
		// mouse button, or when a modifier key is pressed.
		return (e.which === 1 && !e.ctrlKey && !e.altKey);
	}

	function identifiedTouch(touchList, id) {
		var i, l;

		if (touchList.identifiedTouch) {
			return touchList.identifiedTouch(id);
		}
		
		// touchList.identifiedTouch() does not exist in
		// webkit yet… we must do the search ourselves...
		
		i = -1;
		l = touchList.length;
		
		while (++i < l) {
			if (touchList[i].identifier === id) {
				return touchList[i];
			}
		}
	}

	function changedTouch(e, event) {
		var touch = identifiedTouch(e.changedTouches, event.identifier);

		// This isn't the touch you're looking for.
		if (!touch) { return; }

		// Chrome Android (at least) includes touches that have not
		// changed in e.changedTouches. That's a bit annoying. Check
		// that this touch has changed.
		if (touch.pageX === event.pageX && touch.pageY === event.pageY) { return; }

		return touch;
	}


	// Handlers that decide when the first movestart is triggered
	
	function mousedown(e){
		var data;

		if (!isLeftButton(e)) { return; }

		data = {
			target: e.target,
			startX: e.pageX,
			startY: e.pageY,
			timeStamp: e.timeStamp
		};

		add(document, mouseevents.move, mousemove, data);
		add(document, mouseevents.cancel, mouseend, data);
	}

	function mousemove(e){
		var data = e.data;

		checkThreshold(e, data, e, removeMouse);
	}

	function mouseend(e) {
		removeMouse();
	}

	function removeMouse() {
		remove(document, mouseevents.move, mousemove);
		remove(document, mouseevents.cancel, mouseend);
	}

	function touchstart(e) {
		var touch, template;

		// Don't get in the way of interaction with form elements.
		if (ignoreTags[ e.target.tagName.toLowerCase() ]) { return; }

		touch = e.changedTouches[0];
		
		// iOS live updates the touch objects whereas Android gives us copies.
		// That means we can't trust the touchstart object to stay the same,
		// so we must copy the data. This object acts as a template for
		// movestart, move and moveend event objects.
		template = {
			target: touch.target,
			startX: touch.pageX,
			startY: touch.pageY,
			timeStamp: e.timeStamp,
			identifier: touch.identifier
		};

		// Use the touch identifier as a namespace, so that we can later
		// remove handlers pertaining only to this touch.
		add(document, touchevents.move + '.' + touch.identifier, touchmove, template);
		add(document, touchevents.cancel + '.' + touch.identifier, touchend, template);
	}

	function touchmove(e){
		var data = e.data,
		    touch = changedTouch(e, data);

		if (!touch) { return; }

		checkThreshold(e, data, touch, removeTouch);
	}

	function touchend(e) {
		var template = e.data,
		    touch = identifiedTouch(e.changedTouches, template.identifier);

		if (!touch) { return; }

		removeTouch(template.identifier);
	}

	function removeTouch(identifier) {
		remove(document, '.' + identifier, touchmove);
		remove(document, '.' + identifier, touchend);
	}


	// Logic for deciding when to trigger a movestart.

	function checkThreshold(e, template, touch, fn) {
		var distX = touch.pageX - template.startX,
		    distY = touch.pageY - template.startY;

		// Do nothing if the threshold has not been crossed.
		if ((distX * distX) + (distY * distY) < (threshold * threshold)) { return; }

		triggerStart(e, template, touch, distX, distY, fn);
	}

	function handled() {
		// this._handled should return false once, and after return true.
		this._handled = returnTrue;
		return false;
	}

	function flagAsHandled(e) {
		e._handled();
	}

	function triggerStart(e, template, touch, distX, distY, fn) {
		var node = template.target,
		    touches, time;

		touches = e.targetTouches;
		time = e.timeStamp - template.timeStamp;

		// Create a movestart object with some special properties that
		// are passed only to the movestart handlers.
		template.type = 'movestart';
		template.distX = distX;
		template.distY = distY;
		template.deltaX = distX;
		template.deltaY = distY;
		template.pageX = touch.pageX;
		template.pageY = touch.pageY;
		template.velocityX = distX / time;
		template.velocityY = distY / time;
		template.targetTouches = touches;
		template.finger = touches ?
			touches.length :
			1 ;

		// The _handled method is fired to tell the default movestart
		// handler that one of the move events is bound.
		template._handled = handled;
			
		// Pass the touchmove event so it can be prevented if or when
		// movestart is handled.
		template._preventTouchmoveDefault = function() {
			e.preventDefault();
		};

		// Trigger the movestart event.
		trigger(template.target, template);

		// Unbind handlers that tracked the touch or mouse up till now.
		fn(template.identifier);
	}


	// Handlers that control what happens following a movestart

	function activeMousemove(e) {
		var timer = e.data.timer;

		e.data.touch = e;
		e.data.timeStamp = e.timeStamp;
		timer.kick();
	}

	function activeMouseend(e) {
		var event = e.data.event,
		    timer = e.data.timer;
		
		removeActiveMouse();

		endEvent(event, timer, function() {
			// Unbind the click suppressor, waiting until after mouseup
			// has been handled.
			setTimeout(function(){
				remove(event.target, 'click', returnFalse);
			}, 0);
		});
	}

	function removeActiveMouse(event) {
		remove(document, mouseevents.move, activeMousemove);
		remove(document, mouseevents.end, activeMouseend);
	}

	function activeTouchmove(e) {
		var event = e.data.event,
		    timer = e.data.timer,
		    touch = changedTouch(e, event);

		if (!touch) { return; }

		// Stop the interface from gesturing
		e.preventDefault();

		event.targetTouches = e.targetTouches;
		e.data.touch = touch;
		e.data.timeStamp = e.timeStamp;
		timer.kick();
	}

	function activeTouchend(e) {
		var event = e.data.event,
		    timer = e.data.timer,
		    touch = identifiedTouch(e.changedTouches, event.identifier);

		// This isn't the touch you're looking for.
		if (!touch) { return; }

		removeActiveTouch(event);
		endEvent(event, timer);
	}

	function removeActiveTouch(event) {
		remove(document, '.' + event.identifier, activeTouchmove);
		remove(document, '.' + event.identifier, activeTouchend);
	}


	// Logic for triggering move and moveend events

	function updateEvent(event, touch, timeStamp, timer) {
		var time = timeStamp - event.timeStamp;

		event.type = 'move';
		event.distX =  touch.pageX - event.startX;
		event.distY =  touch.pageY - event.startY;
		event.deltaX = touch.pageX - event.pageX;
		event.deltaY = touch.pageY - event.pageY;
		
		// Average the velocity of the last few events using a decay
		// curve to even out spurious jumps in values.
		event.velocityX = 0.3 * event.velocityX + 0.7 * event.deltaX / time;
		event.velocityY = 0.3 * event.velocityY + 0.7 * event.deltaY / time;
		event.pageX =  touch.pageX;
		event.pageY =  touch.pageY;
	}

	function endEvent(event, timer, fn) {
		timer.end(function(){
			event.type = 'moveend';

			trigger(event.target, event);
			
			return fn && fn();
		});
	}


	// jQuery special event definition

	function setup(data, namespaces, eventHandle) {
		// Stop the node from being dragged
		//add(this, 'dragstart.move drag.move', preventDefault);
		
		// Prevent text selection and touch interface scrolling
		//add(this, 'mousedown.move', preventIgnoreTags);
		
		// Tell movestart default handler that we've handled this
		add(this, 'movestart.move', flagAsHandled);

		// Don't bind to the DOM. For speed.
		return true;
	}
	
	function teardown(namespaces) {
		remove(this, 'dragstart drag', preventDefault);
		remove(this, 'mousedown touchstart', preventIgnoreTags);
		remove(this, 'movestart', flagAsHandled);
		
		// Don't bind to the DOM. For speed.
		return true;
	}
	
	function addMethod(handleObj) {
		// We're not interested in preventing defaults for handlers that
		// come from internal move or moveend bindings
		if (handleObj.namespace === "move" || handleObj.namespace === "moveend") {
			return;
		}
		
		// Stop the node from being dragged
		add(this, 'dragstart.' + handleObj.guid + ' drag.' + handleObj.guid, preventDefault, undefined, handleObj.selector);
		
		// Prevent text selection and touch interface scrolling
		add(this, 'mousedown.' + handleObj.guid, preventIgnoreTags, undefined, handleObj.selector);
	}
	
	function removeMethod(handleObj) {
		if (handleObj.namespace === "move" || handleObj.namespace === "moveend") {
			return;
		}
		
		remove(this, 'dragstart.' + handleObj.guid + ' drag.' + handleObj.guid);
		remove(this, 'mousedown.' + handleObj.guid);
	}
	
	jQuery.event.special.movestart = {
		setup: setup,
		teardown: teardown,
		add: addMethod,
		remove: removeMethod,

		_default: function(e) {
			var event, data;
			
			// If no move events were bound to any ancestors of this
			// target, high tail it out of here.
			if (!e._handled()) { return; }

			function update(time) {
				updateEvent(event, data.touch, data.timeStamp);
				trigger(e.target, event);
			}

			event = {
				target: e.target,
				startX: e.startX,
				startY: e.startY,
				pageX: e.pageX,
				pageY: e.pageY,
				distX: e.distX,
				distY: e.distY,
				deltaX: e.deltaX,
				deltaY: e.deltaY,
				velocityX: e.velocityX,
				velocityY: e.velocityY,
				timeStamp: e.timeStamp,
				identifier: e.identifier,
				targetTouches: e.targetTouches,
				finger: e.finger
			};

			data = {
				event: event,
				timer: new Timer(update),
				touch: undefined,
				timeStamp: undefined
			};
			
			if (e.identifier === undefined) {
				// We're dealing with a mouse
				// Stop clicks from propagating during a move
				add(e.target, 'click', returnFalse);
				add(document, mouseevents.move, activeMousemove, data);
				add(document, mouseevents.end, activeMouseend, data);
			}
			else {
				// We're dealing with a touch. Stop touchmove doing
				// anything defaulty.
				e._preventTouchmoveDefault();
				add(document, touchevents.move + '.' + e.identifier, activeTouchmove, data);
				add(document, touchevents.end + '.' + e.identifier, activeTouchend, data);
			}
		}
	};

	jQuery.event.special.move = {
		setup: function() {
			// Bind a noop to movestart. Why? It's the movestart
			// setup that decides whether other move events are fired.
			add(this, 'movestart.move', jQuery.noop);
		},
		
		teardown: function() {
			remove(this, 'movestart.move', jQuery.noop);
		}
	};
	
	jQuery.event.special.moveend = {
		setup: function() {
			// Bind a noop to movestart. Why? It's the movestart
			// setup that decides whether other move events are fired.
			add(this, 'movestart.moveend', jQuery.noop);
		},
		
		teardown: function() {
			remove(this, 'movestart.moveend', jQuery.noop);
		}
	};

	add(document, 'mousedown.move', mousedown);
	add(document, 'touchstart.move', touchstart);

	// Make jQuery copy touch event properties over to the jQuery event
	// object, if they are not already listed. But only do the ones we
	// really need. IE7/8 do not have Array#indexOf(), but nor do they
	// have touch events, so let's assume we can ignore them.
	if (typeof Array.prototype.indexOf === 'function') {
		(function(jQuery, undefined){
			var props = ["changedTouches", "targetTouches"],
			    l = props.length;
			
			while (l--) {
				if (jQuery.event.props.indexOf(props[l]) === -1) {
					jQuery.event.props.push(props[l]);
				}
			}
		})(jQuery);
	};
});

// jQuery.event.swipe
// 0.5
// Stephen Band

// Dependencies
// jQuery.event.move 1.2

// One of swipeleft, swiperight, swipeup or swipedown is triggered on
// moveend, when the move has covered a threshold ratio of the dimension
// of the target node, or has gone really fast. Threshold and velocity
// sensitivity changed with:
//
// jQuery.event.special.swipe.settings.threshold
// jQuery.event.special.swipe.settings.sensitivity

(function (thisModule) {
	if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery'], thisModule);
    } else if ((typeof module !== "undefined" && module !== null) && module.exports) {
        module.exports = thisModule;
	} else {
		// Browser globals
        thisModule(jQuery);
	}
})(function(jQuery, undefined){
	var add = jQuery.event.add,
	   
	    remove = jQuery.event.remove,

	    // Just sugar, so we can have arguments in the same order as
	    // add and remove.
	    trigger = function(node, type, data) {
	    	jQuery.event.trigger(type, data, node);
	    },

	    settings = {
	    	// Ratio of distance over target finger must travel to be
	    	// considered a swipe.
	    	threshold: 0.4,
	    	// Faster fingers can travel shorter distances to be considered
	    	// swipes. 'sensitivity' controls how much. Bigger is shorter.
	    	sensitivity: 6
	    };

	function moveend(e) {
		var w, h, event;

		w = e.currentTarget.offsetWidth;
		h = e.currentTarget.offsetHeight;

		// Copy over some useful properties from the move event
		event = {
			distX: e.distX,
			distY: e.distY,
			velocityX: e.velocityX,
			velocityY: e.velocityY,
			finger: e.finger
		};

		// Find out which of the four directions was swiped
		if (e.distX > e.distY) {
			if (e.distX > -e.distY) {
				if (e.distX/w > settings.threshold || e.velocityX * e.distX/w * settings.sensitivity > 1) {
					event.type = 'swiperight';
					trigger(e.currentTarget, event);
				}
			}
			else {
				if (-e.distY/h > settings.threshold || e.velocityY * e.distY/w * settings.sensitivity > 1) {
					event.type = 'swipeup';
					trigger(e.currentTarget, event);
				}
			}
		}
		else {
			if (e.distX > -e.distY) {
				if (e.distY/h > settings.threshold || e.velocityY * e.distY/w * settings.sensitivity > 1) {
					event.type = 'swipedown';
					trigger(e.currentTarget, event);
				}
			}
			else {
				if (-e.distX/w > settings.threshold || e.velocityX * e.distX/w * settings.sensitivity > 1) {
					event.type = 'swipeleft';
					trigger(e.currentTarget, event);
				}
			}
		}
	}

	function getData(node) {
		var data = jQuery.data(node, 'event_swipe');
		
		if (!data) {
			data = { count: 0 };
			jQuery.data(node, 'event_swipe', data);
		}
		
		return data;
	}

	jQuery.event.special.swipe =
	jQuery.event.special.swipeleft =
	jQuery.event.special.swiperight =
	jQuery.event.special.swipeup =
	jQuery.event.special.swipedown = {
		setup: function( data, namespaces, eventHandle ) {
			var data = getData(this);

			// If another swipe event is already setup, don't setup again.
			if (data.count++ > 0) { return; }

			add(this, 'moveend', moveend);

			return true;
		},

		teardown: function() {
			var data = getData(this);

			// If another swipe event is still setup, don't teardown.
			if (--data.count > 0) { return; }

			remove(this, 'moveend', moveend);

			return true;
		},

		settings: settings
	};
});

/*
 selectivizr v1.0.2 - (c) Keith Clark, freely distributable under the terms
 of the MIT license.

 selectivizr.com
 */
/*

 Notes about this source
 -----------------------

 * The #DEBUG_START and #DEBUG_END comments are used to mark blocks of code
 that will be removed prior to building a final release version (using a
 pre-compression script)


 References:
 -----------

 * CSS Syntax          : http://www.w3.org/TR/2003/WD-css3-syntax-20030813/#style
 * Selectors           : http://www.w3.org/TR/css3-selectors/#selectors
 * IE Compatability    : http://msdn.microsoft.com/en-us/library/cc351024(VS.85).aspx
 * W3C Selector Tests  : http://www.w3.org/Style/CSS/Test/CSS3/Selectors/current/html/tests/

 */

(function(win) {

    // If browser isn't IE, then stop execution! This handles the script
    // being loaded by non IE browsers because the developer didn't use
    // conditional comments.
    if (/*@cc_on!@*/true) return;

    // =========================== Init Objects ============================

    var doc = document;
    var root = doc.documentElement;
    var xhr = getXHRObject();
    var ieVersion = /MSIE (\d+)/.exec(navigator.userAgent)[1];

    // If were not in standards mode, IE is too old / new or we can't create
    // an XMLHttpRequest object then we should get out now.
    if (doc.compatMode != 'CSS1Compat' || ieVersion<6 || ieVersion>8 || !xhr) {
        return;
    }


    // ========================= Common Objects ============================

    // Compatiable selector engines in order of CSS3 support. Note: '*' is
    // a placholder for the object key name. (basically, crude compression)
    var selectorEngines = {
        "NW"								: "*.Dom.select",
        "MooTools"							: "$$",
        "DOMAssistant"						: "*.$",
        "Prototype"							: "$$",
        "YAHOO"								: "*.util.Selector.query",
        "Sizzle"							: "*",
        "jQuery"							: "*",
        "dojo"								: "*.query"
    };

    var selectorMethod;
    var enabledWatchers 					= [];     // array of :enabled/:disabled elements to poll
    var ie6PatchID 							= 0;      // used to solve ie6's multiple class bug
    var patchIE6MultipleClasses				= true;   // if true adds class bloat to ie6
    var namespace 							= "slvzr";

    // Stylesheet parsing regexp's
    var RE_COMMENT							= /(\/\*[^*]*\*+([^\/][^*]*\*+)*\/)\s*/g;
    var RE_IMPORT							= /@import\s*(?:(?:(?:url\(\s*(['"]?)(.*)\1)\s*\))|(?:(['"])(.*)\3))[^;]*;/g;
    var RE_ASSET_URL 						= /\burl\(\s*(["']?)(?!data:)([^"')]+)\1\s*\)/g;
    var RE_PSEUDO_STRUCTURAL				= /^:(empty|(first|last|only|nth(-last)?)-(child|of-type))$/;
    var RE_PSEUDO_ELEMENTS					= /:(:first-(?:line|letter))/g;
    var RE_SELECTOR_GROUP					= /(^|})\s*([^\{]*?[\[:][^{]+)/g;
    var RE_SELECTOR_PARSE					= /([ +~>])|(:[a-z-]+(?:\(.*?\)+)?)|(\[.*?\])/g;
    var RE_LIBRARY_INCOMPATIBLE_PSEUDOS		= /(:not\()?:(hover|enabled|disabled|focus|checked|target|active|visited|first-line|first-letter)\)?/g;
    var RE_PATCH_CLASS_NAME_REPLACE			= /[^\w-]/g;

    // HTML UI element regexp's
    var RE_INPUT_ELEMENTS					= /^(INPUT|SELECT|TEXTAREA|BUTTON)$/;
    var RE_INPUT_CHECKABLE_TYPES			= /^(checkbox|radio)$/;

    // Broken attribute selector implementations (IE7/8 native [^=""], [$=""] and [*=""])
    var BROKEN_ATTR_IMPLEMENTATIONS			= ieVersion>6 ? /[\$\^*]=(['"])\1/ : null;

    // Whitespace normalization regexp's
    var RE_TIDY_TRAILING_WHITESPACE			= /([(\[+~])\s+/g;
    var RE_TIDY_LEADING_WHITESPACE			= /\s+([)\]+~])/g;
    var RE_TIDY_CONSECUTIVE_WHITESPACE		= /\s+/g;
    var RE_TIDY_TRIM_WHITESPACE				= /^\s*((?:[\S\s]*\S)?)\s*$/;

    // String constants
    var EMPTY_STRING						= "";
    var SPACE_STRING						= " ";
    var PLACEHOLDER_STRING					= "$1";

    // =========================== Patching ================================

    // --[ patchStyleSheet() ]----------------------------------------------
    // Scans the passed cssText for selectors that require emulation and
    // creates one or more patches for each matched selector.
    function patchStyleSheet( cssText ) {
        return cssText.replace(RE_PSEUDO_ELEMENTS, PLACEHOLDER_STRING).
            replace(RE_SELECTOR_GROUP, function(m, prefix, selectorText) {
                var selectorGroups = selectorText.split(",");
                for (var c = 0, cs = selectorGroups.length; c < cs; c++) {
                    var selector = normalizeSelectorWhitespace(selectorGroups[c]) + SPACE_STRING;
                    var patches = [];
                    selectorGroups[c] = selector.replace(RE_SELECTOR_PARSE,
                        function(match, combinator, pseudo, attribute, index) {
                            if (combinator) {
                                if (patches.length>0) {
                                    applyPatches( selector.substring(0, index), patches );
                                    patches = [];
                                }
                                return combinator;
                            }
                            else {
                                var patch = (pseudo) ? patchPseudoClass( pseudo ) : patchAttribute( attribute );
                                if (patch) {
                                    patches.push(patch);
                                    return "." + patch.className;
                                }
                                return match;
                            }
                        }
                    );
                }
                return prefix + selectorGroups.join(",");
            });
    };

    // --[ patchAttribute() ]-----------------------------------------------
    // returns a patch for an attribute selector.
    function patchAttribute( attr ) {
        return (!BROKEN_ATTR_IMPLEMENTATIONS || BROKEN_ATTR_IMPLEMENTATIONS.test(attr)) ?
        { className: createClassName(attr), applyClass: true } : null;
    };

    // --[ patchPseudoClass() ]---------------------------------------------
    // returns a patch for a pseudo-class
    function patchPseudoClass( pseudo ) {

        var applyClass = true;
        var className = createClassName(pseudo.slice(1));
        var isNegated = pseudo.substring(0, 5) == ":not(";
        var activateEventName;
        var deactivateEventName;

        // if negated, remove :not()
        if (isNegated) {
            pseudo = pseudo.slice(5, -1);
        }

        // bracket contents are irrelevant - remove them
        var bracketIndex = pseudo.indexOf("(")
        if (bracketIndex > -1) {
            pseudo = pseudo.substring(0, bracketIndex);
        }

        // check we're still dealing with a pseudo-class
        if (pseudo.charAt(0) == ":") {
            switch (pseudo.slice(1)) {

                case "root":
                    applyClass = function(e) {
                        return isNegated ? e != root : e == root;
                    }
                    break;

                case "target":
                    // :target is only supported in IE8
                    if (ieVersion == 8) {
                        applyClass = function(e) {
                            var handler = function() {
                                var hash = location.hash;
                                var hashID = hash.slice(1);
                                return isNegated ? (hash == EMPTY_STRING || e.id != hashID) : (hash != EMPTY_STRING && e.id == hashID);
                            };
                            addEvent( win, "hashchange", function() {
                                toggleElementClass(e, className, handler());
                            })
                            return handler();
                        }
                        break;
                    }
                    return false;

                case "checked":
                    applyClass = function(e) {
                        if (RE_INPUT_CHECKABLE_TYPES.test(e.type)) {
                            addEvent( e, "propertychange", function() {
                                if (event.propertyName == "checked") {
                                    toggleElementClass( e, className, e.checked !== isNegated );
                                }
                            })
                        }
                        return e.checked !== isNegated;
                    }
                    break;

                case "disabled":
                    isNegated = !isNegated;

                case "enabled":
                    applyClass = function(e) {
                        if (RE_INPUT_ELEMENTS.test(e.tagName)) {
                            addEvent( e, "propertychange", function() {
                                if (event.propertyName == "$disabled") {
                                    toggleElementClass( e, className, e.$disabled === isNegated );
                                }
                            });
                            enabledWatchers.push(e);
                            e.$disabled = e.disabled;
                            return e.disabled === isNegated;
                        }
                        return pseudo == ":enabled" ? isNegated : !isNegated;
                    }
                    break;

                case "focus":
                    activateEventName = "focus";
                    deactivateEventName = "blur";

                case "hover":
                    if (!activateEventName) {
                        activateEventName = "mouseenter";
                        deactivateEventName = "mouseleave";
                    }
                    applyClass = function(e) {
                        addEvent( e, isNegated ? deactivateEventName : activateEventName, function() {
                            toggleElementClass( e, className, true );
                        })
                        addEvent( e, isNegated ? activateEventName : deactivateEventName, function() {
                            toggleElementClass( e, className, false );
                        })
                        return isNegated;
                    }
                    break;

                // everything else
                default:
                    // If we don't support this pseudo-class don't create
                    // a patch for it
                    if (!RE_PSEUDO_STRUCTURAL.test(pseudo)) {
                        return false;
                    }
                    break;
            }
        }
        return { className: className, applyClass: applyClass };
    };

    // --[ applyPatches() ]-------------------------------------------------
    // uses the passed selector text to find DOM nodes and patch them
    function applyPatches(selectorText, patches) {
        var elms;

        // Although some selector libraries can find :checked :enabled etc.
        // we need to find all elements that could have that state because
        // it can be changed by the user.
        var domSelectorText = selectorText.replace(RE_LIBRARY_INCOMPATIBLE_PSEUDOS, EMPTY_STRING);

        // If the dom selector equates to an empty string or ends with
        // whitespace then we need to append a universal selector (*) to it.
        if (domSelectorText == EMPTY_STRING || domSelectorText.charAt(domSelectorText.length - 1) == SPACE_STRING) {
            domSelectorText += "*";
        }

        // Ensure we catch errors from the selector library
        try {
            elms = selectorMethod( domSelectorText );
        } catch (ex) {
            // #DEBUG_START
            log( "Selector '" + selectorText + "' threw exception '" + ex + "'" );
            // #DEBUG_END
        }


        if (elms) {
            for (var d = 0, dl = elms.length; d < dl; d++) {
                var elm = elms[d];
                var cssClasses = elm.className;
                for (var f = 0, fl = patches.length; f < fl; f++) {
                    var patch = patches[f];

                    if (!hasPatch(elm, patch)) {
                        if (patch.applyClass && (patch.applyClass === true || patch.applyClass(elm) === true)) {
                            cssClasses = toggleClass(cssClasses, patch.className, true );
                        }
                    }
                }
                elm.className = cssClasses;
            }
        }
    };

    // --[ hasPatch() ]-----------------------------------------------------
    // checks for the exsistence of a patch on an element
    function hasPatch( elm, patch ) {
        return new RegExp("(^|\\s)" + patch.className + "(\\s|$)").test(elm.className);
    };


    // =========================== Utility =================================

    function createClassName( className ) {
        return namespace + "-" + ((ieVersion == 6 && patchIE6MultipleClasses) ?
            ie6PatchID++
            :
            className.replace(RE_PATCH_CLASS_NAME_REPLACE, function(a) { return a.charCodeAt(0) }));
    };

    // --[ log() ]----------------------------------------------------------
    // #DEBUG_START
    function log( message ) {
        if (win.console) {
            win.console.log(message);
        }
    };
    // #DEBUG_END

    // --[ trim() ]---------------------------------------------------------
    // removes leading, trailing whitespace from a string
    function trim( text ) {
        return text.replace(RE_TIDY_TRIM_WHITESPACE, PLACEHOLDER_STRING);
    };

    // --[ normalizeWhitespace() ]------------------------------------------
    // removes leading, trailing and consecutive whitespace from a string
    function normalizeWhitespace( text ) {
        return trim(text).replace(RE_TIDY_CONSECUTIVE_WHITESPACE, SPACE_STRING);
    };

    // --[ normalizeSelectorWhitespace() ]----------------------------------
    // tidies whitespace around selector brackets and combinators
    function normalizeSelectorWhitespace( selectorText ) {
        return normalizeWhitespace(selectorText.
                replace(RE_TIDY_TRAILING_WHITESPACE, PLACEHOLDER_STRING).
                replace(RE_TIDY_LEADING_WHITESPACE, PLACEHOLDER_STRING)
        );
    };

    // --[ toggleElementClass() ]-------------------------------------------
    // toggles a single className on an element
    function toggleElementClass( elm, className, on ) {
        var oldClassName = elm.className;
        var newClassName = toggleClass(oldClassName, className, on);
        if (newClassName != oldClassName) {
            elm.className = newClassName;
            elm.parentNode.className += EMPTY_STRING;
        }
    };

    // --[ toggleClass() ]--------------------------------------------------
    // adds / removes a className from a string of classNames. Used to
    // manage multiple class changes without forcing a DOM redraw
    function toggleClass( classList, className, on ) {
        var re = RegExp("(^|\\s)" + className + "(\\s|$)");
        var classExists = re.test(classList);
        if (on) {
            return classExists ? classList : classList + SPACE_STRING + className;
        } else {
            return classExists ? trim(classList.replace(re, PLACEHOLDER_STRING)) : classList;
        }
    };

    // --[ addEvent() ]-----------------------------------------------------
    function addEvent(elm, eventName, eventHandler) {
        elm.attachEvent("on" + eventName, eventHandler);
    };

    // --[ getXHRObject() ]-------------------------------------------------
    function getXHRObject()
    {
        if (win.XMLHttpRequest) {
            return new XMLHttpRequest;
        }
        try	{
            return new ActiveXObject('Microsoft.XMLHTTP');
        } catch(e) {
            return null;
        }
    };

    // --[ loadStyleSheet() ]-----------------------------------------------
    function loadStyleSheet( url ) {
        xhr.open("GET", url, false);
        xhr.send();
        return (xhr.status==200) ? xhr.responseText : EMPTY_STRING;
    };

    // --[ resolveUrl() ]---------------------------------------------------
    // Converts a URL fragment to a fully qualified URL using the specified
    // context URL. Returns null if same-origin policy is broken
    function resolveUrl( url, contextUrl ) {

        function getProtocolAndHost( url ) {
            return url.substring(0, url.indexOf("/", 8));
        };

        // absolute path
        if (/^https?:\/\//i.test(url)) {
            return getProtocolAndHost(contextUrl) == getProtocolAndHost(url) ? url : null;
        }

        // root-relative path
        if (url.charAt(0)=="/")	{
            return getProtocolAndHost(contextUrl) + url;
        }

        // relative path
        var contextUrlPath = contextUrl.split(/[?#]/)[0]; // ignore query string in the contextUrl
        if (url.charAt(0) != "?" && contextUrlPath.charAt(contextUrlPath.length - 1) != "/") {
            contextUrlPath = contextUrlPath.substring(0, contextUrlPath.lastIndexOf("/") + 1);
        }

        return contextUrlPath + url;
    };

    // --[ parseStyleSheet() ]----------------------------------------------
    // Downloads the stylesheet specified by the URL, removes it's comments
    // and recursivly replaces @import rules with their contents, ultimately
    // returning the full cssText.
    function parseStyleSheet( url ) {
        if (url) {
            return loadStyleSheet(url).replace(RE_COMMENT, EMPTY_STRING).
                replace(RE_IMPORT, function( match, quoteChar, importUrl, quoteChar2, importUrl2 ) {
                    return parseStyleSheet(resolveUrl(importUrl || importUrl2, url));
                }).
                replace(RE_ASSET_URL, function( match, quoteChar, assetUrl ) {
                    quoteChar = quoteChar || EMPTY_STRING;
                    return " url(" + quoteChar + resolveUrl(assetUrl, url) + quoteChar + ") ";
                });
        }
        return EMPTY_STRING;
    };

    // --[ init() ]---------------------------------------------------------
    function init() {
        // honour the <base> tag
        var url, stylesheet;
        var baseTags = doc.getElementsByTagName("BASE");
        var baseUrl = (baseTags.length > 0) ? baseTags[0].href : doc.location.href;

        /* Note: This code prevents IE from freezing / crashing when using
         @font-face .eot files but it modifies the <head> tag and could
         trigger the IE stylesheet limit. It will also cause FOUC issues.
         If you choose to use it, make sure you comment out the for loop
         directly below this comment.

         var head = doc.getElementsByTagName("head")[0];
         for (var c=doc.styleSheets.length-1; c>=0; c--) {
         stylesheet = doc.styleSheets[c]
         head.appendChild(doc.createElement("style"))
         var patchedStylesheet = doc.styleSheets[doc.styleSheets.length-1];

         if (stylesheet.href != EMPTY_STRING) {
         url = resolveUrl(stylesheet.href, baseUrl)
         if (url) {
         patchedStylesheet.cssText = patchStyleSheet( parseStyleSheet( url ) )
         stylesheet.disabled = true
         setTimeout( function () {
         stylesheet.owningElement.parentNode.removeChild(stylesheet.owningElement)
         })
         }
         }
         }
         */

        for (var c = 0; c < doc.styleSheets.length; c++) {
            stylesheet = doc.styleSheets[c]
            if (stylesheet.href != EMPTY_STRING) {
                url = resolveUrl(stylesheet.href, baseUrl);
                if (url) {
                    stylesheet.cssText = patchStyleSheet( parseStyleSheet( url ) );
                }
            }
        }

        // :enabled & :disabled polling script (since we can't hook
        // onpropertychange event when an element is disabled)
        if (enabledWatchers.length > 0) {
            setInterval( function() {
                for (var c = 0, cl = enabledWatchers.length; c < cl; c++) {
                    var e = enabledWatchers[c];
                    if (e.disabled !== e.$disabled) {
                        if (e.disabled) {
                            e.disabled = false;
                            e.$disabled = true;
                            e.disabled = true;
                        }
                        else {
                            e.$disabled = e.disabled;
                        }
                    }
                }
            },250)
        }
    };

    // Bind selectivizr to the ContentLoaded event.
    ContentLoaded(win, function() {
        // Determine the "best fit" selector engine
        for (var engine in selectorEngines) {
            var members, member, context = win;
            if (win[engine]) {
                members = selectorEngines[engine].replace("*", engine).split(".");
                while ((member = members.shift()) && (context = context[member])) {}
                if (typeof context == "function") {
                    selectorMethod = context;
                    init();
                    return;
                }
            }
        }
    });


    /*!
     * ContentLoaded.js by Diego Perini, modified for IE<9 only (to save space)
     *
     * Author: Diego Perini (diego.perini at gmail.com)
     * Summary: cross-browser wrapper for DOMContentLoaded
     * Updated: 20101020
     * License: MIT
     * Version: 1.2
     *
     * URL:
     * http://javascript.nwbox.com/ContentLoaded/
     * http://javascript.nwbox.com/ContentLoaded/MIT-LICENSE
     *
     */

    // @w window reference
    // @f function reference
    function ContentLoaded(win, fn) {

        var done = false, top = true,
            init = function(e) {
                if (e.type == "readystatechange" && doc.readyState != "complete") return;
                (e.type == "load" ? win : doc).detachEvent("on" + e.type, init, false);
                if (!done && (done = true)) fn.call(win, e.type || e);
            },
            poll = function() {
                try { root.doScroll("left"); } catch(e) { setTimeout(poll, 50); return; }
                init('poll');
            };

        if (doc.readyState == "complete") fn.call(win, EMPTY_STRING);
        else {
            if (doc.createEventObject && root.doScroll) {
                try { top = !win.frameElement; } catch(e) { }
                if (top) poll();
            }
            addEvent(doc,"readystatechange", init);
            addEvent(win,"load", init);
        }
    };
})(this);

/*! http://mths.be/placeholder v2.0.8 by @mathias */
;(function(window, document, $) {

    // Opera Mini v7 doesn’t support placeholder although its DOM seems to indicate so
    var isOperaMini = Object.prototype.toString.call(window.operamini) == '[object OperaMini]';
    var isInputSupported = 'placeholder' in document.createElement('input') && !isOperaMini;
    var isTextareaSupported = 'placeholder' in document.createElement('textarea') && !isOperaMini;
    var prototype = $.fn;
    var valHooks = $.valHooks;
    var propHooks = $.propHooks;
    var hooks;
    var placeholder;

    if (isInputSupported && isTextareaSupported) {

        placeholder = prototype.placeholder = function() {
            return this;
        };

        placeholder.input = placeholder.textarea = true;

    } else {

        placeholder = prototype.placeholder = function() {
            var $this = this;
            $this
                .filter((isInputSupported ? 'textarea' : ':input') + '[placeholder]')
                .not('.placeholder')
                .bind({
                    'focus.placeholder': clearPlaceholder,
                    'blur.placeholder': setPlaceholder
                })
                .data('placeholder-enabled', true)
                .trigger('blur.placeholder');
            return $this;
        };

        placeholder.input = isInputSupported;
        placeholder.textarea = isTextareaSupported;

        hooks = {
            'get': function(element) {
                var $element = $(element);

                var $passwordInput = $element.data('placeholder-password');
                if ($passwordInput) {
                    return $passwordInput[0].value;
                }

                return $element.data('placeholder-enabled') && $element.hasClass('placeholder') ? '' : element.value;
            },
            'set': function(element, value) {
                var $element = $(element);

                var $passwordInput = $element.data('placeholder-password');
                if ($passwordInput) {
                    return $passwordInput[0].value = value;
                }

                if (!$element.data('placeholder-enabled')) {
                    return element.value = value;
                }
                if (value == '') {
                    element.value = value;
                    // Issue #56: Setting the placeholder causes problems if the element continues to have focus.
                    if (element != safeActiveElement()) {
                        // We can't use `triggerHandler` here because of dummy text/password inputs :(
                        setPlaceholder.call(element);
                    }
                } else if ($element.hasClass('placeholder')) {
                    clearPlaceholder.call(element, true, value) || (element.value = value);
                } else {
                    element.value = value;
                }
                // `set` can not return `undefined`; see http://jsapi.info/jquery/1.7.1/val#L2363
                return $element;
            }
        };

        if (!isInputSupported) {
            valHooks.input = hooks;
            propHooks.value = hooks;
        }
        if (!isTextareaSupported) {
            valHooks.textarea = hooks;
            propHooks.value = hooks;
        }

        $(function() {
            // Look for forms
            $(document).delegate('form', 'submit.placeholder', function() {
                // Clear the placeholder values so they don't get submitted
                var $inputs = $('.placeholder', this).each(clearPlaceholder);
                setTimeout(function() {
                    $inputs.each(setPlaceholder);
                }, 10);
            });
        });

        // Clear placeholder values upon page reload
        $(window).bind('beforeunload.placeholder', function() {
            $('.placeholder').each(function() {
                this.value = '';
            });
        });

    }

    function args(elem) {
        // Return an object of element attributes
        var newAttrs = {};
        var rinlinejQuery = /^jQuery\d+$/;
        $.each(elem.attributes, function(i, attr) {
            if (attr.specified && !rinlinejQuery.test(attr.name)) {
                newAttrs[attr.name] = attr.value;
            }
        });
        return newAttrs;
    }

    function clearPlaceholder(event, value) {
        var input = this;
        var $input = $(input);
        if (input.value == $input.attr('placeholder') && $input.hasClass('placeholder')) {
            if ($input.data('placeholder-password')) {
                $input = $input.hide().next().show().attr('id', $input.removeAttr('id').data('placeholder-id'));
                // If `clearPlaceholder` was called from `$.valHooks.input.set`
                if (event === true) {
                    return $input[0].value = value;
                }
                $input.focus();
            } else {
                input.value = '';
                $input.removeClass('placeholder');
                input == safeActiveElement() && input.select();
            }
        }
    }

    function setPlaceholder() {
        var $replacement;
        var input = this;
        var $input = $(input);
        var id = this.id;
        if (input.value == '') {
            if (input.type == 'password') {
                if (!$input.data('placeholder-textinput')) {
                    try {
                        $replacement = $input.clone().attr({ 'type': 'text' });
                    } catch(e) {
                        $replacement = $('<input>').attr($.extend(args(this), { 'type': 'text' }));
                    }
                    $replacement
                        .removeAttr('name')
                        .data({
                            'placeholder-password': $input,
                            'placeholder-id': id
                        })
                        .bind('focus.placeholder', clearPlaceholder);
                    $input
                        .data({
                            'placeholder-textinput': $replacement,
                            'placeholder-id': id
                        })
                        .before($replacement);
                }
                $input = $input.removeAttr('id').hide().prev().attr('id', id).show();
                // Note: `$input[0] != input` now!
            }
            $input.addClass('placeholder');
            $input[0].value = $input.attr('placeholder');
        } else {
            $input.removeClass('placeholder');
        }
    }

    function safeActiveElement() {
        // Avoid IE9 `document.activeElement` of death
        // https://github.com/mathiasbynens/jquery-placeholder/pull/99
        try {
            return document.activeElement;
        } catch (exception) {}
    }

}(this, document, jQuery));

/*! Respond.js v1.4.2: min/max-width media query polyfill
 * Copyright 2014 Scott Jehl
 * Licensed under MIT
 * http://j.mp/respondjs */

/*! matchMedia() polyfill - Test a CSS media type/query in JS. Authors & copyright (c) 2012: Scott Jehl, Paul Irish, Nicholas Zakas. Dual MIT/BSD license */
/*! NOTE: If you're already including a window.matchMedia polyfill via Modernizr or otherwise, you don't need this part */
(function(w) {
    "use strict";
    w.matchMedia = w.matchMedia || function(doc, undefined) {
        var bool, docElem = doc.documentElement, refNode = docElem.firstElementChild || docElem.firstChild, fakeBody = doc.createElement("body"), div = doc.createElement("div");
        div.id = "mq-test-1";
        div.style.cssText = "position:absolute;top:-100em";
        fakeBody.style.background = "none";
        fakeBody.appendChild(div);
        return function(q) {
            div.innerHTML = '&shy;<style media="' + q + '"> #mq-test-1 { width: 42px; }</style>';
            docElem.insertBefore(fakeBody, refNode);
            bool = div.offsetWidth === 42;
            docElem.removeChild(fakeBody);
            return {
                matches: bool,
                media: q
            };
        };
    }(w.document);
})(this);

/*! matchMedia() polyfill addListener/removeListener extension. Author & copyright (c) 2012: Scott Jehl. Dual MIT/BSD license */
(function(w) {
    "use strict";
    if (w.matchMedia && w.matchMedia("all").addListener) {
        return false;
    }
    var localMatchMedia = w.matchMedia, hasMediaQueries = localMatchMedia("only all").matches, isListening = false, timeoutID = 0, queries = [], handleChange = function(evt) {
        w.clearTimeout(timeoutID);
        timeoutID = w.setTimeout(function() {
            for (var i = 0, il = queries.length; i < il; i++) {
                var mql = queries[i].mql, listeners = queries[i].listeners || [], matches = localMatchMedia(mql.media).matches;
                if (matches !== mql.matches) {
                    mql.matches = matches;
                    for (var j = 0, jl = listeners.length; j < jl; j++) {
                        listeners[j].call(w, mql);
                    }
                }
            }
        }, 30);
    };
    w.matchMedia = function(media) {
        var mql = localMatchMedia(media), listeners = [], index = 0;
        mql.addListener = function(listener) {
            if (!hasMediaQueries) {
                return;
            }
            if (!isListening) {
                isListening = true;
                w.addEventListener("resize", handleChange, true);
            }
            if (index === 0) {
                index = queries.push({
                    mql: mql,
                    listeners: listeners
                });
            }
            listeners.push(listener);
        };
        mql.removeListener = function(listener) {
            for (var i = 0, il = listeners.length; i < il; i++) {
                if (listeners[i] === listener) {
                    listeners.splice(i, 1);
                }
            }
        };
        return mql;
    };
})(this);

(function(w) {
    "use strict";
    var respond = {};
    w.respond = respond;
    respond.update = function() {};
    var requestQueue = [], xmlHttp = function() {
        var xmlhttpmethod = false;
        try {
            xmlhttpmethod = new w.XMLHttpRequest();
        } catch (e) {
            xmlhttpmethod = new w.ActiveXObject("Microsoft.XMLHTTP");
        }
        return function() {
            return xmlhttpmethod;
        };
    }(), ajax = function(url, callback) {
        var req = xmlHttp();
        if (!req) {
            return;
        }
        req.open("GET", url, true);
        req.onreadystatechange = function() {
            if (req.readyState !== 4 || req.status !== 200 && req.status !== 304) {
                return;
            }
            callback(req.responseText);
        };
        if (req.readyState === 4) {
            return;
        }
        req.send(null);
    }, isUnsupportedMediaQuery = function(query) {
        return query.replace(respond.regex.minmaxwh, "").match(respond.regex.other);
    };
    respond.ajax = ajax;
    respond.queue = requestQueue;
    respond.unsupportedmq = isUnsupportedMediaQuery;
    respond.regex = {
        media: /@media[^\{]+\{([^\{\}]*\{[^\}\{]*\})+/gi,
        keyframes: /@(?:\-(?:o|moz|webkit)\-)?keyframes[^\{]+\{(?:[^\{\}]*\{[^\}\{]*\})+[^\}]*\}/gi,
        comments: /\/\*[^*]*\*+([^/][^*]*\*+)*\//gi,
        urls: /(url\()['"]?([^\/\)'"][^:\)'"]+)['"]?(\))/g,
        findStyles: /@media *([^\{]+)\{([\S\s]+?)$/,
        only: /(only\s+)?([a-zA-Z]+)\s?/,
        minw: /\(\s*min\-width\s*:\s*(\s*[0-9\.]+)(px|em)\s*\)/,
        maxw: /\(\s*max\-width\s*:\s*(\s*[0-9\.]+)(px|em)\s*\)/,
        minmaxwh: /\(\s*m(in|ax)\-(height|width)\s*:\s*(\s*[0-9\.]+)(px|em)\s*\)/gi,
        other: /\([^\)]*\)/g
    };
    respond.mediaQueriesSupported = w.matchMedia && w.matchMedia("only all") !== null && w.matchMedia("only all").matches;
    if (respond.mediaQueriesSupported) {
        return;
    }
    var doc = w.document, docElem = doc.documentElement, mediastyles = [], rules = [], appendedEls = [], parsedSheets = {}, resizeThrottle = 30, head = doc.getElementsByTagName("head")[0] || docElem, base = doc.getElementsByTagName("base")[0], links = head.getElementsByTagName("link"), lastCall, resizeDefer, eminpx, getEmValue = function() {
        var ret, div = doc.createElement("div"), body = doc.body, originalHTMLFontSize = docElem.style.fontSize, originalBodyFontSize = body && body.style.fontSize, fakeUsed = false;
        div.style.cssText = "position:absolute;font-size:1em;width:1em";
        if (!body) {
            body = fakeUsed = doc.createElement("body");
            body.style.background = "none";
        }
        docElem.style.fontSize = "100%";
        body.style.fontSize = "100%";
        body.appendChild(div);
        if (fakeUsed) {
            docElem.insertBefore(body, docElem.firstChild);
        }
        ret = div.offsetWidth;
        if (fakeUsed) {
            docElem.removeChild(body);
        } else {
            body.removeChild(div);
        }
        docElem.style.fontSize = originalHTMLFontSize;
        if (originalBodyFontSize) {
            body.style.fontSize = originalBodyFontSize;
        }
        ret = eminpx = parseFloat(ret);
        return ret;
    }, applyMedia = function(fromResize) {
        var name = "clientWidth", docElemProp = docElem[name], currWidth = doc.compatMode === "CSS1Compat" && docElemProp || doc.body[name] || docElemProp, styleBlocks = {}, lastLink = links[links.length - 1], now = new Date().getTime();
        if (fromResize && lastCall && now - lastCall < resizeThrottle) {
            w.clearTimeout(resizeDefer);
            resizeDefer = w.setTimeout(applyMedia, resizeThrottle);
            return;
        } else {
            lastCall = now;
        }
        for (var i in mediastyles) {
            if (mediastyles.hasOwnProperty(i)) {
                var thisstyle = mediastyles[i], min = thisstyle.minw, max = thisstyle.maxw, minnull = min === null, maxnull = max === null, em = "em";
                if (!!min) {
                    min = parseFloat(min) * (min.indexOf(em) > -1 ? eminpx || getEmValue() : 1);
                }
                if (!!max) {
                    max = parseFloat(max) * (max.indexOf(em) > -1 ? eminpx || getEmValue() : 1);
                }
                if (!thisstyle.hasquery || (!minnull || !maxnull) && (minnull || currWidth >= min) && (maxnull || currWidth <= max)) {
                    if (!styleBlocks[thisstyle.media]) {
                        styleBlocks[thisstyle.media] = [];
                    }
                    styleBlocks[thisstyle.media].push(rules[thisstyle.rules]);
                }
            }
        }
        for (var j in appendedEls) {
            if (appendedEls.hasOwnProperty(j)) {
                if (appendedEls[j] && appendedEls[j].parentNode === head) {
                    head.removeChild(appendedEls[j]);
                }
            }
        }
        appendedEls.length = 0;
        for (var k in styleBlocks) {
            if (styleBlocks.hasOwnProperty(k)) {
                var ss = doc.createElement("style"), css = styleBlocks[k].join("\n");
                ss.type = "text/css";
                ss.media = k;
                head.insertBefore(ss, lastLink.nextSibling);
                if (ss.styleSheet) {
                    ss.styleSheet.cssText = css;
                } else {
                    ss.appendChild(doc.createTextNode(css));
                }
                appendedEls.push(ss);
            }
        }
    }, translate = function(styles, href, media) {
        var qs = styles.replace(respond.regex.comments, "").replace(respond.regex.keyframes, "").match(respond.regex.media), ql = qs && qs.length || 0;
        href = href.substring(0, href.lastIndexOf("/"));
        var repUrls = function(css) {
            return css.replace(respond.regex.urls, "$1" + href + "$2$3");
        }, useMedia = !ql && media;
        if (href.length) {
            href += "/";
        }
        if (useMedia) {
            ql = 1;
        }
        for (var i = 0; i < ql; i++) {
            var fullq, thisq, eachq, eql;
            if (useMedia) {
                fullq = media;
                rules.push(repUrls(styles));
            } else {
                fullq = qs[i].match(respond.regex.findStyles) && RegExp.$1;
                rules.push(RegExp.$2 && repUrls(RegExp.$2));
            }
            eachq = fullq.split(",");
            eql = eachq.length;
            for (var j = 0; j < eql; j++) {
                thisq = eachq[j];
                if (isUnsupportedMediaQuery(thisq)) {
                    continue;
                }
                mediastyles.push({
                    media: thisq.split("(")[0].match(respond.regex.only) && RegExp.$2 || "all",
                    rules: rules.length - 1,
                    hasquery: thisq.indexOf("(") > -1,
                    minw: thisq.match(respond.regex.minw) && parseFloat(RegExp.$1) + (RegExp.$2 || ""),
                    maxw: thisq.match(respond.regex.maxw) && parseFloat(RegExp.$1) + (RegExp.$2 || "")
                });
            }
        }
        applyMedia();
    }, makeRequests = function() {
        if (requestQueue.length) {
            var thisRequest = requestQueue.shift();
            ajax(thisRequest.href, function(styles) {
                translate(styles, thisRequest.href, thisRequest.media);
                parsedSheets[thisRequest.href] = true;
                w.setTimeout(function() {
                    makeRequests();
                }, 0);
            });
        }
    }, ripCSS = function() {
        for (var i = 0; i < links.length; i++) {
            var sheet = links[i], href = sheet.href, media = sheet.media, isCSS = sheet.rel && sheet.rel.toLowerCase() === "stylesheet";
            if (!!href && isCSS && !parsedSheets[href]) {
                if (sheet.styleSheet && sheet.styleSheet.rawCssText) {
                    translate(sheet.styleSheet.rawCssText, href, media);
                    parsedSheets[href] = true;
                } else {
                    if (!/^([a-zA-Z:]*\/\/)/.test(href) && !base || href.replace(RegExp.$1, "").split("/")[0] === w.location.host) {
                        if (href.substring(0, 2) === "//") {
                            href = w.location.protocol + href;
                        }
                        requestQueue.push({
                            href: href,
                            media: media
                        });
                    }
                }
            }
        }
        makeRequests();
    };
    ripCSS();
    respond.update = ripCSS;
    respond.getEmValue = getEmValue;
    function callMedia() {
        applyMedia(true);
    }
    if (w.addEventListener) {
        w.addEventListener("resize", callMedia, false);
    } else if (w.attachEvent) {
        w.attachEvent("onresize", callMedia);
    }
})(this);
;(function ($) {

    /*! Tiny Pub/Sub - v0.7.0 - 2013-01-29
     * https://github.com/cowboy/jquery-tiny-pubsub
     * Copyright (c) 2014 "Cowboy" Ben Alman; Licensed MIT */
    var o = $({});
    $.subscribe = function () {
        o.on.apply(o, arguments);
    };

    $.unsubscribe = function () {
        o.off.apply(o, arguments);
    };

    $.publish = function () {
        o.trigger.apply(o, arguments);
    };
}(jQuery));

;(function ($, window) {
    'use strict';

    var numberRegex = /^\-?\d*\.?\d*$/,
        objectRegex = /^[\[\{]/;

    /**
     * Tries to deserialize the given string value and returns the right
     * value if its successful.
     *
     * @private
     * @method deserializeValue
     * @param {String} value
     * @returns {String|Boolean|Number|Object|Array|null}
     */
    function deserializeValue(value) {
        try {
            return !value ? value : value === 'true' || (
                value === 'false' ? false
                    : value === 'null' ? null
                    : numberRegex.test(value) ? +value
                    : objectRegex.test(value) ? $.parseJSON(value)
                    : value
            )
        } catch (e) {
            return value;
        }
    }

    /**
     * Constructor method of the PluginBase class. This method will try to
     * call the ```init```-method, where you can place your custom initialization of the plugin.
     *
     * @class PluginBase
     * @constructor
     * @param {String} name - Plugin name that is used for the events suffixes.
     * @param {HTMLElement} element - Element which should be used for the plugin.
     * @param {Object} options - The user settings, which overrides the default settings
     */
    function PluginBase(name, element, options) {
        var me = this;

        /**
         * @property {String} _name - Name of the Plugin
         * @private
         */
        me._name = name;

        /**
         * @property {jQuery} $el - Plugin element wrapped by jQuery
         */
        me.$el = $(element);

        /**
         * @property {Object} opts - Merged plugin options
         */
        me.opts = $.extend({}, me.defaults || {}, options);

        /**
         * @property {string} eventSuffix - Suffix which will be appended to the eventType to get namespaced events
         */
        me.eventSuffix = '.' + name;

        /**
         * @property {Array} _events Registered events listeners. See {@link PluginBase._on} for registration
         * @private
         */
        me._events = [];

        // Create new selector for the plugin
        $.expr[':']['plugin-' + name.toLowerCase()] = function (elem) {
            return !!$.data(elem, 'plugin_' + name);
        };

        // Call the init method of the plugin
        me.init();

        $.publish('plugin/' + name + '/onInit', [ me ]);
    }

    PluginBase.prototype = {

        /**
         * Template function for the plugin initialisation.
         * Must be overridden for custom initialisation logic or an error will be thrown.
         *
         * @public
         * @method init
         */
        init: function () {
            throw new Error('Plugin ' + this.getName() + ' has to have an init function!');
        },

        /**
         * Template function for the plugin destruction.
         * Should be overridden for custom destruction code.
         *
         * @public
         * @method destroy
         */
        destroy: function () {

            if (typeof console !== 'undefined' && typeof console.warn === 'function') {
                console.warn('Plugin ' + this.getName() + ' should have a custom destroy method!');
            }

            this._destroy();
        },

        /**
         * Template function to update the plugin.
         * This function will be called when the breakpoint has changed but the configurations are the same.
         *
         * @public
         * @method update
         */
        update: function () {

        },

        /**
         * Destroys the plugin on the {@link HTMLElement}. It removes the instance of the plugin
         * which is bounded to the {@link jQuery} element.
         *
         * If the plugin author has used the {@link PluginBase._on} method, the added event listeners
         * will automatically be cleared.
         *
         * @private
         * @method _destroy
         * @returns {PluginBase}
         */
        _destroy: function () {
            var me = this,
                name = me.getName();

            $.each(me._events, function (i, obj) {
                obj.el.off(obj.event);
            });

            // remove all references of extern plugins
            $.each(me.opts, function (o) {
                delete me.opts[o];
            });

            me.$el.removeData('plugin_' + name);

            $.publish('plugin/' + name + '/onDestroy', [ me ]);

            return me;
        },

        /**
         * Wrapper method for {@link jQuery.on}, which registers in the event in the {@link PluginBase._events} array,
         * so the listeners can automatically be removed using the {@link PluginBase._destroy} method.
         *
         * @params {jQuery} Element, which should be used to add the listener
         * @params {String} Event type, you want to register.
         * @returns {PluginBase}
         */
        _on: function () {
            var me = this,
                $el = $(arguments[0]),
                event = me.getEventName(arguments[1]),
                args = Array.prototype.slice.call(arguments, 2);

            me._events.push({ 'el': $el, 'event': event });
            args.unshift(event);
            $el.on.apply($el, args);

            $.publish('plugin/' + me._name + '/onRegisterEvent', [ $el, event ]);

            return me;
        },

        /**
         * Wrapper method for {@link jQuery.off}, which removes the event listener from the {@link PluginBase._events}
         * array.
         *
         * @param {jQuery} element - Element, which contains the listener
         * @param {String} event - Name of the event to remove.
         * @returns {PluginBase}
         * @private
         */
        _off: function (element, event) {
            var me = this,
                events = me._events,
                pluginEvent = me.getEventName(event),
                eventIds = [],
                $element = $(element),
                filteredEvents = $.grep(events, function (obj, index) {
                    eventIds.push(index);
                    return typeof obj !== 'undefined' && pluginEvent === obj.event && $element[0] === obj.el[0];
                });

            $.each(filteredEvents, function (event) {
                $element.off.call($element, event.event);
            });

            $.each(eventIds, function (id) {
                if (!events[id]) {
                    return;
                }
                delete events[id];
            });

            $.publish('plugin/' + me._name + '/onRemoveEvent', [ $element, event ]);

            return me;
        },

        /**
         * Returns the name of the plugin.
         * @returns {PluginBase._name|String}
         */
        getName: function () {
            return this._name;
        },

        /**
         * Returns the event name with the event suffix appended.
         * @param {String} event - Event name
         * @returns {String}
         */
        getEventName: function (event) {
            var suffix = this.eventSuffix,
                parts = event.split(' '),
                len = parts.length,
                i = 0;

            for (; i < len; i++) {
                parts[i] += suffix;
            }

            return parts.join(' ');
        },

        /**
         * Returns the element which registered the plugin.
         * @returns {PluginBase.$el}
         */
        getElement: function () {
            return this.$el;
        },

        /**
         * Returns the options of the plugin. The method returns a copy of the options object and not a reference.
         * @returns {Object}
         */
        getOptions: function () {
            return $.extend({}, this.opts);
        },

        /**
         * Returns the value of a single option.
         * @param {String} key - Option key.
         * @returns {mixed}
         */
        getOption: function (key) {
            return this.opts[key];
        },

        /**
         * Sets a plugin option. Deep linking of the options are now supported.
         * @param {String} key - Option key
         * @param {mixed} value - Option value
         * @returns {PluginBase}
         */
        setOption: function (key, value) {
            var me = this;

            me.opts[key] = value;

            return me;
        },

        /**
         * Fetches the configured options based on the {@link PluginBase.$el}.
         *
         * @param {Boolean} shouldDeserialize
         * @returns {mixed} configuration
         */
        applyDataAttributes: function (shouldDeserialize) {
            var me = this, attr;

            $.each(me.opts, function (key) {
                attr = me.$el.attr('data-' + key);

                if (typeof attr === 'undefined') {
                    return true;
                }

                me.opts[key] = shouldDeserialize !== false ? deserializeValue(attr) : attr;

                return true;
            });

            $.publish('plugin/' + me._name + '/onDataAttributes', [ me.$el, me.opts ]);

            return me.opts;
        }
    };

    // Expose the private PluginBase constructor to global jQuery object
    $.PluginBase = PluginBase;

    // Object.create support test, and fallback for browsers without it
    if (typeof Object.create !== 'function') {
        Object.create = function (o) {
            function F() { }
            F.prototype = o;
            return new F();
        };
    }

    /**
     * Creates a new jQuery plugin based on the {@link PluginBase} object prototype. The plugin will
     * automatically created in {@link jQuery.fn} namespace and will initialized on the fly.
     *
     * The {@link PluginBase} object supports an automatically destruction of the registered events. To
     * do so, please use the {@link PluginBase._on} method to create event listeners.
     *
     * @param {String} name - Name of the plugin
     * @param {Object|Function} plugin - Plugin implementation
     * @returns {void}
     *
     * @example
     * // Register your plugin
     * $.plugin('yourName', {
     *    defaults: { key: 'value' },
     *
     *    init: function() {
     *        // ...initialization code
     *    },
     *
     *    destroy: function() {
     *      // ...your destruction code
     *
     *      // Use the force! Use the internal destroy method.
     *      me._destroy();
     *    }
     * });
     *
     * // Call the plugin
     * $('.test').yourName();
     */
    $.plugin = function (name, plugin) {
        var pluginFn = function (options) {
                return this.each(function () {
                    var element = this,
                        pluginData = $.data(element, 'plugin_' + name);

                    if (!pluginData) {
                        if (typeof plugin === 'function') {
                            pluginData = new plugin();
                        } else {
                            var Plugin = function () {
                                PluginBase.call(this, name, element, options);
                            };

                            Plugin.prototype = $.extend(Object.create(PluginBase.prototype), { constructor: Plugin }, plugin);
                            pluginData = new Plugin();
                        }

                        $.data(element, 'plugin_' + name, pluginData);
                    }
                });
            };

        window.PluginsCollection = window.PluginsCollection || {};
        window.PluginsCollection[name] = plugin;

        $.fn[name] = pluginFn;
    };

    /**
     * Provides the ability to overwrite jQuery plugins which are built on top of the {@link PluginBase} class. All of
     * our jQuery plugins (or to be more technical about it, the prototypes of our plugins) are registered in the object
     * {@link window.PluginsCollection} which can be accessed from anywhere in your storefront.
     *
     * Please keep in mind that the method overwrites the plugin in jQuery's plugin namespace {@link jQuery.fn} as well,
     * but you still have the ability to access the overwritten method(s) using the ```superclass``` object property.
     *
     * @example How to overwrite the ```showResult```-method in the "search" plugin.
     * $.overridePlugin('search', {
     *    showResult: function(response) {
     *        //.. do something with the response
     *    }
     * });
     *
     * @example Call the original method without modifications
     * $.overridePlugin('search', {
     *    showResult: function(response) {
     *        this.superclass.showResult.apply(this, arguments);
     *    }
     * });
     */
    $.overridePlugin = function (pluginName, override) {
        var overridePlugin = window.PluginsCollection[pluginName];

        if (typeof overridePlugin !== 'object' || typeof override !== 'object') {
            return false;
        }

        $.fn[pluginName] = function (options) {
            return this.each(function () {
                var element = this,
                    pluginData = $.data(element, 'plugin_' + pluginName);

                if (!pluginData) {
                    var Plugin = function () {
                        PluginBase.call(this, pluginName, element, options);
                    };

                    Plugin.prototype = $.extend(Object.create(PluginBase.prototype), { constructor: Plugin, superclass: overridePlugin }, overridePlugin, override);
                    pluginData = new Plugin();

                    $.data(element, 'plugin_' + pluginName, pluginData);
                }
            });
        };
    };
})(jQuery, window);

/**
 * Global state manager
 *
 * The state manager helps to master different behaviors for different screen sizes.
 * It provides you with the ability to register different states that are handled
 * by breakpoints.
 *
 * Those Breakpoints are defined by entering and exiting points (in pixels)
 * based on the viewport width.
 * By entering the breakpoint range, the enter functions of the registered
 * listeners are called.
 * But when the defined points are exceeded, the registered listeners exit
 * functions will be called.
 *
 * That way you can register callbacks that will be called on entering / exiting the defined state.
 *
 * The manager provides you multiple helper methods and polyfills which help you
 * master responsive design.
 *
 * @example Initialize the StateManager
 * ```
 *     StateManager.init([{
 *         state: 'xs',
 *         enter: '0em',
 *         exit: '47.5em'
 *      }, {
 *         state: 'm',
 *         enter: '47.5em',
 *         exit: '64em'
 *      }]);
 * ```
 *
 * @example Register breakpoint listeners
 * ```
 *     StateManager.registerListener([{
 *        state: 'xs',
 *        enter: function() { console.log('onEnter'); },
 *        exit: function() { console.log('onExit'); }
 *     }]);
 * ```
 *
 * @example Wildcard support
 * ```
 *     StateManager.registerListener([{
 *         state: '*',
 *         enter: function() { console.log('onGlobalEnter'); },
 *         exit: function() { console.log('onGlobalExit'); }
 *     }]);
 * ```
 *
 * @example StateManager Events
 * In this example we are adding an event listener for the 'resize' event.
 * This event will be called independent of the original window resize event,
 * because the resize will be compared in a requestAnimationFrame loop.
 *
 * ```
 *     StateManager.on('resize', function () {
 *         console.log('onResize');
 *     });
 *
 *     StateManager.once('resize', function () {
 *         console.log('This resize event will only be called once');
 *     });
 * ```
 *
 * @example StateManager plugin support
 * In this example we register the plugin 'pluginName' on the element
 * matching the '.my-selector' selector.
 * You can also define view ports in which the plugin will be available.
 * When switching the view ports and the configuration isn't changed for
 * that state, only the 'update' function of the plugin will be called.
 *
 * ```
 *     // The plugin will be available on all view port states.
 *     // Uses the default configuration
 *
 *     StateManager.addPlugin('.my-selector', 'pluginName');
 *
 *     // The plugin will only be available for the 'xs' state.
 *     // Uses the default configuration.
 *
 *     StateManager.addPlugin('.my-selector', 'pluginName', 'xs');
 *
 *     // The plugin will only be available for the 'l' and 'xl' state.
 *     // Uses the default configuration.
 *
 *     StateManager.addPlugin('.my-selector', 'pluginName', ['l', 'xl']);
 *
 *     // The plugin will only be available for the 'xs' and 's' state.
 *     // For those two states, the passed config will be used.
 *
 *     StateManager.addPlugin('.my-selector', 'pluginName', {
 *         'configA': 'valueA',
 *         'configB': 'valueB',
 *         'configFoo': 'valueBar'
 *     }, ['xs', 's']);
 *
 *     // The plugin is available on all view port states.
 *     // We override the 'foo' config only for the 'm' state.
 *
 *     StateManager.addPlugin('.my-selector', 'pluginName', { 'foo': 'bar' })
 *                .addPlugin('.my-selector', 'pluginName', { 'foo': 'baz' }, 'm');
 * ```
 */
;(function ($, window, document) {
    'use strict';

    var $html = $('html'),
        vendorPropertyDiv = document.createElement('div'),
        vendorPrefixes = ['webkit', 'moz', 'ms', 'o'];

    /**
     * @class EventEmitter
     * @constructor
     */
    function EventEmitter() {
        var me = this;

        /**
         * @private
         * @property _events
         * @type {Object}
         */
        me._events = {};
    }

    EventEmitter.prototype = {

        constructor: EventEmitter,

        name: 'EventEmitter',

        /**
         * @public
         * @chainable
         * @method on
         * @param {String} eventName
         * @param {Function} callback
         * @param {*} context
         * @returns {EventEmitter}
         */
        on: function (eventName, callback, context) {
            var me = this,
                events = me._events || (me._events = {}),
                event = events[eventName] || (events[eventName] = []);

            event.push({
                callback: callback,
                context: context || me
            });

            return me;
        },

        /**
         * @public
         * @chainable
         * @method once
         * @param {String} eventName
         * @param {Function} callback
         * @param {*} context
         * @returns {EventEmitter}
         */
        once: function (eventName, callback, context) {
            var me = this,
                once = function () {
                    me.off(eventName, once, context);
                    callback.apply(me, arguments);
                };

            return me.on(eventName, once, context);
        },

        /**
         * @public
         * @chainable
         * @method off
         * @param {String} eventName
         * @param {Function} callback
         * @param {*} context
         * @returns {EventEmitter}
         */
        off: function (eventName, callback, context) {
            var me = this,
                events = me._events || (me._events = {}),
                eventNames = eventName ? [eventName] : Object.keys(events),
                eventList,
                event,
                name,
                len,
                i, j;

            for (i = 0, len = eventNames.length; i < len; i++) {
                name = eventNames[i];
                eventList = events[name];

                /**
                 * Return instead of continue because only the one passed
                 * event name can be wrong / not available.
                 */
                if (!eventList) {
                    return me;
                }

                if (!callback && !context) {
                    eventList.length = 0;
                    delete events[name];
                    continue;
                }

                for (j = eventList.length - 1; j >= 0; j--) {
                    event = eventList[j];

                    // Check if the callback and the context (if passed) is the same
                    if ((callback && callback !== event.callback) || (context && context !== event.context)) {
                        continue;
                    }

                    eventList.splice(j, 1);
                }
            }

            return me;
        },

        /**
         * @public
         * @chainable
         * @method trigger
         * @param {String} eventName
         * @returns {EventEmitter}
         */
        trigger: function (eventName) {
            var me = this,
                events = me._events || (me._events = {}),
                eventList = events[eventName],
                event,
                args,
                a1, a2, a3,
                len, i;

            if (!eventList) {
                return me;
            }

            args = Array.prototype.slice.call(arguments, 1);
            len = eventList.length;
            i = -1;

            if (args.length <= 3) {
                a1 = args[0];
                a2 = args[1];
                a3 = args[2];
            }

            /**
             * Using switch to improve the performance of listener calls
             * .call() has a much greater performance than .apply() on
             * many parameters.
             */
            switch (args.length) {
                case 0:
                    while (++i < len) (event = eventList[i]).callback.call(event.context);
                    return me;
                case 1:
                    while (++i < len) (event = eventList[i]).callback.call(event.context, a1);
                    return me;
                case 2:
                    while (++i < len) (event = eventList[i]).callback.call(event.context, a1, a2);
                    return me;
                case 3:
                    while (++i < len) (event = eventList[i]).callback.call(event.context, a1, a2, a3);
                    return me;
                default:
                    while (++i < len) (event = eventList[i]).callback.apply(event.context, args);
                    return me;
            }
        },

        /**
         * @public
         * @method destroy
         */
        destroy: function () {
            this.off();
        }
    };

    /**
     * @public
     * @static
     * @class StateManager
     * @extends {EventEmitter}
     * @type {Object}
     */
    window.StateManager = $.extend(Object.create(EventEmitter.prototype), {

        /**
         * Collection of all registered breakpoints
         *
         * @private
         * @property _breakpoints
         * @type {Array}
         */
        _breakpoints: [],

        /**
         * Collection of all registered listeners
         *
         * @private
         * @property _listeners
         * @type {Array}
         */
        _listeners: [],

        /**
         * Collection of all added plugin configurations
         *
         * @private
         * @property _plugins
         * @type {Object}
         */
        _plugins: {},

        /**
         * Collection of all plugins that should be initialized when the DOM is ready
         *
         * @private
         * @property _pluginQueue
         * @type {Object}
         */
        _pluginQueue: {},

        /**
         * Flag whether the queued plugins were initialized or not
         *
         * @private
         * @property _pluginsInitialized
         * @type {Boolean}
         */
        _pluginsInitialized: false,

        /**
         * Current breakpoint type
         *
         * @private
         * @property _currentState
         * @type {String}
         */
        _currentState: '',

        /**
         * Previous breakpoint type
         *
         * @private
         * @property _previousState
         * @type {String}
         */
        _previousState: '',

        /**
         * Last calculated viewport width.
         *
         * @private
         * @property _viewportWidth
         * @type {Number}
         */
        _viewportWidth: 0,

        /**
         * Cache for all previous gathered vendor properties.
         *
         * @private
         * @property _vendorPropertyCache
         * @type {Object}
         */
        _vendorPropertyCache: {},

        /**
         * Initializes the StateManager with the incoming breakpoint
         * declaration and starts the listing of the resize of the browser window.
         *
         * @public
         * @chainable
         * @method init
         * @param {Object|Array} breakpoints - User defined breakpoints.
         * @returns {StateManager}
         */
        init: function (breakpoints) {
            var me = this;

            me._viewportWidth = me.getViewportWidth();

            me._baseFontSize = parseInt($html.css('font-size'));

            me.registerBreakpoint(breakpoints);

            me._checkResize();
            me._browserDetection();
            me._setDeviceCookie();

            $($.proxy(me.initQueuedPlugins, me, true));

            return me;
        },

        /**
         * Adds a breakpoint to check against, after the {@link StateManager.init} was called.
         *
         * @public
         * @chainable
         * @method registerBreakpoint
         * @param {Array|Object} breakpoint.
         * @returns {StateManager}
         */
        registerBreakpoint: function (breakpoint) {
            var me = this,
                breakpoints = breakpoint instanceof Array ? breakpoint : Array.prototype.slice.call(arguments),
                len = breakpoints.length,
                i = 0;

            for (; i < len; i++) {
                me._addBreakpoint(breakpoints[i]);
            }

            return me;
        },

        /**
         * Adds a breakpoint to check against, after the {@link StateManager.init} was called.
         *
         * @private
         * @chainable
         * @method _addBreakpoint
         * @param {Object} breakpoint.
         */
        _addBreakpoint: function (breakpoint) {
            var me = this,
                breakpoints = me._breakpoints,
                existingBreakpoint,
                state = breakpoint.state,
                enter = me._convertRemValue(breakpoint.enter),
                exit = me._convertRemValue(breakpoint.exit),
                len = breakpoints.length,
                i = 0;

            breakpoint.enter = enter;
            breakpoint.exit = exit;

            for (; i < len; i++) {
                existingBreakpoint = breakpoints[i];

                if (existingBreakpoint.state === state) {
                    throw new Error('Multiple breakpoints of state "' + state + '" detected.');
                }

                if (existingBreakpoint.enter <= exit && enter <= existingBreakpoint.exit) {
                    throw new Error('Breakpoint range of state "' + state + '" overlaps state "' + existingBreakpoint.state + '".');
                }
            }

            breakpoints.push(breakpoint);

            me._plugins[state] = {};
            me._checkBreakpoint(breakpoint, me._viewportWidth);

            return me;
        },

        _convertRemValue: function(remValue) {
            var me = this,
                baseFontSize = me._baseFontSize;

            return remValue * baseFontSize;
        },

        /**
         * Removes breakpoint by state and removes the generated getter method for the state.
         *
         * @public
         * @chainable
         * @method removeBreakpoint
         * @param {String} state State which should be removed
         * @returns {StateManager}
         */
        removeBreakpoint: function (state) {
            var me = this,
                breakpoints = me._breakpoints,
                len = breakpoints.length,
                i = 0;

            if (typeof state !== 'string') {
                return me;
            }

            for (; i < len; i++) {
                if (state !== breakpoints[i].state) {
                    continue;
                }

                breakpoints.splice(i, 1);

                return me._removeStatePlugins(state);
            }

            return me;
        },

        /**
         * @protected
         * @chainable
         * @method _removeStatePlugins
         * @param {String} state
         * @returns {StateManager}
         */
        _removeStatePlugins: function (state) {
            var me = this,
                plugins = me._plugins[state],
                selectors = Object.keys(plugins),
                selectorLen = selectors.length,
                pluginNames,
                pluginLen,
                i, j;

            for (i = 0; i < selectorLen; i++) {
                pluginNames = Object.keys(plugins[selectors[i]]);

                for (j = 0, pluginLen = pluginNames.length; j < pluginLen; j++) {
                    me.destroyPlugin(selectors[i], pluginNames[j]);
                }
            }

            delete plugins[state];

            return me;
        },

        /**
         * Registers one or multiple event listeners to the StateManager,
         * so they will be fired when the state matches the current active
         * state..
         *
         * @public
         * @chainable
         * @method registerListener
         * @param {Object|Array} listener
         * @returns {StateManager}
         */
        registerListener: function (listener) {
            var me = this,
                listenerArr = listener instanceof Array ? listener : Array.prototype.slice.call(arguments),
                len = listenerArr.length,
                i = 0;

            for (; i < len; i++) {
                me._addListener(listenerArr[i]);
            }

            return me;
        },

        /**
         * @private
         * @chainable
         * @method _addListener
         * @param {Object} listener.
         */
        _addListener: function (listener) {
            var me = this,
                listeners = me._listeners,
                enterFn = listener.enter;

            listeners.push(listener);

            if ((listener.state === me._currentState || listener.state === '*') && typeof enterFn === 'function') {
                enterFn({
                    'exiting': me._previousState,
                    'entering': me._currentState
                });
            }

            return me;
        },

        /**
         * @public
         * @chainable
         * @method addPlugin
         * @param {String} selector
         * @param {String} pluginName
         * @param {Object|Array|String} config
         * @param {Array|String} viewport
         * @returns {StateManager}
         */
        addPlugin: function (selector, pluginName, config, viewport) {
            var me = this,
                pluginsInitialized = me._pluginsInitialized,
                breakpoints = me._breakpoints,
                currentState = me._currentState,
                len,
                i;

            // If the third parameter are the viewport states
            if (typeof config === 'string' || config instanceof Array) {
                viewport = config;
                config = {};
            }

            if (typeof viewport === 'string') {
                viewport = [viewport];
            }

            if (!(viewport instanceof Array)) {
                viewport = [];

                for (i = 0, len = breakpoints.length; i < len; i++) {
                    viewport.push(breakpoints[i].state);
                }
            }

            for (i = 0, len = viewport.length; i < len; i++) {
                me._addPluginOption(viewport[i], selector, pluginName, config);

                if (currentState !== viewport[i]) {
                    continue;
                }

                if (pluginsInitialized) {
                    me._initPlugin(selector, pluginName);
                    continue;
                }

                me.addPluginToQueue(selector, pluginName);
            }

            return me;
        },

        /**
         * @public
         * @chainable
         * @method removePlugin
         * @param {String} selector
         * @param {String} pluginName
         * @param {Array|String} viewport
         * @returns {StateManager}
         */
        removePlugin: function (selector, pluginName, viewport) {
            var me = this,
                breakpoints = me._breakpoints,
                plugins = me._plugins,
                state,
                sel,
                len,
                i;

            if (typeof viewport === 'string') {
                viewport = [viewport];
            }

            if (!(viewport instanceof Array)) {
                viewport = [];

                for (i = 0, len = breakpoints.length; i < len; i++) {
                    viewport.push(breakpoints[i].state);
                }
            }

            for (i = 0, len = viewport.length; i < len; i++) {
                if (!(state = plugins[viewport[i]])) {
                    continue;
                }

                if (!(sel = state[selector])) {
                    continue;
                }

                delete sel[pluginName];
            }

            if (!me._pluginsInitialized) {
                me.removePluginFromQueue(selector, pluginName);
            }

            return me;
        },

        /**
         * @public
         * @chainable
         * @method updatePlugin
         * @param {String} selector
         * @param {String} pluginName
         * @returns {StateManager}
         */
        updatePlugin: function (selector, pluginName) {
            var me = this,
                state = me._currentState,
                pluginConfigs = me._plugins[state][selector] || {},
                pluginNames = (typeof pluginName === 'string') ? [pluginName] : Object.keys(pluginConfigs),
                len = pluginNames.length,
                i = 0;

            for (; i < len; i++) {
                me._initPlugin(selector, pluginNames[i]);
            }

            return me;
        },

        /**
         * @private
         * @method _addPluginOption
         * @param {String} state
         * @param {String} selector
         * @param {String} pluginName
         * @param {Object} config
         */
        _addPluginOption: function (state, selector, pluginName, config) {
            var me = this,
                plugins = me._plugins,
                selectors = plugins[state] || (plugins[state] = {}),
                configs = selectors[selector] || (selectors[selector] = {}),
                pluginConfig = configs[pluginName];

            configs[pluginName] = $.extend(pluginConfig || {}, config);
        },

        /**
         * @private
         * @method _initPlugin
         * @param {String} selector
         * @param {String} pluginName
         */
        _initPlugin: function (selector, pluginName) {
            var me = this,
                $el = $(selector);

            if ($el.length > 1) {
                $.each($el, function () {
                    me._initSinglePlugin($(this), selector, pluginName);
                });
                return;
            }

            me._initSinglePlugin($el, selector, pluginName);
        },

        /**
         * @public
         * @method addPluginToQueue
         * @param {String} selector
         * @param {String} pluginName
         */
        addPluginToQueue: function (selector, pluginName) {
            var me = this,
                queue = me._pluginQueue,
                pluginNames = queue[selector] || (queue[selector] = []);

            if (pluginNames.indexOf(pluginName) === -1) {
                pluginNames.push(pluginName);
            }
        },

        /**
         * @public
         * @method removePluginFromQueue
         * @param {String} selector
         * @param {String} pluginName
         */
        removePluginFromQueue: function (selector, pluginName) {
            var me = this,
                queue = me._pluginQueue,
                pluginNames = queue[selector],
                index;

            if (pluginNames && (index = pluginNames.indexOf(pluginName)) !== -1) {
                pluginNames.splice(index, 1);
            }
        },

        /**
         * @public
         * @method initQueuedPlugins
         * @param {Boolean} clearQueue
         */
        initQueuedPlugins: function (clearQueue) {
            var me = this,
                queue = me._pluginQueue,
                selectors = Object.keys(queue),
                selectorLen = selectors.length,
                i = 0,
                selector,
                plugins,
                pluginLen,
                j;

            for (; i < selectorLen; i++) {
                selector = selectors[i];
                plugins = queue[selector];

                for (j = 0, pluginLen = plugins.length; j < pluginLen; j++) {
                    me._initPlugin(selector, plugins[j]);
                }

                if (clearQueue !== false) {
                    delete queue[selector];
                }
            }

            me._pluginsInitialized = true;
        },

        /**
         * @private
         * @method _initSinglePlugin
         * @param {Object} element
         * @param {String} selector
         * @param {String} pluginName
         */
        _initSinglePlugin: function (element, selector, pluginName) {
            var me = this,
                currentConfig = me._getPluginConfig(me._currentState, selector, pluginName),
                plugin = element.data('plugin_' + pluginName);

            if (!plugin) {
                if (!element[pluginName]) {
                    console.error('Plugin "' + pluginName + '" is not a valid jQuery-plugin!');
                    return;
                }

                element[pluginName](currentConfig);
                return;
            }

            if (JSON.stringify(currentConfig) === JSON.stringify(me._getPluginConfig(me._previousState, selector, pluginName))) {
                if (typeof plugin.update === 'function') {
                    plugin.update.call(plugin, me._currentState, me._previousState);
                }
                return;
            }

            me.destroyPlugin(element, pluginName);

            element[pluginName](currentConfig);
        },

        /**
         * @private
         * @method _getPluginConfig
         * @param {String} state
         * @param {String} selector
         * @param {String} plugin
         */
        _getPluginConfig: function (state, selector, plugin) {
            var selectors = this._plugins[state] || {},
                pluginConfigs = selectors[selector] || {};

            return pluginConfigs[plugin] || {};
        },

        /**
         * @private
         * @method _checkResize
         */
        _checkResize: function () {
            var me = this,
                width = me.getViewportWidth();

            if (width !== me._viewportWidth) {
                me._checkBreakpoints(width);
                me.trigger('resize', width);
                me._setDeviceCookie();
            }

            me._viewportWidth = width;

            me.requestAnimationFrame(me._checkResize.bind(me));
        },

        /**
         * @private
         * @method _checkBreakpoints
         * @param {Number} width
         */
        _checkBreakpoints: function (width) {
            var me = this,
                checkWidth = width || me.getViewportWidth(),
                breakpoints = me._breakpoints,
                len = breakpoints.length,
                i = 0;

            for (; i < len; i++) {
                me._checkBreakpoint(breakpoints[i], checkWidth);
            }

            return me;
        },

        /**
         * @private
         * @method _checkBreakpoint
         * @param {Object} breakpoint
         * @param {Number} width
         */
        _checkBreakpoint: function (breakpoint, width) {
            var me = this,
                checkWidth = width || me.getViewportWidth(),
                enterWidth = ~~(breakpoint.enter),
                exitWidth = ~~(breakpoint.exit),
                state = breakpoint.state;

            if (state !== me._currentState && checkWidth >= enterWidth && checkWidth <= exitWidth) {
                me._changeBreakpoint(state);
            }
        },

        /**
         * @private
         * @chainable
         * @method _changeBreakpoint
         * @param {String} state
         * @returns {StateManager}
         */
        _changeBreakpoint: function (state) {
            var me = this,
                previousState = me._previousState = me._currentState,
                currentState = me._currentState = state;

            return me
                .trigger('exitBreakpoint', previousState)
                .trigger('changeBreakpoint', {
                    'entering': currentState,
                    'exiting': previousState
                })
                .trigger('enterBreakpoint', currentState)
                ._switchListener(previousState, currentState)
                ._switchPlugins(previousState, currentState);
        },

        /**
         * @private
         * @chainable
         * @method _switchListener
         * @param {String} fromState
         * @param {String} toState
         * @returns {StateManager}
         */
        _switchListener: function (fromState, toState) {
            var me = this,
                previousListeners = me._getBreakpointListeners(fromState),
                currentListeners = me._getBreakpointListeners(toState),
                eventObj = {
                    'exiting': fromState,
                    'entering': toState
                },
                callFn,
                len,
                i;

            for (i = 0, len = previousListeners.length; i < len; i++) {
                if (typeof (callFn = previousListeners[i].exit) === 'function') {
                    callFn(eventObj);
                }
            }

            for (i = 0, len = currentListeners.length; i < len; i++) {
                if (typeof (callFn = currentListeners[i].enter) === 'function') {
                    callFn(eventObj);
                }
            }

            return me;
        },

        /**
         * @private
         * @method _getBreakpointListeners
         * @param {String} state
         * @returns {Array}
         */
        _getBreakpointListeners: function (state) {
            var me = this,
                listeners = me._listeners,
                breakpointListeners = [],
                len = listeners.length,
                i = 0,
                listenerType;

            for (; i < len; i++) {
                if ((listenerType = listeners[i].state) === state || listenerType === '*') {
                    breakpointListeners.push(listeners[i]);
                }
            }

            return breakpointListeners;
        },

        /**
         * @private
         * @chainable
         * @method _switchPlugins
         * @param {String} fromState
         * @param {String} toState
         * @returns {StateManager}
         */
        _switchPlugins: function (fromState, toState) {
            var me = this,
                plugins = me._plugins,
                fromSelectors = plugins[fromState] || {},
                fromKeys = Object.keys(fromSelectors),
                selector,
                oldPluginConfigs,
                newPluginConfigs,
                configKeys,
                pluginName,
                pluginConfig,
                plugin,
                $el,
                toSelectors = plugins[toState] || {},
                toKeys = Object.keys(toSelectors),
                lenKeys, lenConfig, lenEl,
                x, y, z;

            for (x = 0, lenKeys = fromKeys.length; x < lenKeys; x++) {
                selector = fromKeys[x];
                oldPluginConfigs = fromSelectors[selector];
                $el = $(selector);

                if (!oldPluginConfigs || !(lenEl = $el.length)) {
                    continue;
                }

                newPluginConfigs = toSelectors[selector];
                configKeys = Object.keys(oldPluginConfigs);

                for (y = 0, lenConfig = configKeys.length; y < lenConfig; y++) {
                    pluginName = configKeys[y];

                    // When no new state config is available, destroy the old plugin
                    if (!newPluginConfigs || !(pluginConfig = newPluginConfigs[pluginName])) {
                        me.destroyPlugin($el, pluginName);
                        continue;
                    }

                    if (JSON.stringify(newPluginConfigs[pluginName]) === JSON.stringify(oldPluginConfigs[pluginName])) {
                        for (z = 0; z < lenEl; z++) {
                            if (!(plugin = $($el[z]).data('plugin_' + pluginName))) {
                                continue;
                            }

                            if (typeof plugin.update === 'function') {
                                plugin.update.call(plugin, fromState, toState);
                            }
                        }
                        continue;
                    }

                    me.destroyPlugin($el, pluginName);
                }
            }

            for (x = 0, lenKeys = toKeys.length; x < lenKeys; x++) {
                selector = toKeys[x];
                newPluginConfigs = toSelectors[selector];
                $el = $(selector);

                if (!newPluginConfigs || !$el.length) {
                    continue;
                }

                configKeys = Object.keys(newPluginConfigs);

                for (y = 0, lenConfig = configKeys.length; y < lenConfig; y++) {
                    pluginName = configKeys[y];

                    if (!$el.data('plugin_' + pluginName)) {
                        $el[pluginName](newPluginConfigs[pluginName]);
                    }
                }
            }

            return me;
        },

        /**
         * @public
         * @method destroyPlugin
         * @param {String|jQuery} selector
         * @param {String} pluginName
         */
        destroyPlugin: function (selector, pluginName) {
            var $el = (typeof selector === 'string') ? $(selector) : selector,
                name = 'plugin_' + pluginName,
                len = $el.length,
                i = 0,
                $currentEl,
                plugin;

            if (!len) {
                return;
            }

            for (; i < len; i++) {
                $currentEl = $($el[i]);

                if ((plugin = $currentEl.data(name))) {
                    plugin.destroy();
                    $currentEl.removeData(name);
                }
            }
        },

        /**
         * Returns the current viewport width.
         *
         * @public
         * @method getViewportWidth
         * @returns {Number} The width of the viewport in pixels.
         */
        getViewportWidth: function () {
            var width = window.innerWidth;

            if (typeof width === 'number') {
                return width;
            }

            return (width = document.documentElement.clientWidth) !== 0 ? width : document.body.clientWidth;
        },

        /**
         * Returns the current viewport height.
         *
         * @public
         * @method getViewportHeight
         * @returns {Number} The height of the viewport in pixels.
         */
        getViewportHeight: function () {
            var height = window.innerHeight;

            if (typeof height === 'number') {
                return height;
            }

            return (height = document.documentElement.clientHeight) !== 0 ? height : document.body.clientHeight;
        },

        /**
         * Returns the current active state.
         *
         * @public
         * @method getPrevious
         * @returns {String} previous breakpoint state
         */
        getPreviousState: function () {
            return this._previousState;
        },

        /**
         * Returns whether or not the previous active type is the passed one.
         *
         * @public
         * @method getPrevious
         * @param {String|Array} state
         * @returns {Boolean}
         */
        isPreviousState: function (state) {
            var states = state instanceof Array ? state : Array.prototype.slice.call(arguments),
                previousState = this._previousState,
                len = states.length,
                i = 0;

            for (; i < len; i++) {
                if (previousState === states[i]) {
                    return true;
                }
            }

            return false;
        },

        /**
         * Returns the current active state.
         *
         * @public
         * @method getCurrent
         * @returns {String} current breakpoint state
         */
        getCurrentState: function () {
            return this._currentState;
        },

        /**
         * Returns whether or not the current active state is the passed one.
         *
         * @public
         * @method isCurrent
         * @param {String | Array} state
         * @returns {Boolean}
         */
        isCurrentState: function (state) {
            var states = state instanceof Array ? state : Array.prototype.slice.call(arguments),
                currentState = this._currentState,
                len = states.length,
                i = 0;

            for (; i < len; i++) {
                if (currentState === states[i]) {
                    return true;
                }
            }

            return false;
        },

        /**
         * Checks if the device is currently running in portrait mode.
         *
         * @public
         * @method isPortraitMode
         * @returns {Boolean} Whether or not the device is in portrait mode
         */
        isPortraitMode: function () {
            return !!this.matchMedia('(orientation: portrait)').matches;
        },

        /**
         * Checks if the device is currently running in landscape mode.
         *
         * @public
         * @method isLandscapeMode
         * @returns {Boolean} Whether or not the device is in landscape mode
         */
        isLandscapeMode: function () {
            return !!this.matchMedia('(orientation: landscape)').matches;
        },

        /**
         * Gets the device pixel ratio. All retina displays should return a value > 1, all standard
         * displays like a desktop monitor will return 1.
         *
         * @public
         * @method getDevicePixelRatio
         * @returns {Number} The device pixel ratio.
         */
        getDevicePixelRatio: function () {
            return window.devicePixelRatio || 1;
        },

        /**
         * Returns if the current user agent is matching the browser test.
         *
         * @param browser
         * @returns {boolean}
         */
        isBrowser: function(browser) {
            var regEx = new RegExp(browser.toLowerCase(), 'i');
            return this._checkUserAgent(regEx);
        },

        /**
         * Checks the user agent against the given regexp.
         *
         * @param regEx
         * @returns {boolean}
         * @private
         */
        _checkUserAgent: function(regEx) {
            return !!navigator.userAgent.toLowerCase().match(regEx);
        },

        /**
         * Detects the browser type and adds specific css classes to the html tag.
         *
         * @private
         */
        _browserDetection: function() {
            var me = this,
                detections = {};

            detections['is--opera']     = me._checkUserAgent(/opera/);
            detections['is--chrome']    = me._checkUserAgent(/\bchrome\b/);
            detections['is--firefox']   = me._checkUserAgent(/firefox/);
            detections['is--webkit']    = me._checkUserAgent(/webkit/);
            detections['is--safari']    = !detections['is--chrome'] && me._checkUserAgent(/safari/);
            detections['is--ie']        = !detections['is--opera'] && (me._checkUserAgent(/msie/) || me._checkUserAgent(/trident\/7/));
            detections['is--ie-touch']  = detections['is--ie'] && me._checkUserAgent(/touch/);
            detections['is--gecko']     = !detections['is--webkit'] && me._checkUserAgent(/gecko/);

            $.each(detections, function(key, value) {
                if (value) $html.addClass(key);
            });
        },

        _getCurrentDevice: function() {
            var me = this,
                devices = {
                    'xs': 'mobile',
                    's' : 'mobile',
                    'm' : 'tablet',
                    'l' : 'tablet',
                    'xl': 'desktop'
                };

            return devices[me.getCurrentState()] || 'desktop';
        },

        _setDeviceCookie: function() {
            var me = this,
                device = me._getCurrentDevice();

            document.cookie = 'x-ua-device=' + device + '; path=/';
        },

        /**
         * First calculates the scroll bar width and height of the browser
         * and saves it to a object that can be accessed.
         *
         * @private
         * @property _scrollBarSize
         * @type {Object}
         */
        _scrollBarSize: (function () {
            var $el = $('<div>', {
                    css: {
                        width: 100,
                        height: 100,
                        overflow: 'scroll',
                        position: 'absolute',
                        top: -9999
                    }
                }),
                el = $el[0],
                width,
                height;

            $('body').append($el);

            width = el.offsetWidth - el.clientWidth;
            height = el.offsetHeight - el.clientHeight;

            $($el).remove();

            return {
                width: width,
                height: height
            };
        }()),

        /**
         * Returns an object containing the width and height of the default
         * scroll bar sizes.
         *
         * @public
         * @method getScrollBarSize
         * @returns {Object} The width/height pair of the scroll bar size.
         */
        getScrollBarSize: function () {
            return $.extend({}, this._scrollBarSize);
        },

        /**
         * Returns the default scroll bar width of the browser.
         *
         * @public
         * @method getScrollBarWidth
         * @returns {Number} Width of the default browser scroll bar.
         */
        getScrollBarWidth: function () {
            return this._scrollBarSize.width;
        },

        /**
         * Returns the default scroll bar width of the browser.
         *
         * @public
         * @method getScrollBarHeight
         * @returns {Number} Height of the default browser scroll bar.
         */
        getScrollBarHeight: function () {
            return this._scrollBarSize.height;
        },

        /**
         * matchMedia() polyfill
         * Test a CSS media type/query in JS.
         * Authors & copyright (c) 2012: Scott Jehl, Paul Irish, Nicholas Zakas, David Knight.
         * Dual MIT/BSD license
         *
         * @public
         * @method matchMedia
         * @param {String} media
         */
        matchMedia: (function () {
            // For browsers that support matchMedium api such as IE 9 and webkit
            var styleMedia = (window.styleMedia || window.media);

            // For those that don't support matchMedium
            if (!styleMedia) {
                var style = document.createElement('style'),
                    script = document.getElementsByTagName('script')[0],
                    info = null;

                style.type = 'text/css';
                style.id = 'matchmediajs-test';

                script.parentNode.insertBefore(style, script);

                // 'style.currentStyle' is used by IE <= 8 and 'window.getComputedStyle' for all other browsers
                info = ('getComputedStyle' in window) && window.getComputedStyle(style, null) || style.currentStyle;

                styleMedia = {
                    matchMedium: function (media) {
                        var text = '@media ' + media + '{ #matchmediajs-test { width: 1px; } }';

                        // 'style.styleSheet' is used by IE <= 8 and 'style.textContent' for all other browsers
                        if (style.styleSheet) {
                            style.styleSheet.cssText = text;
                        } else {
                            style.textContent = text;
                        }

                        // Test if media query is true or false
                        return info.width === '1px';
                    }
                };
            }

            return function (media) {
                return {
                    matches: styleMedia.matchMedium(media || 'all'),
                    media: media || 'all'
                };
            };
        }()),

        /**
         * requestAnimationFrame() polyfill
         *
         * @public
         * @method requestAnimationFrame
         * @param {Function} callback
         * @returns {Number}
         */
        requestAnimationFrame: (function () {
            var raf = window.requestAnimationFrame,
                i = vendorPrefixes.length,
                lastTime = 0;

            while (!raf && i) {
                raf = window[vendorPrefixes[i--] + 'RequestAnimationFrame'];
            }

            return raf || function (callback) {
                var currTime = +(new Date()),
                    timeToCall = Math.max(0, 16 - (currTime - lastTime)),
                    id = window.setTimeout(function () {
                        callback(currTime + timeToCall);
                    }, timeToCall);

                lastTime = currTime + timeToCall;

                return id;
            };
        }()).bind(window),

        /**
         * cancelAnimationFrame() polyfill
         *
         * @public
         * @method cancelAnimationFrame
         * @param {Number} id
         */
        cancelAnimationFrame: (function () {
            var caf = window.cancelAnimationFrame,
                i = vendorPrefixes.length,
                fnName;

            while (!caf && i) {
                fnName = vendorPrefixes[i--];
                caf = window[fnName + 'CancelAnimationFrame'] || window[fnName + 'CancelRequestAnimationFrame'];
            }

            return caf || window.clearTimeout;
        }()).bind(window),

        /**
         * Tests the given CSS style property on an empty div with all vendor
         * properties. If it fails and the softError flag was not set, it
         * returns null, otherwise the given property.
         *
         * @example
         *
         * // New chrome version
         * StateManager.getVendorProperty('transform'); => 'transform'
         *
         * // IE9
         * StateManager.getVendorProperty('transform'); => 'msTransform'
         *
         * // Property not supported, without soft error flag
         * StateManager.getVendorProperty('animation'); => null
         *
         * // Property not supported, with soft error flag
         * StateManager.getVendorProperty('animation', true); => 'animate'
         *
         * @public
         * @method getVendorProperty
         * @param {String} property
         * @param {Boolean} softError
         */
        getVendorProperty: function (property, softError) {
            var cache = this._vendorPropertyCache,
                style = vendorPropertyDiv.style;

            if (cache[property]) {
                return cache[property];
            }

            if (property in style) {
                return (cache[property] = property);
            }

            var prop = property.charAt(0).toUpperCase() + property.substr(1),
                len = vendorPrefixes.length,
                i = 0,
                vendorProp;

            for (; i < len; i++) {
                vendorProp = vendorPrefixes[i] + prop;

                if (vendorProp in style) {
                    return (cache[property] = vendorProp);
                }
            }

            return (cache[property] = (softError ? property : null));
        }
    });

})(jQuery, window, document);

;(function (window, document) {
    'use strict';

    /**
     * Global storage manager
     *
     * The storage manager provides a unified way to store items in the localStorage and sessionStorage.
     * It uses a polyfill that uses cookies as a fallback when no localStorage or sessionStore is available or working.
     *
     * @example
     *
     * Saving an item to localStorage:
     *
     * StorageManager.setItem('local', 'key', 'value');
     *
     * Retrieving it:
     *
     * var item = StorageManager.getItem('local', 'key'); // item === 'value'
     *
     * Basically you can use every method of the Storage interface (http://www.w3.org/TR/webstorage/#the-storage-interface)
     * But notice that you have to pass the storage type ('local' | 'session') in the first parameter for every call.
     *
     * @example
     *
     * Getting the localStorage/sessionStorage (polyfill) object
     *
     * var localStorage = StorageManager.getStorage('local');
     * var sessionStorage = StorageManager.getStorage('session');
     *
     * You can also use its shorthands:
     *
     * var localStorage = StorageManager.getLocalStorage();
     * var sessionStorage = StorageManager.getSessionStorage();
     */
    window.StorageManager = (function () {

        /**
         * The polyfill for localStorage and sessionStorage.
         * Uses cookies for storing items.
         *
         * @class StoragePolyFill
         * @constructor
         * @param {String} type
         * @returns {Object}
         */
        function StoragePolyFill(type) {
            /**
             * Creates a cookie with a given name, its values as a string (e.g. JSON) and expiration in days
             *
             * @param {String} name
             * @param {String} value
             * @param {Number} days
             */
            function createCookie(name, value, days) {
                var date,
                    expires = '';

                if (days) {
                    date = new Date();
                    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                    expires = '; expires=' + date.toGMTString();
                }

                value = encodeURI(value);

                document.cookie = name + '=' + value + expires + '; path=/';
            }

            /**
             * Searches for a cookie by the given name and returns its values.
             *
             * @param name
             * @returns {String|null}
             */
            function readCookie(name) {
                var nameEq = name + '=',
                    cookies = document.cookie.split(';'),
                    cookie,
                    len = cookies.length,
                    i = 0;

                for (; i < len; i++) {
                    cookie = cookies[i];

                    while (cookie.charAt(0) == ' ') {
                        cookie = cookie.substring(1, cookie.length);
                    }

                    if (cookie.indexOf(nameEq) == 0) {
                        return decodeURI(cookie.substring(nameEq.length, cookie.length));
                    }
                }
                return null;
            }

            /**
             * Turns the passed data object into a string via JSON.stringify() and sets it into its proper cookie.
             *
             * @param {Object} data
             */
            function setData(data) {
                data = JSON.stringify(data);
                if (type == 'session') {
                    createCookie('sessionStorage', data, 0);
                } else {
                    createCookie('localStorage', data, 365);
                }
            }

            /**
             * clears the whole data set of a storage cookie.
             */
            function clearData() {
                if (type == 'session') {
                    createCookie('sessionStorage', '', 0);
                } else {
                    createCookie('localStorage', '', 365);
                }
            }

            /**
             * Returns the data set of a storage cookie.
             *
             * @returns {Object}
             */
            function getData() {
                var data = (type == 'session') ? readCookie('sessionStorage') : readCookie('localStorage');

                return data ? JSON.parse(data) : { };
            }

            var data = getData();

            /**
             * Returns an object to expose public functions and hides privates.
             */
            return {
                /**
                 * data set length.
                 *
                 * @public
                 * @property length
                 * @type {Number}
                 */
                length: 0,

                /**
                 * Clears the whole data set.
                 *
                 * @public
                 * @method clear
                 */
                clear: function () {
                    var me = this,
                        p;

                    for(p in data) {
                        if (!data.hasOwnProperty(p)) {
                            continue;
                        }
                        delete data[p];
                    }

                    me.length = 0;

                    clearData();
                },

                /**
                 * Returns the data item by the given key or null if the item was not found.
                 *
                 * @param key
                 * @returns {String|null}
                 */
                getItem: function (key) {
                    return typeof data[key] === 'undefined' ? null : data[key];
                },

                /**
                 * Returns the data item key of the given index.
                 *
                 * @param {Number} index
                 * @returns {String}
                 */
                key: function (index) {
                    var i = 0,
                        p;

                    for (p in data) {
                        if (!data.hasOwnProperty(p)) {
                            continue;
                        }

                        if (i === index) {
                            return p;
                        }

                        i++;
                    }

                    return null;
                },

                /**
                 * Removes an item by the given key.
                 *
                 * @param {String} key
                 */
                removeItem: function (key) {
                    var me = this;

                    if (data.hasOwnProperty(key)) {
                        me.length--;
                    }

                    delete data[key];

                    setData(data);
                },

                /**
                 * Sets the value of a storage item.
                 *
                 * @param {String} key
                 * @param {String} value
                 */
                setItem: function (key, value) {
                    var me = this;

                    if (!data.hasOwnProperty(key)) {
                        me.length++;
                    }

                    data[key] = value + ''; // forces the value to a string

                    setData(data);
                }
            };
        }

        /**
         * Helper function to detect if cookies are enabled.
         * @returns {boolean}
         */
        function hasCookiesSupport() {
            return ('cookie' in document && (document.cookie.length > 0 ||
            (document.cookie = 'test').indexOf.call(document.cookie, 'test') > -1));
        }

        /**
         * Helper function to detect if localStorage is enabled.
         * @returns {boolean}
         */
        function hasLocalStorageSupport() {
            try {
                return (typeof window.localStorage !== 'undefined');
            }
            catch (err) {
                return false;
            }
        }

        /**
         * Helper function to detect if sessionStorage is enabled.
         * @returns {boolean}
         */
        function hasSessionStorageSupport() {
            try {
                return (typeof window.sessionStorage !== 'undefined');
            }
            catch (err) {
                return false;
            }
        }

        var localStorageSupport = hasLocalStorageSupport(),
            sessionStorageSupport = hasSessionStorageSupport(),
            storage = {
                local: localStorageSupport ? window.localStorage : new StoragePolyFill('local'),
                session: sessionStorageSupport ? window.sessionStorage : new StoragePolyFill('session')
            },
            p;

        // test for safari's "QUOTA_EXCEEDED_ERR: DOM Exception 22" issue.
        for (p in storage) {
            if (!storage.hasOwnProperty(p)) {
                continue;
            }

            try {
                storage[p].setItem('storage', '');
                storage[p].removeItem('storage');
            }
            catch (err) {
                storage[p] = new StoragePolyFill(p);
            }
        }

        // Just return the public API instead of all available functions
        return {
            /**
             * Returns the storage object/polyfill of the given type.
             *
             * @returns {Storage|StoragePolyFill}
             */
            getStorage: function (type) {
                return storage[type];
            },

            /**
             * Returns the sessionStorage object/polyfill.
             *
             * @returns {Storage|StoragePolyFill}
             */
            getSessionStorage: function () {
                return this.getStorage('session');
            },

            /**
             * Returns the localStorage object/polyfill.
             *
             * @returns {Storage|StoragePolyFill}
             */
            getLocalStorage: function () {
                return this.getStorage('local');
            },

            /**
             * Calls the clear() method of the storage from the given type.
             *
             * @param {String} type
             */
            clear: function (type) {
                this.getStorage(type).clear();
            },

            /**
             * Calls the getItem() method of the storage from the given type.
             *
             * @param {String} type
             * @param {String} key
             * @returns {String}
             */
            getItem: function (type, key) {
                return this.getStorage(type).getItem(key);
            },

            /**
             * Calls the key() method of the storage from the given type.
             *
             * @param {String} type
             * @param {Number|String} i
             * @returns {String}
             */
            key: function (type, i) {
                return this.getStorage(type).key(i);
            },

            /**
             * Calls the removeItem() method of the storage from the given type.
             *
             * @param {String} type
             * @param {String} key
             */
            removeItem: function (type, key) {
                this.getStorage(type).removeItem(key);
            },

            /**
             * Calls the setItem() method of the storage from the given type.
             *
             * @param {String} type
             * @param {String} key
             * @param {String} value
             */
            setItem: function (type, key, value) {
                this.getStorage(type).setItem(key, value);
            },

            /**
             * Helper function call to check if cookies are enabled.
             */
            hasCookiesSupport: hasCookiesSupport(),

            /**
             * Helper function call to check if localStorage is enabled.
             */
            hasLocalStorageSupport: hasLocalStorageSupport(),

            /**
             * Helper function call to check if sessionStorage is enabled.
             */
            hasSessionStorageSupport: hasSessionStorageSupport()
        };
    })();
})(window, document);
;(function ($) {
    'use strict';

    var $html = $('html');

    /**
     * Off canvas menu plugin
     *
     * The plugin provides an lightweight way to use an off canvas pattern for all kind of content. The content
     * needs to be positioned off canvas using CSS3 `transform`. All the rest will be handled by the plugin.
     *
     * @example Simple usage
     * ```
     *     <a href="#" data-offcanvas="true">Menu</a>
     * ```
     *
     * @example Show the menu on the right side
     * ```
     *     <a href="#" data-offcanvas="true" data-direction="fromRight">Menu</a>
     * ```
     *
     * @ToDo: Implement swipe gesture control. The old swipe gesture was removed due to a scrolling bug.
     */
    $.plugin('swOffcanvasMenu', {

        /**
         * Plugin default options.
         * Get merged automatically with the user configuration.
         */
        defaults: {

            /**
             * Selector for the content wrapper
             *
             * @property wrapSelector
             * @type {String}
             */
            'wrapSelector': '.page-wrap',

            /**
             * Whether or not the wrapper should be moved.
             *
             * @property moveWrapper
             * @type {Boolean}
             */
            'moveWrapper': false,

            /**
             * Selector of the off-canvas element
             *
             * @property offCanvasSelector
             * @type {String}
             */
            'offCanvasSelector': '.sidebar-main',

            /**
             * Selector for an additional button to close the menu
             *
             * @property closeButtonSelector
             * @type {String}
             */
            'closeButtonSelector': '.entry--close-off-canvas',

            /**
             * Animation direction, `fromLeft` (default) and `fromRight` are possible
             *
             * @property direction
             * @type {String}
             */
            'direction': 'fromLeft',

            /**
             * Additional class for the off-canvas menu for necessary styling
             *
             * @property offCanvasElementCls
             * @type {String}
             */
            'offCanvasElementCls': 'off-canvas',

            /**
             * Class which should be added when the menu will be opened on the left side
             *
             * @property leftMenuCls
             * @type {String}
             */
            'leftMenuCls': 'is--left',

            /**
             * Class which should be added when the menu will be opened on the right side
             *
             * @property rightMenuCls
             * @type {String}
             */
            'rightMenuCls': 'is--right',

            /**
             * Class which indicates if the off-canvas menu is visible
             *
             * @property activeMenuCls
             * @type {String}
             */
            'activeMenuCls': 'is--active',

            /**
             * Class which indicates if the off-canvas menu is visible
             *
             * @property openClass
             * @type {String}
             */
            'openClass': 'is--open',

            /**
             * Flag whether to show the offcanvas menu in full screen or not.
             *
             * @property fullscreen
             * @type {Boolean}
             */
            'fullscreen': false,

            /**
             * Class which sets the canvas to full screen
             *
             * @property fullscreenCls
             * @type {String}
             */
            'fullscreenCls': 'is--full-screen',

            /**
             * When this flag is set to true, the off canvas menu
             * will pop open instead of sliding.
             *
             * @property disableTransitions
             * @type {Boolean}
             */
            'disableTransitions': false,

            /**
             * The class that will be applied to the off canvas menu
             * to disable the transition property.
             *
             * @property disableTransitionCls
             * @type {String}
             */
            'disableTransitionCls': 'no--transitions',

            /**
             * The mode in which the off canvas menu should be showing.
             *
             * 'local': The given 'offCanvasSelector' will be used as the off canvas menu.
             *
             * 'ajax': The given 'offCanvasSelector' will be used as an URL to
             *         load the content via AJAX.
             *
             * @type {String}
             */
            'mode': 'local',

            /**
             * The URL that will be called when the menu is in 'ajax' mode.
             *
             * @type {String}
             */
            'ajaxURL': ''
        },

        /**
         * Initializes the plugin, sets up event listeners and adds the necessary
         * classes to get the plugin up and running.
         *
         * @public
         * @method init
         */
        init: function () {
            var me = this,
                opts = me.opts,
                themeConfig = window.themeConfig,
                $offCanvas;

            opts.moveWrapper = opts.moveWrapper || !!(themeConfig && !~~themeConfig.offcanvasOverlayPage);

            me.applyDataAttributes();

            // Cache the necessary elements
            me.$pageWrap = $(opts.wrapSelector);

            me.isOpened = false;

            if (opts.mode === 'ajax') {
                $offCanvas = me.$offCanvas = $('<div>', {
                    'class': opts.offCanvasElementCls
                }).appendTo('body');
            } else {
                $offCanvas = me.$offCanvas = $(opts.offCanvasSelector);
                $offCanvas.addClass(opts.offCanvasElementCls);
            }

            $offCanvas.addClass((opts.direction === 'fromLeft') ? opts.leftMenuCls : opts.rightMenuCls);
            $offCanvas.addClass(opts.disableTransitionCls);

            if (!opts.disableTransitions) {
                $offCanvas.removeClass(opts.disableTransitionCls);
            }

            if (opts.fullscreen) {
                $offCanvas.addClass(opts.fullscreenCls);
            }

            // Add active class with a timeout to properly register the disable transition class.
            setTimeout(function () {
                $offCanvas.addClass(opts.activeMenuCls);
            }, 0);

            me.registerEventListeners();
        },

        /**
         * Registers all necessary event listeners for the plugin to proper operate.
         *
         * @public
         * @method onClickElement
         */
        registerEventListeners: function () {
            var me = this,
                opts = me.opts;

            // Button click
            me._on(me.$el, 'click', $.proxy(me.onClickElement, me));

            // Allow the user to close the off canvas menu
            me.$offCanvas.on(me.getEventName('click'), opts.closeButtonSelector, $.proxy(me.onClickCloseButton, me));

            $.subscribe('plugin/swOffcanvasMenu/onBeforeOpenMenu', $.proxy(me.onBeforeOpenMenu, me));

            $.publish('plugin/swOffcanvasMenu/onRegisterEvents', [ me ]);
        },

        /**
         * Called when a off canvas menu opens.
         * Closes all other off canvas menus if its not the opening menu instance.
         *
         * @param {jQuery.Event} event
         * @param {PluginBase} plugin
         */
        onBeforeOpenMenu: function (event, plugin) {
            var me = this;

            if (plugin !== me) {
                me.closeMenu();
            }
        },

        /**
         * Called when the plugin element was clicked on.
         * Opens the off canvas menu, if the clicked element is not inside
         * the off canvas menu, prevent its default behaviour.
         *
         * @public
         * @method onClickElement
         * @param {jQuery.Event} event
         */
        onClickElement: function (event) {
            var me = this;

            if (!$.contains(me.$offCanvas[0], (event.target || event.currentTarget))) {
                event.preventDefault();
            }

            me.openMenu();

            $.publish('plugin/swOffcanvasMenu/onClickElement', [ me, event ]);
        },

        /**
         * Called when the body was clicked on.
         * Closes the off canvas menu.
         *
         * @public
         * @method onClickBody
         * @param {jQuery.Event} event
         */
        onClickCloseButton: function (event) {
            var me = this;

            event.preventDefault();
            event.stopPropagation();

            me.closeMenu();

            $.publish('plugin/swOffcanvasMenu/onClickCloseButton', [ me, event ]);
        },

        /**
         * Opens the off-canvas menu based on the direction.
         * Also closes all other off-canvas menus.
         *
         * @public
         * @method openMenu
         */
        openMenu: function () {
            var me = this,
                opts = me.opts,
                fromLeft = opts.direction === 'fromLeft',
                menuWidth = me.$offCanvas.outerWidth(),
                plugin;

            if (me.isOpened) {
                return;
            }
            me.isOpened = true;

            $.publish('plugin/swOffcanvasMenu/onBeforeOpenMenu', [ me ]);

            $html.addClass('no--scroll');

            $.overlay.open({
                onClose: $.proxy(me.closeMenu, me)
            });

            if (opts.moveWrapper) {
                if (opts.direction === 'fromRight') {
                    menuWidth *= -1;
                }

                me.$pageWrap.css('left', menuWidth);
            }

            me.$offCanvas.addClass(opts.openClass);

            $.publish('plugin/swOffcanvasMenu/onOpenMenu', [ me ]);

            if (opts.mode === 'ajax' && opts.ajaxURL) {
                $.ajax({
                    url: opts.ajaxURL,
                    success: function (result) {
                        me.$offCanvas.html(result);
                    }
                });
            }
        },

        /**
         * Closes the menu and slides the content wrapper
         * back to the normal position.
         *
         * @public
         * @method closeMenu
         */
        closeMenu: function () {
            var me = this,
                opts = me.opts;

            if (!me.isOpened) {
                return;
            }
            me.isOpened = false;

            $.overlay.close();

            // Disable scrolling on body
            $html.removeClass('no--scroll');

            if (opts.moveWrapper) {
                me.$pageWrap.css('left', 0);
            }

            me.$offCanvas.removeClass(opts.openClass);

            $.publish('plugin/swOffcanvasMenu/onCloseMenu', [ me ]);
        },

        /**
         * Destroys the initialized plugin completely, so all event listeners will
         * be removed and the plugin data, which is stored in-memory referenced to
         * the DOM node.
         *
         * @public
         * @method destroy
         */
        destroy: function () {
            var me = this,
                opts = me.opts;

            me.closeMenu();

            me.$offCanvas.removeClass(opts.offCanvasElementCls)
                .removeClass(opts.activeMenuCls)
                .removeClass(opts.openClass)
                .removeAttr('style');

            if (opts.moveWrapper) {
                me.$pageWrap.removeAttr('style');
            }

            me.$el.off(me.getEventName('click'), opts.closeButtonSelector);

            $.unsubscribe('plugin/swOffcanvasMenu/onBeforeOpenMenu', $.proxy(me.onBeforeOpenMenu, me));

            me._destroy();
        }
    });
})(jQuery);

;(function ($, StateManager, window) {
    'use strict';

    var msPointerEnabled = window.navigator.msPointerEnabled,
        $body = $('body');

    /**
     * Shopware Search Plugin.
     *
     * The plugin controlling the search field behaviour in all possible states
     */
    $.plugin('swSearch', {

        defaults: {

            /**
             * Class which will be added when the drop down was triggered
             *
             * @type {String}
             */
            activeCls: 'is--active',

            /**
             * Class which will be used for generating search results
             *
             * @type {String}
             */
            searchFieldSelector: '.main-search--field',

            /**
             * Selector for the search result list.
             *
             * @type {String}
             */
            resultsSelector: '.main-search--results',

            /**
             * Selector for the link in a result entry.
             *
             * @type {String}
             */
            resultLinkSelector: '.search-result--link',

            /**
             * Selector for a single result entry.
             *
             * @type {String}
             */
            resultItemSelector: '.result--item',

            /**
             * Selector for the ajax loading indicator.
             *
             * @type {String}
             */
            loadingIndicatorSelector: '.form--ajax-loader',

            /**
             * Selector for the main header element.
             * On mobile viewport the header get an active class when the
             * search bar is opened for additional styling.
             *
             * @type {String}
             */
            headerSelector: '.header-main',

            /**
             * Gets added when the search bar is active on mobile viewport.
             * Handles additional styling.
             *
             * @type {String}
             */
            activeHeaderClass: 'is--active-searchfield',

            /**
             * Selector for the ajax loading indicator.
             *
             * @type {String}
             */
            triggerSelector: '.entry--trigger',

            /**
             * The URL used for the search request.
             * This option has to be set or an error will be thrown.
             *
             * @type {String}
             */
            requestUrl: '',

            /**
             * Flag whether or not the keyboard navigation is enabled
             *
             * @type {Boolean}
             */
            keyBoardNavigation: true,

            /**
             * Whether or not the active class is set by default
             *
             * @type {String}
             */
            activeOnStart: false,

            /**
             * Minimum amount of characters needed to trigger the search request
             *
             * @type {Number}
             */
            minLength: 3,

            /**
             * Time in milliseconds to wait after each key down event before
             * before starting the search request.
             * If a key was pressed in this time, the last request will be aborted.
             *
             * @type {Number}
             */
            searchDelay: 250,

            /**
             * The speed of all animations.
             *
             * @type {String|Number}
             */
            animationSpeed: 200,

            /**
             * The kay mapping for navigation the search results via keyboard.
             *
             * @type {Object}
             */
            keyMap: {
                'UP': 38,
                'DOWN': 40,
                'ENTER': 13
            }
        },

        /**
         * Initializes the plugin
         *
         * @public
         * @method init
         */
        init: function () {
            var me = this,
                $el = me.$el,
                opts = me.opts;

            me.applyDataAttributes();

            /**
             * The URL to which the search term will send via AJAX
             *
             * @public
             * @property requestURL
             * @type {String}
             */
            me.requestURL = opts.requestUrl || window.controller.ajax_search;

            if (!me.requestURL) {
                throw new Error('Parameter "requestUrl" needs to be set.');
            }

            /**
            * Converts the url to a protocol relative url, so we don't need to manually
            * check the used http protocol. See the example from paul irish to get an idea
            * how it should work:
            *    `http://www.paulirish.com/2010/the-protocol-relative-url/`
            *    `http://blog.httpwatch.com/2010/02/10/using-protocol-relative-urls-to-switch-between-http-and-https/`
            *
            * @param {String} url - the url which needs to be converted
            * @returns {String} converted string
            */
            var convertUrlToRelativeUrl = function(url) {
                url = url.replace('https:', '');
                url = url.replace('http:', '');

                return url;
            };

            me.requestURL = convertUrlToRelativeUrl(me.requestURL);

            /**
             * The search field itself.
             *
             * @public
             * @property $searchfield
             * @type {jQuery}
             */
            me.$searchField = $el.find(opts.searchFieldSelector);

            /**
             * The list in which the top results will be shown
             *
             * @public
             * @property $results
             * @type {jQuery}
             */
            me.$results = $el.find(opts.resultsSelector);

            /**
             * The loading indicator thats inside the search
             *
             * @public
             * @property $loader
             * @type {jQuery}
             */
            me.$loader = $el.find(opts.loadingIndicatorSelector);

            /**
             * The button to toggle the search field on mobile viewport
             *
             * @public
             * @property $toggleSearchBtn
             * @type {jQuery}
             */
            me.$toggleSearchBtn = $el.find(opts.triggerSelector);

            /**
             * The shop header to add a new class after opening
             *
             * @public
             * @property $mainHeader
             * @type {jQuery}
             */
            me.$mainHeader = $(opts.headerSelector);

            /**
             * The last search term that was entered in the search field.
             *
             * @public
             * @property lastSearchTerm
             * @type {String}
             */
            me.lastSearchTerm = '';

            /**
             * Timeout ID of the key up event.
             * The timeout is used to buffer fast key events.
             *
             * @public
             * @property keyupTimeout
             * @type {Number}
             */
            me.keyupTimeout = 0;

            /**
             * Indicates if the form is already submitted
             *
             * @type {boolean}
             * @private
             */
            me._isSubmitting = false;

            me.registerListeners();
        },

        /**
         * Registers all necessary events for the plugin.
         *
         * @public
         * @method registerListeners
         */
        registerListeners: function () {
            var me = this,
                opts = me.opts,
                $searchField = me.$searchField,
                $formElement = me.$searchField.closest('form');

            me._on($searchField, 'keyup', $.proxy(me.onKeyUp, me));
            me._on($searchField, 'keydown', $.proxy(me.onKeyDown, me));
            me._on(me.$toggleSearchBtn, 'click', $.proxy(me.onClickSearchEntry, me));
            me._on($formElement, 'submit', $.proxy(me.onSubmit, me));

            if (msPointerEnabled) {
                me.$results.on('click', opts.resultLinkSelector, function (event) {
                    window.location.href = $(event.currentTarget).attr('href');
                });
            }

            StateManager.registerListener({
                state: 'xs',
                enter: function () {
                    if (opts.activeOnStart) {
                        me.openMobileSearch();
                    }
                },
                exit: function () {
                    me.closeMobileSearch();
                }
            });

            $.publish('plugin/swSearch/onRegisterEvents', [ me ]);
        },

        /**
         * Event handler method which will be fired when the user presses a key when
         * focusing the field.
         *
         * @public
         * @method onKeyDown
         * @param {jQuery.Event} event
         */
        onKeyDown: function (event) {
            var me = this,
                opts = me.opts,
                keyMap = opts.keyMap,
                keyCode = event.which,
                navKeyPressed = opts.keyBoardNavigation && (keyCode === keyMap.UP || keyCode === keyMap.DOWN || keyCode === keyMap.ENTER);

            $.publish('plugin/swSearch/onKeyDown', [ me, event ]);

            if (navKeyPressed && me.$results.hasClass(opts.activeCls)) {
                me.onKeyboardNavigation(keyCode);
                event.preventDefault();
                return false;
            }

            return true;
        },

        /**
         * Will be called when a key was released on the search field.
         *
         * @public
         * @method onKeyUp
         * @param {jQuery.Event} event
         */
        onKeyUp: function (event) {
            var me = this,
                opts = me.opts,
                term = me.$searchField.val() + '',
                timeout = me.keyupTimeout;

            $.publish('plugin/swSearch/onKeyUp', [ me, event ]);

            if (timeout) {
                window.clearTimeout(timeout);
            }

            if (term.length < opts.minLength) {
                me.lastSearchTerm = '';
                me.closeResult();
                return;
            }

            if (term === me.lastSearchTerm) {
                return;
            }

            me.keyupTimeout = window.setTimeout($.proxy(me.triggerSearchRequest, me, term), opts.searchDelay);
        },

        /**
         * Blocks further submit events to throttle requests to the server
         *
         * @param event
         */
        onSubmit: function (event) {
            var me = this;

            if (me._isSubmitting) {
                event.preventDefault();
                return;
            }

            me._isSubmitting = true;
        },

        /**
         * Triggers an AJAX request with the given search term.
         *
         * @public
         * @method triggerSearchRequest
         * @param {String} searchTerm
         */
        triggerSearchRequest: function (searchTerm) {
            var me = this;

            me.$loader.fadeIn(me.opts.animationSpeed);

            me.lastSearchTerm = $.trim(searchTerm);

            $.publish('plugin/swSearch/onSearchRequest', [ me, searchTerm ]);

            $.ajax({
                'url': me.requestURL,
                'data': {
                    'sSearch': me.lastSearchTerm
                },
                'success': function (response) {
                    me.showResult(response);

                    $.publish('plugin/swSearch/onSearchResponse', [ me, searchTerm, response ]);
                }
            });
        },

        /**
         * Clears the result list and appends the given (AJAX) response to it.
         *
         * @public
         * @method showResult
         * @param {String} response
         */
        showResult: function (response) {
            var me = this,
                opts = me.opts;

            me.$loader.fadeOut(opts.animationSpeed);
            me.$results.empty().html(response).addClass(opts.activeCls).show();

            if (!StateManager.isCurrentState('xs')) {
                $body.on(me.getEventName('click touchstart'), $.proxy(me.onClickBody, me));
            }

            picturefill();

            $.publish('plugin/swSearch/onShowResult', [ me ]);
        },

        /**
         * Closes the result list and removes all its items.
         *
         * @public
         * @method closeResult
         */
        closeResult: function () {
            var me = this;

            me.$results.removeClass(me.opts.activeCls).hide().empty();

            $.publish('plugin/swSearch/onCloseResult', [ me ]);
        },

        /**
         * Called when the body was clicked after the search field went active.
         * Closes the search field and results.
         *
         * @public
         * @method onClickBody
         * @param {jQuery.Event} event
         */
        onClickBody: function (event) {
            var me = this,
                target = event.target,
                pluginEl = me.$el[0],
                resultsEl = me.$results[0];

            if (target === pluginEl || target === resultsEl || $.contains(pluginEl, target) || $.contains(resultsEl, target)) {
                return;
            }

            $body.off(me.getEventName('click touchstart'));

            me.closeMobileSearch();
        },

        /**
         * Adds support to navigate using the keyboard.
         *
         * @public
         * @method onKeyboardNavigation
         * @param {Number} keyCode
         */
        onKeyboardNavigation: function (keyCode) {
            var me = this,
                opts = me.opts,
                keyMap = opts.keyMap,
                $results = me.$results,
                activeClass = opts.activeCls,
                $selected = $results.find('.' + activeClass),
                $resultItems,
                $nextSibling;

            $.publish('plugin/swSearch/onKeyboardNavigation', [ me, keyCode ]);

            if (keyCode === keyMap.UP || keyCode === keyMap.DOWN) {
                $resultItems = $results.find(opts.resultItemSelector);

                //First time the user hits the navigation key "DOWN"
                if (!$selected.length && keyCode == keyMap.DOWN) {
                    me.selectFirstResultItem($resultItems);
                    return;
                }

                //First time the user hits the navigation key "UP"
                if (!$selected.length && keyCode == keyMap.UP) {
                    me.selectLastResultItem($resultItems);
                    return;
                }

                $resultItems.removeClass(activeClass);
                if (me.selectResultItem(keyCode, $selected)) {
                    return;
                }

            }

            //Start on top or bottom if the user reached the end of the list
            switch (keyCode) {
                case keyMap.DOWN:
                    me.selectFirstResultItem($resultItems);
                    break;
                case keyMap.UP:
                    me.selectLastResultItem($resultItems);
                    break;
                case keyMap.ENTER:
                    me.onPressEnter($selected);
                    break;
            }
        },

        /**
         * onClickSearchTrigger event for displaying and hiding
         * the search field
         *
         * @public
         * @method onClickSearchEntry
         * @param event
         */
        onClickSearchEntry: function (event) {
            var me = this,
                $el = me.$el,
                opts = me.opts;

            $.publish('plugin/swSearch/onClickSearchEntry', [ me, event ]);

            if (!StateManager.isCurrentState('xs')) {
                return;
            }

            event.preventDefault();
            event.stopPropagation();

            $el.hasClass(opts.activeCls) ? me.closeMobileSearch() : me.openMobileSearch();
        },

        /**
         * Opens the mobile search bar and focuses it.
         *
         * @public
         * @method openMobileSearch
         */
        openMobileSearch: function () {
            var me = this,
                $el = me.$el,
                opts = me.opts,
                activeCls = opts.activeCls;

            $body.on(me.getEventName('click touchstart'), $.proxy(me.onClickBody, me));

            $el.addClass(activeCls);
            me.$toggleSearchBtn.addClass(activeCls);
            me.$mainHeader.addClass(opts.activeHeaderClass);

            me.$searchField.focus();

            $.publish('plugin/swSearch/onOpenMobileSearch', [ me ]);
        },

        /**
         * Closes the mobile search bar and removes its focus.
         *
         * @public
         * @method closeMobileSearch
         */
        closeMobileSearch: function () {
            var me = this,
                $el = me.$el,
                opts = me.opts,
                activeCls = opts.activeCls;

            $el.removeClass(activeCls);
            me.$toggleSearchBtn.removeClass(activeCls);
            me.$mainHeader.removeClass(opts.activeHeaderClass);

            me.$searchField.blur();

            $.publish('plugin/swSearch/onCloseMobileSearch', [ me ]);

            me.closeResult();
        },

        /**
         * @param {Object} resultItems
         */
        selectFirstResultItem: function (resultItems) {
            var me = this,
                opts = me.opts,
                activeClass = opts.activeCls;

            $.publish('plugin/swSearch/onSelectFirstResultItem', [ me, resultItems ]);

            resultItems.first().addClass(activeClass);
        },

        /**
         * @param {Object} resultItems
         */
        selectLastResultItem: function (resultItems) {
            var me = this,
                opts = me.opts,
                activeClass = opts.activeCls;

            $.publish('plugin/swSearch/onSelectLastResultItem', [ me, resultItems ]);

            resultItems.last().addClass(activeClass);
        },

        /**
         * Selects the next or previous result item based on the pressed navigation key.
         *
         * @param {Number} keyCode
         * @param {Object} $selected
         */
        selectResultItem: function (keyCode, $selected) {
            var me = this,
                opts = me.opts,
                keyMap = opts.keyMap,
                activeClass = opts.activeCls,
                $nextSibling;

            $.publish('plugin/swSearch/onSelectNextResultItem', [ me, keyCode ]);

            $nextSibling = $selected[(keyCode === keyMap.DOWN) ? 'next' : 'prev'](opts.resultItemSelector);
            if ($nextSibling.length) {
                $nextSibling.addClass(activeClass);
                return true;
            }
            return false;
        },

        /**
         * Redirects the user to the search result page on enter.
         *
         * @param {Object} $selected
         */
        onPressEnter: function ($selected) {
            var me = this,
                opts = me.opts;

            $.publish('plugin/swSearch/onPressEnter', [ me, $selected ]);

            if ($selected.length) {
                window.location.href = $selected.find(opts.resultLinkSelector).attr('href');
                return;
            }

            me.$parent.submit();
        },

        /**
         * Destroys the plugin and removes registered events.
         *
         * @public
         * @method destroy
         */
        destroy: function () {
            var me = this;

            me.closeMobileSearch();

            $body.off(me.getEventName('click touchstart'));

            me._destroy();
        }
    });
})(jQuery, StateManager, window);

;(function ($) {

    /**
     * Shopware Tab Menu Plugin
     *
     * This plugin sets up a menu with tabs you can switch between.
     */
    $.plugin('swTabMenu', {

        defaults: {

            /**
             * Class that should be set on the plugin element when initializing
             *
             * @property pluginClass
             * @type {String}
             */
            'pluginClass': 'js--tab-menu',

            /**
             * Selector for the tab navigation list
             *
             * @property tabContainerSelector
             * @type {String}
             */
            'tabContainerSelector': '.tab--navigation',

            /**
             * Selector for a tab navigation item
             *
             * @property tabSelector
             * @type {String}
             */
            'tabSelector': '.tab--link',

            /**
             * Selector for the tab content list
             *
             * @property containerListSelector
             * @type {String}
             */
            'containerListSelector': '.tab--container-list',

            /**
             * Selector for the tab container in a tab container list.
             *
             * @property containerSelector
             * @type {String}
             */
            'containerSelector': '.tab--container',

            /**
             * Selector for the content element inside a tab container.
             *
             * @property contentSelector
             * @type {String}
             */
            'contentSelector': '.tab--content',

            /**
             * Class that will be applied to a content container and
             * its corresponding tab when the container has any content.
             *
             * @property hasContentClass
             * @type {String}
             */
            'hasContentClass': 'has--content',

            /**
             * Class that should be set on an active tab navigation item
             *
             * @property activeTabClass
             * @type {String}
             */
            'activeTabClass': 'is--active',

            /**
             * Class that should be set on an active tab content item
             *
             * @property activeContainerClass
             * @type {String}
             */
            'activeContainerClass': 'is--active',

            /**
             * Starting index of the tabs
             *
             * @property startIndex
             * @type {Number}
             */
            'startIndex': -1,

            /**
             * This option can make the tab menu container horizontally
             * scrollable when too many tab menu items are displayed.
             * The functionality is provided by the swMenuScroller plugin.
             *
             * @property scrollable
             * @type {Boolean}
             */
            'scrollable': false
        },

        /**
         * Initializes the plugin and register its events
         *
         * @public
         * @method init
         */
        init: function () {
            var me = this,
                opts = me.opts,
                $el = me.$el,
                $container,
                $tab;

            me.applyDataAttributes();

            $el.addClass(opts.pluginClass);

            me.$tabContainer = $el.find(opts.tabContainerSelector);

            me.$containerList = $el.find(opts.containerListSelector);

            me.$tabs = me.$tabContainer.find(opts.tabSelector);

            me.$container = me.$containerList.find(opts.containerSelector);

            me.$container.each(function (i, el) {
                $container = $(el);
                $tab = $(me.$tabs.get(i));

                if ($container.find(opts.contentSelector).html().trim().length) {
                    $container.addClass(opts.hasContentClass);
                    $tab.addClass(opts.hasContentClass);
                    
                    // When no start index is specified, we take the first tab with content.
                    if (opts.startIndex === -1) {
                        $tab.addClass(opts.activeTabClass);
                        opts.startIndex = i;
                    }
                }
            });

            if (me.opts.scrollable) {
                me.$el.swMenuScroller({
                    'listSelector': me.$tabContainer
                });
            }

            opts.startIndex = Math.max(opts.startIndex, 0);

            me._index = null;

            me.registerEventListeners();

            me.changeTab(opts.startIndex)
        },

        /**
         * This method registers the event listeners when when clicking
         * or tapping a tab navigation item.
         *
         * @public
         * @method registerEvents
         */
        registerEventListeners: function () {
            var me = this;

            me.$tabs.each(function (i, el) {
                me._on(el, 'click touchstart', $.proxy(me.changeTab, me, i));
            });

            $.publish('plugin/swTabMenu/onRegisterEvents', [ me ]);
        },

        /**
         * This method switches to a new tab depending on the passed index
         * If the give index is the same as the current active one, nothing happens.
         *
         * @public
         * @method changeTab
         * @param {Number} index
         * @param {jQuery.Event} event
         */
        changeTab: function (index, event) {
            var me = this,
                opts = me.opts,
                activeTabClass = opts.activeTabClass,
                activeContainerClass = opts.activeContainerClass,
                $tab,
                tabId,
                dataUrl,
                $container;

            if (event) {
                event.preventDefault();
            }

            if (index === me._index) {
                return;
            }

            me._index = index;

            $tab = $(me.$tabs.get(index));
            $container = $(me.$container.get(index));

            me.$tabContainer
                .find('.' + activeTabClass)
                .removeClass(activeTabClass);

            $tab.addClass(activeTabClass);

            me.$containerList
                .find('.' + activeContainerClass)
                .removeClass(activeContainerClass);

            $container.addClass(activeContainerClass);

            dataUrl = $tab.attr('data-url');
            tabId = $container.attr('data-tab-id');

            if ($tab.attr('data-mode') === 'remote' && dataUrl) {
                $container.load(dataUrl);
            }

            if (tabId !== undefined) {
                $.publish('onShowContent-' + tabId, [ me, index ]);
            }

            $.publish('plugin/swTabMenu/onChangeTab', [ me, index ]);
        },

        /**
         * This method removes all plugin specific classes
         * and removes all registered events
         *
         * @public
         * @method destroy
         */
        destroy: function () {
            var me = this,
                menuScroller = me.$el.data('plugin_swMenuScroller');

            if (menuScroller !== undefined) {
                menuScroller.destroy();
            }

            me.$el.removeClass(me.opts.pluginClass);

            me._destroy();
        }
    });
})(jQuery);

;(function ($, Modernizr, window, Math) {
    'use strict';

    var transitionProperty = StateManager.getVendorProperty('transition'),
        transformProperty = StateManager.getVendorProperty('transform'),
        killEvent = function (event) {
            event.preventDefault();
            event.stopPropagation();
        };

    /**
     * Image Slider Plugin.
     *
     * This plugin provides the functionality for an advanced responsive image slider.
     * It has support for thumbnails, arrow controls, touch controls and automatic sliding.
     *
     * Example DOM Structure:
     *
     * <div class="image-slider" data-image-slider="true">
     *      <div class="image-slider--container">
     *          <div class="image-slider--slide">
     *              <div class="image-slider--item"></div>
     *              <div class="image-slider--item"></div>
     *              <div class="image-slider--item"></div>
     *          </div>
     *      </div>
     *      <div class="image-slider--thumbnails">
     *          <div class="image-slider--thumbnails-slide">
     *              <a class="thumbnail--link"></a>
     *              <a class="thumbnail--link"></a>
     *              <a class="thumbnail--link"></a>
     *          </div>
     *      </div>
     * </div>
     */
    $.plugin('swImageSlider', {

        defaults: {

            /**
             * Set the speed of the slide animation in ms.
             *
             * @property animationSpeed
             * @type {Number}
             */
            animationSpeed: 350,

            /**
             * Easing function for the slide animations.
             * Will only be set when transitions and
             * transforms are supported by the browser.
             *
             * @property animationEasing
             * @type {String}
             */
            animationEasing: 'cubic-bezier(.2,.89,.75,.99)',

            /**
             * Turn thumbnail support on and off.
             *
             * @property thumbnails
             * @type {Boolean}
             */
            thumbnails: true,

            /**
             * Turn support for a small dot navigation on and off.
             *
             * @property dotNavigation
             * @type {Boolean}
             */
            dotNavigation: true,

            /**
             * Turn arrow controls on and off.
             *
             * @property arrowControls
             * @type {Boolean}
             */
            arrowControls: true,

            /**
             * Turn touch controls on and off.
             *
             * @property touchControls
             * @type {Boolean}
             */
            touchControls: true,

            /**
             * Whether or not the automatic slide feature should be active.
             *
             * @property autoSlide
             * @type {Boolean}
             */
            autoSlide: false,

            /**
             * Whether or not the pinch to zoom feature should be active.
             *
             * @property pinchToZoom
             * @type {Boolean}
             */
            pinchToZoom: false,

            /**
             * Whether or not the swipe to slide feature should be active.
             *
             * @property swipeToSlide
             * @type {Boolean}
             */
            swipeToSlide: true,

            /**
             * Whether or not the pull preview feature should be active.
             *
             * @property pullPreview
             * @type {Boolean}
             */
            pullPreview: false,

            /**
             * Whether or not the double tap/click should be used to zoom in/out..
             *
             * @property doubleTap
             * @type {Boolean}
             */
            doubleTap: false,

            /**
             * Time in milliseconds in which two touches should be
             * registered as a double tap.
             *
             * @property doubleTapPeriod
             * @type {Number}
             */
            doubleTapPeriod: 400,

            /**
             * Whether or not the scrolling should be prevented when moving on the slide.
             *
             * @property preventScrolling
             * @type {Boolean}
             */
            preventScrolling: false,

            /**
             * The minimal zoom factor an image can have.
             *
             * @property minZoom
             * @type {Number}
             */
            minZoom: 1,

            /**
             * The maximal zoom factor an image can have.
             * Can either be a number or 'auto'.
             *
             * If set to 'auto', you can only zoom to the original image size.
             *
             * @property maxZoom
             * @type {Number|String}
             */
            maxZoom: 'auto',

            /**
             * The distance in which a pointer move is registered.
             *
             * @property moveTolerance
             * @type {Number}
             */
            moveTolerance: 30,

            /**
             * The distance you have to travel to recognize a swipe in pixels.
             *
             * @property swipeTolerance
             * @type {Number}
             */
            swipeTolerance: 50,

            /**
             * Time period in which the swipe gesture will be registered.
             *
             * @property swipePeriod
             * @type {Number}
             */
            swipePeriod: 250,

            /**
             * Tolerance of the pull preview.
             * When this tolerance is exceeded,
             * the image will slide to the next/previous image.
             * Can either be a number that represent a pixel value or
             * 'auto' to take a third of the viewport as the tolerance.
             *
             * @property pullTolerance
             * @type {String|Number}
             */
            pullTolerance: 'auto',

            /**
             * The image index that will be set when the plugin gets initialized.
             *
             * @property startIndex
             * @type {Number}
             */
            startIndex: 0,

            /**
             * Set the speed for the automatic sliding in ms.
             *
             * @property autoSlideInterval
             * @type {Number}
             */
            autoSlideInterval: 5000,

            /**
             * This property indicates whether or not the slides are looped.
             * If this flag is active and the last slide is active, you can
             * slide to the next one and it will start from the beginning.
             *
             * @property loopSlides
             * @type {Boolean}
             */
            loopSlides: false,

            /**
             * The selector for the container element holding the actual image slider.
             *
             * @property imageContainerSelector
             * @type {String}
             */
            imageContainerSelector: '.image-slider--container',

            /**
             * The selector for the slide element which slides inside the image container.
             *
             * @property imageSlideSelector
             * @type {String}
             */
            imageSlideSelector: '.image-slider--slide',

            /**
             * The selector fot the container element holding the thumbnails.
             *
             * @property thumbnailContainerSelector
             * @type {String}
             */
            thumbnailContainerSelector: '.image-slider--thumbnails',

            /**
             * The selector for the element that slides inside the thumbnail container.
             * This element should be contained in the thumbnail container.
             *
             * @property thumbnailSlideSelector
             * @type {String}
             */
            thumbnailSlideSelector: '.image-slider--thumbnails-slide',

            /**
             * Selector of a single thumbnail.
             * Those thumbnails should be contained in the thumbnail slide.
             *
             * @property thumbnailSlideSelector
             * @type {String}
             */
            thumbnailSelector: '.thumbnail--link',

            /**
             * The selector for the dot navigation container.
             *
             * @property dotNavSelector
             * @type {String}
             */
            dotNavSelector: '.image-slider--dots',

            /**
             * The selector for each dot link in the dot navigation.
             *
             * @property dotLinkSelector
             * @type {String}
             */
            dotLinkSelector: '.dot--link',

            /**
             * Class that will be applied to both the previous and next arrow.
             *
             * @property thumbnailArrowCls
             * @type {String}
             */
            thumbnailArrowCls: 'thumbnails--arrow',

            /**
             * The css class for the left slider arrow.
             *
             * @property leftArrowCls
             * @type {String}
             */
            leftArrowCls: 'arrow is--left',

            /**
             * The css class for the right slider arrow.
             *
             * @property rightArrowCls
             * @type {String}
             */
            rightArrowCls: 'arrow is--right',

            /**
             * The css class for a top positioned thumbnail arrow.
             *
             * @property thumbnailArrowTopCls
             * @type {String}
             */
            thumbnailArrowTopCls: 'is--top',

            /**
             * The css class for a left positioned thumbnail arrow.
             *
             * @property thumbnailArrowLeftCls
             * @type {String}
             */
            thumbnailArrowLeftCls: 'is--left',

            /**
             * The css class for a right positioned thumbnail arrow.
             *
             * @property thumbnailArrowRightCls
             * @type {String}
             */
            thumbnailArrowRightCls: 'is--right',

            /**
             * The css class for a bottom positioned thumbnail arrow.
             *
             * @property thumbnailArrowBottomCls
             * @type {String}
             */
            thumbnailArrowBottomCls: 'is--bottom',

            /**
             * The css class for active states of the arrows.
             *
             * @property activeStateClass
             * @type {String}
             */
            activeStateClass: 'is--active',

            /**
             * Class that will be appended to the image container
             * when the user is grabbing an image
             *
             * @property grabClass
             * @type {String}
             */
            dragClass: 'is--dragging',

            /**
             * Class that will be appended to the thumbnail container
             * when no other thumbnails are available
             *
             * @property noThumbClass
             * @type {String}
             */
            noThumbClass: 'no--thumbnails',

            /**
             * Selector for the image elements in the slider.
             * Those images should be contained in the image slide element.
             *
             * @property imageSelector
             * @type {String}
             */
            imageSelector: '.image-slider--item img',

            /**
             * Selector for a single slide item.
             * Those elements should be contained in the image slide element.
             *
             * @property itemSelector
             * @type {String}
             */
            itemSelector: '.image-slider--item',

            /**
             * Class that will be appended when an element should not be shown.
             *
             * @property hiddenClass
             * @type {String}
             */
            hiddenClass: 'is--hidden'
        },

        /**
         * Method for the plugin initialisation.
         * Merges the passed options with the data attribute configurations.
         * Creates and references all needed elements and properties.
         * Calls the registerEvents method afterwards.
         *
         * @public
         * @method init
         */
        init: function () {
            var me = this,
                opts = me.opts;

            // Merge the data attribute configurations with the default ones
            me.applyDataAttributes();

            /**
             * Container of the slide element.
             * Acts as a wrapper and container for additional
             * elements like arrows.
             *
             * @private
             * @property _$slideContainer
             * @type {jQuery}
             */
            me._$slideContainer = me.$el.find(opts.imageContainerSelector);

            /**
             * Container of the slide element.
             * Acts as a wrapper and container for additional
             * elements like arrows.
             *
             * @private
             * @property $slide
             * @type {jQuery}
             */
            me._$slide = me._$slideContainer.find(opts.imageSlideSelector);

            /**
             * Current index of the active slide.
             * Will be used for correctly showing the active thumbnails / dot.
             *
             * @private
             * @property _slideIndex
             * @type {Number}
             */
            me._slideIndex = opts.startIndex;

            /**
             * ID of the setTimeout that will be called if the
             * auto slide option is active.
             * Wil be used for removing / resetting the timer.
             *
             * @private
             * @property _slideInterval
             * @type {Number}
             */
            me._slideInterval = 0;

            /**
             * References the currently active image.
             * This element is contained in a jQuery wrapper.
             *
             * @private
             * @property _$currentImage
             * @type {jQuery}
             */
            me._$currentImage = null;

            /**
             * Minimal zoom factor for image scaling
             *
             * @private
             * @property _minZoom
             * @type {Number}
             */
            me._minZoom = parseFloat(opts.minZoom) || 1;

            /**
             * Maximum zoom factor for image scaling
             *
             * @private
             * @property _maxZoom
             * @type {Number}
             */
            me._maxZoom = parseFloat(opts.maxZoom);

            /**
             * Whether or not the scale should be recalculated for each image.
             *
             * @private
             * @property _autoScale
             * @type {Boolean}
             */
            me._autoScale = !me._maxZoom && (me._maxZoom = me._minZoom);

            if (opts.thumbnails) {
                me._$thumbnailContainer = me.$el.find(opts.thumbnailContainerSelector);
                me._$thumbnailSlide = me._$thumbnailContainer.find(opts.thumbnailSlideSelector);
                me._thumbnailOrientation = me.getThumbnailOrientation();
                me._thumbnailOffset = 0;
                me.createThumbnailArrows();
            }

            if (opts.dotNavigation) {
                me._$dotNav = me.$el.find(opts.dotNavSelector);
                me._$dots = me._$dotNav.find(opts.dotLinkSelector);
                me.setActiveDot(me._slideIndex);
            }

            me.trackItems();

            if (opts.arrowControls) {
                me.createArrows();
            }

            if (opts.thumbnails) {
                me.trackThumbnailControls();
                me.setActiveThumbnail(me._slideIndex);
            }

            me.setIndex(me._slideIndex);

            /**
             * Whether or not the user is grabbing the image with the mouse.
             *
             * @private
             * @property _grabImage
             * @type {Boolean}
             */
            me._grabImage = false;

            /**
             * First touch point position from touchstart event.
             * Will be used to determine the swiping gesture.
             *
             * @private
             * @property _startTouchPoint
             * @type {Vector}
             */
            me._startTouchPoint = new Vector(0, 0);

            /**
             * Translation (positioning) of the current image.
             *
             * @private
             * @property _imageTranslation
             * @type {Vector}
             */
            me._imageTranslation = new Vector(0, 0);

            /**
             * Scaling (both X and Y equally) of the current image.
             *
             * @private
             * @property _imageScale
             * @type {Number}
             */
            me._imageScale = 1;

            /**
             * Relative distance when pinching.
             * Will be used for the pinch to zoom gesture.
             *
             * @private
             * @property _touchDistance
             * @type {Number}
             */
            me._touchDistance = 0;

            /**
             * Last time the current image was touched.
             * Used to determine double tapping.
             *
             * @private
             * @property _lastTouchTime
             * @type {Number}
             */
            me._lastTouchTime = 0;

            /**
             * Last time the current image was touched.
             * Used to determine a swipe instead of a pull.
             *
             * @private
             * @property _lastMoveTime
             * @type {Number}
             */
            me._lastMoveTime = 0;

            /**
             * Whether or not the slider should scroll while the finger is down.
             * Used to determin if the user scrolls down to lock the horizontal
             * scrolling.
             * Gets unlocked when the user end the touch.
             *
             * @private
             * @property _lockSlide
             * @type {Boolean}
             */
            me._lockSlide = false;

            me.registerEvents();
        },

        /**
         * Registers all necessary event listeners.
         *
         * @public
         * @method registerEvents
         */
        registerEvents: function () {
            var me = this,
                opts = me.opts,
                $slide = me._$slide;

            if (opts.touchControls) {
                me._on($slide, 'touchstart mousedown', $.proxy(me.onTouchStart, me));
                me._on($slide, 'touchmove mousemove', $.proxy(me.onTouchMove, me));
                me._on($slide, 'touchend mouseup mouseleave', $.proxy(me.onTouchEnd, me));
                me._on($slide, 'MSHoldVisual', killEvent);
                me._on($slide, 'click', $.proxy(me.onClick, me));

                if (!opts.preventScrolling && ('ontouchstart' in window || navigator.msMaxTouchPoints)) {
                    me._on($slide, 'movestart', function (e) {
                        // Allows the normal up and down scrolling from the browser
                        if ((e.distX > e.distY && e.distX < -e.distY) || (e.distX < e.distY && e.distX > -e.distY)) {
                            me._lockSlide = true;
                            e.preventDefault();
                        }
                    });
                }

                if (opts.pinchToZoom) {
                    me._on($slide, 'mousewheel DOMMouseScroll scroll', $.proxy(me.onScroll, me));
                }

                if (opts.doubleTap) {
                    me._on($slide, 'dblclick', $.proxy(me.onDoubleClick, me));
                }
            }

            if (opts.arrowControls) {
                me._on(me._$arrowLeft, 'click touchstart', $.proxy(me.onLeftArrowClick, me));
                me._on(me._$arrowRight, 'click touchstart', $.proxy(me.onRightArrowClick, me));
            }

            if (opts.thumbnails) {
                me._$thumbnails.each($.proxy(me.applyClickEventHandler, me));

                me._on(me._$thumbnailArrowPrev, 'click touchstart', $.proxy(me.onThumbnailPrevArrowClick, me));
                me._on(me._$thumbnailArrowNext, 'click touchstart', $.proxy(me.onThumbnailNextArrowClick, me));

                if (opts.touchControls) {
                    me._on(me._$thumbnailSlide, 'touchstart', $.proxy(me.onThumbnailSlideTouch, me));
                    me._on(me._$thumbnailSlide, 'touchmove', $.proxy(me.onThumbnailSlideMove, me));
                }
            }

            if (opts.dotNavigation && me._$dots) {
                me._$dots.each($.proxy(me.applyClickEventHandler, me));
            }

            if (opts.autoSlide) {
                me.startAutoSlide();

                me._on(me.$el, 'mouseenter', $.proxy(me.stopAutoSlide, me));
                me._on(me.$el, 'mouseleave', $.proxy(me.startAutoSlide, me));
            }

            StateManager.on('resize', me.onResize, me);

            $.publish('plugin/swImageSlider/onRegisterEvents', [ me ]);
        },

        /**
         * Will be called when the user starts touching the image slider.
         * Checks if the user is double tapping the image.
         *
         * @event onTouchStart
         * @param {jQuery.Event} event
         */
        onTouchStart: function (event) {
            var me = this,
                opts = me.opts,
                pointers = me.getPointers(event),
                pointerA = pointers[0],
                currTime = Date.now(),
                startPoint = me._startTouchPoint,
                startX = startPoint.x,
                startY = startPoint.y,
                distance,
                deltaX,
                deltaY;

            startPoint.set(pointerA.clientX, pointerA.clientY);

            if (pointers.length === 1) {
                me._lastMoveTime = currTime;

                if (opts.autoSlide) {
                    me.stopAutoSlide();
                }

                if (event.originalEvent instanceof MouseEvent) {
                    event.preventDefault();

                    me._grabImage = true;
                    me._$slideContainer.addClass(opts.dragClass);
                    return;
                }

                if (!opts.doubleTap) {
                    return;
                }

                deltaX = Math.abs(pointerA.clientX - startX);
                deltaY = Math.abs(pointerA.clientY - startY);

                distance = Math.sqrt(deltaX * deltaX + deltaY * deltaY);

                if (currTime - me._lastTouchTime < opts.doubleTapPeriod && distance <= opts.moveTolerance) {
                    me.onDoubleClick(event);
                    return;
                }

                me._lastTouchTime = currTime;
            } else {
                event.preventDefault();
            }
        },

        /**
         * Will be called when the user is moving the finger while touching
         * the image slider.
         *
         * When only one finger is touching the screen
         * and the image was scaled, it will be translated (moved).
         *
         * If two fingers are available, the image will be zoomed (pinch to zoom).
         *
         * @event onTouchMove
         * @param {jQuery.Event} event
         */
        onTouchMove: function (event) {
            var me = this,
                opts = me.opts,
                touches = me.getPointers(event),
                touchA = touches[0],
                touchB = touches[1],
                scale = me._imageScale,
                startTouch = me._startTouchPoint,
                touchDistance = me._touchDistance,
                slideStyle = me._$slide[0].style,
                percentage,
                offset,
                distance,
                deltaX,
                deltaY;

            if (touches.length > 2) {
                return;
            }

            if (touches.length === 1) {

                if (event.originalEvent instanceof MouseEvent && !me._grabImage) {
                    return;
                }

                deltaX = touchA.clientX - startTouch.x;
                deltaY = touchA.clientY - startTouch.y;

                if (scale === 1) {
                    if (me._lockSlide) {
                        return;
                    }

                    offset = (me._slideIndex * -100);
                    percentage = (deltaX / me._$slide.width()) * 100;

                    if (me._slideIndex === 0 && deltaX > 0) {
                        percentage *= Math.atan(percentage) / Math.PI;
                    }

                    if (me._slideIndex === me._itemCount - 1 && deltaX < 0) {
                        percentage *= Math.atan(percentage) / -Math.PI;
                    }

                    if (transitionProperty && transformProperty) {
                        slideStyle[transitionProperty] = 'none';
                        slideStyle[transformProperty] = 'translateX(' + (offset + percentage) + '%)';
                    } else {
                        slideStyle.left = (offset + percentage) + '%';
                    }

                    if (opts.preventScrolling) {
                        event.preventDefault();
                    }
                    return;
                }

                // If the image is zoomed, move it
                startTouch.set(touchA.clientX, touchA.clientY);

                me.translate(deltaX / scale, deltaY / scale);

                event.preventDefault();
                return;
            }

            if (!opts.pinchToZoom || !touchB) {
                return;
            }

            deltaX = Math.abs(touchA.clientX - touchB.clientX);
            deltaY = Math.abs(touchA.clientY - touchB.clientY);

            distance = Math.sqrt(deltaX * deltaX + deltaY * deltaY);

            if (touchDistance === 0) {
                me._touchDistance = distance;
                return;
            }

            me.scale((distance - touchDistance) / 100);

            me._touchDistance = distance;
        },

        /**
         * Will be called when the user ends touching the image slider.
         * If the swipeToSlide option is active and the swipe tolerance is
         * exceeded, it will slide to the previous / next image.
         *
         * @event onTouchEnd
         * @param {jQuery.Event} event
         */
        onTouchEnd: function (event) {
            var me = this,
                opts = me.opts,
                touches = event.changedTouches,
                remaining = event.originalEvent.touches,
                touchA = (touches && touches[0]) || event.originalEvent,
                touchB = remaining && remaining[0],
                swipeTolerance = opts.swipeTolerance,
                pullTolerance = (typeof opts.pullTolerance === 'number') ? opts.pullTolerance : me._$slide.width() / 3,
                startPoint = me._startTouchPoint,
                deltaX,
                deltaY,
                absX,
                absY,
                swipeValid,
                pullValid;

            if (event.originalEvent instanceof MouseEvent && !me._grabImage) {
                return;
            }

            me._touchDistance = 0;
            me._grabImage = false;
            me._$slideContainer.removeClass(opts.dragClass);
            me._lockSlide = false;

            if (touchB) {
                startPoint.set(touchB.clientX, touchB.clientY);
                return;
            }

            if (opts.autoSlide) {
                me.startAutoSlide();
            }

            if (!opts.swipeToSlide || me._imageScale > 1) {
                return;
            }

            deltaX = startPoint.x - touchA.clientX;
            deltaY = startPoint.y - touchA.clientY;
            absX = Math.abs(deltaX);
            absY = Math.abs(deltaY);

            swipeValid = (Date.now() - me._lastMoveTime) < opts.swipePeriod && absX > swipeTolerance && absY < swipeTolerance;
            pullValid = (absX >= pullTolerance);

            if (Math.sqrt(deltaX * deltaX + deltaY * deltaY) > opts.moveTolerance) {
                event.preventDefault();
            }

            if (pullValid || swipeValid) {
                (deltaX < 0) ? me.slidePrev() : me.slideNext();
                return;
            }

            me.slide(me._slideIndex);
        },

        /**
         * Will be called when the user clicks on the slide.
         * This event will cancel its bubbling when the move tolerance
         * was exceeded.
         *
         * @event onClick
         * @param {jQuery.Event} event
         */
        onClick: function (event) {
            var me = this,
                opts = me.opts,
                touches = event.changedTouches,
                touchA = (touches && touches[0]) || event.originalEvent,
                startPoint = me._startTouchPoint,
                deltaX = startPoint.x - touchA.clientX,
                deltaY = startPoint.y - touchA.clientY;

            if (Math.sqrt(deltaX * deltaX + deltaY * deltaY) > opts.moveTolerance) {
                event.preventDefault();
                event.stopImmediatePropagation();
            }

            $.publish('plugin/swImageSlider/onClick', [ me, event ]);
        },

        /**
         * Will be called when the user scrolls the image by the mouse.
         * Zooms the image in/out by the factor 0.25.
         *
         * @event onScroll
         * @param {jQuery.Event} event
         */
        onScroll: function (event) {
            var me = this,
                e = event.originalEvent;

            if ((e.detail ? e.detail * -1 : e.wheelDelta) > 0) {
                me.scale(0.25);
            } else {
                me.scale(-0.25);
            }

            event.preventDefault();

            $.publish('plugin/swImageSlider/onScroll', [ me, event ]);
        },

        /**
         * Will be called when the user
         * double clicks or double taps on the image slider.
         * When the image was scaled, it will reset its scaling
         * otherwise it will zoom in by the factor of 1.
         *
         * @event onDoubleClick
         * @param {jQuery.Event} event
         */
        onDoubleClick: function (event) {
            var me = this;

            if (!me.opts.doubleTap) {
                return;
            }

            event.preventDefault();

            if (me._imageScale <= 1) {
                me.scale(1, true);
            } else {
                me.setScale(1, true);
            }

            $.publish('plugin/swImageSlider/onDoubleClick', [ me, event ]);
        },

        /**
         * Is triggered when the left arrow
         * of the image slider is clicked or tapped.
         *
         * @event onLeftArrowClick
         * @param {jQuery.Event} event
         */
        onLeftArrowClick: function (event) {
            var me = this;

            event.preventDefault();

            me.slidePrev();

            $.publish('plugin/swImageSlider/onLeftArrowClick', [ me, event ]);
        },

        /**
         * Is triggered when the right arrow
         * of the image slider is clicked or tapped.
         *
         * @event onRightArrowClick
         * @param {jQuery.Event} event
         */
        onRightArrowClick: function (event) {
            var me = this;

            event.preventDefault();

            me.slideNext();

            $.publish('plugin/swImageSlider/onRightArrowClick', [ me, event ]);
        },

        /**
         * Slides the thumbnail slider one position backwards.
         *
         * @event onThumbnailPrevArrowClick
         * @param {jQuery.Event} event
         */
        onThumbnailPrevArrowClick: function (event) {
            event.preventDefault();

            var me = this,
                $container = me._$thumbnailContainer,
                size = me._thumbnailOrientation === 'horizontal' ? $container.innerWidth() : $container.innerHeight();

            me.setThumbnailSlidePosition(me._thumbnailOffset + (size / 2), true);
        },

        /**
         * Slides the thumbnail slider one position forward.
         *
         * @event onThumbnailNextArrowClick
         * @param {jQuery.Event} event
         */
        onThumbnailNextArrowClick: function (event) {
            event.preventDefault();

            var me = this,
                $container = me._$thumbnailContainer,
                size = me._thumbnailOrientation === 'horizontal' ? $container.innerWidth() : $container.innerHeight();

            me.setThumbnailSlidePosition(me._thumbnailOffset - (size / 2), true);

            $.publish('plugin/swImageSlider/onThumbnailNextArrowClick', [ me, event ]);
        },

        /**
         * Will be called when the user leaves the image slide with the mouse.
         * Resets the cursor grab indicator.
         *
         * @event onMouseLeave
         */
        onMouseLeave: function (event) {
            var me = this;

            me._grabImage = false;
            me._$slideContainer.removeClass(me.opts.dragClass);

            me.slide(me._slideIndex);

            $.publish('plugin/swImageSlider/onMouseLeave', [ me, event ]);
        },

        /**
         * Will be called when the viewport has been resized.
         * When thumbnails are enabled, the trackThumbnailControls function
         * will be called.
         *
         * @event onResize
         */
        onResize: function (newWidth) {
            var me = this;

            me.updateMaxZoomValue();

            me.scale(0);
            me.translate(0, 0);

            if (me.opts.thumbnails) {
                me.trackThumbnailControls();
            }

            $.publish('plugin/swImageSlider/onResize', [ me, newWidth ]);
        },

        /**
         * Will be called when the user starts touching the thumbnails slider.
         *
         * @event onThumbnailSlideTouch
         * @param {jQuery.Event} event
         */
        onThumbnailSlideTouch: function (event) {
            var me = this,
                pointers = me.getPointers(event),
                pointerA = pointers[0];

            me._startTouchPoint.set(pointerA.clientX, pointerA.clientY);

            $.publish('plugin/swImageSlider/onThumbnailSlideTouch', [ me, event, pointerA.clientX, pointerA.clientY ]);
        },

        /**
         * Will be called when the user is moving the finger while touching
         * the thumbnail slider.
         * Slides the thumbnails slider to the left/right depending on the user.
         *
         * @event onThumbnailSlideMove
         * @param {jQuery.Event} event
         */
        onThumbnailSlideMove: function (event) {
            event.preventDefault();

            var me = this,
                pointers = me.getPointers(event),
                pointerA = pointers[0],
                startPoint = me._startTouchPoint,
                isHorizontal = me._thumbnailOrientation === 'horizontal',
                posA = isHorizontal ? pointerA.clientX : pointerA.clientY,
                posB = isHorizontal ? startPoint.x : startPoint.y,
                delta = posA - posB;

            startPoint.set(pointerA.clientX, pointerA.clientY);

            me.setThumbnailSlidePosition(me._thumbnailOffset + delta, false);

            me.trackThumbnailControls();

            $.publish('plugin/swImageSlider/onThumbnailSlideTouch', [ me, event, pointerA.clientX, pointerA.clientY ]);
        },

        /**
         * Returns either an array of touches or a single mouse event.
         * This is a helper function to unify the touch/mouse gesture logic.
         *
         * @private
         * @method getPointers
         * @param {jQuery.Event} event
         */
        getPointers: function (event) {
            var origEvent = event.originalEvent || event;

            return origEvent.touches || [origEvent];
        },

        /**
         * Calculates the new x/y coordinates for the image based by the
         * given scale value.
         *
         * @private
         * @method getTransformedPosition
         * @param {Number} x
         * @param {Number} y
         * @param {Number} scale
         */
        getTransformedPosition: function (x, y, scale) {
            var me = this,
                $image = me._$currentImage,
                $container = me._$slideContainer,
                minX = Math.max(0, (($image.width() * scale - $container.width()) / scale) / 2),
                minY = Math.max(0, (($image.height() * scale - $container.height()) / scale) / 2),
                newPos = new Vector(
                    Math.max(minX * -1, Math.min(minX, x)),
                    Math.max(minY * -1, Math.min(minY, y))
                );

            $.publish('plugin/swImageSlider/onGetTransformedPosition', [ me, newPos, x, y, scale ]);

            return newPos;
        },

        /**
         * Returns the minimum possible zoom factor.
         *
         * @public
         * @method getMinScale
         * @returns {Number}
         */
        getMinScale: function () {
            return this._minZoom;
        },

        /**
         * Returns the maximum possible zoom factor.
         *
         * @public
         * @method getMaxScale
         * @returns {Number}
         */
        getMaxScale: function () {
            return this._maxZoom;
        },

        /**
         * Sets the translation (position) of the current image.
         *
         * @public
         * @method setTranslation
         * @param {Number} x
         * @param {Number} y
         */
        setTranslation: function (x, y) {
            var me = this,
                newPos = me.getTransformedPosition(x, y, me._imageScale);

            me._imageTranslation.set(newPos.x, newPos.y);

            me.updateTransform(false);

            $.publish('plugin/swImageSlider/onSetTranslation', [ me, x, y ]);
        },

        /**
         * Translates the current image relative to the current position.
         * The x/y values will be added together.
         *
         * @public
         * @method translate
         * @param {Number} x
         * @param {Number} y
         */
        translate: function (x, y) {
            var me = this,
                translation = me._imageTranslation;

            me.setTranslation(translation.x + x, translation.y + y);

            $.publish('plugin/swImageSlider/onTranslate', [ me, x, y ]);
        },

        /**
         * Scales the current image to the given scale value.
         * You can also pass the option if it should be animated
         * and if so, you can also pass a callback.
         *
         * @public
         * @method setScale
         * @param {Number|String} scale
         * @param {Boolean} animate
         * @param {Function} callback
         */
        setScale: function (scale, animate, callback) {
            var me = this,
                oldScale = me._imageScale;

            me.updateMaxZoomValue();

            me._imageScale = Math.max(me._minZoom, Math.min(me._maxZoom, scale));

            if (me._imageScale === oldScale) {
                if (typeof callback === 'function') {
                    callback.call(me);
                }
                return;
            }

            me.updateTransform(animate, callback);

            $.publish('plugin/swImageSlider/onSetScale', [ me, scale, animate, callback ]);
        },

        /**
         * Returns the current image scaling.
         *
         * @public
         * @method getScale
         * @returns {Number}
         */
        getScale: function () {
            return this._imageScale;
        },

        /**
         * Scales the current image relative to the current scale value.
         * The factor value will be added to the current scale.
         *
         * @public
         * @method scale
         * @param {Number} factor
         * @param {Boolean} animate
         * @param {Function} callback
         */
        scale: function (factor, animate, callback) {
            var me = this;

            me.setScale(me._imageScale + factor, animate, callback);

            $.publish('plugin/swImageSlider/onScale', [ me, factor, animate, callback ]);
        },

        /**
         * Updates the transformation of the current image.
         * The scale and translation will be considered into this.
         * You can also decide if the update should be animated
         * and if so, you can provide a callback function
         *
         * @public
         * @method updateTransform
         * @param {Boolean} animate
         * @param {Function} callback
         */
        updateTransform: function (animate, callback) {
            var me = this,
                translation = me._imageTranslation,
                scale = me._imageScale,
                newPosition = me.getTransformedPosition(translation.x, translation.y, scale),
                image = me._$currentImage[0],
                animationSpeed = me.opts.animationSpeed;

            translation.set(newPosition.x, newPosition.y);

            image.style[transitionProperty] = animate ? ('all ' + animationSpeed + 'ms') : '';

            image.style[transformProperty] = 'scale(' + scale + ') translate(' + translation.x + 'px, ' + translation.y + 'px)';

            $.publish('plugin/swImageSlider/onUpdateTransform', [ me, animate, callback ]);

            if (!callback) {
                return;
            }

            if (!animate) {
                callback.call(me);
                return;
            }

            setTimeout($.proxy(callback, me), animationSpeed);
        },

        /**
         * Applies a click event handler to the element
         * to slide the slider to the index of that element.
         *
         * @private
         * @method applyClickEventHandler
         * @param {Number} index
         * @param {HTMLElement} el
         */
        applyClickEventHandler: function (index, el) {
            var me = this,
                $el = $(el),
                i = index || $el.index();

            me._on($el, 'click', function (event) {
                event.preventDefault();
                me.slide(i);
            });

            $.publish('plugin/swImageSlider/onApplyClickEventHandler', [ me, index, el ]);
        },

        /**
         * Creates the arrow controls for the image slider.
         *
         * @private
         * @method createArrows
         */
        createArrows: function () {
            var me = this,
                opts = me.opts,
                hiddenClass = ' ' + opts.hiddenClass;

            /**
             * Left slide arrow element.
             *
             * @private
             * @property _$arrowLeft
             * @type {jQuery}
             */
            me._$arrowLeft = $('<a>', {
                'class': opts.leftArrowCls + ((opts.loopSlides || me._slideIndex > 0) && me._itemCount > 1 ? '' : hiddenClass)
            }).appendTo(me._$slideContainer);

            /**
             * Right slide arrow element.
             *
             * @private
             * @property _$arrowRight
             * @type {jQuery}
             */
            me._$arrowRight = $('<a>', {
                'class': opts.rightArrowCls + ((opts.loopSlides || me._slideIndex < me._itemCount - 1) && me._itemCount > 1 ? '' : hiddenClass)
            }).appendTo(me._$slideContainer);

            $.publish('plugin/swImageSlider/onCreateArrows', [ me, me._$arrowLeft, me._$arrowRight ]);
        },

        /**
         * Creates the thumbnail arrow controls for the thumbnail slider.
         *
         * @private
         * @method createThumbnailArrows
         */
        createThumbnailArrows: function () {
            var me = this,
                opts = me.opts,
                isHorizontal = (me._thumbnailOrientation === 'horizontal'),
                prevClass = isHorizontal ? opts.thumbnailArrowLeftCls : opts.thumbnailArrowTopCls,
                nextClass = isHorizontal ? opts.thumbnailArrowRightCls : opts.thumbnailArrowBottomCls;

            /**
             * Left/Top thumbnail slide arrow element.
             *
             * @private
             * @property _$thumbnailArrowPrev
             * @type {jQuery}
             */
            me._$thumbnailArrowPrev = $('<a>', {
                'class': opts.thumbnailArrowCls + ' ' + prevClass
            }).appendTo(me._$thumbnailContainer);

            /**
             * Right/Bottom thumbnail slide arrow element.
             *
             * @private
             * @property _$thumbnailArrowNext
             * @type {jQuery}
             */
            me._$thumbnailArrowNext = $('<a>', {
                'class': opts.thumbnailArrowCls + ' ' + nextClass
            }).appendTo(me._$thumbnailContainer);

            $.publish('plugin/swImageSlider/onCreateThumbnailArrows', [ me, me._$thumbnailArrowPrev, me._$thumbnailArrowNext ]);
        },

        /**
         * Tracks and counts the image elements and the thumbnail elements.
         *
         * @private
         * @method trackItems
         */
        trackItems: function () {
            var me = this,
                opts = me.opts;

            /**
             * This property contains every item in the slide.
             *
             * @private
             * @property _$items
             * @type {jQuery}
             */
            me._$items = me._$slide.find(opts.itemSelector);

            picturefill();

            /**
             * This property contains every item in the slide.
             *
             * @private
             * @property _$images
             * @type {jQuery}
             */
            me._$images = me._$slide.find(opts.imageSelector);

            if (opts.thumbnails) {

                /**
                 * Array of all thumbnail elements.
                 *
                 * @private
                 * @property _$thumbnails
                 * @type {jQuery}
                 */
                me._$thumbnails = me._$thumbnailContainer.find(opts.thumbnailSelector);

                /**
                 * Amount of all thumbnails.
                 *
                 * @private
                 * @property _thumbnailCount
                 * @type {Number}
                 */
                me._thumbnailCount = me._$thumbnails.length;

                if (me._thumbnailCount === 0) {
                    me.$el.addClass(opts.noThumbClass);
                    opts.thumbnails = false;
                }
            }

            /**
             * This property contains every item in the slide.
             *
             * @private
             * @property _itemCount
             * @type {jQuery}
             */
            me._itemCount = me._$items.length;

            $.publish('plugin/swImageSlider/onTrackItems', [ me ]);
        },

        /**
         * Sets the position of the image slide to the given image index.
         *
         * @public
         * @method setIndex
         * @param {Number} index
         */
        setIndex: function (index) {
            var me = this,
                slideStyle = me._$slide[0].style,
                percentage = ((index || me._slideIndex) * -100);

            if (transformProperty && transitionProperty) {
                slideStyle[transitionProperty] = 'none';
                slideStyle[transformProperty] = 'translateX(' + percentage + '%)';
            } else {
                slideStyle.left = percentage + '%';
            }

            me._$currentImage = $(me._$images[index]);

            me.updateMaxZoomValue();

            $.publish('plugin/swImageSlider/onSetIndex', [ me, index ]);
        },

        /**
         * Returns the current slide index.
         *
         * @public
         * @method getIndex
         * @returns {Number}
         */
        getIndex: function (event) {
            return this._slideIndex;
        },

        /**
         * Updates the max zoom factor specific to the current image.
         *
         * @private
         * @method updateMaxZoomValue
         */
        updateMaxZoomValue: function () {
            var me = this,
                $currentImage = me._$currentImage,
                image = $currentImage[0];

            if (!me._autoScale) {
                return;
            }

            if (!image) {
                me._maxZoom = me._minZoom;
                return;
            }

            me._maxZoom = Math.max(image.naturalWidth, image.naturalHeight) / Math.max($currentImage.width(), $currentImage.height());

            $.publish('plugin/swImageSlider/onUpdateMaxZoomValue', [ me, me._maxZoom ]);
        },

        /**
         * Returns the orientation of the thumbnail container.
         *
         * @private
         * @method getThumbnailOrientation
         * @returns {String}
         */
        getThumbnailOrientation: function () {
            var $container = this._$thumbnailContainer;

            return ($container.innerWidth() > $container.innerHeight()) ? 'horizontal' : 'vertical';
        },

        /**
         * Sets the active state for the thumbnail at the given index position.
         *
         * @public
         * @method setActiveThumbnail
         * @param {Number} index
         */
        setActiveThumbnail: function (index) {
            var me = this,
                isHorizontal = me._thumbnailOrientation === 'horizontal',
                orientation = isHorizontal ? 'left' : 'top',
                $thumbnail = me._$thumbnails.eq(index),
                $container = me._$thumbnailContainer,
                thumbnailPos = $thumbnail.position(),
                slidePos = me._$thumbnailSlide.position(),
                slideOffset = slidePos[orientation],
                posA = thumbnailPos[orientation] * -1,
                posB = thumbnailPos[orientation] + (isHorizontal ? $thumbnail.outerWidth() : $thumbnail.outerHeight()),
                containerSize = isHorizontal ? $container.width() : $container.height(),
                activeClass = me.opts.activeStateClass,
                newPos;

            if (posA < slideOffset && posB * -1 < slideOffset + (containerSize * -1)) {
                newPos = containerSize - Math.max(posB, containerSize);
            } else {
                newPos = Math.max(posA, slideOffset);
            }

            me._$thumbnails.removeClass(activeClass);

            $thumbnail.addClass(activeClass);

            me.setThumbnailSlidePosition(newPos, true);

            $.publish('plugin/swImageSlider/onSetActiveThumbnail', [ me, index ]);
        },

        /**
         * Sets the active state for the dot at the given index position.
         *
         * @public
         * @method setActiveDot
         * @param {Number} index
         */
        setActiveDot: function (index) {
            var me = this,
                $dots = me._$dots;

            if (me.opts.dotNavigation && $dots) {
                $dots.removeClass(me.opts.activeStateClass);
                $dots.eq(index || me._slideIndex).addClass(me.opts.activeStateClass);
            }

            $.publish('plugin/swImageSlider/onSetActiveDot', [ me, index ]);
        },

        /**
         * Sets the position of the thumbnails slider
         * If the offset exceeds the minimum/maximum position, it will be culled
         *
         * @public
         * @method setThumbnailSlidePosition
         * @param {Number} offset
         * @param {Boolean} animate
         */
        setThumbnailSlidePosition: function (offset, animate) {
            var me = this,
                $slide = me._$thumbnailSlide,
                $container = me._$thumbnailContainer,
                isHorizontal = me._thumbnailOrientation === 'horizontal',
                sizeA = isHorizontal ? $container.innerWidth() : $container.innerHeight(),
                sizeB = isHorizontal ? $slide.outerWidth(true) : $slide.outerHeight(true),
                min = Math.min(0, sizeA - sizeB),
                css = {};

            me._thumbnailOffset = Math.max(min, Math.min(0, offset));

            css[isHorizontal ? 'left' : 'top'] = me._thumbnailOffset;
            css[isHorizontal ? 'top' : 'left'] = 'auto';

            if (!animate) {
                $slide.css(css);
            } else {
                $slide[Modernizr.csstransitions ? 'transition' : 'animate'](css, me.animationSpeed, $.proxy(me.trackThumbnailControls, me));
            }

            $.publish('plugin/swImageSlider/onSetThumbnailSlidePosition', [ me, offset, animate ]);
        },

        /**
         * Checks which thumbnail arrow controls have to be shown.
         *
         * @private
         * @method trackThumbnailControls
         */
        trackThumbnailControls: function () {
            var me = this,
                opts = me.opts,
                isHorizontal = me._thumbnailOrientation === 'horizontal',
                $container = me._$thumbnailContainer,
                $slide = me._$thumbnailSlide,
                $prevArr = me._$thumbnailArrowPrev,
                $nextArr = me._$thumbnailArrowNext,
                activeCls = me.opts.activeStateClass,
                pos = $slide.position(),
                orientation = me.getThumbnailOrientation();

            if (me._thumbnailOrientation !== orientation) {

                $prevArr
                    .toggleClass(opts.thumbnailArrowLeftCls, !isHorizontal)
                    .toggleClass(opts.thumbnailArrowTopCls, isHorizontal);

                $nextArr
                    .toggleClass(opts.thumbnailArrowRightCls, !isHorizontal)
                    .toggleClass(opts.thumbnailArrowBottomCls, isHorizontal);

                me._thumbnailOrientation = orientation;

                me.setActiveThumbnail(me._slideIndex);
            }

            if (me._thumbnailOrientation === 'horizontal') {
                $prevArr.toggleClass(activeCls, pos.left < 0);
                $nextArr.toggleClass(activeCls, ($slide.innerWidth() + pos.left) > $container.innerWidth());
            } else {
                $prevArr.toggleClass(activeCls, pos.top < 0);
                $nextArr.toggleClass(activeCls, ($slide.innerHeight() + pos.top) > $container.innerHeight());
            }

            $.publish('plugin/swImageSlider/onTrackThumbnailControls', [ me ]);
        },

        /**
         * Starts the auto slide interval.
         *
         * @private
         * @method startAutoSlide
         */
        startAutoSlide: function () {
            var me = this;

            me.stopAutoSlide(me._slideInterval);

            me._slideInterval = window.setTimeout($.proxy(me.slideNext, me), me.opts.autoSlideInterval);

            $.publish('plugin/swImageSlider/onStartAutoSlide', [ me, me._slideInterval ]);
        },

        /**
         * Stops the auto slide interval.
         *
         * @private
         * @method stopAutoSlide
         */
        stopAutoSlide: function () {
            var me = this;

            window.clearTimeout(me._slideInterval);

            $.publish('plugin/swImageSlider/onStopAutoSlide', [ me ]);
        },

        /**
         * Slides the image slider to the given index position.
         *
         * @public
         * @method slide
         * @param {Number} index
         * @param {Function} callback
         */
        slide: function (index, callback) {
            var me = this,
                opts = me.opts,
                slideStyle = me._$slide[0].style;

            me._slideIndex = index;

            if (opts.thumbnails) {
                me.setActiveThumbnail(index);
                me.trackThumbnailControls();
            }

            if (opts.dotNavigation && me._$dots) {
                me.setActiveDot(index);
            }

            if (opts.autoSlide) {
                me.stopAutoSlide();
                me.startAutoSlide();
            }

            me.resetTransformation(true, function () {
                if (transitionProperty && transformProperty) {
                    slideStyle[transitionProperty] = 'all ' + opts.animationSpeed + 'ms ' + opts.animationEasing;
                    slideStyle[transformProperty] = 'translateX(' + (index * -100) + '%)';

                    if (typeof callback === 'function') {
                        setTimeout($.proxy(callback, me), opts.animationSpeed);
                    }
                } else {
                    me._$slide.animate({
                        'left': (index * -100) + '%',
                        'easing': 'ease-out'
                    }, opts.animationSpeed, $.proxy(callback, me));
                }
            });

            me._$currentImage = $(me._$images[index]);

            me.updateMaxZoomValue();

            if (opts.arrowControls) {
                me._$arrowLeft.toggleClass(opts.hiddenClass, !opts.loopSlides && index <= 0);
                me._$arrowRight.toggleClass(opts.hiddenClass, !opts.loopSlides && index >= me._itemCount - 1);
            }

            $.publish('plugin/swImageSlider/onSlide', [ me, index, callback ]);
        },

        /**
         * Resets the current image transformation (scale and translation).
         * Can also be animated.
         *
         * @public
         * @method resetTransformation
         * @param {Boolean} animate
         * @param {Function} callback
         */
        resetTransformation: function (animate, callback) {
            var me = this,
                translation = me._imageTranslation;

            me._touchDistance = 0;

            if (me._imageScale !== 1 || translation.x !== 0 || translation.y !== 0) {

                me._imageScale = 1;

                me._imageTranslation.set(0, 0);

                me.updateTransform(animate, callback);

            } else if (callback) {
                callback.call(me);
            }

            $.publish('plugin/swImageSlider/onResetTransformation', [ me, animate, callback ]);
        },

        /**
         * Slides the image slider one position forward.
         *
         * @public
         * @method slideNext
         */
        slideNext: function () {
            var me = this,
                newIndex = me._slideIndex + 1,
                itemCount = me._itemCount,
                isLooping = me.opts.loopSlides;

            me._lastTouchTime = 0;

            me.slide((newIndex >= itemCount && isLooping) ? 0 : Math.min(itemCount - 1, newIndex));

            $.publish('plugin/swImageSlider/onSlideNext', [ me, newIndex ]);
        },

        /**
         * Slides the image slider one position backwards.
         *
         * @public
         * @method slidePrev
         */
        slidePrev: function () {
            var me = this,
                newIndex = me._slideIndex - 1,
                itemCount = me._itemCount,
                isLooping = me.opts.loopSlides;

            me._lastTouchTime = 0;

            me.slide((newIndex < 0 && isLooping) ? itemCount - 1 : Math.max(0, newIndex));

            $.publish('plugin/swImageSlider/onSlidePrev', [ me, newIndex ]);
        },

        /**
         * Destroys the plugin and removes
         * all elements created by the plugin.
         *
         * @public
         * @method destroy
         */
        destroy: function () {
            var me = this,
                opts = me.opts;

            me.resetTransformation(false);

            me._$slideContainer = null;
            me._$items = null;
            me._$currentImage = null;

            if (opts.dotNavigation && me._$dots) {
                me._$dots.removeClass(me.opts.activeStateClass);
                me._$dotNav = null;
                me._$dots = null;
            }

            if (opts.arrowControls) {
                me._$arrowLeft.remove();
                me._$arrowRight.remove();
            }

            if (opts.thumbnails) {
                me._$thumbnailArrowPrev.remove();
                me._$thumbnailArrowNext.remove();

                me._$thumbnailContainer = null;
                me._$thumbnailSlide = null;

                me._$thumbnails.removeClass(me.opts.activeStateClass);
                me._$thumbnails = null;
            }

            if (opts.autoSlide) {
                me.stopAutoSlide();
            }

            StateManager.off('resize', me.onResize, me);

            me._destroy();
        }
    });

    /**
     * Helper Class to manager coordinates of X and Y pair values.
     *
     * @class Vector
     * @constructor
     * @param {Number} x
     * @param {Number} y
     */
    function Vector(x, y) {
        var me = this;

        me.x = x || 0;
        me.y = y || 0;
    }

    /**
     * Sets the X and Y values.
     * If one of the passed parameter is not a number, it
     * will be ignored.
     *
     * @public
     * @method set
     * @param {Number} x
     * @param {Number} y
     */
    Vector.prototype.set = function (x, y) {
        var me = this;

        me.x = (typeof x === 'number') ? x : me.x;
        me.y = (typeof y === 'number') ? y : me.y;
    };
})(jQuery, Modernizr, window, Math);

;(function ($) {
    'use strict';

    /**
     * Shopware Image Zoom Plugin.
     *
     * Creates a zoomed view of a product image.
     * You can move a lens object over the original image to
     * see the zoomed view of the hovered area.
     */
    $.plugin('swImageZoom', {

        defaults: {

            /* Setting for showing the image title in the zoom view */
            showTitle: true,

            /* The css class for the container element which contains the image */
            containerCls: 'js--img-zoom--container',

            /* The css class for the lens element which displays the current zoom viewport */
            lensCls: 'js--img-zoom--lens',

            /* The css class for the container where the zoomed image is viewed */
            flyoutCls: 'js--img-zoom--flyout',

            /* The css class for the container if the image title */
            titleContainerCls: 'js--img-zoom--title',

            /* The selector for identifying the active image */
            activeSelector: '.is--active',

            /* The speed for animations in ms */
            animationSpeed: 300
        },

        /**
         * Initializes the plugin.
         */
        init: function () {
            var me = this;

            me.applyDataAttributes();

            me.active = false;

            me.$container = me.$el.find('.image-slider--slide');
            me.imageBox = me.$el.find('.image--box');
            me.$imageElements = me.$el.find('.image--element');
            me.$thumbnails = me.$el.find('.thumbnail--link');

            me.$flyout = me.createFlyoutElement();
            me.$lens = me.createLensElement();

            if (me.opts.showTitle) {
                me.$title = me.createTitleContainer();
            }

            me.zoomImage = false;

            me.$activeImage = me.getActiveImageElement();

            me.flyoutWidth = me.$flyout.outerWidth();
            me.flyoutHeight = me.$flyout.outerHeight();

            me.registerEvents();
        },

        /**
         * Registers all necessary event listeners.
         */
        registerEvents: function() {
            var me = this;

            $('body').on('scroll.imageZoom', $.proxy(me.stopZoom, me));

            me._on(me.$container, 'mousemove', $.proxy(me.onMouseMove, me));
            me._on(me.$container, 'mouseout', $.proxy(me.stopZoom, me));
            me._on(me.$lens, 'click', $.proxy(me.onLensClick, me));

            $.subscribe('plugin/swImageSlider/onRightArrowClick', $.proxy(me.stopZoom, me));
            $.subscribe('plugin/swImageSlider/onLeftArrowClick', $.proxy(me.stopZoom, me));
            $.subscribe('plugin/swImageSlider/onClick', $.proxy(me.stopZoom, me));
            $.subscribe('plugin/swImageSlider/onLightbox', $.proxy(me.stopZoom, me));

            $.publish('plugin/swImageZoom/onRegisterEvents', [ me ]);
        },

        /**
         * Creates the dom element for the lens.
         *
         * @returns {*}
         */
        createLensElement: function() {
            var me = this,
                $el = $('<div>', {
                    'class': me.opts.lensCls,
                    'html': '&nbsp;'
                }).appendTo(me.$container);

            $.publish('plugin/swImageZoom/onCreateLensElement', [me, $el]);

            return $el;
        },

        /**
         * Creates the flyout element in
         * which the zoomed image will be shown.
         *
         * @returns {*}
         */
        createFlyoutElement: function() {
            var me = this,
                $el = $('<div>', {
                    'class': me.opts.flyoutCls
                }).appendTo(me.$el);

            $.publish('plugin/swImageZoom/onCreateFlyoutElement', [me, $el]);

            return $el;
        },

        /**
         * Creates the container element
         * for the image title in the zoom view.
         *
         * @returns {*}
         */
        createTitleContainer: function() {
            var me = this,
                $el;

            if (!me.$flyout.length || !me.opts.showTitle) {
                return;
            }

            $el = $('<div>', {
                'class': me.opts.titleContainerCls
            }).appendTo(me.$flyout);

            $.publish('plugin/swImageZoom/onCreateTitleContainer', [me, $el]);

            return $el;
        },

        /**
         * Returns the thumbnail of the
         * current active image.
         *
         * @returns {*|Array}
         */
        getActiveImageThumbnail: function() {
            var me = this,
                $thumbnail = me.$thumbnails.filter(me.opts.activeSelector);

            $.publish('plugin/swImageZoom/onGetActiveImageThumbnail', [me, $thumbnail]);

            return $thumbnail;
        },

        /**
         * Returns the image element of
         * the current active image.
         *
         * @returns {*}
         */
        getActiveImageElement: function() {
            var me = this,
                $el;

            me.$activeImageThumbnail = me.getActiveImageThumbnail();

            if (!me.$activeImageThumbnail.length) {
                $el = me.$imageElements.eq(0);
            } else {
                $el = me.$imageElements.eq(me.$activeImageThumbnail.index());
            }

            $.publish('plugin/swImageZoom/onGetActiveImageElement', [me, $el]);

            return $el;
        },

        /**
         * Computes and sets the size of
         * the lens element based on the factor
         * between the image and the zoomed image.
         *
         * @param factor
         */
        setLensSize: function(factor) {
            var me = this;

            me.lensWidth = me.flyoutWidth / factor;
            me.lensHeight = me.flyoutHeight / factor;

            if (me.lensWidth > me.imageWidth) {
                me.lensWidth = me.imageWidth;
            }

            if (me.lensHeight > me.imageHeight) {
                me.lensHeight = me.imageHeight;
            }

            me.$lens.css({
                'width': me.lensWidth,
                'height': me.lensHeight
            });

            $.publish('plugin/swImageZoom/onSetLensSize', [me, me.$lens, factor]);
        },

        /**
         * Sets the lens position over
         * the original image.
         *
         * @param x
         * @param y
         */
        setLensPosition: function(x, y) {
            var me = this;

            me.$lens.css({
                'top': y,
                'left': x
            });

            $.publish('plugin/swImageZoom/onSetLensPosition', [me, me.$lens, x, y]);
        },

        /**
         * Makes the lens element visible.
         */
        showLens: function() {
            var me = this;

            me.$lens.stop(true, true).fadeIn(me.opts.animationSpeed);

            $.publish('plugin/swImageZoom/onShowLens', [me, me.$lens]);
        },

        /**
         * Hides the lens element.
         */
        hideLens: function() {
            var me = this;

            me.$lens.stop(true, true).fadeOut(me.opts.animationSpeed);

            $.publish('plugin/swImageZoom/onHideLens', [me, me.$lens]);
        },

        /**
         * Sets the position of the zoomed image area.
         *
         * @param x
         * @param y
         */
        setZoomPosition: function(x, y) {
            var me = this;

            me.$flyout.css('backgroundPosition', x+'px '+y+'px');

            $.publish('plugin/swImageZoom/onSetZoomPosition', [me, me.$flyout, x, y]);
        },

        /**
         * Makes the zoom view visible.
         */
        showZoom: function() {
            var me = this;

            me.$flyout.stop(true, true).fadeIn(me.opts.animationSpeed);

            $.publish('plugin/swImageZoom/onShowZoom', [me, me.$flyout]);
        },

        /**
         * Hides the zoom view.
         */
        hideZoom: function() {
            var me = this;

            me.$flyout.stop(true, true).fadeOut(me.opts.animationSpeed);

            $.publish('plugin/swImageZoom/onHideZoom', [me, me.$flyout]);
        },

        /**
         * Sets the title of the zoom view.
         *
         * @param title
         */
        setImageTitle: function(title) {
            var me = this;

            if (!me.opts.showTitle || !me.$title.length) {
                return;
            }

            me.$title.html('<span>' + (title || me.imageTitle) + '</span>');

            $.publish('plugin/swImageZoom/onSetImageTitle', [me, me.$title, title]);
        },

        /**
         * Eventhandler for handling the
         * mouse movement on the image container.
         *
         * @param event
         */
        onMouseMove: function(event) {
            var me = this;

            if (!me.zoomImage) {
                me.activateZoom();
                return;
            }

            var containerOffset = me.$container.offset(),
                mouseX = event.pageX,
                mouseY = event.pageY,
                containerX = mouseX - containerOffset.left,
                containerY = mouseY - containerOffset.top,
                lensX = containerX - (me.lensWidth / 2),
                lensY = containerY - (me.lensHeight / 2),
                minX = me.imageOffset.left - containerOffset.left,
                minY = me.imageOffset.top - containerOffset.top,
                maxX = minX + me.imageWidth - me.$lens.outerWidth(),
                maxY = minY + me.imageHeight - me.$lens.outerHeight(),
                lensLeft = me.clamp(lensX, minX, maxX),
                lensTop = me.clamp(lensY, minY, maxY),
                zoomLeft = -(lensLeft - minX) * me.factor,
                zoomTop = -(lensTop - minY) * me.factor;

            if (minX >= maxX) {
                zoomLeft = zoomLeft + (me.flyoutWidth / 2) - (me.zoomImage.width / 2);
            }

            if (minY >= maxY) {
                zoomTop = zoomTop + (me.flyoutHeight / 2) - (me.zoomImage.height / 2);
            }

            if (mouseX > me.imageOffset.left && mouseX < me.imageOffset.left + me.imageWidth &&
                mouseY > me.imageOffset.top && mouseY < me.imageOffset.top + me.imageHeight) {
                me.showLens();
                me.showZoom();
                me.setLensPosition(lensLeft, lensTop);
                me.setZoomPosition(zoomLeft, zoomTop);
            } else {
                me.stopZoom();
            }
        },

        /**
         * Sets the active image element
         * for the zoom view.
         */
        setActiveImage: function() {
            var me = this;

            me.$activeImageElement = me.getActiveImageElement();
            me.$activeImage = me.$activeImageElement.find('img');

            me.imageTitle = me.$activeImageElement.attr('data-alt');
            me.imageWidth = me.$activeImage.innerWidth();
            me.imageHeight = me.$activeImage.innerHeight();
            me.imageOffset = me.$activeImage.offset();

            $.publish('plugin/swImageZoom/onSetActiveImage', me);
        },

        /**
         * Activates the zoom view.
         */
        activateZoom: function() {
            var me = this;

            me.setActiveImage();

            if (!me.zoomImage) {
                me.zoomImageUrl = me.$activeImageElement.attr('data-img-original');
                me.zoomImage = new Image();

                me.zoomImage.onload = function() {
                    me.factor = me.zoomImage.width / me.$activeImage.innerWidth();

                    me.setLensSize(me.factor);
                    me.$flyout.css('background', 'url(' + me.zoomImageUrl + ') 0px 0px no-repeat #fff');

                    if (me.opts.showTitle) {
                        me.setImageTitle(me.title);
                    }

                    $.publish('plugin/swImageZoom/onZoomImageLoaded', [me, me.zoomImage]);
                };

                me.zoomImage.src = me.zoomImageUrl;
            }

            $.publish('plugin/swImageZoom/onActivateZoom', me);

            me.active = true;
        },

        /**
         * Stops the zoom view.
         */
        stopZoom: function() {
            var me = this;

            me.hideLens();
            me.hideZoom();
            me.zoomImage = false;
            me.active = false;

            $.publish('plugin/swImageZoom/onStopZoom', me);
        },

        /**
         * Handles click events on the lens.
         * Used for legacy browsers to handle
         * click events on the original image.
         *
         * @param event
         */
        onLensClick: function(event) {
            $.publish('plugin/swImageZoom/onLensClick', [this, event]);
        },

        /**
         * Clamps a number between
         * a max and a min value.
         *
         * @param number
         * @param min
         * @param max
         * @returns {number}
         */
        clamp: function(number, min, max) {
            return Math.max(min, Math.min(max, number));
        },

        /**
         * Destroys the plugin and removes
         * all created elements of the plugin.
         */
        destroy: function () {
            var me = this;

            me.$lens.remove();
            me.$flyout.remove();
            me.$container.removeClass(me.opts.containerCls);

            $('body').off('scroll.imageZoom');

            me._destroy();
        }
    });
})(jQuery);

;(function ($) {
    'use strict';

    /**
     * Shopware Collapse Panel Plugin.
     */
    $.plugin('swCollapsePanel', {

        alias: 'collapsePanel',

        /**
         * Default options for the collapse panel plugin.
         *
         * @public
         * @property defaults
         * @type {Object}
         */
        defaults: {

            /**
             * The selector of the target element which should be collapsed.
             *
             * @type {String|HTMLElement}
             */
            collapseTarget: false,

            /**
             * Selector for the content sibling when no collapseTargetCls was passed.
             *
             * @type {String}
             */
            contentSiblingSelector: '.collapse--content',

            /**
             * Additional class which will be added to the collapse target.
             *
             * @type {String}
             */
            collapseTargetCls: 'js--collapse-target',

            /**
             * The class which triggers the collapsed state.
             *
             * @type {String}
             */
            collapsedStateCls: 'is--collapsed',

            /**
             * The class for the active state of the trigger element.
             *
             * @type {String}
             */
            activeTriggerCls: 'is--active',

            /**
             * Decide if sibling collapse panels should be closed when the target is collapsed.
             *
             * @type {Boolean}
             */
            closeSiblings: false,

            /**
             * The speed of the collapse animation in ms.
             *
             * @type {Number}
             */
            animationSpeed: 400
        },

        /**
         * Default plugin initialisation function.
         * Sets all needed properties, adds classes
         * and registers all needed event listeners.
         *
         * @public
         * @method init
         */
        init: function () {
            var me = this,
                opts = me.opts;

            me.applyDataAttributes();

            if (opts.collapseTarget) {
                me.$targetEl = $(opts.collapseTarget);
            } else {
                me.$targetEl = me.$el.next(opts.contentSiblingSelector);
            }

            me.$targetEl.addClass(opts.collapseTargetCls);

            me.registerEvents();
        },

        /**
         * Registers all necessary event handlers.
         *
         * @public
         * @method registerEvents
         */
        registerEvents: function () {
            var me = this;

            me._on(me.$el, 'click', function (e) {
                e.preventDefault();
                me.toggleCollapse();
            });

            $.publish('plugin/swCollapsePanel/onRegisterEvents', [ me ]);
        },

        /**
         * Toggles the collapse state of the element.
         *
         * @public
         * @method toggleCollapse
         */
        toggleCollapse: function () {
            var me = this;

            if (me.$targetEl.hasClass(me.opts.collapsedStateCls)) {
                me.closePanel();
            } else {
                me.openPanel();
            }

            $.publish('plugin/swCollapsePanel/onToggleCollapse', [ me ]);
        },

        /**
         * Opens the panel by sliding it down.
         *
         * @public
         * @method openPanel
         */
        openPanel: function () {
            var me = this,
                opts = me.opts,
                $targetEl = me.$targetEl,
                siblings = $('.' + opts.collapseTargetCls).not($targetEl),
                tabId = $targetEl.parent().attr('data-tab-id');

            me.$el.addClass(opts.activeTriggerCls);

            $targetEl.slideDown(opts.animationSpeed, function () {

                $.publish('plugin/swCollapsePanel/onOpen', [ me ]);
            }).addClass(opts.collapsedStateCls);

            if (opts.closeSiblings) {
                siblings.slideUp(opts.animationSpeed, function () {
                    siblings.removeClass(opts.collapsedStateCls);
                    siblings.prev().removeClass(opts.activeTriggerCls);
                });
            }

            if(tabId !== undefined) {
                $.publish('onShowContent-' + tabId, [ me ]);
            }

            $.publish('plugin/swCollapsePanel/onOpenPanel', [ me ]);
        },

        /**
         * Closes the panel by sliding it up.
         *
         * @public
         * @method openPanel
         */
        closePanel: function () {
            var me = this,
                opts = me.opts;

            me.$el.removeClass(opts.activeTriggerCls);
            me.$targetEl.slideUp(opts.animationSpeed, function() {

                $.publish('plugin/swCollapsePanel/onClose', [ me ]);
            }).removeClass(opts.collapsedStateCls);

            $.publish('plugin/swCollapsePanel/onClosePanel', [ me ]);
        },

        /**
         * Destroys the initialized plugin completely, so all event listeners will
         * be removed and the plugin data, which is stored in-memory referenced to
         * the DOM node.
         *
         * @public
         * @method destroy
         */
        destroy: function () {
            var me = this,
                opts = me.opts;

            me.$el.removeClass(opts.activeTriggerCls);
            me.$targetEl.removeClass(opts.collapsedStateCls)
                .removeClass(opts.collapseTargetCls)
                .removeAttr('style');

            me._destroy();
        }
    });
})(jQuery);

;(function($) {
    'use strict';

    /**
     * Shopware Auto Submit Plugin
     *
     * @example
     *
     * HTML:
     *
     * <form method="GET" action="URL">
     *     <input type="checkbox" name="item1" value="1" data-auto-submit="true" />
     *     <input type="radio" name="item2" value="2" data-auto-submit="true" />
     *     <select name="item3" data-auto-submit="true">
     *         <option value="opt1" selected="selected">My option 1</option>
     *         <option value="opt2">My option 2</option>
     *         <option value="opt3">My option 3</option>
     *     </select>
     * </form>
     *
     * JS:
     *
     * $('form *[data-auto-submit="true"]').autoSubmit();
     *
     * If you now change either an input or an option in the select, the form will be submitted.
     *
     */
    $.plugin('swAutoSubmit', {

        defaults: {

            /**
             * Decide if loading indicator is shown until the form is submitted.
             *
             * @property loadingindicator
             * @type {Boolean}
             */
            'loadingindicator': true
        },

        /**
         * Default plugin initialisation function.
         * Registers an event listener on the change event.
         * When it's triggered, the parent form will be submitted.
         *
         * @public
         * @method init
         */
        init: function () {
            var me = this;

            me.applyDataAttributes();

            me.$form = $(me.$el.parents('form')[0]);

            // Will be automatically removed when destroy() is called.
            me._on(me.$el, 'change', $.proxy(me.onChangeSelection, me));

            $.publish('plugin/swAutoSubmit/onRegisterEvents', [ me ]);
        },

        onChangeSelection: function () {
            var me = this;

            if(me.opts.loadingindicator) {
                $.loadingIndicator.open({
                    closeOnClick: false
                });
            }

            me.$form.submit();
        }
    });
})(jQuery);

;(function ($) {
    'use strict';

    /**
     * Shopware Scroll Plugin.
     *
     * This plugin scrolls the page or given element to a certain point when the
     * plugin element was clicked.
     */
    $.plugin('swScrollAnimate', {

        defaults: {

            /**
             * The selector of the container which should be scrolled.
             *
             * @property scrollContainerSelector
             * @type {String}
             */
            scrollContainerSelector: 'body, html',

            /**
             * The selector of the target element or the position in px where the container should be scrolled to.
             *
             * @property scrollTarget
             * @type {Number|String}
             */
            scrollTarget: 0,

            /**
             * The speed of the scroll animation in ms.
             *
             * @property animationSpeed
             * @type {Number}
             */
            animationSpeed: 500
        },

        /**
         * Initializes the plugin and register its events
         *
         * @public
         * @method init
         */
        init: function () {
            var me = this,
                opts = me.opts;

            me.applyDataAttributes();

            me.$container = $(opts.scrollContainerSelector);

            if (typeof opts.scrollTarget === 'string') {
                me.$targetEl = $(opts.scrollTarget);
            }

            me.registerEvents();
        },

        /**
         * This method registers the event listeners when when clicking
         * or tapping the plugin element.
         *
         * @public
         * @method registerEvents
         */
        registerEvents: function () {
            var me = this;

            me._on(me.$el, 'touchstart click', $.proxy(me.onClickElement, me));

            $.publish('plugin/swScrollAnimate/onRegisterEvents', [ me ]);
        },

        /**
         * This method will be called when the plugin element was either clicked or tapped.
         * It scrolls the target element to the given destination.
         *
         * @public
         * @method onClickElement
         */
        onClickElement: function (event) {
            event.preventDefault();

            var me = this,
                opts = me.opts;

            $.publish('plugin/swScrollAnimate/onClickElement', [ me, event ]);

            if (me.$targetEl) {
                me.scrollToElement(me.$targetEl);
                return;
            }

            me.scrollToPosition(opts.scrollTarget);
        },

        /**
         * Scrolls the target element to the vertical position of another element.
         *
         * @public
         * @method scrollToElement
         * @param {jQuery} $targetEl
         * @param {Number} offset
         */
        scrollToElement: function ($targetEl, offset) {
            var me = this;

            if (!$targetEl.length) {
                return;
            }

            $.publish('plugin/swScrollAnimate/onScrollToElement', [ me, $targetEl, offset ]);

            me.scrollToPosition($targetEl.offset().top + ~~(offset));
        },

        /**
         * Scrolls the target element to the given vertical position in pixel.
         *
         * @public
         * @method scrollToPosition
         * @param {Number} position
         */
        scrollToPosition: function (position) {
            var me = this;

            me.$container.animate({
                scrollTop: position
            }, me.opts.animationSpeed);

            $.publish('plugin/swScrollAnimate/onScrollToPosition', [ me, position ]);
        },

        /**
         * This method destroys the plugin and its registered events
         *
         * @public
         * @method destroy
         */
        destroy: function () {
            this._destroy();
        }
    });
})(jQuery);

/**
 * Product Slider
 *
 * A jQuery Plugin for dynamic sliders.
 * It has functionality for slide and scroll animations.
 * Supports momentum scrolling via touch gestures on mobile devices.
 * Can load items via ajax or use an existing dom structure.
 * Use the different config options to adjust the slider to your needs.
 *
 * @Example DOM structure:
 *
 * <div class="product-slider">
 *     <div class="product-slider--container">
 *         <div class="product-slider--item"></div>
 *         <div class="product-slider--item"></div>
 *         <div class="product-slider--item"></div>
 *     </div>
 * </div>
 */
;(function ($, window) {
    'use strict';

    /**
     * Private window object
     */
    var $window = $(window);

    /**
     * Additional jQuery easing methods.
     */
    jQuery.extend(jQuery.easing, {
        easeOutExpo: function (x, t, b, c, d) {
            return (t == d) ? b + c : c * (-Math.pow(2, -10 * t / d) + 1) + b;
        }
    });

    /**
     * Product Slider Plugin
     */
    $.plugin('swProductSlider', {

        defaults: {

            /**
             * The mode for getting the items.
             *
             * @property mode ( local | ajax )
             * @type {String}
             */
            mode: 'local',

            /**
             * The orientation of the slider.
             *
             * @property orientation ( horizontal | vertical )
             * @type {String}
             */
            orientation: 'horizontal',

            /**
             * The minimal width a slider item should have.
             * Used for horizontal sliders.
             *
             * @property itemMinWidth
             * @type {Number}
             */
            itemMinWidth: 220,

            /**
             * The minimal height a slider item should have.
             * Used for vertical sliders.
             *
             * @property itemMinHeight
             * @type {Number}
             */
            itemMinHeight: 240,

            /**
             * Number of items moved on each slide.
             *
             * @property itemsPerSlide
             * @type {Number}
             */
            itemsPerSlide: 1,

            /**
             * Turn automatic sliding on and off.
             *
             * @property autoSlide
             * @type {Boolean}
             */
            autoSlide: false,

            /**
             * Direction of the auto sliding.
             *
             * @property autoSlideDirection ( next | prev )
             * @type {String}
             */
            autoSlideDirection: 'next',

            /**
             * Time in seconds between each auto slide.
             *
             * @property autoSlideSpeed
             * @type {Number}
             */
            autoSlideSpeed: 4,

            /**
             * Turn automatic scrolling on and off.
             *
             * @property autoScroll
             * @type {Boolean}
             */
            autoScroll: false,

            /**
             * Direction if the auto scrolling.
             *
             * @property autoScrollDirection ( next | prev )
             * @type {String}
             */
            autoScrollDirection: 'next',

            /**
             * Distance in px for every auto scroll step.
             *
             * @property autoScrollSpeed
             * @type {Number}
             */
            autoScrollSpeed: 1,

            /**
             * Distance in px for scroll actions triggered by arrow controls.
             *
             * @property scrollDistance
             * @type {Number}
             */
            scrollDistance: 350,

            /**
             * Speed in ms for slide animations.
             *
             * @property animationSpeed
             * @type {Number}
             */
            animationSpeed: 800,

            /**
             * Turn arrow controls on and off.
             *
             * @property arrowControls
             * @type {Boolean}
             */
            arrowControls: true,

            /**
             * The type of action the arrows should trigger.
             *
             * @property arrowAction ( slide | scroll )
             * @type {String}
             */
            arrowAction: 'slide',

            /**
             * The css class for the slider wrapper.
             *
             * @property wrapperCls
             * @type {String}
             */
            wrapperCls: 'product-slider',

            /**
             * The css class for the horizontal state.
             *
             * @property horizontalCls
             * @type {String}
             */
            horizontalCls: 'is--horizontal',

            /**
             * The css class for the vertical state.
             *
             * @property verticalCls
             * @type {String}
             */
            verticalCls: 'is--vertical',

            /**
             * The css class for the arrow controls.
             *
             * @property arrowCls
             * @type {String}
             */
            arrowCls: 'product-slider--arrow',

            /**
             * The css class for the left arrow.
             *
             * @property prevArrowCls
             * @type {String}
             */
            prevArrowCls: 'arrow--prev',

            /**
             * The css class for the right arrow.
             *
             * @property nextArrowCls
             * @type {String}
             */
            nextArrowCls: 'arrow--next',

            /**
             * The selector for the item container.
             *
             * @property containerSelector
             * @type {String}
             */
            containerSelector: '.product-slider--container',

            /**
             * The selector for the single items.
             *
             * @property itemSelector
             * @type {String}
             */
            itemSelector: '.product-slider--item',

            /**** Ajax Config ****/

            /**
             * The controller url for ajax loading.
             *
             * @property ajaxCtrlUrl
             * @type {String}
             */
            ajaxCtrlUrl: null,

            /**
             * The category id for ajax loading.
             *
             * @property ajaxCategoryID
             * @type {Number}
             */
            ajaxCategoryID: null,

            /**
             * The maximum number of items to load via ajax.
             *
             * @property ajaxMaxShow
             * @type {Number}
             */
            ajaxMaxShow: 30,

            /**
             * Option to toggle the ajax loading indicator
             *
             * @property ajaxShowLoadingIndicator
             * @type {Boolean}
             */
            ajaxShowLoadingIndicator: true,

            /**
             * The css class for the ajax loading indicator container
             *
             * @property ajaxLoadingIndicatorCls
             * @type {String}
             */
            ajaxLoadingIndicatorCls: 'js--loading-indicator indicator--absolute',

            /**
             * The css class for the ajax loading indicator icon
             *
             * @property ajaxLoadingIndicatorIconCls
             * @type {String}
             */
            ajaxLoadingIndicatorIconCls: 'icon--default',

            /**
             * Optional event to initialize the product slider
             *
             * @property initOnEvent
             * @type {String}
             */
            initOnEvent: null
        },

        /**
         * Initializes the plugin
         *
         * @public
         * @method init
         */
        init: function () {
            var me = this;

            me.applyDataAttributes();

            me.autoScrollAnimation = false;
            me.autoSlideAnimation = false;
            me.bufferedCall = false;
            me.initialized = false;

            me.isLoading = false;
            me.isAnimating = false;

            if (me.opts.mode === 'ajax' && me.opts.ajaxCtrlUrl === null) {
                console.error('The controller url for the ajax slider is not defined!');
                return;
            }

            if (me.opts.mode === 'ajax' && me.opts.ajaxShowLoadingIndicator) {
                me.showLoadingIndicator();
            }

            if (me.opts.initOnEvent !== null) {
                $.subscribe(me.opts.initOnEvent, function() {
                    if (!me.initialized) {
                        me.initSlider();
                        me.registerEvents();
                    }
                });
            } else {
                me.initSlider();
                me.registerEvents();
            }
        },

        /**
         * Updates the plugin.
         *
         * @public
         * @method update
         */
        update: function () {
            var me = this;

            if (!me.initialized) {
                return false;
            }

            me.trackItems();
            me.setSizes();

            /**
             * Always set back to the first item on update
             */
            me.setPosition(0);
            me.trackArrows();

            $.publish('plugin/swProductSlider/onUpdate', [ me ]);
        },

        /**
         * Initializes all necessary slider configs.
         *
         * @public
         * @method initSlider
         */
        initSlider: function () {
            var me = this,
                opts = me.opts;

            me.$el.addClass(opts.wrapperCls);

            me.createContainer();
            me.trackItems();
            me.setSizes();

            /**
             * Used for smooth animations.
             */
            me.currentPosition = me.getScrollPosition();

            if (me.itemsCount <= 0 && opts.mode === 'ajax') {
                me.loadItems(0, Math.min(me.itemsPerPage * 2, opts.ajaxMaxShow), $.proxy(me.initSlider, me));
                return;
            }

            if (me.opts.arrowControls && me.isActive()) me.createArrows();
            if (me.opts.autoScroll && me.isActive()) me.autoScroll();
            if (me.opts.autoSlide && me.isActive()) me.autoSlide();

            me.initialized = true;

            $.publish('plugin/swProductSlider/onInitSlider', [ me ]);
        },

        /**
         * Registers all necessary event listeners.
         *
         * @public
         * @method registerEvents
         */
        registerEvents: function () {
            var me = this;

            me._on(me.$el, 'touchstart mouseenter', $.proxy(me.onMouseEnter, me));
            me._on(me.$el, 'mouseleave', $.proxy(me.onMouseLeave, me));

            me._on(me.$container, 'scroll', $.proxy(me.onScroll, me));

            me._on($window, 'resize', $.proxy(me.buffer, me, me.update, 600));

            $.subscribe('plugin/swTabMenu/onChangeTab', $.proxy(me.update, me));
            $.subscribe('plugin/swCollapsePanel/onOpenPanel', $.proxy(me.update, me));

            $.publish('plugin/swProductSlider/onRegisterEvents', [ me ]);
        },

        /**
         * Returns the active state of the slider.
         *
         * @public
         * @method isActive
         * @returns {Boolean}
         */
        isActive: function () {
            var me = this;

            return me.$items.length > me.itemsPerPage;
        },

        /**
         * Returns the current position of the slider.
         *
         * @public
         * @method getScrollPosition
         * @param {String} orientation
         * @returns {jQuery}
         */
        getScrollPosition: function (orientation) {
            var me = this,
                o = orientation || me.opts.orientation;

            return (o === 'vertical') ? me.$container.scrollTop() : me.$container.scrollLeft();
        },

        /**
         * Sets the position of the slider.
         *
         * @public
         * @method setPosition
         * @param {Number} position
         */
        setPosition: function (position) {
            var me = this,
                pos = position || 0,
                method = (me.opts.orientation === 'vertical') ? 'scrollTop' : 'scrollLeft';

            me.$container[method](pos);
            me.currentPosition = pos;

            $.publish('plugin/swProductSlider/onSetPosition', [ me, pos ]);
        },

        /**
         * Sets all necessary size values of the slider.
         *
         * @public
         * @method setSizes
         * @param {String} orientation
         */
        setSizes: function (orientation) {
            var me = this,
                o = orientation || me.opts.orientation,
                containerSize = (o === 'vertical') ? me.$el.innerHeight() : me.$el.innerWidth(),
                itemSize = (o === 'vertical') ? me.opts.itemMinHeight : me.opts.itemMinWidth;

            me.itemsPerPage = Math.floor(containerSize / itemSize);

            if (me.itemsPerPage < 1) me.itemsPerPage = 1;

            me.itemSizePercent = 100 / me.itemsPerPage;

            if (o === 'vertical') {
                me.$items.css({ 'height': me.itemSizePercent + '%' });
                me.itemSize = me.$items.outerHeight();
            } else {
                me.$items.css({ 'width': me.itemSizePercent + '%' });
                me.itemSize = me.$items.outerWidth();
            }

            /**
             * Triggered for sizing lazy loaded images.
             */
            window.picturefill();

            $.publish('plugin/swProductSlider/onSetSizes', [ me, orientation ]);
        },

        /**
         * Tracks the number of items the slider contains.
         *
         * @public
         * @method trackItems
         * @returns {Number}
         */
        trackItems: function () {
            var me = this;

            me.$items = me.$container.find(me.opts.itemSelector);

            me.itemsCount = me.$items.length;

            $.publish('plugin/swProductSlider/onTrackItems', [ me, me.items, me.itemsCount ]);

            return me.itemsCount;
        },

        /**
         * Tracks the arrows and shows/hides them
         *
         * @public
         * @method trackArrows
         */
        trackArrows: function() {
            var me = this;

            if(!me.$arrowPrev || !me.$arrowNext) {
                if (me.isActive() && me.opts.arrowControls) me.createArrows();
                return;
            }

            if (!me.isActive()) {
                me.$arrowPrev.hide();
                me.$arrowNext.hide();
                return;
            }

            /**
             * Five pixel tolerance for momentum scrolling.
             */
            var slideEnd = me.currentPosition + me.$container[(me.opts.orientation === 'vertical') ? 'outerHeight': 'outerWidth']();
            me.$arrowPrev[(me.currentPosition > 5) ? 'show' : 'hide']();
            me.$arrowNext[(slideEnd >= parseInt(me.itemSize * me.itemsCount, 10) - 5) ? 'hide' : 'show']();

            $.publish('plugin/swProductSlider/onTrackArrows', [ me, me.$arrowPrev, me.$arrowNext ]);
        },

        /**
         * Helper function to show a loading indicator.
         * Gets called when ajax products are being loaded.
         *
         * @public
         * @method showLoadingIndicator
         */
        showLoadingIndicator: function() {
            var me = this;

            me.$ajaxLoadingIndicator = $('<div>', {
                'class': me.opts.ajaxLoadingIndicatorCls,
                'html': $('<i>', {
                    'class': me.opts.ajaxLoadingIndicatorIconCls
                })
            }).appendTo(me.$el);
        },

        /**
         * Helper function to remove the loading indicator.
         * Gets called when ajax products have been successfully loaded.
         *
         * @public
         * @method removeLoadingIndicator
         */
        removeLoadingIndicator: function() {
            var me = this;

            if (me.$ajaxLoadingIndicator) {
                me.$ajaxLoadingIndicator.remove();
            }
        },

        /**
         * Loads new items via ajax.
         *
         * @public
         * @method loadItems
         * @param {Number} start
         * @param {Number} limit
         * @param {Function} callback
         */
        loadItems: function (start, limit, callback) {
            var me = this,
                data = {
                    'start': start,
                    'limit': limit
                };

            if (me.opts.ajaxCategoryID !== null) {
                data['category'] = me.opts.ajaxCategoryID
            }

            me.isLoading = true;

            $.publish('plugin/swProductSlider/onLoadItemsBefore', [ me, data ]);

            $.ajax({
                url: me.opts.ajaxCtrlUrl,
                method: 'GET',
                data: data,
                success: function (response) {
                    me.removeLoadingIndicator();

                    if (!response) {
                        // Prevent infinite loop
                        return;
                    }

                    me.isLoading = false;
                    me.$container.append(response);
                    me.trackItems();
                    me.setSizes();
                    me.trackArrows();

                    $.publish('plugin/swProductSlider/onLoadItemsSuccess', [ me, response ]);

                    if (typeof callback === 'function') {
                        callback.call(me, response);
                    }
                }
            });

            $.publish('plugin/swProductSlider/onLoadItems', [ me ]);
        },

        /**
         * Creates and returns the container for the items.
         *
         * @public
         * @method createContainer
         * @param {String} orientation
         * @returns {jQuery}
         */
        createContainer: function (orientation) {
            var me = this,
                o = orientation || me.opts.orientation,
                orientationCls = (o === 'vertical') ? me.opts.verticalCls : me.opts.horizontalCls,
                $container = me.$el.find(me.opts.containerSelector);

            if (!$container.length) {
                $container = $('<div>', {
                    'class': me.opts.containerSelector.substr(1)
                }).appendTo(me.$el);
            }

            $container.addClass(orientationCls);

            me.$container = $container;

            $.publish('plugin/swProductSlider/onCreateContainer', [ me, $container, orientation ]);

            return $container;
        },

        /**
         * Creates the arrow controls.
         *
         * @private
         * @method createArrows
         */
        createArrows: function () {
            var me = this,
                orientationCls = (me.opts.orientation === 'vertical') ? me.opts.verticalCls : me.opts.horizontalCls;

            if (!me.opts.arrowControls || !me.isActive()) {
                return;
            }

            if (!me.$arrowPrev) {
                me.$arrowPrev = $('<a>', {
                    'class': me.opts.arrowCls + ' ' +
                        me.opts.prevArrowCls + ' ' +
                        orientationCls
                }).prependTo(me.$el);

                me._on(me.$arrowPrev, 'click', $.proxy(me.onArrowClick, me, 'prev'));
            }

            if (!me.$arrowNext) {
                me.$arrowNext = $('<a>', {
                    'class': me.opts.arrowCls + ' ' +
                        me.opts.nextArrowCls + ' ' +
                        orientationCls
                }).prependTo(me.$el);

                me._on(me.$arrowNext, 'click', $.proxy(me.onArrowClick, me, 'next'));
            }

            me.trackArrows();

            $.publish('plugin/swProductSlider/onCreateArrows', [ me, me.$arrowPrev, me.$arrowNext ]);
        },

        /**
         * Event listener for click events on the arrows controls.
         *
         * @public
         * @method onArrowClick
         * @param {String} type
         * @param {jQuery.Event} event
         */
        onArrowClick: function (type, event) {
            var me = this,
                next = (me.opts.arrowAction === 'scroll') ? 'scrollNext' : 'slideNext',
                prev = (me.opts.arrowAction === 'scroll') ? 'scrollPrev' : 'slidePrev';

            event.preventDefault();

            me[(type === 'prev') ? prev : next]();

            $.publish('plugin/swProductSlider/onArrowClick', [ me, event, type ]);
        },

        /**
         * Event listener for mouseenter event.
         *
         * @public
         * @method onMouseEnter
         */
        onMouseEnter: function (event) {
            var me = this;

            me.stopAutoScroll();
            me.stopAutoSlide();

            $.publish('plugin/swProductSlider/onMouseEnter', [ me, event ]);
        },

        /**
         * Event listener for mouseleave event.
         *
         * @public
         * @method onMouseLeave
         */
        onMouseLeave: function (event) {
            var me = this;

            if (me.isActive() && me.opts.autoScroll) me.autoScroll();
            if (me.isActive() && me.opts.autoSlide) me.autoSlide();

            $.publish('plugin/swProductSlider/onMouseLeave', [ me, event ]);
        },

        /**
         * Event listener for scroll event.
         *
         * @public
         * @method onScroll
         */
        onScroll: function (event) {
            var me = this;

            if (!me.isAnimating) {
                me.currentPosition = me.getScrollPosition();
            }

            me.trackArrows();

            if (me.opts.mode !== 'ajax' || me.isLoading) {
                return;
            }

            var position = me.getScrollPosition(),
                scrolledItems = Math.floor(position / me.itemSize),
                itemsLeftToLoad = me.opts.ajaxMaxShow - me.itemsCount,
                loadMoreCount = me.itemsCount - me.itemsPerPage * 2;

            if (scrolledItems >= loadMoreCount && itemsLeftToLoad > 0) {
                me.loadItems(me.itemsCount, Math.min(me.itemsPerPage, itemsLeftToLoad));
            }

            $.publish('plugin/swProductSlider/onScroll', [ me, event ]);
        },

        /**
         * Moves the slider exactly to the next item(s).
         * Based on the "itemsPerSlide" option.
         *
         * @public
         * @method slideNext
         */
        slideNext: function () {
            var me = this;

            me.currentPosition = Math.floor((me.currentPosition + me.itemSize * me.opts.itemsPerSlide) / me.itemSize) * me.itemSize;
            me.slide(me.currentPosition);

            $.publish('plugin/swProductSlider/onSlideNext', [ me, me.currentPosition ]);
        },

        /**
         * Moves the slider exactly to the previous item(s).
         * Based on the "itemsPerSlide" option.
         *
         * @public
         * @method slidePrev
         */
        slidePrev: function () {
            var me = this;

            me.currentPosition = Math.ceil((me.currentPosition - me.itemSize * me.opts.itemsPerSlide) / me.itemSize) * me.itemSize;
            me.slide(me.currentPosition);

            $.publish('plugin/swProductSlider/onSlidePrev', [ me, me.currentPosition ]);
        },

        /**
         * Moves the slider to the position of an item.
         *
         * @public
         * @method slideToElement
         * @param {jQuery} $el
         * @param {String} orientation
         */
        slideToElement: function ($el, orientation) {
            var me = this,
                o = orientation || me.opts.orientation,
                position = $el.position(),
                slide = (o === 'vertical') ? position.top : position.left;

            me.slide(slide);

            $.publish('plugin/swProductSlider/onSlideToElement', [ me, $el, orientation ]);
        },

        /**
         * Does the slide animation to the given position.
         *
         * @public
         * @method slide
         * @param {Number} position
         */
        slide: function (position) {
            var me = this,
                animation = {};

            me.isAnimating = true;

            animation[(me.opts.orientation === 'vertical') ? 'scrollTop' : 'scrollLeft'] = position;

            me.$container.stop().animate(animation, me.opts.animationSpeed, 'easeOutExpo', function () {
                me.currentPosition = me.getScrollPosition();
                me.isAnimating = false;

                $.publish('plugin/swProductSlider/onSlideFinished', [me, me.currentPosition]);
            });

            $.publish('plugin/swProductSlider/onSlide', [ me, position ]);
        },

        /**
         * Handles the automatic sliding.
         *
         * @public
         * @method autoSlide
         * @param {String} slideDirection
         * @param {Number} slideSpeed
         */
        autoSlide: function (slideDirection, slideSpeed) {
            var me = this,
                direction = slideDirection || me.opts.autoSlideDirection,
                speed = slideSpeed || me.opts.autoSlideSpeed,
                method = (direction === 'prev') ? me.slidePrev : me.slideNext;

            me.autoSlideAnimation = window.setInterval($.proxy(method, me), speed * 1000);

            $.publish('plugin/swProductSlider/onAutoSlide', [ me, me.autoSlideAnimation, slideDirection, slideSpeed ]);
        },

        /**
         * Stops the automatic sliding.
         *
         * @public
         * @method stopAutoSlide
         */
        stopAutoSlide: function () {
            var me = this;

            window.clearInterval(me.autoSlideAnimation);
            me.autoSlideAnimation = false;

            $.publish('plugin/swProductSlider/onStopAutoSlide', [ me ]);
        },

        /**
         * Scrolls the slider forward by the given distance.
         *
         * @public
         * @method scrollNext
         * @param {Number} scrollDistance
         */
        scrollNext: function (scrollDistance) {
            var me = this;

            me.currentPosition += scrollDistance || me.opts.scrollDistance;

            me.slide(me.currentPosition);

            $.publish('plugin/swProductSlider/onScrollNext', [ me, me.currentPosition, scrollDistance ]);
        },

        /**
         * Scrolls the slider backwards by the given distance.
         *
         * @public
         * @method scrollPrev
         * @param {Number} scrollDistance
         */
        scrollPrev: function (scrollDistance) {
            var me = this;

            me.currentPosition -= scrollDistance || me.opts.scrollDistance;

            me.slide(me.currentPosition);

            $.publish('plugin/swProductSlider/onScrollPrev', [ me, me.currentPosition, scrollDistance ]);
        },

        /**
         * Handles the automatic scrolling of the slider.
         *
         * @public
         * @method autoScroll
         * @param {String} scrollDirection
         * @param {Number} scrollSpeed
         */
        autoScroll: function (scrollDirection, scrollSpeed) {
            var me = this,
                direction = scrollDirection || me.opts.autoScrollDirection,
                speed = scrollSpeed || me.opts.autoScrollSpeed,
                position = me.getScrollPosition();

            me.autoScrollAnimation = StateManager.requestAnimationFrame($.proxy(me.autoScroll, me, direction, speed));

            me.setPosition((direction === 'prev') ? position - speed : position + speed);

            $.publish('plugin/swProductSlider/onAutoScroll', [ me, me.autoScrollAnimation, scrollDirection, scrollSpeed ]);
        },

        /**
         * Stops the automatic scrolling.
         *
         * @public
         * @method stopAutoScroll
         */
        stopAutoScroll: function () {
            var me = this;

            StateManager.cancelAnimationFrame(me.autoScrollAnimation);
            me.autoScrollAnimation = false;

            $.publish('plugin/swProductSlider/onStopAutoScroll', [ me ]);
        },

        /**
         * Buffers the calling of a function.
         *
         * @param func
         * @param bufferTime
         */
        buffer: function(func, bufferTime) {
            var me = this;

            window.clearTimeout(me.bufferedCall);

            me.bufferedCall = window.setTimeout($.proxy(func, me), bufferTime);

            $.publish('plugin/swProductSlider/onBuffer', [ me, me.bufferedCall, func, bufferTime ]);
        },

        /**
         * Destroys the plugin and all necessary settings.
         *
         * @public
         * @method destroy
         */
        destroy: function () {
            var me = this;

            if (me.$arrowPrev) me.$arrowPrev.remove();
            if (me.$arrowNext) me.$arrowNext.remove();

            me.stopAutoSlide();
            me.stopAutoScroll();

            me._destroy();
        }
    });
})(jQuery, window);

;(function ($) {
    'use strict';

    /**
     * Register plugin
     *
     * This plugin handles validation and addition logic for the registration form and its fields.
     */
    $.plugin('swRegister', {

        /**
         * Plugin default options.
         * Get merged automatically with the user configuration.
         */
        defaults: {

            /**
             * Class to indicate an element to be hidden.
             *
             * @property hiddenClass
             * @type {String}
             */
            hiddenClass: 'is--hidden',

            /**
             * Class to indicate that an element has an error.
             *
             * @property errorClass
             * @type {String}
             */
            errorClass: 'has--error',

            /**
             * Selector for the form.
             *
             * @property formSelector
             * @type {String}
             */
            formSelector: '.register--form',

            /**
             * Selector for the forms submit button.
             *
             * @property submitBtnSelector
             * @type {String}
             */
            submitBtnSelector: '.register--submit,.address--form-submit',

            /**
             * Selector for the type selection field.
             *
             * @property typeFieldSelector
             * @type {String}
             */
            typeFieldSelector: '.register--customertype select,.address--customertype select,.address--customertype input',

            /**
             * Type name for a company selection.
             * Used for comparison on the type selection field.
             *
             * @property companyType
             * @type {String}
             */
            companyType: 'business',

            /**
             * Selector for the skip account creation checkbox.
             * Toggles specific field sets when checked.
             *
             * @property skipAccountSelector
             * @type {String}
             */
            skipAccountSelector: '.register--check input',

            /**
             * Selector for the alternative shipping checkbox.
             * Toggles specific field sets when checked.
             *
             * @property altShippingSelector
             * @type {String}
             */
            altShippingSelector: '.register--alt-shipping input',

            /**
             * Selector for the company field set.
             *
             * @property companyFieldSelector
             * @type {String}
             */
            companyFieldSelector: '.register--company,.address--company',

            /**
             * Selector for the account field set.
             *
             * @property accountFieldSelector
             * @type {String}
             */
            accountFieldSelector: '.register--account-information',

            /**
             * Selector for the shipping field set.
             *
             * @property shippingFieldSelector
             * @type {String}
             */
            shippingFieldSelector: '.register--shipping',

            /**
             * Selector for the payment field set.
             *
             * @property paymentFieldSelector
             * @type {String}
             */
            paymentFieldSelector: '.payment--content',

            /**
             * Selector for the payment selection radio button.
             *
             * @property paymentInputSelector
             * @type {String}
             */
            paymentInputSelector: '.payment--selection-input input',

            /**
             * Selector for the country select field.
             *
             * @property countryFieldSelector
             * @type {String}
             */
            countryFieldSelector: '.select--country',

            /**
             * Selector for the state field set.
             * This corresponding field set will be toggled
             * when a country was selected.
             *
             * @property stateContainerSelector
             * @type {String}
             */
            stateContainerSelector: '.register--state-selection, .address--state-selection',

            /**
             * Selector for the payment method select fields.
             *
             * @property paymentMethodSelector
             * @type {String}
             */
            paymentMethodSelector: '.payment--method',

            /**
             * Selector for a input field.
             *
             * @property inputSelector
             * @type {String}
             */
            inputSelector: '.is--required',

            /**
             * Class that will be added to a error message.
             *
             * @property errorMessageClass
             * @type {String}
             */
            errorMessageClass: 'register--error-msg',

            /**
             * Selector for the email field.
             *
             * @property personalEmailSelector
             * @type {String}
             */
            personalEmailSelector: '#register_personal_email',

            /**
             * Selector for the password field.
             *
             * @property personalPasswordSelector
             * @type {String}
             */
            personalPasswordSelector: '#register_personal_password',

            /**
             * Selector for the email confirmation field.
             *
             * @property personalEmailConfirmationSelector
             * @type {String}
             */
            personalEmailConfirmationSelector: '#register_personal_emailConfirmation',

            /**
             * Selector for the password confirmation field.
             *
             * @property personalPasswordConfirmationSelector
             * @type {String}
             */
            personalPasswordConfirmationSelector: '#register_personal_passwordConfirmation',

            /**
             * Selector for the guest checkbox.
             *
             * @property personalPasswordConfirmationSelector
             * @type {String}
             */
            personalGuestSelector: '#register_personal_skipLogin',
        },

        /**
         * Initializes the plugin, sets up event listeners and adds the necessary
         * classes to get the plugin up and running.
         *
         * @public
         * @method init
         */
        init: function () {
            var me = this,
                opts = me.opts,
                $el = me.$el;

            me.$personalEmail = $el.find(opts.personalEmailSelector);
            me.$personalPassword = $el.find(opts.personalPasswordSelector);
            me.$personalEmailConfirmation = $el.find(opts.personalEmailConfirmationSelector);
            me.$personalPasswordConfirmation = $el.find(opts.personalPasswordConfirmationSelector);
            me.$personalGuest = $el.find(opts.personalGuestSelector);

            me.$form = $el.find(opts.formSelector);

            me.$submitBtn = $el.find(opts.submitBtnSelector);

            me.$typeSelection = $el.find(opts.typeFieldSelector);
            me.$skipAccount = $el.find(opts.skipAccountSelector);
            me.$alternativeShipping = $el.find(opts.altShippingSelector);

            me.$companyFieldset = $el.find(opts.companyFieldSelector);
            me.$accountFieldset = $el.find(opts.accountFieldSelector);
            me.$shippingFieldset = $el.find(opts.shippingFieldSelector);

            me.$countySelectFields = $el.find(opts.countryFieldSelector);

            me.$paymentMethods = $el.find(opts.paymentMethodSelector);

            me.$inputs = $el.find(opts.inputSelector);
            me.$stateContainers = $el.find(opts.stateContainerSelector);

            me.checkType();
            me.checkSkipAccount();
            me.checkChangeShipping();

            me.registerEvents();
        },

        /**
         * Registers all necessary event listeners for the plugin to proper operate.
         *
         * @public
         * @method registerEvents
         */
        registerEvents: function () {
            var me = this;

            me._on(me.$typeSelection, 'change', $.proxy(me.checkType, me));
            me._on(me.$skipAccount, 'change', $.proxy(me.checkSkipAccount, me));
            me._on(me.$alternativeShipping, 'change', $.proxy(me.checkChangeShipping, me));
            me._on(me.$countySelectFields, 'change', $.proxy(me.onCountryChanged, me));
            me._on(me.$paymentMethods, 'change', $.proxy(me.onPaymentChanged, me));
            me._on(me.$form, 'focusout', $.proxy(me.onValidateInput, me));
            me._on(me.$submitBtn, 'click', $.proxy(me.onSubmitBtn, me));

            $.publish('plugin/swRegister/onRegisterEvents', [ me ]);
        },

        /**
         * Checks the type selection field.
         * If the value is equal to the configured companyType,
         * the company field set will be shown.
         *
         * @public
         * @method checkType
         */
        checkType: function () {
            var me = this,
                opts = me.opts,
                $fieldSet = me.$companyFieldset,
                hideCompanyFields = (me.$typeSelection.length && me.$typeSelection.val() !== opts.companyType),
                requiredFields = $fieldSet.find(opts.inputSelector),
                requiredMethod = (!hideCompanyFields) ? me.setHtmlRequired : me.removeHtmlRequired,
                classMethod = (!hideCompanyFields) ? 'removeClass' : 'addClass',
                disabledMethod = (!hideCompanyFields) ? 'removeAttr' : 'attr';

            requiredMethod(requiredFields);

            $fieldSet[classMethod](opts.hiddenClass);
            $fieldSet.find('input, select, textarea')[disabledMethod]('disabled', 'disabled');

            $.publish('plugin/swRegister/onCheckType', [ me, hideCompanyFields ]);
        },

        /**
         * Checks the skip account checkbox.
         * The account field set will be shown/hidden depending
         * on the check state of the checkbox.
         *
         * @public
         * @method checkSkipAccount
         */
        checkSkipAccount: function () {
            var me = this,
                opts = me.opts,
                $fieldSet = me.$accountFieldset,
                isChecked = me.$skipAccount.is(':checked'),
                requiredFields = $fieldSet.find(opts.inputSelector),
                requiredMethod = (!isChecked) ? me.setHtmlRequired : me.removeHtmlRequired,
                classMethod = (isChecked) ? 'addClass' : 'removeClass';

            requiredMethod(requiredFields);

            $fieldSet[classMethod](opts.hiddenClass);

            $.publish('plugin/swRegister/onCheckSkipAccount', [ me, isChecked ]);
        },

        /**
         * Checks the alternative shipping checkbox.
         * The shipping field set will be shown/hidden depending
         * on the check state of the checkbox.
         *
         * @public
         * @method checkChangeShipping
         */
        checkChangeShipping: function () {
            var me = this,
                opts = me.opts,
                $fieldSet = me.$shippingFieldset,
                isChecked = me.$alternativeShipping.is(':checked'),
                requiredFields = $fieldSet.find(opts.inputSelector),
                requiredMethod = (isChecked) ? me.setHtmlRequired : me.removeHtmlRequired,
                classMethod = (isChecked) ? 'removeClass' : 'addClass';

            requiredMethod(requiredFields);

            $fieldSet[classMethod](opts.hiddenClass);

            $.publish('plugin/swRegister/onCheckChangeShipping', [ me, isChecked ]);
        },

        /**
         * Called when another country was selected in the country selection.
         * Triggers additional classes depending on the selection.
         *
         * @public
         * @method onCountryChanged
         * @param {jQuery.Event} event
         */
        onCountryChanged: function (event) {
            var me = this,
                $select = $(event.currentTarget),
                countryId = $select.val(),
                addressType = $select.attr('data-address-type'),
                $stateContainers,
                plugin;

            $.publish('plugin/swRegister/onCountryChangedBefore', [ me, event, countryId, addressType ]);

            me.resetStateSelections(addressType);

            $stateContainers = me.$stateContainers.filter('[data-address-type="' + addressType + '"]');

            // if there is no address type defined or no targets are found, fall back to all state containers
            if ($stateContainers.length === 0) {
                $stateContainers = me.$stateContainers;
            }

            $stateContainers = $stateContainers.filter('[data-country-id="' + countryId + '"]');

            if ($stateContainers.length) {
                $stateContainers.removeClass(me.opts.hiddenClass);
                $select = $stateContainers.find('select');
                $select.removeAttr('disabled');

                if ((plugin = $select.data('plugin_swSelectboxReplacement'))) {
                    plugin.$el.removeClass(me.opts.hiddenClass);
                    plugin.$wrapEl.removeClass(me.opts.hiddenClass);
                    plugin.setEnabled();
                }
            }

            $.publish('plugin/swRegister/onCountryChanged', [ me, event, countryId, addressType ]);
        },

        /**
         * Called every time the country selection changes. This method disables and hides all state selections
         * to prevent sending invalid data. The caller method needs to make sure, that the correct
         * state selection gets activated and shown again.
         *
         * @public
         * @method resetStateSelections
         * @param {String} addressType
         */
        resetStateSelections: function (addressType) {
            var me = this,
                plugin,
                $select,
                $stateContainers,
                $stateContainer;

            $stateContainers = me.$stateContainers.filter('[data-address-type="' + addressType + '"]');
            if ($stateContainers.length === 0) {
                $stateContainers = me.$stateContainers;
            }

            $.each($stateContainers, function(index, stateContainer) {
                $stateContainer = $(stateContainer);
                $select = $stateContainer.find('select');

                if (plugin = $select.data('plugin_swSelectboxReplacement')) {
                    plugin.setDisabled();
                } else {
                    $select.attr('disabled', 'disabled');
                }

                $stateContainer.addClass(me.opts.hiddenClass);
            });
        },

        /**
         * Called when another payment method was selected.
         * Depending on the selection, the payment field set will be toggled.
         *
         * @public
         * @method onPaymentChanged
         */
        onPaymentChanged: function () {
            var me = this,
                opts = me.opts,
                inputClass = opts.inputSelector,
                hiddenClass = opts.hiddenClass,
                inputSelector = opts.paymentInputSelector,
                paymentSelector = opts.paymentFieldSelector,
                requiredMethod,
                $fieldSet,
                isChecked,
                radio,
                $el;

            $.each(me.$paymentMethods, function (index, el) {
                $el = $(el);

                radio = $el.find(inputSelector);
                isChecked = radio[0].checked;

                requiredMethod = (isChecked) ? me.setHtmlRequired : me.removeHtmlRequired;

                requiredMethod($el.find(inputClass));

                $fieldSet = $el.find(paymentSelector);
                $fieldSet[((isChecked) ? 'removeClass' : 'addClass')](hiddenClass);
            });

            $.publish('plugin/swRegister/onPaymentChanged', [ me ]);
        },

        /**
         * Will be called when the submit button was clicked.
         * Loops through all input fields and checks if they have a value.
         * When no value is available, the field will be marked with an error.
         *
         * @public
         * @method onSubmitBtn
         */
        onSubmitBtn: function () {
            var me = this,
                $input;

            me.$inputs.each(function () {
                $input = $(this);

                if (!$input.val()) {
                    me.setFieldAsError($input);
                }
            });

            $.publish('plugin/swRegister/onSubmitButton', [ me ]);
        },

        /**
         * Called when a input field lost its focus.
         * Depending on the elements id, the corresponding method will be called.
         * billing ust id, emails and passwords will be validated via AJAX.
         *
         * @public
         * @method onValidateInput
         * @param {jQuery.Event} event
         */
        onValidateInput: function (event) {
            var me = this,
                $el = $(event.target),
                id = $el.attr('id'),
                action,
                relatedTarget = event.relatedTarget || document.activeElement;

            me.$targetElement = $(relatedTarget);

            switch (id) {
                case 'register_personal_email':
                case 'register_personal_emailConfirmation':
                    action = 'ajax_validate_email';
                    break;
                case 'register_billing_ustid':
                    action = 'ajax_validate_billing';
                    break;
                case 'register_personal_password':
                case 'register_personal_passwordConfirmation':
                    action = 'ajax_validate_password';
                    break;
                default:
                    break;
            }

            if (!$el.val() && $el.attr('required')) {
                me.setFieldAsError($el);
            } else if ($el.attr('type') === 'checkbox' && !$el.is(':checked')) {
                me.setFieldAsError($el);
            } else if (action) {
                me.validateUsingAjax($el, action);
            } else {
                me.setFieldAsSuccess($el);
            }

            $.publish('plugin/swRegister/onValidateInput', [ me, event, action ]);
        },

        /**
         * Adds additional attributes to the given elements to indicate
         * the elements to be required.
         *
         * @private
         * @method setHtmlRequired
         * @param {jQuery} $elements
         */
        setHtmlRequired: function ($elements) {
            $elements.attr({
                'required': 'required',
                'aria-required': 'true'
            });

            $.publish('plugin/swRegister/onSetHtmlRequired', [ this, $elements ]);
        },

        /**
         * Removes addition attributes that indicate the input as required.
         *
         * @public
         * @method removeHtmlRequired
         * @param {jQuery} $inputs
         */
        removeHtmlRequired: function ($inputs) {
            $inputs.removeAttr('required aria-required');

            $.publish('plugin/swRegister/onRemoveHtmlRequired', [ this, $inputs ]);
        },

        /**
         * Adds the defined error class to the given field or calls the
         * setError() method of the selectboxReplacement plugin if its
         * registered on the element.
         *
         * @public
         * @method setFieldAsError
         * @param {jQuery} $el
         */
        setFieldAsError: function ($el) {
            var me = this,
                plugin;

            if ((plugin = $el.data('plugin_swSelectboxReplacement'))) {
                plugin.setError();
            } else {
                $el.addClass(me.opts.errorClass);
            }

            $.publish('plugin/swRegister/onSetFieldAsError', [ me, $el ]);
        },

        /**
         * Removes the defined error class to the given field or calls the
         * removeError() method of the selectboxReplacement plugin if its
         * registered on the element.
         *
         * @public
         * @method setFieldAsSuccess
         * @param {jQuery} $el
         */
        setFieldAsSuccess: function ($el) {
            var me = this,
                plugin;

            if ((plugin = $el.data('plugin_swSelectboxReplacement'))) {
                plugin.removeError();
            } else {
                $el.removeClass(me.opts.errorClass);
            }

            $.publish('plugin/swRegister/onSetFieldAsSuccess', [ me, $el ]);
        },

        /**
         * Sends an ajax request to validate a given field server side.
         *
         * @public
         * @method validateUsingAjax
         * @param {jQuery} $input
         * @param {String} action
         */
        validateUsingAjax: function ($input, action) {
            var me = this,
                data = 'action=' + action + '&' + me.$el.find('form').serialize(),
                URL = window.controller.ajax_validate + '/' + action;

            if (!URL) {
                return;
            }

            $.publish('plugin/swRegister/onValidateBefore', [ me, data, URL ]);

            $.ajax({
                'data': data,
                'type': 'post',
                'dataType': 'json',
                'url': URL,
                'success': $.proxy(me.onValidateSuccess, me, action, $input)
            });
        },

        /**
         * This method gets called when the server side validation request
         * was successfully called. Updates the corresponding fields
         * and adds/removes error messages.
         *
         * @public
         * @method onValidateSuccess
         * @param {String} action
         * @param {jQuery} $input
         * @param {Object} result
         */
        onValidateSuccess: function (action, $input, result) {
            var me = this,
                isError,
                errorMessages = [],
                skipEmailConfirmationError = me.$targetElement.attr('name') == me.$personalEmailConfirmation.attr('name') && typeof me.$personalEmailConfirmation.val() === 'undefined',
                skipPasswordConfirmationError = me.$targetElement.attr('name') == me.$personalPasswordConfirmation.attr('name') && typeof me.$personalPasswordConfirmation.val() === 'undefined';

            $('#' + action + '--message').remove();

            if (!result) {
                return;
            }

            if (skipEmailConfirmationError) {
                result['emailConfirmation'] = false;
            } else if (skipPasswordConfirmationError) {
                result['passwordConfirmation'] = false;
            }

            for (var key in result) {
                //fields with `false` are now valid
                isError = result[key] ? true : false;

                if (!isError) {
                    continue;
                }

                if (key == 'emailConfirmation' && skipEmailConfirmationError) {
                    result[key] = false;
                    continue;
                } else if (key == 'passwordConfirmation' && skipPasswordConfirmationError) {
                    result[key] = false;
                    continue;
                }

                if ($input.attr('name') == me.$personalEmailConfirmation.attr('name') || $input.attr('name') == me.$personalGuest.attr('name')) {
                    $input = me.$personalEmail;
                } else if ($input.attr('name') == me.$personalPasswordConfirmation.attr('name')) {
                    $input = me.$personalPassword;
                }

                errorMessages.push(result[key]);
            }

            if (result) {
                me.updateFieldFlags(result);
            }

            if (errorMessages && errorMessages.length) {
                $('<div>', {
                    'html': '<p>' + errorMessages.join('<br/>') + '</p>',
                    'id': action + '--message',
                    'class': me.opts.errorMessageClass
                }).insertAfter($input);

                me.setFieldAsError($input);
            }

            $.publish('plugin/swRegister/onValidateSuccess', [ me, $input ]);
        },

        /**
         * Loops through all flags and updates the error/success status
         * of the corresponding elements.
         *
         * @public
         * @method updateFieldFlags
         * @param {Object} flags
         */
        updateFieldFlags: function (flags) {
            var me = this,
                $el = me.$el,
                keys = Object.keys(flags),
                len = keys.length,
                i = 0,
                flag,
                $input;

            for (; i < len; i++) {
                flag = keys[i];
                $input = $el.find('.' + flag);

                if (flags[flag]) {
                    me.setFieldAsError($input);
                    continue;
                }

                me.setFieldAsSuccess($input);
            }

            $.publish('plugin/swRegister/onUpdateFields', [ me, flags ]);
        },

        /**
         * Destroys the initialized plugin completely, so all event listeners will
         * be removed and the plugin data, which is stored in-memory referenced to
         * the DOM node.
         *
         * @public
         * @method destroy
         */
        destroy: function () {
            this._destroy();
        }
    });
})(jQuery);

;(function ($, window) {
    'use strict';

    var emptyFn = function () {},
        $html = $('html');

    /**
     * Shopware Modal Module
     *
     * The modalbox is "session based".
     * That means, that an .open() call will completely override the settings of the previous .open() calls.
     *
     * @example
     *
     * Simple content / text:
     *
     * $.modal.open('Hello World', {
     *     title: 'My title'
     * });
     *
     * Ajax loading:
     *
     * $.modal.open('account/ajax_login', {
     *     mode: 'ajax'
     * });
     *
     * Iframe example / YouTube Video:
     *
     * $.modal.open('http://www.youtube.com/embed/5dxVfU-yerQ', {
     *     mode: 'iframe'
     * });
     *
     * To close the modal box simply call:
     *
     * $.modal.close();
     *
     * @type {Object}
     */
    $.modal = {
        /**
         * The complete template wrapped in jQuery.
         *
         * @private
         * @property _$modalBox
         * @type {jQuery}
         */
        _$modalBox: null,

        /**
         * Container for the title wrapped in jQuery.
         *
         * @private
         * @property _$header
         * @type {jQuery}
         */
        _$header: null,

        /**
         * The title element wrapped in jQuery.
         *
         * @private
         * @property _$title
         * @type {jQuery}
         */
        _$title: null,

        /**
         * The content element wrapped in jQuery.
         *
         * @private
         * @property _$content
         * @type {jQuery}
         */
        _$content: null,

        /**
         * The close button wrapped in jQuery.
         *
         * @private
         * @property _$closeButton
         * @type {jQuery}
         */
        _$closeButton: null,

        /**
         * Default options of a opening session.
         *
         * @public
         * @property defaults
         * @type {jQuery}
         */
        defaults: {
            /**
             * The mode in which the lightbox should be showing.
             *
             * 'local':
             *
             * The given content is either text or HTML.
             *
             * 'ajax':
             *
             * The given content is the URL from what it should load the HTML.
             *
             * 'iframe':
             *
             * The given content is the source URL of the iframe.
             *
             * @type {String}
             */
            mode: 'local',

            /**
             * Sizing mode of the modal box.
             *
             * 'auto':
             *
             * Will set the given width as max-width so the container can shrink.
             * Fullscreen mode on small mobile devices.
             *
             * 'fixed':
             *
             * Will use the width and height as static sizes and will not change to fullscreen mode.
             *
             * 'content':
             *
             * Will use the height of its content instead of a given height.
             * The 'height' option will be ignored when set.
             *
             * 'full':
             *
             * Will set the modalbox to fullscreen.
             *
             * @type {String}
             */
            sizing: 'auto',

            /**
             * The width of the modal box window.
             *
             * @type {Number}
             */
            width: 600,

            /**
             * The height of the modal box window.
             *
             * @type {Number}
             */
            height: 600,

            /**
             * The max height if sizing is set to `content`
             * 
             * @type {Number}
             */
            maxHeight: 0,

            /**
             * Whether or not the overlay should be shown.
             *
             * @type {Boolean}
             */
            overlay: true,

            /**
             * Whether or not the modal box should be closed when the user clicks on the overlay.
             *
             * @type {Boolean}
             */
            closeOnOverlay: true,

            /**
             * Whether or not the closing button should be shown.
             *
             * @type {Boolean}
             */
            showCloseButton: true,

            /**
             * Speed for every CSS transition animation
             *
             * @type {Number}
             */
            animationSpeed: 500,

            /**
             * The window title of the modal box.
             * If empty, the header will be hidden.
             *
             * @type {String}
             */
            title: '',

            /**
             * Will be overridden by the current URL when the mode is 'ajax' or 'iframe'.
             * Can be accessed by the options object.
             *
             * @type {String}
             */
            src: '',

            /**
             * Array of key codes the modal box can be closed.
             *
             * @type {Array}
             */
            closeKeys: [27],

            /**
             * Whether or not it is possible to close the modal box by the keyboard.
             *
             * @type {Boolean}
             */
            keyboardClosing: true,

            /**
             * Function which will be called when the modal box is closing.
             *
             * @type {Function}
             */
            onClose: emptyFn,

            /**
             * Whether or not the picturefill function will be called when setting content.
             *
             * @type {Boolean}
             */
            updateImages: false,

            /**
             * Class that will be added to the modalbox.
             *
             * @type {String}
             */
            additionalClass: ''
        },

        /**
         * The current merged options of the last .open() call.
         *
         * @public
         * @property options
         * @type {Object}
         */
        options: {},

        /**
         * Opens the modal box.
         * Sets the given content and applies the given options to the current session.
         * If given, the overlay options will be passed in its .open() call.
         *
         * @public
         * @method open
         * @param {String|jQuery|HTMLElement} content
         * @param {Object} options
         */
        open: function (content, options) {
            var me = this,
                $modalBox = me._$modalBox,
                opts;

            me.options = opts = $.extend({}, me.defaults, options);

            if (opts.overlay) {
                $.overlay.open($.extend({}, {
                    closeOnClick: opts.closeOnOverlay,
                    onClose: $.proxy(me.onOverlayClose, me)
                }));
            }

            if (!$modalBox) {
                me.initModalBox();
                me.registerEvents();

                $modalBox = me._$modalBox;
            }

            me._$closeButton.toggle(opts.showCloseButton);

            $modalBox.toggleClass('sizing--auto', opts.sizing === 'auto');
            $modalBox.toggleClass('sizing--fixed', opts.sizing === 'fixed');
            $modalBox.toggleClass('sizing--content', opts.sizing === 'content');
            $modalBox.toggleClass('no--header', opts.title.length === 0);

            $modalBox.addClass(opts.additionalClass);

            if (opts.sizing === 'content') {
                opts.height = 'auto';
            } else {
                $modalBox.css('top', 0);
            }

            me.setTitle(opts.title);
            me.setWidth(opts.width);
            me.setHeight(opts.height);
            me.setMaxHeight(opts.maxHeight);

            // set display to block instead of .show() for browser compatibility
            $modalBox.css('display', 'block');

            switch (opts.mode) {
                case 'ajax':
                    $.ajax(content, {
                        data: {
                            isXHR: 1
                        },
                        success: function (result) {
                            me.setContent(result);
                            $.publish('plugin/swModal/onOpenAjax', me);
                        }
                    });
                    me.options.src = content;
                    break;
                case 'iframe':
                    me.setContent('<iframe class="content--iframe" src="' + content + '" width="100%" height="100%"></iframe>');
                    me.options.src = content;
                    break;
                default:
                    me.setContent(content);
                    break;
            }

            me.setTransition({
                opacity: 1
            }, me.options.animationSpeed, 'linear');

            $html.addClass('no--scroll');

            $.publish('plugin/swModal/onOpen', [ me ]);

            return me;
        },

        /**
         * Closes the modal box and the overlay if its enabled.
         * if the fading is completed, the content will be removed.
         *
         * @public
         * @method close
         */
        close: function () {
            var me = this,
                opts = me.options,
                $modalBox = me._$modalBox;

            if (opts.overlay) {
                $.overlay.close();
            }

            $html.removeClass('no--scroll');

            if ($modalBox !== null) {
                me.setTransition({
                    opacity: 0
                }, opts.animationSpeed, 'linear', function () {
                    $modalBox.removeClass(opts.additionalClass);

                    // set display to none instead of .hide() for browser compatibility
                    $modalBox.css('display', 'none');

                    opts.onClose.call(me);

                    me._$content.empty();
                });
            }

            $.publish('plugin/swModal/onClose', [ me ]);

            return me;
        },

        /**
         * Sets the title of the modal box.
         *
         * @public
         * @method setTransition
         * @param {Object} css
         * @param {Number} duration
         * @param {String} animation
         * @param {Function} callback
         */
        setTransition: function (css, duration, animation, callback) {
            var me = this,
                $modalBox = me._$modalBox,
                opts = $.extend({
                    animation: 'ease',
                    duration: me.options.animationSpeed
                }, {
                    animation: animation,
                    duration: duration
                });

            if (!$.support.transition) {
                $modalBox.stop(true).animate(css, opts.duration, opts.animation, callback);
                return;
            }

            $modalBox.stop(true).transition(css, opts.duration, opts.animation, callback);

            $.publish('plugin/swModal/onSetTransition', [ me, css, opts ]);
        },

        /**
         * Sets the title of the modal box.
         *
         * @public
         * @method setTitle
         * @param {String} title
         */
        setTitle: function (title) {
            var me = this;

            me._$title.html(title);

            $.publish('plugin/swModal/onSetTitle', [ me, title ]);
        },

        /**
         * Sets the content of the modal box.
         *
         * @public
         * @method setContent
         * @param {String|jQuery|HTMLElement} content
         */
        setContent: function (content) {
            var me = this,
                opts = me.options;

            me._$content.html(content);

            if (opts.sizing === 'content') {
                // initial centering
                me.center();

                // centering again to fix some styling/positioning issues
                window.setTimeout(me.center.bind(me), 25);
            }

            if (opts.updateImages) {
                picturefill();
            }

            $.publish('plugin/swModal/onSetContent', [ me ]);
        },

        /**
         * Sets the width of the modal box.
         * If a string was passed containing a only number, it will be parsed as a pixel value.
         *
         * @public
         * @method setWidth
         * @param {Number|String} width
         */
        setWidth: function (width) {
            var me = this;

            me._$modalBox.css('width', (typeof width === 'string' && !(/^\d+$/.test(width))) ? width : parseInt(width, 10));

            $.publish('plugin/swModal/onSetWidth', [ me ]);
        },

        /**
         * Sets the height of the modal box.
         * If a string was passed containing a only number, it will be parsed as a pixel value.
         *
         * @public
         * @method setHeight
         * @param {Number|String} height
         */
        setHeight: function (height) {
            var me = this,
                hasTitle = me._$title.text().length > 0,
                headerHeight;

            height = (typeof height === 'string' && !(/^\d+$/.test(height))) ? height : window.parseInt(height, 10);

            if(hasTitle) {
                headerHeight = window.parseInt(me._$header.css('height'), 10);
                me._$content.css('height', (height - headerHeight));
            } else {
                me._$content.css('height', '100%');
            }

            me._$modalBox.css('height', height);
            $.publish('plugin/swModal/onSetHeight', [ me ]);
        },

        /**
         * Sets the max height of the modal box if the provided value is not empty or greater than 0.
         * If a string was passed containing a only number, it will be parsed as a pixel value.
         *
         * @public
         * @method setMaxHeight
         * @param {Number|String} height
         */
        setMaxHeight: function (height) {
            var me = this;

            if (!height) {
                return;
            }

            height = (typeof height === 'string' && !(/^\d+$/.test(height))) ? height : window.parseInt(height, 10);

            me._$modalBox.css('max-height', height);
            $.publish('plugin/swModal/onSetMaxHeight', [ me ]);
        },

        /**
         * Creates the modal box and all its elements.
         * Appends it to the body.
         *
         * @public
         * @method initModalBox
         */
        initModalBox: function () {
            var me = this;

            me._$modalBox = $('<div>', {
                'class': 'js--modal'
            });

            me._$header = $('<div>', {
                'class': 'header'
            }).appendTo(me._$modalBox);

            me._$title = $('<div>', {
                'class': 'title'
            }).appendTo(me._$header);

            me._$content = $('<div>', {
                'class': 'content'
            }).appendTo(me._$modalBox);

            me._$closeButton = $('<div>', {
                'class': 'btn icon--cross is--small btn--grey modal--close'
            }).appendTo(me._$modalBox);

            $('body').append(me._$modalBox);

            $.publish('plugin/swModal/onInit', [ me ]);
        },

        /**
         * Registers all needed event listeners.
         *
         * @public
         * @method registerEvents
         */
        registerEvents: function () {
            var me = this,
                $window = $(window);

            me._$closeButton.on('click.modal touchstart.modal', $.proxy(me.close, me));

            $window.on('keydown.modal', $.proxy(me.onKeyDown, me));
            StateManager.on('resize', me.onWindowResize, me);

            StateManager.registerListener({
                state: 'xs',
                enter: function() {
                    me._$modalBox.addClass('is--fullscreen');
                },
                exit: function () {
                    me._$modalBox.removeClass('is--fullscreen');
                }
            });

            $.publish('plugin/swModal/onRegisterEvents', [ me ]);
        },

        /**
         * Called when a key was pressed.
         * Closes the modal box when the keyCode is mapped to a close key.
         *
         * @public
         * @method onKeyDown
         */
        onKeyDown: function (event) {
            var me = this,
                keyCode = event.which,
                keys = me.options.closeKeys,
                len = keys.length,
                i = 0;

            if (!me.options.keyboardClosing) {
                return;
            }

            for (; i < len; i++) {
                if (keys[i] === keyCode) {
                    me.close();
                }
            }

            $.publish('plugin/swModal/onKeyDown', [ me, event, keyCode ]);
        },

        /**
         * Called when the window was resized.
         * Centers the modal box when the sizing is set to 'content'.
         *
         * @public
         * @method onWindowResize
         */
        onWindowResize: function (event) {
            var me = this;

            if (me.options.sizing === 'content') {
                me.center();
            }

            $.publish('plugin/swModal/onWindowResize', [ me, event ]);
        },

        /**
         * Sets the top position of the modal box to center it to the screen
         *
         * @public
         * @method centerModalBox
         */
        center: function () {
            var me = this,
                $modalBox = me._$modalBox;

            $modalBox.css('top', ($(window).height() - $modalBox.height()) / 2);

            $.publish('plugin/swModal/onCenter', [ me ]);
        },

        /**
         * Called when the overlay was clicked.
         * Closes the modalbox when the 'closeOnOverlay' option is active.
         *
         * @public
         * @method onOverlayClose
         */
        onOverlayClose: function () {
            var me = this;

            if (!me.options.closeOnOverlay) {
                return;
            }

            me.close();

            $.publish('plugin/swModal/onOverlayClick', [ me ]);
        },

        /**
         * Removes the current modalbox element from the DOM and destroys its items.
         * Also clears the options.
         *
         * @public
         * @method destroy
         */
        destroy: function () {
            var me = this,
                p;

            me._$modalBox.remove();

            me._$modalBox = null;
            me._$header = null;
            me._$title = null;
            me._$content = null;
            me._$closeButton = null;

            for (p in me.options) {
                if (!me.options.hasOwnProperty(p)) {
                    continue;
                }
                delete me.options[p];
            }

            StateManager.off('resize', me.onWindowResize, [ me ]);
        }
    };

    /**
     * Shopware Modalbox Plugin
     *
     * This plugin opens a offcanvas menu on click.
     * The content of the offcanvas can either be passed to the plugin
     * or the target element will be used as the content.
     */
    $.plugin('swModalbox', {

        defaults: {

            /**
             * Selector for the target when clicked on.
             * If no selector is passed, the element itself will be used.
             * When no content was passed, the target will be used as the content.
             *
             * @property targetSelector
             * @type {String}
             */
            targetSelector: '',

            /**
             * Optional content for the modal box.
             *
             * @property content
             * @type {String}
             */
            content: '',

            /**
             * Fetch mode for the modal box
             *
             * @property mode
             * @type {String}
             */
            mode: 'local'
        },

        /**
         * Initializes the plugin, applies addition data attributes and
         * registers events for clicking the target element.
         *
         * @public
         * @method init
         */
        init: function () {
            var me = this,
                opts;

            me.opts = $.extend({}, Object.create($.modal.defaults), me.opts);

            me.applyDataAttributes();

            opts = me.opts;

            me.$target = opts.targetSelector && (me.$target = me.$el.find(opts.targetSelector)).length ? me.$target : me.$el;

            me._isOpened = false;

            me._on(me.$target, 'click', $.proxy(me.onClick, me));

            $.subscribe('plugin/swModal/onClose', $.proxy(me.onClose, me));

            $.publish('plugin/swModalbox/onRegisterEvents', [ me ]);
        },

        /**
         * This method will be called when the target element was clicked.
         * Opens the actual modal box and uses the provided content.
         *
         * @public
         * @method onClick
         * @param {jQuery.Event} event
         */
        onClick: function (event) {
            event.preventDefault();

            var me = this,
                target = me.$target.length === 1 && me.$target || $(event.target);

            $.modal.open(me.opts.content || (me.opts.mode !== 'local' ? target.attr('href') : target), me.opts);

            me._isOpened = true;

            $.publish('plugin/swModalbox/onClick', [ me, event ]);
        },

        /**
         * This method will be called when the plugin specific modal box was closed.
         *
         * @public
         * @method onClick
         */
        onClose: function () {
            var me = this;

            me._isOpened = false;

            $.publish('plugin/swModalbox/onClose', [ me ]);
        },

        /**
         * This method closes the modal box when its opened, destroys
         * the plugin and removes all registered events
         *
         * @public
         * @method destroy
         */
        destroy: function () {
            var me = this;

            if (me._isOpened) {
                $.modal.close();
            }

            $.unsubscribe('plugin/swModal/onClose', $.proxy(me.onClose, me));

            me._destroy();
        }
    });
})(jQuery, window);


;(function ($, window, document, undefined) {
    "use strict";

    $.plugin('swSelectboxReplacement', {

        /** @property {Object} Default settings for the plugin **/
        defaults: {

            /** @property {String} Basic class name for the plugin. */
            'baseCls': 'js--fancy-select',

            /** @property {String} Focus class. */
            'focusCls': 'js--is--focused',

            /** @property {String} Text / html content for the trigger field. */
            'triggerText': '<i class="icon--arrow-down"></i>',

            /** @property {String} Class which indicates that the field is disabled. */
            'disabledCls': 'is--disabled',

            /** @property {String} Class which indicates that the field has an error. */
            'errorCls': 'has--error',

            /** @property {boolean} Truthy to set all the classes on the parent element to the wrapper element. */
            'compatibility': true,

            /** @property {String} Additional css class for styling purpose */
            'class': ''
        },

        /**
         * Initializes the plugin
         *
         * @returns {Plugin}
         */
        init: function () {
            var me = this;

            // Update the plugin configuration with the HTML5 data-attributes
            me.applyDataAttributes();

            me.$wrapEl = me.createTemplate(me.$el);
            me.registerEventListeners();

            // Disable the select box
            if (me.$el.attr('disabled') !== undefined) {
                me.setDisabled();
            }

            // Support marking the field as error
            if (me.$el.hasClass(me.opts.errorCls)) {
                me.setError();
            }

            // Set the compatibility classes
            if (me.opts.compatibility) {
                me._setCompatibilityClasses();
            }

            return me;
        },

        /**
         * Creates the neccessary DOM structure and wraps the {@link me.$el} into the newly created
         * structure.
         * @param {jQuery} $el - HTMLElement which fires the plugin.
         * @returns {jQuery} wrapEl - jQuery object of the newly created structure
         */
        createTemplate: function ($el) {
            var me = this,
                wrapEl;

            // We need to use the array syntax here due to the fact that ```class``` is a reserved keyword in IE and Safari
            wrapEl = me._formatString('<div class="{0}"></div>', me.opts.baseCls + ' ' + me.opts['class']);
            wrapEl = $el.wrap(wrapEl).parents('.' + me.opts.baseCls);

            me.$textEl = $('<div>', { 'class': me.opts.baseCls + '-text' }).appendTo(wrapEl);
            me.$triggerEl =$('<div>', { 'class': me.opts.baseCls + '-trigger', 'html': me.opts.triggerText }).appendTo(wrapEl);

            me.selected = me.$el.find(':selected');
            me.$textEl.html(me.selected.html());

            $.publish('plugin/swSelectboxReplacement/onCreateTemplate', [ me, wrapEl ]);

            return wrapEl;
        },

        /**
         * Disables the select box
         * @returns {jQuery|Plugin.$el|*|PluginBase.$el}
         */
        setDisabled: function () {
            var me = this;

            me.$wrapEl.addClass(me.opts.disabledCls);
            me.$el.attr('disabled', 'disabled');

            $.publish('plugin/swSelectboxReplacement/onSetDisabled', [ me ]);

            return me.$el;
        },

        /**
         * Enables the select box
         * @returns {jQuery|Plugin.$el|*|PluginBase.$el}
         */
        setEnabled: function () {
            var me = this;

            me.$wrapEl.removeClass(me.opts.disabledCls);
            me.$el.removeAttr('disabled');

            $.publish('plugin/swSelectboxReplacement/onSetEnabled', [ me ]);

            return me.$el;
        },

        /**
         * Marks the field as error.
         * @returns {jQuery}
         */
        setError: function () {
            var me = this;

            me.$wrapEl.addClass(me.opts.errorCls);

            $.publish('plugin/swSelectboxReplacement/onSetError', [ me ]);

            return me.$wrapEl;
        },

        /**
         * Removes the error mark of the field.
         * @returns {jQuery}
         */
        removeError: function () {
            var me = this;

            me.$wrapEl.removeClass(me.opts.errorCls);

            $.publish('plugin/swSelectboxReplacement/onRemoveError', [ me ]);

            return me.$wrapEl;
        },

        /**
         * Wrapper method for jQuery's ```val``` method.
         * @returns {jQuery}
         */
        val: function() {
            var me = this, val;

            val = me.$el.val.apply(me.$el, arguments);

            if(typeof arguments[0] !== 'function') {
                me.setSelectedOnTextElement();
            }

            $.publish('plugin/swSelectboxReplacement/onSetVal', [ me ]);

            return val;
        },

        /**
         * Wrapper method for jQuery's ```show``` method.
         * @returns {jQuery}
         */
        show: function() {
            var me = this;

            me.$wrapEl.show.apply(me.$wrapEl, arguments);

            $.publish('plugin/swSelectboxReplacement/onShow', [ me ]);

            return me.$wrapEl;
        },

        /**
         * Wrapper method for jQuery's ```hide``` method.
         * @returns {jQuery}
         */
        hide: function() {
            var me = this;

            me.$wrapEl.hide.apply(me.$wrapEl, arguments);

            $.publish('plugin/swSelectboxReplacement/onHide', [ me ]);

            return me.$wrapEl;
        },

        /**
         * Registers the neccessary event listeners for the plugin.
         *
         * @returns {boolean}
         */
        registerEventListeners: function () {
            var me = this;

            me._on(me.$el, 'change', $.proxy(me.onChange, me));
            me._on(me.$el, 'keyup', $.proxy(me.onKeyUp, me));
            me._on(me.$el, 'focus', $.proxy(me.onFocus, me));
            me._on(me.$el, 'blur', $.proxy(me.onBlur, me));

            $.publish('plugin/swSelectboxReplacement/onRegisterEvents', [ me ]);

            return true;
        },

        /**
         * Helper method which reads out the selected entry from the "select" element
         * and writes it into the text element which is visible to the user.
         *
         * @returns {String} selected entry from the "select" element
         */
        setSelectedOnTextElement: function () {
            var me = this;

            me.selected = me.$el.find(':selected');
            me.$textEl.html(me.selected.html());

            $.publish('plugin/swSelectboxReplacement/onSetSelected', [ me, me.selected ]);

            return me.selected;
        },

        /**
         * Event listener method which will be fired when the user
         * changes the value of the select box.
         *
         * @event `change`
         * @param {Object} event - jQuery event eOpts
         * @returns {void}
         */
        onChange: function () {
            var me = this;

            me.setSelectedOnTextElement();

            $.publish('plugin/swSelectboxReplacement/onChange', [ me ]);
        },

        /**
         * Event listener which fires on key up on the "select" element.
         *
         * Checks if the user presses the up or down key to update the
         * text element with the currently selected entry in the select box.
         *
         * @event `keyup`
         * @param {Object} event - jQuery event eOpts
         * @returns {boolean}
         */
        onKeyUp: function (event) {
            var me = this;

            // 38 = up arrow, 40 = down arrow
            if(event.which === 38 || event.which === 40) {
                me.setSelectedOnTextElement();
            }

            $.publish('plugin/swSelectboxReplacement/onKeyUp', [ me ]);

            return false;
        },

        /**
         * Event listener which fires on focus on the "select" element.
         *
         * Just adds a class for styling purpose.
         *
         * @returns {void}
         */
        onFocus: function () {
            var me = this;

            me.$wrapEl.addClass(me.opts.focusCls);

            $.publish('plugin/swSelectboxReplacement/onFocus', [ me ]);
        },

        /**
         * Event listener which fires on blur on the "select" element.
         *
         * Just removes a class which was set for styling purpose.
         *
         * @returns {void}
         */
        onBlur: function () {
            var me = this;

            me.$wrapEl.removeClass(me.opts.focusCls);

            $.publish('plugin/swSelectboxReplacement/onBlur', [ me ]);
        },

        /**
         * Applies all the classes from the ```field--select``` parent element to the {@link me.$wrapEl}.
         *
         * @returns {boolean}
         * @private
         */
        _setCompatibilityClasses: function () {
            var me = this,
                $el = me.$el,
                $parent = $el.parents('.field--select'),
                classList;

            if(!$parent || !$parent.length) {
                return false;
            }
            classList = $parent.attr('class').split(/\s+/);
            $.each(classList, function () {
                me.$wrapEl.addClass(this);
            });

            return true;
        },

        /**
         * Allows you to define a tokenized string and pass an arbitrary number of arguments to replace the tokens.
         * Each token must be unique, and must increment in the format {0}, {1}, etc.
         *
         * @example Sample usage
         *    me._formatString('<div class="{0}">Text</div>', 'test');
         *
         * @param {String} str - The tokenized string to be formatted.
         * @returns {String} The formatted string
         * @private
         */
        _formatString: function (str) {
            var i = 1,
                len = arguments.length;

            for (; i < len; i++) {
                str = str.replace('{' + (i - 1) + '}', arguments[i]);
            }
            return str;
        }
    });
})(jQuery, window, document);

;(function($, window) {
    'use strict';

    /**
     * Shopware Captcha Plugin.
     *
     * @example
     *
     * Call the plugin on a node with a "data-src" attribute.
     * This attribute should provide the url for retrieving the captcha.
     *
     * HTML:
     *
     * <div data-src="CAPTCHA_REFRESH_URL" data-captcha="true"></div>
     *
     * JS:
     *
     * $('*[data-captcha="true"]').swCaptcha();
     *
     */
    $.plugin('swCaptcha', {

        /**
         * Default plugin initialisation function.
         * Registers all needed event listeners and sends a request to load the captcha image.
         *
         * @public
         * @method init
         */
        init: function () {
            var me = this,
                $el = me.$el,
                url = $el.attr('data-src'),
                $window = $(window);

            if (!url || !url.length) {
                return;
            }

            // fix bfcache from caching the captcha/whole rendered page
            me._on($window, 'unload', function () {});
            me._on($window, 'pageshow', function (event) {
                if (event.originalEvent.persisted) {
                    me.sendRequest(url);
                }
            });

            me.sendRequest(url);
        },

        /**
         * Sends an ajax request to the passed url and sets the result into the plugin's element.
         *
         * @public
         * @method _sendRequest
         * @param {String} url
         */
        sendRequest: function (url) {
            var me = this,
                $el = me.$el;

            $.ajax({
                url: url,
                cache: false,
                success: function (response) {
                    $el.html(response);

                    $.publish('plugin/swCaptcha/onSendRequestSuccess', [ me ]);
                }
            });

            $.publish('plugin/swCaptcha/onSendRequest', [ me ]);
        }
    });
})(jQuery, window);

;(function($) {
    'use strict';

    $.plugin('swDropdownMenu', {

        defaults: {
            activeCls: 'js--is--dropdown-active',
            preventDefault: true,
            closeOnBody: true
        },

        init: function () {
            var me = this;

            me._on(me.$el, 'touchstart click', $.proxy(me.onClickMenu, me));

            $.publish('plugin/swDropdownMenu/onRegisterEvents', [ me ]);
        },

        onClickMenu: function (event) {
            var me = this;

            if($(event.target).is('.service--link, .compare--list, .compare--entry, .compare--link, .btn--item-delete, .compare--icon-remove')) {
                return;
            }

            if (me.opts.preventDefault) {
                event.preventDefault();
            }

            me.$el.toggleClass(me.opts.activeCls);

            if (me.opts.closeOnBody) {
                event.stopPropagation();
                $('body').on(me.getEventName('touchstart click'), $.proxy(me.onClickBody, me));
            }

            $.publish('plugin/swDropdownMenu/onClickMenu', [ me, event ]);
        },

        onClickBody: function(event) {
            var me = this;

            if($(event.target).is('.service--link, .compare--list, .compare--entry, .compare--link, .btn--item-delete, .compare--icon-remove')) {
                return;
            }

            event.preventDefault();

            $('body').off(me.getEventName('touchstart click'));

            me.$el.removeClass(me.opts.activeCls);

            $.publish('plugin/swDropdownMenu/onClickBody', [ me, event ]);
        },

        destroy: function () {
            var me = this;

            me._destroy();
        }
    });
})(jQuery);

;(function ($) {
    "use strict";

    /**
     * jQuery loading indicator component.
     *
     * @type {Object}
     */
    $.loadingIndicator = {
        /**
         * The loader jQuery element.
         * Will be created when opening the indicator.
         * Contains the loading icon.
         *
         * @type {Null|jQuery}
         * @private
         */
        $loader: null,

        /**
         * The default options for the indicator.
         * When certain options were not passed, these will be used instead.
         *
         * @type {Object}
         */
        defaults: {
            loaderCls: 'js--loading-indicator',
            iconCls: 'icon--default',
            delay: 0,
            animationSpeed: 500,
            closeOnClick: true,
            openOverlay: true
        },

        /**
         * The extended options for the current opened overlay.
         *
         * @type {Object}
         */
        options: {},

        /**
         * Opens/Shows the loading indicator along with the overlay.
         * If the loader is not available, it will be created.
         *
         * @param {Object} indicatorOptions
         */
        open: function (indicatorOptions) {
            var me = this;

            me.options = $.extend({}, me.defaults, indicatorOptions);

            if (me.$loader === null) {
                me.$loader = me._createLoader();
                $('body').append(me.$loader);
            }

            me._updateLoader();

            me._timeout = window.setTimeout(function() {
                if (me.options.openOverlay !== false) {
                    $.overlay.open($.extend({}, {
                        closeOnClick: me.options.closeOnClick,
                        onClose: me.close.bind(me)
                    }));
                }

                me.$loader.fadeIn(me.options.animationSpeed, function () {
                    $.publish('plugin/swLoadingIndicator/onOpenFinished', [ me ]);
                });
            }, me.options.delay);

            $.publish('plugin/swLoadingIndicator/onOpen', [ me ]);
        },

        /**
         * Closes the loader element along with the overlay.
         */
        close: function (callback) {
            var me = this,
                opts = me.options;

            callback = callback || function() {};

            if (me.$loader !== null) {
                me.$loader.fadeOut(opts.animationSpeed || me.defaults.animationSpeed, function () {
                    callback.call(me);

                    if(me._timeout) {
                        window.clearTimeout(me._timeout);
                    }

                    if (opts.openOverlay !== false) {
                        $.overlay.close();
                    }

                    $.publish('plugin/swLoadingIndicator/onCloseFinished', [ me ]);
                });
            }

            $.publish('plugin/swLoadingIndicator/onClose', [ me ]);
        },

        /**
         * Updates the loader element.
         * If the current loader/icon classes differentiate with the passed options, they will be set.
         *
         * @private
         */
        _updateLoader: function () {
            var me = this,
                opts = me.options,
                $loader = me.$loader,
                $icon = $($loader.children()[0]);

            if (!$loader.hasClass(opts.loaderCls)) {
                $loader.removeClass('').addClass(opts.loaderCls);
            }

            if (!$icon.hasClass(opts.iconCls)) {
                $icon.removeClass('').addClass(opts.iconCls);
            }
        },

        /**
         * Creates the loader with the indicator icon in it.
         *
         * @returns {jQuery}
         * @private
         */
        _createLoader: function () {
            var me = this,
                loader = $('<div>', {
                    'class': me.options.loaderCls
                }),
                icon = $('<div>', {
                    'class': me.options.iconCls
                });

            loader.append(icon);

            return loader;
        }
    };
})(jQuery);
/**
 * Shopware Overlay Module.
 *
 * Displays/Hides a fullscreen overlay with a certain color and opacity.
 *
 * @example
 *
 * Open th overlay:
 *
 * $.overlay.open({
     *     color: '#FF0000' //red
     *     opacity: 0.5
     * });
 *
 * Closing the overlay:
 *
 * $.overlay.close();
 *
 * @type {Object}
 */
;(function ($) {
    'use strict';

    var $overlay = $('<div>', {
            'class': 'js--overlay'
        }).appendTo('body'),

        isOpen = false,

        openClass = 'is--open',

        closableClass = 'is--closable',

        events = ['click', 'touchstart', 'MSPointerDown'].join('.overlay') + '.overlay',

        /**
         *
         * {
         *     // Whether or not the overlay should be closable by click.
         *     closeOnClick: {Boolean},
         *
         *     // Function that gets called every time the user clicks on the overlay.
         *     onClick: {Function},
         *
         *     // Function that gets called only when the overlay is closable and the user clicks on it.
         *     onClose: {Function}
         * }
         *
         * @param options
         */
        openOverlay = function (options) {
            if (isOpen) {
                updateOverlay(options);
                return;
            }
            isOpen = true;

            $overlay.addClass(openClass);

            if (options && options.closeOnClick !== false) {
                $overlay.addClass(closableClass);
            }

            $overlay.on(events, $.proxy(onOverlayClick, this, options));
        },

        closeOverlay = function () {
            if (!isOpen) {
                return;
            }
            isOpen = false;

            $overlay.removeClass(openClass + ' ' + closableClass);

            $overlay.off(events);
        },

        onOverlayClick = function (options) {
            if (options) {
                if (typeof options.onClick === 'function') {
                    options.onClick.call($overlay);
                }

                if (options.closeOnClick === false) {
                    return;
                }

                if (typeof options.onClose === 'function' && options.onClose.call($overlay) === false) {
                    return;
                }
            }

            closeOverlay();
        },

        updateOverlay = function (options) {
            $overlay.toggleClass(closableClass, options.closeOnClick !== false);

            $overlay.off(events);

            $overlay.on(events, $.proxy(onOverlayClick, this, options));
        };

    $overlay.on('mousewheel DOMMouseScroll', function (event) {
        event.preventDefault();
    });

    $.overlay = {
        open: openOverlay,

        close: closeOverlay,

        isOpen: function () {
            return isOpen;
        },

        getElement: function () {
            return $overlay;
        }
    };

})(jQuery);

;(function($) {
    'use strict';

    $.plugin('swFormPolyfill', {

        defaults: {
            eventType: 'click'
        },

        /**
         * Initializes the plugin and sets up all necessary event listeners.
         */
        init: function() {
            var me = this;

            // If the browser supports the feature, we don't need to take action
            if(!me.isSupportedBrowser()) {
                return false;
            }

            me.applyDataAttributes();
            me.registerEvents();
        },

        /**
         * Registers all necessary event listener.
         */
        registerEvents: function() {
            var me = this;

            me._on(me.$el, me.opts.eventType, $.proxy(me.onSubmitForm, this));

            $.publish('plugin/swFormPolyfill/onRegisterEvents', [ me ]);
        },

        /**
         * Wrapper method to return supported browser checks.
         *
         * @returns {Boolean|*|boolean}
         */
        isSupportedBrowser: function() {
            var me = this;

            return me.isIE() || me.isEdge();
        },

        /**
         * Checks if we're dealing with the internet explorer.
         *
         * @private
         * @returns {Boolean} Truthy, if the browser supports it, otherwise false.
         */
        isIE: function() {
            var myNav = navigator.userAgent.toLowerCase();
            return myNav.indexOf('msie') !== -1 || !!navigator.userAgent.match(/Trident.*rv[ :]*11\./);
        },

        /**
         * Checks if we're dealing with the Windows 10 Edge browser.
         *
         * @private
         * @returns {boolean}
         */
        isEdge: function() {
            var myNav = navigator.userAgent.toLowerCase();
            return myNav.indexOf('edge') !== -1;
        },

        /**
         * Event listener method which is necessary when the browser
         * doesn't support the ```form``` attribute on ```input``` elements.
         * @returns {boolean}
         */
        onSubmitForm: function() {
            var me = this,
                id = '#' + me.$el.attr('form'),
                $form = $(id);

            // We can't find the form
            if(!$form.length) {
                return false;
            }

            $form.submit();

            $.publish('plugin/swFormPolyfill/onSubmitForm', [ me, $form ]);
        },

        /**
         * Destroy method of the plugin.
         * Removes attached event listener.
         */
        destroy: function() {
            var me = this;

            me._destroy();
        }
    });
})(jQuery);

;(function($, window) {
    "use strict";

    /**
     * Pseudo text plugin
     *
     * The plugin provides an mechanism to duplicate the inserted text into another element. That behavior comes in
     * handy when you're dealing with complex layouts where the input element is placed outside of a ```form```-tag
     * but the value of the input needs to be send to the server-side.
     *
     * @example The example shows the basic usage:
     *
     * ```
     * <form>
     *    <textarea class="is--hidden my-field--hidden"></textarea>
     * </form>
     *
     * <textarea data-pseudo-text="true" data-selector=".my-field--hidden"></textarea>
     * ```
     */
    $.plugin('swPseudoText', {

        /**
         * Default settings for the plugin
         * @type {Object}
         */
        defaults: {
            /** @type {String} eventType - The event type which should be used to duplicate the content */
            eventType: 'keyup'
        },

        /**
         * Initializes the plugin and sets up the necessary event listeners.
         */
        init: function () {
            var me = this,
                selector = $(me.$el.attr('data-selector')),
                val;

            if(!selector.length) {
                throw new Error('Given selector does not match any element on the page.');
            }

            me._on(me.$el, me.opts.eventType, function() {
                val = me.$el.val();
                selector.val(val.length ? val : '');
            });
        }
    });
})(jQuery, window);

;(function ($) {

    var emptyObj = {};

    /**
     * Shopware Last Seen Products Plugin
     *
     * This plugin creates a list of collected articles.
     * Those articles will be collected, when the user opens a detail page.
     * The created list will be showed as a product slider.
     */
    $.plugin('swLastSeenProducts', {
        
        defaults: {

            /**
             * Limit of the products showed in the slider
             *
             * @property productLimit
             * @type {Number}
             */
            productLimit: 20,

            /**
             * Base url used for uniquely identifying an article
             *
             * @property baseUrl
             * @type {String}
             */
            baseUrl: '/',

            /**
             * ID of the current shop used for uniquely identifying an article.
             *
             * @property shopId
             * @type {Number}
             */
            shopId: 1,

            /**
             * Article that will be added to the list when we are
             * on the detail page.
             *
             * @property currentArticle
             * @type {Object}
             */
            currentArticle: emptyObj,

            /**
             * Selector for the product list used for the product slider
             *
             * @property listSelector
             * @type {String}
             */
            listSelector: '.last-seen-products--slider',

            /**
             * Selector for the product slider container
             *
             * @property containerSelector
             * @type {String}
             */
            containerSelector: '.last-seen-products--container',

            /**
             * Class that will be used for a single product slider items
             *
             * @property itemCls
             * @type {String}
             */
            itemCls: 'last-seen-products--item product-slider--item product--box box--slider',

            /**
             * Class that will be used for the product title
             *
             * @property titleCls
             * @type {String}
             */
            titleCls: 'last-seen-products-item--title product--title',

            /**
             * Class that will be used for the product image
             *
             * @property imageCls
             * @type {String}
             */
            imageCls: 'last-seen-products-item--image product--image',

            /**
             * Picture source when no product image is available
             *
             * @property noPicture
             * @type {String}
             */
            noPicture: ''
        },

        /**
         * Initializes all necessary elements and collects the current
         * article when we are on the detail page.
         *
         * @public
         * @method init
         */
        init: function () {
            var me = this;

            me.applyDataAttributes();

            me.$list = me.$el.find(me.opts.listSelector);
            me.$container = me.$list.find(me.opts.containerSelector);

            me.productSlider = me.$list.data('plugin_swProductSlider');

            if (!me.productSlider) {
                return;
            }

            me.storage = StorageManager.getLocalStorage();

            if ($('body').hasClass('is--ctl-detail')) {
                me.collectProduct(me.opts.currentArticle);
            }

            me.createProductList();
        },

        /**
         * Creates a list of all collected articles and calls
         * the product slider plugin.
         *
         * @public
         * @method createProductList
         */
        createProductList: function () {
            var me = this,
                opts = me.opts,
                itemKey = 'lastSeenProducts-' + opts.shopId + '-' + opts.baseUrl,
                productsJson = me.storage.getItem(itemKey),
                products = productsJson ? JSON.parse(productsJson) : [],
                len = Math.min(opts.productLimit, products.length),
                i = 0;

            if (len > 0) {
                me.$el.removeClass('is--hidden');
            }

            for (; i < len; i++) {
                me.$container.append(me.createTemplate(products[i]));
            }

            me.productSlider.initSlider();

            $.publish('plugin/swLastSeenProducts/onCreateProductList', [ me ]);
        },

        /**
         * Creates a product slider item template.
         *
         * @public
         * @method createTemplate
         * @param {Object} article
         */
        createTemplate: function (article) {
            var me = this,
                $template = $('<div>', {
                    'class': me.opts.itemCls,
                    'html': [
                        me.createProductImage(article),
                        me.createProductTitle(article)
                    ]
                });

            $.publish('plugin/swLastSeenProducts/onCreateTemplate', [ me, $template, article ]);

            return $template;
        },

        /**
         * Creates the product name title by the provided article data
         *
         * @public
         * @method createProductTitle
         * @param {Object} data
         */
        createProductTitle: function (data) {
            var me = this,
                $title = $('<a>', {
                    'rel': 'nofollow',
                    'class': me.opts.titleCls,
                    'title': data.articleName,
                    'href': data.linkDetailsRewritten,
                    'html': data.articleName
                });

            $.publish('plugin/swLastSeenProducts/onCreateProductTitle', [ me, $title, data ]);

            return $title;
        },

        /**
         * Creates a product image with all media queries for the
         * picturefill plugin
         *
         * @public
         * @method createProductImage
         * @param {Object} data
         */
        createProductImage: function (data) {
            var me = this,
                image = data.images[0],
                element,
                imageEl,
                imageMedia,
                srcSet;

            element = $('<a>', {
                'class': me.opts.imageCls,
                'href': data.linkDetailsRewritten,
                'title': data.articleName
            });

            imageEl = $('<span>', { 'class': 'image--element' }).appendTo(element);
            imageMedia = $('<span>', { 'class': 'image--media' }).appendTo(imageEl);

            if (image) {
                srcSet = image.sourceSet;
            } else {
                srcSet = me.opts.noPicture
            }

            $('<img>', {
                'srcset': srcSet,
                'alt': data.articleName,
                'title': data.articleName
            }).appendTo(imageMedia);

            $.publish('plugin/swLastSeenProducts/onCreateProductImage', [ me, element, data ]);

            return element;
        },

        /**
         * Adds a new article to the local storage for usage in the product slider.
         *
         * @public
         * @method collectProduct
         * @param {Object} newProduct
         */
        collectProduct: function (newProduct) {
            var me = this,
                opts = me.opts,
                itemKey = 'lastSeenProducts-' + opts.shopId + '-' + opts.baseUrl,
                productsJson = me.storage.getItem(itemKey),
                products = productsJson ? $.parseJSON(productsJson) : [],
                len = products.length,
                i = 0,
                url;

            if (!newProduct || $.isEmptyObject(newProduct)) {
                return;
            }

            for (; i < len; i++) {
                if (products[i].articleId === newProduct.articleId) {
                    newProduct = products.splice(i, 1)[0];
                    break;
                }
            }

            url = newProduct.linkDetailsRewritten;

            // Remove query string from article url
            if (url.indexOf('/sCategory') !== -1) {
                newProduct.linkDetailsRewritten = url.substring(0, url.indexOf('/sCategory'));
            } else if (url.indexOf('?') !== -1) {
                newProduct.linkDetailsRewritten = url.substring(0, url.indexOf('?'));
            }

            products.splice(0, 0, newProduct);

            while (products.length > opts.productLimit) {
                products.pop();
            }

            me.storage.setItem(itemKey, JSON.stringify(products));

            $.publish('plugin/swLastSeenProducts/onCollectProduct', [ me, newProduct ]);
        }
    });
}(jQuery));

;(function ($, window, Math) {
    'use strict';

    /**
     * Shopware Lightbox Plugin.
     *
     * This plugin is based on the modal plugin.
     * It opens images in a modal window and sets the width and height
     * of the modal box automatically to the image size. If the image
     * size is bigger than the window size, the modal will be set to
     * 90% of the window size so there is little margin between the modal
     * and the window edge. It calculates always the correct aspect.
     *
     * Usage:
     * $.lightbox.open('http://url.to.my.image.de');
     *
     */
    $.lightbox = {

        /**
         * Holds the object of the modal plugin.
         *
         * @type {Boolean | Object}
         */
        modal: false,

        /**
         * Opens the image from the given image url
         * in a lightbox window.
         *
         * @param imageURL
         */
        open: function(imageURL) {
            var me = this, size;

            me.image =  new Image();
            me.content = me.createContent(imageURL);

            me.image.onload = function() {
                size = me.getOptimizedSize(me.image.width, me.image.height);

                me.modal = $.modal.open(me.content, {
                    'width': size.width,
                    'height': size.height
                });

                $(window).on('resize.lightbox', function() {
                    me.setSize(me.image.width, me.image.height);
                });

                $.subscribe('plugin/swModal/onClose', function() {
                    $(window).off('resize.lightbox');
                });
            };

            me.image.src = imageURL;

            $.publish('plugin/swLightbox/onOpen', [ me ]);
        },

        /**
         * Creates the content for the lightbox.
         *
         * @param imageURL
         * @returns {*|HTMLElement}
         */
        createContent: function(imageURL) {
            var me = this,
                content = $('<div>', {
                'class': 'lightbox--container',
                'html':  $('<img>', {
                    'src': imageURL,
                    'class': 'lightbox--image'
                })
            });

            $.publish('plugin/swLightbox/onCreateContent', [ me, content, imageURL ]);

            return content;
        },

        /**
         * Set the size of the modal window.
         *
         * @param width
         * @param height
         */
        setSize: function(width, height) {
            var me = this,
                size = me.getOptimizedSize(width, height);

            if (!me.modal) {
                return;
            }

            me.modal.setWidth(size.width);
            me.modal.setHeight(size.height);

            $.publish('plugin/swLightbox/onSetSize', [ me, width, height ]);
        },

        /**
         * Computes the optimal size for the lightbox
         * based on the measurements of the shown image.
         *
         * @param width
         * @param height
         * @returns {{width: *, height: *}}
         */
        getOptimizedSize: function(width, height) {
            var me = this,
                aspect = width / height,
                maxWidth = Math.round(window.innerWidth * 0.9),
                maxHeight = Math.round(window.innerHeight * 0.9),
                size;

            if (width > maxWidth) {
                width = maxWidth;
                height = Math.round(width / aspect);
            }

            if (height > maxHeight) {
                height = maxHeight;
                width = Math.round(height * aspect);
            }

            size = {
                'width': width,
                'height': height
            };

            $.publish('plugin/swLightbox/onGetOptimizedSize', [ me, size ]);

            return size;
        }
    }
})(jQuery, window, Math);
;(function ($, Modernizr, location) {
    'use strict';

    /**
     * Ajax Product navigation
     *
     * The jQuery plugin provides the product navigation (= previous / next product and the overview button) using AJAX.
     * The plugin is necessary to fully support the HTTP cache.
     *
     * Please keep in mind that the plugin only works when the url contains the category parameter and the browser
     * needs to support {@link window.sessionStorage}.
     */
    $.plugin('swAjaxProductNavigation', {

        /**
         * Default configuration of the plugin
         *
         * @type {Object}
         */
        defaults: {

            /**
             * Animation speed in milliseconds of the arrow fading.
             *
             * @type {Number}
             */
            arrowFadeSpeed: 500,

            /**
             * Default offset of the arrows.
             *
             * @type {Number}
             */
            arrowOffset: 40,

            /**
             * Offset of the arrows in pixel when they get hovered over.
             *
             * @type {Number}
             */
            arrowSlideOffset: 140,

            /**
             * Class to enable the arrow sliding.
             *
             * @type {String}
             */
            arrowSlideClass: 'can--slide',

            /**
             * Selector for the product box in the listing.
             *
             * @type {String}
             */
            productBoxSelector: '.product--box',

            /**
             * Selector for the product details.
             * This element should have data attributes of the ordernumber and product navigation link.
             *
             * @type {String}
             */
            productDetailsSelector: '.product--details',

            /**
             * Selector for the previous button.
             *
             * @type {String}
             */
            prevLinkSelector: '.navigation--link.link--prev',

            /**
             * Selector for the next button.
             *
             * @type {String}
             */
            nextLinkSelector: '.navigation--link.link--next',

            /**
             * Selector for the breadcrumb back button.
             *
             * @type {String}
             */
            breadcrumbButtonSelector: '.content--breadcrumb .breadcrumb--button',

            /**
             * Selector for the image container.
             *
             * @type {String}
             */
            imageContainerSelector: '.image--container',

            /**
             * Selectors of product box childs in the listing.
             *
             * @type {Array}
             */
            listingSelectors: [
                '.listing .product--box .product--image',
                '.listing .product--box .product--title',
                '.listing .product--box .product--actions .action--more'
            ]
        },

        /**
         * Initializes the plugin and registers event listeners depending on
         * whether we are on the listing- or detail page.
         *
         * @public
         * @method init
         */
        init: function () {
            var me = this,
                $el = me.$el,
                opts = me.opts,
                isListing = $el.hasClass('is--ctl-listing'),
                isDetail = $el.hasClass('is--ctl-detail'),
                params = me.parseQueryString(location.href);

            if (!(isListing || isDetail)) {
                return;
            }

            me.storage = StorageManager.getStorage('session');

            if (isListing) {
                me.registerListingEventListeners();
                return;
            }

            me.$prevButton = $el.find(opts.prevLinkSelector);
            me.$nextButton = $el.find(opts.nextLinkSelector);
            me.$backButton = $el.find(opts.breadcrumbButtonSelector);
            me.$productDetails = $el.find(opts.productDetailsSelector);

            me.categoryId = ~~(me.$productDetails.attr('data-category-id') || params && params.c);
            me.orderNumber = me.$productDetails.attr('data-main-ordernumber');
            me.productState = me.getProductState();
            if (!me.categoryId) {
                return;
            }

            // Clear the product state if the order numbers are not identical
            if (!$.isEmptyObject(me.productState) && me.productState.ordernumber !== me.orderNumber) {
                me.clearProductState();
                me.productState = {};
            }

            me.registerDetailEventListeners();
            me.getProductNavigation();
        },

        /**
         * Parses the given {@link url} parameter and extracts all query parameters. If the parameter is numeric
         * it will automatically based to a {@link Number} instead of a {@link String}.
         *
         * @private
         * @method parseQueryString
         * @param {String} url - Usually {@link window.location.href}
         * @returns {Object} All extracted URL-parameters
         */
        parseQueryString: function (url) {
            var params = {},
                urlParts = (url + '').split('?'),
                queryParts,
                part,
                key,
                value,
                p;

            if (urlParts.length < 2) {
                return params;
            }

            queryParts = urlParts[1].split('&');

            for (p in queryParts) {
                if (!queryParts.hasOwnProperty(p)) {
                    continue;
                }

                part = queryParts[p].split('=');

                key = decodeURIComponent(part[0]);
                value = decodeURIComponent(part[1] || '');

                params[key] = $.isNumeric(value) ? parseFloat(value) : value;
            }

            $.publish('plugin/swAjaxProductNavigation/onParseQueryString', [ this, url, params ]);

            return params;
        },

        /**
         * Reads the last saved product state by the key 'lastProductState'.
         *
         * @private
         * @method getProductState
         * @returns {Object} The last saved product state or an empty object.
         */
        getProductState: function () {
            var me = this,
                state = JSON.parse(me.storage.getItem('lastProductState')) || {};

            $.publish('plugin/swAjaxProductNavigation/onSetProductState', [ me, state ]);

            return state;
        },

        /**
         * Writes the given parameters into the {@link window.sessionStorage}.
         * The key 'lastProductState' will be used.
         *
         * @private
         * @method setProductState
         * @param {Object} params
         */
        setProductState: function (params) {
            var me = this;

            me.storage.setItem('lastProductState', JSON.stringify(params));

            $.publish('plugin/swAjaxProductNavigation/onSetProductState', [ me, params ]);
        },

        /**
         * Removes the product state from the {@link window.sessionStorage}.
         *
         * @private
         * @method clearProductState
         */
        clearProductState: function () {
            var me = this;

            me.storage.removeItem('lastProductState');

            $.publish('plugin/swAjaxProductNavigation/onClearProductState', [ me ]);
        },

        /**
         * Registers the event listeners for the listing page.
         *
         * @private
         * @method registerListingEventListeners
         */
        registerListingEventListeners: function () {
            var me = this,
                selectors = me.opts.listingSelectors.join(', ');

            me.$el.on(me.getEventName('click'), selectors, $.proxy(me.onClickProductInListing, me));

            $.publish('plugin/swAjaxProductNavigation/onRegisterEventsListing', [ me ]);
        },

        /**
         * Event handler method which saves the current listing state like
         * selected sorting and active page into the {@link window.sessionStorage}
         *
         * @event click
         * @param {MouseEvent} event
         */
        onClickProductInListing: function (event) {
            var me = this,
                opts = me.opts,
                $target = $(event.target),
                $parent = $target.parents(opts.productBoxSelector),
                params = me.parseQueryString(location.href);

            me.setProductState($.extend({}, params, {
                'categoryId': ~~($parent.attr('data-category-id')),
                'ordernumber': $parent.attr('data-ordernumber')
            }));

            $.publish('plugin/swAjaxProductNavigation/onClickProductInListing', [ me, event ]);
        },

        /**
         * Registers the event listeners for the detail page.
         *
         * @private
         * @method registerDetailEventListeners
         */
        registerDetailEventListeners: function () {
            var me = this;

            StateManager.on('resize', me.checkPossibleSliding, me);

            me._on(me.$prevButton, 'click', $.proxy(me.onArrowClick, me));
            me._on(me.$nextButton, 'click', $.proxy(me.onArrowClick, me));

            $.publish('plugin/swAjaxProductNavigation/onRegisterEventsDetail', [ me ]);
        },

        /**
         * @private
         * @method onArrowClick
         */
        onArrowClick: function (event) {
            var me = this,
                $target = $(event.currentTarget);

            if (!$.isEmptyObject(me.productState)) {
                me.productState.ordernumber = $target.attr('data-ordernumber');
                me.setProductState(me.productState);
            }

            $.publish('plugin/swAjaxProductNavigation/onArrowClick', [ me, event ]);
        },

        /**
         * Checks if it's possible for the arrows to slide to full extend.
         * Also checks if there's an image to display
         * If so, add the arrow slide class to the arrows.
         *
         * @private
         * @method checkPossibleSliding
         */
        checkPossibleSliding: function () {
            var me = this,
                opts = me.opts,
                offset = opts.arrowOffset,
                slideOffset = opts.arrowSlideOffset,
                $prevBtn = me.$prevButton,
                $nextBtn = me.$nextButton,
                remainingSpacePrev,
                remainingSpaceNext,
                prevBtnImage,
                nextBtnImage;

            if (!$nextBtn.length || !$prevBtn.length) {
                return false;
            }

            remainingSpacePrev = $prevBtn.offset().left + offset;
            remainingSpaceNext = $(window).width() - $nextBtn.offset().left - $nextBtn.outerWidth() + offset;

            prevBtnImage = $prevBtn
                .find(opts.imageContainerSelector)
                .css('background-image');

            nextBtnImage = $nextBtn
                .find(opts.imageContainerSelector)
                .css('background-image');

            $prevBtn[(prevBtnImage !== 'none' && remainingSpacePrev >= slideOffset) ? 'addClass' : 'removeClass'](opts.arrowSlideClass);
            $nextBtn[(nextBtnImage !== 'none' && remainingSpaceNext >= slideOffset) ? 'addClass' : 'removeClass'](opts.arrowSlideClass);

            $.publish('plugin/swAjaxProductNavigation/onCheckPossibleSliding', [ me ]);
        },

        /**
         * Requests the product navigation information from the server side
         * using an AJAX request.
         *
         * The url will be fetched from the product details element by
         * the 'data-product-navigation' attribute.
         *
         * @private
         * @method getProductNavigation
         */
        getProductNavigation: function () {
            var me = this,
                url = me.$productDetails.attr('data-product-navigation'),
                params = $.extend({}, me.productState, {
                    'ordernumber': me.orderNumber,
                    'categoryId': me.categoryId
                });

            if ($.isEmptyObject(params) || !url || !url.length) {
                return;
            }

            $.ajax({
                'url': url,
                'data': params,
                'method': 'GET',
                'dataType': 'json',
                'success': $.proxy(me.onProductNavigationLoaded, me)
            });

            $.publish('plugin/swAjaxProductNavigation/onGetProductNavigation', [ me ]);
        },

        /**
         * Sets the requested product navigation information into the DOM and displays the
         * prev and next arrow.
         *
         * @private
         * @method onProductNavigationLoaded
         * @param {Object} response - Server response
         */
        onProductNavigationLoaded: function (response) {
            var me = this,
                opts = me.opts,
                $prevBtn = me.$prevButton,
                $nextBtn = me.$nextButton,
                listing = response.currentListing,
                prevProduct = response.previousProduct,
                nextProduct = response.nextProduct,
                animSpeed = opts.arrowFadeSpeed,
                animCss = {
                    opacity: 1
                };

            $.publish('plugin/swAjaxProductNavigation/onProductNavigationLoaded', [ me, response ]);

            if (listing && listing.href) {
                me.$backButton.attr('href', listing.href);
            }

            if (typeof prevProduct === 'object') {
                $prevBtn.attr('data-ordernumber', prevProduct.orderNumber);

                if (prevProduct.image) {
                    $prevBtn
                        .find(opts.imageContainerSelector)
                        .css('background-image', 'url(' + prevProduct.image.thumbnails[0].source + ')');
                }

                $prevBtn
                    .attr('href', prevProduct.href)
                    .attr('title', prevProduct.name)
                    .css('display', 'inline');

                if (Modernizr.csstransitions) {
                    $prevBtn.transition(animCss, animSpeed);
                } else {
                    $prevBtn.animate(animCss, animSpeed);
                }
            }

            if (typeof nextProduct === 'object') {
                $nextBtn.attr('data-ordernumber', nextProduct.orderNumber);

                if (nextProduct.image) {
                    $nextBtn
                        .find(opts.imageContainerSelector)
                        .css('background-image', 'url(' + nextProduct.image.thumbnails[0].source + ')');
                }

                $nextBtn
                    .attr('href', nextProduct.href)
                    .attr('title', nextProduct.name)
                    .css('display', 'inline');

                if (Modernizr.csstransitions) {
                    $nextBtn.transition(animCss, animSpeed);
                } else {
                    $nextBtn.animate(animCss, animSpeed);
                }
            }

            me.checkPossibleSliding();

            $.publish('plugin/swAjaxProductNavigation/onProductNavigationFinished', [ me, response ]);
        },

        /**
         * Destroys the plugin by removing all listeners.
         *
         * @public
         * @method destroy
         */
        destroy: function () {
            var me = this,
                selectors = me.opts.listingSelectors.join(', ');

            StateManager.off('resize', me.checkPossibleSliding, me);

            me.$el.off(me.getEventName('click'), selectors);

            me._destroy();
        }
    });
})(jQuery, Modernizr, location);

;(function ($) {
    "use strict";

    $.plugin('swNewsletter', {

        init: function () {
            var me = this;

            me.$checkMail = me.$el.find('.newsletter--checkmail');
            me.$addionalForm = me.$el.find('.newsletter--additional-form');

            me._on(me.$checkMail, 'change', $.proxy(me.refreshAction, me));

            $.publish('plugin/swNewsletter/onRegisterEvents', [ me ]);

            me.$checkMail.trigger('change');
        },

        refreshAction: function (event) {
            var me = this,
                $el = $(event.currentTarget),
                val = $el.val();

            if (val == -1) {
                me.$addionalForm.hide();
            } else {
                me.$addionalForm.show();
            }

            $.publish('plugin/swNewsletter/onRefreshAction', [ me ]);
        },

        destroy: function () {
            this._destroy();
        }
    });
}(jQuery));

;(function ($) {
    'use strict';

    /**
     * Shopware Menu Scroller Plugin
     *
     * @example
     *
     * HTML:
     *
     * <div class="container">
     *     <ul class="my--list">
     *         <li>
     *             <!-- Put any element you want in here -->
     *         </li>
     *
     *         <li>
     *             <!-- Put any element you want in here -->
     *         </li>
     *
     *         <!-- More li elements -->
     *     </ul>
     * </div>
     *
     * JS:
     *
     * $('.container').swMenuScroller();
     */
    $.plugin('swMenuScroller', {

        /**
         * Default options for the menu scroller plugin
         *
         * @public
         * @property defaults
         * @type {Object}
         */
        defaults: {

            /**
             * CSS selector for the starting active item.
             * On initialisation, the slider will jump to it so it's visible..
             *
             * @type {String}
             */
            activeItemSelector: '.is--active',

            /**
             * CSS selector for the element listing
             *
             * @type {String}
             */
            listSelector: '*[class$="--list"]',

            /**
             * CSS class which will be added to the wrapper / this.$el
             *
             * @type {String}
             */
            wrapperClass: 'js--menu-scroller',

            /**
             * CSS class which will be added to the listing
             *
             * @type {String}
             */
            listClass: 'js--menu-scroller--list',

            /**
             * CSS class which will be added to every list item
             *
             * @type {String}
             */
            itemClass: 'js--menu-scroller--item',

            /**
             * CSS class(es) which will be set for the left arrow
             *
             * @type {String}
             */
            leftArrowClass: 'js--menu-scroller--arrow left--arrow',

            /**
             * CSS class(es) which will be set for the right arrow
             *
             * @type {String}
             */
            rightArrowClass: 'js--menu-scroller--arrow right--arrow',

            /**
             * CSS Class for the arrow content to center the arrow text.
             *
             * @type {String}
             */
            arrowContentClass: 'arrow--content',

            /**
             * Content of the left arrow.
             * Default it's an arrow pointing left.
             *
             * @type {String}
             */
            leftArrowContent: '&#58897;',

            /**
             * Content of the right arrow.
             * Default it's an arrow pointing right.
             *
             * @type {String}
             */
            rightArrowContent: '&#58895;',

            /**
             * Amount of pixels the plugin should scroll per arrow click.
             *
             * There is also a additional option:
             *
             * 'auto': the visible width will be taken.
             *
             * @type {String|Number}
             */
            scrollStep: 'auto',

            /**
             * Time in milliseconds the slide animation needs.
             *
             * @type {Number}
             */
            animationSpeed: 400,

            /**
             * Offset of the scroll position when we jump to the active item.
             *
             * @type {Number}
             */
            arrowOffset: 25
        },

        /**
         * Default plugin initialisation function.
         * Sets all needed properties, creates the slider template
         * and registers all needed event listeners.
         *
         * @public
         * @method init
         */
        init: function () {
            var me = this,
                opts = me.opts,
                $activeChild;

            // Apply all given data attributes to the options
            me.applyDataAttributes();

            /**
             * Length in pixel the menu has to scroll when clicked on a button.
             *
             * @private
             * @property scrollStep
             * @type {Number}
             */
            me.scrollStep = (opts.scrollStep === 'auto') ? me.$el.width() / 2 : parseFloat(opts.scrollStep);

            /**
             * Length in pixel the menu has to scroll when clicked on a button.
             *
             * @private
             * @property $list
             * @type {jQuery}
             */
            me.$list = me.$el.find(opts.listSelector);

            /**
             * The offset based on the current scroll bar height of the list.
             *
             * @private
             * @property scrollBarOffset
             * @type {Number}
             */
            me.scrollBarOffset = 0;

            // Initializes the template by adding classes to the existing elements and creating the buttons
            me.initTemplate();

            // Register window resize and button events
            me.registerEvents();

            // Update the button visibility
            me.updateButtons();

            $activeChild = me.$list.children(opts.activeItemSelector);

            if ($activeChild.length) {
                me.jumpToElement($activeChild);
            }
        },

        /**
         * Creates all needed control items and adds plugin classes
         *
         * @public
         * @method initTemplate
         */
        initTemplate: function () {
            var me = this,
                opts = me.opts,
                $el = me.$el,
                $list = me.$list;

            $el.addClass(opts.wrapperClass);

            $list.addClass(opts.listClass);

            me.updateScrollBarOffset();

            // Add the item class to every list item
            $list.children().addClass(opts.itemClass);

            me.$leftArrow = $('<div>', {
                'html': $('<span>', {
                    'class': opts.arrowContentClass,
                    'html': opts.leftArrowContent
                }),
                'class': opts.leftArrowClass
            }).appendTo($el);

            me.$rightArrow = $('<div>', {
                'html': $('<span>', {
                    'class': opts.arrowContentClass,
                    'html': opts.rightArrowContent
                }),
                'class': opts.rightArrowClass
            }).appendTo($el);

            $.publish('plugin/swMenuScroller/onInitTemplate', [ me ]);
        },

        /**
         * Creates all needed control items and adds plugin classes
         *
         * @public
         * @method initTemplate
         */
        updateScrollBarOffset: function () {
            var me = this,
                $list = me.$list,
                offset;

            offset = me.scrollBarOffset = Math.min(Math.abs($list[0].scrollHeight - $list.height()) * -1, me.scrollBarOffset);

            $list.css({
                'bottom': offset,
                'margin-top': offset
            });

            $.publish('plugin/swMenuScroller/onUpdateScrollBarOffset', [ me, offset ]);
        },

        /**
         * Registers the listener for the window resize.
         * Also adds the click/tap listeners for the navigation buttons.
         *
         * @public
         * @method registerEvents
         */
        registerEvents: function () {
            var me = this;

            StateManager.on('resize', me.updateResize, me);

            me._on(me.$leftArrow, 'click touchstart', $.proxy(me.onLeftArrowClick, me));
            me._on(me.$rightArrow, 'click touchstart', $.proxy(me.onRightArrowClick, me));

            me._on(me.$list, 'scroll', $.proxy(me.updateButtons, me));

            $.publish('plugin/swMenuScroller/onRegisterEvents', [ me ]);
        },

        /**
         * Will be called when the window resizes.
         * Calculates the new width and scroll step.
         * Refreshes the button states.
         *
         * @public
         * @method updateResize
         */
        updateResize: function () {
            var me = this,
                opts = me.opts,
                viewPortWidth = me.$el.width();

            me.updateScrollBarOffset();

            if (opts.scrollStep === 'auto') {
                me.scrollStep = viewPortWidth / 2;
            }

            me.updateButtons();

            $.publish('plugin/swMenuScroller/onUpdateResize', [ me ]);
        },

        /**
         * Called when left arrow was clicked / touched.
         * Adds the negated offset step to the offset.
         *
         * @public
         * @method onLeftArrowClick
         * @param {jQuery.Event} event
         */
        onLeftArrowClick: function (event) {
            event.preventDefault();

            var me = this;

            me.addOffset(me.scrollStep * -1);

            $.publish('plugin/swMenuScroller/onLeftArrowClick', [ me ]);
        },

        /**
         * Called when right arrow was clicked / touched.
         * Adds the offset step to the offset.
         *
         * @public
         * @method onRightArrowClick
         * @param {jQuery.Event} event
         */
        onRightArrowClick: function (event) {
            event.preventDefault();

            var me = this;

            me.addOffset(me.scrollStep);

            $.publish('plugin/swMenuScroller/onRightArrowClick', [ me ]);
        },

        /**
         * Adds the given offset relatively to the current offset.
         *
         * @public
         * @method addOffset
         * @param {Number} offset
         */
        addOffset: function (offset) {
            this.setOffset(this.$list.scrollLeft() + offset, true);
        },

        /**
         * Sets the absolute scroll offset.
         * Min / Max the offset so the menu stays in bounds.
         *
         * @public
         * @method setOffset
         * @param {Number} offset
         * @param {Boolean} animate
         */
        setOffset: function (offset, animate) {
            var me = this,
                opts = me.opts,
                $list = me.$list,
                maxWidth = $list.prop('scrollWidth') - me.$el.width(),
                newPos = Math.max(0, Math.min(maxWidth, offset));

            if (animate !== false) {
                $list.stop(true).animate({
                    'scrollLeft': newPos
                }, opts.animationSpeed, $.proxy(me.updateButtons, me));

                $.publish('plugin/swMenuScroller/onSetOffset', [ me, offset, animate ]);
                return;
            }

            $list.scrollLeft(newPos);

            me.updateButtons();

            $.publish('plugin/swMenuScroller/onSetOffset', [ me, offset, animate ]);
        },

        /**
         * Updates the buttons status and toggles their visibility.
         *
         * @public
         * @method updateButtons
         */
        updateButtons: function () {
            var me = this,
                $list = me.$list,
                elWidth = me.$el.width(),
                listWidth = $list.prop('scrollWidth'),
                scrollLeft = $list.scrollLeft();

            me.$leftArrow.toggle(scrollLeft > 0);
            me.$rightArrow.toggle(listWidth > elWidth && scrollLeft < (listWidth - elWidth));

            $.publish('plugin/swMenuScroller/onUpdateButtons', [ me, me.$leftArrow, me.$rightArrow ]);
        },

        /**
         * Jumps to the given active element on plugin initialisation.
         *
         * @public
         * @method jumpToElement
         */
        jumpToElement: function ($el) {
            var me = this,
                $list = me.$list,
                elWidth = me.$el.width(),
                scrollLeft = $list.scrollLeft(),
                leftPos = $el.position().left,
                rightPos = leftPos + $el.outerWidth(true),
                newPos;

            if (leftPos > scrollLeft && rightPos > scrollLeft + elWidth) {
                newPos = rightPos - elWidth + me.opts.arrowOffset;
            } else {
                newPos = Math.min(leftPos - me.$leftArrow.width(), scrollLeft);
            }

            me.setOffset(newPos, false);

            $.publish('plugin/swMenuScroller/onJumpToElement', [ me, $el, newPos ]);
        },

        /**
         * Removed all listeners, classes and values from this plugin.
         *
         * @public
         * @method destroy
         */
        destroy: function () {
            var me = this,
                opts = me.opts;

            StateManager.off('resize', me.updateResize, me);

            me.$el.removeClass(opts.wrapperClass);
            me.$list.removeClass(opts.listClass);

            me.$list.css({
                'bottom': '',
                'margin-top': ''
            });

            // Remove the item class of every list item
            me.$list.children().removeClass(opts.itemClass);

            me.$leftArrow.remove();
            me.$rightArrow.remove();

            me._destroy();
        }
    });
}(jQuery));

;(function($) {
    'use strict';

    $.plugin('swShippingPayment', {

        defaults: {

            formSelector: '#shippingPaymentForm',

            radioSelector: 'input.auto_submit[type=radio]',

            submitSelector: 'input[type=submit]'
        },

        /**
         * Plugin constructor.
         */
        init: function () {
            var me = this;

            me.applyDataAttributes();
            me.registerEvents();
        },

        /**
         * Registers all necessary event listener.
         */
        registerEvents: function () {
            var me = this;

            me.$el.on('change', me.opts.radioSelector, $.proxy(me.onInputChanged, me));

            $.publish('plugin/swShippingPayment/onRegisterEvents', [ me ]);
        },

        /**
         * Called on change event of the radio fields.
         */
        onInputChanged: function () {
            var me = this,
                form = me.$el.find(me.opts.formSelector),
                url = form.attr('action'),
                data = form.serialize() + '&isXHR=1';

            $.publish('plugin/swShippingPayment/onInputChangedBefore', [ me ]);

            $.loadingIndicator.open();

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function(res) {
                    me.$el.empty().html(res);
                    me.$el.find('input[type="submit"][form], button[form]').swFormPolyfill();
                    me.$el.find('select:not([data-no-fancy-select="true"])').swSelectboxReplacement();
                    $.loadingIndicator.close();
                    window.picturefill();

                    $.publish('plugin/swShippingPayment/onInputChanged', [ me ]);
                }
            })
        },

        /**
         * Destroy method of the plugin.
         * Removes attached event listener.
         */
        destroy: function() {
            var me = this;

            me.$el.off('change', me.opts.radioSelector);

            me._destroy();
        }
    });
})(jQuery);

;(function($, window) {
    'use strict';

    /**
     * Shopware Add Article Plugin
     *
     * @example Button Element (can be pretty much every element)
     *
     * HTML:
     *
     * <button data-add-article="true" data-addArticleUrl="{url controller='checkout' action='addArticle' sAdd=$sArticle.ordernumber}">
     *     Jetzt bestellen
     * </button>
     *
     * @example Form
     *
     * HTML:
     *
     * <form data-add-article="true" data-eventName="submit">
     *     <input type="hidden" name="sAdd" value="SW10165"> // Contains the ordernumber of the article
     *     <input type="hidden" name="sQuantity" value"10"> // Optional (Default: 1). Contains the amount of articles to be added (Can also be an select box)
     *
     *     <button>In den Warenkorb</button>
     * </form>
     *
     *
     * You can either add an article by giving a specific url to the property "addArticleUrl" (First example)
     * or you can add hidden input fields to the element with name "sAdd" and "sQuantity" (Second example).
     *
     * JS:
     *
     * $('*[data-add-article="true"]').swAddArticle();
     *
     */
    $.plugin('swAddArticle', {

        defaults: {
            /**
             * Event name that the plugin listens to.
             *
             * @type {String}
             */
            'eventName': 'click',

            /**
             * The ajax url that the request should be send to.
             *
             * Default: myShop.com/(Controller:)checkout/(Action:)addArticle
             *
             * @type {String}
             */
            'addArticleUrl': window.controller['ajax_add_article'],

            /**
             * Default value that is used for the per-page amount when the current device is not mapped.
             * An extra option because the mapping table can be accidentally overwritten.
             *
             * @type {Number}
             */
            'sliderPerPageDefault': 3,

            /**
             * Whether or not the modal box should be shown.
             *
             * @type {Boolean}
             */
            'showModal': true,

            /**
             * Selector for the product slider in the add article modal box.
             *
             * @type {String}
             */
            'productSliderSelector': '.js--modal .product-slider'
        },

        /**
         * Default plugin initialisation function.
         * Registers an event listener on the change event.
         * When it's triggered, the parent form will be submitted.
         *
         * @public
         * @method init
         */
        init: function () {
            var me = this,
                opts = me.opts;

            // Applies HTML data attributes to the current options
            me.applyDataAttributes();

            opts.showModal = !!opts.showModal && opts.showModal !== 'false';

            // Will be automatically removed when destroy() is called.
            me._on(me.$el, opts.eventName, $.proxy(me.sendSerializedForm, me));

            // Close modal on continue shopping button
            $('body').delegate('*[data-modal-close="true"]', 'click.modal', $.proxy(me.closeModal, me));

            StateManager.addPlugin(opts.productSliderSelector, 'swProductSlider');
        },

        /**
         * Gets called when the element was triggered by the given event name.
         * Serializes the plugin element {@link $el} and sends it to the given url.
         * When the ajax request was successful, the {@link initModalSlider} will be called.
         *
         * @public
         * @event sendSerializedForm
         * @param {jQuery.Event} event
         */
        sendSerializedForm: function (event) {
            event.preventDefault();

            var me = this,
                opts = me.opts,
                $el = me.$el,
                ajaxData = $el.serialize();

            ajaxData += '&isXHR=1';

            if (opts.showModal) {
                $.overlay.open({
                    'closeOnClick': false
                });

                $.loadingIndicator.open({
                    'openOverlay': false
                });
            }

            $.publish('plugin/swAddArticle/onBeforeAddArticle', [ me, ajaxData ]);

            $.ajax({
                'data': ajaxData,
                'dataType': 'jsonp',
                'url': opts.addArticleUrl,
                'success': function (result) {
                    $.publish('plugin/swAddArticle/onAddArticle', [ me, result ]);

                    if (!opts.showModal) {
                        return;
                    }

                    $.loadingIndicator.close(function () {
                        $.modal.open(result, {
                            width: 750,
                            sizing: 'content',
                            onClose: me.onCloseModal.bind(me)
                        });

                        picturefill();

                        StateManager.updatePlugin(opts.productSliderSelector, 'swProductSlider');

                        $.publish('plugin/swAddArticle/onAddArticleOpenModal', [ me, result ]);
                    });
                }
            });
        },

        /**
         * Closes the modal by continue shopping link.
         *
         * @public
         * @event closeModal
         */
        closeModal: function (event) {
            event.preventDefault();

            $.modal.close();

            $.publish('plugin/swAddArticle/onCloseModal', [ this ]);
        },

        /**
         * Gets called when the modal box is closing.
         * Destroys the product slider when its available.
         *
         * @public
         * @event onCloseModal
         */
        onCloseModal: function () {
            var me = this;

            StateManager.destroyPlugin(me.opts.productSliderSelector, 'swProductSlider');

            $.publish('plugin/swAddArticle/onCloseModal', [ me ]);
        }
    });
})(jQuery, window);

;(function($, window, document) {
    'use strict';

    var $document = $(document);

    /**
     * Rounds the given value to the chosen base.
     *
     * Example: 5.46 with a base of 0.5 will round to 5.5
     *
     * @param value
     * @param base
     * @param method | round / floor / ceil
     * @returns {number}
     */
    function round(value, base, method) {
        var rounding = method || 'round',
            b = base || 1,
            factor = 1 / b;

        return Math[rounding](value * factor) / factor;
    }

    /**
     * Rounds an integer to the next 5er brake
     * based on the sum of digits.
     *
     * @param value
     * @param method
     * @returns {number}
     */
    function roundPretty(value, method) {
        var rounding = method || 'round',
            digits = countDigits(value),
            step = (digits > 1) ? 2 : 1,
            base = 5 * Math.pow(10, digits - step);

        return round(value, base, rounding);
    }

    /**
     * Get the sum of digits before the comma of a number.
     *
     * @param value
     * @returns {number}
     */
    function countDigits(value) {
        return ~~(Math.log(Math.floor(value)) / Math.LN10 + 1);
    }

    /**
     * Clamps a number between a min and a max value.
     *
     * @param value
     * @param min
     * @param max
     * @returns {number}
     */
    function clamp(value, min, max) {
        return Math.max(min, Math.min(max, value));
    }

    /**
     * Converts a value to an integer.
     *
     * @param value
     * @returns {Number}
     */
    function int(value) {
        return parseFloat(value);
    }

    $.plugin('swRangeSlider', {

        defaults: {
            /**
             * The css class for the range slider container element.
             */
            sliderContainerCls: 'range-slider--container',

            /**
             * The css class for the range bar element.
             */
            rangeBarCls: 'range-slider--range-bar',

            /**
             * The css class for the handle elements at the start and end of the range bar.
             */
            handleCls: 'range-slider--handle',

            /**
             * The css class for the handle element at the min position.
             */
            handleMinCls: 'is--min',

            /**
             * The css class for the handle element at the max position.
             */
            handleMaxCls: 'is--max',

            /**
             * The css class for active handle elements which get dragged.
             */
            activeDraggingCls: 'is--dragging',

            /**
             * The selector for the hidden input field which holds the min value.
             */
            minInputElSelector: '*[data-range-input="min"]',

            /**
             * The selector for the hidden input field which holds the max value.
             */
            maxInputElSelector: '*[data-range-input="max"]',

            /**
             * The selector for the label which displays the min value.
             */
            minLabelElSelector: '*[data-range-label="min"]',

            /**
             * The selector for the label which displays the max value.
             */
            maxLabelElSelector: '*[data-range-label="max"]',

            /**
             * An example string for the format of the value label.
             */
            labelFormat: '',

            /**
             * Turn pretty rounding for cleaner steps on and off.
             */
            roundPretty: false,

            /**
             * The min value which the slider should show on start.
             */
            startMin: 20,

            /**
             * The max value which the slider should show on start.
             */
            startMax: 80,

            /**
             * The minimal value you can slide to.
             */
            rangeMin: 0,

            /**
             * The maximum value you can slide to.
             */
            rangeMax: 100,

            /**
             * The number of steps the slider is divided in.
             */
            stepCount: 100,

            /**
             * Function for calculation
             */
            stepCurve: 'linear'
        },

        init: function() {
            var me = this;

            me.applyDataAttributes();

            me.$minInputEl = me.$el.find(me.opts.minInputElSelector);
            me.$maxInputEl = me.$el.find(me.opts.maxInputElSelector);

            me.$minLabel = me.$el.find(me.opts.minLabelElSelector);
            me.$maxLabel = me.$el.find(me.opts.maxLabelElSelector);

            me.dragState = false;
            me.dragType = 'min';

            me.createSliderTemplate();
            me.validateStepCurve();

            me.computeBaseValues();
            me.registerEvents();
        },

        validateStepCurve: function() {
            var me = this,
                validCurves = ['linear', 'log'];

            me.opts.stepCurve = me.opts.stepCurve.toString().toLowerCase();

            if (validCurves.indexOf(me.opts.stepCurve) < 0) {
                me.opts.stepCurve = 'linear';
            }
        },

        registerEvents: function() {
            var me = this;

            me._on(me.$minHandle, 'mousedown touchstart', $.proxy(me.onStartDrag, me, 'min', me.$minHandle));
            me._on(me.$maxHandle, 'mousedown touchstart', $.proxy(me.onStartDrag, me, 'max', me.$maxHandle));

            me._on($document, 'mouseup touchend', $.proxy(me.onEndDrag, me));
            me._on($document, 'mousemove touchmove', $.proxy(me.slide, me));

            $.publish('plugin/swRangeSlider/onRegisterEvents', [ me ]);
        },

        createSliderTemplate: function() {
            var me = this;

            me.$rangeBar = me.createRangeBar();
            me.$container = me.createRangeContainer();

            me.$minHandle = me.createHandle('min');
            me.$maxHandle = me.createHandle('max');

            me.$minHandle.appendTo(me.$rangeBar);
            me.$maxHandle.appendTo(me.$rangeBar);
            me.$rangeBar.appendTo(me.$container);
            me.$container.prependTo(me.$el);
        },

        createRangeContainer: function() {
            var me = this,
                $container = $('<div>', {
                    'class': me.opts.sliderContainerCls
                });

            $.publish('plugin/swRangeSlider/onCreateRangeContainer', [ me, $container ]);

            return $container;
        },

        createRangeBar: function() {
            var me = this,
                $bar = $('<div>', {
                    'class': me.opts.rangeBarCls
                });

            $.publish('plugin/swRangeSlider/onCreateRangeBar', [ me, $bar ]);

            return $bar;
        },

        createHandle: function(type) {
            var me = this,
                typeClass = (type == 'max') ? me.opts.handleMaxCls : me.opts.handleMinCls,
                $handle = $('<div>', {
                    'class': me.opts.handleCls + ' ' + typeClass
                });

            $.publish('plugin/swRangeSlider/onCreateHandle', [ me, $handle ]);

            return $handle;
        },

        computeBaseValues: function() {
            var me = this;

            me.minRange = int(me.opts.rangeMin);
            me.maxRange = int(me.opts.rangeMax);

            if (me.opts.roundPretty) {
                me.minRange = roundPretty(me.minRange, 'floor');
                me.maxRange = roundPretty(me.maxRange, 'ceil');
            }

            me.range = me.maxRange - me.minRange;
            me.stepSize = me.range / int(me.opts.stepCount);
            me.stepWidth = 100 / int(me.opts.stepCount);

            me.minValue = (me.opts.startMin === me.opts.rangeMin || me.opts.startMin <= me.minRange) ? me.minRange : int(me.opts.startMin);
            me.maxValue = (me.opts.startMax === me.opts.rangeMax || me.opts.startMax >= me.maxRange) ? me.maxRange : int(me.opts.startMax);

            if (me.maxValue == me.minValue || me.maxValue == 0) {
                me.maxValue = me.maxRange;
            }

            $.publish('plugin/swRangeSlider/onComputeBaseValues', [ me, me.minValue, me.maxValue ]);

            me.setRangeBarPosition(me.minValue, me.maxValue);
            me.updateLayout();
        },

        setRangeBarPosition: function(minValue, maxValue) {
            var me = this,
                min = minValue || me.minValue,
                max = maxValue || me.maxValue,
                left = me.getPositionByValue(min),
                right = me.getPositionByValue(max),
                width = right - left;

            me.$rangeBar.css({
                'left': left + '%',
                'width': width + '%'
            });

            $.publish('plugin/swRangeSlider/onSetRangeBarPosition', [ me, me.$rangeBar, minValue, maxValue ]);
        },

        setMin: function(min, updateInput) {
            var me = this,
                update = updateInput || false;

            min = (min === me.opts.rangeMin || min <= me.minRange) ? me.minRange : int(min);
            me.minValue = min;

            if (update) {
                me.updateMinInput(min);
            }

            me.setRangeBarPosition();
            me.updateLayout();

            $.publish('plugin/swRangeSlider/onSetMin', [ me, min, updateInput ]);
        },

        setMax: function(max, updateInput) {
            var me = this,
                update = updateInput || false;

            max = (max === me.opts.rangeMax || max >= me.maxRange) ? me.maxRange : int(max);
            me.maxValue = max;

            if (update) {
                me.updateMaxInput(max);
            }

            me.setRangeBarPosition();
            me.updateLayout();

            $.publish('plugin/swRangeSlider/onSetMax', [ me, max, updateInput ]);
        },

        reset: function(param) {
            var me = this;

            if (param == 'max') {
                me.maxValue = me.maxRange;
                me.$maxInputEl.attr('disabled', 'disabled')
                    .val(me.maxRange)
                    .trigger('change');
            } else {
                me.minValue = me.minRange;
                me.$minInputEl.attr('disabled', 'disabled')
                    .val(me.minRange)
                    .trigger('change');
            }

            me.setRangeBarPosition();
            me.updateLayout();

            $.publish('plugin/swRangeSlider/onReset', [ me, param ]);
        },

        onStartDrag: function(type, $handle) {
            var me = this;

            $handle.addClass(me.opts.activeDraggingCls);

            me.dragState = true;
            me.dragType = type;

            $.publish('plugin/swRangeSlider/onStartDrag', [ me, type, $handle ]);
        },

        onEndDrag: function() {
            var me = this;

            if (!me.dragState) {
                return;
            }
            me.dragState = false;

            me.updateLayout();

            me.$minHandle.removeClass(me.opts.activeDraggingCls);
            me.$maxHandle.removeClass(me.opts.activeDraggingCls);

            if (me.dragType == 'max') {
                me.updateMaxInput(me.maxValue);
            } else {
                me.updateMinInput(me.minValue);
            }

            $(me).trigger('rangeChange', me);

            $.publish('plugin/swRangeSlider/onEndDrag', [ me, me.dragType ]);
        },

        slide: function(event) {
            var me = this;

            if (!me.dragState) {
                return;
            }

            var pageX = (event.originalEvent.touches) ? event.originalEvent.touches[0].pageX : event.pageX,
                offset = me.$container.offset(),
                width = me.$container.innerWidth(),
                mouseX = pageX - offset.left,
                xPercent = clamp(round((100 / width * mouseX), me.stepWidth, 'round'), 0, 100),
                value = me.getValueByPosition(xPercent);

            event.preventDefault();

            if (me.dragType == 'max') {
                var minValue = me.getValueByPosition(me.getPositionByValue(me.minValue) + me.stepWidth);
                me.setMax(clamp(value, minValue, me.maxRange));
            } else {
                var maxValue = me.getValueByPosition(me.getPositionByValue(me.maxValue) - me.stepWidth);
                me.setMin(clamp(value, me.minRange, maxValue));
            }

            $.publish('plugin/swRangeSlider/onSlide', [ me, event, xPercent, value ]);
        },

        updateMinInput: function(value) {
            var me = this;

            if (me.$minInputEl.length) {
                me.$minInputEl.val(value.toFixed(2))
                    .removeAttr('disabled')
                    .trigger('change');

                $.publish('plugin/swRangeSlider/onUpdateMinInput', [ me, me.$minInputEl, value ]);
            }
        },

        updateMaxInput: function(value) {
            var me = this;

            if (me.$maxInputEl.length) {
                me.$maxInputEl.val(value.toFixed(2))
                    .removeAttr('disabled')
                    .trigger('change');

                $.publish('plugin/swRangeSlider/onUpdateMaxInput', [ me, me.$maxInputEl, value ]);
            }
        },

        updateMinLabel: function(value) {
            var me = this;

            if (me.$minLabel.length) {
                me.$minLabel.html(me.formatValue(value));

                $.publish('plugin/swRangeSlider/onUpdateMinLabel', [ me, me.$minLabel, value ]);
            }
        },

        updateMaxLabel: function(value) {
            var me = this;

            if (me.$maxLabel.length) {
                me.$maxLabel.html(me.formatValue(value));

                $.publish('plugin/swRangeSlider/onUpdateMaxLabel', [ me, me.$maxLabel, value ]);
            }
        },

        updateLayout: function(minValue, maxValue) {
            var me = this,
                min = minValue || me.minValue,
                max = maxValue || me.maxValue;

            me.updateMinLabel(min);
            me.updateMaxLabel(max);

            $.publish('plugin/swRangeSlider/onUpdateLayout', [ me, minValue, maxValue ]);
        },

        roundValue: function(value) {
            var me = this;

            if (value < 10) {
                value = me.roundTo(value, 0.10);
            } else if (value < 100) {
                value = me.roundTo(value, 1);
            } else {
                value = me.roundTo(value, 5);
            }

            return value;
        },

        formatValue: function(value) {
            var me = this;

            $.publish('plugin/swRangeSlider/onFormatValueBefore', [ me, value ]);

            if (value != me.minRange && value != me.maxRange) {
                value = me.roundValue(value);
            }

            if (!me.opts.labelFormat.length) {
                return value.toFixed(2);
            }

            value = Math.round(value * 100) / 100;
            value = value.toFixed(2);

            if (me.opts.labelFormat.indexOf('0.00') >= 0) {
                value = me.opts.labelFormat.replace('0.00', value);
            } else {
                value = value.replace('.', ',');
                value = me.opts.labelFormat.replace('0,00', value);
            }

            $.publish('plugin/swRangeSlider/onFormatValue', [ me, value ]);

            return value;
        },

        roundTo: function(value, num) {
            var resto = value%num;

            if (resto <= (num/2)) {
                return value - resto;
            } else {
                return value + num - resto;
            }
        },

        getPositionByValue: function(value) {
            var me = this;

            if(me.opts.stepCurve == 'log') {
                return me._getPositionLog(value);
            }

            return me._getPositionLinear(value);
        },

        _getPositionLog: function(value) {
            var me = this,
                minp = 0,
                maxp = me.opts.stepCount,
                minv = Math.log(me.opts.rangeMin),
                maxv = Math.log(me.opts.rangeMax),
                scale = (maxv-minv) / (maxp-minp),
                pos = minp + (Math.log(value) - minv) / scale;

            pos = Math.round(pos * me.stepWidth);

            return pos > 0 && pos || 0;
        },

        _getPositionLinear: function(value) {
            var me = this;

            return 100 / me.range * (value - me.minRange);
        },

        getValueByPosition: function(position) {
            var me = this;

            if(me.opts.stepCurve == 'log') {
                return me._getValueLog(position);
            }

            return me._getValueLinear(position);
        },

        _getValueLinear: function(position) {
            var me = this;

            return (me.range / 100 * position) + me.minRange;
        },

        _getValueLog: function(position) {
            var me = this;

            if (position === 0) {
                return me.minRange;
            } else if (position === 100) {
                return me.maxRange;
            }

            var minp = 0,
                maxp = me.opts.stepCount,
                minv = Math.log(me.opts.rangeMin),
                maxv = Math.log(me.opts.rangeMax),
                scale = (maxv-minv) / (maxp-minp);

            position = position / me.stepWidth;

            return Math.exp(minv + scale*(position-minp));
        },

        getStepWidth: function(value) {
            var me = this;

            if (me.opts.stepCurve == 'log') {
                return value;
            }

            return me.stepWidth;
        },

        destroy: function() {
            var me = this;

            me._destroy();
        }
    });
})(jQuery, window, document);

;(function($, window, document, undefined) {
    'use strict';

    /**
     * An object holding the configuration objects
     * of special component types. The specific
     * configuration objects are getting merged
     * into the original plugin for the corresponding
     * component type. This is used for special components
     * to override some of the base methods to make them
     * work properly and for firing correct change events.
     *
     * @type {}
     */
    var specialComponents = {

        /**
         * Range-Slider component
         */
        'range': {

            compOpts: {
                rangeSliderSelector: '*[data-range-slider="true"]'
            },

            initComponent: function() {
                var me = this;

                me.$rangeSliderEl = me.$el.find(me.opts.rangeSliderSelector);
                me.$rangeInputs = me.$rangeSliderEl.find('input');
                me.rangeSlider = me.$rangeSliderEl.data('plugin_swRangeSlider');

                me.registerComponentEvents();
            },

            registerComponentEvents: function() {
                var me = this;

                me._on(me.$rangeInputs, 'change', $.proxy(me.onChange, me));
            }
        },

        /**
         * Rating component
         */
        'rating': {

            compOpts: {
                starInputSelector: '.rating-star--input'
            },

            initComponent: function() {
                var me = this;

                me.$starInputs = me.$el.find(me.opts.starInputSelector);

                me.registerComponentEvents();
            },

            registerComponentEvents: function() {
                var me = this;

                me._on(me.$starInputs, 'change', function(event) {
                    var $el = $(event.currentTarget);

                    if ($el.is(':checked')) {
                        me.onChange(event);
                    }
                });
            }
        },

        /**
         * Radio component
         */
        'radio': {

            compOpts: {
                radioInputSelector: 'input[type="radio"]'
            },

            initComponent: function() {
                var me = this;

                me.$radioInputs = me.$el.find(me.opts.radioInputSelector);

                me.registerComponentEvents();
            },

            registerComponentEvents: function() {
                var me = this;

                me._on(me.$radioInputs, 'change', function(event) {
                    me.onChange(event);
                });
            }
        }
    };

    /**
     * The actual plugin.
     */
    $.plugin('swFilterComponent', {

        defaults: {
            /**
             * The type of the filter component
             *
             * @String value|range|image|pattern|radio|rating
             */
            type: 'value',

            /**
             * The css class for collapsing the filter component flyout.
             */
            collapseCls: 'is--collapsed',

            /**
             * The css selector for the title element of the filter flyout.
             */
            titleSelector: '.filter-panel--title',

            /**
             * The css selector for checkbox elements in the components.
             */
            checkBoxSelector: 'input[type="checkbox"]'
        },

        /**
         * Initializes the plugin.
         */
        init: function() {
            var me = this;

            me.applyDataAttributes();

            me.type = me.$el.attr('data-filter-type') || me.opts.type;

            me.$title = me.$el.find(me.opts.titleSelector);
            me.$siblings = me.$el.siblings('*[data-filter-type]');

            /**
             * Checks if the type of the component uses
             * any special configuration or methods.
             */
            if (specialComponents[me.type] !== undefined) {
                /**
                 * Extends the plugin object with the
                 * corresponding component object.
                 */
                $.extend(me, specialComponents[me.type]);

                /**
                 * Merges the component options into
                 * the plugin options.
                 */
                $.extend(me.opts, me.compOpts);
            }

            me.initComponent();
            me.registerEvents();
        },

        /**
         * Initializes the component based on the type.
         * This method may be overwritten by special components.
         */
        initComponent: function() {
            var me = this;

            me.$inputs = me.$el.find(me.opts.checkBoxSelector);

            me.registerComponentEvents();

            $.publish('plugin/swFilterComponent/onInitComponent', [ me ]);
        },

        /**
         * Registers all necessary global event listeners.
         */
        registerEvents: function() {
            var me = this;

            if (me.type != 'value') {
                me._on(me.$title, 'click', $.proxy(me.toggleCollapse, me, true));
            }

            $.publish('plugin/swFilterComponent/onRegisterEvents', [ me ]);
        },

        /**
         * Registers all necessary events for the component.
         * This method may be overwritten by special components.
         */
        registerComponentEvents: function() {
            var me = this;

            me._on(me.$inputs, 'change', $.proxy(me.onChange, me));

            $.publish('plugin/swFilterComponent/onRegisterComponentEvents', [ me ]);
        },

        /**
         * Called on the change events of each component.
         * Triggers a custom change event on the component,
         * so that other plugins can listen to changes in
         * the different components.
         *
         * @param event
         */
        onChange: function(event) {
            var me = this,
                $el = $(event.currentTarget);

            me.$el.trigger('onChange', [me, $el]);

            $.publish('plugin/swFilterComponent/onChange', [ me, event ]);
        },

        /**
         * Returns the type of the component.
         *
         * @returns {type|*}
         */
        getType: function() {
            return this.type;
        },

        /**
         * Opens the component flyout panel.
         *
         * @param closeSiblings
         */
        open: function(closeSiblings) {
            var me = this;

            if (closeSiblings) {
                me.$siblings.removeClass(me.opts.collapseCls);
            }

            me.$el.addClass(me.opts.collapseCls);

            $.publish('plugin/swFilterComponent/onOpen', [ me ]);
        },

        /**
         * Closes the component flyout panel.
         */
        close: function()  {
            var me = this;

            me.$el.removeClass(me.opts.collapseCls);

            $.publish('plugin/swFilterComponent/onClose', [ me ]);
        },

        /**
         * Toggles the viewed state of the component.
         */
        toggleCollapse: function() {
            var me = this,
                shouldOpen = !me.$el.hasClass(me.opts.collapseCls);

            if (shouldOpen) {
                me.open(true);
            } else {
                me.close();
            }

            $.publish('plugin/swFilterComponent/onToggleCollapse', [ me, shouldOpen ]);
        },

        /**
         * Destroys the plugin.
         */
        destroy: function() {
            var me = this;

            me._destroy();
        }
    });
})(jQuery, window, document, undefined);

;(function($, window, StateManager, undefined) {
    'use strict';

    var $body = $('body');

    /**
     * Plugin for handling the filter functionality and
     * all other actions for changing the product listing.
     * It handles the current set of category parameters and applies
     * them to the current top location url when something was
     * changed by the user over the filter form, action forms or
     * the action links.
     *
     * ** Filter Form **
     * The filter form exists of different filter components,
     * the filter submit button and the labels for active filters.
     * Each component is rendered in a single panel and has its own functionality.
     * All single components are handled by the "filterComponent" plugin.
     * The plugin for the components fires correct change events for each type
     * of component, so the "listingActions" plugin can listen on the changes
     * of the user. A filter form has to be a normal form with the selector,
     * which is set in the plugin options, so the form can be found by the plugin.
     * The actual submitting of the form will always be prevented to build the complex
     * category parameters out of the serialized form data.
     *
     * Example:
     * <form id="filter" method="get" data-filter-form="true">
     *
     *
     * ** Action Forms **
     * You can apply different category parameters over additional action forms.
     * In most cases these forms are auto submitting forms using the "autoSubmit" plugin,
     * which change just one parameter via a combo- or checkbox. So with these
     * action forms you have the possibility to apply all kind of category parameters
     * like sorting, layout type, number of products per page etc.
     *
     * Example:
     * <form method="get" data-action-form="true">
     *  <select name="{$shortParameters.sSort}" data-auto-submit="true">
     *      {...}
     *  </select>
     * </form>
     *
     *
     * ** Action Links **
     * You can also apply different category parameter via direct links.
     * Just use the corresponding get parameters in the href attribute of the link.
     * The new parameter will be added to the existing category parameters.
     * If the parameter already exists the value will be updated with the new one.
     *
     * Example:
     * <a href="?p=1&l=list" data-action-link="true">list view</a>
     *
     */
    $.plugin('swListingActions', {

        defaults: {

            /**
             * The selector for the filter panel form.
             */
            filterFormSelector: '*[data-filter-form="true"]',

            /**
             * The selector for the single filter components.
             */
            filterComponentSelector: '*[data-filter-type]',

            /**
             * The selector for the button which shows and hides the filter panel.
             */
            filterTriggerSelector: '*[data-filter-trigger="true"]',

            /**
             * The selector for the icon inside the filter trigger button.
             */
            filterTriggerIconSelector: '.action--collapse-icon',

            /**
             * The selector for the filter panel element.
             */
            filterContainerSelector: '.action--filter-options',

            /**
             * The selector for additional listing action forms.
             */
            actionFormSelector: '*[data-action-form="true"]',

            /**
             * The selector for additional listing action links.
             */
            actionLinkSelector: '*[data-action-link="true"]',

            /**
             * The selector for the container where the active filters are shown.
             */
            activeFilterContSelector: '.filter--active-container',

            /**
             * The selector for the button which applies the filter changes.
             */
            applyFilterBtnSelector: '.filter--btn-apply',

            /**
             * The css class for active filter labels.
             */
            activeFilterCls: 'filter--active',

            /**
             * The close icon element which is used for the active filter labels.
             */
            activeFilterIconCls: 'filter--active-icon',

            /**
             * The css class for the filter panel when it is completely collapsed.
             */
            collapsedCls: 'is--collapsed',

            /**
             * The css class for the filter container when it shows only the preview of the active filters.
             */
            hasActiveFilterCls: 'is--active-filter',

            /**
             * The css class for active states.
             */
            activeCls: 'is--active',

            /**
             * The css class for disabled states.
             */
            disabledCls: 'is--disabled',

            /**
             * Selector for the element that contains the found product count.
             */
            filterCountSelector: '.filter--count',

            /**
             * Class that will be added to the apply filter button
             * when loading the results.
             */
            loadingClass: 'is--loading',

            /**
             * The characters used as a prefix to identify property field names.
             * The properties will be merged in one GET parameter.
             * For example properties with field names beginning with __f__"ID"
             * will be merged to &f=ID1|ID2|ID3|ID4 etc.
             *
             */
            propertyPrefixChar: '__',

            /**
             * The buffer time in ms to wait between each action before firing the ajax call.
             */
            bufferTime: 850,

            /**
             * The time in ms for animations.
             */
            animationSpeed: 400
        },

        /**
         * Initializes the plugin.
         */
        init: function() {
            var me = this,
                filterCount;

            me.applyDataAttributes();

            me.$filterForm = $(me.opts.filterFormSelector);
            me.$filterComponents = me.$el.find(me.opts.filterComponentSelector);
            me.$filterTrigger = me.$el.find(me.opts.filterTriggerSelector);
            me.$filterTriggerIcon = me.$filterTrigger.find(me.opts.filterTriggerIconSelector);
            me.$filterCont = me.$el.find(me.opts.filterContainerSelector);
            me.$actionForms = $(me.opts.actionFormSelector);
            me.$actionLinks = $(me.opts.actionLinkSelector);
            me.$activeFilterCont = me.$el.find(me.opts.activeFilterContSelector);
            me.$applyFilterBtn = me.$el.find(me.opts.applyFilterBtnSelector);

            me.resultCountURL = me.$filterForm.attr('data-count-ctrl');
            me.controllerURL = window.location.href.split('?')[0];
            me.resetLabel = me.$activeFilterCont.attr('data-reset-label');
            me.propertyFieldNames = [];
            me.activeFilterElements = {};
            me.categoryParams = {};
            me.urlParams = '';
            me.bufferTimeout = 0;

            me.getPropertyFieldNames();
            me.setCategoryParamsFromTopLocation();
            me.createActiveFiltersFromCategoryParams();
            me.createUrlParams();

            filterCount = Object.keys(me.activeFilterElements).length;

            me.updateFilterTriggerButton(filterCount > 1 ? filterCount - 1 : filterCount);

            me.initStateHandling();
            me.registerEvents();
        },

        /**
         * Initializes the state manager for specific device options.
         */
        initStateHandling: function() {
            var me = this,
                enterFn = $.proxy(me.onEnterMobile, me),
                exitFn = $.proxy(me.onExitMobile, me);

            StateManager.registerListener([
                {
                    state: 'xs',
                    enter: enterFn,
                    exit: exitFn
                },
                {
                    state: 's',
                    enter: enterFn,
                    exit: exitFn
                }
            ]);

            $.publish('plugin/swListingActions/onInitStateHandling', [ me ]);
        },

        /**
         * Called when entering the xs or s viewport.
         * Removes/Clears style attributes that were set in higher viewports.
         */
        onEnterMobile: function () {
            var me = this,
                opts = me.opts;

            me.$filterForm.removeAttr('style');

            me.$activeFilterCont.removeAttr('style').removeClass(opts.disabledCls);

            me.$filterCont.removeClass(opts.collapsedCls);

            me.$filterTrigger.removeClass(opts.activeCls);

            $.publish('plugin/swListingActions/onEnterMobile', [ me ]);
        },

        /**
         * Called when exiting the xs or s viewport.
         * Add the disabled class to the active filter container
         * when it has active filter elements.
         */
        onExitMobile: function () {
            var me = this;

            if (StateManager.isCurrentState(['xs', 's'])) {
                return;
            }

            if (Object.keys(me.activeFilterElements).length) {
                me.$activeFilterCont.addClass(me.opts.disabledCls);
            }

            $.publish('plugin/swListingActions/onExitMobile', [ me ]);
        },

        /**
         * Registers all necessary events.
         */
        registerEvents: function() {
            var me = this;

            me._on(me.$filterForm, 'submit',  $.proxy(me.onFilterSubmit, me));
            me._on(me.$actionForms, 'submit',  $.proxy(me.onActionSubmit, me));
            me._on(me.$actionLinks, 'click', $.proxy(me.onActionLink, me));
            me._on(me.$filterComponents, 'onChange', $.proxy(me.onComponentChange, me));
            me._on(me.$filterTrigger, 'click', $.proxy(me.onFilterTriggerClick, me));

            me._on($body, 'click', $.proxy(me.onBodyClick, me));

            me.$el.on(me.getEventName('click'), '.' + me.opts.activeFilterCls, $.proxy(me.onActiveFilterClick, me));

            $.publish('plugin/swListingActions/onRegisterEvents', [ me ]);
        },

        /**
         * Called by event listener on submitting the filter form.
         * Gets the serialized form data and applies it to the category params.
         *
         * @param event
         */
        onFilterSubmit: function(event) {
            event.preventDefault();

            var me = this,
                formData = me.$filterForm.serializeArray(),
                categoryParams = me.setCategoryParamsFromData(formData);

            me.applyCategoryParams(categoryParams);

            $.publish('plugin/swListingActions/onFilterSubmit', [ me, event ]);
        },

        /**
         * Called by event listener on submitting an action form.
         * Gets the serialized form data and applies it to the category params.
         *
         * @param event
         */
        onActionSubmit: function(event) {
            event.preventDefault();

            var me = this,
                $form = $(event.currentTarget),
                formData = $form.serializeArray(),
                categoryParams = me.setCategoryParamsFromData(formData, true);

            me.applyCategoryParams(categoryParams);

            $.publish('plugin/swListingActions/onActionSubmit', [ me, event ]);
        },

        /**
         * Called by event listener on clicking on an action link.
         * Reads the parameter in the href attribute and adds it to the
         * category params.
         *
         * @param event
         */
        onActionLink: function(event) {
            event.preventDefault();

            var me = this,
                $link = $(event.currentTarget),
                linkParams = $link.attr('href').split('?')[1];

            me.applyCategoryParams(
                me.setCategoryParamsFromUrlParams(linkParams)
            );

            $.publish('plugin/swListingActions/onActionLink', [ me, event ]);
        },

        /**
         * Called by event listener on clicking the filter trigger button.
         * Opens and closes the filter form panel.
         *
         * @param event
         */
        onFilterTriggerClick: function(event) {
            event.preventDefault();

            if (StateManager.isCurrentState(['xs', 's'])) {
                return;
            }

            var me = this;

            if (me.$filterCont.hasClass(me.opts.collapsedCls)) {
                me.closeFilterPanel();
            } else {
                me.openFilterPanel();
            }

            $.publish('plugin/swListingActions/onFilterTriggerClick', [ me, event ]);
        },

        /**
         * Closes all filter panels if the user clicks anywhere else.
         *
         * @param event
         */
        onBodyClick: function(event) {
            var me = this,
                $target = $(event.target);

            if (!$target.is(me.opts.filterComponentSelector + ', ' + me.opts.filterComponentSelector + ' *')) {
                $.each(me.$filterComponents, function(index, item) {
                    $(item).data('plugin_swFilterComponent').close();
                });
            }

            $.publish('plugin/swListingActions/onBodyClick', [ me, event ]);
        },

        /**
         * Called by event listener on the change event of the
         * single filter components. Applies the changes of the
         * component values to the category params.
         *
         * @param event
         */
        onComponentChange: function(event) {
            var me = this,
                formData = me.$filterForm.serializeArray(),
                categoryParams = me.setCategoryParamsFromData(formData),
                urlParams = me.createUrlParams(categoryParams);

            me.createActiveFiltersFromCategoryParams(categoryParams);

            me.$applyFilterBtn.addClass(me.opts.loadingClass);

            me.buffer($.proxy(me.getFilterResult, me, urlParams), me.opts.bufferTime);

            $.publish('plugin/swListingActions/onComponentChange', [ me, event ]);
        },

        /**
         * Called by event listener on clicking an active filter label.
         * It removes the clicked filter param form the set of active filters
         * and updates the specific component.
         *
         * @param event
         */
        onActiveFilterClick: function(event) {
            var me = this,
                $activeFilter = $(event.currentTarget),
                param = $activeFilter.attr('data-filter-param'),
                isMobile = StateManager.isCurrentState(['xs', 's']);

            if (param == 'reset') {
                $.each(me.activeFilterElements, function(key) {
                    me.removeActiveFilter(key);
                    me.resetFilterProperty(key);
                });

                if (!isMobile && !me.$filterCont.hasClass(me.opts.collapsedCls)) {
                    me.applyCategoryParams();
                }

            } else if (isMobile || !me.$activeFilterCont.hasClass(me.opts.disabledCls)) {
                me.removeActiveFilter(param);
                me.resetFilterProperty(param);
            }

            $.publish('plugin/swListingActions/onActiveFilterClick', [ me, event ]);
        },

        getPropertyFieldNames: function() {
            var me = this;

            $.each(me.$filterComponents, function(index, item) {
                var $comp = $(item),
                    type = $comp.attr('data-filter-type'),
                    fieldName = $comp.attr('data-field-name');

                if ((type == 'value-list' || type == 'value-tree' || type == 'media') &&
                    me.propertyFieldNames.indexOf(fieldName) == -1) {
                    me.propertyFieldNames.push(fieldName);
                }
            });

            $.publish('plugin/swListingActions/onGetPropertyFieldNames', [ me, me.propertyFieldNames ]);

            return me.propertyFieldNames;
        },

        /**
         * Converts given form data to the category parameter object.
         * You can choose to either extend or override the existing object.
         *
         * @param formData
         * @param extend
         * @returns {*}
         */
        setCategoryParamsFromData: function(formData, extend) {
            var me = this,
                tempParams = {};

            $.each(formData, function(index, item) {
                if (item['value']) tempParams[item['name']] = item['value'];
            });

            if (extend) {
                return $.extend(me.categoryParams, tempParams);
            }

            me.categoryParams = tempParams;

            $.publish('plugin/swListingActions/onSetCategoryParamsFromData', [ me, tempParams ]);

            return tempParams;
        },

        /**
         * Converts top location parameters to the category parameter object.
         *
         * @returns {*}
         */
        setCategoryParamsFromTopLocation: function () {
            var me = this,
                urlParams = window.location.search.substr(1),
                categoryParams = me.setCategoryParamsFromUrlParams(urlParams);

            $.publish('plugin/swListingActions/onSetCategoryParamsFromData', [ me, categoryParams ]);

            return categoryParams;
        },

        /**
         * Converts url parameters to the category parameter object.
         *
         * @param urlParamString
         * @returns {{}|*}
         */
        setCategoryParamsFromUrlParams: function (urlParamString) {
            var me = this,
                categoryParams,
                params;

            if (urlParamString.length <= 0) {
                categoryParams = {};

                $.publish('plugin/swListingActions/onSetCategoryParamsFromUrlParams', [ me, categoryParams ]);

                return categoryParams;
            }

            categoryParams = me.categoryParams;
            params = urlParamString.split('&');

            $.each(params, function (index, item) {
                var param = item.split('=');

                param = $.map(param, function (val) {
                    val = val.replace(/\+/g, '%20');
                    return decodeURIComponent(val);
                });

                if (param[1] == 'reset') {
                    delete categoryParams[param[0]];

                } else if (me.propertyFieldNames.indexOf(param[0]) != -1) {
                    var properties = param[1].split('|');

                    $.each(properties, function (index, property) {
                        categoryParams[me.opts.propertyPrefixChar + param[0] + me.opts.propertyPrefixChar + property] = property;
                    });

                } else {
                    categoryParams[param[0]] = param[1];
                }
            });

            $.publish('plugin/swListingActions/onSetCategoryParamsFromUrlParams', [ me, categoryParams ]);

            return categoryParams;
        },

        /**
         * Converts the category parameter object to url parameters
         * and applies the url parameters to the current top location.
         *
         * @param categoryParams
         */
        applyCategoryParams: function(categoryParams) {
            var me = this,
                params = categoryParams || me.categoryParams,
                urlParams = me.createUrlParams(params);

            me.applyUrlParams(urlParams);

            $.publish('plugin/swListingActions/onApplyCategoryParams', [ me, categoryParams ]);
        },

        /**
         * Converts the category parameter object to url parameters.
         *
         * @param categoryParams
         * @returns {string}
         */
        createUrlParams: function(categoryParams) {
            var me = this,
                categoryParams = categoryParams || me.categoryParams,
                params = me.cleanParams(categoryParams),
                filterList = [];

            $.each(params, function(key, value) {
                filterList.push(encodeURIComponent(key) + '=' + encodeURIComponent(value));
            });

            me.urlParams = '?' + filterList.join('&');

            $.publish('plugin/swListingActions/onCreateUrlParams', [me, me.urlParams]);

            return me.urlParams;
        },

        cleanParams: function(params) {
            var me = this,
                propertyParams = {};

            $.each(params, function(key, value) {
                if (key.substr(0, 2) == me.opts.propertyPrefixChar) {
                    var propertyKey = key.split(me.opts.propertyPrefixChar)[1];

                    if (propertyParams[propertyKey] !== undefined) {
                        propertyParams[propertyKey] += '|' + value;
                    } else {
                        propertyParams[propertyKey] = value;
                    }
                } else {
                    propertyParams[key] = value;
                }
            });

            return propertyParams;
        },

        /**
         * Applies given url params to the top location.
         *
         * @param urlParams | String
         */
        applyUrlParams: function(urlParams) {
            var me = this,
                params = urlParams || me.urlParams;

            window.location.href = me.getListingUrl(params, false);

            $.publish('plugin/swListingActions/onApplyUrlParams', [ me, urlParams ]);
        },

        /**
         * Returns the full url path to the listing
         * including all current url params.
         *
         * @param urlParams
         * @param encode | Boolean
         * @returns {*}
         */
        getListingUrl: function(urlParams, encode) {
            var me = this,
                params = urlParams || me.urlParams,
                url;

            if (encode) {
                url = encodeURI(me.controllerURL + params);
            } else {
                url = me.controllerURL + params;
            }

            $.publish('plugin/swListingActions/onGetListingUrl', [ me, url, urlParams, encode ]);

            return url;
        },

        /**
         * Buffers a function by the given buffer time.
         *
         * @param func
         * @param bufferTime
         */
        buffer: function(func, bufferTime) {
            var me = this;

            if (me.bufferTimeout) {
                clearTimeout(me.bufferTimeout);
            }

            me.bufferTimeout = setTimeout(func, bufferTime);

            $.publish('plugin/swListingActions/onBuffer', [ me, me.bufferTimeout, func, bufferTime ]);
        },

        /**
         * Resets the current buffer timeout.
         */
        resetBuffer: function() {
            var me = this;

            me.bufferTimeout = 0;

            $.publish('plugin/swListingActions/onResetBuffer', [ me, me.bufferTimeout ]);
        },

        /**
         * Gets the counted result of found products
         * with the current applied category parameters.
         * Updates the filter submit button on success.
         *
         * @param urlParams
         */
        getFilterResult: function(urlParams) {
            var me = this,
                params = urlParams || me.urlParams;

            me.resetBuffer();

            $.ajax({
                type: 'get',
                url: me.resultCountURL + params,
                success: function(response) {
                    me.$applyFilterBtn.removeClass(me.opts.loadingClass);

                    me.updateFilterButton(response.totalCount);

                    $.publish('plugin/swListingActions/onGetFilterResultFinished', [ me, response, params ]);
                }
            });

            $.publish('plugin/swListingActions/onGetFilterResult', [ me, params ]);
        },

        /**
         * Updates the layout of the filter submit button
         * with the new count of found products.
         *
         * @param count
         */
        updateFilterButton: function(count) {
            var me = this;

            me.$applyFilterBtn.find(me.opts.filterCountSelector).html(count);

            if (count <= 0) {
                me.$applyFilterBtn.attr('disabled', 'disabled');
            } else {
                me.$applyFilterBtn.removeAttr('disabled');
            }

            $.publish('plugin/swListingActions/onUpdateFilterButton', [ me, count ]);
        },

        /**
         * Updates the layout of the filter trigger button
         * on mobile viewports with the current count of active filters.
         *
         * @param activeFilterCount
         */
        updateFilterTriggerButton: function(activeFilterCount) {
            var me = this;

            me.$filterTriggerIcon.html(activeFilterCount || '');

            $.publish('plugin/swListingActions/onUpdateFilterTriggerButton', [ me, activeFilterCount ]);
        },

        /**
         * Creates the labels for active filters from the category params.
         *
         * @param categoryParams
         */
        createActiveFiltersFromCategoryParams: function(categoryParams) {
            var me = this,
                count = 0,
                params = categoryParams || me.categoryParams;

            $.each(me.activeFilterElements, function(key) {
                if (params[key] === undefined || params[key] == 0) {
                    me.removeActiveFilter(key);
                }
            });

            $.each(params, function(key, value) {
                me.createActiveFilter(key, value);
            });

            $.each(me.activeFilterElements, function() {
                count++;
            });

            if (count > 1) {
                me.createActiveFilterElement('reset', me.resetLabel);
            }

            me.$filterCont.toggleClass(me.opts.hasActiveFilterCls, (count > 0));
            me.$activeFilterCont.toggleClass(me.opts.disabledCls, !me.$filterCont.hasClass(me.opts.collapsedCls));

            $.publish('plugin/swListingActions/onCreateActiveFiltersFromCategoryParams', [ me, categoryParams ]);
        },

        /**
         * Creates an active filter label for the given parameter.
         * If the label for the given parameter already
         * exists it will be updated.
         *
         * @param param
         * @param value
         */
        createActiveFilter: function(param, value) {
            var me = this,
                label = me.createActiveFilterLabel(param, value);

            if (label !== undefined && label.length) {
                if (me.activeFilterElements[param] !== undefined) {
                    me.updateActiveFilterElement(param, label)
                } else {
                    me.createActiveFilterElement(param, label);
                }
            }

            $.publish('plugin/swListingActions/onCreateActiveFilter', [ me, param, value ]);
        },

        /**
         * Creates the DOM element for an active filter label.
         *
         * @param param
         * @param label
         */
        createActiveFilterElement: function(param, label) {
            var me = this;

            me.activeFilterElements[param] = $('<span>', {
                'class': me.opts.activeFilterCls,
                'html': me.getLabelIcon() + label,
                'data-filter-param': param
            }).appendTo(me.$activeFilterCont);

            $.publish('plugin/swListingActions/onCreateActiveFilterElement', [ me, param, label ]);
        },

        /**
         * Updates the layout of an existing filter label element.
         *
         * @param param
         * @param label
         */
        updateActiveFilterElement: function(param, label) {
            var me = this;

            me.activeFilterElements[param].html(me.getLabelIcon() + label);

            $.publish('plugin/swListingActions/onUpdateActiveFilterElement', [ me, param, label ]);
        },

        /**
         * Removes an active filter label from the set and from the DOM.
         *
         * @param param
         */
        removeActiveFilter: function(param) {
            var me = this;

            me.activeFilterElements[param].remove();

            delete me.activeFilterElements[param];

            $.publish('plugin/swListingActions/onRemoveActiveFilter', [ me, param ]);
        },

        /**
         * Resets a filter parameter and updates
         * the component based on the component type.
         *
         * @param param
         */
        resetFilterProperty: function(param) {
            var me = this,
                $input,
                rangeSlider;

            if (param == 'rating') {
                me.$el.find('#star--reset').prop('checked', true).trigger('change');
            } else {
                $input = me.$el.find('[name="'+me.escapeDoubleQuotes(param)+'"]');
                if ($input.is('[data-range-input]')) {
                    rangeSlider = $input.parents('[data-range-slider="true"]').data('plugin_swRangeSlider');
                    rangeSlider.reset($input.attr('data-range-input'));
                } else {
                    $input.removeAttr('checked').trigger('change');
                }
            }

            $.publish('plugin/swListingActions/onResetFilterProperty', [ me, param ]);
        },

        /**
         * Creates the correct label content for an active
         * filter label based on the component type.
         *
         * @param param
         * @param value
         * @returns {string}
         */
        createActiveFilterLabel: function(param, value) {
            var me = this,
                $label,
                labelText = '',
                valueString = value + '';

            if (param == 'rating' && value > 0) {
                labelText = me.createStarLabel(value);
            } else {
                $label = me.$filterForm.find('label[for="'+me.escapeDoubleQuotes(param)+'"]');

                if ($label.is('[data-range-label]')) {
                    labelText = $label.prev('span').html() + $label.html();
                } else if ($label.find('img').length) {
                    labelText = $label.find('img').attr('alt');
                } else if (value > 0 || valueString.length > 0) {
                    labelText = $label.html();
                }
            }

            $.publish('plugin/swListingActions/onCreateActiveFilterLabel', [ me, labelText, param, value ]);

            return labelText;
        },

        /**
         * Only escapes a " if it's not already escaped
         * @param string str
         * @returns string
         */
        escapeDoubleQuotes: function (str) {
            return str.replace(/\\([\s\S])|(")/g,"\\$1$2");
        },

        /**
         * Creates the label content for the special rating component.
         *
         * @param stars | Integer
         * @returns {string}
         */
        createStarLabel: function(stars) {
            var me = this,
                label = '',
                i = 0;

            for (i; i < 5; i++) {
                if (i < stars) {
                    label += '<i class="icon--star"></i>';
                } else {
                    label += '<i class="icon--star-empty"></i>';
                }
            }

            $.publish('plugin/swListingActions/onCreateStarLabel', [ me, label, stars ]);

            return label;
        },

        /**
         * Returns the html string of the delete icon
         * for an active filter label.
         *
         * @returns {string}
         */
        getLabelIcon: function() {
            var me = this,
                icon = '<span class="' + me.opts.activeFilterIconCls + '"></span>';

            $.publish('plugin/swListingActions/onCreateStarLabel', [ me, icon ]);

            return icon;
        },

        /**
         * Opens the filter form panel based on the current state.
         */
        openFilterPanel: function() {
            var me = this;

            if (!me.$filterCont.hasClass(me.opts.hasActiveFilterCls)) {
                me.$activeFilterCont.slideDown(me.opts.animationSpeed);
            }

            me.$filterForm.slideDown(me.opts.animationSpeed);
            me.$activeFilterCont.removeClass(me.opts.disabledCls);
            me.$filterCont.addClass(me.opts.collapsedCls);
            me.$filterTrigger.addClass(me.opts.activeCls);

            $.publish('plugin/swListingActions/onOpenFilterPanel', [ me ]);
        },

        /**
         * Closes the filter form panel based on the current state.
         */
        closeFilterPanel: function() {
            var me = this;

            if (!me.$filterCont.hasClass(me.opts.hasActiveFilterCls)) {
                me.$activeFilterCont.slideUp(me.opts.animationSpeed);
            }

            me.$filterForm.slideUp(me.opts.animationSpeed);
            me.$activeFilterCont.addClass(me.opts.disabledCls);
            me.$filterCont.removeClass(me.opts.collapsedCls);
            me.$filterTrigger.removeClass(me.opts.activeCls);

            $.publish('plugin/swListingActions/onCloseFilterPanel', [ me ]);
        },

        /**
         * Destroys the plugin.
         */
        destroy: function() {
            var me = this;

            me.$el.off(me.getEventName('click'), '.' + me.opts.activeFilterCls);

            me._destroy();
        }
    });
})(jQuery, window, StateManager, undefined);

;(function($, window) {
    'use strict';

    $.plugin('swCollapseCart', {

        defaults: {

            /**
             * URL that will be called when the plugin is fetching the cart.
             *
             * @type {String}
             */
            'ajaxCartURL': window.controller['ajax_cart'],

            /**
             * Selector for the trigger element.
             * The trigger is the element that attaches to the click/tap/hover events.
             *
             * @type {String}
             */
            'triggerElSelector': '.navigation--entry.entry--cart',

            /**
             * Selector for the elements item container.
             *
             * @type {String}
             */
            'itemContainerSelector': '.item--container',

            /**
             * Selector for the remove button of single items.
             *
             * @type {String}
             */
            'removeItemSelector': '.action--remove',

            /**
             * Selector for the offcanvas close button.
             *
             * @type {String}
             */
            'offcanvasCloseElSelector': '.close--off-canvas',

            /**
             * Class for the loading indicator icon.
             *
             * @type {String}
             */
            'loadingIconClass': 'icon--loading-indicator',

            /**
             * Class that will be used for the loading icon wrapper.
             *
             * @type {String}
             */
            'loadingIconWrapperClass': 'ajax--cart',

            /**
             * Class that will be applied to the main plugin element when the menu opens.
             *
             * @type {String}
             */
            'activeClass': 'is--shown',

            /**
             * Mode of displaying the cart.
             * Can either be "collapsible" or "offcanvas".
             *
             * @type {String} displayMode
             */
            'displayMode': 'collapsible'
        },

        /**
         * Initializes the plugin and create all needed elements.
         */
        init: function () {
            var me = this,
                opts;

            // Override options with data attributes.
            me.applyDataAttributes();

            opts = me.opts;

            /**
             * Element that the events get attached to.
             *
             * @private
             * @property _$triggerEl
             * @type {jQuery}
             */
            me._$triggerEl = $(opts.triggerElSelector);

            /**
             * Button element to change disabled state
             *
             * @private
             * @property _$linkEl
             * @type {jQuery}
             */
            me._$linkEl = me._$triggerEl.find('.cart--link');

            /**
             * Holds the state if the mouse is over the cart
             *
             * @private
             * @property _mousePosition
             * @type {boolean}
             */
            me._isOverMe = false;

            /**
             * Holds the state if the cart is loading
             * @type {boolean}
             * @private
             */
            me._isCartLoading = false;

            /**
             * Loading icon that will be used for loading when an AJAX request is send.
             *
             * @private
             * @property _$loadingIcon
             * @type {jQuery}
             */
            me._$loadingIcon = $('<i>', {
                'class': opts.loadingIconClass
            });

            /**
             * Flag whether or not the menu is opened.
             *
             * @private
             * @property _isOpened
             * @type {Boolean}
             */
            me._isOpened = false;

            // if the display mode is "offcanvas", call the offcanvas plugin.
            if (me.isDisplayMode('offcanvas')) {
                me._$triggerEl.swOffcanvasMenu({
                    'offCanvasSelector': me.$el,
                    'direction': 'fromRight'
                });
            }

            me.registerEvents();
        },

        /**
         * Registers all needed events specific to the display mode.
         *
         * @public
         * @method registerEvents
         */
        registerEvents: function () {
            var me = this;

            me.$el.on(me.getEventName('click'), me.opts.removeItemSelector, $.proxy(me.onRemoveButtonClick, me));
            me.$el.on(me.getEventName('click touchstart'), me.opts.offcanvasCloseElSelector, $.proxy(me.onCloseButtonClick, me));

            if (me.isDisplayMode('offcanvas')) {
                me._on(me._$triggerEl, 'click touchstart', $.proxy(me.onMouseEnter, me));

                $.subscribe('plugin/swAddArticle/onAddArticle', $.proxy(me.onArticleAdded, me));
                $.subscribe('plugin/swAddArticle/onBeforeAddArticle', $.proxy(me.onBeforeAddArticle, me));
            } else {
                me._on('.container--ajax-cart,' + me.opts.triggerElSelector, 'mousemove', $.proxy(me.onMouseHover, me));
                me._on(me._$triggerEl, 'mouseenter touchstart', $.proxy(me.onMouseEnter, me));
                me._on(me._$triggerEl, 'mouseleave', $.proxy(me.onMouseLeave, me));
                me._on(me._$triggerEl, 'click', $.proxy(me.onClick, me));
                me._on(me.$el, 'mouseleave', $.proxy(me.onMouseLeave, me));
                $('.container--ajax-cart,' + me.opts.triggerElSelector).hover(
                    $.proxy(me.onMouseHoverStart, me),
                    $.proxy(me.onMouseHoverEnd, me)
                );
            }

            $.publish('plugin/swCollapseCart/onRegisterEvents', [ me ]);
        },

        /**
         * Will be fired from the addArticle plugin before the add-AJAX request will be send.
         * Sets the loading indicator as the content and opens the menu.
         *
         * @event onBeforeAddArticle
         */
        onBeforeAddArticle: function () {
            var me = this;

            me.showLoadingIndicator();
            me.openMenu();

            $.publish('plugin/swCollapseCart/onBeforeAddArticle', [ me ]);
        },

        /**
         * Will be fired from the addArticle plugin before the add-AJAX request is finished.
         * Loads the cart via AJAX and appends it to the basket.
         *
         * @event onArticleAdded
         */
        onArticleAdded: function (event, plugin, response) {
            var me = this;

            if (me.isDisplayMode('collapsible')) {
                return;
            }

            me.$el
                .html(response)
                .find('.ajax--cart .alert')
                .removeClass('is--hidden');

            picturefill();

            $.publish('plugin/swCollapseCart/onArticleAdded', [ me ]);
        },

        /**
         * Will be called when the pointer enters/clicks/taps the trigger element.
         *
         * @event onMouseEnter
         * @param {jQuery.Event} event
         */
        onMouseEnter: function (event) {
            var me = this;

            if (me.isDisplayMode('offcanvas')) {
                event.preventDefault();

                me.showLoadingIndicator();
                me.openMenu();

                me.loadCart();
            } else {
                if (me.isCartLoading()) {
                    me.showLoadingIndicator();
                    me.openMenu();
                } else {
                    me.buffer(function () {
                        if (me.isOverMe() === false || me._wasClicked === true) {
                            return;
                        }

                        me.showLoadingIndicator();
                        me.openMenu();

                        me.loadCart(function () {
                            $('body').one('touchstart', $.proxy(me.onMouseLeave, me));

                            $.publish('plugin/swCollapseCart/onMouseEnterLoaded', [me, event]);
                        });

                        $.publish('plugin/swCollapseCart/onMouseEnterBuffer', [me, event]);
                    }, 500);
                }
            }

            $.publish('plugin/swCollapseCart/onMouseEnter', [ me, event ]);
        },

        /**
         * Will be called when the mouse leaves the trigger/plugin element.
         *
         * @event onMouseLeave
         * @param {jQuery.Event} event
         */
        onMouseLeave: function (event) {
            var me = this,
                target = event.toElement || event.relatedTarget || event.target;

            $.publish('plugin/swCollapseCart/onMouseLeave', [ me, event ]);

            if (me.isElementOrChild(me.$el[0], target) || me.isElementOrChild(me._$triggerEl[0], target)) {
                return;
            }

            me.closeMenu();
            me.clearBuffer();
        },

        /**
         * Will be called when the off canvas close button was clicked/tapped
         *
         * @event onCloseButtonClick
         * @param {jQuery.Event} event
         */
        onCloseButtonClick: function (event) {
            event.preventDefault();

            $.publish('plugin/swCollapseCart/onCloseButton', [ this ]);

            this.closeMenu();
        },

        /**
         * Will be called when the remove item button was clicked.
         *
         * @event onRemoveButtonClick
         * @param {jQuery.Event} event
         */
        onRemoveButtonClick: function (event) {
            event.preventDefault();

            var me = this,
                $currentTarget = $(event.currentTarget),
                $parent = $currentTarget.parent(),
                $form = $currentTarget.closest('form'),
                url;

            // @deprecated: Don't use anchors for action links. Use forms with method="post" instead.
            if ($currentTarget.attr('href')) {
                url = $currentTarget.attr('href');
            } else {
                url = $form.attr('action');
            }

            $.publish('plugin/swCollapseCart/onRemoveArticle', [ me, event ]);
            $parent.html(me._$loadingIcon.clone());

            $.ajax({
                'url': url,
                'dataType': 'jsonp',
                'success': function(result) {

                    me.$el.html(result);

                    picturefill();

                    $.publish('plugin/swCollapseCart/onRemoveArticleFinished', [ me, event, result ]);
                }
            });
        },

        /**
         * Sets a timeout and saves its timeout id.
         * When an id already exists, clear the timeout that belongs to that id.
         *
         * @param func
         * @param bufferTime
         */
        buffer: function(func, bufferTime) {
            var me = this;

            me.clearBuffer();
            me.bufferTimeout = setTimeout(func, bufferTime);
        },

        /**
         * Clears the open cart timeout
         */
        clearBuffer: function() {
            var me = this;

            if (me.bufferTimeout) {
                clearTimeout(me.bufferTimeout);
            }
        },

        /**
         * Returns whether or not the second element is the same as / a child of the first.
         *
         * @param {HTMLElement} firstEl
         * @param {HTMLElement} secondEl
         * @returns {Boolean}
         */
        isElementOrChild: function (firstEl, secondEl) {
            return firstEl === secondEl || $.contains(firstEl, secondEl);
        },

        /**
         * Returns whether or not the current display mode is the given one.
         *
         * @public
         * @method isDisplayMode
         * @param {String} mode
         * @returns {Boolean}
         */
        isDisplayMode: function (mode) {
            return this.opts.displayMode === mode;
        },

        /**
         * Overrides the elements content with the configured loading indicator.
         *
         * @public
         * @method showLoadingIndicator
         */
        showLoadingIndicator: function () {
            var me = this;

            me.$el.html($('<div>', {
                'class': me.opts.loadingIconWrapperClass,
                'html': me._$loadingIcon.clone()
            }));

            $.publish('plugin/swCollapseCart/onShowLoadingIndicator', [ me ]);
        },

        /**
         * Opens the offcanvas/collapsible cart.
         * If the offcanvas plugin is active on the element, its openMenu function will also be called.
         *
         * @public
         * @method closeMenu
         */
        openMenu: function () {
            var me = this,
                plugin;

            me._isOpened = true;

            if (me.isDisplayMode('offcanvas') && (plugin = me._$triggerEl.data('plugin_swOffcanvasMenu'))) {
                plugin.openMenu();
            } else {
                me.$el.addClass(me.opts.activeClass);
            }

            $.publish('plugin/swCollapseCart/onMenuOpen', [ me ]);
        },

        /**
         * Loads the cart content via the configured URL
         * and sets the response as plugin elements content.
         *
         * @public
         * @method loadCart
         * @param {Function} callback
         */
        loadCart: function (callback) {
            var me = this,
                opts = me.opts,
                $el = me.$el;

            if (me.isCartLoading()) {
                return;
            }

            $.publish('plugin/swCollapseCart/onLoadCart', [ me ]);

            me._$linkEl.addClass('is--disabled');
            me._isCartLoading = true;

            $.ajax({
                'url': opts.ajaxCartURL,
                'dataType': 'jsonp',
                'success': function (result) {
                    $el.html(result);
                    picturefill();

                    if (typeof callback === 'function') {
                        callback();
                    }

                    $.publish('plugin/swCollapseCart/onLoadCartFinished', [ me, result ]);
                },
                'complete': function () {
                    me._$linkEl.removeClass('is--disabled');
                    me._isCartLoading = false;
                }
            });
        },

        /**
         * Closes the offcanvas/collapsible cart.
         * If the offcanvas plugin is active on the element, its closeMenu function will also be called.
         *
         * @public
         * @method closeMenu
         */
        closeMenu: function () {
            var me = this,
                plugin;

            me._isOpened = false;

            if (me.isDisplayMode('offcanvas') && (plugin = me._$triggerEl.data('plugin_swOffcanvasMenu'))) {
                plugin.closeMenu();
            } else {
                me.$el.removeClass(me.opts.activeClass);
            }

            $.publish('plugin/swCollapseCart/onCloseMenu', [ me ]);
        },

        /**
         * Intercepts the click event to prevent redirect while
         * the request is being made
         *
         * @param event
         */
        onClick: function(event) {
            var me = this;

            if (me.isCartLoading()) {
                event.preventDefault();
                return false;
            }

            me._wasClicked = true;
        },

        /**
         * Indicates if the cart is currently loading
         *
         * @returns {boolean}
         */
        isCartLoading: function() {
            return !!this._isCartLoading;
        },

        /**
         * Indicates if the mouse is over the cart button or cart menu itself
         *
         * @returns {boolean}
         */
        isOverMe: function() {
            return !!this._isOverMe;
        },

        /**
         * Indicates that the mouse is over the element
         */
        onMouseHoverStart: function() {
            this._isOverMe = true;
        },

        /**
         * Indicates that the mouse is not over the element anymore
         */
        onMouseHoverEnd: function() {
            this._isOverMe = false;
        },

        /**
         * Destroys the plugin and removes all attached events and delegations.
         *
         * @public
         * @method destroy
         */
        destroy: function () {
            var me = this;

            me.off(me.eventSuffix);

            me._destroy();
        }
    });
})(jQuery, window);

;(function($, window, document, undefined) {
    "use strict";

    /**
     * Local private variables.
     */
    var $window = $(window),
        $body = $('body');

    /**
     * Emotion Loader Plugin
     *
     * This plugin is called on emotion wrappers to load emotion worlds
     * for the specific device types dynamically via ajax.
     */
    $.plugin('swEmotionLoader', {

        defaults: {

            /**
             * The url of the controller to load the emotion world.
             *
             * @property controllerUrl
             * @type {string}
             */
            controllerUrl: null,

            /**
             * The names of the devices for which the emotion world is available.
             *
             * @property availableDevices
             * @type {string}
             */
            availableDevices: null,

            /**
             * Show or hide the listing on category pages.
             *
             * @property showListing
             * @type {boolean}
             */
            showListing: false,

            /**
             * Configuration object to map device types to IDs.
             *
             * @property deviceTypes
             * @type {object}
             */
            deviceTypes: {
                'xl': '0',
                'l' : '1',
                'm' : '2',
                's' : '3',
                'xs': '4'
            },

            /**
             * The DOM selector of emotion wrapper elements
             *
             * @property wrapperSelector,
             * @type {string}
             */
            wrapperSelector: '.emotion--wrapper',

            /**
             * The DOM selector of the fallback content
             * if no emotion world is available.
             *
             * @property fallbackContentSelector
             * @type {string}
             */
            fallbackContentSelector: '.listing--wrapper',

            /**
             * The DOM selector of the show listing link.
             *
             * @property showListingSelector
             * @type {string}
             */
            showListingSelector: '.emotion--show-listing',

            /**
             * The DOM selector for the loading overlay.
             *
             * @property loadingOverlaySelector
             * @type {string}
             */
            loadingOverlaySelector: '.emotion--overlay'
        },

        /**
         * Plugin constructor
         */
        init: function() {
            var me = this,
                opts = me.opts;

            me.applyDataAttributes();

            if (opts.controllerUrl === null ||
                opts.availableDevices === null) {
                me.$el.remove();
                return;
            }

            me.$emotion = false;

            me.$siblings = me.$el.siblings(opts.wrapperSelector);

            me.hasSiblings = (me.$siblings.length > 0);
            me.availableDevices = (opts.availableDevices + '').split(',');

            me.$fallbackContent = $(opts.fallbackContentSelector);
            me.$showListingLink = $(opts.showListingSelector);

            me.$overlay = $(me.opts.loadingOverlaySelector);

            if (!opts.showListing) {
                me.hideFallbackContent();
            }

            me.loadEmotion();
            me.registerEvents();
        },

        /**
         * Registers all necessary event listner.
         */
        registerEvents: function() {
            var me = this;

            StateManager.on('resize', $.proxy(me.onDeviceChange, me));

            $.publish('plugin/swEmotionLoader/onRegisterEvents', [ me ]);
        },

        /**
         * Called on resize event of the StateManager.
         */
        onDeviceChange: function() {
            var me = this;

            me.loadEmotion();

            $.publish('plugin/swEmotionLoader/onDeviceChange', [ me ]);
        },

        /**
         * Loads an emotion world for a given device state.
         * If the emotion world for the state was already loaded
         * it will just be initialized again from local save.
         *
         * @param controllerUrl
         * @param deviceState
         */
        loadEmotion: function(controllerUrl, deviceState) {
            var me = this,
                devices = me.availableDevices,
                types = me.opts.deviceTypes,
                url = controllerUrl || me.opts.controllerUrl,
                state = deviceState || StateManager.getCurrentState();

            /**
             * If the emotion world is not defined for the current device,
             * hide the wrapper element and show the default content.
             */
            if (devices.indexOf(types[state]) === -1) {
                var hasSameDeviceSibling = false;

                me.hideEmotion();

                me.$siblings.each(function(index, el) {
                    var devices = $(el).attr('data-availabledevices');

                    if (devices.indexOf(types[state]) !== -1) {
                        hasSameDeviceSibling = true;
                    }
                });

                if (!hasSameDeviceSibling) me.showFallbackContent();
                return;
            }

            /**
             * If the plugin is not configured correctly show the default content.
             */
            if (!devices.length || !state.length || !url.length) {
                me.hideEmotion();
                me.showFallbackContent();
                return;
            }

            /**
             * If the emotion world was already loaded show it.
             */
            if (me.$emotion.length) {

                (me.opts.showListing) ? me.showFallbackContent() : me.hideFallbackContent();

                me.$overlay.remove();
                me.showEmotion();
                return;
            }

            /**
             * Show the loading indicator and load the emotion world.
             */
            me.showEmotion();

            if (me.isLoading) {
                return;
            }

            me.isLoading = true;
            me.$overlay.insertBefore('.content-main');

            $.ajax({
                url: url,
                method: 'GET',
                success: function (response) {

                    me.isLoading = false;
                    me.$overlay.remove();

                    $.publish('plugin/swEmotionLoader/onLoadEmotionLoaded', [ me ]);

                    if (!response.length) {
                        me.hideEmotion();
                        me.showFallbackContent();
                        return;
                    }

                    (me.opts.showListing) ? me.showFallbackContent() : me.hideFallbackContent();

                    me.initEmotion(response);

                    $.publish('plugin/swEmotionLoader/onLoadEmotionFinished', [ me ]);
                }
            });

            $.publish('plugin/swEmotionLoader/onLoadEmotion', [ me ]);
        },

        /**
         * Removes the content of the container by
         * the new emotion world markup and initializes it.
         *
         * @param html
         */
        initEmotion: function(html) {
            var me = this;

            me.$el.html(html);
            me.$emotion = me.$el.find('*[data-emotion="true"]');

            if (!me.$emotion.length) {
                me.showFallbackContent();
                return;
            }

            me.$emotion.swEmotion();

            $.publish('plugin/swEmotionLoader/onInitEmotion', [ me, html ]);
        },

        /**
         * Shows the emotion world.
         */
        showEmotion: function() {
            var me = this;

            me.$el.css('display', 'block');

            $.publish('plugin/swEmotionLoader/onShowEmotion', [ me ]);
        },

        /**
         * Hides the emotion world.
         */
        hideEmotion: function() {
            var me = this;

            me.$el.css('display', 'none');

            $.publish('plugin/swEmotionLoader/onHideEmotion', [ me ]);
        },

        /**
         * Shows the fallback content.
         */
        showFallbackContent: function() {
            var me = this;

            me.$fallbackContent.removeClass('is--hidden');
            me.$showListingLink.addClass('is--hidden');

            me.$overlay.remove();

            StateManager.updatePlugin('*[data-infinite-scrolling="true"]', 'swInfiniteScrolling');

            $.publish('plugin/swEmotionLoader/onShowFallbackContent', [ me ]);
        },

        /**
         * Hides the fallback content.
         */
        hideFallbackContent: function() {
            var me = this;

            me.$fallbackContent.addClass('is--hidden');
            me.$showListingLink.removeClass('is--hidden');

            StateManager.updatePlugin('*[data-infinite-scrolling="true"]', 'swInfiniteScrolling');

            $.publish('plugin/swEmotionLoader/onHideFallbackContent', [ me ]);
        },

        /**
         * Destroys the plugin.
         */
        destroy: function() {
            var me = this;

            me._destroy();
        }
    });


    /**
     * Emotion plugin
     *
     * This plugin is called on each single emotion world
     * for handling the grid sizing and all elements in it.
     */
    $.plugin('swEmotion', {

        defaults: {

            /**
             * The grid mode of the emotion grid.
             *
             * @property gridMode ( resize | fluid )
             * @type {string}
             */
            gridMode: 'resize',

            /**
             * The base width in px for dynamic measurement.
             * Used for resize mode to have a base orientation for scaling.
             * Number is based on the fixed container width in desktop mode.
             *
             * @property baseWidth
             * @type {number}
             */
            baseWidth: 1160,

            /**
             * Turn fullscreen mode on and off.#
             *
             * @property fullScreen
             * @type {boolean}
             */
            fullscreen: false,

            /**
             * The number of columns in the grid.
             *
             * @property columns
             * @type {number}
             */
            columns: 4,

            /**
             * The height of one grid cell in px.
             *
             * @property cellHeight
             * @type {number}
             */
            cellHeight: 185,

            /**
             * The space in px between the elements in the grid.
             *
             * @property cellSpacing
             * @type {number}
             */
            cellSpacing: 10,

            /**
             * The DOM selector for the emotion elements.
             *
             * @property elementSelector
             * @type {string}
             */
            elementSelector: '.emotion--element',

            /**
             * The DOM selector for the sizer element.
             *
             * @property elementSelector
             * @type {string}
             */
            gridSizerSelector: '.emotion--sizer',

            /**
             * The DOM selector for banner elements.
             *
             * @property bannerElSelector
             * @type {string}
             */
            bannerElSelector: '[data-coverImage="true"]',

            /**
             * The DOM selector for video elements.
             *
             * @property videoElSelector
             * @type {string}
             */
            videoElSelector: '.emotion--video'
        },

        /**
         * Plugin constructor
         */
        init: function() {
            var me = this;

            me.applyDataAttributes();

            me.bufferedCall = false;

            me.$contentMain = $('.content-main');
            me.$container = me.$el.parents('.content--emotions');
            me.$wrapper = me.$el.parents('.emotion--wrapper');

            me.$elements = me.$el.find(me.opts.elementSelector);
            me.$gridSizer = me.$el.find(me.opts.gridSizerSelector);

            me.$bannerElements = me.$elements.find(me.opts.bannerElSelector);
            me.$videoElements = me.$elements.find(me.opts.videoElSelector);
            me.$productSliderElements = me.$elements.find('*[data-product-slider="true"]');

            me.remSpacing = ~~me.opts.cellSpacing / 16;

            me.currentState = window.StateManager.getCurrentState();

            if (me.opts.fullscreen) {
                me.initFullscreen();
            }

            me.initState(me.currentState);
            me.initMode(me.opts.gridMode);

            me.initElements();
            me.registerEvents();
        },

        /**
         * Initializes the grid mode by the given option.
         * Searches for a method with the name pattern 'init' + Name + 'Grid'.
         * This enables you to extend the plugin with additional grid types by adding the necessary init method.
         * If there is no corresponding method for the grid type, the mode "fluid" will be used as default.
         *
         * @param {string} gridMode
         */
        initMode: function(gridMode) {
            var me = this,
                mode = gridMode || me.opts.gridMode,
                modeMethod = 'init' + mode.charAt(0).toUpperCase() + mode.slice(1) + 'Grid';

            if (typeof me[modeMethod] === 'function') {
                me[modeMethod]();
            } else {
                me.initFluidGrid();
            }

            if (mode !== 'resize') {
                me.setContainerSpacing();
            }
        },

        /**
         * Initializes the shopping world for the current viewport state.
         */
        initState: function(state) {
            var me = this;

            state = state || window.StateManager.getCurrentState();

            me.$sizer = me.$el.find('.emotion--sizer-' + state);

            me.clsPrefix = '-' + state;

            if (me.$sizer.length <= 0) {
                me.$sizer = me.$el.find('.emotion--sizer');
                me.clsPrefix = '';
            }

            me.rows = ~~me.$sizer.attr('data-rows');
        },

        /**
         * Initializes special elements and their needed plugins.
         */
        initElements: function() {
            var me = this;

            if (me.opts.gridMode !== 'rows') {

                $.each(me.$bannerElements, function(index, item) {
                    $(item).swEmotionBanner();
                });
            }

            $.each(me.$videoElements, function(index, item) {
                $(item).swEmotionVideo();
            });

            StateManager.updatePlugin('*[data-product-slider="true"]', 'swProductSlider');
            StateManager.updatePlugin('*[data-image-slider="true"]', 'swImageSlider');

            window.picturefill();

            $.publish('plugin/swEmotion/onInitElements', [ me ]);
        },

        /**
         * Initializes the fullscreen mode.
         */
        initFullscreen: function() {
            var me = this;

            $body.addClass('is--no-sidebar');
            me.$contentMain.addClass('is--fullscreen');
            me.$wrapper.addClass('is--fullscreen');

            $.publish('plugin/swEmotion/onInitFullscreen', [ me ]);
        },

        /**
         * Removes the fullscreen mode.
         */
        removeFullscreen: function(showSidebar) {
            var me = this;

            if (showSidebar) $body.removeClass('is--no-sidebar');
            me.$contentMain.removeClass('is--fullscreen');
            me.$wrapper.removeClass('is--fullscreen');

            $.publish('plugin/swEmotion/onRemoveFullscreen', [ me, showSidebar ]);
        },

        /**
         * @deprecated The masonry mode was removed with version 5.2
         */
        initMasonryGrid: function() {
            var me = this;

            /**
             * It will fallback to the new fluid mode
             */
            me.initFluidGrid();

            $.publish('plugin/swEmotion/onInitMasonryGrid', [ me ]);
        },

        /**
         * Initializes the grid for the fluid mode.
         */
        initFluidGrid: function() {
            var me = this;

            me.setElementHeights();
            me.setElementPositions();

            $.publish('plugin/swEmotion/onInitFluidGrid', [ me ]);
        },

        /**
         * Initializes the grid for the resize mode.
         */
        initResizeGrid: function() {
            var me = this;

            me.baseWidth = ~~me.opts.baseWidth;

            me.$el.css('width', me.baseWidth + me.opts.cellSpacing);

            if (!me.opts.fullscreen) {
                me.$wrapper.css('max-width', me.baseWidth);
            }

            me.setElementHeights();
            me.setElementPositions();

            me.scale();

            $.publish('plugin/swEmotion/onInitScaleGrid', [ me ]);
        },

        /**
         * Initializes the grid for the rows mode.
         */
        initRowsGrid: function() {
            var me = this,
                r, c, rowCls, colCls, element, elementCols, lastCol = 0,
                colExp = new RegExp(' col'+ me.clsPrefix +'-(\\d)', 'i'),
                hiddenElements = $('<div>', { 'class': 'hidden-elements' }),
                rows = [];

            // Save hidden elements in new element for later use
            me.$elements.filter('.is--hidden' + me.clsPrefix).appendTo(hiddenElements);

            // Iterate through all rows and create wrapper elements for each row
            for(r = 1; r <= me.rows; r++) {

                rows[r] = $('<div>', { 'class': 'emotion--row row--' + r });
                lastCol = 0;

                // Iterate through each column of the row and add the corresponding elements to the row
                for(c = 1; c <= me.opts.columns; c++) {
                    rowCls = '.start-row' + me.clsPrefix + '-' + r;
                    colCls = '.start-col' + me.clsPrefix + '-' + c;

                    // Get all elements matching the row and col class, excluding the hidden elements.
                    element = me.$elements.filter(rowCls + colCls).not('.is--hidden' + me.clsPrefix);

                    if (element.length > 0) {
                        elementCols = ~~(element.attr('class').match(colExp)[1] || 1);

                        element.appendTo(rows[r]);

                        if (c - lastCol > 1) {
                            element.css('margin-left', 100 / me.opts.columns * (c - lastCol - 1) + '%');
                        } else {
                            element.css('margin-left', 'inherit');
                        }

                        lastCol = c + elementCols - 1;
                    }
                }
            }

            me.$el.find(':not([data-rows])').remove();

            hiddenElements.appendTo(me.$el);

            $.each(rows, function (rowIndex, $row) {
                me.$el.append($row);
            });

            $.publish('plugin/swEmotion/onInitRowsGrid', [ me, rows, hiddenElements ]);
        },

        /**
         * Registers all necessary event listener.
         */
        registerEvents: function() {
            var me = this;

            window.StateManager.on('resize', $.proxy(me.onResize, me));

            if (me.opts.fullscreen) {
                $.subscribe('plugin/swEmotionLoader/onShowEmotion', $.proxy(me.onShow, me));
                $.subscribe('plugin/swEmotionLoader/onHideEmotion', $.proxy(me.onHide, me));
            }

            $.publish('plugin/swEmotion/onRegisterEvents', [ me ]);
        },

        /**
         * Called by event listener on window resize.
         */
        onResize: function() {
            var me = this,
                state = window.StateManager.getCurrentState();

            me.initState(state);

            if (me.opts.gridMode === 'resize') {
                me.scale();
            }

            if (me.opts.gridMode === 'resize' || me.opts.gridMode === 'fluid') {
                me.setElementHeights();
                me.setElementPositions();
            }

            if (me.opts.gridMode === 'rows' && me.currentState !== state) {
                me.initRowsGrid();
            }

            me.$bannerElements.trigger('emotionResize');
            me.$videoElements.trigger('emotionResize');

            me.currentState = state;

            $.publish('plugin/swEmotion/onResize', [ me, me.currentState ]);
        },

        onShow: function(event, emotion) {
            var me = this;

            if (emotion.$el.is(me.$el)) {
                me.initFullscreen();
            }

            $.publish('plugin/swEmotion/onShow', [ me, event, emotion ]);
        },

        onHide: function(event, emotion) {
            var me = this;

            if (emotion.$el.is(me.$el)) {
                me.removeFullscreen();
            }

            $.publish('plugin/swEmotion/onHide', [ me, event, emotion ]);
        },

        /**
         * Adds the negative spacing to the container for the grid spacing.
         */
        setContainerSpacing: function() {
            var me = this;

            me.$el.css({
                'margin-left': -me.remSpacing + 'rem'
            });

            $.publish('plugin/swEmotion/onSetContainerSpacing', [ me ]);
        },

        /**
         * Sets the correct position styling for all elements based on the viewport.
         */
        setElementPositions: function() {
            var me = this, i = 1;

            for (i; i <= me.rows; i++) {
                var top = 100 / me.rows * (i - 1);
                me.$elements.filter('.start-row' + me.clsPrefix + '-' + i).css('top', top + '%');
            }

            $.publish('plugin/swEmotion/onSetElementPositions', [ me ]);
        },

        /**
         * Sets the correct height for all elements based on the viewport.
         */
        setElementHeights: function() {
            var me = this, i = 1;

            for (i; i <= me.rows; i++) {
                var height = 100 / me.rows * i;
                me.$elements.filter('.row' + me.clsPrefix + '-' + i).css('height', height + '%');
            }

            $.publish('plugin/swEmotion/onSetElementHeights', [ me ]);
        },

        /**
         * Scales the emotion grid via css3 transformation for resize mode.
         */
        scale: function() {
            var me = this,
                width = (me.opts.fullscreen) ? $window.outerWidth() : me.$wrapper.outerWidth(),
                ratio = me.baseWidth / me.$el.outerHeight(),
                factor = width / me.baseWidth,
                containerStyle = me.$el.get(0).style,
                wrapperHeight = width / ratio;

            $.extend(containerStyle, {
                'MsTransform': 'scale('+ factor +') translateX(' + -me.remSpacing + 'rem)',
                'OTransform': 'scale('+ factor +') translateX(' + -me.remSpacing + 'rem)',
                'MozTransform': 'scale('+ factor +') translateX(' + -me.remSpacing + 'rem)',
                'webkitTransform': 'scale('+ factor +') translateX(' + -me.remSpacing + 'rem)',
                'transform': 'scale('+ factor +') translateX(' + -me.remSpacing + 'rem)'
            });

            me.$wrapper.css('height', wrapperHeight);

            $.publish('plugin/swEmotion/onScale', [ me, width, factor, wrapperHeight ]);
        },

        /**
         * Buffers the calling of a function.
         *
         * @param func
         * @param bufferTime
         */
        buffer: function(func, bufferTime) {
            var me = this;

            window.clearTimeout(me.bufferedCall);

            me.bufferedCall = window.setTimeout($.proxy(func, me), bufferTime);

            $.publish('plugin/swEmotion/onBuffer', [ me, me.bufferedCall, func, bufferTime ]);
        },

        /**
         * Destroys the plugin.
         */
        destroy: function() {
            var me = this;

            me._destroy();
        }
    });


    /**
     * Emotion Banner Element
     *
     * This plugin handles banner elements in an emotion world.
     */
    $.plugin('swEmotionBanner', {

        defaults: {

            /**
             * The width of the image in px.
             *
             * @property width
             * @type {number}
             */
            width: null,

            /**
             * The height of the image in px.
             *
             * @proeprty height
             * @type {number}
             */
            height: null,

            /**
             * The DOM selector for the banner container.
             *
             * @property containerSelector
             * @type {string}
             */
            containerSelector: '.banner--content'
        },

        /**
         * Plugin constructor
         */
        init: function() {
            var me = this;

            me.applyDataAttributes();

            me.$container = me.$el.find(me.opts.containerSelector);

            me.imageRatio = me.opts.width / me.opts.height;

            me.resizeBanner();
            me.registerEvents();
        },

        /**
         * Registers all necessary event listener.
         */
        registerEvents: function() {
            var me = this;

            me._on(me.$el, 'emotionResize', $.proxy(me.resizeBanner, me));

            $.publish('plugin/swEmotionBanner/onRegisterEvents', [ me ]);
        },

        /**
         * Does the measuring for the banner mapping container
         * and sets it's new dimensions.
         */
        resizeBanner: function() {
            var me = this,
                containerWidth = me.$el.width(),
                containerHeight = me.$el.height(),
                containerRatio = containerWidth / containerHeight,
                orientation = me.imageRatio > containerRatio,
                bannerWidth = orientation ? containerHeight * me.imageRatio : '100%',
                bannerHeight = orientation ? '100%' : containerWidth / me.imageRatio;

            me.$container.css({
                'width': bannerWidth,
                'height': bannerHeight
            });

            $.publish('plugin/swEmotionBanner/onResizeBanner', [ me ]);
        },

        /**
         * Destroys the plugin.
         */
        destroy: function() {
            var me = this;

            me._destroy();
        }
    });


    /**
     * Emotion Video Element
     *
     * This plugin handles html5 video elements in an emotion world.
     */
    $.plugin('swEmotionVideo', {

        defaults: {

            /**
             * The sizing mode for the video.
             *
             * @property mode ( scale | cover | stretch )
             * @type {string}
             */
            mode: 'cover',

            /**
             * The X coordinate for the transform origin.
             *
             * @property scaleOriginX
             * @type {number}
             */
            scaleOriginX: 50,

            /**
             * The Y coordinate for the transform origin.
             *
             * @property scaleOriginX
             * @type {number}
             */
            scaleOriginY: 50,

            /**
             * The scale factor for the transforming.
             *
             * @property scale
             * @type {number}
             */
            scale: 1,

            /**
             * The css class for the play icon.
             *
             * @property playIconCls
             * @type {string}
             */
            playIconCls: 'icon--play',

            /**
             * The css class for the pause icon.
             *
             * @property pauseIconCls
             * @type {string}
             */
            pauseIconCls: 'icon--pause',

            /**
             * The DOM selector for the video element.
             *
             * @property videoSelector
             * @type {string}
             */
            videoSelector: '.video--element',

            /**
             * The DOM selector for the video cover element.
             *
             * @property coverSelector
             * @type {string}
             */
            coverSelector: '.video--cover',

            /**
             * The DOM selector for the play button.
             *
             * @property playBtnSelector
             * @type {string}
             */
            playBtnSelector: '.video--play-btn',

            /**
             * The DOM selector for the play icon.
             *
             * @property playIconSelector
             * @type {string}
             */
            playIconSelector: '.video--play-icon'
        },

        /**
         * Plugin constructor
         */
        init: function() {
            var me = this;

            me.applyDataAttributes();

            me.$video = me.$el.find(me.opts.videoSelector);
            me.$videoCover = me.$el.find(me.opts.coverSelector);
            me.$playBtn = me.$el.find(me.opts.playBtnSelector);
            me.$playBtnIcon = me.$playBtn.find(me.opts.playIconSelector);

            me.player = me.$video.get(0);

            /**
             * Cross browser mute support.
             */
            if (me.$video.attr('muted') !== undefined) {
                me.player.volume = 0.0;
            }

            me.setScaleOrigin(me.opts.scaleOriginX, me.opts.scaleOriginY);

            me.registerEvents();
        },

        /**
         * Registers all necessary event listener.
         */
        registerEvents: function() {
            var me = this;

            me._on(me.$video, 'loadedmetadata', $.proxy(me.onLoadMeta, me));
            me._on(me.$video, 'canplay', $.proxy(me.onCanPlay, me));
            me._on(me.$video, 'play', $.proxy(me.onVideoPlay, me));
            me._on(me.$video, 'ended', $.proxy(me.onVideoEnded, me));

            me._on(me.$el, 'emotionResize', $.proxy(me.resizeVideo, me));

            me._on(me.$videoCover, 'click', $.proxy(me.onPlayClick, me));
            me._on(me.$playBtn, 'click', $.proxy(me.onPlayClick, me));

            $.publish('plugin/swEmotionVideo/onRegisterEvents', [ me ]);
        },

        /**
         * Called on loaded meta data event.
         * Gets the video properties from the loaded video.
         */
        onLoadMeta: function(event) {
            var me = this;

            me.videoWidth = me.player.videoWidth;
            me.videoHeight = me.player.videoHeight;
            me.videoRatio = me.videoWidth / me.videoHeight;

            me.resizeVideo();

            $.publish('plugin/swEmotionVideo/onLoadMeta', [ me, event ]);
        },

        /**
         * Called on can play event.
         * Sets the correct play button icon.
         */
        onCanPlay: function(event) {
            var me = this;

            if(!me.player.paused || me.player.autoplay) {
                me.$playBtnIcon.addClass(me.opts.pauseIconCls).removeClass(me.opts.playIconCls);
            }

            $.publish('plugin/swEmotionVideo/onCanPlay', [ me, event ]);
        },

        /**
         * Called on play event.
         */
        onVideoPlay: function(event) {
            var me = this;

            me.$videoCover.hide();

            $.publish('plugin/swEmotionVideo/onVideoPlay', [ me, event ]);
        },

        /**
         * Called on ended event.
         * Sets the correct play button icon.
         */
        onVideoEnded: function(event) {
            var me = this;

            me.$playBtnIcon.removeClass(me.opts.pauseIconCls).addClass(me.opts.playIconCls);

            $.publish('plugin/swEmotionVideo/onVideoEnded', [ me, event ]);
        },

        /**
         * Called on click event on the the play button.
         * Starts or pauses the video.
         */
        onPlayClick: function(event) {
            var me = this;

            event.preventDefault();

            (me.player.paused) ? me.playVideo() : me.stopVideo();

            $.publish('plugin/swEmotionVideo/onPlayClick', [ me, event ]);
        },

        /**
         * Starts the video and sets the correct play button icon.
         */
        playVideo: function() {
            var me = this;

            me.$playBtnIcon.addClass(me.opts.pauseIconCls).removeClass(me.opts.playIconCls);
            me.player.play();

            $.publish('plugin/swEmotionVideo/onPlayVideo', [ me ]);
        },

        /**
         * Pauses the video and sets the correct play button icon.
         */
        stopVideo: function() {
            var me = this;

            me.$playBtnIcon.removeClass(me.opts.pauseIconCls).addClass(me.opts.playIconCls);
            me.player.pause();

            $.publish('plugin/swEmotionVideo/onStopVideo', [ me ]);
        },

        /**
         * Measures the correct dimensions for the video
         * based on the transformation mode.
         */
        resizeVideo: function() {
            var me = this;

            /**
             * Do nothing because it is the standard browser behaviour.
             * The empty space will be filled by black bars.
             */
            if (me.opts.mode === 'scale') {
                return;
            }

            var containerWidth = me.$el.outerWidth(),
                containerHeight = me.$el.outerHeight(),
                containerRatio = containerWidth / containerHeight,
                orientation = me.videoRatio > containerRatio,
                positiveFactor = me.videoRatio / containerRatio,
                negativeFactor = containerRatio / me.videoRatio;

            /**
             * Stretches the video to fill the hole container
             * no matter what dimensions the container has.
             */
            if (me.opts.mode === 'stretch') {
                if (orientation) {
                    me.transformVideo('scaleY(' + positiveFactor * me.opts.scale + ')');
                } else {
                    me.transformVideo('scaleX(' + negativeFactor * me.opts.scale + ')');
                }
            }

            /**
             * Scales up the video to fill the hole container by
             * keeping the video dimensions but cutting overlapping content.
             */
            if (me.opts.mode === 'cover') {
                if (orientation) {
                    me.transformVideo('scaleX(' + positiveFactor * me.opts.scale + ') scaleY(' + positiveFactor * me.opts.scale + ')');
                } else {
                    me.transformVideo('scaleX(' + negativeFactor * me.opts.scale + ') scaleY(' + negativeFactor * me.opts.scale + ')');
                }
            }

            $.publish('plugin/swEmotionVideo/onResizeVideo', [ me ]);
        },

        /**
         * Sets the transform origin coordinates on the video element.
         *
         * @param originX
         * @param originY
         */
        setScaleOrigin: function(originX, originY) {
            var me = this,
                x = originX || me.opts.scaleOriginX,
                y = originY || me.opts.scaleOriginY,
                origin = x+'% '+y+'%';

            me.$video.css({
                '-ms-transform-origin': origin,
                '-o-transform-origin': origin,
                '-moz-transform-origin': origin,
                '-webkit-transform-origin': origin,
                'transform-origin': origin
            });

            $.publish('plugin/swEmotionVideo/onSetScaleOrigin', [ me, x, y ]);
        },

        /**
         * Transforms the video by the given css3 transformation.
         *
         * @param transformation
         */
        transformVideo: function(transformation) {
            var me = this,
                videoElementStyle = me.player.style;

            $.extend(videoElementStyle, {
                'MsTransform': transformation,
                'OTransform': transformation,
                'MozTransform': transformation,
                'webkitTransform': transformation,
                'transform': transformation
            });

            $.publish('plugin/swEmotionVideo/onTransformVideo', [ me, transformation ]);
        },

        /**
         * Destroys the plugin.
         */
        destroy: function() {
            var me = this;

            me._destroy();
        }
    });

})(jQuery, window, document);

;(function($) {
    "use strict";

    /**
     * Shopware Article Compare Add Plugin.
     *
     * The plugin handles the compare add button on every product box.
     */
    $.plugin('swProductCompareAdd', {

        /** Your default options */
        defaults: {
            /** @string compareMenuSelector Listener Class for compare button */
            compareMenuSelector: '.entry--compare',

            /** @string hiddenCls Class which indicates that the element is hidden */
            hiddenCls: 'is--hidden'
        },

        /**
         * Initializes the plugin
         *
         * @returns {Plugin}
         */
        init: function () {
            var me = this;

            // On add article to compare button
            me.$el.on(me.getEventName('click'), '*[data-product-compare-add="true"]', $.proxy(me.onAddArticleCompare, me));

            $.publish('plugin/swProductCompareAdd/onRegisterEvents', [ me ]);
        },

        /**
         * onAddArticleCompare function for adding articles to
         * the compare menu, which will be refreshed by ajax request.
         *
         * @method onAddArticleCompare
         */
        onAddArticleCompare: function (event) {
            var me = this,
                $target = $(event.target),
                $form = $target.closest('form'),
                addArticleUrl;

            event.preventDefault();

            // @deprecated: Don't use anchors for action links. Use forms with method="post" instead.
            if ($target.attr('href')) {
                addArticleUrl = $target.attr('href');
            } else {
                addArticleUrl = $form.attr('action');
            }

            if(!addArticleUrl) {
                return;
            }

            $.overlay.open({
                closeOnClick: false
            });

            $.loadingIndicator.open({
                openOverlay: false
            });

            $.publish('plugin/swProductCompareAdd/onAddArticleCompareBefore', [ me, event ]);

            // Ajax request for adding article to compare list
            $.ajax({
                'url': addArticleUrl,
                'dataType': 'jsonp',
                'success': function(data) {
                    var compareMenu = $(me.opts.compareMenuSelector);

                    if (compareMenu.hasClass(me.opts.hiddenCls)) {
                        compareMenu.removeClass(me.opts.hiddenCls);
                    }

                    // Check if error thrown
                    if (data.indexOf('data-max-reached="true"') !== -1) {

                        $.loadingIndicator.close(function() {
                            $.modal.open(data, {
                                sizing: 'content'
                            });
                        });

                    } else {
                        compareMenu.html(data);

                        // Reload compare menu plugin
                        $('*[data-product-compare-menu="true"]').swProductCompareMenu();

                        // Prevent too fast closing of loadingIndicator and overlay
                        $.loadingIndicator.close(function() {
                            $('html, body').animate({
                                scrollTop: ($('.top-bar').offset().top)
                            }, 'slow');

                            $.overlay.close();
                        })
                    }

                    $.publish('plugin/swProductCompareAdd/onAddArticleCompareSuccess', [ me, event, data, compareMenu ]);
                }
            });

            $.publish('plugin/swProductCompareAdd/onAddArticleCompare', [ me, event ]);
        },

        /** Destroys the plugin */
        destroy: function () {
            this.$el.off(this.getEventName('click'));

            this._destroy();
        }
    });
})(jQuery);

;(function($) {
    "use strict";

    /**
     * Shopware product Compare Plugin.
     *
     * The plugin controlls the topbar-navigation dropdown menu für product comparisons.
     */
    $.plugin('swProductCompareMenu', {

        /** Your default options */
        defaults: {
            /** @string compareMenuSelector HTML class for the topbarnavigation menu wrapper */
            compareMenuSelector: '.entry--compare',

            /** @string startCompareSelector HTML class for the start compare button */
            startCompareSelector: '.btn--compare-start',

            /** @string deleteCompareSelector HTML class for the cancel compare button */
            deleteCompareSelector: '.btn--compare-delete',

            /** @string deleteCompareItemSelector HTML class for delete single product from comparison */
            deleteCompareItemSelector: '.btn--item-delete',

            /** @string modalSelector HTML class for modal window */
            modalSelector: '.js--modal',

            /** @string modalContentInnerSelector HTML class for modal inner content */
            modalContentInnerSelector: '.modal--compare',

            /** @string compareEntriesSelector Selector for switching between single remove or full plugin reload */
            compareEntriesSelector: '.compare--list .compare--entry',

            /** @string compareEntry Selector for single compare item inside the dropdown */
            compareEntrySelector: '.compare--entry',

            /** @string hiddenCls Class which indicates that the element is hidden */
            hiddenCls: 'is--hidden'
        },

        /**
         * Initializes the plugin
         *
         * @returns {Plugin}
         */
        init: function () {
            var me = this,
                $compareMenu = $(me.opts.compareMenuSelector);

            if(!$compareMenu.is(':empty')) {
                $compareMenu.removeClass(me.opts.hiddenCls);
            }

            // on start compare
            me._on(me.opts.startCompareSelector, 'touchstart click', $.proxy(me.onStartCompare, me));

            // On cancel compare
            me._on(me.opts.deleteCompareSelector, 'touchstart click', $.proxy(me.onDeleteCompare, me));

            // On delete single product item from comparison
            me._on(me.opts.deleteCompareItemSelector, 'touchstart click', $.proxy(me.onDeleteItem, me));

            $.publish('plugin/swProductCompareMenu/onRegisterEvents', [ me ]);
        },

        /**
         * Opens the comparison modal by startCompareSelector.
         *
         * @public
         * @method onStartCompare
         */
        onStartCompare: function (event) {
            event.preventDefault();

            var me = this,
                startCompareBtn = me.$el.find(me.opts.startCompareSelector),
                modalUrl = startCompareBtn.attr('href'),
                modalTitle = startCompareBtn.attr('data-modal-title');

            $.overlay.open({
                closeOnClick: false
            });

            $.loadingIndicator.open({
                openOverlay: false
            });

            $.publish('plugin/swProductCompareMenu/onStartCompareBefore', [ me ]);

            // Load compare modal before opening modal box
            $.ajax({
                'url': modalUrl,
                'dataType': 'jsonp',
                'success': function(template) {
                    $.publish('plugin/swProductCompareMenu/onStartCompareSuccess', [ me, template ]);

                    $.loadingIndicator.close(function() {

                        $.modal.open(template, {
                            title: modalTitle,
                            sizing: 'content'
                        });

                        // Auto sizing for width
                        var templateWidth = $(me.opts.modalSelector).find(me.opts.modalContentInnerSelector).outerWidth();
                        $(me.opts.modalSelector).css('width', templateWidth);

                        picturefill();

                        // Resize every property row height to biggest height in cell
                        var maxRows = 0;
                        $(".entry--property").each(function () {
                            var row = ~~($(this).attr('data-property-row'));
                            if (row > maxRows) {
                                maxRows = row;
                            }
                        });

                        var maximumHeight,
                            rowSelector,
                            i = 1;

                        for( ; i <= maxRows; i++) {
                            rowSelector = '.entry--property[data-property-row="' + i + '"]';

                            maximumHeight = 0;
                            $(rowSelector).each(function () {
                                var rowHeight = $(this).height();

                                if (rowHeight > maximumHeight ) {
                                    maximumHeight = rowHeight;
                                }
                            });

                            $(rowSelector).height(maximumHeight);
                        }
                        $.publish('plugin/swProductCompareMenu/onStartCompareFinished', [ me, template ]);
                    });
                }
            });

            $.publish('plugin/swProductCompareMenu/onStartCompare', [ me ]);
        },

        /**
         * Cancel the compare
         *
         * @method onDeleteCompare
         */
        onDeleteCompare: function (event) {
            var me = this,
                $target = $(event.currentTarget),
                deleteCompareBtn = me.$el.find(me.opts.deleteCompareSelector),
                $form = deleteCompareBtn.closest('form'),
                $menu = $(me.opts.compareMenuSelector),
                deleteUrl;

            event.preventDefault();

            // @deprecated: Don't use anchors for action links. Use forms with method="post" instead.
            if ($target.attr('href')) {
                deleteUrl = $target.attr('href');
            } else {
                deleteUrl = $form.attr('action');
            }

            $.ajax({
                'url': deleteUrl,
                'dataType': 'jsonp',
                'success': function () {
                    $menu.empty().addClass(me.opts.hiddenCls);

                    $.publish('plugin/swProductCompareMenu/onDeleteCompareSuccess', [ me ]);
                }
            });

            $.publish('plugin/swProductCompareMenu/onDeleteCompare', [ me ]);
        },

        /**
         * Delete one product item from comparison
         *
         * @method onDeleteItem
         */
        onDeleteItem: function (event) {
            event.preventDefault();

            var me = this,
                $deleteBtn = $(event.currentTarget),
                $form = $deleteBtn.closest('form'),
                rowElement = $deleteBtn.closest(me.opts.compareEntrySelector),
                compareCount = $(me.opts.compareEntriesSelector).length,
                deleteUrl;

            // @deprecated: Don't use anchors for action links. Use forms with method="post" instead.
            if ($deleteBtn.attr('href')) {
                deleteUrl = $deleteBtn.attr('href');
            } else {
                deleteUrl = $form.attr('action');
            }

            if(compareCount > 1) {

                // slide up and remove product from unordered list
                rowElement.slideUp('fast', function() {
                    rowElement.remove();
                });

                // update compare counter
                $('.compare--quantity').html('(' + (compareCount - 1) + ')');

                // remove product silent in the background
                $.ajax({
                    'url': deleteUrl,
                    'dataType': 'jsonp',
                    'success': function (response) {
                        $.publish('plugin/swProductCompareMenu/onDeleteItemSuccess', [ me, response ]);
                    }
                });

            } else {
                // remove last product, reload full compare plugin
                $.ajax({
                    'url': deleteUrl,
                    'dataType': 'jsonp',
                    'success': function (response) {
                        $(me.opts.compareMenuSelector).empty().addClass(me.opts.hiddenCls);

                        //Reload compare menu plugin
                        $('*[data-product-compare-menu="true"]').swProductCompareMenu();

                        $.publish('plugin/swProductCompareMenu/onDeleteItemSuccess', [ me, response ]);
                    }
                });
            }

            $.publish('plugin/swProductCompareMenu/onDeleteItem', [ me, event, deleteUrl ]);
        },

        /** Destroys the plugin */
        destroy: function () {
            this._destroy();
        }
    });
})(jQuery);

;(function($, window) {
    'use strict';

    /**
     * Parses the given {@link url} parameter and extracts all query parameters. If the parameter is numeric
     * it will automatically based to a {@link Number} instead of a {@link String}.
     * @private
     * @param {String} url - Usually {@link window.location.href}
     * @returns {{}} Object with all extracted parameters
     */
    var parseQueryString = function(url) {
        var qparams = {},
            parts = (url || '').split('?'),
            qparts, qpart,
            i=0;

        if(parts.length <= 1){
            return qparams;
        }

        qparts = parts[1].split('&');
        for (i in qparts) {
            var key, value;

            qpart = qparts[i].split('=');
            key = decodeURIComponent(qpart[0])
            value = decodeURIComponent(qpart[1] || '');
            qparams[key] = ($.isNumeric(value) ? parseFloat(value, 10) : value);
        }

        return qparams;
    };

    $.plugin('swInfiniteScrolling', {

        defaults: {

            /** @bool enabled - enable or disable infinite scrolling plugin */
            'enabled': true,

            /** @string event - default "scroll" will be used for triggering this plugin */
            'eventName': 'scroll',

            /** @int categoryId - category id is used for generating ajax request */
            'categoryId': 0,

            /** @string pagingSelector - listing paging selector **/
            'pagingSelector': '.listing--paging',

            /** @string productBoxSelector - selector for single product boxes **/
            'productBoxSelector': '.product--box',

            /** @string defaultPerPageSelector - default per page selector which will be removed **/
            'defaultPerPageSelector': '.action--per-page',

            /** @string defaultChangeLayoutSelector - default change layout selecot which will be get a new margin **/
            'defaultChangeLayoutSelector': '.action--change-layout',

            /** @int threshold - after this threshold reached, auto fetching is disabled and the "load more" button is shown. */
            'threshold': 3,

            /** @string loadMoreCls - this class will be used for fetching further data by button. */
            'loadMoreCls': 'js--load-more',

            /** @string loadPreviousCls - this class  will be used for fetching previous data by button. */
            'loadPreviousCls': 'js--load-previous',

            /** @string Class will be used for load more or previous button */
            'loadBtnCls': 'btn is--primary is--icon-right',

            /** @string loadMoreSnippet - this snippet will be printed inside the load more button */
            'loadMoreSnippet': 'Weitere Artikel laden',

            /** @string loadPreviousSnippet - this snippet will be printed inside the load previous button */
            'loadPreviousSnippet': 'Vorherige Artikel laden',

            /** @string listingContainerSelector - will be used for prepending and appending load previous and load more button */
            'listingContainerSelector': '.listing--container',

            /** @string pagingBottomSelector - this class will be used for removing the bottom paging bar if infinite scrolling is enabled */
            'pagingBottomSelector': '.listing--bottom-paging',

            /** @string listingActionsWrapper - this class will be cloned and used as a actions wrapper for the load more and previous button */
            'listingActionsWrapper': 'infinite--actions',

            /** @string ajaxUrl - this string will be used as url for the ajax-call to load the articles */
            ajaxUrl: window.controller.ajax_listing || null
        },

        /**
         * Default plugin initialisation function.
         * Handle all logic and events for infinite scrolling
         *
         * @public
         * @method init
         */
        init: function() {
            var me = this,
                $body = $('body');

            // Overwrite plugin configuration with user configuration
            me.applyDataAttributes();

            // Check if plugin is enabled
            if(!me.opts.enabled || !me.$el.is(':visible') || !me.opts.categoryId || me.opts.ajaxUrl === null) {
                return;
            }

            // Remove paging top bar
            $(me.opts.pagingSelector).remove();

            // remove bottom paging bar
            $(me.opts.pagingBottomSelector).remove();

            // Check max pages by data attribute
            me.maxPages = me.$el.attr('data-pages');
            if(me.maxPages <= 1) {
                return;
            }

            // isLoading state for preventing double fetch same content
            me.isLoading = false;

            // isFinished state for disabling plugin if all pages rendered
            me.isFinished = false;

            // resetting fetch Count to prevent auto fetching after threshold reached
            me.fetchCount = 0;

            // previosPageIndex for loading in other direction
            me.previousPageIndex = 0;

            // Prepare top and bottom actions containers
            me.buttonWrapperTop = $('<div>', {
                'class': me.opts.listingActionsWrapper
            });

            me.buttonWrapperBottom = $('<div>', {
                'class': me.opts.listingActionsWrapper
            });

            // append load more button
            $(me.opts.listingContainerSelector).after(me.buttonWrapperBottom);
            $(me.opts.listingContainerSelector).before(me.buttonWrapperTop);

            // base url for push state and ajax fetch url
            me.baseUrl = window.location.href.split('?')[0];

            // Ajax configuration
            me.ajax = {
                'url': me.opts.ajaxUrl,
                'params': parseQueryString(window.location.href)
            };

            me.params = parseQueryString(window.location.href);
            me.upperParams = $.extend({}, me.params);
            me.historyParams = $.extend({}, me.params);

            me.urlBasicMode = false;

            // if no seo url is provided, use the url basic push mode
            if(!me.params.p) {

                me.basicModeSegments = window.location.pathname.split("/");
                me.basicModePageKey = $.inArray('sPage', me.basicModeSegments);
                me.basicModePageValue = me.basicModeSegments[ me.basicModePageKey +1];

                if(me.basicModePageValue) {
                    me.urlBasicMode = true;
                    me.params.p = me.basicModePageValue;
                    me.upperParams.p = me.basicModePageValue;
                }
            }

            // set page index to one if not assigned
            if(!me.params.p) {
                me.params.p = 1;
            }

            // set start page
            me.startPage = me.params.p;

            // holds the current listing url with all params
            me.currentPushState = '';

            // Check if there is/are previous pages
            if(me.params.p && me.params.p > 1) {
                me.showLoadPrevious();
            }

            // on scrolling event for auto fetching new pages and push state
            me._on(window, me.opts.eventName, $.proxy(me.onScrolling, me));

            // on load more button event for manually fetching further pages
            var loadMoreSelector = '.' + me.opts.loadMoreCls;
            $body.delegate(loadMoreSelector, 'click', $.proxy(me.onLoadMore, me));

            // on load previous button event for manually fetching previous pages
            var loadPreviousSelector = '.' + me.opts.loadPreviousCls;
            $body.delegate(loadPreviousSelector, 'click', $.proxy(me.onLoadPrevious, me));

            $.publish('plugin/swInfiniteScrolling/onRegisterEvents', [ me ]);
        },

        update: function () {
            var me = this;

            // disable infinite scrolling, because listing container is not visible
            me.opts.enabled = me.$el.is(':visible');

            $.publish('plugin/swInfiniteScrolling/onUpdate', [ me ]);
        },

        /**
         * onScrolling method
         */
        onScrolling: function() {
            var me = this;

            // stop fetch new page if is loading atm
            if(me.isLoading || !me.opts.enabled) {
                return;
            }

            // Viewport height
            var $window = $(window),
                docTop = $window.scrollTop() + $window.height(),

                // Get last element in list to get the reference point for fetching new data
                fetchPoint = me.$el.find(me.opts.productBoxSelector).last(),
                fetchPointOffset = fetchPoint.offset().top,
                bufferSize = fetchPoint.height(),
                triggerPoint = fetchPointOffset - bufferSize;

            if(docTop > triggerPoint && (me.params.p < me.maxPages)) {
                me.fetchNewPage();
            }

            // collect all pages
            var $products = $('*[data-page-index]'),
                visibleProducts = $.grep($products, function(item) {
                return $(item).offset().top <= docTop;
            });

            // First visible Product
            var $firstProduct = $(visibleProducts).last(),
                tmpPageIndex = $firstProduct.attr('data-page-index');

            // Collection variables and build push state url
            var tmpParams = me.historyParams;

            // remove category id from history url
            delete tmpParams.c;

            // setting actual page index
            if(!tmpParams.p || !tmpPageIndex) {
                tmpParams.p = me.startPage;
            }

            if(tmpPageIndex) {
                tmpParams.p = tmpPageIndex;
            }

            var tmpPushState = me.baseUrl + '?' + $.param(tmpParams);

            if(me.urlBasicMode) {
                // use start page parameter if no one exists
                if(!tmpPageIndex) {
                    tmpPageIndex = me.basicModePageValue;
                }

                // redesign push url,
                var segments = me.basicModeSegments;
                segments[me.basicModePageKey+1] = tmpPageIndex;

                tmpPushState = segments.join('/');
            }

            if(me.currentPushState != tmpPushState) {

                me.currentPushState = tmpPushState;
                if(!history || !history.pushState) {
                    return;
                }

                history.pushState('data', '', me.currentPushState);
            }

            $.publish('plugin/swInfiniteScrolling/onScrolling', [ me ]);
        },

        /**
         * fetchNewPage method
         */
        fetchNewPage: function() {
            var me = this;

            // Quit here if all pages rendered
            if(me.isFinished || me.params.p >= me.maxPages) {
                return;
            }

            // stop if process is running
            if(me.isLoading) {
                return;
            }

            // Stop automatic fetch if page threshold reached
            if(me.fetchCount >= me.opts.threshold) {
                var button = me.generateButton('next');

                // append load more button
                me.buttonWrapperBottom.html(button);

                // set finished flag
                me.isFinished = true;

                return;
            }

            me.isLoading = true;

            me.openLoadingIndicator();

            // add parameter to see whether the next page or a previous page has been loaded
            me.params.mode = 'next';

            // increase page index for further page loading
            me.params.p ++;

            // increase fetch count for preventing auto fetching
            me.fetchCount++;

            // use categoryid by settings if not defined by filters
            if(!me.params.c) me.params.c = me.opts.categoryId;

            $.publish('plugin/swInfiniteScrolling/onBeforeFetchNewPage', [ me ]);

            // generate ajax fefch url by all params
            var url = me.ajax.url + '?' + $.param(me.params);

            $.get(url, function(data) {
                var template = data.trim();

                $.publish('plugin/swInfiniteScrolling/onFetchNewPageLoaded', [ me, template ]);

                // Cancel is no data provided
                if(!template) {
                    me.isFinished = true;

                    me.closeLoadingIndicator();
                    return;
                }

                // append fetched data into listing
                me.$el.append(template);

                // trigger picturefill for regenerating thumbnail sizes
                picturefill();

                me.closeLoadingIndicator();

                // enable loading for further pages
                me.isLoading = false;

                // check if last page reached
                if(me.params.p >= me.maxPages) {
                    me.isFinished = true;
                }

                $.publish('plugin/swInfiniteScrolling/onFetchNewPageFinished', [ me, template ]);
            });

            $.publish('plugin/swInfiniteScrolling/onFetchNewPage', [ me ]);
        },

        generateButton: function(buttonType) {
            var me = this,
                type = buttonType || 'next',
                cls = (type == 'previous') ? me.opts.loadPreviousCls : me.opts.loadMoreCls,
                snippet = (type == 'previous') ? me.opts.loadPreviousSnippet : me.opts.loadMoreSnippet,
                $button = $('<a>', {
                    'class': me.opts.loadBtnCls + ' ' + cls,
                    'html': snippet + ' <i class="icon--cw is--large"></i>'
                });

            $.publish('plugin/swInfiniteScrolling/onLoadMore', [ me, $button, buttonType ]);

            return $button;
        },

        /**
         * onLoadMore method
         *
         * @param event
         */
        onLoadMore: function(event) {
            event.preventDefault();

            var me = this;

            // Remove load more button
            $('.' + me.opts.loadMoreCls).remove();

            // Set finished to false to reanable the fetch method
            me.isFinished = false;

            // Increase threshold for auto fetch next page if there is a next page
            if(me.maxPages >= me.opts.threshold) {
                me.opts.threshold++;
            }

            // fetching new page
            me.fetchNewPage();

            $.publish('plugin/swInfiniteScrolling/onLoadMore', [ me, event ]);
        },

        /**
         * showLoadPrevius method
         *
         * Shows the load previous button
         */
        showLoadPrevious: function() {
            var me = this,
                button = me.generateButton('previous');

            // append load previous button
            me.buttonWrapperTop.html(button);

            $.publish('plugin/swInfiniteScrolling/onShowLoadPrevious', [ me, button ]);
        },

        /**
         * onLoadPrevious method
         *
         * @param event
         *
         * will be triggered by load previous button
         */
        onLoadPrevious: function(event) {
            event.preventDefault();

            var me = this;

            // Remove load previous button
            $('.' + me.opts.loadPreviousCls).remove();

            // fetching new page
            me.openLoadingIndicator('top');

            // build ajax url
            var tmpParams = me.upperParams;

            // use categoryid by settings if not defined by filters
            if(!tmpParams.c) tmpParams.c = me.opts.categoryId;

            tmpParams.p = tmpParams.p - 1;

            // add parameter to see whether the next page or a previous page has been loaded
            tmpParams.mode = 'previous';

            $.publish('plugin/swInfiniteScrolling/onBeforeFetchPreviousPage', [ me ]);

            // generate ajax fefch url by all params
            var url = me.ajax.url + '?' + $.param(tmpParams);

            $.get(url, function(data) {
                var template = data.trim();

                // append fetched data into listing
                me.$el.prepend(template);

                picturefill();

                me.closeLoadingIndicator();

                // enable loading for further pages
                me.isLoading = false;

                // Set load previous button if we aren't already on page one
                if(tmpParams.p > 1) {
                    me.showLoadPrevious();
                }

                $.publish('plugin/swInfiniteScrolling/onLoadPreviousFinished', [ me, event, data ]);
            });

            $.publish('plugin/swInfiniteScrolling/onLoadPrevious', [ me, event ]);
        },

        /**
         * openLoadingIndicator method
         *
         * opens the loading indicator relative
         */
        openLoadingIndicator: function(type) {
            var me = this,
                $indicator = $('.js--loading-indicator.indicator--relative');

            if($indicator.length) {
                return;
            }

            $indicator = $('<div>', {
                'class': 'js--loading-indicator indicator--relative',
                'html': $('<i>', {
                    'class': 'icon--default'
                })
            });

            if(!type) {
                me.$el.parent().after($indicator);
            } else {
                me.$el.parent().before($indicator);
            }

            $.publish('plugin/swInfiniteScrolling/onOpenLoadingIndicator', [ me, $indicator ]);
        },

        /**
         * closeLoadingIndicator method
         *
         * close the relative loading indicator
         */
        closeLoadingIndicator: function() {
            var me = this,
                $indicator = $('.js--loading-indicator.indicator--relative');

            if(!$indicator.length) {
                return;
            }

            $indicator.remove();

            $.publish('plugin/swInfiniteScrolling/onCloseLoadingIndicator', [ me ]);
        }
    });
})(jQuery, window);
;(function ($) {
    'use strict';

    /**
     * Shopware Menu Scroller Plugin
     */
    $.plugin('swOffcanvasButton', {

        /**
         * Default options for the offcanvas button plugin
         *
         * @public
         * @property defaults
         * @type {Object}
         */
        defaults: {

            /**
             * CSS selector for the element listing
             *
             * @type {String}
             */
            pluginClass: 'js--off-canvas-button',

            /**
             * CSS class which will be added to the wrapper / this.$el
             *
             * @type {String}
             */
            contentSelector: '.offcanvas--content',

            /**
             * Selector for the closing button
             *
             * @type {String}
             */
            closeButtonSelector: '.close--off-canvas',

            /**
             * CSS class which will be added to the listing
             *
             * @type {Boolean}
             */
            fullscreen: true
        },

        /**
         * Default plugin initialisation function.
         * Sets all needed properties, creates the slider template
         * and registers all needed event listeners.
         *
         * @public
         * @method init
         */
        init: function () {
            var me = this,
                $el = me.$el,
                opts = me.opts;

            me.applyDataAttributes();

            $el.addClass(opts.pluginClass);

            $el.swOffcanvasMenu({
                'direction': 'fromRight',
                'offCanvasSelector': $el.find(opts.contentSelector),
                'fullscreen': opts.fullscreen,
                'closeButtonSelector': opts.closeButtonSelector
            });
        },

        /**
         * Removed all listeners, classes and values from this plugin.
         *
         * @public
         * @method destroy
         */
        destroy: function () {
            var me = this,
                $el = me.$el,
                plugin = $el.data('plugin_swOffcanvasMenu');

            if (plugin) {
                plugin.destroy();
            }

            $el.removeClass(me.opts.pluginClass);

            me._destroy();
        }
    });
}(jQuery));

;(function ($, Modernizr) {
    'use strict';

    /**
     * Sub Category Navigation plugin
     *
     * The plugin provides an category slider inside the off canvas menu. The categories and sub categories
     * could be fetched by ajax calls and uses a CSS3 `transitions` to slide in or out. The main sidebar will not
     * be overwritten. The categories slider plugin uses two overlays to interact.
     *
     * @example usage
     * ```
     *    <div data-subcategory-nav="true"
     *      data-mainCategoryId="{$Shop->get('parentID')}"
     *      data-categoryId="{$sCategoryContent.id}"
     *      data-fetchUrl="{url module=widgets controller=listing action=getCategory categoryId={$sCategoryContent.id}}"></div>
     *
     *    $('*[data-subcategory-nav="true"]').swSubCategoryNav();
     * ```
     */
    $.plugin('swSubCategoryNav', {

        defaults: {

            /**
             * Whether or not the plugin is enabled or not.
             *
             * @property enabled
             * @type {Boolean}
             */
            'enabled': true,

            /**
             * Event name(s) used for registering the events to navigate
             *
             * @property eventName
             * @type {String}
             */
            'eventName': 'click',

            /**
             * Selector for a single navigation
             *
             * @property sidebarCategorySelector
             * @type {String}
             */
            'sidebarCategorySelector': '.sidebar--navigation',

            /**
             * Selector for the back buttons.
             *
             * @property backwardsSelector
             * @type {String}
             */
            'backwardsSelector': '.link--go-back',

            /**
             * Selector for the forward buttons.
             *
             * @property forwardSelector
             * @type {String}
             */
            'forwardsSelector': '.link--go-forward',

            /**
             * Selector for the main menu buttons.
             *
             * @property mainMenuSelector
             * @type {String}
             */
            'mainMenuSelector': '.link--go-main',

            /**
             * Selector for the wrapper of the sidebar navigation.
             * This wrapper will contain the main menu.
             *
             * @property sidebarWrapperSelector
             * @type {String}
             */
            'sidebarWrapperSelector': '.sidebar--categories-wrapper',

            /**
             * ID of the root category ID of the current shop.
             * This is used to determine if the user switches to the main
             * menu when clicking on a back button.
             *
             * @property mainCategoryId
             * @type {Number}
             */
            'mainCategoryId': null,

            /**
             * Category ID of the current page.
             * When this and fetchUrl is set, the correct slide will be loaded.
             *
             * @property categoryId
             * @type {Number}
             */
            'categoryId': null,

            /**
             * URL to get the current navigation slide.
             * When this and categoryID is set, the correct slide will be loaded.
             *
             * @property fetchUrl
             * @type {String}
             */
            'fetchUrl': '',

            /**
             * Selector for a overlay navigation slide.
             *
             * @property overlaySelector
             * @type {String}
             */
            'overlaySelector': '.offcanvas--overlay',

            /**
             * Selector for the whole sidebar itself.
             *
             * @property sidebarMainSelector
             * @type {String}
             */
            'sidebarMainSelector': '.sidebar-main',

            /**
             * Selector for the mobile navigation.
             *
             * @property mobileNavigationSelector
             * @type {String}
             */
            'mobileNavigationSelector': '.navigation--smartphone',

            /**
             * Loading class for the ajax calls.
             * This class will be used for a loading item.
             * This item will be appended to the clicked navigation item.
             *
             * @property loadingClass
             * @type {String}
             */
            'loadingClass': 'sidebar--ajax-loader',

            /**
             * Class that determines the existing slides to remove
             * them if no longer needed.
             *
             * @property backSlideClass
             * @type {String}
             */
            'backSlideClass': 'background',

            /**
             * Selector for the right navigation icon.
             * This icon will be hidden and replaced with the loading icon.
             *
             * @property iconRightSelector
             * @type {String}
             */
            'iconRightSelector': '.is--icon-right',

            /**
             * Class that will be appended to the main sidebar to
             * disable the scrolling functionality.
             *
             * @property disableScrollingClass
             * @type {String}
             */
            'disableScrollingClass': 'is--inactive',

            /**
             * Speed of the slide animations in milliseconds.
             *
             * @property animationSpeedIn
             * @type {Number}
             */
            'animationSpeedIn': 450,

            /**
             * Speed of the slide animations in milliseconds.
             *
             * @property animationSpeedOut
             * @type {Number}
             */
            'animationSpeedOut': 300,

            /**
             * Easing function for sliding a slide into the viewport.
             *
             * @property easingIn
             * @type {String}
             */
            'easingIn': 'cubic-bezier(.3,0,.15,1)',

            /**
             * Easing function for sliding a slide out of the viewport.
             *
             * @property easingOut
             * @type {String}
             */
            'easingOut': 'cubic-bezier(.02, .01, .47, 1)',

            /**
             * The animation easing used when transitions are not supported.
             *
             * @property easingFallback
             * @type {String}
             */
            'easingFallback': 'swing'
        },

        /**
         * Default plugin initialisation function.
         * Handle all logic and events for the category slider
         *
         * @public
         * @method init
         */
        init: function () {
            var me = this,
                transitionSupport = Modernizr.csstransitions,
                opts;

            // Overwrite plugin configuration with user configuration
            me.applyDataAttributes();

            opts = me.opts;

            // return, if no main category available
            if (!opts.enabled || !opts.mainCategoryId) {
                return;
            }

            /**
             * Reference of the main sidebar element.
             *
             * @private
             * @property $sidebar
             * @type {jQuery}
             */
            me.$sidebar = $(opts.sidebarMainSelector);

            /**
             * Wrapper of the navigation lists in the main navigation.
             *
             * @private
             * @property $sidebarWrapper
             * @type {jQuery}
             */
            me.$sidebarWrapper = $(opts.sidebarWrapperSelector);

            /**
             * Wrapper of the offcanvas animation
             *
             * @private
             * @property $navigation
             * @type {jQuery}
             */
            me.$navigation = $(opts.mobileNavigationSelector);
            me.$navigation.show();

            /**
             * Loading icon element that will be appended to the
             * clicked element on loading.
             *
             * @private
             * @property $loadingIcon
             * @type {jQuery}
             */
            me.$loadingIcon = $('<div>', {
                'class': opts.loadingClass
            });

            /**
             * Function used in jQuery based on CSS transition support.
             *
             * @private
             * @property slideFunction
             * @type {String}
             */
            me.slideFunction = transitionSupport ? 'transition' : 'animate';

            /**
             * Easing used for the slide in.
             *
             * @private
             * @property easingEffectIn
             * @type {String}
             */
            me.easingEffectIn = transitionSupport ? opts.easingIn : opts.easingFallback;

            /**
             * Easing used for the slide out.
             *
             * @private
             * @property easingEffectOut
             * @type {String}
             */
            me.easingEffectOut = transitionSupport ? opts.easingOut : opts.easingFallback;

            /**
             * Flag to determine whether or not a slide is in a current
             * animation or if an ajax call is still loading.
             *
             * @private
             * @property inProgress
             * @type {Boolean}
             */
            me.inProgress = false;

            // remove sub level unordered lists
            $(opts.sidebarCategorySelector + ' ul').not('.navigation--level-high').css('display', 'none');

            me.addEventListener();

            // fetch menu by category id if actual category is not the main category
            if (!opts.categoryId || !opts.fetchUrl || (opts.mainCategoryId == opts.categoryId)) {
                return;
            }

            $.get(opts.fetchUrl, function (template) {

                me.$sidebarWrapper.css('display', 'none');

                me.$sidebar.addClass(opts.disableScrollingClass).append(template);

                // add background class
                $(opts.overlaySelector).addClass(opts.backSlideClass);
            });
        },

        /**
         * Registers all needed event listeners.
         *
         * @public
         * @method addEventListener
         */
        addEventListener: function () {
            var me = this,
                opts = me.opts,
                $sidebar = me.$sidebar,
                eventName = opts.eventName;

            $sidebar.on(me.getEventName(eventName), opts.backwardsSelector, $.proxy(me.onClickBackButton, me));

            $sidebar.on(me.getEventName(eventName), opts.forwardsSelector, $.proxy(me.onClickForwardButton, me));

            $sidebar.on(me.getEventName(eventName), opts.mainMenuSelector, $.proxy(me.onClickMainMenuButton, me));

            $.publish('plugin/swSubCategoryNav/onRegisterEvents', [ me ]);
        },

        /**
         * Called when clicked on a back button.
         * Loads the overlay based on the parent id and fetch url.
         * When the no fetch url is available or the parent id is the same
         * as the main menu one, the slideToMainMenu function will be called.
         *
         * @public
         * @method onClickBackButton
         * @param {Object} event
         */
        onClickBackButton: function (event) {
            event.preventDefault();

            var me = this,
                $target = $(event.target),
                url = $target.attr('href'),
                parentId = ~~$target.attr('data-parentId');

            if (me.inProgress) {
                return;
            }

            me.inProgress = true;

            $.publish('plugin/swSubCategoryNav/onClickBackButton', [ me, event ]);

            // decide if there is a parent group or main sidebar
            if (!url || parentId === me.opts.mainCategoryId) {
                me.slideToMainMenu();
                return;
            }

            me.loadTemplate(url, me.slideOut, $target);
        },

        /**
         * Called when clicked on a forward button.
         * Loads the overlay based on the category id and fetch url.
         *
         * @public
         * @method onClickForwardButton
         * @param {Object} event
         */
        onClickForwardButton: function (event) {
            event.preventDefault();

            var me = this,
                $target = $(event.currentTarget),
                url = $target.attr('data-fetchUrl');

            if (me.inProgress) {
                return;
            }

            me.inProgress = true;

            $.publish('plugin/swSubCategoryNav/onClickForwardButton', [ me, event ]);

            // Disable scrolling on main menu
            me.$sidebar.addClass(me.opts.disableScrollingClass);

            me.loadTemplate(url, me.slideIn, $target);
        },

        /**
         * Called when clicked on a main menu button.
         * Calls the slideToMainMenu function.
         *
         * @public
         * @method onClickMainMenuButton
         * @param {Object} event
         */
        onClickMainMenuButton: function (event) {
            event.preventDefault();

            var me = this;

            if (me.inProgress) {
                return;
            }

            me.inProgress = true;

            $.publish('plugin/swSubCategoryNav/onClickMainMenuButton', [ me, event ]);

            me.slideToMainMenu();
        },

        /**
         * loads a template via ajax call
         *
         * @public
         * @method loadTemplate
         * @param {String} url
         * @param {Function} callback
         * @param {jQuery} $loadingTarget
         */
        loadTemplate: function (url, callback, $loadingTarget) {
            var me = this;

            $.publish('plugin/swSubCategoryNav/onLoadTemplateBefore', [ me ]);

            if (!$loadingTarget) {
                $.get(url, function (template) {
                    $.publish('plugin/swSubCategoryNav/onLoadTemplate', [ me ]);

                    callback.call(me, template)
                });
                return;
            }

            $loadingTarget.find(me.opts.iconRightSelector).fadeOut('fast');

            $loadingTarget.append(me.$loadingIcon);

            me.$loadingIcon.fadeIn();

            $.get(url, function (template) {
                me.$loadingIcon.hide();

                $.publish('plugin/swSubCategoryNav/onLoadTemplate', [ me ]);

                callback.call(me, template);
            });
        },

        /**
         * Sliding out the first level overlay and removes the slided overlay.
         *
         * @public
         * @method slideOut
         * @param {String} template
         */
        slideOut: function (template) {
            var me = this,
                opts = me.opts,
                $overlays,
                $slide;

            $.publish('plugin/swSubCategoryNav/onSlideOutBefore', [ me ]);

            me.$sidebar.append(template);

            // get all overlays
            $overlays = $(opts.overlaySelector);

            // flip background classes
            $overlays.toggleClass(opts.backSlideClass);

            $slide = $overlays.not('.' + opts.backSlideClass);

            $slide[me.slideFunction]({ 'left': 280 }, opts.animationSpeedOut, me.easingEffectOut, function () {
                $slide.remove();

                me.inProgress = false;

                $.publish('plugin/swSubCategoryNav/onSlideOut', [ me ]);
            });
        },

        /**
         * Slides a given template/slide into the viewport of the sidebar.
         * After the sliding animation is finished,
         * the previous slide will be removed.
         *
         * @public
         * @method slideIn
         * @param {String} template
         */
        slideIn: function (template) {
            var me = this,
                opts = me.opts,
                $overlays,
                $slide,
                $el;

            $.publish('plugin/swSubCategoryNav/onSlideInBefore', [ me ]);

            // hide main menu
            me.$sidebar.scrollTop(0);

            me.$sidebar.append(template);

            $overlays = $(opts.overlaySelector);

            $slide = $overlays.not('.' + opts.backSlideClass).css({
                'left': 280,
                'display': 'block'
            });

            $slide[me.slideFunction]({ 'left': 0 }, opts.animationSpeedIn, me.easingEffectIn, function () {
                // remove background layer
                $overlays.each(function (i, el) {
                    $el = $(el);

                    if ($el.hasClass(opts.backSlideClass)) {
                        $el.remove();
                    }
                });

                $slide.addClass(opts.backSlideClass);

                // hide main menu
                me.$sidebarWrapper.css('display', 'none');

                me.$navigation.hide().show(0);

                $slide.addClass(opts.backSlideClass);

                me.inProgress = false;

                $.publish('plugin/swSubCategoryNav/onSlideIn', [ me ]);
            });
        },

        /**
         * Slides all overlays out of the viewport and removes them.
         * That way the main menu will be uncovered.
         *
         * @public
         * @method slideToMainMenu
         */
        slideToMainMenu: function () {
            var me = this,
                opts = me.opts,
                $overlay = $(opts.overlaySelector);

            $.publish('plugin/swSubCategoryNav/onSlideToMainMenuBefore', [ me ]);

            // make the main menu visible
            me.$sidebarWrapper.css('display', 'block');

            // fade in arrow icons
            me.$sidebarWrapper.find(me.opts.iconRightSelector).fadeIn('slow');

            $overlay[me.slideFunction]({ 'left': 280 }, opts.animationSpeedOut, me.easingEffectOut, function () {
                $overlay.remove();

                // enable scrolling on main menu
                me.$sidebar.removeClass(opts.disableScrollingClass);

                me.inProgress = false;

                $.publish('plugin/swSubCategoryNav/onSlideToMainMenu', [ me ]);
            });
        },

        /**
         * Destroys the plugin by removing all events and references
         * of the plugin.
         * Resets all changed CSS properties to default.
         *
         * @public
         * @method destroy
         */
        destroy: function () {
            var me = this,
                opts = me.opts,
                $sidebar = me.$sidebar,
                $sidebarWrapper = me.$sidebarWrapper;

            if ($sidebar) {
                $sidebar.off(me.getEventName(opts.eventName), '**');
            }

            me.$navigation.hide();

            // make category children visible
            $(opts.sidebarCategorySelector + ' ul').not('.navigation--level-high').css('display', 'block');

            // force sidebar to be shown
            if ($sidebarWrapper) {
                me.$sidebarWrapper.css('display', 'block');
            }

            // clear overlay
            $(opts.overlaySelector).remove();

            me._destroy();
        }
    });
}(jQuery, Modernizr));

;(function($, window, undefined) {
    'use strict';

    /**
     * AJAX wishlist plugin
     *
     * The plugin provides the ability to add products to the notepad using AJAX. The benefit
     * using AJAX is that the user doesn't get a page reload and therefor remains at the
     * exact same spot on the page.
     *
     * @example
     * <div class="container" data-ajax-wishlist="true">
     *     ...lots of data
     *     <a href="action--note" data-text="Saved">Note it</a>
     * </div>
     */
    $.plugin('swAjaxWishlist', {

        /** @object Default configuration */
        defaults: {

            /**
             * The DOM selector for the counter.
             *
             * @property counterSelector
             * @type {String}
             */
            counterSelector: '.notes--quantity',

            /**
             * The DOM selector for the wishlist link.
             *
             * @property wishlistSelector
             * @type {String}
             */
            wishlistSelector: '.entry--notepad',

            /**
             * The css class for the check icon.
             *
             * @property iconCls
             * @type {String}
             */
            iconCls: 'icon--check',

            /**
             * The css class for the saved state.
             *
             * @property savedCls
             * @type {String}
             */
            savedCls: 'js--is-saved',

            /**
             * The snippet text for the saved state.
             *
             * @property text
             * @type {String}
             */
            text: 'Gemerkt',

            /**
             * Delay of the toggle back animation of the button
             *
             * @property delay
             * @type {Number}
             */
            delay: 1500
        },

        /**
         * Initializes the plugin
         */
        init: function() {
            var me = this;

            me.applyDataAttributes();

            me.$wishlistButton = $(me.opts.wishlistSelector);
            me.$counter = $(me.opts.counterSelector);

            me.registerEvents();
        },

        /**
         * Registers the necessary event listeners for the plugin
         */
        registerEvents: function() {
            var me = this;

            me.$el.on(me.getEventName('click'), '.action--note, .link--notepad', $.proxy(me.triggerRequest, me));

            $.publish('plugin/swAjaxWishlist/onRegisterEvents', [ me ]);
        },

        /**
         * Event listener handler which will be called when the user clicks on the associated element.
         *
         * The handler triggers an AJAX call to add a product to the notepad.
         *
         * @param {object} event - event object
         */
        triggerRequest: function(event) {
            var me = this,
                $target = $(event.currentTarget),
                url = $target.attr('data-ajaxUrl');

            if (url == undefined || $target.hasClass(me.opts.savedCls)) {
                return;
            }

            event.preventDefault();

            $.ajax({
                'url': url,
                'dataType': 'jsonp',
                'success': $.proxy(me.responseHandler, me, $target)
            });

            $.publish('plugin/swAjaxWishlist/onTriggerRequest', [ me, event, url ]);
        },

        /**
         * Handles the server response and terminates if the AJAX was successful,
         * updates the counter in the head area of the store front and
         * triggers the animation of the associated element.
         *
         * @param {object} $target - The associated element
         * @param {String} json - The ajax response as a JSON string
         */
        responseHandler: function($target, json) {
            var me = this,
                response = JSON.parse(json);

            $.publish('plugin/swAjaxWishlist/onTriggerRequestLoaded', [ me, $target, response ]);

            if (!response.success) {
                return;
            }

            me.updateCounter(response.notesCount);
            me.animateElement($target);

            $.publish('plugin/swAjaxWishlist/onTriggerRequestFinished', [ me, $target, response ]);
        },

        /**
         * Animates the element when the AJAX request was successful.
         *
         * @param {object} $target - The associated element
         */
        animateElement: function($target) {
            var me = this,
                $icon = $target.find('i'),
                originalIcon = $icon[0].className,
                $text = $target.find('.action--text'),
                originalText = $text.html();

            $target.addClass(me.opts.savedCls);
            $text.html($target.attr('data-text') || me.opts.text);
            $icon.removeClass(originalIcon).addClass(me.opts.iconCls);

            window.setTimeout(function() {
                $target.removeClass(me.opts.savedCls);
                $text.html(originalText);
                $icon.removeClass(me.opts.iconCls).addClass(originalIcon);

                $.publish('plugin/swAjaxWishlist/onAnimateElementFinished', [ me, $target ]);
            }, me.opts.delay);

            $.publish('plugin/swAjaxWishlist/onAnimateElement', [ me, $target ]);
        },

        /**
         * Updates the wishlist badge counter. If the badge isn't available,
         * it will be created on runtime and nicely showed with a transition.
         *
         * @param {String|Number} count
         * @returns {*|HTMLElement|$counter}
         */
        updateCounter: function (count) {
            var me = this,
                $btn = me.$wishlistButton,
                animate = 'transition';

            if(me.$counter.length) {
                me.$counter.html(count);
                return me.$counter;
            }

            // Initial state don't has the badge, so we need to create it
            me.$counter = $('<span>', {
                'class': 'badge notes--quantity',
                'html': count,
                'css': { 'opacity': 0 }
            }).appendTo($btn.find('a'));

            if (!$.support.transition) {
                animate = 'animate';
            }

            // Show it with a nice transition
            me.$counter[animate]({
                'opacity': 1
            }, 500);

            $.publish('plugin/swAjaxWishlist/onUpdateCounter', [ me, me.$counter, count ]);

            return me.$counter;
        },

        /**
         * Destroys the plugin
         */
        destroy: function() {
            var me = this;

            me.$el.off(me.getEventName('click'));
        }
    });
})(jQuery, window);

;(function($, window, undefined) {
    'use strict';

    /**
     * Simple plugin which replaces the button with a loading indicator to prevent multiple clicks on the
     * same button.
     *
     * @example
     * <button type="submit" data-preloader-button="true">Submit me!</button>
     */
    $.plugin('swPreloaderButton', {

        /** @object Default configuration */
        defaults: {

            /** @string CSS class for the loading indicator */
            loaderCls: 'js--loading',

            /** @boolean Truthy, if the button is attached to a form which needs to be valid before submitting  */
            checkFormIsValid: true
        },

        /**
         * Initializes the plugin
         */
        init: function() {
            var me = this;

            me.applyDataAttributes();

            me.opts.checkFormIsValid = me.opts.checkFormIsValid && me.checkForValiditySupport();
            me._on(me.$el, 'click', $.proxy(me.onShowPreloader, me));

            $.publish('plugin/swPreloaderButton/onRegisterEvents', [ me ]);
        },

        /**
         * Checks if the browser supports HTML5 form validation
         * on form elements.
         *
         * @returns {boolean}
         */
        checkForValiditySupport: function() {
            var me = this,
                element = document.createElement('input'),
                valid = (typeof element.validity === 'object');

            $.publish('plugin/swPreloaderButton/onCheckForValiditySupport', [ me, valid ]);

            return valid;
        },

        /**
         * Event handler method which will be called when the user clicks on the
         * associated element.
         */
        onShowPreloader: function() {
            var me = this;

            if(me.opts.checkFormIsValid) {
                var $form = $('#' + me.$el.attr('form'));

                if (!$form.length) {
                    $form = me.$el.parents('form');
                }

                if (!$form.length || !$form[0].checkValidity()) {
                    return;
                }
            }

            //... we have to use a timeout, otherwise the element will not be inserted in the page.
            window.setTimeout(function() {
                me.$el.html(me.$el.text() + '<div class="' + me.opts.loaderCls + '"></div>').attr('disabled', 'disabled');

                $.publish('plugin/swPreloaderButton/onShowPreloader', [ me ]);
            }, 25);
        },

        /**
         * Removes the loading indicator and re-enables the button
         */
        reset: function() {
            var me = this;

            me.$el.find('.' + me.opts.loaderCls).removeAttr('disabled').remove();
        }
    });
})(jQuery, window);

;(function ($, window) {
    'use strict';

    /**
     * Image Gallery Plugin.
     *
     * This plugin opens a clone of an existing image slider in a lightbox.
     * This image slider clone provides three control buttons (zoom in, zoom out
     * and reset zoom) and also enables advanced features of the
     * image slider plugin like pinch-to-zoom, double-tap, moving scaled images.
     */
    $.plugin('swImageGallery', {

        defaults: {

            /**
             * Selector for the image container..
             *
             * @property imageContainerSelector
             * @type {String}
             */
            imageContainerSelector: '.image-slider--container',

            /**
             * Selector for the image slider itself..
             *
             * @property imageSlideSelector
             * @type {String}
             */
            imageSlideSelector: '.image-slider--slide',

            /**
             * Selector for the thumbnail container.
             *
             * @property thumbnailContainerSelector
             * @type {String}
             */
            thumbnailContainerSelector: '.image-slider--thumbnails',

            /**
             * Class that is used for the lightbox template.
             *
             * @property imageGalleryClass
             * @type {String}
             */
            imageGalleryClass: 'image--gallery',

            /**
             * Key code for the button that let the image slider
             * slide to the previous image.
             *
             * @property previousKeyCode
             * @type {Number}
             */
            previousKeyCode: 37,

            /**
             * Key code for the button that let the image slider
             * slide to the next image.
             *
             * @property nextKeyCode
             * @type {Number}
             */
            nextKeyCode: 39,

            /**
             * Maximum zoom factor for the image slider.
             * Will be passed to the image slider configuration in the lightbox.
             *
             * @property maxZoom
             * @type {Number|String}
             */
            maxZoom: 'auto',

            /**
             * Class that will be appended to the buttons when they
             * should be disabled.
             *
             * @property disabledClass
             * @type {String}
             */
            disabledClass: 'is--disabled',

            /**
             * Base class that will be applied to every gallery button.
             *
             * @property btnClass
             * @type {String}
             */
            btnClass: 'btn is--small',

            /**
             * Class that will be applied to the zoom in button.
             *
             * @property zoomInClass
             * @type {String}
             */
            zoomInClass: 'icon--plus3 button--zoom-in',

            /**
             * Class that will be applied to the zoom out button.
             *
             * @property zoomOutClass
             * @type {String}
             */
            zoomOutClass: 'icon--minus3 button--zoom-out',

            /**
             * Class that will be applied to the reset zoom button.
             *
             * @property zoomResetClass
             * @type {String}
             */
            zoomResetClass: 'icon--resize-shrink button--zoom-reset'
        },

        /**
         * Method for the plugin initialisation.
         * Merges the passed options with the data attribute configurations.
         * Creates and references all needed elements and properties.
         * Calls the registerEvents method afterwards.
         *
         * @public
         * @method init
         */
        init: function () {
            var me = this;

            me.applyDataAttributes();

            /**
             * Reference of the image container that should be cloned.
             *
             * @private
             * @property _$imageContainer
             * @type {jQuery}
             */
            me._$imageContainer = me.$el.find(me.opts.imageContainerSelector);

            if (!me._$imageContainer.length) {
                return;
            }

            /**
             * Reference of the thumbnail container that should be cloned.
             *
             * @private
             * @property _$thumbContainer
             * @type {jQuery}
             */
            me._$thumbContainer = me.$el.find(me.opts.thumbnailContainerSelector);

            /**
             * Clone of the given image container.
             * This clone will be used in the image gallery template.
             *
             * @private
             * @property _$imageContainerClone
             * @type {jQuery}
             */
            me._$imageContainerClone = me._$imageContainer.clone();

            /**
             * Clone of the given thumbnail container.
             * This clone will be used in the image gallery template.
             *
             * @private
             * @property _$thumbContainerClone
             * @type {jQuery}
             */
            me._$thumbContainerClone = me._$thumbContainer.clone();

            /**
             * Buttons that zooms the current image out by the factor of 1.
             *
             * @public
             * @property $zoomOutBtn
             * @type {jQuery}
             */
            me.$zoomOutBtn = me.createZoomOutButton().appendTo(me._$imageContainerClone);

            /**
             * Buttons that resets the current image zoom..
             *
             * @public
             * @property $zoomResetBtn
             * @type {jQuery}
             */
            me.$zoomResetBtn = me.createZoomResetButton().appendTo(me._$imageContainerClone);

            /**
             * Buttons that zooms the current image in by the factor of 1.
             *
             * @public
             * @property $zoomInBtn
             * @type {jQuery}
             */
            me.$zoomInBtn = me.createZoomInButton().appendTo(me._$imageContainerClone);

            /**
             * Image gallery template that will be used in the modal box.
             * Will be lazy created only when its needed (on this.$el click).
             *
             * @public
             * @property $template
             * @type {jQuery|null}
             */
            me.$template = null;

            me.registerEvents();
        },

        /**
         * Creates and returns the zoom in ( [+] ) button.
         *
         * @private
         * @method createZoomInButton
         */
        createZoomInButton: function () {
            var me = this,
                opts = this.opts,
                $zoomInButton = $('<div>', {
                    'class': opts.btnClass + ' ' + opts.zoomInClass
                });

            $.publish('plugin/swImageGallery/onCreateZoomInButton', [ me, $zoomInButton ]);

            return $zoomInButton;
        },

        /**
         * Creates and returns the zoom out ( [-] ) button.
         *
         * @private
         * @method createZoomOutButton
         */
        createZoomOutButton: function () {
            var me = this,
                opts = me.opts,
                $zoomOutButton = $('<div>', {
                    'class': opts.btnClass + ' ' + opts.zoomOutClass
                });

            $.publish('plugin/swImageGallery/onCreateZoomOutButton', [ me, $zoomOutButton ]);

            return $zoomOutButton;
        },

        /**
         * Creates and returns the zoom reset ( [-><-] ) button.
         *
         * @private
         * @method createZoomResetButton
         */
        createZoomResetButton: function () {
            var me = this,
                opts = me.opts,
                $zoomResetButton = $('<div>', {
                    'class': opts.btnClass + ' ' + opts.zoomResetClass
                });

            $.publish('plugin/swImageGallery/onCreateZoomResetButton', [ me, $zoomResetButton ]);

            return $zoomResetButton;
        },

        /**
         * Registers all needed events of the plugin.
         *
         * @private
         * @method registerEvents
         */
        registerEvents: function () {
            var me = this;

            me._on(me._$imageContainer.find(me.opts.imageSlideSelector), 'click', $.proxy(me.onClick, me));

            $.subscribe('plugin/swImageSlider/onSlide', $.proxy(me.onImageUpdate, me));
            $.subscribe('plugin/swImageSlider/onUpdateTransform', $.proxy(me.onImageUpdate, me));

            me._on(window, 'keydown', $.proxy(me.onKeyDown, me));

            $.publish('plugin/swImageGallery/onRegisterEvents', [ me ]);
        },

        /**
         * Returns the image slider plugin instance of the gallery.
         * If its not available, returns null instead.
         *
         * @public
         * @method getImageSlider
         * @returns {$.PluginBase|null}
         */
        getImageSlider: function () {
            var me = this,
                $template = me.$template,
                slider = ($template && $template.data('plugin_swImageSlider')) || null;

            $.publish('plugin/swImageGallery/onGetImageSlider', [ me, slider ]);

            return slider;
        },

        /**
         * Will be called when an image or its transformation
         * in the slider was updated.
         * Toggles the buttons specific to the image slider zoom options.
         *
         * @event onImageUpdate
         * @param {jQuery.Event} event
         * @param {$.PluginBase} context
         */
        onImageUpdate: function (event, context) {
            var me = this,
                plugin = me.getImageSlider();

            if (plugin !== context) {
                return;
            }

            me.toggleButtons(plugin);

            $.publish('plugin/swImageGallery/onImageUpdate', [ me, event, plugin ]);
        },

        /**
         * Will be called when the zoom reset button was clicked.
         * Resets the current image scaling of the image slider.
         *
         * @event onResetZoom
         * @param {jQuery.Event} event
         */
        onResetZoom: function (event) {
            var me = this,
                plugin = me.getImageSlider();

            event.preventDefault();

            if (!plugin || me.$zoomResetBtn.hasClass(me.opts.disabledClass)) {
                return;
            }

            me.disableButtons();

            plugin.resetTransformation(true, function () {
                me.toggleButtons(plugin);

                $.publish('plugin/swImageGallery/onResetZoomFinished', [ me, event, plugin ]);
            });

            $.publish('plugin/swImageGallery/onResetZoom', [ me, event, plugin ]);
        },

        /**
         * Will be called when the zoom in button was clicked.
         * Zooms the image slider in by the factor of 1.
         *
         * @event onZoomIn
         * @param {jQuery.Event} event
         */
        onZoomIn: function (event) {
            var me = this,
                plugin = me.getImageSlider();

            event.preventDefault();

            if (!plugin || me.$zoomInBtn.hasClass(me.opts.disabledClass)) {
                return;
            }

            me.disableButtons();

            plugin.scale(1, true, function () {
                me.toggleButtons(plugin);

                $.publish('plugin/swImageGallery/onZoomInFinished', [ me, event, plugin ]);
            });

            $.publish('plugin/swImageGallery/onZoomIn', [ me, event, plugin ]);
        },

        /**
         * Will be called when the zoom out button was clicked.
         * Zooms the image slider out by the factor of 1.
         *
         * @event onZoomOut
         * @param {jQuery.Event} event
         */
        onZoomOut: function (event) {
            var me = this,
                plugin = me.getImageSlider();

            event.preventDefault();

            if (!plugin || me.$zoomOutBtn.hasClass(me.opts.disabledClass)) {
                return;
            }

            me.disableButtons();

            plugin.scale(-1, true, function () {
                me.toggleButtons(plugin);

                $.publish('plugin/swImageGallery/onZoomOutFinished', [ me, event, plugin ]);
            });

            $.publish('plugin/swImageGallery/onZoomOut', [ me, event, plugin ]);
        },

        /**
         * Will be called when an keyboard key was pressed.
         * If the previous/next keycode was pressed, it will slide to
         * the previous/next image.
         *
         * @event onKeyDown
         * @param {jQuery.Event} event
         */
        onKeyDown: function (event) {
            var me = this,
                opts = me.opts,
                plugin = me.getImageSlider(),
                keyCode = event.which;

            if (!plugin) {
                return;
            }

            if (keyCode === opts.previousKeyCode) {
                plugin.slidePrev();
            }

            if (keyCode === opts.nextKeyCode) {
                plugin.slideNext();
            }

            $.publish('plugin/swImageGallery/onKeyDown', [ me, event, keyCode ]);
        },

        /**
         * Creates and returns the gallery template.
         * Will be used to lazy create the slider template
         * with all its large images.
         *
         * @private
         * @method createTemplate
         * @returns {jQuery}
         */
        createTemplate: function () {
            var me = this,
                $template,
                $el,
                img;

            me._$imageContainerClone.find('span[data-img-original]').each(function (i, el) {
                $el = $(el);

                img = $('<img>', {
                    'class': 'image--element',
                    'src': $el.attr('data-img-original')
                });

                $el.replaceWith(img);
            });

            me._$thumbContainerClone.find('a.thumbnails--arrow').remove();
            me._$imageContainerClone.find('.arrow').remove();

            $template = $('<div>', {
                'class': me.opts.imageGalleryClass,
                'html': [
                    me._$imageContainerClone,
                    me._$thumbContainerClone
                ]
            });

            $.publish('plugin/swImageGallery/onCreateTemplate', [ me, $template ]);

            return $template;
        },

        /**
         * Will be called when the detail page image slider was clicked..
         * Opens the lightbox with an image slider clone in it.
         *
         * @event onClick
         */
        onClick: function (event) {
            var me = this,
                imageSlider = me.$el.data('plugin_swImageSlider');

            $.modal.open(me.$template || (me.$template = me.createTemplate()), {
                width: '100%',
                height: '100%',
                animationSpeed: 350,
                additionalClass: 'image-gallery--modal no--border-radius',
                onClose: me.onCloseModal.bind(me)
            });

            me._on(me.$zoomInBtn, 'click touchstart', $.proxy(me.onZoomIn, me));
            me._on(me.$zoomOutBtn, 'click touchstart', $.proxy(me.onZoomOut, me));
            me._on(me.$zoomResetBtn, 'click touchstart', $.proxy(me.onResetZoom, me));

            picturefill();

            me.$template.swImageSlider({
                dotNavigation: false,
                swipeToSlide: true,
                pinchToZoom: true,
                doubleTap: true,
                maxZoom: me.opts.maxZoom,
                startIndex: imageSlider ? imageSlider.getIndex() : 0,
                preventScrolling: true
            });

            me.toggleButtons(me.getImageSlider());

            $.publish('plugin/swImageGallery/onClick', [ me, event ]);
        },

        /**
         * Will be called when the modal box was closed.
         * Destroys the imageSlider plugin instance of the lightbox template.
         *
         * @event onCloseModal
         */
        onCloseModal: function () {
            var me = this,
                plugin = me.getImageSlider();

            if (!plugin) {
                return;
            }

            plugin.destroy();

            $.publish('plugin/swImageGallery/onCloseModal', [ me ]);
        },

        /**
         * This function disables all three control buttons.
         * Will be called when an animation begins.
         *
         * @public
         * @method disableButtons
         */
        disableButtons: function () {
            var me = this,
                disabledClass = me.opts.disabledClass;

            me.$zoomResetBtn.addClass(disabledClass);
            me.$zoomOutBtn.addClass(disabledClass);
            me.$zoomInBtn.addClass(disabledClass);

            $.publish('plugin/swImageGallery/onDisableButtons', [ me ]);
        },

        /**
         * This function disables all three control buttons.
         * Will be called when an animation begins.
         *
         * @public
         * @method toggleButtons
         */
        toggleButtons: function (plugin) {
            var me = this,
                disabledClass = me.opts.disabledClass,
                scale,
                minScale,
                maxScale;

            if (!plugin) {
                return;
            }

            scale = plugin.getScale();
            minScale = plugin.getMinScale();
            maxScale = plugin.getMaxScale();

            me.$zoomResetBtn.toggleClass(disabledClass, scale === minScale);
            me.$zoomOutBtn.toggleClass(disabledClass, scale === minScale);
            me.$zoomInBtn.toggleClass(disabledClass, scale === maxScale);

            $.publish('plugin/swImageGallery/onToggleButtons', [ me ]);
        },

        /**
         * Destroys the plugin and removes
         * all elements created by the plugin.
         *
         * @public
         * @method destroy
         */
        destroy: function () {
            var me = this,
                plugin = me.getImageSlider();

            if (plugin) {
                plugin.destroy();
            }

            me.$template.remove();
            me.$template = null;

            me.$zoomOutBtn.remove();
            me.$zoomResetBtn.remove();
            me.$zoomInBtn.remove();

            me._$imageContainer = null;
            me._$thumbContainer = null;
            me._$imageContainerClone = null;
            me._$thumbContainerClone = null;
        }
    });
})(jQuery, window);

;(function ($) {

    /**
     * Shopware Offcanvas HTML Panel
     *
     * This plugin displays the given content inside an off canvas menu
     *
     * @example
     *
     * HTML Structure
     *
     * <div class="teaser--text-long">Off Canvas Content</div>
     * <div class="teaser--text-short is--hidden">
     *      Short Description with the
     *
     *      <a href="" class="text--offcanvas-link">Off canvas trigger element</a>
     * </div>
     *
     * <div class="teaser--text-offcanvas is--hidden">
     *      <a href="" class="close--off-canvas"><i class="icon--arrow-left"></i> Close window</a>
     * </div>
     *
     * <div class="offcanvas--content">This content will be displayed inside the off canvas menu.</div>
     *
     *
     * jQuery Initializing for all viewports
     *
     * StateManager.addPlugin('.category--teaser', 'swOffcanvasHtmlPanel');
     *
     * jQuery Initializing for some states
     *
     * StateManager.addPlugin('.category--teaser', 'swOffcanvasHtmlPanel', ['xs', 's']);
     *
     */
    $.plugin('swOffcanvasHtmlPanel', {

        defaults: {
            /**
             * Offcanvas Content which will be displayed in the off canvas menu
             *
             * @property offcanvasContent
             * @type {String}
             */
            'offcanvasContent': '.teaser--text-long',

            /**
             * Short description which will be displayed if viewport match plugin configuration
             *
             * @property shortDescription
             * @type {String}
             */
            'shortDescription': '.teaser--text-short',

            /**
             * Off canvas trigger element
             *
             * @property offcanvasTrigger
             * @type {String}
             */
            'offcanvasTrigger': '.text--offcanvas-link',

            /**
             * off canvas container
             *
             * @property offCanvasSelector
             * @type {String}
             */
            'offCanvasSelector': '.teaser--text-offcanvas',

            /**
             * off canvas close button
             *
             * @property offCanvasCloseSelector
             * @type {String}
             */
            'offCanvasCloseSelector': '.close--off-canvas',

            /**
             * off canvas direction type
             * @type {String} (fromLeft | fromRight)
             */
            'offCanvasDirection': 'fromRight',

            /**
             * hidden class for hiding long description
             *
             * @property hiddenCls
             * @type {String}
             */
            'hiddenCls': 'is--hidden'
        },

        /**
         * Initializes the plugin and register its events
         *
         * @public
         * @method init
         */
        init: function () {
            var me = this,
                opts = me.opts,
                $el = me.$el;

            me.applyDataAttributes();

            me._$shortText = $el.find(opts.shortDescription).removeClass(opts.hiddenCls);
            me._$longText = $el.find(opts.offcanvasContent).addClass(opts.hiddenCls);
            me._$offCanvas = $el.find(opts.offCanvasSelector).removeClass(opts.hiddenCls);
            me._$offcanvasTrigger = $el.find(opts.offcanvasTrigger);

            me._$offcanvasTrigger.swOffcanvasMenu({
                'offCanvasSelector': opts.offCanvasSelector,
                'closeButtonSelector': opts.offCanvasCloseSelector,
                'direction': opts.offCanvasDirection
            });
        },

        /**
         * This method removes all plugin specific classes
         * and removes all registered events
         *
         * @public
         * @method destroy
         */
        destroy: function () {
            var me = this,
                hiddenClass = me.opts.hiddenCls,
                plugin = me._$offcanvasTrigger.data('plugin_swOffcanvasMenu');

            // redesign content to old structure
            me._$longText.removeClass(hiddenClass);
            me._$shortText.addClass(hiddenClass);

            // hide offcanvas menu
            me._$offCanvas.addClass(hiddenClass);

            if (plugin) {
                plugin.destroy();
            }

            me._destroy();
        }
    });
})(jQuery);

;(function($, window) {
    'use strict';

    /**
     * Tab Switcher Plugin
     *
     * This Plugin switches to the correct content tab when the user adds
     * a product review which causes a page reload. The Plugin also
     * scrolls to the correct page position where the alert messages
     * are shown.
     */
    $.plugin('swJumpToTab', {

        defaults: {
            contentCls: 'has--content',
            tabDetail: '.tab-menu--product',
            tabCrossSelling: '.tab-menu--cross-selling'
        },

        init: function () {
            var me = this,
                param = decodeURI((RegExp('action=(.+?)(&|$)').exec(location.search) || [, null])[1]),
                tabId;

            me.$htmlBody = $('body, html');
            me.tabMenuProduct = me.$el.find(me.opts.tabDetail).data('plugin_swTabMenu');
            me.$tabMenuCrossSelling = me.$el.find(me.opts.tabCrossSelling);

            me.resizeCrossSelling();
            me.registerEvents();

            if (param === 'rating') {
                var $tab = $('[data-tabName="' + param + '"]'),
                    index = $tab.index() || 1;

                me.jumpToTab(index, $tab);
            }
        },

        resizeCrossSelling: function () {
            var me = this,
                $container;

            if (StateManager.isCurrentState(['xs', 's']) && me.$tabMenuCrossSelling.length) {
                me.$tabMenuCrossSelling.find('.tab--container').each(function (i, el) {
                    $container = $(el);

                    if ($container.find('.tab--content').html().trim().length) {
                        $container.addClass('has--content');
                    }
                });
            }
        },

        registerEvents: function () {
            var me = this;

            me.$el.on(me.getEventName('click'), '.product--rating-link, .link--publish-comment', $.proxy(me.onJumpToTab, me));

            $.publish('plugin/swJumpToTab/onRegisterEvents', [ me ]);
        },

        onJumpToTab: function (event) {
            var me = this,
                $tab = $('[data-tabName="rating"]'),
                index = $tab.index() || 1;

            event.preventDefault();

            me.jumpToTab(index, $tab);

            $.publish('plugin/swJumpToTab/onClick', [ me, event ]);
        },

        jumpToTab: function (tabIndex, jumpTo) {
            var me = this;

            if (!me.$el.hasClass('is--ctl-blog')) {
                me.tabMenuProduct.changeTab(tabIndex);
            }

            $.publish('plugin/swJumpToTab/onChangeTab', [ me, tabIndex, jumpTo ]);

            if (!jumpTo || !jumpTo.length) {
                return;
            }

            me.$htmlBody.animate({
                scrollTop: $(jumpTo).offset().top
            }, 0);

            $.publish('plugin/swJumpToTab/onJumpToTab', [ me, tabIndex, jumpTo ]);
        }
    });

})(jQuery, window);

;(function($, window) {
    var emptyFn = function() {};

    /**
     * Shopware AJAX variant
     *
     * @example
     * HTML:
     * <div data-ajax-variants-container="true"></div>
     *
     * JS:
     * $('*[data-ajax-variants-container="true"]').swAjaxVariant();
     */
    $.plugin('swAjaxVariant', {

        /**
         * Supports the browser the history api
         * @boolean
         */
        hasHistorySupport: Modernizr.history,

        /**
         * Safari specific property which prevent safari to do another request on page load.
         * @boolean
         */
        initialPopState: true,

        /**
         * Default configuration of the plugin
         * @object
         */
        defaults: {
            productDetailsSelector: '.product--detail-upper',
            configuratorFormSelector: '.configurator--form',
            orderNumberSelector: '.entry--sku .entry--content',
            historyIdentifier: 'sw-ajax-variants',
            productDetailsDescriptionSelector: '.content--description'
        },

        /**
         * Initializes the plugin and sets up the necessary event handler
         */
        init: function() {
            var me = this,
                ie;

            // Check if we have a variant configurator
            if (!me.$el.find('.product--configurator').length) {
                return;
            }

            me.applyDataAttributes();

            // Detecting IE version using feature detection (IE7+, browsers prior to IE7 are detected as 7)
            ie = (function (){
                if (window.ActiveXObject === undefined) return null;
                if (!document.querySelector) return 7;
                if (!document.addEventListener) return 8;
                if (!window.atob) return 9;
                if (!document.__proto__) return 10;
                return 11;
            })();

            if (ie && ie <= 9) {
                me.hasHistorySupport = false;
            }

            me.$el
                .on(me.getEventName('click'), '*[data-ajax-variants="true"]', $.proxy(me.onChange, me))
                .on(me.getEventName('change'), '*[data-ajax-select-variants="true"]', $.proxy(me.onChange, me))
                .on(me.getEventName('click'), '.reset--configuration', $.proxy(me.onChange, me));

            $(window).on("popstate", $.proxy(me.onPopState, me));

            if(me.hasHistorySupport) {
                me.publishInitialState();
            }
        },

        /**
         * Replaces the most recent history entry, when the user enters the page.
         *
         * @returns void
         */
        publishInitialState: function() {
            var me = this,
                stateObj = me._createHistoryStateObject();

            window.history.replaceState(stateObj.state, stateObj.title);
        },

        /**
         * Requests the HTML structure of the product detail page using AJAX and injects the returned
         * content into the page.
         *
         * @param {String} values
         * @param {Boolean} pushState
         */
        requestData: function(values, pushState) {
            var me = this,
                stateObj = me._createHistoryStateObject();

            $.loadingIndicator.open({
                closeOnClick: false,
                delay: 100
            });

            $.publish('plugin/swAjaxVariant/onBeforeRequestData', [ me, values, stateObj.location ]);

            values += '&template=ajax';

            if(stateObj.params.hasOwnProperty('c')) {
                values += '&c=' + stateObj.params.c;
            }

            $.ajax({
                url: stateObj.location,
                data: values,
                method: 'GET',
                success: function(response) {
                    var $response = $($.parseHTML(response)),
                        $productDetails,
                        $productDescription,
                        ordernumber;

                    // Replace the content
                    $productDetails = $response.find(me.opts.productDetailsSelector);
                    $(me.opts.productDetailsSelector).html($productDetails.html());

                    // Replace the description box
                    $productDescription = $response.find(me.opts.productDetailsDescriptionSelector);
                    $(me.opts.productDetailsDescriptionSelector).html($productDescription.html());

                    // Get the ordernumber for the url
                    ordernumber = $.trim(me.$el.find(me.opts.orderNumberSelector).text());

                    StateManager.addPlugin('select:not([data-no-fancy-select="true"])', 'swSelectboxReplacement')
                        .addPlugin('*[data-image-slider="true"]', 'swImageSlider', { touchControls: true })
                        .addPlugin('.product--image-zoom', 'swImageZoom', 'xl')
                        .addPlugin('*[data-image-gallery="true"]', 'swImageGallery')
                        .addPlugin('*[data-add-article="true"]', 'swAddArticle')
                        .addPlugin('*[data-modalbox="true"]', 'swModalbox');

                    // Plugin developers should subscribe to this event to update their plugins accordingly
                    $.publish('plugin/swAjaxVariant/onRequestData', [ me, response, values, stateObj.location ]);

                    if(pushState && me.hasHistorySupport) {
                        var location = stateObj.location + '?number=' + ordernumber;

                        if(stateObj.params.hasOwnProperty('c')) {
                            location += '&c=' + stateObj.params.c;
                        }

                        window.history.pushState(stateObj.state, stateObj.title, location);
                    }
                },
                complete: function() {
                    $.loadingIndicator.close();
                }
            });
        },

        /**
         * Event handler method which will be fired when the user click the back button
         * in it's browser.
         *
         * @param {EventObject} event
         * @returns {boolean}
         */
        onPopState: function(event) {
            var me = this,
                state = event.originalEvent.state;

            if (!state || !state.hasOwnProperty('type') || state.type !== 'sw-ajax-variants') {
                return;
            }

            if ($('html').hasClass('is--safari') && me.initialPopState) {
                me.initialPopState = false;
                return;
            }

            if (!state.values.length) {
                state = '';
            }

            // Prevents the scrolling to top in webkit based browsers
            if(state && state.scrollPos) {
                window.setTimeout(function() {
                    $(window).scrollTop(state.scrollPos);
                }, 10);
            }

            $.publish('plugin/swAjaxVariant/onPopState', [ me, state ]);

            me.requestData(state.values, false);
        },

        /**
         * Event handler which will fired when the user selects a variant in the storefront.
         * @param {EventObject} event
         */
        onChange: function(event) {
            var me = this,
                $target = $(event.target),
                $form = $target.parents('form'),
                values = $form.serialize();

            event.preventDefault();

            if (!me.hasHistorySupport) {
                $.loadingIndicator.open({
                    closeOnClick: false,
                    delay: 0
                });
                $form.submit();

                return false;
            }

            $.publish('plugin/swAjaxVariant/onChange', [ me, values, $target ]);

            me.requestData(values, true);
        },

        /**
         * Helper method which returns all available url parameters.
         * @returns {Object}
         * @private
         */
        _getUrlParams: function() {
            var url = window.decodeURIComponent(window.location.search.substring(1)),
                urlParams = url.split('&'),
                params = {};

            $.each(urlParams, function(i, param) {
                param = param.split('=');

                if(param[0].length && param[1].length && !params.hasOwnProperty(param[0])) {
                    params[param[0]] = param[1];
                }
            });

            return params;
        },

        /**
         * Helper method which returns the full URL of the shop
         * @returns {string}
         * @private
         */
        _getUrl: function() {
            return window.location.protocol + "//" + window.location.host + window.location.pathname;
        },

        /**
         * Provides a state object which can be used with the {@link Window.history} API.
         *
         * The ordernumber will be fetched every time 'cause we're replacing the upper part of the detail page and
         * therefore we have to get the ordernumber using the DOM.
         *
         * @returns {Object} state object including title and location
         * @private
         */
        _createHistoryStateObject: function() {
            var me = this,
                $form = me.$el.find(me.opts.configuratorFormSelector),
                urlParams = me._getUrlParams(),
                location = me._getUrl();

            return {
                state: {
                    type: me.opts.historyIdentifier,
                    values: $form.serialize(),
                    scrollPos: $(window).scrollTop()
                },
                title: document.title,
                location: location,
                params: urlParams
            };
        }
    });
})(jQuery, window);

;(function($, window, document) {
    'use strict';

    /**
     * Get the value of a cookie with the given name
     * @param name
     * @returns {string|undefined}
     */
    $.getCookie = function(name) {
        var value = "; " + document.cookie,
            parts = value.split("; " + name + "=");

        if (parts.length == 2) {
            return parts.pop().split(";").shift();
        }
        return undefined;
    };

    /**
     * Remove a cookie with the provided name
     * @param name
     */
    $.removeCookie = function(name) {
        var basePath = window.csrfConfig.basePath || "/";
        document.cookie = name + '=; path=' + basePath + '; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    };

    var CSRF = {

        /**
         * Key including subshop and -path
         */
        storageKey: '__csrf_token-' + window.csrfConfig.shopId,

        /**
         * Temporary request callback store
         */
        pendingRequests: {},

        /**
         * Returns the token
         * @returns {string}
         */
        getToken: function() {
            return $.getCookie(this.storageKey);
        },

        /**
         * Checks if the token needs to be requested
         * @returns {boolean}
         */
        checkToken: function() {
            return $.getCookie('invalidate-xcsrf-token') === undefined && this.getToken() !== undefined;
        },

        /**
         * Creates a hidden input fields which holds the csrf information
         * @returns {HTMLElement}
         */
        createTokenField: function() {
            var me = this;

            return $('<input>', {
                'type': 'hidden',
                'name': '__csrf_token',
                'value': me.getToken()
            });
        },

        /**
         * Adds the token field to the given form
         * @param {HTMLElement} formElement
         */
        addTokenField: function(formElement) {
            formElement.append(CSRF.createTokenField());
            $.publish('plugin/swCsrfProtection/addTokenField', [ this, formElement ]);
        },

        /**
         *
         * @returns {HTMLElement[]}
         */
        getFormElements: function() {
            return $('form[method="post"]');
        },

        /**
         * Search all forms on the page and create or update their csrf input fields
         */
        updateForms: function() {
            var me = this,
                formElements = me.getFormElements();

            $.each(formElements, function(index, formElement) {
                var csrfInput;

                formElement = $(formElement);
                csrfInput = formElement.find('input[name="__csrf_token"]');

                if (csrfInput.length > 0) {
                    csrfInput.val(me.getToken());
                } else {
                    me.addTokenField(formElement);
                }
            });

            $.publish('plugin/swCsrfProtection/updateForms', [ this, formElements ]);
        },

        /**
         * Registers handlers before sending an AJAX request & after it is completed.
         */
        setupAjax: function() {
            var me = this;

            $(document).ajaxSend($.proxy(me._ajaxBeforeSend, me));
            $(document).ajaxComplete($.proxy(me._ajaxAfterSend, me));

            $.publish('plugin/swCsrfProtection/setupAjax', [ me, me.getToken() ]);
        },

        /**
         * Update all forms in case a callback has replaced html parts and needs to be rebound
         *
         * @private
         */
        _ajaxAfterSend: function() {
            window.setTimeout(function() {
                this.updateForms();
            }.bind(this), 1);
        },

        /**
         * Append X-CSRF-Token header to every request
         *
         * @param event
         * @param request
         * @private
         */
        _ajaxBeforeSend: function(event, request) {
            request.setRequestHeader('X-CSRF-Token', this.getToken());
        },

        /**
         * Calls the frontend to retrieve a new csrf token and executes the afterInit on success
         */
        requestToken: function() {
            var me = this;

            $.ajax({
                url: window.csrfConfig.generateUrl,
                success: function(response, status, xhr) {
                    me.saveToken(xhr.getResponseHeader('x-csrf-token'));
                    $.removeCookie('invalidate-xcsrf-token');
                    $.publish('plugin/swCsrfProtection/requestToken', [ me, me.getToken() ]);
                    me.afterInit();
                }
            });
        },

        /**
         * Save token into a cookie
         * @param token
         */
        saveToken: function(token) {
            var me = this,
                basePath = window.csrfConfig.basePath || "/";

            document.cookie = me.storageKey + "=" + token + "; path=" + basePath;
        },

        /**
         * Initialize the CSRF protection
         */
        init: function() {
            var me = this;

            if (me.checkToken()) {
                me.afterInit();
                return;
            }

            me.requestToken();
        },

        /**
         * Runs after a valid token is set
         */
        afterInit: function() {
            var me = this;

            me.updateForms();
            me.setupAjax();

            $.publish('plugin/swCsrfProtection/init', [ me ]);
        }

    };

    $(function() {
        CSRF.init();
    });

    window.CSRF = CSRF;

})(jQuery, window, document);
;(function($) {
    'use strict';

    /**
     * Shopware Panel Auto Resizer Plugin
     *
     * This plugin allows you to automatically resize a bunch of panels to match their biggest height. By default,
     * the setting 'columns' is set to 0, which will calculate the height based on all children elements beneath the
     * plugin element. If you want to resize panels in a 2-column layout and their height should match the height
     * of their neighbour, you have to set 'columns' to 2.
     *
     * You can activate this plugin by setting `data-panel-auto-resizer="true"` on the parent element of the elements
     * to resize.
     */
    $.plugin('swPanelAutoResizer', {

        defaults: {
            /**
             * CSS class selector for panel headers
             */
            panelHeaderSelector: '.panel--header',

            /**
             * CSS class selector for panel bodies
             */
            panelBodySelector: '.panel--body',

            /**
             * CSS class selector for panel actions
             */
            panelFooterSelector: '.panel--actions',

            /**
             * Maximal height, set to NULL (default) if it should not be limited
             */
            maxHeight: null
        },

        /**
         * Cache property for children elements
         */
        $elChildren: null,

        /**
         * If set to true, the modal will center after resizing
         */
        isModal: false,

        /**
         * Automatic resizing of header, body and footer
         */
        init: function() {
            var me = this;

            me.applyDataAttributes();

            me.$elChildren = me.$el.children();
            me.isModal = me.$el.closest('.js--modal').length > 0;

            $.subscribe(me.getEventName('plugin/swPanelAutoResizer/onAfterSetHeight'), $.proxy(me._onAfterSetHeight, me));

            $.publish('plugin/swPanelAutoResizer/onInit', [ me ]);
            me.update();
            $.publish('plugin/swPanelAutoResizer/onAfterInit', [ me ]);
        },

        /**
         * Trigger a recalculation if the panel is nested any parent panel has been resized.
         *
         * @param event
         * @param context
         * @private
         */
        _onAfterSetHeight: function(event, context) {
            var me = this;

            if (me === context) {
                return;
            }

            if (me.$el.closest(context.$el).length > 0) {
                me._calculateColumns();
                me.resize();
            }
        },

        /**
         * Calculate how many columns need to be sized properly
         * based on their and their container's width
         *
         * @private
         */
        _calculateColumns: function() {
            var me = this,
                maxWidth = me.$el.width(),
                width = 0,
                columns = 0,
                childWidth = 0;

            $.each(me.$elChildren, function(index, child) {
                childWidth = $(child).width();

                if ((width + childWidth) > maxWidth) {
                    return;
                }

                width += childWidth;
                columns++;
            });

            me._columns = columns;
        },

        /**
         * Recalculate the columns and resize all elements
         *
         * @private
         */
        update: function() {
            var me = this;

            if (me._resizeTimeout) {
                window.clearTimeout(me._resizeTimeout);
            }

            me._resizeTimeout = window.setTimeout(function() {
                $.publish('plugin/swPanelAutoResizer/onUpdate', [ me ]);

                me._calculateColumns();
                me.resize();

                $.publish('plugin/swPanelAutoResizer/afterUpdate', [ me ]);
            }, 150);
        },

        /**
         * Calculate the maximum height of all given elements. It might be capped by the `maxHeight`
         * default option.
         *
         * @param $elements
         * @returns {number}
         */
        getMaxHeight: function ($elements) {
            var me = this,
                opts = me.opts,
                itemHeight = 0,
                height = 0;

            $.publish('plugin/swPanelAutoResizer/onGetMaxHeight', [ me ]);

            // set heights to auto to recalculate the actual content height
            $elements.each(function(index, childElement) {
                $(childElement).css('height', 'auto');
            });

            $elements.each(function(index, childElement) {
                itemHeight = $(childElement).height();
                if (itemHeight > height) {
                    height = itemHeight;
                }
            });

            if (opts.maxHeight !== null && opts.maxHeight < height) {
                height = opts.maxHeight;
            }

            $.publish('plugin/swPanelAutoResizer/onAfterGetMaxHeight', [ me, height ]);

            return height;
        },

        /**
         * Sets height on the given elements
         *
         * @param $elements
         * @param {number} height
         */
        setHeight: function($elements, height) {
            var me = this;

            if (height <= 0) {
                return;
            }

            $.publish('plugin/swPanelAutoResizer/onSetHeight', [ me ]);

            $.each($elements, function(index, childElement) {
                $(childElement).height(height);
            });

            $.publish('plugin/swPanelAutoResizer/onAfterSetHeight', [ me ]);
        },

        /**
         * Get maximal height and set the height of the elements
         *
         * @param {string} selector
         */
        resize: function(selector) {
            var me = this,
                height = 0,
                chunkItems = [],
                i = 0,
                childrenCount = me.$elChildren.length;

            // shortcut to resize all
            if (typeof selector === 'undefined') {
                me.resize(me.opts.panelHeaderSelector);
                me.resize(me.opts.panelBodySelector);
                me.resize(me.opts.panelFooterSelector);
                return;
            }

            $.publish('plugin/swPanelAutoResizer/onResize', [ me, selector ]);

            if (me._columns > 1) {
                for (i; i < childrenCount; i += me._columns) {
                    chunkItems = me.$elChildren
                        .slice(i, i + me._columns)
                        .map(function(index, child) {
                            return $(child).find(selector).first();
                        });

                    height = me.getMaxHeight(chunkItems);
                    me.setHeight(chunkItems, height);
                }
            } else {
               me.resetHeight();
            }

            me._centerModal();

            $.publish('plugin/swPanelAutoResizer/onAfterResize', [ me, selector ]);
        },

        /**
         * Call center() on modal
         * 
         * @private
         */
        _centerModal: function() {
            if (this.isModal === false) {
                return;
            }

            $.modal.center();
        },

        /**
         * Sets the height back to 'auto' if the plugin gets disabled
         */
        resetHeight: function() {
            var me = this,
                opts = me.opts;

            var allSelectorClass = [
                    opts.panelHeaderSelector,
                    opts.panelBodySelector,
                    opts.panelFooterSelector
                ].join(",");

            me.$elChildren.find(allSelectorClass).each(function(index, childElement) {
                $(childElement).css('height', 'auto');
            });
        },

        /**
         * Destroy the plugin and reset it's modified attributes
         */
        destroy: function() {
            var me = this;

            me.resetHeight();
            $.unsubscribe(me.getEventName('plugin/swPanelAutoResizer/onAfterSetHeight'));
            me._destroy();
        }

    });

})(jQuery);
;(function($, window) {
    "use strict";

    $.addressSelection = {

        /**
         * @string _name
         */
        _name: 'addressSelection',

        /**
         * Holds the options of the last opened selection
         *
         * @object _previousOptions
         */
        _previousOptions: {},

        /** Your default options */
        defaults: {
            /**
             * Id of an address which should not be shown
             *
             * @int id
             */
            id: null,

            /**
             * Form selector for each address
             *
             * @string formSelector
             */
            formSelector: '.address-manager--selection-form',

            /**
             * Width of the selection
             *
             * @string width
             */
            width: '80%',

            /**
             * Height of the selection
             *
             * @string height
            */
            height: '80%',

            /**
             * Modal sizing
             *
             * @string sizing
             */
            sizing: 'content',

            /**
             * Extra parameters to trigger specific actions afterwards
             *
             * Comma separated list of session keys to be filled with address id
             *
             * @string sessionKey
             */
            sessionKey: '',

            /**
             * Set the address as default billing address
             *
             * @boolean setDefaultBillingAddress
             */
            setDefaultBillingAddress: null,

            /**
             * Set the address as default shipping address
             *
             * @boolean setDefaultShippingAddress
             */
            setDefaultShippingAddress: null
        },

        /**
         * add namespace for events
         * @param event
         * @returns {string}
         */
        getEventName: function(event) {
            return event + '.' + this._name;
        },

        /**
         * Open the selection with the previous options
         */
        openPrevious: function () {
            this.open(this._previousOptions);
        },

        /**
         * open function for opening the selection modal. The available addresses will be
         * fetched as html from the api
         */
        open: function (options) {
            var me = this,
                sizing,
                extraData,
                maxHeight = 0;

            me.opts = $.extend({}, me.defaults, options);

            extraData = {
                sessionKey: me.opts.sessionKey,
                setDefaultBillingAddress: me.opts.setDefaultBillingAddress,
                setDefaultShippingAddress: me.opts.setDefaultShippingAddress
            };

            sizing = me.opts.sizing;

            me._previousOptions = Object.create(me.opts);

            if (window.StateManager._getCurrentDevice() === 'mobile') {
                sizing = 'auto';
            } else {
                maxHeight = me.opts.height;
            }

            // reset modal
            $.modal.close();
            $.overlay.open({ closeOnClick: false });
            $.loadingIndicator.open({ openOverlay: false });

            $.publish('plugin/swAddressSelection/onBeforeAddressFetch', [ me ]);

            // Ajax request to fetch available addresses
            $.ajax({
                'url': window.controller['ajax_address_selection'],
                'data': {
                    id: me.opts.id,
                    extraData: extraData
                },
                'success': function(data) {
                    $.loadingIndicator.close(function() {
                        $.subscribe(me.getEventName('plugin/swModal/onOpen'), $.proxy(me._onSetContent, me));

                        $.modal.open(data, {
                            width: me.opts.width,
                            maxHeight: maxHeight,
                            additionalClass: 'address-manager--modal address-manager--selection',
                            sizing: sizing
                        });

                        $.unsubscribe(me.getEventName('plugin/swModal/onOpen'));
                    });

                    $.publish('plugin/swAddressSelection/onAddressFetchSuccess', [ me, data ]);
                }
            });
        },

        /**
         * Callback from $.modal setContent method
         *
         * @param event
         * @param $modal
         * @private
         */
        _onSetContent: function(event, $modal) {
            var me = this;

            me._registerPlugins();
            me._bindButtonAction($modal);
        },

        /**
         * Re-register plugins to enable them in the modal
         * @private
         */
        _registerPlugins: function() {
            window.StateManager
                .addPlugin('*[data-panel-auto-resizer="true"]', 'swPanelAutoResizer')
                .addPlugin('*[data-address-editor="true"]', 'swAddressEditor')
                .addPlugin('*[data-preloader-button="true"]', 'swPreloaderButton');

            $.publish('plugin/swAddressSelection/onRegisterPlugins', [ this ]);
        },

        /**
         * Registers listeners for the click event on the "select address" buttons. The buttons contain the
         * needed data for the address selection. It then sends an ajax post request to the provided
         * action
         *
         * @param $modal
         * @private
         */
        _bindButtonAction: function($modal) {
            var me = this;

            $.publish('plugin/swAddressSelection/onBeforeBindButtonAction', [ me, $modal ]);

            $modal._$content
                .find(me.opts.formSelector)
                .on('submit', function(event) {
                    var $target = $(event.target);

                    event.preventDefault();

                    $.publish('plugin/swAddressSelection/onBeforeSave', [ me, $target ]);

                    // send data to api endpoint
                    $.ajax({
                        method: $target.attr('method'),
                        url: $target.attr('action'),
                        data: $target.serialize(),
                        success: function(response) {
                            me.onSave($modal, response);
                        }
                    });
                });

            $.publish('plugin/swAddressSelection/onAfterBindButtonAction', [ me, $modal ]);
        },

        /**
         * Callback after the API has been called successfully
         */
        onSave: function($modal, response) {
            var me = this;

            $.publish('plugin/swAddressSelection/onAfterSave', [ me, $modal, response ]);

            window.location.reload();
        }
    };

    /**
     * Shopware Address Selector Plugin.
     *
     * The plugin handles the address selection. You need to set some extra data to make something happen.
     *
     * Usually you specify a list of sessionKey's or set the selected address as default billing or shipping address.
     *
     * Example usage:
     * ```
     * <button class="btn" data-address-selection="true" data-id="123" data-setDefaultBillingAddress="1">
     *   Select address
     * </button>
     * ``
     */
    $.plugin('swAddressSelection', {
        /**
         * Initializes the plugin
         *
         * @returns {Plugin}
         */
        init: function () {
            var me = this;

            me.opts = $.extend({}, Object.create($.addressSelection.defaults), me.opts);
            me.applyDataAttributes(true);

            me._on(me.$el, 'click', $.proxy(me.onClick, me));

            $.publish('plugin/swAddressSelection/onRegisterEvents', [ me ]);
        },

        /**
         * Click callback
         * @param event
         */
        onClick: function(event) {
            event.preventDefault();
            $.addressSelection.open(this.opts);
        }
    });
})(jQuery, window);

;(function($, window) {
    "use strict";

    /**
     * Shopware Address Editor Plugin.
     *
     * The plugin handles the address editing of a given address or creation of a new one. You can specify
     * additional parameters to do various operations afterwards. See property section extra parameters for
     * more information.
     *
     * Example usage:
     * ```
     * <button class="btn" data-address-editor="true" data-id="123">
     *   Change address
     * </button>
     * ``
     */
    $.plugin('swAddressEditor', {

        /** Your default options */
        defaults: {
            /**
             * Id of an address which should be edited
             *
             * @int id
             */
            id: null,

            /**
             * Submit button class to dis/enable them later on
             *
             * @string submitButtonSelector
             */
            submitButtonSelector: '.address--form-submit',

            /**
             * Width of the selection
             *
             * @string width
             */
            width: 650,

            /**
             * Height of the selection
             *
             * @string height
             */
            height: '80%',

            /**
             * Modal sizing
             *
             * @string sizing
             */
            sizing: 'content',

            /**
             * Extra parameters to trigger specific actions afterwards
             *
             * Comma separated list of session keys to be filled with address id
             *
             * @string sessionKey
             */
            sessionKey: '',

            /**
             * Set the address as default billing address
             *
             * @boolean setDefaultBillingAddress
             */
            setDefaultBillingAddress: null,

            /**
             * Set the address as default shipping address
             *
             * @boolean setDefaultShippingAddress
             */
            setDefaultShippingAddress: null,

            /**
             * Display the address selection after the editor has been closed
             *
             * @boolean showSelectionOnClose
             */
            showSelectionOnClose: false
        },

        /**
         * Initializes the plugin
         *
         * @returns {Plugin}
         */
        init: function () {
            var me = this;

            me.applyDataAttributes(true);

            me._on(me.$el, 'click', $.proxy(me.onClick, me));

            $.publish('plugin/swAddressEditor/onRegisterEvents', [ me ]);
        },

        /**
         * Handle click event and delegate to the open() method
         *
         * @param event
         */
        onClick: function(event) {
            var me = this;

            event.preventDefault();

            $.publish('plugin/swAddressEditor/onBeforeClick', [ me, me.opts.id ]);

            if (me.opts.id) {
                me.open(me.opts.id);
            } else {
                me.open();
            }

            $.publish('plugin/swAddressEditor/onAfterClick', [ me, me.opts.id ]);
        },

        /**
         * Open modal and load data if addressId is a valid number
         *
         * @param {int} addressId
         */
        open: function(addressId) {
            var me = this,
                sizing = me.opts.sizing,
                maxHeight = 0,
                requestData = {
                    id: addressId || null,
                    extraData: {
                        sessionKey: me.opts.sessionKey,
                        setDefaultBillingAddress: me.opts.setDefaultBillingAddress,
                        setDefaultShippingAddress: me.opts.setDefaultShippingAddress
                    }
                };

            if (window.StateManager._getCurrentDevice() === 'mobile') {
                sizing = 'auto';
            } else {
                maxHeight = me.opts.height;
            }

            // reset modal
            $.modal.close();
            $.overlay.open({ closeOnClick: false });
            $.loadingIndicator.open({ openOverlay: false });

            $.publish('plugin/swAddressEditor/onBeforeOpen', [ me, requestData ]);

            // Ajax request to fetch available addresses
            $.ajax({
                'url': window.controller['ajax_address_editor'],
                'data': requestData,
                'success': function(data) {
                    $.loadingIndicator.close(function() {
                        $.subscribe(me.getEventName('plugin/swModal/onOpen'), $.proxy(me._onSetContent, me));

                        $.modal.open(data, {
                            width: me.opts.width,
                            height: me.opts.height,
                            maxHeight: maxHeight,
                            sizing: sizing,
                            additionalClass: 'address-manager--modal address-manager--editor',
                            addressId: addressId
                        });

                        $.unsubscribe(me.getEventName('plugin/swModal/onOpen'));
                    });

                    $.publish('plugin/swAddressEditor/onAddressFetchSuccess', [ me, data ]);
                }
            });

            $.publish('plugin/swAddressEditor/onAfterOpen', [ me ]);
        },

        /**
         * Callback from $.modal setContent method
         *
         * @param event
         * @param $modal
         * @private
         */
        _onSetContent: function(event, $modal) {
            var me = this;

            me._registerPlugins();
            me._bindButtonAction($modal);
        },

        /**
         * Re-register plugins to enable them in the modal
         * @private
         */
        _registerPlugins: function() {
            window.StateManager
                .addPlugin('div[data-register="true"]', 'swRegister')
                .addPlugin('select:not([data-no-fancy-select="true"])', 'swSelectboxReplacement')
                .addPlugin('*[data-preloader-button="true"]', 'swPreloaderButton');

            $.publish('plugin/swAddressEditor/onRegisterPlugins', [ this ]);
        },


        /**
         * Registers listeners for the click event on the "change address" buttons. The buttons contain the
         * needed data for the address selection. It then sends an ajax post request to the form
         * action
         *
         * @param $modal
         * @private
         */
        _bindButtonAction: function($modal) {
            var me = this,
                $submitButtons = $modal._$content.find(me.opts.submitButtonSelector),
                $actionInput = $modal._$content.find('input[name=saveAction]');

            $.publish('plugin/swAddressEditor/onBeforeBindButtonAction', [ me, $modal ]);

            // hook into submit button click to eventually update the saveAction value bound to data-value
            $submitButtons.on('click', function(event) {
                var $elem = $(this);

                event.preventDefault();

                $actionInput.val($elem.attr('data-value'));
                $elem.closest('form').submit();
            });

            // submit form via ajax
            $modal._$content
                .find('form')
                .on('submit', function(event) {
                    var $target = $(event.target),
                        actionData = {
                            id: $modal.options.addressId || null
                        };

                    me._resetErrorMessage($modal);
                    me._disableSubmitButtons($modal);

                    event.preventDefault();

                    $.each($target.serializeArray(), function() {
                        actionData[this.name] = this.value;
                    });

                    $.publish('plugin/swAddressEditor/onBeforeSave', [ me, actionData ]);

                    // send data to api endpoint
                    $.ajax({
                        url: $target.attr('action'),
                        data: actionData,
                        method: 'POST',
                        success: function(response) {
                            me.onSave($modal, response);
                        }
                    });
                });

            $.publish('plugin/swAddressEditor/onAfterBindButtonAction', [ me, $modal ]);
        },

        /**
         * Callback after the API has been called
         */
        onSave: function($modal, response) {
            var me = this;

            $.publish('plugin/swAddressEditor/onAfterSave', [ me, $modal, response ]);

            if (response.success === true) {
                if (me.opts.showSelectionOnClose) {
                    $.addressSelection.openPrevious();
                } else {
                    window.location.reload();
                }
            } else {
                me._highlightErrors($modal, response.errors);
                me._enableSubmitButtons($modal);
            }
        },

        /**
         * Display error container and highlight the fields containing errors
         *
         * @param $modal
         * @param errors
         * @private
         */
        _highlightErrors: function($modal, errors) {
            var fieldPrefix = $modal._$content.find('.address-form--panel').attr('data-prefix') || 'address';

            $modal._$content.find('.address-editor--errors').removeClass('is--hidden');

            $.each(errors, function(field) {
                $modal._$content.find('[name="' + fieldPrefix + '[' + field + ']"]').addClass('has--error');
            });
        },

        /**
         * Hide error container in popup
         *
         * @param $modal
         * @private
         */
        _resetErrorMessage: function($modal) {
            $modal._$content.find('.address-editor--errors').addClass('is--hidden');
        },

        /**
         * Disable submit buttons to prevent multiple submissions
         *
         * @param $modal
         * @private
         */
        _disableSubmitButtons: function($modal) {
            var me = this;
            $modal._$content.find(me.opts.submitButtonSelector).attr('disabled', 'disabled');
        },

        /**
         * Reset state of preloader plugin and remove disable attribute
         *
         * @param $modal
         * @private
         */
        _enableSubmitButtons: function($modal) {
            var me = this;

            $modal._$content
                .find(me.opts.submitButtonSelector)
                .removeAttr('disabled')
                .data('plugin_swPreloaderButton')
                    .reset();
        }
    });
})(jQuery, window);

(function($, window) {

    window.StateManager.init([
        {
            state: 'xs',
            enter: 0,
            exit: 29.9375   // 479px
        },
        {
            state: 's',
            enter: 30,      // 480px
            exit: 47.9375   // 767px
        },
        {
            state: 'm',
            enter: 48,      // 768px
            exit: 63.9375   // 1023px
        },
        {
            state: 'l',
            enter: 64,      // 1024px
            exit: 78.6875   // 1259px
        },
        {
            state: 'xl',
            enter: 78.75,   // 1260px
            exit: 322.5     // 5160px
        }
    ]);

    window.StateManager

        // OffCanvas menu
        .addPlugin('*[data-offcanvas="true"]', 'swOffcanvasMenu', ['xs', 's'])

        // Search field
        .addPlugin('*[data-search="true"]', 'swSearch')

        // Slide panel
        .addPlugin('.footer--column .column--headline', 'swCollapsePanel', {
            contentSiblingSelector: '.column--content'
        }, ['xs', 's'])

        // Collapse panel
        .addPlugin('#new-customer-action', 'swCollapsePanel', ['xs', 's'])

        // Image slider
        .addPlugin('*[data-image-slider="true"]', 'swImageSlider', { touchControls: true })

        // Image zoom
        .addPlugin('.product--image-zoom', 'swImageZoom', 'xl')

        // Collapse panel
        .addPlugin('.blog-filter--trigger', 'swCollapsePanel', ['xs', 's', 'm', 'l'])

        // Off canvas HTML Panel
        .addPlugin('.category--teaser .hero--text', 'swOffcanvasHtmlPanel', ['xs', 's'])

        // Default product slider
        .addPlugin('*[data-product-slider="true"]', 'swProductSlider')

        // Detail page tab menus
        .addPlugin('.product--rating-link, .link--publish-comment', 'swScrollAnimate', {
            scrollTarget: '.tab-menu--product'
        })
        .addPlugin('.tab-menu--product', 'swTabMenu', ['s', 'm', 'l', 'xl'])
        .addPlugin('.tab-menu--cross-selling', 'swTabMenu', ['m', 'l', 'xl'])
        .addPlugin('.tab-menu--product .tab--container', 'swOffcanvasButton', {
            titleSelector: '.tab--title',
            previewSelector: '.tab--preview',
            contentSelector: '.tab--content'
        }, ['xs'])
        .addPlugin('.tab-menu--cross-selling .tab--header', 'swCollapsePanel', {
            'contentSiblingSelector': '.tab--content'
        }, ['xs', 's'])
        .addPlugin('body', 'swAjaxProductNavigation')
        .addPlugin('*[data-collapse-panel="true"]', 'swCollapsePanel')
        .addPlugin('*[data-range-slider="true"]', 'swRangeSlider')
        .addPlugin('*[data-auto-submit="true"]', 'swAutoSubmit')
        .addPlugin('*[data-drop-down-menu="true"]', 'swDropdownMenu')
        .addPlugin('*[data-newsletter="true"]', 'swNewsletter')
        .addPlugin('*[data-pseudo-text="true"]', 'swPseudoText')
        .addPlugin('*[data-preloader-button="true"]', 'swPreloaderButton')
        .addPlugin('*[data-filter-type]', 'swFilterComponent')
        .addPlugin('*[data-listing-actions="true"]', 'swListingActions')
        .addPlugin('*[data-scroll="true"]', 'swScrollAnimate')
        .addPlugin('*[data-ajax-wishlist="true"]', 'swAjaxWishlist')
        .addPlugin('*[data-image-gallery="true"]', 'swImageGallery')

        // Emotion Ajax Loader
        .addPlugin('.emotion--wrapper', 'swEmotionLoader')

        .addPlugin('input[type="submit"][form], button[form]', 'swFormPolyfill')
        .addPlugin('select:not([data-no-fancy-select="true"])', 'swSelectboxReplacement')

        // Deferred loading of the captcha
        .addPlugin('div.captcha--placeholder[data-src]', 'swCaptcha')
        .addPlugin('*[data-modalbox="true"]', 'swModalbox')

        // Change the active tab to the customer reviews
        .addPlugin('.is--ctl-detail', 'swJumpToTab')
        .addPlugin('*[data-ajax-shipping-payment="true"]', 'swShippingPayment')

        // Initialize the registration plugin
        .addPlugin('div[data-register="true"]', 'swRegister')
        .addPlugin('*[data-last-seen-products="true"]', 'swLastSeenProducts', $.extend({}, window.lastSeenProductsConfig))
        .addPlugin('*[data-add-article="true"]', 'swAddArticle')
        .addPlugin('*[data-menu-scroller="true"]', 'swMenuScroller')
        .addPlugin('*[data-collapse-cart="true"]', 'swCollapseCart')
        .addPlugin('*[data-compare-ajax="true"]', 'swProductCompareAdd')
        .addPlugin('*[data-product-compare-menu="true"]', 'swProductCompareMenu')
        .addPlugin('*[data-infinite-scrolling="true"]', 'swInfiniteScrolling')
        .addPlugin('*[data-ajax-variants-container="true"]', 'swAjaxVariant')
        .addPlugin('*[data-subcategory-nav="true"]', 'swSubCategoryNav', ['xs', 's'])
        .addPlugin('*[data-panel-auto-resizer="true"]', 'swPanelAutoResizer')
        .addPlugin('*[data-address-selection="true"]', 'swAddressSelection')
        .addPlugin('*[data-address-editor="true"]', 'swAddressEditor')
    ;

    $(function($) {

        // Check if cookies are disabled and show notification
        if (!StorageManager.hasCookiesSupport) {
            createNoCookiesNoticeBox(window.snippets.noCookiesNotice);
        }

        // Create the no cookies notification message
        function createNoCookiesNoticeBox(message) {
            $('<div/>', { 'class': 'alert is--warning no--cookies' }).append(
                $('<div/>', {'class': 'alert--icon'}).append(
                    $('<i/>', {'class': 'icon--element icon--warning'})
                )
            ).append(
                $('<div/>', {
                    'class': 'alert--content',
                    'html': message
                }).append(
                    $('<a/>', {
                        'class': 'close--alert',
                        'html': '✕'
                    })
                    .on('click', function () {
                        $(this).closest('.no--cookies').hide();
                    })
                )
            ).appendTo('.page-wrap');
        }

        // Lightbox auto trigger
        $('*[data-lightbox="true"]').on('click.lightbox', function (event) {
            var $el = $(this),
                target = ($el.is('[data-lightbox-target]')) ? $el.attr('data-lightbox-target') : $el.attr('href');

            event.preventDefault();

            if (target.length) {
                $.lightbox.open(target);
            }
        });

        // Start up the placeholder polyfill, see ```jquery.ie-fixes.js```
        $('input, textarea').placeholder();

        $('.add-voucher--checkbox').on('change', function (event) {
            var method = (!$(this).is(':checked')) ? 'addClass' : 'removeClass';
            event.preventDefault();

            $('.add-voucher--panel')[method]('is--hidden');
        });

        $('.table--shipping-costs-trigger').on('click touchstart', function (event) {

            event.preventDefault();

            var $this = $(this),
                $next = $this.next(),
                method = ($next.hasClass('is--hidden')) ? 'removeClass' : 'addClass';

            $next[method]('is--hidden');
        });

        // Ajax cart amount display
        function cartRefresh() {
            var ajaxCartRefresh = window.controller.ajax_cart_refresh,
                $cartAmount = $('.cart--amount'),
                $cartQuantity = $('.cart--quantity');

            if (!ajaxCartRefresh.length) {
                return;
            }

            $.ajax({
                'url': ajaxCartRefresh,
                'dataType': 'jsonp',
                'success': function (response) {
                    var cart = JSON.parse(response);

                    if(!cart.amount || !cart.quantity) {
                        return;
                    }

                    $cartAmount.html(cart.amount);
                    $cartQuantity.html(cart.quantity).removeClass('is--hidden');

                    if(cart.quantity == 0) {
                        $cartQuantity.addClass('is--hidden');
                    }
                }
            });
        }

        $.subscribe('plugin/swAddArticle/onAddArticle', cartRefresh);
        $.subscribe('plugin/swCollapseCart/onRemoveArticleFinished', cartRefresh);

        $('.is--ctl-detail .reset--configuration').on('click', function () {
            $.loadingIndicator.open({
                closeOnClick: false
            });
        });
    });
})(jQuery, window);
