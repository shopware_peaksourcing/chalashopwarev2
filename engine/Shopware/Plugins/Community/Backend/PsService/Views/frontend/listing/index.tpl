{extends file="parent:frontend/listing/index.tpl"}

{if $sDelivery}
	{block name="frontend_listing_index_faq"}
		<h2>Fragen und Antworten</h2>
		<!-- start: Accordion -->
		<div class="accordion">
			{foreach from=$sFaq item=faq key=key name="counter"}
				<div class="item">
					<h3>{$faq.name}</h3>
					<p>{$faq.description|unescape:'html'}</p>
				</div>
			{/foreach}
		</div>
		<!-- end: Accordion -->
		{block name="frontend_listing_index_faqlink"}
			<a href="{$sFaqLink}" class="button">{s name="ToAllFaq" namespace="frontend/listing/index"}Alle Fragen und Antworten zeigen{/s}</a>
		{/block}
	{/block}
{/if}

{if $sDelivery}
	{block name="frontend_listing_index_delivery"}
		<h2>Versand und Rücksendung</h2>
		{foreach from=$sDelivery item=delivery key=key name="counter"}
			<div class="text_item">
				<h3>{$delivery.name}</h3>
				{$delivery.description|unescape:'html'}
			</div>
		{/foreach}
	{/block}
{/if}
