Ext.define('Shopware.apps.PsDelivery.model.Delivery', {
  extend: 'Shopware.data.Model',

  configure: function() {
    return {
      controller: 'PsDelivery',
      detail: 'Shopware.apps.PsDelivery.view.detail.Delivery'
    };
  },

  fields: [
    { name : 'id', type: 'int', useNull: true },
    { name : 'name', type: 'string' },
    { name : 'description', type: 'string', useNull: true }
  ]
});
