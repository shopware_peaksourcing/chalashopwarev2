Ext.define('Shopware.apps.PsDelivery', {
  extend: 'Enlight.app.SubApplication',

  name:'Shopware.apps.PsDelivery',

  loadPath: '{url action=load}',
  bulkLoad: true,

  controllers: [ 'Main' ],

  views: [
    'list.Window',
    'list.Delivery',

    'detail.Window',
    'detail.Delivery'
  ],

  models: [ 'Delivery' ],
  stores: [ 'Delivery' ],

  launch: function() {
    return this.getController('Main').mainWindow;
  }
});
