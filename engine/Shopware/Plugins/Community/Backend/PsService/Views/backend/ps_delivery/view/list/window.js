Ext.define('Shopware.apps.PsDelivery.view.list.Window', {
    extend: 'Shopware.window.Listing',
    alias: 'widget.ps-delivery-list-window',
    height: 450,
    title : '{s name=window_title}Delivery listing{/s}',

    configure: function() {
        return {
            listingGrid: 'Shopware.apps.PsDelivery.view.list.Delivery',
            listingStore: 'Shopware.apps.PsDelivery.store.Delivery'
        };
    }
});
