Ext.define('Shopware.apps.PsFaq.store.Faq', {
    extend:'Shopware.store.Listing',

    configure: function() {
        return {
            controller: 'PsFaq'
        };
    },
    model: 'Shopware.apps.PsFaq.model.Faq'
});
