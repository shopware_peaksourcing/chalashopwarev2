Ext.define('Shopware.apps.PsFaq', {
  extend: 'Enlight.app.SubApplication',

  name:'Shopware.apps.PsFaq',

  loadPath: '{url action=load}',
  bulkLoad: true,

  controllers: [ 'Main' ],

  views: [
    'list.Window',
    'list.Faq',

    'detail.Window',
    'detail.Faq'
  ],

  models: [ 'Faq' ],
  stores: [ 'Faq' ],

  launch: function() {
    return this.getController('Main').mainWindow;
  }
});
