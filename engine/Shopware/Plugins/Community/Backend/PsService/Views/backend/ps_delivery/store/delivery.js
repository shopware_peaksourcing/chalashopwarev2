Ext.define('Shopware.apps.PsDelivery.store.Delivery', {
    extend:'Shopware.store.Listing',

    configure: function() {
        return {
            controller: 'PsDelivery'
        };
    },
    model: 'Shopware.apps.PsDelivery.model.Delivery'
});
