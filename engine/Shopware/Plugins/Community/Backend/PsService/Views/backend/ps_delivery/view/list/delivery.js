Ext.define('Shopware.apps.PsDelivery.view.list.Delivery', {
    extend: 'Shopware.grid.Panel',
    alias:  'widget.ps-delivery-listing-grid',
    region: 'center',
    configure: function() {
        return {
            detailWindow: 'Shopware.apps.PsDelivery.view.detail.Window'
        };
    }
});
