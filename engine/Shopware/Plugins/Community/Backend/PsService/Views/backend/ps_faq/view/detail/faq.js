Ext.define('Shopware.apps.PsFaq.view.detail.Faq', {
    extend: 'Shopware.model.Container',
    padding: 20,

    configure: function() {
        return {
            controller: 'PsFaq',
            fieldSets: [{
                title: 'Faq data',
                fields: {
                   name: 'Faq'
                }
             }, {
                title: 'Description',
                layout: 'fit',
                fields: {
                   description: {
                      fieldLabel: null,
                      xtype: 'textarea'
                   }
                }
             }]
        };
    }
});
