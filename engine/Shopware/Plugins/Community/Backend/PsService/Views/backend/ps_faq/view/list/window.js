Ext.define('Shopware.apps.PsFaq.view.list.Window', {
    extend: 'Shopware.window.Listing',
    alias: 'widget.ps-faq-list-window',
    height: 450,
    title : '{s name=window_title}Faq listing{/s}',

    configure: function() {
        return {
            listingGrid: 'Shopware.apps.PsFaq.view.list.Faq',
            listingStore: 'Shopware.apps.PsFaq.store.Faq'
        };
    }
});
