Ext.define('Shopware.apps.PsFaq.view.list.Faq', {
    extend: 'Shopware.grid.Panel',
    alias:  'widget.ps-faq-listing-grid',
    region: 'center',
    configure: function() {
        return {
            detailWindow: 'Shopware.apps.PsFaq.view.detail.Window'
        };
    }
});
