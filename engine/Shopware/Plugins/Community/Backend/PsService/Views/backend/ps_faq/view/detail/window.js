Ext.define('Shopware.apps.PsFaq.view.detail.Window', {
    extend: 'Shopware.window.Detail',
    alias: 'widget.ps-faq-detail-window',
    title : '{s name=title}Faq details{/s}',
    height: 420,
    width: 900
});
