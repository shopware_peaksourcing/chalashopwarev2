Ext.define('Shopware.apps.PsDelivery.view.detail.Delivery', {
    extend: 'Shopware.model.Container',
    padding: 20,

    configure: function() {
        return {
            controller: 'PsDelivery',
            fieldSets: [{
                title: 'Delivery data',
                fields: {
                   name: 'Delivery name'
                }
             }, {
                title: 'Additional data',
                layout: 'fit',
                fields: {
                   description: { 
                      fieldLabel: null,
                      xtype: 'tinymce'
                   }
                }
             }]
        };
    }
});
