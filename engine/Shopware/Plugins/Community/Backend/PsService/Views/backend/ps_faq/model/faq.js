Ext.define('Shopware.apps.PsFaq.model.Faq', {
  extend: 'Shopware.data.Model',

  configure: function() {
    return {
      controller: 'PsFaq',
      detail: 'Shopware.apps.PsFaq.view.detail.Faq'
    };
  },

  fields: [
    { name : 'id', type: 'int', useNull: true },
    { name : 'name', type: 'string' },
    { name : 'description', type: 'string', useNull: true }
  ]
});
