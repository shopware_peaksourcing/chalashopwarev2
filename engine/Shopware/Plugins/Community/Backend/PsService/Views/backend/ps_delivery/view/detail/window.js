Ext.define('Shopware.apps.PsDelivery.view.detail.Window', {
    extend: 'Shopware.window.Detail',
    alias: 'widget.ps-delivery-detail-window',
    title : '{s name=title}Delivery details{/s}',
    height: 420,
    width: 900
});
