<?php

class Shopware_Plugins_Backend_PsService_Bootstrap extends Shopware_Components_Plugin_Bootstrap
{
    /**
     * @var bool
     */
    protected $isInitialized = false;

    public function getInfo()
    {
        return array(
            'version' => $this->getVersion(),
            'label' => 'Ps Service',
            'supplier' => 'Peak Sourcing',
            'author' => 'Dragan Radisic',
            'description' => 'Service Page Data',
            'link' => 'URL'
        );
    }

    public function afterInit()
    {
        /*
         * prevent double initialization
         */
        if (!$this->isInitialized) {
            $this->Application()->Loader()->registerNamespace(
                'Shopware\PsService',
                $this->Path()
            );
            $this->isInitialized = true;
        }

        $this->registerCustomModels();
    }

    public function install()
    {
        $this->createDb();
        $this->updateSchema();

        $this->subscribeEvent(
            'Enlight_Controller_Dispatcher_ControllerPath_Backend_PsFaq',
            'getBackendFaqController'
        );
        $this->subscribeEvent(
            'Enlight_Controller_Dispatcher_ControllerPath_Backend_PsDelivery',
            'getBackendDeliveryController'
        );
        $this->subscribeEvent(
            'Enlight_Controller_Action_PostDispatchSecure_Frontend',
            'onPostDispatchIndex'
        );

        $this->createMenuItem(array(
            'label' => 'Faq',
            'controller' => 'PsFaq',
            'class' => 'sprite-application-block',
            'action' => 'Index',
            'active' => 1,
            'parent' => $this->Menu()->findOneBy(['label' => 'Artikel'])
        ));

        $this->createMenuItem(array(
            'label' => 'Delivery',
            'controller' => 'PsDelivery',
            'class' => 'sprite-application-block',
            'action' => 'Index',
            'active' => 1,
            'parent' => $this->Menu()->findOneBy(['label' => 'Artikel'])
        ));

        $form = $this->Form();
        $form->setElement('text', 'faqLink', array(
            'label' => 'Alle Fragen und Antworten zeigen',
            'value' => '/service/fragen-und-antworten/',
            'required' => true
        ));

        return [
            'success' => true,
            'invalidateCache' => [
                'frontend',
                'backend',
                'config',
                'http'
            ]
        ];
    }

    public function getBackendFaqController(Enlight_Event_EventArgs $args)
    {
        // Register the template directory to not have to provide it
        // in the controller every time
        $this->Application()->Template()->addTemplateDir(
            $this->Path() . 'Views/'
        );

        // Register the models to have access to them in the controller
        $this->registerCustomModels();

        return $this->Path() . '/Controllers/Backend/PsFaq.php';
    }

    public function getBackendDeliveryController(Enlight_Event_EventArgs $args)
    {
        // Register the template directory to not have to provide it
        // in the controller every time
        $this->Application()->Template()->addTemplateDir(
            $this->Path() . 'Views/'
        );

        // Register the models to have access to them in the controller
        $this->registerCustomModels();

        return $this->Path() . '/Controllers/Backend/PsDelivery.php';
    }

    protected function updateSchema()
    {
        $this->registerCustomModels();

        $em = Shopware()->Models();
        $em = $this->Application()->Models();
        $tool = new \Doctrine\ORM\Tools\SchemaTool($em);

        $classes = array(
            $em->getClassMetadata('Shopware\CustomModels\Faq'),
            $em->getClassMetadata('Shopware\CustomModels\Delivery')
        );

        try {
            $tool->dropSchema($classes);
        } catch (Exception $e) {
            //ignore
        }

        $tool->createSchema($classes);
    }

    public function uninstall()
    {
        $this->registerCustomModels();

        $em = $this->Application()->Models();
        $tool = new \Doctrine\ORM\Tools\SchemaTool($em);
        $classes = array(
            $em->getClassMetadata('Shopware\CustomModels\Faq'),
            $em->getClassMetadata('Shopware\CustomModels\Delivery')
        );
        $tool->dropSchema($classes);

        return true;
    }

    public function createDb()
    {
        $sql = "
            CREATE TABLE IF NOT EXISTS `s_faq` (
                `id` int(11) NOT NULL,
                `name` text COLLATE utf8_unicode_ci NOT NULL,
                `description` longtext COLLATE utf8_unicode_ci,
            PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
        ";
        Shopware()->Db()->query($sql);

        $sql = "
            CREATE TABLE IF NOT EXISTS `s_delivery` (
                `id` int(11) NOT NULL,
                `name` text COLLATE utf8_unicode_ci NOT NULL,
                `description` longtext COLLATE utf8_unicode_ci,
            PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
        ";
        Shopware()->Db()->query($sql);
    }

    public function onPostDispatchIndex(Enlight_Event_EventArgs $args)
    {
        /** @var \Enlight_Controller_Action $controller */
        $controller = $args->get('subject');

        /** Add Template directory */
        $view = $controller->View();
        $view->addTemplateDir(
            __DIR__ . '/Views'
        );
        /*         echo '<pre class="debug">';
                \Doctrine\Common\Util\Debug::dump($this->Config()->faqLink);
                echo '</pre>'; die; */

        $faq = Shopware()->Models()->createQueryBuilder()->select('s')->from('\Shopware\CustomModels\Faq',
            's')->getQuery()->getArrayResult();
        $delivery = Shopware()->Models()->createQueryBuilder()->select('d')->from('\Shopware\CustomModels\Delivery',
            'd')->getQuery()->getArrayResult();


        $view->assign('sFaq', $faq);
        $view->assign('sDelivery', $delivery);
        $view->assign('sFaqLink', $this->Config()->faqLink);

    }

}
                                       