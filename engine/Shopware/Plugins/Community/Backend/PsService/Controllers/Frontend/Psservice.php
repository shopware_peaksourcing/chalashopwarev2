<?php

/**
 * Class Shopware_Controllers_Backend_PsFaq
 *
 *
 * Copyright (C) 2016 Dragan Radisic, Peak Sourcing
 *
 * This plugin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This plugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see LICENSE.txt). If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * @author Dragan Radisic <dradisic@peak-sourcing.com>
 *
 */
 class Shopware_Controllers_Frontend_Psfaq extends Enlight_Controller_Action
 {
     public function indexAction()
     {
         die('Hello world');
     }
 }
