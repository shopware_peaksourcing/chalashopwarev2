<?php

class Shopware_Plugins_Frontend_ChalaConfiguration_Bootstrap extends Shopware_Components_Plugin_Bootstrap
{
    public function getVersion()
    {
        return '1.0.0';
    }

    public function getLabel()
    {
        return 'Chala Configuration';
    }

    public function install()
    {
        $this->subscribeEvent(
            'Enlight_Controller_Action_PostDispatchSecure_Frontend',
            'onFrontendPostDispatch'
        );

        /* $this->createConfig(); */

        return true;
    }

    public function onFrontendPostDispatch(Enlight_Event_EventArgs $args)
    {
        //setlocale(LC_ALL, 'de_DE');
        /** @var \Enlight_Controller_Action $controller */
        $controller = $args->get('subject');
        $view = $controller->View();
        $sArticle = $controller->View()->getAssign('sArticle');
        $attributes = array();
        $sView = $args->getSubject()->View();
        $repository = Shopware()->Models()->getRepository('Shopware\Models\Article\Detail');
       // $catRepository = Shopware()->Models()->getRepository('Shopware\Models\Category\Category');
  	    /*  echo '<pre class="debug">';
        \Doctrine\Common\Util\Debug::dump($controller);
        echo '</pre>'; die;  */

        /*Get Main Categories*/
        $sMainCategories = Shopware()->Modules()->Categories()->sGetMainCategories();

            foreach ($sMainCategories as $key => $sMainCategory){
                if($sMainCategory['attribute']['productConfigurator'] != null) {

                    $result = $repository->findByNumber($sMainCategory['attribute']['productConfigurator']);

					if($result != null) {
						$detail =  "?sViewport=detail&sArticle=". $result[0]->getArticle()->getId();
						if ($sMainCategory['id']) {
							$detail .= '&sCategory='.$sMainCategory['id'];
						}
						$configuratorLink = Shopware()->Modules()->Core()->sRewriteLink($detail, $result[0]->getArticle()->getName());
					}

               }
            }
            foreach ($sMainCategories as $key => $sMainCategory){
                if($sMainCategory['attribute']['productsInMenu'] != null) {

                    $sMenuProducts['products_in_menu'][$key] = explode('|',substr($sMainCategory["attribute"]["productsInMenu"], 1, -1));
                    foreach ($sMenuProducts['products_in_menu'][$key] as $keyProduct => $sMenuProduct){
                        $articleDetail = Shopware()->Models()->getRepository('Shopware\Models\Article\Detail')->findByNumber($sMenuProduct)[0];
						if($articleDetail != null) {
							$articleId = $articleDetail
								->getArticle()
								->getId();
							$sMenuSelectedProducts[$keyProduct]['id'] = $articleId;
							$sMenuSelectedProducts[$keyProduct]['attributes'] = $articleDetail
								->getArticle()
								->getAttribute();
							$sMenuSelectedProducts[$keyProduct]['name'] = $articleDetail
								->getArticle()
								->getName();

							foreach ($articleDetail->getArticle()->getImages() as $image) {
							  if ($image->getMain() == 1) {
								$sMenuSelectedProducts[$keyProduct]['image'] = $image->getMedia()->getThumbnailFilePaths();
								break;
							  }
							}
							$sMenuSelectedProducts[$keyProduct]['link'] =
								Shopware()
								->Modules()
								->Core()
								->sRewriteLink(
							"?sViewport=detail&sArticle=". $articleId .= '&sCategory='.$sMainCategory['id'],
									$articleDetail
									->getArticle()
									->getName()[0]					
								);
						}
                    }
                }
            }

		// Initilize advancedMenu
		$advancedMenu = Shopware()->Plugins()->get('Frontend')->get('AdvancedMenu');
		
		// Get Parent Id
        $categoryParentId = Shopware()->Shop()->get('parentID');
		
		// Get Current Id
        $currentCategoryId = $args->getRequest()->getParam('sCategory', $categoryParentId);
		
		// Get Current Category Content
		$categoryContent = Shopware()->Modules()->Categories()->sGetCategoryContent($currentCategoryId);	
		
		// Get Parent Category Content
		$categoryParent = Shopware()->Modules()->Categories()->sGetCategoryContent($categoryContent["parentId"]);
		
		// Get Category Contetn Chosent as Parent for Category filter
		$categoryMenuParent = Shopware()->Modules()->Categories()->sGetCategoryContent($categoryContent["attribute"]["parent_category_filter"]);
		
		if($categoryContent["attribute"]["parent_category_filter"] != NULL){
			// Set Menu from subcategories of chosen parent Category 
			$subCatogoriesMenu = $advancedMenu->getAdvancedMenu($categoryContent["attribute"]["parent_category_filter"], $categoryContent["attribute"]["parent_category_filter"], 20);
			
			// Prepend Parent Category
			array_unshift($subCatogoriesMenu, $categoryMenuParent);
			
			// Set Active states
			$subCatogoriesMenu = $this->setActiveFlags($subCatogoriesMenu, array($currentCategoryId));
		}
				
        $view->addTemplateDir(
            __DIR__ . '/Views'
        );

        $view->assign('configuratorLink', $configuratorLink);
        $view->assign('subCatogoriesMenu', $subCatogoriesMenu);
        $view->assign('currentCategoryId', $currentCategoryId);
        $view->assign('categoryParent', $categoryParent);
        $view->assign('categoryContent', $categoryContent);
        $view->assign('categoryMenuParent', $categoryMenuParent);
        $view->assign('customArticleList', Shopware()->Modules()->Articles()->sGetArticlesByCategory()["sArticles"]);
        $view->assign('sMenuSelectedProducts', $sMenuSelectedProducts);

    }
	
    /**
     * @param array[] $categories
     * @param int[] $actives
     * @return array[]
     */
    private function setActiveFlags($categories, $actives)
    {
        foreach ($categories as &$category) {
            $category['flag'] = in_array($category['id'], $actives);


            if (!empty($category['sub'])) {
                $category = $this->setActiveFlags($category, $actives);
            }
        }
        return $categories;
    }

}
