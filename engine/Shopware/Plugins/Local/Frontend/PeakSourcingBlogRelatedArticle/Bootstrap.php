<?php
class Shopware_Plugins_Frontend_PeakSourcingBlogRelatedArticle_Bootstrap
    extends Shopware_Components_Plugin_Bootstrap
{
    /**
     * @var \Shopware\Models\Blog\Repository
     */
    protected $repository;
	
    /**
     * Helper Method to get access to the blog repository.
     *
     * @return Shopware\Models\Blog\Repository
     */
    public function getRepository()
    {
        if ($this->repository === null) {
            $this->repository = Shopware()->Models()->getRepository('Shopware\Models\Blog\Blog');
        }
        return $this->repository;
    }
	
    public function getCapabilities()
    {
        return [
            'install' => true,
            'update' => true,
            'enable' => true,
            'delete' => true
        ];
    }

    public function getLabel()
    {
        return 'Related Blog Articles';
    }

    public function getVersion()
    {
        return '1.0.0';
    }

    public function getInfo()
    {
        return [
            'version' => $this->getVersion(),
            'label' => $this->getLabel(),
            'supplier' => 'Peak Sourcing',
            'author' => 'Peak Sourcing',
            'description' => 'Display Related Blog Articles',
            'link' => 'URL'
        ];
    }

    public function install()
    {
        try
        {
            $this->registerEvents();

            return [
                'success' => true,
                'invalidateCache' => [
                    'frontend',
                    'backend',
                    'config',
                    'http'
                ]
            ];
        }
        catch(Exception $e) {
            return [
                'success' => false,
                'message' => $e->getMessage()
            ];
        }
    }

    public function update()
    {
        return true;
    }

    public function uninstall()
    {
        return [
            'success' => true,
            'invalidateCache' => [
                'fronend',
                'backend',
                'config',
                'http'
            ]
        ];
    }

    public function registerEvents()
    {
        $this->subscribeEvent(
            'Enlight_Controller_Action_PostDispatchSecure_Frontend',
            'onFrontendPostDispatch'
        );
    }

    public function onFrontendPostDispatch(Enlight_Event_EventArgs $args)
    {
        /** @var Shopware_Controllers_Frontend_Blog $subject */
        $subject = $args->getSubject();
        $request = $subject->Request();
		$view = $subject->View();
		$sArticle = $subject->View()->getAssign('sArticle');

		// Get Parent Id
        $categoryParentId = Shopware()->Shop()->get('parentID');
		
		// Get Current Id
        $currentCategoryId = $args->getRequest()->getParam('sCategory', $categoryParentId);
		
		// Get Current Category Content
		$categoryContent = Shopware()->Modules()->Categories()->sGetCategoryContent($currentCategoryId);
		
		$listRelated = explode('|',substr($sArticle["attribute"]["related"], 1, -1));
		if(!empty($listRelated)){
			$sRelatedBlogArticles = $this->raltionBlogToBlogAction($listRelated);
			$sArticle['sRelatedBlogArticles'] = $sRelatedBlogArticles;
		}
		$allBlogArticles = $this->getRepository()->getListQuery()->getArrayResult();
		foreach ($allBlogArticles as $key => $homeBlogArticle) {
			if (isset($homeBlogArticle["attribute"]["displayOnHomePageSlider"]) && isset($homeBlogArticle["active"])){
				$homeBlogArticlesSlider[$key]= $homeBlogArticle;
			}
			if (isset($homeBlogArticle["attribute"]["displayOnHomePage"]) && isset($homeBlogArticle["active"])){
				$homeBlogArticles[$key]= $homeBlogArticle;
			}
			
		}
		
/* 		echo '<pre class="debug">';
			\Doctrine\Common\Util\Debug::dump($relatedBlogData[0]["double_images"]);
        echo '</pre>'; */
		$view->addTemplateDir(
			__DIR__ . '/Views'
		);
		$view->assign('homeBlogArticles', $homeBlogArticles);
		$view->assign('homeBlogArticlesSlider', $homeBlogArticlesSlider);
		$view->assign('relatedBlog', $this->relatedArticlesAction($sArticle["related_blog"]));
		$view->assign('relatedBlogCategory', $this->relatedArticlesAction($categoryContent["attribute"]["related_blog"]));
		$subject->View()->assign('sArticle', $sArticle);	
		
    }
	
	public function raltionBlogToBlogAction($selectedArticles){
		if( $selectedArticles != NULL){
			foreach($selectedArticles as $key => $selectedArticle){		
				$article = $this
				->getRepository()->getDetailQuery(intval($selectedArticle))->getOneOrNullResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);

				$relatedArticles[$key]['imagePath'] = $article['media'][0]['media']['path'];
				$relatedArticles[$key]['title'] = $article['title'];
				$relatedArticles[$key]['displayDate'] = $article['displayDate'];
				$relatedArticles[$key]['categoryId'] = $article['categoryId'];
				$relatedArticles[$key]['id'] = $article['id'];
				$relatedArticles[$key]['authorName'] = $article['author'];
			}
		return $relatedArticles;
		}
	}

	public function relatedArticlesAction($selectedArticles){
		if( $selectedArticles != NULL){
			foreach (explode('|',substr($selectedArticles, 1, -1)) as $key => $relatedBlogArticle){
				$relatedBlog[$key] = Shopware()->Models()->find('Shopware\Models\Blog\Blog', $relatedBlogArticle);
				if ($relatedBlog[$key] != NULL){
					if ($relatedBlog[$key]->getActive() != NULL){
						$relatedBlogData[$key]['title'] = $relatedBlog[$key]->getTitle();
						$relatedBlogData[$key]['id'] = $relatedBlog[$key]->getId();
						$relatedBlogData[$key]['categoryId'] = $relatedBlog[$key]->getCategory()->getId();
						$relatedBlogData[$key]['description'] = $relatedBlog[$key]->getDescription();
						$relatedBlogData[$key]['image'] = $relatedBlog[$key]->getMedia()[0]->getMedia()->getThumbnailFilePaths();
						/*
						$relatedBlogData[$key]['left_article_title'] = $relatedBlog[$key]->getAttribute()->getLeftArticleTitle();
						$relatedBlogData[$key]['left_article_link'] = $relatedBlog[$key]->getAttribute()->getLeftArticleLink();
						$relatedBlogData[$key]['left_article_image'] = $relatedBlog[$key]->getAttribute()->getLeftArticleImage();
						$relatedBlogData[$key]['right_article_title'] = $relatedBlog[$key]->getAttribute()->getRightArticleTitle();
						$relatedBlogData[$key]['right_article_link'] = $relatedBlog[$key]->getAttribute()->getRightArticleLink();
						$relatedBlogData[$key]['right_article_image'] = $relatedBlog[$key]->getAttribute()->getRightArticleImage();
						*/
						$relatedBlogData[$key]['icon'] = $relatedBlog[$key]->getAttribute()->getIcon();
						$relatedBlogData[$key]['use_video_link'] = $relatedBlog[$key]->getAttribute()->getUseVideoLink();
						$relatedBlogData[$key]['double_images'] = explode('|',substr($relatedBlog[$key]->getAttribute()->getDoubleImages(), 1, -1));
					}
				}
			}
			return $relatedBlogData;
		}
	}
}