<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage PluginsModifier
 */
/**
 * Smarty spacify modifier plugin
 *
 * Type:     modifier<br>
 * Name:     spacify<br>
 * Purpose:  add spaces between characters in a string
 *
 * @link http://smarty.php.net/manual/en/language.modifier.spacify.php spacify (Smarty online manual)
 * @author Dragan Radisic <dradisic@peak-sourcing.com>
 * @param int $parentId id
 * @return array
 */
function smarty_modifier_parent($parentId)
{
	if($parentId != null){
		return Shopware()->Modules()->sCategories()->sGetCategoriesByParent($parentId);
	}
	return '';
}
?>
