<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage PluginsModifier
 */
/**
 * Smarty spacify modifier plugin
 *
 * Type:     modifier<br>
 * Name:     spacify<br>
 * Purpose:  add spaces between characters in a string
 *
 * @link http://smarty.php.net/manual/en/language.modifier.spacify.php spacify (Smarty online manual)
 * @author Dragan Radisic <dradisic@peak-sourcing.com>
 * @param int $imageId id
 * @return string
 */
function smarty_modifier_articleMainImage($id)
{
	if ($id){
		$article = Shopware()->Models()->getRepository('Shopware\Models\Article\Article')->findOneById($id);
		if ($article && $article->getImages()) {
			foreach ($article->getImages() as $image) {
				if ($image->getMain() == 1) {
					return [
						'source' => $image->getMedia()->getPath(),
						'thumbnails' => $image->getMedia()->getThumbnailFilePaths(),
					];
				}
			}
		}
	}
	return '';
}
?>
