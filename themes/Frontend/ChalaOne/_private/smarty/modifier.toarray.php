<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage PluginsModifier
 */
/**
 * Smarty spacify modifier plugin
 *
 * Type:     modifier<br>
 * Name:     spacify<br>
 * Purpose:  returns array of multiple selection models
 *
 * @link http://smarty.php.net/manual/en/language.modifier.spacify.php spacify (Smarty online manual)
 * @author Dragan Radisic <dradisic@peak-sourcing.com>
 * @param int $imagesId id
 * @return array
 */
function smarty_modifier_toarray($imagesId)
{

    $imageList = explode('|',substr($imagesId, 1, -1));
    return $imageList ? $imageList : '';
}