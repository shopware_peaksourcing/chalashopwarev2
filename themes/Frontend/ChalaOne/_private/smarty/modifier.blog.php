<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage PluginsModifier
 */
/**
 * Smarty spacify modifier plugin
 *
 * Type:     modifier<br>
 * Name:     spacify<br>
 * Purpose:  add spaces between characters in a string
 *
 * @link http://smarty.php.net/manual/en/language.modifier.spacify.php spacify (Smarty online manual)
 * @author Dragan Radisic <dradisic@peak-sourcing.com>
 * @param int $blogId id
 * @return object
 */
function smarty_modifier_blog($blogId)
{
    if($blogId != null){
        $blogModel = Shopware()->Models()->find('Shopware\Models\Blog\Blog', $blogId);
        return (array)$blogModel ? (array)$blogModel : '';
    }else{
        return '';
    }
}
?>