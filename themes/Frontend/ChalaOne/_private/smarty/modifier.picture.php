<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage PluginsModifier
 */
/**
 * Smarty spacify modifier plugin
 * 
 * Type:     modifier<br>
 * Name:     spacify<br>
 * Purpose:  add spaces between characters in a string
 * 
 * @link http://smarty.php.net/manual/en/language.modifier.spacify.php spacify (Smarty online manual)
 * @author Dragan Radisic <dradisic@peak-sourcing.com>
 * @param int $imageId id
 * @return string
 */
function smarty_modifier_picture($imageId)
{
	if($imageId != null){
		$mediaModel = Shopware()->Models()->find('Shopware\Models\Media\Media', $imageId);
		return $mediaModel->getPath() ? $mediaModel->getPath() : '';
	}else{
		return '';
	}
} 
?>