<?php

/**
 * Smarty plugin
 * @package Smarty
 * @subpackage PluginsModifier
 */
/**
 * Smarty spacify modifier plugin
 *
 * Type:     modifier<br>
 * Name:     spacify<br>
 * Purpose:  add spaces between characters in a string
 *
 * @link http://smarty.php.net/manual/en/language.modifier.spacify.php spacify (Smarty online manual)
 * @author Dragan Radisic <dradisic@peak-sourcing.com>
 * @param int $articleId id
 * @param int $categoryId id
 * @return string
 */
function smarty_modifier_article($articleId)
{
	if($articleId != null){
		$articleModel = Shopware()->Models()->find('Shopware\Models\Article\Article', $articleId);
		return [
			'name' => $articleModel->getName(),
			'attributes' => $articleModel->getAttribute(),
/* 			'thumbnail' => $mediaModel->getThumbnailFilePaths(),
			'filename' => $mediaModel->getFileName(),
			'filesize' => $mediaModel->getFormattedFileSize(),
			'extension' => $mediaModel->getExtension() */
		];
	}
	return '';
}