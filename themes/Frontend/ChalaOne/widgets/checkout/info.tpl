{* Notepad entry *}
{block name="frontend_index_checkout_actions_notepad"}
{/block}

{* My account entry *}
{block name="frontend_index_checkout_actions_my_options"}
    <li class="navigation--entry entry--account login" role="menuitem">
        {block name="frontend_index_checkout_actions_account"}
            <a id="user" href="{url controller='account'}"
               title="{"{s namespace='frontend/index/checkout_actions' name='IndexLinkAccount'}{/s}"|escape}"
               class="is--icon-left entry--link account--link">
                {if $sUserLoggedIn && $sUserData}
                    {$sUserData.additional.user.firstname} {$sUserData.additional.user.lastname}
                {else}
                    {s namespace='frontend/index/checkout_actions' name='IndexLinkAccount'}{/s}
                {/if}
            </a>
            {include file="frontend/index/login-box.tpl"}
        {/block}
    </li>
{/block}

{* Cart entry *}
{block name="frontend_index_checkout_actions_cart"}
    <li class="navigation--entry entry--cart cart hidden-xs" role="menuitem">
        <a id="cart" class="is--icon-left cart--link" href="{url controller='checkout' action='cart'}"
           title="{"{s namespace='frontend/index/checkout_actions' name='IndexLinkCart'}{/s}"|escape}">
                <span class="cart--display">
                    {if $sUserLoggedIn}
                        {s namespace='frontend/index/checkout_actions' name='IndexLinkCheckout'}{/s}
                    {else}
                        {s namespace='frontend/index/checkout_actions' name='IndexLinkCart'}{/s}
                    {/if}
                </span>
            {if $sBasketQuantity}
                <span class="badge is--primary is--minimal cart--quantity{if $sBasketQuantity < 1} is--hidden{/if}">{$sBasketQuantity}</span>
            {/if}
        </a>
        <div class="ajax-loader">&nbsp;</div>

        {block name='frontend_index_container_ajax_cart'}
            <div class="container--ajax-cart-dropdown" data-collapse-cart="true" data-displayMode="dropdownmenu"></div>
        {/block}
    </li>
    <li class="navigation--entry entry--cart mobile cart visible-xs" role="menuitem">
        <a id="cart" class="is--icon-left cart--link" href="{url controller='checkout' action='cart'}"
           title="{"{s namespace='frontend/index/checkout_actions' name='IndexLinkCart'}{/s}"|escape}">
                <span class="cart--display">
                    {if $sUserLoggedIn}
                        {s namespace='frontend/index/checkout_actions' name='IndexLinkCheckout'}{/s}
                    {else}
                        {s namespace='frontend/index/checkout_actions' name='IndexLinkCart'}{/s}
                    {/if}
                </span>
            <span class="badge is--primary is--minimal cart--quantity{if $sBasketQuantity < 1} is--hidden{/if}">{$sBasketQuantity}</span>
        </a>
        <div class="ajax-loader hidden-xs">&nbsp;</div>

        {*{block name='frontend_index_container_ajax_cart'}*}
            {*<div class="container--ajax-cart" data-collapse-cart="true" data-displayMode="offcanvas"></div>*}
        {*{/block}*}
    </li>
{/block}

{block name="frontend_index_checkout_actions_inner"}{/block}
