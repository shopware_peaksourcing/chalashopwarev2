{block name="widget_emotion_component_youtube"}
    <section class="section narrow">
        <div class="container">
        {if $Data && $Data.video_id}
            {if $Data.video_hd}{$options[] = 'hd=1&vq=hd720'}{/if}
            {if $Data.video_autoplay}{$options[] = 'autoplay=1'}{/if}
            {if $Data.video_related}{$options[] = 'rel=0'}{/if}
            {if $Data.video_controls}{$options[] = 'controls=0'}{/if}
            {if $Data.video_start}{$options[] = 'start='|cat:$Data.video_start}{/if}
            {if $Data.video_end}{$options[] = 'end='|cat:$Data.video_end}{/if}
            {if $Data.video_info}{$options[] = 'showinfo=0'}{/if}
            {if $Data.video_branding}{$options[] = 'modestbranding=1'}{/if}
            {if $Data.video_loop}{$options[] = 'loop=1&playlist='|cat:$Data.video_id}{/if}
            {if $options|@count > 0}
                {foreach $options as $option}
                    {if $option@first}
                        {$params = "?{$option}"}
                    {else}
                        {$params = "$params&$option"}
                    {/if}
                {/foreach}
            {/if}
			
            <div class="row">
                <h2>{if $emotion.name}{$emotion.name}{/if}</h2>
            </div>
            <div class="row h_list video scissors">
                <div class="col-xs-12 col-sm-12 col-md-8 col-md-push-4 col-lg-8 image">
					<img src="https://img.youtube.com/vi/{$Data.video_id}/hqdefault.jpg">
					<a href="https://www.youtube.com/embed/{$Data.video_id}{if $params}{$params}{/if}?autoplay=1" class="various fancybox.iframe">
					</a>	
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-md-pull-8 col-lg-4 text">
					<h3>{if $emotion.attribute.videoTextTitle}{$emotion.attribute.videoTextTitle}{/if}</h3>
                    {if $emotion.attribute.videoText}{$emotion.attribute.videoText}{/if}
                </div>
            </div>
        {/if}
        </div>
    </section>
{/block}