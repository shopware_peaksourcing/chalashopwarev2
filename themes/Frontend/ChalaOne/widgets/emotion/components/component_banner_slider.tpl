{extends file="parent:widgets/emotion/components/component_banner_slider.tpl"}
    {block name="frontend_widgets_banner_slider"}
        {block name="frontend_widgets_banner_slider_container"}
            {block name="frontend_widgets_banner_slider_slide"}
                <section id="home_slider">
                    <div class="slider">
                        {foreach $Data.values as $banner}
                            {block name="frontend_widgets_banner_slider_item"}
								{block name="frontend_widgets_banner_slider_banner"}
								
									<div class="item evo">
                                            {block name="frontend_widgets_banner_slider_banner_picture"}
                                                {if $banner.thumbnails}
                                                    {$baseSource = $banner.thumbnails[0].source}
                                                    {$srcSet = ''}
                                                    {$itemSize = ''}

                                                    {foreach $element.viewports as $viewport}
                                                        {$cols = ($viewport.endCol - $viewport.startCol) + 1}
                                                        {$elementSize = $cols * $cellWidth}
                                                        {$size = "{$elementSize}vw"}

                                                        {if $breakpoints[$viewport.alias]}

                                                            {if $viewport.alias === 'xl' && !$emotionFullscreen}
                                                                {$size = "calc({$elementSize / 100} * {$baseWidth}px)"}
                                                            {/if}

                                                            {$size = "(min-width: {$breakpoints[$viewport.alias]}) {$size}"}
                                                        {/if}

                                                        {$itemSize = "{$size}{if $itemSize}, {$itemSize}{/if}"}
                                                    {/foreach}

                                                    {foreach $banner.thumbnails as $image}
                                                        {$srcSet = "{if $srcSet}{$srcSet}, {/if}{$image.source} {$image.maxWidth}w"}

                                                        {if $image.retinaSource}
                                                            {$srcSet = "{if $srcSet}{$srcSet}, {/if}{$image.retinaSource} {$image.maxWidth * 2}w"}
                                                        {/if}
                                                    {/foreach}
                                                {else}
                                                    {$baseSource = $banner.source}
                                                {/if}
                                                <img src="{$banner.source}"
                                                     class="banner-slider--image"
                                                     {if $banner.altText}alt="{$banner.altText|escape}" {/if}/>
                                            {/block}
										<div class="price_tag">
											{if $banner.title}<p class="model">{$banner.title}</p>{/if}
											{if $banner.altText}<p class="price"><span class="from">ab</span> <span class="bold">{$banner.altText}</span>€</p>{/if}
										</div>
									</div>
								{/block}
							{/block}
                        {/foreach}
                    </div>
                </section>
            {/block}
        {/block}
    {/block}