{* Slider panel *}
{block name="widget_emotion_component_product_slider"}
    <section id="home_models" class="models_list section{if !$Data.no_border} has--border{/if}">
        <div class="container">
            <div class="row">
				{* Title *}
				{block name="widget_emotion_component_product_slider_title"}
					{if $Data.article_slider_title}
						<h2>
							{$Data.article_slider_title}
						</h2>
					{/if}
				{/block}
            </div>
            <div class="row list">
				{* Slider content based on the configuration *}
				{block name="widget_emotion_component_product_slider_content"}
					{if $Data.article_slider_type == 'selected_article'}
						{$articles = $Data.values}
					{/if}
					{include file="frontend/_includes/product_slider.tpl"}
				{/block}
            </div>
        </div>
    </section>
{/block}
