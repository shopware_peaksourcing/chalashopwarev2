{block name="widget_emotion_component_blog"}
	{if $Data}
		{block name="widget_emotion_component_blog_container"}
			{if ! $emotion.attribute.chalaStories}
				<section class="section narrow">
					<div class="container">
						{foreach $Data.entries as $key => $entry name=ent}
							{block name="widget_emotion_component_blog_entry"}
								<div class="row h_list stories">
									<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 image">
										{block name="widget_emotion_component_blog_entry_image"}
											{if $entry.media.thumbnails}
												{$images = $entry.media.thumbnails}
													<img src="{$images[2].source}">
											{else}
												<a class="blog--image"
												   href="{url controller=blog action=detail sCategory=$entry.categoryId blogArticle=$entry.id}"
												   title="{$entry.title|escape}">
													{s name="EmotionBlogPreviewNopic"}{/s}
												</a>
											{/if}
										{/block}
									</div>
									<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 text">
										{block name="widget_emotion_component_blog_entry_title"}
										   <h3>{$entry.title|truncate:40}</h3>
										{/block}
										<p class="date">
											{$entry.displayDate|date_format}
											{if $entry.author.name}
												 von {s name="BlogInfoFrom"}{/s}: {$entry.author.name}
											{/if}
										</p>
										{block name="widget_emotion_component_blog_entry_description"}
											{if $entry.description}
												{$entry.description|unescape:"html"|truncate:540}
											{/if}
										{/block}
									</div>
								</div>
							{/block}
						{/foreach}
					</div>
				</section>
			{else}
				<section class="section narrow" {if $emotion.attribute.chalaStories}id="leading_story"{/if}>
					<div class="container">
						{if $emotion.attribute.title}
							<div class="row">
								<h2>{$emotion.attribute.title}</h2>
							</div>
						{/if}
						<div class="row stories_nav top hidden-md hidden-lg">
							<div class="col-xs-12 col-sm-12">
								<a href="#" class="prev" data-slick="slickPrev"><span>vorherige</span></a>
								<a href="#" class="next" data-slick="slickNext"><span>nächste</span></a>
							</div>
						</div>
						<div class="blog-slider">
							{foreach $Data.entries as $key => $entry name=ent}
								{if isset($entry.attribute.display_on_home_page)}
									{block name="widget_emotion_component_blog_entry"}									
										<div class="row h_list {if $smarty.foreach.ent.iteration == 1}leaf{else}support{/if}">
											{block name="widget_emotion_component_blog_entry_image"}
												{if $entry.media.thumbnails}
													{$images = $entry.media.thumbnails}
													<div class="col-xs-12 col-sm-12 col-md-7 {if $smarty.foreach.ent.iteration == 1}col-md-push-5{/if} col-lg-7 image">
														<img src="{$images[2].source}">
													</div>
												{else}
													<a class="blog--image"
													   href="{url controller=blog action=detail sCategory=$entry.categoryId blogArticle=$entry.id}"
													   title="{$entry.title|escape}">
														{s name="EmotionBlogPreviewNopic"}{/s}
													</a>
												{/if}
											{/block}
											<div class="col-xs-12 col-sm-12 col-md-5 {if $smarty.foreach.ent.iteration == 1}col-md-pull-7{/if} col-lg-5 text">
												{block name="widget_emotion_component_blog_entry_title"}
												   <h3>{$entry.title|truncate:40}</h3>
												{/block}

												{block name="widget_emotion_component_blog_entry_description"}
													<p>
														{if $entry.description}
															{$entry.description|unescape:"html"|truncate:540}
														{/if}
													</p>
												{/block}
											</div>
										</div>
									{/block}
								{/if}
							{/foreach}
								<div class="row stories_nav blog_home_nav">
									<div class="hidden-xs hidden-sm col-md-12 col-lg-12">
										<a href="#" class="prev" data-slick="slickPrev">
											<span>
												{if $Data.entries[0]}
													{$Data.entries[0].title|truncate:40}
												{/if}
											</span>
										</a>
										<span class="separator">//</span>
										<a href="#" class="next" data-slick="slickNext">
											<span>
												{if $Data.entries[1]}
													{$Data.entries[1].title|truncate:40}
												{/if}
											</span>
										</a>
									</div>
								</div>
						</div>
					</div>
				</section>
			{/if}
		{/block}
	{/if}

{/block}
