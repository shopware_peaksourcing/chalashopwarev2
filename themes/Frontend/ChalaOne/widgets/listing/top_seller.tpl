{block name="widgets_listing_top_seller"}
    {if $sCharts|@count}
        {block name="widgets_listing_top_seller_panel"}

            {if $sNav}
                {block name="widgets_listing_top_seller_panel_inner"}
                    {block name="widgets_listing_top_seller_slider"}
                        {include file="frontend/_includes/product_nav.tpl" articles=$sCharts}
                    {/block}
                {/block}
            {else}
                <div class="topseller panel has--border is--rounded">
                    {block name="widgets_listing_top_seller_panel_inner"}
                        {block name="widgets_listing_top_seller_slider"}
                            {include file="frontend/_includes/product_slider.tpl" nav=TRUE articles=$sCharts productSliderCls="topseller--content panel--body"}
                        {/block}
                    {/block}
                </div>
            {/if}
        {/block}
    {/if}
{/block}