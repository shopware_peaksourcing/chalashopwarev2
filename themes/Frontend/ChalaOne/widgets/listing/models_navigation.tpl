{block name="widgets_listing_top_seller"}
    {if $sCharts|@count}
        {block name="widgets_listing_top_seller_panel"}
            <ul class="drop">
                {block name="widgets_listing_top_seller_panel_inner"}

                    {block name="widgets_listing_top_seller_title"}
                    {/block}

                    {block name="widgets_listing_top_seller_slider"}
                        {include file="frontend/_includes/product_slider.tpl" articles=$sCharts productSliderCls="topseller--content panel--body"}
                    {/block}
                {/block}
            </ul>
        {/block}
    {/if}
{/block}