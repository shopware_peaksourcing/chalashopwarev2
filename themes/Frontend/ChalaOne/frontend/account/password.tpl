{extends file='frontend/index/index.tpl'}

{* Main content *}
{block name='frontend_index_content'}
	<div class="content account--password-reset">

		{* Error messages *}
		{block name='frontend_account_error_messages'}
			{if $sErrorMessages}
				<div class="account--error">
					{include file="frontend/register/error_message.tpl" error_messages=$sErrorMessages}
				</div>
			{/if}
		{/block}

		{if $sSuccess}
			{* Success message *}
			{block name='frontend_account_password_success'}
				{block name="frontend_account_password_reset_headline"}
					<h2 class="password-reset--title panel--title is--underline">{s name="PasswordHeader"}{/s}</h2>
				{/block}
				<div class="forma-white-background col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<div class="password--success">
						{include file="frontend/_includes/password_messages.tpl" type="success" content="{s name='PasswordInfoSuccess'}{/s}"}
					</div>
					<a href="{url controller='account' action='password'}"
					   class="btn is--secondary is--icon-left">
						{*<i class="icon--arrow-left"></i>*}{s name="LoginBack"}{/s}
					</a>
				</div>
			{/block}
		{else}
			{* Recover password *}
			{block name="frontend_account_password_reset"}
				<div class="password-reset--content panel has--border is--rounded">

					{block name="frontend_account_password_reset_headline"}
						<h2 class="password-reset--title panel--title is--underline">{s name="PasswordHeader"}{/s}</h2>
					{/block}

					{block name='frontend_account_password_form'}
						{* Recover password form *}
					<div class="forma-white-background col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<form name="frmRegister" method="post" action="{url action=password}" class="password-reset--form col-xs-12 col-sm-12 col-md-12 col-lg-12">

							{block name="frontend_account_password_reset_content"}
								<div class="password-reset--form-content panel--body is--wide is--align-center nopadding">
									<p>{s name="PasswordText"}{/s}</p>
									<p>
										<input name="email" type="email" required="required" aria-required="true" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 password-reset--input" placeholder="{s name='PasswordPlaceholderMail'}{/s}" />
									</p>
									
								</div>
							{/block}

							{* Recover password actions *}
							{block name="frontend_account_password_reset_actions"}
								<div class="password-reset--form-actions panel--actions is--wide is--align-center col-xs-12 col-sm-12 col-md-12 col-lg-12 nopadding">
                                    {*<a href="{url controller='account'}"
                                       class="password-reset--link btn is--secondary is--icon-left is--center">
                                        <i class="icon--arrow-left"></i>{s name="PasswordLinkBack"}{/s}
                                    </a>*}
                                    <button type="submit"
                                            class="col-xs-12 col-sm-12 col-md-12 col-lg-12 password-reset--link btn is--primary is--icon-right is--center">
                                        {s name="PasswordSendAction"}{/s} {*<i class="icon--arrow-right"></i>*}
                                    </button>
                                </div>
							{/block}
						</form>
					</div>
					{/block}
				</div>
			{/block}
		{/if}
	</div>
{/block}