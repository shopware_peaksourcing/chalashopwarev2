{* Top bar main *}
{extends file="parent:frontend/index/topbar-navigation.tpl"}
{block name="frontend_index_top_bar_main"}
    <!-- start: Mobile Menu Toggle -->
    <button type="button" class="navbar-toggle">
        <span class="sr-only">{s name='ToggleNavigation'}Toggle navigation{/s}</span>
        <i class="fa fa-bars" aria-hidden="true"></i>
    </button>
    <!-- end: Mobile Menu Toggle -->
{/block}
