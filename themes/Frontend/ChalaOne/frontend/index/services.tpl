<!-- start: Services -->
<section class="section" id="service_overview">
    <div class="container">
        <div class="row">
            <!-- start: Left -->
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 left">
                <h2>Fragen und Antworten</h2>
                <!-- start: Accordion -->
                <div class="accordion">
                    <div class="item">
                        <h3>Was ist der Unterschied im tragen/laufen zwischen der dünnen und der dicken Sohle bei den Chala Classics?</h3>
                        <p>Bei der dünnen spürst Du den Untergrund besser, bei schwacher Muskulatur könnte dadurch allerdings der Fuß schneller ermüden. Auf der anderen Seite stärkst Du damit aber auch die Fußmuskulatur stärker. Die dicke schützt Deinen Fuß besser, Du spürst den Untergrund weniger und die Muskulatur wird weniger beansprucht.</p>
                    </div>
                    <div class="item">
                        <h3>Wieviel km kann ich mit einer Chala Classic Sohle laufen?</h3>
                        <p>Das kommt darauf an... wie der Untergrund ist, wie schwer Du bist, wie Deine Füße abrollen, etc. Bei tagtäglichem Gebrauch haben wir von unseren Kunden eine Nutzungsdauer von 2-5 Jahren zurückgemeldet bekommen.</p>
                    </div>
                    <div class="item">
                        <h3>Was ist der Unterschied im tragen/laufen zwischen der dünnen und der dicken Sohle bei den Chala Classics?</h3>
                        <p>Bei der dünnen spürst Du den Untergrund besser, bei schwacher Muskulatur könnte dadurch allerdings der Fuß schneller ermüden. Auf der anderen Seite stärkst Du damit aber auch die Fußmuskulatur stärker. Die dicke schützt Deinen Fuß besser, Du spürst den Untergrund weniger und die Muskulatur wird weniger beansprucht.</p>
                    </div>
                    <div class="item">
                        <h3>Wieviel km kann ich mit einer Chala Classic Sohle laufen?</h3>
                        <p>Das kommt darauf an... wie der Untergrund ist, wie schwer Du bist, wie Deine Füße abrollen, etc. Bei tagtäglichem Gebrauch haben wir von unseren Kunden eine Nutzungsdauer von 2-5 Jahren zurückgemeldet bekommen.</p>
                    </div>
                </div>
                <!-- end: Accordion -->
                <a href="#" class="button">Alle Fragen und Antworten zeigen</a>
                <h2>Anleitungen</h2>
                <!-- start: List Item -->
                <div class="list_item">
                    <img src="themes/Frontend/ChalaOne/frontend/_public/src/img/service_overview/left_1.png">
                    <h3>Schnüranleitung Chala Basis</h3>
                    <p>Alle Schritte zur perfekten Basis-Schnürung.</p>
                    <p><a href="#">Ansehen</a> oder Download als <a href="#">PDF</a></p>
                    <p></p>
                </div>
                <!-- end: List Item -->
                <!-- start: List Item -->
                <div class="list_item">
                    <img src="themes/Frontend/ChalaOne/frontend/_public/src/img/service_overview/left_2.png">
                    <h3>Schnüranleitung Slip-On-Slip-Off-Schnürung</h3>
                    <p>Alle Schritte zur stylischen Slip-On-Slip-Off-Schnürung.</p>
                    <p><a href="#">Ansehen</a> oder Download als <a href="#">PDF</a></p>
                </div>
                <!-- end: List Item -->
                <!-- start: List Item -->
                <div class="list_item">
                    <img src="themes/Frontend/ChalaOne/frontend/_public/src/img/service_overview/left_1.png">
                    <h3>Schnüranleitung Chala Basis</h3>
                    <p>Alle Schritte zur perfekten Basis-Schnürung.</p>
                    <p><a href="#">Ansehen</a> oder Download als <a href="#">PDF</a></p>
                    <p></p>
                </div>
                <!-- end: List Item -->
                <!-- start: List Item -->
                <div class="list_item">
                    <img src="themes/Frontend/ChalaOne/frontend/_public/src/img/service_overview/left_2.png">
                    <h3>Schnüranleitung Slip-On-Slip-Off-Schnürung</h3>
                    <p>Alle Schritte zur stylischen Slip-On-Slip-Off-Schnürung.</p>
                    <p><a href="#">Ansehen</a> oder Download als <a href="#">PDF</a></p>
                </div>
                <!-- end: List Item -->
                <a href="#" class="button">Alle Anleitungen zeigen</a>
            </div>
            <!-- end: Left -->
            <!-- start: Right -->
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 right">
                <h2>Kurse</h2>
                <!-- start: List Item -->
                <div class="list_item">
                    <img src="themes/Frontend/ChalaOne/frontend/_public/src/img/service_overview/right.png">
                    <h3>Chala Lauftechnik Kurs</h3>
                    <p class="date">12. Mai 2016 in Freiburg</p>
                    <p>4 Stunden Lauftechnik Kurs wo wir Dich in die Geheimnisse von und zu einweihen.</p>
                    <a href="#">Mehr</a>
                </div>
                <!-- end: List Item -->
                <!-- start: List Item -->
                <div class="list_item">
                    <img src="themes/Frontend/ChalaOne/frontend/_public/src/img/service_overview/right.png">
                    <h3>Chala Lauftechnik Kurs</h3>
                    <p class="date">12. Mai 2016 in Freiburg</p>
                    <p>4 Stunden Lauftechnik Kurs wo wir Dich in die Geheimnisse von und zu einweihen.</p>
                    <a href="#">Mehr</a>
                </div>
                <!-- end: List Item -->
                <!-- start: List Item -->
                <div class="list_item">
                    <img src="themes/Frontend/ChalaOne/frontend/_public/src/img/service_overview/right.png">
                    <h3>Chala Lauftechnik Kurs</h3>
                    <p class="date">12. Mai 2016 in Freiburg</p>
                    <p>4 Stunden Lauftechnik Kurs wo wir Dich in die Geheimnisse von und zu einweihen.</p>
                    <a href="#">Mehr</a>
                </div>
                <!-- end: List Item -->
                <a href="#" class="button">Alle Kurse zeigen</a>
                <h2>Versand und Rücksendung</h2>
                <div class="text_item">
                    <h3>Versanddestinationen und -kosten</h3>
                    <p>Nach Deutschland versenden wir für 4,90 €.</p>
                    <p>In alle anderen EU-Länder, inkl. Schweiz versenden wir für 15,90 €</p>
                    <p>Ausserhalb der EU versenden wir unter Vorbehalt und auf Anfrage.
                        Die Verfügbarkeit der Produkte wird bei diesen angezeigt. Ist ein Produkt verfügbar wird es innerhalb 1 Tages versand und ist dann wird dann in der Regel innerhalb von 1-3 Tagen in Deutschland und 4-6 Tagen in der EU geliefert.</p>
                </div>
                <div class="text_item">
                    <h3>Rücksendung</h3>
                    <p>Alle Produkte, die unbenutzt sind und nicht individuelle auf Maß angefertigt wurden können innerhalb der gesetzlichen Frist zurückgesandt werden. Bitte sende das Produkt in der Original Verpackung und vor Transportschäden geschützt an uns zurück. Versandkosten trägst Du und die Adresse findest Du <a href="#">hier</a>.</p>
                </div>
            </div>
            <!-- end: Right -->
        </div>
    </div>
</section>
<!-- end: Services -->