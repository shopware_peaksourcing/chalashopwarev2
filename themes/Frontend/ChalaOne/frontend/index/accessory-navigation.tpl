<div class="row filters">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        {block name="frontend_accessory_navigation"}
			{foreach from=$subCatogoriesMenu item=subCategory name=item}
                <a href="{$subCategory.link}" {if $subCategory.flag}class="active"{/if}> {if $smarty.foreach.item.first}{s name="IndexParentNavigationName"}{/s}{else}<div class="hidden-xs">{$subCategory.name}</div><div class="visible-xs">{$subCategory.attribute.mobile_description}</div>{/if}</a>
            {/foreach}
        {/block}
    </div>
</div>

