<ul class="drop">
    {if !$sUserLoggedIn}
        <li class="login_panel active">
            <p class="close_label visible-xs">{s name="AccountHeaderNavigation"}{/s} <a href="#" class="close"></a></p>
            <p class="hidden-xs">{s name="AccountHeaderNavigation"}{/s}</p>
            <form name="sLogin" method="post" action="/account/login/sTarget/account/sTargetAction/index">
                {if $sTarget}<input name="sTarget" type="hidden" value="{$sTarget|escape}" />{/if}

                <div class="item register--login-email">
                   
                    <input name="email" placeholder="{s name='LoginLable' namespace='frontend/index/login-box'}Login{/s}" type="email" tabindex="1" value="{$sFormData.email|escape}"
                           id="email" class="register--login-field{if $sErrorFlag.email} has--error{/if}"/>
                </div>

                <div class="item register--login-password">
                   
                    <input name="password" placeholder="{s name='PasswordLable' namespace='frontend/index/login-box'}Password{/s}" type="password" tabindex="2" id="passwort"
                           class="register--login-field{if $sErrorFlag.password} has--error{/if}"/>
                </div>

                <a href="{url controller=account action=password}">{s name='LoginLinkLostPassword' namespace='frontend/account/ajax_login'}Passwort vergessen?{/s}</a>
                <input value="Einloggen" type="submit">
            </form>
            <div class="separator-white-border">
                <p class="separator">{s name='NochLable' namespace='frontend/index/login-box'}Noch kein Chala Kunde?{/s}</p>
                <a href="{url controller='account' action='register'}"
                   class="toggle register">{s name="RegisterNow"}Neue Anforderung{/s}</a>
            </div>
        </li>
    {/if}
    {if $sUserLoggedIn}
        <li class="user_panel active">
            <ul>
                {include file="frontend/index/login-box-layout-2.tpl"}
            </ul>
        </li>
    {/if}
</ul>
