{block name='frontend_index_navigation_categories'}

    {block name='frontend_index_navigation_categories_navigation_list'}
        <ul class="navigation--list" role="menubar" itemscope="itemscope"
            itemtype="http://schema.org/SiteNavigationElement">
            {strip}
                {block name='frontend_index_navigation_categories_top_home'}
                {/block}

                {block name='frontend_index_navigation_categories_top_before'}{/block}

                {foreach from=$sAdvancedMenu key=i item=sCategory}

                    {block name='frontend_index_navigation_categories_top_entry'}
                        {if !$sCategory.hideTop}
                            <li class="navigation--entry{if $sCategory.flag} is--active{/if} {if $sCategory.attribute.menu_style}{$sCategory.attribute.menu_style}{/if}"
                                role="menuitem">
                                {block name='frontend_index_navigation_categories_top_link'}
                                    <a class="navigation--link{if $sCategory.flag} is--active{/if}"
                                       href="{if $sCategory.external}{$sCategory.external}{else}{$sCategory.link}{/if}" title="{$sCategory.description}" itemprop="url">
                                        {$sCategory.description}
                                    </a>
                                    <ul class="drop">
                                        {if $sCategory.sub}
                                            {foreach $sCategory.sub as $subCategory}
                                                {if !$subCategory.hideTop}
                                                    <li class="{if $subCategory.attribute.menu_product_style}{$subCategory.attribute.menu_product_style}{/if}{if $subCategory.flag} is--active{/if}">
                                                        <a href="{if $subCategory.external}{$subCategory.external}
																 {elseif $subCategory.attribute.product_configurator}{$configuratorLink}
															     {else}{$subCategory.link}{/if}">
                                                            <span>{if $subCategory.attribute.menu_label}{$subCategory.attribute.menu_label}{else}{$subCategory.description}{/if}</span>
                                                            {if $subCategory.media.thumbnails[3]}
                                                                <img src="{$subCategory.media.thumbnails[3].source}">
                                                            {elseif $subCategory.media.thumbnails[0].source}
                                                                <img src="{$subCategory.media.thumbnails[0].source}">
                                                            {/if}
                                                        </a>
                                                    </li>
                                                {/if}
                                            {/foreach}
                                        {else}
                                            <a class="navigation--link{if $sCategory.flag} is--active{/if}"
                                               href="{if $sCategory.external}{$sCategory.external}{else}{$sCategory.link}{/if}" title="{$sCategory.description}"
                                               itemprop="url">
                                                <span>{if $sCategory.attribute.menu_label}{$sCategory.attribute.menu_label}{else}{$sCategory.description}{/if}</span>
                                                {if $sCategory.media.thumbnails[3]}
                                                    <img src="{$sCategory.media.thumbnails[3].source}">
                                                {else}
                                                    <img src="{$sCategory.media.thumbnails[0].source}">
                                                {/if}
                                            </a>
                                            <a class="button{if $sCategory.flag} is--active{/if}"
                                               href="{if $sCategory.external}{$sCategory.external}{else}{$sCategory.link}{/if}"
                                               title="{$sCategory.description}"
                                               itemprop="url">
                                                {if $sCategory.attribute.menu_bottom_label}{$sCategory.attribute.menu_bottom_label}{else}{$sCategory.description}{/if}
                                            </a>
                                        {/if}
                                    </ul>
                                    {if $sCategory.attribute.contact_image}
                                        <div class="contact-image">
                                            <img src="/{$sCategory.attribute.contact_image|picture}">
                                        </div>
                                    {/if}
                                {/block}
                            </li>
                        {/if}
                    {/block}
                {/foreach}

                {block name='frontend_index_navigation_categories_top_after'}{/block}
            {/strip}
        </ul>
    {/block}
{/block}
