{block name='frontend_index_navigation_categories'}

    {block name='frontend_index_navigation_categories_navigation_list'}
		<ul role="menubar" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
            {strip}
					<li class="navigation--entry" role="menuitem">
						
						{block name='frontend_index_navigation_categories_top_link'}
							<a class="navigation--link" href="/" title="{s name='HomePage'}Startseite{/s}" itemprop="url">
								{s name='HomePage'}Startseite{/s}
							</a>
						{/block}
					</li>
					{foreach $sAdvancedMenu as $sCategory}
                        {block name='frontend_index_navigation_categories_top_entry'}
                            {if !$sCategory.hideTop}
								{if $sCategory.attribute.hide_in_mobile_menu != 1}
									<li {if $sCategory.media.thumbnails[5].source && $sCategory.attribute.hide_image_for_mobile_menu !=1}style="background: url('{$sCategory.media.thumbnails[5].source}') no-repeat 170px center;"{/if}
										class="navigation--entry{if $sCategory.flag} is--active{/if} {if $sCategory.attribute.menu_style}{$sCategory.attribute.menu_style}{/if}" role="menuitem">
										{block name='frontend_index_navigation_categories_top_link'}
											<a class="navigation--link{if $sCategory.flag} is--active{/if}" href="
											{if $sCategory.external}{$sCategory.external}
											{else}{$sCategory.link}{/if}" title="{$sCategory.description}" itemprop="url">
												{$sCategory.description}
											</a>
										{/block}
									</li>
								{/if}
								{if $sCategory.sub}
									<li class="dash"></li>
									{foreach $sCategory.sub as $subCategory}
										{if !$subCategory.hideTop && !isset($subCategory.attribute.hide_in_mobile_menu)}
											<li {if $subCategory.media.thumbnails[5].source && $sCategory.attribute.hide_image_for_mobile_menu !=1}style="background: url('{$subCategory.media.thumbnails[5].source}') no-repeat 170px center;"{/if}
												class="{if $subCategory.attribute.menu_product_style}{$subCategory.attribute.menu_product_style}{/if}{if $subCategory.flag} is--active{/if}">
												<a href="{if $subCategory.external}{$subCategory.external}
														{elseif $subCategory.attribute.product_configurator}{$configuratorLink}
														{else}{$subCategory.link}{/if}">
													{if $subCategory.attribute.menu_bottom_label}{$subCategory.attribute.menu_bottom_label}{else}{$subCategory.description}{/if}											
												</a>
											</li>
										{/if}
									{/foreach}
								{/if}
                            {/if}
                        {/block}
                    {/foreach}	
            {/strip}
        </ul>

    {/block}
{/block}