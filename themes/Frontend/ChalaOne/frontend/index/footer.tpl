{* Footer menu *}
{block name='frontend_index_footer_menu'}
    {include file='frontend/index/footer-navigation.tpl'}
{/block}

{* Copyright in the footer *}
{block name='frontend_index_footer_copyright'}

<!--         {* Vat info *}
        {block name='frontend_index_footer_vatinfo'}
            <div class="footer--vat-info">
                <p class="vat-info--text">
                    {if $sOutputNet}
                        {s name='FooterInfoExcludeVat' namespace="frontend/index/footer"}{/s}
                    {else}
                        {s name='FooterInfoIncludeVat' namespace="frontend/index/footer"}{/s}
                    {/if}
                </p>
            </div>
        {/block}

        {block name='frontend_index_footer_minimal'}
            {include file="frontend/index/footer_minimal.tpl" hideCopyrightNotice=true}
        {/block} -->

    {* Shopware footer *}
    {block name="frontend_index_shopware_footer"}


        {* Logo *}
        {block name="frontend_index_shopware_footer_logo"}
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 payment">
                        <img src="{link file='frontend/_public/src/img/logos/paypal.png'}">
                        <img src="{link file='frontend/_public/src/img/logos/maestro.png'}">
                        <img src="{link file='frontend/_public/src/img/logos/mastercard.png'}">
                        <img src="{link file='frontend/_public/src/img/logos/visa.png'}">
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 logo">
                    <a href="#">
                        <img src="{link file='frontend/_public/src/img/logos/logo.png'}">
                    </a>
                </div>
            </div>
        {/block}
    {/block}
{/block}
