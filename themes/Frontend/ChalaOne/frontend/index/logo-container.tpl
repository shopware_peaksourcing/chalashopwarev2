{* Main shop logo *}
{block name='frontend_index_logo'}
    <a id="header_logo" class="logo--link" href="{url controller='index'}" title="{"{config name=shopName}"|escapeHtml} - {"{s name='IndexLinkDefault' namespace="frontend/index/index"}{/s}"|escape}">
        <picture>
            <img srcset="{link file=$theme.desktopLogo}" class="hidden-xs" alt="{"{config name=shopName}"|escapeHtml} - {"{s name='IndexLinkDefault' namespace="frontend/index/index"}{/s}"|escape}" />
            <img srcset="{link file=$theme.mobileLogo}"  class="visible-xs" alt="{"{config name=shopName}"|escapeHtml} - {"{s name='IndexLinkDefault' namespace="frontend/index/index"}{/s}"|escape}" />
        </picture>
    </a>
{/block}

{* Support Info *}
{block name='frontend_index_logo_supportinfo'}
    {if $theme.checkoutHeader && {controllerName|lower} === 'checkout' && {controllerAction|lower} !== 'cart'}
        <div class="logo--supportinfo block">
            {s name='RegisterSupportInfo' namespace='frontend/register/index'}{/s}
        </div>
    {/if}
{/block}

{* Trusted Shops *}
{block name='frontend_index_logo_trusted_shops'}{/block}
