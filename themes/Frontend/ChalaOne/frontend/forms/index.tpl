{extends file='frontend/index/index.tpl'}

{* Breadcrumb *}
{block name='frontend_index_start' prepend}
{/block}

{* Sidebar left *}
{block name='frontend_index_content_left'}
{/block}

{block name="frontend_index_header"}
    {include file="frontend/forms/header.tpl"}
{/block}

{* Main content *}
{block name='frontend_index_content'}

    <!-- start: Contact -->
    <section class="section" id="contact">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h1>{$sSupport.name}</h1>
                </div>
            </div>
            <div class="row bottom">
                <!-- start: Left -->
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 left">
                    <div class="wrapper">
                        <h2>Wir freuen uns auf Deine Nachricht!</h2>
                        {* Form error *}
                        {block name='frontend_forms_elements_error'}
                            {if $sSupport.sErrors.e || $sSupport.sErrors.v}
                                {$errorContent=""}
                                <div class="error">
                                    {if $sSupport.sErrors.v}
                                        {foreach from=$sSupport.sErrors.v key=sKey item=sError}
                                            {if $sKey !=0&&$sSupport.sElements.$sError.error_msg}{$errorContent="{$errorContent}<br />"}{/if}
                                            {$errorContent="{$errorContent}{$sSupport.sElements.$sError.error_msg}"}
                                        {/foreach}
                                        {if $sSupport.sErrors.e}
                                            {$errorContent="{$errorContent}<br />"}
                                        {/if}
                                    {/if}

                                    {if $sSupport.sErrors.e}
                                        {$errorContent="{$errorContent}{s name='SupportInfoFillRedFields' namespace="frontend/forms/elements"}{/s}"}
                                    {/if}

                                    {block name='frontend_forms_elements_error_messages'}
                                        {include file="frontend/_includes/messages.tpl" type='error' content=$errorContent}
                                    {/block}
                                </div>
                            {/if}
                        {/block}

                        {* Forms headline *}
                        {block name='frontend_forms_index_headline'}
                        {/block}

                        {* Forms Content *}
                        {block name='frontend_forms_index_content'}
                            {if $sSupport.sElements}
                                {block name='frontend_forms_index_elements'}
                                    {include file="frontend/forms/elements.tpl"}
                                {/block}
                            {/if}
                        {/block}
                    </div>
                </div>
                <!-- end: Left -->
                <!-- start: Right -->
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 right">
                    <div class="wrapper">
                        {$sSupport.text|unescape:'html'}
                    </div>
                </div>
                <!-- end: Right -->
            </div>
        </div>
    </section>
    <!-- end: Contact -->
{/block}

{* Hide sidebar right *}
{block name='frontend_index_content_right'}{/block}

{* Include jQuery and all other javascript files at the bottom of the page *}
{block name="frontend_index_header_javascript_jquery_lib" append}
    <script type="text/javascript">
        //<![CDATA[
        $(document).ready(function () {
            $("#support").validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    vorname: "required",
                    nachname: "required",
                    kommentar: "required",
                    sCaptcha: "required"

                },
                messages: {
                    email: "{s name='SupportInfoFillRedFields' namespace='frontend/forms/elements'}Bitte gültige E-Mail Adresse eingeben{/s}",
                    vorname: "{s name='SupportInfoVornamRedFields' namespace='frontend/forms/elements'}Bitte gültige Vorname eingeben{/s}",
                    nachname: "{s name='SupportInfoNameRedFields' namespace='frontend/forms/elements'}Bitte gültige Nachname eingeben{/s}",
                    kommentar: "{s name='SupportInfoCommentRedFields' namespace='frontend/forms/elements'}Bitte gültige Kommentar eingeben{/s}",
                    sCaptcha: "{s name='SupportInfoCharactersRedFields' namespace='frontend/forms/elements'}Bitte gültige Zeichen ein eingeben{/s}"
                }
            });
        });
        //]]>
    </script>
{/block}
