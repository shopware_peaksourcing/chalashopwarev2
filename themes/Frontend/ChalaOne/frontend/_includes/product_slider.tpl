{* Config *}
{block name="frontend_common_product_slider_config"}
    {$productSliderCls = ($productSliderCls)?$productSliderCls:""}
    {$productBoxLayout = ($productBoxLayout)?$productBoxLayout:"slider"}
    {$sliderMode = ($sliderMode)?$sliderMode:""}
    {$sliderOrientation = ($sliderOrientation)?$sliderOrientation:""}
    {$sliderItemMinWidth = ($sliderItemMinWidth)?$sliderItemMinWidth:""}
    {$sliderItemMinHeight = ($sliderItemMinHeight)?$sliderItemMinHeight:""}
    {$sliderItemsPerSlide = ($sliderItemsPerSlide)?$sliderItemsPerSlide:""}
    {$sliderItemsPerPage = ($sliderItemsPerPage)?$sliderItemsPerPage:""}
    {$sliderAutoSlide = ($sliderAutoSlide)?$sliderAutoSlide:""}
    {$sliderAutoSlideDirection = ($sliderAutoSlideDirection)?$sliderAutoSlideDirection:""}
    {$sliderAutoSlideSpeed = ($sliderAutoSlideSpeed)?$sliderAutoSlideSpeed:""}
    {$sliderAutoScroll = ($sliderAutoScroll)?$sliderAutoScroll:""}
    {$sliderAutoScrollDirection = ($sliderAutoScrollDirection)?$sliderAutoScrollDirection:""}
    {$sliderAutoScrollSpeed = ($sliderAutoScrollSpeed)?$sliderAutoScrollSpeed:""}
    {$sliderScrollDistance = ($sliderScrollDistance)?$sliderScrollDistance:""}
    {$sliderAnimationSpeed = ($sliderAnimationSpeed)?$sliderAnimationSpeed:""}
    {$sliderArrowControls = ($sliderArrowControls)?$sliderArrowControls:""}
    {$sliderArrowAction = ($sliderArrowAction)?$sliderArrowAction:""}
    {$sliderWrapperCls = ($sliderWrapperCls)?$sliderWrapperCls:""}
    {$sliderHorizontalCls = ($sliderHorizontalCls)?$sliderHorizontalCls:""}
    {$sliderVerticalCls = ($sliderVerticalCls)?$sliderVerticalCls:""}
    {$sliderArrowCls = ($sliderArrowCls)?$sliderArrowCls:""}
    {$sliderPrevArrowCls = ($sliderPrevArrowCls)?$sliderPrevArrowCls:""}
    {$sliderNextArrowCls = ($sliderNextArrowCls)?$sliderNextArrowCls:""}
    {$sliderAjaxCtrlUrl = ($sliderAjaxCtrlUrl)?$sliderAjaxCtrlUrl:""}
    {$sliderAjaxCategoryID = ($sliderAjaxCategoryID)?$sliderAjaxCategoryID:""}
    {$sliderAjaxMaxShow = ($sliderAjaxMaxShow)?$sliderAjaxMaxShow:""}
    {$sliderAjaxShowLoadingIndicator = ($sliderAjaxShowLoadingIndicator)?$sliderAjaxShowLoadingIndicator:""}
    {$sliderInitOnEvent = ($sliderInitOnEvent)?$sliderInitOnEvent:""}
    {$fixedImageSize = ($fixedImageSize)?$fixedImageSize:""}
{/block}

{* Template *}
{block name="frontend_common_product_slider_component"}
	{block name="frontend_common_product_slider_container"}
        {if $sSimilar}
            {include file="frontend/_includes/product_slider_similar_items.tpl" articles=$articles sConf=$sProductConfigurator}
        {else}
            {include file="frontend/_includes/product_slider_items.tpl" articles=$articles sConfigurator=$sConfigurator}
        {/if}
	{/block}
{/block}