{block name="frontend_common_product_slider_item_config"}
    {$productBoxLayout = ($productBoxLayout) ? $productBoxLayout : ""}
    {$fixedImageSize = ($fixedImageSize) ? $fixedImageSize : ""}
{/block}

{block name="frontend_common_product_slider_item"}
    {include file="frontend/listing/box_article.tpl" sArticle=$article productBoxLayout=$productBoxLayout fixedImageSize=$fixedImageSize}
{/block}