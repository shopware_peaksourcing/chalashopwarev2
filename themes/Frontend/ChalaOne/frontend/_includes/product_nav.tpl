{* Config *}
{block name="frontend_common_product_slider_config"}
{/block}

{* Template *}
{block name="frontend_common_product_slider_component"}
	{block name="frontend_common_product_slider_container"}
        {block name="frontend_common_product_slider_items"}
            {foreach $articles as $article}
                <li class="{$article.item_style}">
                    {block name="frontend_common_product_slider_item"}
                        {block name="frontend_listing_box_article_includes_additional"}
                            {include file="frontend/listing/product-box/box-basic.tpl"}
                        {/block}
                    {/block}
                </li>
            {/foreach}
        {/block}
	{/block}
{/block}