{block name="frontend_common_product_slider_items"}
    {foreach $articles as $article}
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 item {$article.item_style}">
			{include file="frontend/_includes/product_slider_item.tpl"}
		</div>
    {/foreach}
{/block}