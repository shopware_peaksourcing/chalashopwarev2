{* Thumbnails *}
{if $sArticle.images}

    {* Thumbnail - Container *}
    <div class="thumbs">
        {* Thumbnail - Slide Container *}
		{block name='frontend_detail_image_thumbnail_items'}



			{* Thumbnails *}
			
			{foreach from=$sArticle.images item=image name=img}
				{if $image.thumbnails}
					{block name='frontend_detail_image_thumbnail_images'}

						{$alt = $sArticle.articleName|escape}

						{if $image.description}
							{$alt = $image.description|escape}
						{/if}
						<a href="{$image.src.1}"
						   title='{s name="DetailThumbnailText" namespace="frontend/detail/index"}{/s}: {$alt}'
						   class="thumbnail--link "
						   number="{$smarty.foreach.img.iteration}">
							{block name='frontend_detail_image_thumbs_images_img'}
								<img srcset="{$image.thumbnails[0].sourceSet}"
									 alt='{s name="DetailThumbnailText" namespace="frontend/detail/index"}{/s}: {$alt}'
									 title='{s name="DetailThumbnailText" namespace="frontend/detail/index"}{/s}: {$alt|truncate:160}' 
									 class="thumbnail--image"/>
							{/block}
						</a>
					{/block}
				{/if}
			{/foreach}
		{/block}
	</div>
{/if}