{block name='frontend_detail_configurator_variant'}
    <div class="configurator--variant">


        {block name='frontend_detail_configurator_variant_form'}
            <form method="post" action="{url sArticle=$sArticle.articleID sCategory=$sArticle.categoryID}"
                  class="configurator--form">
                {foreach $configurator as $configuratorGroup}
                    {block name='frontend_detail_configurator_variant_group'}
                        <div class="variant--group">
                            {block name='frontend_detail_configurator_variant_group_name'}
                            {/block}

                            {block name='frontend_detail_configurator_variant_group_options'}

                                {if $configuratorGroup.groupname|lower == 'color' || $configuratorGroup.groupID == 1}
                                    <div class="material">
                                        {foreach $configuratorGroup.values as $option}
                                            {block name='frontend_detail_configurator_variant_group_option'}
                                                <div class="color variant--option{if $option.media} is--image{/if} item {if $option.user_selected && $option.selectable}active{/if}"
                                                     id="{$option.optionname|lower}"
                                                     data-id="{$option.optionID}">

                                                    {block name='frontend_detail_configurator_variant_group_option_input'}
                                                        <span class="image"
                                                              style="background-image: url({$option.media.thumbnails[0].source});"></span>
                                                        <span class="link_label">{$option.optionname}</span>
                                                        <input type="radio"
                                                               class="option--input"
                                                               id="group[{$option.groupID}][{$option.optionID}]"
                                                               name="group[{$option.groupID}]"
                                                               value="{$option.optionID}"
                                                               title="{$option.optionname}"
                                                               data-ajax-select-variants="true"
                                                               {if !$option.selectable}disabled="disabled"{/if}
                                                                {if $option.selected && $option.selectable}checked="checked"{/if} />
                                                    {/block}

                                                    {block name='frontend_detail_configurator_variant_group_option_label'}
                                                        <label for="group[{$option.groupID}][{$option.optionID}]"
                                                               class="option--label{if !$option.selectable} is--disabled{/if}">

                                                            {if $option.media}
                                                                {$media = $option.media}

                                                                {block name='frontend_detail_configurator_variant_group_option_label_image'}
                                                                    <span class="image--element">
                                                                    <span class="image--media">
                                                                        {if isset($media.thumbnails)}
                                                                            <img srcset="{$media.thumbnails[0].sourceSet}"
                                                                                 alt="{$option.optionname}"/>
                                                                        {else}
                                                                            <img src="{link file='frontend/_public/src/img/no-picture.jpg'}"
                                                                                 alt="{$option.optionname}">
                                                                        {/if}
                                                                    </span>
                                                                </span>
                                                                {/block}
                                                            {else}
                                                                {block name='frontend_detail_configurator_variant_group_option_label_text'}
                                                                    {$option.optionname}
                                                                {/block}
                                                            {/if}
                                                        </label>
                                                    {/block}
                                                </div>
                                            {/block}
                                        {/foreach}
                                    </div>
                                {else}
                                    {block name='frontend_detail_configurator_variant_group_option'}
                                        <div class="sizes variant--option{if $option.media} is--image{/if}">
                                            {block name='frontend_detail_configurator_variant_group_option_input'}
                                                <select name="group[{$configuratorGroup.groupID}]"
                                                        data-ajax-select-variants="true">
                                                    {foreach $configuratorGroup.values as $option}
                                                        {if !{config name=hideNoInStock} || ({config name=hideNoInStock} && $option.selectable)}
                                                            <option{if $option.selected} selected="selected"{/if}
                                                                    value="{$option.optionID}">
                                                                {$option.optionname}
                                                            </option>
                                                        {/if}
                                                    {/foreach}
                                                </select>
                                            {/block}

                                            {block name='frontend_detail_configurator_variant_group_option_label'}

                                            {/block}
                                        </div>
                                    {/block}

                                {/if}
                            {/block}
                        </div>
                    {/block}
                {/foreach}
            </form>
        {/block}
    </div>
{/block}

{block name='frontend_detail_configurator_variant_reset'}
{/block}
