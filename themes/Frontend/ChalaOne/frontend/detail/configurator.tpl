{extends file='frontend/detail/index.tpl'}

{* Custom header *}
{block name='frontend_index_header'}
    {include file="frontend/detail/header.tpl"}
{/block}

{* Modify the breadcrumb *}
{block name='frontend_index_breadcrumb_inner'}
{/block}

{block name="frontend_index_content_top" append}
    {* Product navigation - Previous and next arrow button *}
    {block name="frontend_detail_index_navigation"}
    {/block}
{/block}

{* Main content *}

{block name="frontend_index_content_main"}

    {* Breadcrumb *}
    {block name='frontend_index_breadcrumb'}
    {/block}

    {$configurator = $sArticle.sConfigurator}
    <!-- start: Product Configurator -->
    <section id="configurator">
        <div class="container">
            <div class="content product--details" itemscope
                 itemtype="http://schema.org/Product"{if !{config name=disableArticleNavigation}} data-product-navigation="{url module="widgets" controller="listing" action="productNavigation"}" data-category-id="{$sArticle.categoryID}" data-main-ordernumber="{$sArticle.mainVariantNumber}"{/if}
                 data-ajax-wishlist="true" data-compare-ajax="true" data-ajax-variants-container="true">
                <div class="product--detail-upper block-group">
                    <div class="row top">
                        {if $sArticle.articleName}<h1>{$sArticle.articleName}</h1>{/if}
                        {if $sArticle.description_long}{$sArticle.description_long|unescape:'html'}{/if}
                    </div>
                    <!-- start: Configurator Form -->
                    <div class="row form">
                        {* The configurator selection is checked at this early point
                           to use it in different included files in the detail template. *}
                        {block name='frontend_detail_index_configurator_settings'}

                            {* Variable for tracking active user variant selection *}
                            {$activeConfiguratorSelection = true}

                            {if $sArticle.sConfigurator && ($sArticle.sConfiguratorSettings.type == 1 || $sArticle.sConfiguratorSettings.type == 2)}
                                {* If user has no selection in this group set it to false *}
                                {foreach $sArticle.sConfigurator as $configuratorGroup}
                                    {if !$configuratorGroup.selected_value}
                                        {$activeConfiguratorSelection = false}
                                    {/if}
                                {/foreach}
                            {/if}
                        {/block}


                        {* Configurator drop down menu's *}
                        {block name="frontend_detail_index_configurator"}
                            <div class="product--configurator">
                                {if $sArticle.sConfigurator}
                                    {if $sArticle.sConfiguratorSettings.type == 1}
                                        {include file="frontend/detail/config_step.tpl"}
                                    {elseif $sArticle.sConfiguratorSettings.type == 2}
                                        {include file="frontend/detail/config_variant_configurator.tpl"}
                                    {else}
                                        {include file="frontend/detail/config_upprice.tpl"}
                                    {/if}
                                {/if}
                            </div>
                        {/block}

                        {if isset($sArticle.tailor_made_title) && isset($sArticle.tailor_made_text_left) && isset($sArticle.tailor_made_text_right)}
                            <div class="tailor_made_info hidden">
                                <h3>{$sArticle.tailor_made_title}</h3>
                                <div class="text_left">
                                    {$sArticle.tailor_made_text_left|unescape:'html'}
                                </div>
                                <div class="text_right">
                                    {$sArticle.tailor_made_text_right|unescape:'html'}
                                </div>
                            </div>
                        {/if}

                        <div class="form_price_and_submit">
                            <h2 class="hidden-xs">{s name="BuyTitleDesktop"}oder doch eine unserer zeitlosen Klassiker?{/s}</h2>
                            <h2 class="visible-xs">{s name="BuyTitleMobile"}Deine Chala kostet:{/s}</h2>
                            {block name='frontend_detail_index_data'}
                                {if $sArticle.sBlockPrices}
                                    {$lowestPrice=false}
                                    {$highestPrice=false}
                                    {foreach $sArticle.sBlockPrices as $blockPrice}
                                        {if $lowestPrice === false || $blockPrice.price < $lowestPrice}
                                            <p class="price">{$lowestPrice=$blockPrice.price}</p>
                                        {/if}
                                        {if $highestPrice === false || $blockPrice.price > $highestPrice}
                                            <p class="price">{$highestPrice=$blockPrice.price}</p>
                                        {/if}
                                    {/foreach}
                                    <meta itemprop="lowPrice" content="{$lowestPrice}"/>
                                    <meta itemprop="highPrice" content="{$highestPrice}"/>
                                    <meta itemprop="offerCount" content="{$sArticle.sBlockPrices|count}"/>
                                {else}
                                    <meta itemprop="priceCurrency" content="{$Shop->getCurrency()->getCurrency()}"/>
                                {/if}
                                {include file="frontend/detail/data_configurator.tpl" sArticle=$sArticle sView=1}
                            {/block}
                            <div class="form_submit">
                                {* Include buy button and quantity box *}
                                {block name="frontend_detail_index_buybox"}
                                    {include file="frontend/detail/buy_configurator.tpl"}
                                {/block}
                            </div>
                            {* Product actions *}
                            {block name="frontend_detail_index_actions"}
                            {/block}
                            {* Product - Base information *}
                            {block name='frontend_detail_index_buy_container_base_info'}
                                <div class="product--buybox block{if $sArticle.sConfigurator && $sArticle.sConfiguratorSettings.type==2} is--wide{/if}">

                                    {block name="frontend_detail_rich_snippets_brand"}
                                        <meta itemprop="brand" content="{$sArticle.supplierName|escape}"/>
                                    {/block}

                                    {block name="frontend_detail_rich_snippets_weight"}
                                        {if $sArticle.weight}
                                            <meta itemprop="weight" content="{$sArticle.weight} kg"/>
                                        {/if}
                                    {/block}

                                    {block name="frontend_detail_rich_snippets_height"}
                                        {if $sArticle.height}
                                            <meta itemprop="height" content="{$sArticle.height} cm"/>
                                        {/if}
                                    {/block}

                                    {block name="frontend_detail_rich_snippets_width"}
                                        {if $sArticle.width}
                                            <meta itemprop="width" content="{$sArticle.width} cm"/>
                                        {/if}
                                    {/block}

                                    {block name="frontend_detail_rich_snippets_depth"}
                                        {if $sArticle.length}
                                            <meta itemprop="depth" content="{$sArticle.length} cm"/>
                                        {/if}
                                    {/block}

                                    {block name="frontend_detail_rich_snippets_release_date"}
                                        {if $sArticle.sReleasedate}
                                            <meta itemprop="releaseDate" content="{$sArticle.sReleasedate}"/>
                                        {/if}
                                    {/block}

                                    {block name='frontend_detail_buy_laststock'}
                                        {if !$sArticle.isAvailable && ($sArticle.isSelectionSpecified || !$sArticle.sConfigurator)}
                                            {include file="frontend/_includes/messages.tpl" type="error" content="{s name='DetailBuyInfoNotAvailable' namespace='frontend/detail/buy'}{/s}"}
                                        {/if}
                                    {/block}

                                    {* Product email notification *}
                                    {block name="frontend_detail_index_notification"}
                                        {if $sArticle.notification && $sArticle.instock <= 0 && $ShowNotification}
                                            {include file="frontend/plugins/notification/index.tpl"}
                                        {/if}
                                    {/block}

                                    {* Product data *}
                                    {block name='frontend_detail_index_buy_container_inner'}
                                        <div itemprop="offers" itemscope
                                             itemtype="{if $sArticle.sBlockPrices}http://schema.org/AggregateOffer{else}http://schema.org/Offer{/if}"
                                             class="buybox--inner">
                                            {block name='frontend_detail_index_after_data'}{/block}

                                            {* Product SKU *}
                                            {block name='frontend_detail_data_ordernumber'}
                                                <li class="base-info--entry entry--sku hidden">
                                                    {* Product SKU - Label *}
                                                    {block name='frontend_detail_data_ordernumber_label'}
                                                        <strong class="entry--label">
                                                            {s name="DetailDataId" namespace="frontend/detail/data"}{/s}
                                                        </strong>
                                                    {/block}

                                                    {* Product SKU - Content *}
                                                    {block name='frontend_detail_data_ordernumber_content'}
                                                        <meta itemprop="productID"
                                                              content="{$sArticle.articleDetailsID}"/>
                                                            <span class="entry--content" itemprop="sku">
                                                                {$sArticle.ordernumber}
                                                            </span>
                                                    {/block}
                                                </li>
                                            {/block}
                                        </div>
                                    {/block}
                                </div>
                            {/block}
                        </div>
                    </div>
                    {* Product bundle hook point *}
                    {block name="frontend_detail_index_bundle"}{/block}

                    {block name="frontend_detail_index_detail"}

                        {* Tab navigation *}
                        {block name="frontend_detail_index_tabs"}
                            <div class="tab--header hidden">
                                <a href="#" class="tab--title"
                                   title="{$relatedProductStream.name}">{$relatedProductStream.name}</a>
                            </div>
                            <div class="tab--content content--related-product-streams-{$key} hidden">
                                {include file='frontend/detail/tabs/product_streams.tpl'}
                            </div>
                        {/block}
                    {/block}

                    {* Crossselling tab panel *}
                    {block name="frontend_detail_index_tabs_cross_selling"}
                    {/block}
                </div>
            </div>
        </div>
    </section>
    <!-- start: More Models -->
    <section class="models_list section configurator">
        <div class="container">
            {if $sArticle.article_list_title}
                <div class="row">
                    <h2>{$sArticle.article_list_title}</h2>
                </div>
            {/if}
            <div class="row items-row">
                {* Similar products slider *}
                {if $sArticle.sSimilarArticles}
                    {block name="frontend_detail_index_tabs_similar"}
                        {block name="frontend_detail_index_tabs_similar_inner"}
                            {block name="frontend_common_product_slider_items"}
                                {foreach $sArticle.sSimilarArticles as $article}
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 v_list item {$article.item_style}">
                                        {block name="frontend_common_product_slider_item"}
                                            {include file="frontend/listing/box_article.tpl" sArticle=$article productBoxLayout='configurator'}
                                        {/block}
                                    </div>
                                {/foreach}
                            {/block}
                        {/block}
                    {/block}
                {/if}
                <!-- end: Description -->
            </div>
        </div>
    </section>
    <!-- end: Product Top Description -->
    <section class="section articles configurator">
        <div class="container">
            <div class="row">
                {foreach $relatedBlog as $blog}
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 v_list video {$blog.icon}">
                        <div class="wrapper">
                            <div class="text">
                                <h3>{$blog.title}</h3>
                            </div>
                            <div class="image">
                                <img src="{$blog.image.600x600}">
                            </div>
                        </div>
                    </div>
                {/foreach}
            </div>
        </div>
    </section>
{/block}