{if $sArticle.sSimilarArticles}
	{* Similar products - Content *}
	{block name="frontend_detail_index_similar_slider_content"}
		{$similar = true}  
		{include file="frontend/_includes/product_slider.tpl" articles=$sArticle.sSimilarArticles sSimilar=$similar sProductConfigurator=$sConfigurator}
	{/block}
{/if}