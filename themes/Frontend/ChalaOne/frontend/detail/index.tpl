{extends file='frontend/index/index.tpl'}

{* Custom header *}
{block name='frontend_index_header'}
    {include file="frontend/detail/header.tpl"}
{/block}

{* Modify the breadcrumb *}
{block name='frontend_index_breadcrumb_inner'}
{/block}

{block name="frontend_index_content_top" append}
    {* Product navigation - Previous and next arrow button *}
    {block name="frontend_detail_index_navigation"}
    {/block}
{/block}

{* Main content *}

{block name="frontend_index_content_main"}

    {* Breadcrumb *}
    {block name='frontend_index_breadcrumb'}
    {/block}
	
    {* The configurator selection is checked at this early point
       to use it in different included files in the detail template. *}
    {block name='frontend_detail_index_configurator_settings'}
    {/block}
	

        {$foo = $sArticle.top_slider_images|substr:1:-1}
        {$imagesArray = "|"|explode:$foo}
		
		<section class="section narrow">
			<div class="container">
				<div class="row">
					<a href="{$categoryParent.link}" class="arrow_back">
						{s namespace='frontend/index/index' name='BackToOverview'}Zurück zur Übersicht{/s}
					</a>
				</div>
			</div>
		</section>
		
		{if $imagesArray}
			<!-- start: Product Slider -->
			<section id="product_slider">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 slider">
							{foreach $imagesArray as $imagePath}
								{$slideImage = $imagePath|media}
								<!-- start: Slider Item -->
								<div class="item">
									<img src="{$slideImage.thumbnail.1140x380}">
								</div>
								<!-- end: Slider Item -->
							{/foreach}
						</div>
					</div>
				</div>
			</section>
			<!-- end: Product Slider -->
		{/if}

        <section id="product_top" class="{$sArticle.item_style}">
            <div class="container">
                <div class="row">
                    <div class="decoration"></div>
                    <div class="content product--details" itemscope itemtype="http://schema.org/Product"{if !{config name=disableArticleNavigation}} data-product-navigation="{url module="widgets" controller="listing" action="productNavigation"}" data-category-id="{$sArticle.categoryID}" data-main-ordernumber="{$sArticle.mainVariantNumber}"{/if} data-ajax-wishlist="true" data-compare-ajax="true"  data-ajax-variants-container="true">
					<div class="product--detail-upper">
                        {* The configurator selection is checked at this early point
                           to use it in different included files in the detail template. *}
                        {block name='frontend_detail_index_configurator_settings'}

                            {* Variable for tracking active user variant selection *}
                            {$activeConfiguratorSelection = true}

                            {if $sArticle.sConfigurator && ($sArticle.sConfiguratorSettings.type == 1 || $sArticle.sConfiguratorSettings.type == 2)}
                                {* If user has no selection in this group set it to false *}
                                {foreach $sArticle.sConfigurator as $configuratorGroup}
                                    {if !$configuratorGroup.selected_value}
                                        {$activeConfiguratorSelection = false}
                                    {/if}
                                {/foreach}
                            {/if}
                        {/block}

                        {* Product header *}
                        {block name='frontend_detail_index_header'}
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 gallery_wrapper">
                                <div class="product-information hidden-sm hidden-md hidden-lg">
                                    {if $sArticle.articleName}<h1>{$sArticle.articleName}</h1>{/if}
                                    {if $sArticle.sub_header}<p class="highlight">{$sArticle.sub_header}</p>{/if} 
                                </div>

                                {* Product image *}
                                {block name='frontend_detail_index_image_container'}
                                    <div class="gallery">
                                        {include file="frontend/detail/image.tpl"}
                                     </div>
                                {/block}
                            </div>

                            {* Product image *}
                            {block name='frontend_detail_index_image_container'}
                            {/block}
                            {block name='frontend_detail_index_header_inner'}
                            {/block}
                        {/block}
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 product_info">
                            <div class="product--detail-upper block-group">
                                {* Product name *}
                                {block name='frontend_detail_index_name'}
                                    <h1 class="product--title hidden-xs" itemprop="name">
                                        {$sArticle.articleName}
                                    </h1>
                                {/block}
                                {if $sArticle.sub_header}<p class="highlight hidden-xs">{$sArticle.sub_header}</p>{/if} 
                                {if $sArticle.description_long}
									<div class="hidden-xs">
										{$sArticle.description_long|unescape:'html'}{/if}
									</div>
                                {* Product image *}
                                {block name='frontend_detail_index_image_container'}
                                {/block}

                                {* "Buy now" box container *}
                                {block name='frontend_detail_index_buy_container'}
                                    <div class="product--buybox block{if $sArticle.sConfigurator && $sArticle.sConfiguratorSettings.type==2} is--wide{/if}">

                                        {block name="frontend_detail_rich_snippets_brand"}
                                            <meta itemprop="brand" content="{$sArticle.supplierName|escape}"/>
                                        {/block}

                                        {block name="frontend_detail_rich_snippets_weight"}
                                            {if $sArticle.weight}
                                                <meta itemprop="weight" content="{$sArticle.weight} kg"/>
                                            {/if}
                                        {/block}

                                        {block name="frontend_detail_rich_snippets_height"}
                                            {if $sArticle.height}
                                                <meta itemprop="height" content="{$sArticle.height} cm"/>
                                            {/if}
                                        {/block}

                                        {block name="frontend_detail_rich_snippets_width"}
                                            {if $sArticle.width}
                                                <meta itemprop="width" content="{$sArticle.width} cm"/>
                                            {/if}
                                        {/block}

                                        {block name="frontend_detail_rich_snippets_depth"}
                                            {if $sArticle.length}
                                                <meta itemprop="depth" content="{$sArticle.length} cm"/>
                                            {/if}
                                        {/block}

                                        {block name="frontend_detail_rich_snippets_release_date"}
                                            {if $sArticle.sReleasedate}
                                                <meta itemprop="releaseDate" content="{$sArticle.sReleasedate}"/>
                                            {/if}
                                        {/block}

                                        {block name='frontend_detail_buy_laststock'}
                                            {if !$sArticle.isAvailable && ($sArticle.isSelectionSpecified || !$sArticle.sConfigurator)}
                                                {include file="frontend/_includes/messages.tpl" type="error" content="{s name='DetailBuyInfoNotAvailable' namespace='frontend/detail/buy'}{/s}"}
                                            {/if}
                                        {/block}

                                        {* Product email notification *}
                                        {block name="frontend_detail_index_notification"}
                                            {if $sArticle.notification && $sArticle.instock <= 0 && $ShowNotification}
                                                {include file="frontend/plugins/notification/index.tpl"}
                                            {/if}
                                        {/block}

                                        {* Product data *}
                                        {block name='frontend_detail_index_buy_container_inner'}
                                            <div itemprop="offers" itemscope itemtype="{if $sArticle.sBlockPrices}http://schema.org/AggregateOffer{else}http://schema.org/Offer{/if}" class="buybox--inner">
                                                {block name='frontend_detail_index_after_data'}{/block}

                                                {* Configurator drop down menu's *}
                                                {block name="frontend_detail_index_configurator"}
                                                    <div class="product--configurator">
                                                        {if $sArticle.sConfigurator}
                                                            {if $sArticle.sConfiguratorSettings.type == 1}
                                                                {include file="frontend/detail/config_step.tpl"}
                                                            {elseif $sArticle.sConfiguratorSettings.type == 2}
                                                                {include file="frontend/detail/config_variant.tpl" configurator=$sArticle.sConfigurator}
                                                            {else}
                                                                {include file="frontend/detail/config_upprice.tpl"}
                                                            {/if}
                                                        {/if}
                                                    </div>
                                                {/block}

                                                {block name='frontend_detail_index_data'}
                                                    {if $sArticle.sBlockPrices}
                                                        {$lowestPrice=false}
                                                        {$highestPrice=false}
                                                        {foreach $sArticle.sBlockPrices as $blockPrice}
                                                            {if $lowestPrice === false || $blockPrice.price < $lowestPrice}
                                                                {$lowestPrice=$blockPrice.price}
                                                            {/if}
                                                            {if $highestPrice === false || $blockPrice.price > $highestPrice}
                                                                {$highestPrice=$blockPrice.price}
                                                            {/if}
                                                        {/foreach}

                                                        <meta itemprop="lowPrice" content="{$lowestPrice}" />
                                                        <meta itemprop="highPrice" content="{$highestPrice}" />
                                                        <meta itemprop="offerCount" content="{$sArticle.sBlockPrices|count}" />
                                                    {else}
                                                        <meta itemprop="priceCurrency" content="{$Shop->getCurrency()->getCurrency()}"/>
                                                    {/if}
                                                    
                                                {/block}

                                                {* Include buy button and quantity box *}
                                                {block name="frontend_detail_index_buybox"}
                                                    {include file="frontend/detail/buy.tpl"}
                                                {/block}

                                                {* Product actions *}
                                                {block name="frontend_detail_index_actions"}
                                                {/block}
                                            </div>
                                        {/block}

                                        {* Product - Base information *}
                                        {block name='frontend_detail_index_buy_container_base_info'}
											{* Product SKU *}
											{block name='frontend_detail_data_ordernumber'}
												<li class="base-info--entry entry--sku hidden">

													{* Product SKU - Label *}
													{block name='frontend_detail_data_ordernumber_label'}
														<strong class="entry--label">
															{s name="DetailDataId" namespace="frontend/detail/data"}{/s}
														</strong>
													{/block}

													{* Product SKU - Content *}
													{block name='frontend_detail_data_ordernumber_content'}
														<meta itemprop="productID" content="{$sArticle.articleDetailsID}"/>
														<span class="entry--content" itemprop="sku">
															{$sArticle.ordernumber}
														</span>
													{/block}
												</li>
											{/block}
                                        {/block}
                                    </div>
                                {/block}
                            </div>
                        </div>

                        {* Product bundle hook point *}
                        {block name="frontend_detail_index_bundle"}{/block}

                        {block name="frontend_detail_index_detail"}

                            {* Tab navigation *}
                            {block name="frontend_detail_index_tabs"}
								<div class="tab--header hidden">
									<a href="#" class="tab--title" title="{$relatedProductStream.name}">{$relatedProductStream.name}</a>
								</div>
								<div class="tab--content content--related-product-streams-{$key} hidden">
									{include file='frontend/detail/tabs/product_streams.tpl'}
								</div>
                            {/block}
                        {/block}

                        {* Crossselling tab panel *}
                        {block name="frontend_detail_index_tabs_cross_selling"}
                        {/block}
                    </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end: Product Top -->
		{block name="frontend_detail_index_top_description"}
		{if $sArticle.product_parts_title}
			<!-- start: Product Top Description -->
			<section class="section product_top_description">
				<div class="container">
					<div class="row">
						<h2>{$sArticle.product_parts_title}</h2>
					</div>
					<div class="row hidden-md hidden-lg">
						<div class="col-xs-12 col-sm-12">
							<p>{$sArticle.product_parts_description}</p>
						</div>
					</div>
					<div class="row">
						<!-- start: Image -->
						<div class="hidden-xs hidden-sm col-md-4 col-lg-4 image">
							<img src="{$sArticle.product_explanation_image|picture}" />
						</div>
						<!-- end: Image -->
						<!-- start: Description -->
						<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 description">
							<h3>{$sArticle.product_explanation_title_1}</h3>
							<div class="hidden-md hidden-lg">
								<img src="{$sArticle.product_mobile_description_1|picture}" />
							</div>
							<p>{$sArticle.product_explanation_text_1}</p>
							<h3 class="under">{$sArticle.product_explanation_title_2}</h3>
							<div class="hidden-md hidden-lg">
								<img src="{$sArticle.product_mobile_description_2|picture}" />
							</div>
							<p>{$sArticle.product_explanation_text_2}</p>
							<h3>{$sArticle.product_explanation_title_3}</h3>
							<div class="hidden-md hidden-lg">
								<img src="{$sArticle.product_mobile_description_3|picture}" />
							</div>
							<p>{$sArticle.product_explanation_text_3}</p>
							<h3>{$sArticle.product_explanation_title_4}</h3>
							<div class="hidden-md hidden-lg">
								<img src="{$sArticle.product_mobile_description_4|picture}" />
							</div>
							<p>{$sArticle.product_explanation_text_4}</p>
						</div>
						<!-- end: Description -->
					</div>
				</div>
			</section>
			<!-- end: Product Top Description -->
			{/if}

			{if $sArticle.bottom_description_title}
			<!-- start: Product Bottom description -->
			<section class="section product_bottom_description">
				<div class="container product_bottom_description">
					<div class="row">
						<h2>{$sArticle.bottom_description_title}</h2>
					</div>
					<div class="row hidden-sm hidden-md hidden-lg mobile">
						<div class="col-xs-12 image">
							<img src="{$sArticle.bottom_description_image|picture}" />
						</div>
						<div class="col-xs-12">
							{$sArticle.bottom_description_text}
						</div>
					</div>
					<div class="row hidden-xs desktop">
						<div class="col-sm-3 col-md-3 col-lg-3">
							<img src="{$sArticle.bottom_description_image|picture}" />
						</div>
						<div class="col-sm-9 col-md-9 col-lg-9">
							{$sArticle.bottom_description_text}
						</div>
					</div>
				</div>
			</section>
			<!-- end: Product Bottom description -->
			{/if}
			{if $sArticle.sSimilarArticles}
				<!-- start: More Models -->
				<section class="section other_models">
					<div class="container">
						<div class="row">
							<h2>{s name='OtherChalaModels'}{/s}</h2>
						</div>
						<div class="row">
							{* Similar products slider *}
							{if $sArticle.sSimilarArticles}
								{block name="frontend_detail_index_tabs_similar"}
									{block name="frontend_detail_index_tabs_similar_inner"}
											{block name="frontend_common_product_slider_items"}
												{foreach $sArticle.sSimilarArticles as $article}
													<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 item {$article.item_style}">
														{block name="frontend_common_product_slider_item"}
															{include file="frontend/listing/box_article.tpl" sArticle=$article productBoxLayout='detail'}
														{/block}
													</div>
												{/foreach}
											{/block}
									{/block}
								{/block}
							{/if}
							<!-- end: Description -->
						</div>
					</div>
				</section>
				<!-- end: Product Top Description -->
			{/if}
			
			{include file="frontend/blog/related_articles.tpl" relatedBlog=$relatedBlog layout="articles" box_style="v_list"}
		{/block}
{/block}
