{$configurator = $sArticle.sConfigurator}

{block name='frontend_detail_configurator_variant'}
    <div class="konfigurator--variant configurator--variant">

        {block name='frontend_detail_configurator_variant_form'}
            <form method="post" action="{url sArticle=$sArticle.articleID sCategory=$sArticle.categoryID}"
                  class="configurator--form">
                {foreach from=$configurator item=configuratorGroup name=cfg}
                    {block name='frontend_detail_configurator_variant_group'}
                        <div id="filters_{$smarty.foreach.cfg.iteration}" class="filters variant--group">
                            {block name='frontend_detail_configurator_variant_group_name'}
                                {if $configuratorGroup.groupname}<h2>{$configuratorGroup.groupname}</h2>{/if}
                            {/block}

                            {block name='frontend_detail_configurator_variant_group_options'}
                                {foreach from=$configuratorGroup.values item=option name=opt}
                                    {block name='frontend_detail_configurator_variant_group_option'}
                                        <div class="radio_wrapper {if $smarty.foreach.cfg.last && {$option.optionname|count_characters} > 5}auto_width{/if} {if $option.user_selected && $option.selectable}active{/if}" >
                                            <div class="input variant--option{if $option.media} is--image{/if} item {if $option.user_selected && $option.selectable}active{/if}"
                                                 id="{$option.optionname|lower|replace:' ':'-'|replace:'ü':'u'|replace:'ö':'o'|replace:'ä':'a'|replace:'ß':'ss'}"
                                                 data-id="{$option.optionID}">

                                                {block name='frontend_detail_configurator_variant_group_option_input'}
                                                    {if $option.media.thumbnails[0].source || $smarty.foreach.cfg.last}
                                                        <img src="{$option.media.thumbnails[0].source}">
                                                    {/if}
                                                    <input type="radio"
                                                           class="option--input"
                                                           id="group[{$option.groupID}][{$option.optionID}]"
                                                           name="group[{$option.groupID}]"
                                                           value="{$option.optionID}"
                                                           title="{$option.optionname}"
                                                           data-ajax-select-variants="true"
                                                           {if !$option.selectable}disabled="disabled"{/if}
                                                            {if $option.selected && $option.selectable}checked="checked"{/if} />
                                                {/block}
                                            </div>

                                            {block name='frontend_detail_configurator_variant_group_option_label'}
                                                <label for="group[{$option.groupID}][{$option.optionID}]"
                                                       class="option--label{if !$option.selectable} is--disabled{/if}">
                                                            {block name='frontend_detail_configurator_variant_group_option_label_text'}
                                                                <span>{$option.optionname}</span>
                                                            {/block}
                                                </label>
                                            {/block}
                                        </div>
                                    {/block}
                                {/foreach}
                                <div class="radio_wrapper auto_width tailor_made_trigger">
                                    <label for="radio_6_17">
                                        <span>{s name="DetailTailorMade" namespace="frontend/detail/index"}{/s}</span>
                                    </label>
                                </div>
                            {/block}
                        </div>
                        <div class="clearfix"></div>
                    {/block}
                {/foreach}
            </form>
        {/block}
    </div>
{/block}

{block name='frontend_detail_configurator_variant_reset'}
{/block}
