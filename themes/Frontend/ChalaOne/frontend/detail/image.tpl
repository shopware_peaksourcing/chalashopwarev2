{block name="frontend_detail_image"}
    {* Product image - Thumbnails *}
    {block name='frontend_detail_image_thumbs'}
    {/block}
    <div class="preview">
        {* Product image - Gallery *}
        {block name="frontend_detail_image_box"}
            {strip}
                    {block name='frontend_detail_image_default_image_slider_item'}
                        {block name='frontend_detail_image_default_image_element'}
							{*{foreach $sArticle.images as $image name=img}
								{if ! $image.attribute.not_in_gallery}
									{$sPreviewImage = $image}
									{break}
								{/if}
							{/foreach}*}
							{$sPreviewImage = $sArticle.articleID|articleMainImage}
							
							{$alt = $sArticle.articleName|escape}

							{if $sPreviewImage.description}
								{$alt = $sPreviewImage.description|escape}
							{/if}
							{block name='frontend_detail_image_default_image_media'}
								{if isset($sPreviewImage.thumbnails.600x600)}
									{block name='frontend_detail_image_default_picture_element'}
										<img src="{$sPreviewImage.thumbnails.600x600}" alt="{$alt}" itemprop="image" class="active" number="0"/>
									{/block}
								{else}
									{block name='frontend_detail_image_fallback'}
										<img src="{link file='frontend/_public/src/img/no-picture.jpg'}" alt="{$alt}" itemprop="image" number="0"/>
									{/block}
								{/if}
							{/block}
							
                        {/block}
                    {/block}

                    {foreach $sArticle.images as $image name=img}
                        {block name='frontend_detail_images_image_slider_item'}
                            {block name='frontend_detail_images_image_element'}
								{if ! $image.attribute.not_in_gallery}
									{$alt = $sArticle.articleName|escape}

									{if $image.description}
										{$alt = $image.description|escape}
									{/if}

									{block name='frontend_detail_images_image_media'}
										{if isset($image.thumbnails)}
											{block name='frontend_detail_images_picture_element'}
												<img srcset="{$image.thumbnails[1].sourceSet}" alt="{$alt}" itemprop="image" number="{$smarty.foreach.img.iteration}"
												class="{if $smarty.foreach.img.iteration == 0}active{/if}"/>
											{/block}
										{else}
											{block name='frontend_detail_images_fallback'}
												<img src="{link file='frontend/_public/src/img/no-picture.jpg'}" alt="{$alt}" itemprop="image" number="{$smarty.foreach.img.iteration}" />
											{/block}
										{/if}
									{/block}
								{/if}	
                            {/block}
                        {/block}
                    {/foreach}
            {/strip}
        {/block}
    </div>
    {* Product image - Thumbnails *}
    {block name='frontend_detail_image_thumbs'}
        {include file="frontend/detail/images.tpl"}
    {/block}
    {* Product image - Dot navigation *}
    {block name='frontend_detail_image_box_dots'}
    {/block}
{/block}