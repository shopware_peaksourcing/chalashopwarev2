{extends file='frontend/detail/buy.tpl'}

{block name="frontend_detail_buy"}
    <form name="sAddToBasket" method="post" action="{url controller=checkout action=addArticle}" class="buybox--form"
          data-add-article="true" data-eventName="submit" data-showModal="false">
        {block name="frontend_detail_buy_configurator_inputs"}
            {if $sArticle.sConfigurator&&$sArticle.sConfiguratorSettings.type==3}
                {foreach $sArticle.sConfigurator as $group}
                    <input type="hidden" name="group[{$group.groupID}]" value="{$group.selected_value}"/>
                {/foreach}
            {/if}
        {/block}

        <input type="hidden" name="sActionIdentifier" value="{$sUniqueRand}"/>
        <input type="hidden" name="sAddAccessories" id="sAddAccessories" value=""/>

        {* @deprecated - Product variants block *}
        {block name='frontend_detail_buy_variant'}{/block}

        <input type="hidden" name="sAdd" value="{$sArticle.ordernumber}"/>
        {* "Buy now" button *}
        {block name="frontend_detail_buy_button"}
            <div class="buybox--button-container block-group{if $NotifyHideBasket && $sArticle.notification && $sArticle.instock <= 0} is--hidden{/if}">
                {if $sArticle.sConfigurator && !$activeConfiguratorSelection}
                    <button class="btn buy is--disabled" aria-disabled="true"
                            name="{s name="DetailBuyActionAdd"}{/s}"{if $buy_box_display} style="{$buy_box_display}"{/if}>
                        {s name="DetailBuyActionAdd"}{/s}
                    </button>
                {else}
                    <button class="btn buy"
                            name="{s name="DetailBuyActionAdd"}{/s}"{if $buy_box_display} style="{$buy_box_display}"{/if}>
                        {s name="DetailBuyActionAdd"}{/s}
                    </button>
                {/if}
            </div>
        {/block}
        {include file="frontend/detail/data.tpl" sArticle=$sArticle sView=1 configShippingData=1}
    </form>
{/block}
