{block name="frontend_detail_data"}

    {if !$sArticle.liveshoppingData.valid_to_ts}

        {* Tax information *}
        {block name='frontend_detail_data_tax'}
            <p class="product--tax info" data-content="" data-modalbox="true" data-targetSelector="a" data-mode="ajax">
                {s name="DetailDataPriceInfo"}{/s}
                {if $configShippingData}{s name="DetailDataShippingtime"}{/s}{/if}
            </p>
        {/block}
    {/if}

    {if $sArticle.sBlockPrices && (!$sArticle.sConfigurator || $sArticle.pricegroupActive)}
        {foreach $sArticle.sBlockPrices as $blockPrice}
            {if $blockPrice.from == 1}
                <input id="price_{$sArticle.ordernumber}" type="hidden" value="{$blockPrice.price|replace:",":"."}">
            {/if}
        {/foreach}
    {/if}

    {block name="frontend_detail_data_delivery"}
    {/block}

    {* @deprecated Liveshopping data *}
    {block name="frontend_detail_data_liveshopping"}{/block}
{/block}
