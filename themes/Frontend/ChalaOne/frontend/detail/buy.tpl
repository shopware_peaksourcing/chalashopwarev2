{block name="frontend_detail_buy"}
    <form name="sAddToBasket" method="post" action="{url controller=checkout action=addArticle}" class="buybox--form"
          data-add-article="true" data-eventName="submit" data-showModal="false">
        {block name="frontend_detail_buy_configurator_inputs"}
            {if $sArticle.sConfigurator&&$sArticle.sConfiguratorSettings.type==3}
                {foreach $sArticle.sConfigurator as $group}
                    <input type="hidden" name="group[{$group.groupID}]" value="{$group.selected_value}"/>
                {/foreach}
            {/if}
        {/block}

        <input type="hidden" name="sActionIdentifier" value="{$sUniqueRand}"/>
        <input type="hidden" name="sAddAccessories" id="sAddAccessories" value=""/>

        {* @deprecated - Product variants block *}
        {block name='frontend_detail_buy_variant'}{/block}

        <input type="hidden" name="sAdd" value="{$sArticle.ordernumber}"/>


        {* Graduated prices *}
        {if $sArticle.sBlockPrices && !$sArticle.liveshoppingData.valid_to_ts}

            {* Include block prices *}
            {block name="frontend_detail_data_block_price_include"}
                {include file="frontend/detail/block_price.tpl" sArticle=$sArticle}
            {/block}

            {* @deprecated Block *}
            {block name='frontend_detail_data_price_info'}{/block}
        {else}
            <div class="product--price price--default{if $sArticle.has_pseudoprice} price--discount{/if}">

                {* Default price *}
                {block name='frontend_detail_data_price_configurator'}
                    {if $sArticle.priceStartingFrom && !$sArticle.sConfigurator && $sView}
                        {* Price - Starting from *}
                        {block name='frontend_detail_data_price_configurator_starting_from_content'}
                            <p class="price">
                                {s name="DetailDataInfoFrom"}{/s} {$sArticle.priceStartingFrom|currency} {s name="Star" namespace="frontend/listing/box_article"}{/s}
                            </p>
                        {/block}
                    {else}
                        {* Regular price *}
                        {block name='frontend_detail_data_price_default'}
                            <p class="price">
                                <meta itemprop="price" content="{$sArticle.price|replace:',':'.'}">
                                {if $sArticle.priceStartingFrom && !$sArticle.liveshoppingData}{s name='ListingBoxArticleStartsAt' namespace="frontend/listing/box_article"}{/s} {/if}{$sArticle.price|currency} {s name="Star" namespace="frontend/listing/box_article"}{/s}
                            </p>
                        {/block}
                    {/if}
                {/block}

                {* Discount price *}
                {block name='frontend_detail_data_pseudo_price'}
                    {if $sArticle.has_pseudoprice}

                        {block name='frontend_detail_data_pseudo_price_discount_icon'}
                        {/block}

                        {* Discount price content *}
                        {block name='frontend_detail_data_pseudo_price_discount_content'}
                        {/block}
                    {/if}
                {/block}
            </div>
        {/if}
        {* Unit price *}
        {if $sArticle.purchaseunit}
            {block name='frontend_detail_data_price'}
                <div class='product--price price--unit'>

                    {* Unit price label *}
                    {block name='frontend_detail_data_price_unit_label'}
                        <span class="price--label label--purchase-unit">
							{s name="DetailDataInfoContent"}{/s}
						</span>
                    {/block}

                    {* Unit price content *}
                    {block name='frontend_detail_data_price_unit_content'}
                        {$sArticle.purchaseunit} {$sArticle.sUnit.description}
                    {/block}

                    {* Unit price is based on a reference unit *}
                    {if $sArticle.purchaseunit && $sArticle.purchaseunit != $sArticle.referenceunit}

                        {* Reference unit price content *}
                        {block name='frontend_detail_data_price_unit_reference_content'}
                            ({$sArticle.referenceprice|currency} {s name="Star" namespace="frontend/listing/box_article"}{/s}
                            / {$sArticle.referenceunit} {$sArticle.sUnit.description})
                        {/block}
                    {/if}
                </div>
            {/block}
        {/if}

        {* "Buy now" button *}
        {block name="frontend_detail_buy_button"}
            <div class="buybox--button-container block-group{if $NotifyHideBasket && $sArticle.notification && $sArticle.instock <= 0} is--hidden{/if}">
                {if $sArticle.sConfigurator && !$activeConfiguratorSelection}
                    <button class="btn buy is--disabled" disabled="disabled" aria-disabled="true"
                            name="{s name="DetailBuyActionAdd"}{/s}"{if $buy_box_display} style="{$buy_box_display}"{/if}>
                        {s name="DetailBuyActionAdd"}{/s}
                    </button>
                {else}
                    <button class="btn buy"
                            name="{s name="DetailBuyActionAdd"}{/s}"{if $buy_box_display} style="{$buy_box_display}"{/if}>
                        {s name="DetailBuyActionAdd"}{/s}
                    </button>
                {/if}
            </div>
        {/block}

        {include file="frontend/detail/data.tpl" sArticle=$sArticle sView=1}
        <ul>
            <li><span>{s name="DetailDataShippingtime"}{/s}</span></li>
        </ul>
    </form>
{/block}