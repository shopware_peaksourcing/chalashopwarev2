{* Listing wrapper *}
{block name="frontend_listing_listing_wrapper"}

        {block name="frontend_listing_listing_container"}

                {block name="frontend_listing_listing_content"}
                    <!-- start: Accessries List -->
                    <section class="section accessories_list listing"
                         data-ajax-wishlist="true"
                         data-compare-ajax="true"
                            {if $theme.infiniteScrolling}
                                data-infinite-scrolling="true"
                                data-loadPreviousSnippet="{s name="ListingActionsLoadPrevious"}{/s}"
                                data-loadMoreSnippet="{s name="ListingActionsLoadMore"}{/s}"
                                data-categoryId="{$sCategoryContent.id}"
                                data-pages="{$pages}"
                                data-threshold="{$theme.infiniteThreshold}"{/if}>

                            <div class="container">
							
								{* Headline *}
								{block name="frontend_listing_text_headline"}
									{if $sCategoryContent.cmsheadline}
										<div class="row">
											<h1>{$sCategoryContent.cmsheadline}</h1>
										</div>
									{/if}
								{/block}
								
								{* Sorting and changing layout *}
								{block name="frontend_listing_top_actions"}
									{*{include file='frontend/listing/listing_actions.tpl'}*}
								{/block}
							
                                {* Actual listing *}
								
                                {include file="frontend/index/accessory-navigation.tpl"}

                                {block name="frontend_listing_list_inline"}
                                    {foreach $sArticles as $sArticle}
                                        {include file="frontend/listing/box_article.tpl" productBoxLayout='accessory'}
                                    {/foreach}
                                {/block}
								
								{* Paging *}
								{block name="frontend_listing_bottom_paging"}
									<div class="row">
										<div class="listing--bottom-paging">
											{include file="frontend/listing/actions/action-pagination.tpl"}
										</div>
									</div>
								{/block}								
                            </div>
                    </section>
                    <!-- end: Accessries List -->
                {/block}
        {/block}
{/block}
