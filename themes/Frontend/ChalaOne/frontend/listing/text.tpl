{namespace name="frontend/listing/listing"}

{* Categorie headline *}
{block name="frontend_listing_text"}
    {if $sCategoryContent.cmsheadline || $sCategoryContent.cmstext}
        <div class="text row title">
            <div class="col-xs-12 col-sm-12 col-md-10 col-md-push-1 col-lg-10 col-lg-push-1">
                {* Headline *}
                {block name="frontend_listing_text_headline"}
                    {if $sCategoryContent.cmsheadline}
                        <h1>{$sCategoryContent.cmsheadline}</h1>
                    {/if}
                {/block}

                {* Category text *}
                {block name="frontend_listing_text_content"}
                    {if $sCategoryContent.cmstext}

                        {* Long description *}
                        {block name="frontend_listing_text_content_long"}
                            {$sCategoryContent.cmstext|unescape:'html'}
                        {/block}

                        {* Short description *}
                        {block name="frontend_listing_text_content_short"}
                        {/block}

                        {* Off Canvas Container *}
                        {block name="frontend_listing_text_content_offcanvas"}
                        {/block}
                    {/if}
					{*{if $sCategoryContent.media && {$sCategoryContent.attribute.page_template != 'filter' || $sCategoryContent.attribute.page_template != 'model'}}
						<p>{$sCategoryContent.attribute.page_template}</p>
						<img src="{$sCategoryContent.media.source}">
                    {/if}*}
                {/block}
            </div>
        </div>
    {/if}
{/block}
