{block name="frontend_listing_box_article_includes"}

    {if $productBoxLayout == 'minimal'}
        {include file="frontend/listing/product-box/box-minimal.tpl"}

    {elseif $productBoxLayout == 'image'}
        {include file="frontend/listing/product-box/box-big-image.tpl"}

    {elseif $productBoxLayout == 'slider'}
        {include file="frontend/listing/product-box/box-product-slider.tpl"}

    {elseif $productBoxLayout == 'detail'}
        {include file="frontend/listing/product-box/box-product-detail.tpl"}

    {elseif $productBoxLayout == 'emotion'}
        {include file="frontend/listing/product-box/box-emotion.tpl"}

    {elseif $productBoxLayout == 'blog'}
        {include file="frontend/listing/product-box/box-blog.tpl"}

    {elseif $productBoxLayout == 'accessory'}
        {include file="frontend/listing/product-box/box-accessory.tpl" productBoxLayout='accessory'}
		
    {else}
        {block name="frontend_listing_box_article_includes_additional"}
            {include file="frontend/listing/product-box/box-basic.tpl" productBoxLayout="basic"}
        {/block}
    {/if}
{/block}