{extends file='frontend/listing/index.tpl'}
{* Main content *}

{block name="frontend_index_content_main"}
    {if $sCategoryContent.cmstext}

        {* Long description *}
        {block name="frontend_listing_text_content_long"}
            <section class="section" id="service_faq">
                {$sCategoryContent.cmstext|unescape:'html'}
            </section>
        {/block}

        {* Short description *}
        {block name="frontend_listing_text_content_short"}
        {/block}

        {* Off Canvas Container *}
        {block name="frontend_listing_text_content_offcanvas"}
        {/block}
    {/if}
{/block}