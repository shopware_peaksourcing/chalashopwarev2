{extends file='frontend/listing/index.tpl'}
{* Main content *}
{block name='frontend_index_content_main'}
    <section class="section" id="press_images">
        <div class="container">
            <div class="row title">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <a href="{$categoryParent.link}">{s namespace='frontend/index/index' name='BackToOverview'}Zurück zur Übersicht{/s}</a>
                    <h1>{$sCategoryContent.cmsHeadline}</h1>
                </div>
            </div>
            <div class="row title">
                <div class="col-xs-12 col-sm-12 col-md-10 col-md-push-1 col-lg-10 col-lg-push-1">
                    {if $sCategoryContent.cmsText}
                        {$sCategoryContent.cmsText|unescape:'html'}
                    {/if}
                </div>
            </div>
            <div class="row links">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <a href="#"
                       class="item slideshow">{s namespace='frontend/index/index' name='StartSlideShow'}Slideshow starten{/s}</a>
                    {$complete_album = $sCategoryContent.attribute.complete_album|media}
                    <a href="{$complete_album.source}"
                       class="item zip">{s namespace='frontend/index/index' name='CompleteAlbum'}Gesamtes Album{/s}
                        [{$complete_album.extension|upper}, {$complete_album.filesize}]</a>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    {if $sCategoryContent.attribute.contact_text && $sCategoryContent.attribute.contact_image}
                        <div class="contact">
                            {if $sCategoryContent.attribute.contact_image}
                                <img src="{$sCategoryContent.attribute.contact_image|picture}">
                            {/if}
                            {$sCategoryContent.attribute.contact_text}
                        </div>
                    {/if}
                </div>
            </div>
            <div class="row list">
                {foreach from=$sCategoryContent.attribute.images|toarray key=i item=image}
                    {$image = $image|media}
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 item">
                        <div class="wrapper">
                            <a href="{$image.source}" class="fancy-slide" rel="press-gallery"></a>
                            <a href="{$image.source}" download="{$image.filename}">
                                <img src="{if $image.thumbnail.360x240}{$image.thumbnail.360x240}{else}{$image.source}{/if}">
                            </a>
                            <div class="description">
                                <h3>{$image.filename}</h3>
                                <p>
                                    {if $sCategoryContent.attribute.custom_date}
                                        {$sCategoryContent.attribute.custom_date|date_format:"%d.%m.%Y"}
                                    {/if}
                                    {$image.extension|upper} / {$image.filesize}
                                </p>
                            </div>
                        </div>
                    </div>
                {/foreach}
            </div>
        </div>
    </section>
{/block}