{extends file='frontend/listing/index.tpl'}
{* Main content *}
{block name='frontend_index_content_main'}
    <section id="story_top">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <a href="{$categoryParent.link}">{s namespace='frontend/index/index' name='BackToOverview'}Zurück zur Übersicht{/s}</a>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <img src="{$sCategoryContent.mediaId|picture}">
                </div>
            </div>
        </div>
    </section>
    <section id="press_content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 left">
                    <div class="wrapper">
                        <h1>{$sCategoryContent.cmsHeadline}</h1>
                        <p class="user_and_date">
                            {if $sCategoryContent.attribute.custom_date}
                                {$sCategoryContent.attribute.custom_date|date_format:"%d.%m.%Y"}
                            {/if}
                            {if $sCategoryContent.attribute.additional_description}
                                / {$sCategoryContent.attribute.additional_description|strip_tags}
                            {/if}
                        </p>
                        {$sCategoryContent.cmsText}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 right">
                    {if $sCategoryContent.attribute.facebook_link}
                        <a href="{$sCategoryContent.attribute.facebook_link}" class="item fb">Facebookevent anzeigen</a>
                    {/if}
                    {if $sCategoryContent.attribute.instagram_link}
                        <a href="{$sCategoryContent.attribute.instagram_link}" class="item instagram">Instagram Stream
                            anzeigen</a>
                    {/if}
                    {if $sCategoryContent.attribute.callendar_link}
                        <a href="{$sCategoryContent.attribute.callendar_link}" class="item event">Event zum eigenen
                            Kalender hinzufügen (.ical)</a>
                    {/if}
                    <div class="contact">
                        {if $sCategoryContent.attribute.contact_image}
                            <img src="{$sCategoryContent.attribute.contact_image|picture}">
                        {/if}
                        {$sCategoryContent.attribute.contact_text}
                    </div>

                    <div class="item model {$sMenuSelectedProducts[0].name|lower|replace:' ':'-'}">
                        <a href="{$sMenuSelectedProducts[0].link}">
                            <img src="{$sMenuSelectedProducts[0].image.600x600}">
                        </a>
                        {if $sMenuSelectedProducts[0].description_long}
                            {$sMenuSelectedProducts[0].description_long|truncate:120|unescape:'html'}
                        {/if}
                        <p>Product which is mentioned in the article with link to product detail page</p>
                    </div>

                    <h3>Downloads</h3>
                    {if $sCategoryContent.attribute.images}
                        {foreach from=$sCategoryContent.attribute.images|toarray key=i item=image}
                            {$image = $image|media}
                            {if $image.extension == 'zip'}
                                <a href="{$image.source}" class="item zip" download>
                                    {$image.filename} [{$image.extension}, {$image.filesize}]
                                </a>
                            {elseif $image.extension == 'pdf'}
                                <a href="{$image.source}" class="item pdf" download>
                                    {$image.filename} [{$image.extension}, {$image.filesize}]
                                </a>
                            {else}
                                <a href="{$image.source}" class="item image" download>
                                    <img src="{if $image.thumbnail.140x140}{$image.thumbnail.140x140}{else}{$image.source}{/if}">
                                    <span>{$image.filename} [{$image.extension}, {$image.filesize}]</span>
                                </a>
                            {/if}
                        {/foreach}
                    {/if}
                </div>
            </div>
        </div>
    </section>
{/block}
