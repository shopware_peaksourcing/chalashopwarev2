{extends file='frontend/listing/index.tpl'}
{* Main content *}
{block name='frontend_index_content_main'}
    <section class="section" id="press_overview">
        <div class="container">
            <div class="row title">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h1>{$sCategoryContent.cmsHeadline}</h1>
                </div>
            </div>
            <div class="row list" id="list_1">

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 left">
                    <h2>Pressemitteilungen, Events, Bilder</h2>
                    {foreach from=$sCategories[$sCategoryContent.parentId].subcategories[$sCategoryContent.id].subcategories item=categories}
                        {if $categories.attribute.page_template == 'press_event'}
                            <a href="{$categories.link}" class="item">
                        <span class="info">
                          {if $categories.attribute.custom_date}
                              {$categories.attribute.custom_date|date_format:"%d.%m.%Y"}
                          {/if}
                            {if $categories.attribute.additional_description}
                                / {$categories.attribute.additional_description|strip_tags}
                            {/if}
                        </span>
                                <span class="title">{$categories.name}</span>
                            </a>
                        {/if}
                    {/foreach}
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 right">
                    {if $sCategoryContent.attribute.contact_text && $sCategoryContent.attribute.contact_image}
                        <h2>Pressekontakt</h2>
                        <div class="contact">
                            {if $sCategoryContent.attribute.contact_image}
                                <img src="{$sCategoryContent.attribute.contact_image|picture}">
                            {/if}
                            {$sCategoryContent.attribute.contact_text}
                        </div>
                    {/if}
                    <h2>Medien</h2>
                    {foreach from=$sCategories[$sCategoryContent.parentId].subcategories[$sCategoryContent.id].subcategories item=categories}
                        {if $categories.attribute.page_template == 'press_image'}
                            <a href="{$categories.link}" class="item">{$categories.name}</a>
                        {/if}
                    {/foreach}
                </div>

            </div>
        </div>
    </section>
{/block}
