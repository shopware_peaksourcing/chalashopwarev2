{extends file='frontend/listing/index.tpl'}
{* Main content *}

{block name="frontend_index_content_main"}

    {$foo = $sCategoryContent.attribute.top_slider_images|substr:1:-1}
    {$imagesArray = "|"|explode:$foo}

    <!-- start: Product Slider -->
    <section id="product_slider">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 slider">
                    {foreach $imagesArray as $imagePath}
                        {$slideImage = $imagePath|media}
                        <!-- start: Slider Item -->
                        <div class="item">
                            <img src="{$slideImage.thumbnail.1140x380}">
                        </div>
                        <!-- end: Slider Item -->
                    {/foreach}
                </div>
            </div>
        </div>
    </section>
    <!-- end: Product Slider -->
    <section id="product_comparison" class="section">
        <div class="container">
            <div class="row">
                <h2>{s name="ProductCompareTitle"}Unsere {$customArticleList|@count} Schmuckstücke{/s}</h2>
            </div>
            <div class="row hidden-md hidden-lg top_mobile_nav">
                <p>{s name="SliderGuide"}{/s}</p>
                <a href="#" class="prev"></a>
                <span class="pointer"></span>
                <a href="#" class="next"></a>
            </div>
            <div class="row products">
                {foreach from=$customArticleList item=article}
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="item {$article.item_style}">
                            <h3 class="model">
                                <a href="{$article.linkDetails}">{$article.articleName}</a>
                                <div class="product_icon_letter"
                                     {if $article.product_color}style="background: {$article.product_color};"{/if}>
                                    <img class="icon-image" src="/{$article.product_icon_letter|picture}">
                                </div>
                            </h3>
                            <a href="{$article.linkDetails}"><img src="{$article.image.source}"></a>
                            {$article.compare_description}
                            <a href="{$article.linkDetails}"
                               class="button">{s name="ProductLinkDetail"}Zeig mir die {$article.articleName} im Detail{/s}</a>
                        </div>
                    </div>
                {/foreach}
            </div>
            <div class="row bottom_mobile_nav hidden-md hidden-lg">
                {foreach from=$customArticleList item=article}
                    <a class="{$article.item_style}">
                        <span>{$article.item_style|capitalize}</span>
                        <img src="{$article.image.source}">
                    </a>
                {/foreach}
            </div>
        </div>
    </section>
    <!-- start: Mobile Configurator Link -->
    <section id="mobile_configurator_link" class="section hidden-md hidden-lg">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <div class="wrapper">
                        <a href="{$configuratorLink}">
                            <span>{$sCategoryContent.attribute.configurator_text|unescape:'html'}</span>
                            <img src="/{$sCategoryContent.attribute.configurator_image|picture}">
                        </a>
                        <a href="{$configuratorLink}" class="button">{s name="ConfiguratorLink"}{/s}</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end: Mobile Configurator Link -->

    {include file="frontend/blog/related_articles.tpl" relatedBlog=$relatedBlogCategory layout="articles" box_style="v_list"}

{/block}