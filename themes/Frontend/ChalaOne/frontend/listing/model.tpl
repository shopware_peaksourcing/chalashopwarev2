{* Define all necessary template variables for the listing *}
{block name="frontend_listing_index_layout_variables"}

    {* Count of available product pages *}
    {$pages = ceil($sNumberArticles / $criteria->getLimit())}

    {* Controller url for the found products counter *}
    {$countCtrlUrl = "{url module="widgets" controller="listing" action="listingCount" params=$ajaxCountUrlParams fullPath}"}

    {* Layout for the product boxes *}
    {$productBoxLayout = 'basic'}

    {if $sCategoryContent.productBoxLayout !== null &&
    $sCategoryContent.productBoxLayout !== 'extend'}
        {$productBoxLayout = $sCategoryContent.productBoxLayout}
    {/if}
{/block}

{* Headline *}
{block name="frontend_listing_text_headline"}
    {if $sCategoryContent.cmsheadline}
        <h1>{$sCategoryContent.cmsheadline}</h1>
    {/if}
{/block}
{if $sCategoryContent.attribute.page_template == 'model'}
    {include file='frontend/listing/emotion.tpl'}
{/if}

{block name="frontend_listing_index_listing"}
    {include file='frontend/listing/listing_accessories.tpl' productBoxLayout="accessory"}
{/block}

{* Tagcloud *}
{block name="frontend_listing_index_tagcloud"}
    {if {config name=show namespace=TagCloud }}
        {action module=widgets controller=listing action=tag_cloud sController=listing sCategory=$sCategoryContent.id}
    {/if}
{/block}