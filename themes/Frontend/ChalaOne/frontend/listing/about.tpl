{extends file='frontend/listing/index.tpl'}
{* Main content *}

{block name="frontend_index_content_main"}
    <!-- start: About Top Image -->
    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h1>{$sCategoryContent.cmsHeadline}</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <img src="{$sCategoryContent.media.source}">
                </div>
            </div>
        </div>
    </section>
    <!-- end: About Top Image -->

    <!-- start: Content -->
    <section id="about">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="wrapper">
                        {$sCategoryContent.cmsText}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end: Content -->

    <!-- start: Articles -->

    {include file="frontend/blog/related_articles.tpl" relatedBlog=$relatedBlogCategory layout="video_and_support" box_style="item"}

{/block}