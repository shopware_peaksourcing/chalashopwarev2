{extends file='parent:frontend/listing/index.tpl'}
{block name='frontend_index_content'}
    <div class="custom-listing">
        {$smarty.block.parent}
    </div>
{/block}

{block name="frontend_listing_index_text"}
	{if $sCategoryContent.cmsHeadline || $sCategoryContent.cmsText}
		<div class="custom-listing--intro">
			<h1 class="intro--cmsHeadline">{$sCategoryContent.cmsHeadline}</h1>
			<span class="intro--cmsText">{$sCategoryContent.cmsText}</span>
		</div>
	{/if}
{/block}

{block name="frontend_listing_list_inline"}
    {$productBoxLayout = "custom"}
    <div class="custom-listing--listing">
        {foreach $sArticles as $sArticle}
            {include file="frontend/listing/box-custom.tpl"}
        {/foreach}
    </div>
{/block}