{extends file='frontend/listing/index.tpl'}
{* Main content *}
{block name='frontend_index_content_main'}
    <!-- start: Services -->
    <section class="section" id="service_overview">
        <div class="container">
            <div class="row">
                <!-- start: Left -->
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 left">
                    {block name="frontend_listing_index_faq"}{/block}

                    {foreach from=$sAdvancedMenu item=categories}
                        {if $categories.attribute.page_template == 'service'}
                            {foreach from=$categories.sub item=subCategories}
                                {if $subCategories.attribute.page_template == 'anleitungen'}
                                    <h2>{$subCategories.name}</h2>
                                    {foreach from=$subCategories.sub item=subCategory}
                                        <div class="list_item">
                                            <img src="{$subCategory.media.source}">
                                            <h3>{$subCategory.cmsHeadline}</h3>
                                            {$subCategory.cmsText}
                                            <div class="inline">
                                                <a href="{$subCategory.link}">Ansehen</a>
                                                {$subCategory.attribute.additional_description}
                                            </div>
                                        </div>
                                    {/foreach}
                                    <a href="{$subCategories.link}" class="button">Alle Anleitungen zeigen</a>
                                {/if}
                            {/foreach}
                        {/if}
                    {/foreach}

                </div>
                <!-- end: Left -->
                <!-- start: Right -->
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 right">
                    {foreach from=$sAdvancedMenu item=categories}
                        {if $categories.attribute.page_template == 'service'}
                            {foreach from=$categories.sub item=subCategories}
                                {if $subCategories.attribute.page_template == 'kurse'}
                                    <h2>{$subCategories.name}</h2>
                                    {foreach from=$subCategories.sub item=subCategory}
                                        <div class="list_item">
                                            <img src="{$subCategory.media.source}">
                                            <h3>{$subCategory.cmsHeadline}</h3>
                                            <div class="date">
                                                {$subCategory.attribute.custom_date|date_format:"%d. %B %Y"}
                                                {$subCategory.attribute.additional_description}
                                            </div>
                                            {$subCategory.cmsText}
                                            <a href="{$subCategory.link}">Mehr</a>
                                        </div>
                                    {/foreach}
                                    <a href="{$subCategories.link}" class="button">Alle Kurse zeigen</a>
                                {/if}
                            {/foreach}
                        {/if}
                    {/foreach}

                    {block name="frontend_listing_index_delivery"}{/block}
                </div>
                <!-- end: Right -->
            </div>
        </div>
    </section>
    <!-- end: Services -->
{/block}