{extends file='frontend/index/index.tpl'}

{block name='frontend_index_content_left'}
{/block}

{* Breadcrumb *}
{block name='frontend_index_breadcrumb'}
{/block}

{* Main content *}
{block name='frontend_index_content_main'}

        {* Banner *}
        {block name="frontend_listing_index_banner"}
            {if !$hasEmotion}
                {include file='frontend/listing/banner.tpl'}
            {/if}
        {/block}

        {* Topseller *}
        {block name="frontend_listing_index_topseller"}
{*            {if !$hasEmotion && {config name=topSellerActive}}
                {action module=widgets controller=listing action=top_seller sCategory=$sCategoryContent.id}
            {/if}*}
        {/block}
		
        {* Category headline *}
        {block name="frontend_listing_index_text"}
            {include file='frontend/listing/text.tpl'}
            {*
            {if !$hasEmotion && $sCategoryContent.attribute.page_template != 'press_event'
                            && $sCategoryContent.attribute.page_template != 'press_image'
                            && $sCategoryContent.attribute.page_template != 'about'
                            && $sCategoryContent.attribute.page_template != 'filter'
                            && $sCategoryContent.attribute.page_template != 'faq'
                            && $sCategoryContent.attribute.page_template != 'service'
            }
                {include file='frontend/listing/text.tpl'}
            {/if}
            {if $sCategoryContent.attribute.page_template == 'service'}
                {include file='frontend/listing/services.tpl'}
            {elseif $sCategoryContent.attribute.page_template == 'about'}
                {include file='frontend/listing/about.tpl'}
            {elseif $sCategoryContent.attribute.page_template == 'compare'}
                {include file='frontend/listing/compare.tpl'}
            {elseif $sCategoryContent.attribute.page_template == 'filter'}
                {include file='frontend/listing/filter.tpl'}
            {elseif $sCategoryContent.attribute.page_template == 'model'}
                {include file='frontend/listing/model.tpl'}
            {elseif $sCategoryContent.attribute.page_template == 'press'}
                {include file='frontend/listing/press.tpl'}
            {elseif $sCategoryContent.attribute.page_template == 'faq'}
                {include file='frontend/listing/faq.tpl'}
            {elseif $sCategoryContent.attribute.page_template == 'press_event'}
                {include file='frontend/listing/press_event.tpl'}
            {elseif $sCategoryContent.attribute.page_template == 'press_image'}
                {include file='frontend/listing/press_image.tpl'}
            {/if}
            *}
        {/block}
{/block}

{* Sidebar right *}
{block name='frontend_index_content_right'}{/block}
