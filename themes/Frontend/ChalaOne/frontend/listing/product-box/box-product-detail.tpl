{extends file="frontend/listing/product-box/box-basic.tpl"}

{block name="frontend_listing_box_article_rating"}{/block}

{* Product description *}
{block name='frontend_listing_box_article_description'}
	{if $sArticle.description_long}
		<p class="product-description">
			<a href="{$sArticle.linkDetails|rewrite:$sArticle.description_long}"
			   title="{$sArticle.description_long|escapeHtml}">
				{$sArticle.description_long|strip_tags|truncate:240}
			</a>
		</p>
	{/if}
{/block}

{block name="frontend_listing_box_article_actions"}{/block}
