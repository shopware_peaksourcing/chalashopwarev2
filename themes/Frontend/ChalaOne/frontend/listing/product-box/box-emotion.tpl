{extends file="frontend/listing/product-box/box-basic.tpl"}

{namespace name="frontend/listing/box_article"}

{block name="frontend_listing_box_article"}

	{block name="frontend_listing_box_article_content"}

		{* Product badges *}
		{block name='frontend_listing_box_article_badges'}
		{/block}
		
		{block name='frontend_listing_box_article_info_container'}
		
			{* Product image *}
			{block name='frontend_listing_box_article_picture'}
			{/block}
						
			{* Product name *}
			{block name='frontend_listing_box_article_name'}
				<h3 class="model">
					<a href="{$sArticle.linkDetails|rewrite:$sArticle.articleName}"
					   class="product--title"
					   title="{$sArticle.articleName|escapeHtml}">
						{$sArticle.articleName|truncate:50|escapeHtml}
					</a>
				</h3>
			{/block}
			
			{* Customer rating for the product *}
			{block name='frontend_listing_box_article_rating'}
			{/block}
			
			{* Product image *}
			{block name='frontend_listing_box_article_picture'}
				<a href="{$sArticle.linkDetails|rewrite:$sArticle.articleName}"
				   title="{$sArticle.articleName|escape}"
				   class="product--image{if $imageOnly} is--large{/if}">

					{block name='frontend_listing_box_article_image_element'}
						{block name='frontend_listing_box_article_image_media'}

							{block name='frontend_listing_box_article_image_picture'}
								{if $sArticle.image.thumbnails}

									{$baseSource = $sArticle.image.thumbnails[0].source}
									
									
									{if $itemCols && $emotion.grid.cols && !$fixedImageSize}
										{$colSize = 100 / $emotion.grid.cols}
										{$itemSize = "{$itemCols * $colSize}vw"}
									{else}
										{$itemSize = "360px"}
									{/if}

									{foreach $sArticle.image.thumbnails as $image}
										{$srcSet = "{if $image@index !== 0}{$srcSet}, {/if}{$image.source} {$image.maxWidth}w"}

										{if $image.retinaSource}
											{$srcSetRetina = "{if $image@index !== 0}{$srcSetRetina}, {/if}{$image.retinaSource} {$image.maxWidth}w"}
										{/if}
									{/foreach}
								{elseif $sArticle.image.source}
									{$baseSource = $sArticle.image.source}
								{else}
									{$baseSource = "{link file='frontend/_public/src/img/no-picture.jpg'}"}
								{/if}

								{$desc = $sArticle.articleName|escape}

								{if $sArticle.image.description}
									{$desc = $sArticle.image.description|escape}
								{/if}
									{if $srcSetRetina}<source sizes="(min-width: 48em) {$itemSize}, 100vw" srcset="{$srcSetRetina}" media="(min-resolution: 192dpi)" />{/if}
									{if $srcSet}<source sizes="(min-width: 48em) {$itemSize}, 100vw" srcset="{$srcSet}" />{/if}
									<img src="{$baseSource}" alt="{$desc}" title="{$desc|truncate:25:""}" />
							{/block}
						{/block}
					{/block}
				</a>
			{/block}
			
			
			{block name='frontend_listing_box_article_price_info'}
			{/block}
			
		{/block}
	{/block}
{/block}
