{namespace name="frontend/listing/box_article"}

{block name="frontend_listing_box_article"}

        {block name="frontend_listing_box_article_content"}


                {* Product box badges - highlight, newcomer, ESD product and discount *}
                {block name='frontend_listing_box_article_badges'}
                {/block}

                {block name='frontend_listing_box_article_info_container'}

					{* Product image *}
					{block name='frontend_listing_box_article_picture'}
					{/block}

					{* Customer rating for the product *}
					{block name='frontend_listing_box_article_rating'}
					{/block}

					{* Product name *}
					{block name='frontend_listing_box_article_name'}
						<h3 class="model" {if $sArticle.product_color}style="border-bottom: 10px solid {$sArticle.product_color};"{/if}>
							<a href="{$sArticle.linkDetails|rewrite:$sArticle.articleName}"
							   class="product--title"
							   title="{$sArticle.articleName|escapeHtml}">
								<span>{$sArticle.articleName|truncate:50|escapeHtml}</span>
							</a>
							<div class="product_icon_letter" {if $sArticle.product_color}style="background: {$sArticle.product_color};"{/if}><img class="icon-image" src="/{$sArticle.product_icon_letter|picture}"></div>
						</h3>
					{/block}

					{$image = $sArticle.articleID|articleMainImage}
					{* Product image *}
					{block name='frontend_listing_box_article_picture'}

						<a href="{$sArticle.linkDetails|rewrite:$sArticle.articleName}"
						   title="{$sArticle.articleName|escape}"
						   class="product--image{if $imageOnly} is--large{/if}">

							{block name='frontend_listing_box_article_image_element'}
								{block name='frontend_listing_box_article_image_media'}

									{block name='frontend_listing_box_article_image_picture'}
										<img src="{$image.thumbnails.370x370}" alt="{$desc}" title="{$desc|truncate:25:""}" />
									{/block}
									<div class="product_icon_letter" {if $sArticle.product_color}style="background: {$sArticle.product_color};"{/if}>
										<img class="icon-image" src="/{$sArticle.product_icon_letter|picture}">
									</div>
									<div class="product_icon_background" {if $sArticle.product_color}style="background: {$sArticle.product_color};"{/if}>
									</div>
								{/block}
							{/block}
						</a>
					{/block}

					{* Product description *}
					{block name='frontend_listing_box_article_description'}
						{if $sArticle.description_long}
							<span class="line hidden-xs"></span>
							<p class="hidden-xs">
								<a href="{$sArticle.linkDetails|rewrite:$sArticle.description_long}"
								   title="{$sArticle.description_long|escapeHtml}">
									{$sArticle.description_long|strip_tags|truncate:120}
								</a>
							</p>
						{/if}
					{/block}

					{block name='frontend_listing_box_article_price_info'}
					{/block}

					{* Product actions - Compare product, more information, buy now *}
					{block name='frontend_listing_box_article_actions'}
					{/block}
                {/block}
        {/block}
{/block}
