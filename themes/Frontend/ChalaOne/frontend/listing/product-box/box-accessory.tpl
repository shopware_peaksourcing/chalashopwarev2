{extends file="frontend/listing/product-box/box-basic.tpl"}

{namespace name="frontend/listing/box_article"}

{block name="frontend_listing_box_article"}

    {block name="frontend_listing_box_article_content"}

        {* Product badges *}
        {block name='frontend_listing_box_article_badges'}
        {/block}

        {block name='frontend_listing_box_article_info_container'}
            <!-- start: Accessorries Item -->
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 item">
                <div class="wrapper">

                    {* Product name *}
                    {block name='frontend_listing_box_article_name'}
                        <h2>
                            <a href="{$sArticle.linkDetails|rewrite:$sArticle.articleName}"
                               title="{$sArticle.articleName|escapeHtml}">
                                {$sArticle.articleName|truncate:50|escapeHtml}
                            </a>
                        </h2>
                    {/block}

                    {* Product image *}
                    {block name='frontend_listing_box_article_picture'}
                        <a href="{$sArticle.linkDetails|rewrite:$sArticle.articleName}"
                           title="{$sArticle.articleName|escape}"
                           class="product--image{if $imageOnly} is--large{/if}">

                            {block name='frontend_listing_box_article_image_element'}
                                {block name='frontend_listing_box_article_image_media'}

                                    {block name='frontend_listing_box_article_image_picture'}
                                        {if $sArticle.image.thumbnails}

                                            {$baseSource = $sArticle.image.thumbnails[0].source}


                                            {if $itemCols && $emotion.grid.cols && !$fixedImageSize}
                                                {$colSize = 100 / $emotion.grid.cols}
                                                {$itemSize = "{$itemCols * $colSize}vw"}
                                            {else}
                                                {$itemSize = "360px"}
                                            {/if}

                                            {foreach $sArticle.image.thumbnails as $image}
                                                {$srcSet = "{if $image@index !== 0}{$srcSet}, {/if}{$image.source} {$image.maxWidth}w"}

                                                {if $image.retinaSource}
                                                    {$srcSetRetina = "{if $image@index !== 0}{$srcSetRetina}, {/if}{$image.retinaSource} {$image.maxWidth}w"}
                                                {/if}
                                            {/foreach}
                                        {elseif $sArticle.image.source}
                                            {$baseSource = $sArticle.image.source}
                                        {else}
                                            {$baseSource = "{link file='frontend/_public/src/img/no-picture.jpg'}"}
                                        {/if}

                                        {$desc = $sArticle.articleName|escape}

                                        {if $sArticle.image.description}
                                            {$desc = $sArticle.image.description|escape}
                                        {/if}
                                        {if $srcSetRetina}<source sizes="(min-width: 48em) {$itemSize}, 100vw" srcset="{$srcSetRetina}" media="(min-resolution: 192dpi)" />{/if}
                                        {if $srcSet}<source sizes="(min-width: 48em) {$itemSize}, 100vw" srcset="{$srcSet}" />{/if}
                                        <img src="{$baseSource}" alt="{$desc}" title="{$desc|truncate:25:""}" />
                                    {/block}
                                {/block}
                            {/block}
                        </a>
                    {/block}

                    <div class="price">

                        {* Product description *}
                        {block name='frontend_listing_box_article_description'}
                            {if $sArticle.sBlockPrices}
                                {$lowestPrice=false}
                                {$highestPrice=false}
                                {foreach $sArticle.sBlockPrices as $blockPrice}
                                    {if $lowestPrice === false || $blockPrice.price < $lowestPrice}
                                        {$lowestPrice=$blockPrice.price}
                                    {/if}
                                    {if $highestPrice === false || $blockPrice.price > $highestPrice}
                                        {$highestPrice=$blockPrice.price}
                                    {/if}
                                {/foreach}

                                <meta itemprop="lowPrice" content="{$lowestPrice}" />
                                <meta itemprop="highPrice" content="{$highestPrice}" />
                                <meta itemprop="offerCount" content="{$sArticle.sBlockPrices|count}" />
                            {else}
                                <meta itemprop="priceCurrency" content="{$Shop->getCurrency()->getCurrency()}"/>
                            {/if}
                            {include file="frontend/detail/data-accessory.tpl" sArticle=$sArticle sView=1}
                        {/block}

                        <a href="{$sArticle.linkDetails|rewrite:$sArticle.articleName}"
                           title="{$sArticle.articleName|escapeHtml}">
                            {s name="DetailBuyActionAdd"}In den Warenkorb{/s}
                        </a>

                        {* Product actions - Compare product, more information, buy now *}
                       {block name='frontend_listing_box_article_actions'}
                            {include file="frontend/listing/product-box/product-actions-accessory.tpl" productBoxLayout="accessory"}
                        {/block}

                    </div>
                </div>
            </div>

            {* Product image *}
            {block name='frontend_listing_box_article_picture'}
            {/block}

            {* Customer rating for the product *}
            {block name='frontend_listing_box_article_rating'}
            {/block}

            {block name='frontend_listing_box_article_price_info'}
            {/block}

        {/block}
    {/block}
{/block}
