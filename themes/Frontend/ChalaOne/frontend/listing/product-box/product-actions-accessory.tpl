{namespace name="frontend/listing/box_article"}

{* Product actions *}
{block name='frontend_listing_box_article_actions_content'}

        {* Compare button *}
        {block name='frontend_listing_box_article_actions_compare'}

        {/block}

        {* Note button *}
        {block name='frontend_listing_box_article_actions_save'}
        {/block}

        {* @deprecated: block no longer in use *}
        {block name='frontend_listing_box_article_actions_more'}{/block}

        {* @deprecated: misleading name *}
        {block name="frontend_listing_box_article_actions_inline"}{/block}

{/block}
