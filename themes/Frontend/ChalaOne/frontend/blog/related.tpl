{* Blog Filter Button *}
{block name='frontend_blog_listing_filter_button'}
{/block}

{if $sBlogArticles}
	
	{foreach from=$sBlogArticles item=sArticle key=key name="counter"}
		{if ! $sArticle.attribute.attribute2 && !$sArticle.attribute.attribute3}
			<div class="row story_item">
				{include file="frontend/blog/blog_box.tpl" sArticle=$sArticle key=$key}		
			</div>
		{/if} 
	{/foreach}
{/if}