{extends file='frontend/index/index.tpl'}

{* Modify the breadcrumb *}
{block name='frontend_index_breadcrumb_inner'}
{/block}

{* Main content *}
{block name='frontend_index_content_main'}
	<section class="section stories narrow" id="leading_story">
		<div class="container">

			{* Breadcrumb *}
			{block name='frontend_index_breadcrumb'}
			{/block}

			{* Blog Sidebar *}
			{block name='frontend_blog_listing_sidebar'}
			{/block}

			{* Blog Banner *}
			{block name='frontend_blog_index_banner'}
			{/block}

			{* Blog listing *}
			{block name='frontend_blog_index_listing'}
				{include file="frontend/blog/blog_listing.tpl"}
			{/block}
		</div>
	</section>
	<section class="section video_and_support">
		<div class="container">
		</div>
	</section>
	{include file="frontend/blog/related_articles.tpl" relatedBlog=$relatedBlogCategory layout="video_and_support" box_style="item"}
{/block}