{extends file='frontend/index/index.tpl'}

{block name='frontend_index_header'}
    {include file='frontend/blog/header.tpl'}
{/block}

{* Main content *}
{block name='frontend_index_content_main'}


    <section id="story_top">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    {foreach $sMainCategories as $category}
                        {if $sCategoryCurrent == $category.id}
                            <a href="{$category.link}">Zurück zur Übersicht</a>
                        {/if}
                    {/foreach}
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    {* Article pictures *}
                    {block name='frontend_blog_col_article_picture'}
                        {if $sArticle.media}
                            <img srcset="{$sArticle.media[0].source}"
                                 alt="{$sArticle.title|escape}"
                                 title="{$sArticle.title|escape|truncate:25:""}" />
                        {/if}
                    {/block}
                </div>
            </div>
        </div>
    </section>
    <section id="story_content">
        <div class="container">
            <div class="row">
                {* Breadcrumb *}
                {block name='frontend_index_breadcrumb'}
                {/block}
                {* Content *}
                {block name='frontend_blog_detail_content'}

                        <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 left">
                            <div class="wrapper">
                                {* Article name *}
                                {block name='frontend_blog_detail_title'}
                                    <h1 itemprop="name">{$sArticle.title}</h1>
                                {/block}

                                <p class="user_and_date">
                                    {* Author *}
                                    {block name='frontend_blog_detail_author'}
                                    {/block}

                                    {* Date *}
                                    {block name='frontend_blog_detail_date'}{$sArticle.displayDate|date:"DATETIME_SHORT"} {s name="BlogInfoFrom"}{/s} {$sArticle.author.name}
                                    {/block}
                                </p>

                                {* Description *}
                                {block name='frontend_blog_col_meta_data_description'}
                                    {if $sArticle.description}
                                        {if $sArticle.linkCategory}
                                            <a href="{$sArticle.linkCategory}" title="{$sArticle.description|escape}">{$sArticle.description}</a>
                                        {else}
                                            {$sArticle.description}
                                        {/if}
                                    {/if}
                                {/block}

                                {* Tags *}
                                {block name='frontend_blog_detail_tags'}
                                    <div class="blog--detail-tags block">
                                        {if $sArticle.tags}

                                            {$tags=''}
                                            {foreach $sArticle.tags as $tag}
                                                {$tags="{$tags}{$tag.name}{if !$tag@last},{/if}"}
                                            {/foreach}
                                            <meta itemprop="keywords" content="{$tags}">
                                            <p class="tags">
                                                <span class="is--bold">{s name="BlogInfoTags"}{/s}:</span>
                                                {foreach $sArticle.tags as $tag}
                                                    <a href="{url controller=blog sCategory=$sArticle.categoryId sFilterTags=$tag.name}" title="{$tag.name|escape}">{$tag.name}</a>{if !$tag@last}, {/if}
                                                {/foreach}
                                            </p>
                                        {/if}
                                    </div>

                                    {* Bookmarks *}
                                    {block name='frontend_blog_detail_bookmarks'}
                                    {/block}

                                {/block}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 right">
                            {if $sArticle.sRelatedArticles}
                                {foreach $sArticle.sRelatedArticles as $sRelatedArticle}
                                    <div class="article">
                                        {include file="frontend/listing/box_article.tpl"
                                        sArticle=$sRelatedArticle productBoxLayout="blog"
                                        customer=$sArticle.author.name
                                        country=$sArticle.attribute.attribute4
                                        }
                                    </div>

                                   {* <div class="item model {$sRelatedArticle.item_style}">
                                        <a href="{$sRelatedArticle.linkDetails}"
                                           class="blog--picture-main"
                                           title="{$sRelatedArticle.articleName|escape}">
                                            {if isset($sRelatedArticle.image.thumbnails)}
											{$image = $sRelatedArticle.articleID|articleMainImage}
                                                <img src="/{$image.thumbnails.370x370}"
                                                     alt="{$sRelatedArticle.articleName|escape}"
                                                     title="{$sRelatedArticle.articleName|escape|truncate:25:""}" />
                                            {else}
                                                <img src="{link file='frontend/_public/src/img/no-picture.jpg'}"
                                                     alt="{$sRelatedArticle.articleName|escape}"
                                                     title="{$sRelatedArticle.articleName|escape|truncate:25:""}" />
                                            {/if}
                                        </a>
                                        <p>
                                            {$sArticle.author.name} war in {$sArticle.attribute.attribute4}
                                             mit den {$sRelatedArticle.articleName} unterwegs.
                                        </p>
                                    </div>*}
                                {/foreach}
                            {/if}

                            {* Blog listing *}
                            {block name='frontend_blog_index_listing'}
                                {include file="frontend/blog/related.tpl"}
                            {/block}
                                <h3>{s name="RelatedStories"}Ähnliche Geschichten{/s}</h3>

                                {foreach $sArticle.sRelatedBlogArticles as $sRelatedArticle}
                                    {if $sRelatedArticle.id}
                                        <div class="story_item">
                                            <div class="wrapper">
                                                <div class="text">
                                                    <h4><a href="{url controller=blog action=detail sCategory=$sRelatedArticle.categoryId blogArticle=$sRelatedArticle.id}"
                                                           class="blog--picture-main"
                                                           title="{$sRelatedArticle.title|escape}">{$sRelatedArticle.title}</a></h4>
                                                    <p class="date">{$sRelatedArticle.displayDate|date:"DATETIME_SHORT"}
                                                        {s name="BlogInfoFrom"}{/s} {$sRelatedArticle.authorName}
                                                    </p>
                                                </div>
                                                {* Article pictures *}
                                                {block name='frontend_blog_col_article_picture'}
                                                    {if $sRelatedArticle.imagePath}
                                                        <a href="{url controller=blog action=detail sCategory=$sRelatedArticle.categoryId blogArticle=$sRelatedArticle.id}"
                                                           class="blog--picture-main"
                                                           title="{$sRelatedArticle.title|escape}">
                                                                <img src="/{$sRelatedArticle.imagePath}"
                                                                     alt="{$sRelatedArticle.title|escape}"
                                                                     title="{$sRelatedArticle.title|escape|truncate:25:""}" />
                                                        </a>
                                                    {/if}
                                                {/block}
                                            </div>
                                        </div>
                                    {/if}
                                {/foreach}

                                {foreach $sMainCategories as $category}
                                    {if $sCategoryCurrent == $category.id}
                                        <a href="{$category.link}" class="more">{s name="ShowMore"}Storiesweitere Stories anzeigen{/s}</a>
                                    {/if}
                                {/foreach}

                                {* Headline *}
                                {block name='frontend_blog_detail_crossselling_headline'}
                                {/block}

                                {* Listing *}
                                {block name='frontend_blog_detail_crossselling_listing'}
                                {/block}

                            </div>
                        </div>

                        {* Rich snippets *}
                        {block name='frontend_blog_detail_rich_snippets'}
                        {/block}

                        {* Detail Box Header *}
                        {block name='frontend_blog_detail_box_header'}
                        {/block}

                        {* Detail Box Content *}
                        {block name='frontend_blog_detail_box_content'}
                        {/block}
                {/block}

                {* Comments *}
                {block name='frontend_blog_detail_comments'}
                {/block}

                {* Cross selling *}
                {block name='frontend_blog_detail_crossselling'}
                {/block}
            </div>
    </section>
	{include file="frontend/blog/related_articles.tpl" relatedBlog=$relatedBlogCategory layout="video_and_support" box_style="item"}
    <!-- start: Articles -->
	{*
    <section class="section video_and_support">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 item video">
                    <div class="wrapper">
                        <div class="text">
                            <h3>{$sArticle.attribute.leftArticleTitle}</h3>
                        </div>
                        <div class="image">
                            <img src="{$sArticle.attribute.rightArticleImage|picture}" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 item support">
                    <div class="wrapper">
                        <div class="text">
                            <h3>{$sArticle.attribute.rightArticleTitle}</h3>
                        </div>
                        <div class="image">
                            <img src="{$sArticle.attribute.leftArticleImage|picture}" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
	*}
    <!-- end: Articles -->
{/block}