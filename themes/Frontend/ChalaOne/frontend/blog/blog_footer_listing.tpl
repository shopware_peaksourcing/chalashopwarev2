{* Blog Filter Button *}
{block name='frontend_blog_listing_filter_button'}
{/block}

{if $sBlogArticles}
		{foreach from=$sBlogArticles item=sArticle key=key name="counter"}
			<div class="row">
				{if ($sArticle.attribute.attribute3) && ($smarty.foreach.counter.first ==1)}
					<!-- start: Video Item -->
					{include file="frontend/blog/blog_box.tpl" sArticle=$sArticle key=$key}	
					<!-- end: Video Item -->
				{/if}
				{if ($sArticle.attribute.attribute3) && ($key == 2)}
					<!-- start: Support Item -->
					{include file="frontend/blog/blog_box.tpl" sArticle=$sArticle key=$key}	
					<!-- end: Support Item -->
				{/if}	
			</div>		
		{/foreach}
	

	{* Paging *}
	{block name="frontend_listing_bottom_paging"}
		{if $sNumberPages > 1}
			{include file='frontend/blog/listing_actions.tpl'}
		{/if}
	{/block}
{/if}