{block name='frontend_blog_col_blog_entry'}
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 image">
        {* Blog Box *}
        {block name='frontend_blog_col_box_content'}
            {* Article pictures *}
            {block name='frontend_blog_col_article_picture'}
                {if $sArticle.media}
                    {if isset($sArticle.media.thumbnails)}
                        <img srcset="{$sArticle.media.thumbnails[1].sourceSet}"
                             alt="{$sArticle.title|escape}"
                             title="{$sArticle.title|escape|truncate:25:""}"/>
                    {else}
                        <img src="{link file='frontend/_public/src/img/no-picture.jpg'}"
                             alt="{$sArticle.title|escape}"
                             title="{$sArticle.title|escape|truncate:25:""}"/>
                    {/if}
                {/if}
            {/block}

            {* Article Description *}
            {block name='frontend_blog_col_description'}
            {/block}
        {/block}
    </div>
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 text">
        {* Blog Header *}
        {block name='frontend_blog-col_box_header'}
            {* Article name *}
            {block name='frontend_blog_col_article_name'}
                <h2>
                    <a href="{url controller=blog action=detail sCategory=$sArticle.categoryId blogArticle=$sArticle.id}"
                       title="{$sArticle.title|escape}">{$sArticle.title}</a>
                </h2>
            {/block}

            {* Meta data *}
            {block name='frontend_blog_col_meta_data'}
                {* Author *}
                {block name='frontend_blog_col_meta_data_name'}
                {/block}

                {* Date *}
                {block name='frontend_blog_col_meta_data_date'}
                    {if $sArticle.displayDate}
                        <p class="date">{$sArticle.displayDate|date:"DATETIME_SHORT"} von {$sArticle.author.name}</p>
                    {/if}
                {/block}

                {* Description *}
                {block name='frontend_blog_col_meta_data_description'}
                    {if $sArticle.categoryInfo.description}
                        <span class="blog--metadata-description is--nowrap">
							{if $sArticle.categoryInfo.linkCategory}
                                <a href="{$sArticle.categoryInfo.linkCategory}"
                                   title="{$sArticle.categoryInfo.description|escape}">{$sArticle.categoryInfo.description}</a>
                            {else}
                                {$sArticle.categoryInfo.description}
                            {/if}
						</span>
                    {else}
                        {$sArticle.description|unescape:'html'}
                    {/if}

                {/block}
                {if isset($sArticle.categoryId) && isset($sArticle.id)}
                    <a href="{url controller=blog action=detail sCategory=$sArticle.categoryId blogArticle=$sArticle.id}"
                       title="{$sArticle.title|escape}">{s name='HomeBlogLink' namespace='frontend/home/index'}Gesamte Story lesen{/s}</a>
                {/if}
                {* Comments *}
                {block name='frontend_blog_col_meta_data_comments'}
                {/block}

                {* Rating *}
                {block name='frontend_blog_col_meta_data_rating'}
                {/block}
            {/block}
        {/block}
    </div>
{/block}