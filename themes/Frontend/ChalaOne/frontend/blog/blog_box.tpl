{block name='frontend_blog_col_blog_entry'}
	<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 story_item">
		<div class="wrapper">
			{* Blog Header *}
			{block name='frontend_blog-col_box_header'}
				<div class="text">
					{* Article name *}
					{block name='frontend_blog_col_article_name'}
						<h2>
							<a href="{url controller=blog action=detail sCategory=$sArticle.categoryId blogArticle=$sArticle.id}" title="{$sArticle.title|escape}">{$sArticle.title}</a>
						</h2>
					{/block}

					{* Meta data *}
					{block name='frontend_blog_col_meta_data'}
						{* Author *}
						{block name='frontend_blog_col_meta_data_name'}
						{/block}

						{* Date *}
						{block name='frontend_blog_col_meta_data_date'}
							{if $sArticle.displayDate}
								<p class="date">{$sArticle.displayDate|date_format} von {$sArticle.author.name}
								</p>
							{/if}
						{/block}

						{* Description *}
						{block name='frontend_blog_col_meta_data_description'}
						{/block}

						{* Comments *}
						{block name='frontend_blog_col_meta_data_comments'}
						{/block}

						{* Rating *}
						{block name='frontend_blog_col_meta_data_rating'}
						{/block}
					{/block}
				</div>
			{/block}

			{* Blog Box *}
			{block name='frontend_blog_col_box_content'}
				{* Article pictures *}
				{block name='frontend_blog_col_article_picture'}
					{if $sArticle.media}
						<a href="{url controller=blog action=detail sCategory=$sArticle.categoryId blogArticle=$sArticle.id}"
						   class="blog--picture-main"
						   title="{$sArticle.title|escape}">
							{if isset($sArticle.media.thumbnails)}
								<img srcset="{if $sArticle.media.thumbnails[5]}
								{$sArticle.media.thumbnails[5].sourceSet}
								{else}{$sArticle.media.thumbnails[1].sourceSet}{/if}"
									 alt="{$sArticle.title|escape}"
									 title="{$sArticle.title|escape|truncate:25:""}" />
							{else}
								<img src="{link file='frontend/_public/src/img/no-picture.jpg'}"
									 alt="{$sArticle.title|escape}"
									 title="{$sArticle.title|escape|truncate:25:""}" />
							{/if}
						</a>
					{/if}
				{/block}

				{* Article Description *}
				{block name='frontend_blog_col_description'}
				{/block}
			{/block}
		</div>
	</div>
{/block}