{if isset($relatedBlog)}	
	<section class="section {$layout}">
		<div class="container">
			<div class="row">
				{foreach $relatedBlog as $key => $blog name=blg}
					{if $smarty.foreach.blg.iteration % 3 == 0}
						<div class="hidden-xs hidden-sm col-md-12 col-lg-12 {$blog.icon} inner h_list">
							<a href="{url controller=blog action=detail sCategory=$blog.categoryId blogArticle=$blog.id}" title="{$blog.title|escape}">
								<div class="wrapper text">
									{if $blog.double_images[0]}<img src="{$blog.double_images[0]|picture}" class="left">{/if}
									{if $blog.double_images[1]}<img src="{$blog.double_images[1]|picture}" class="right">{/if}
									{if $blog.title}
										<a href="{url controller=blog action=detail sCategory=$blog.categoryId blogArticle=$blog.id}" title="{$blog.title|escape}">
											<h3>{$blog.title}</h3>
										</a>
									{/if}
									{if $blog.description}
										{$blog.description|unescape:'html'}
									{/if}
								</div>
							</a>
						</div>
					{else}
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 {if $blog.use_video_link}video{/if} {$box_style} {$blog.icon}">
							{if $blog.use_video_link}
								<div class="wrapper">
									<div class="text">
										<h3>{$blog.title}</h3>
									</div>
									<div class="image">
										<img src="{$blog.image.600x600}">	
										<a class="various fancybox.iframe"
										   href="{$blog.use_video_link}"
										   title="{$blog.title|escape}" class="various fancybox.iframe">
										</a>
									</div>
								</div>
							{else}
							<a class="various fancybox.iframe"
							   href="{url controller=blog action=detail sCategory=$blog.categoryId blogArticle=$blog.id}"
							   title="{$blog.title|escape}" class="various fancybox.iframe">
								<div class="wrapper">
									<div class="text">
										<h3>{$blog.title}</h3>
									</div>
									<div class="image">
										<img src="{$blog.image.600x600}">	
									</div>
								</div>
							</a>
							{/if}
						</div>
					{/if}
				{/foreach}
			</div>
		</div>
	</section>
{/if}