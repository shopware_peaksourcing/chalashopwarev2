{* Blog Filter Button *}
{block name='frontend_blog_listing_filter_button'}
{/block}


<div class="row">
	<!-- start: Video Item -->
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 item video">
		<div class="wrapper">
			{if $sCategoryContent.attribute.left_article_title}
				<div class="text">
					<h3>{$sCategoryContent.attribute.left_article_title}</h3>
				</div>
			{/if}
			{if $sCategoryContent.attribute.left_article_image}
				<div class="image">
					<img src="{$sCategoryContent.attribute.left_article_image|picture}" />
					{if $sCategoryContent.attribute.left_article_link}<a href="{$sCategoryContent.attribute.left_article_link}" class="various fancybox.iframe"></a>{/if}
				</div>
			{/if}
		</div>
	</div>
	<!-- end: Video Item -->
	<!-- start: Support Item -->
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 item support">
		<div class="wrapper">
			{if $sCategoryContent.attribute.right_article_title}
				<div class="text">
					<h3>{$sCategoryContent.attribute.right_article_title}</h3>
				</div>
			{/if}
			{if $sCategoryContent.attribute.right_article_image}
				<div class="image">
					<img src="{$sCategoryContent.attribute.right_article_image|picture}" />
					{if $sCategoryContent.attribute.right_article_link}<a href="{$sCategoryContent.attribute.right_article_link}" class="various fancybox.iframe"></a>{/if}
				</div>
			{/if}
		</div>
	</div>
	<!-- end: Support Item -->
</div>