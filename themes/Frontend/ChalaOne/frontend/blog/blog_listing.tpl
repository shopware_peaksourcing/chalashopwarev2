{if $sCategoryContent.cmsheadline || $sCategoryContent.cmstext}
	{include file="frontend/listing/text.tpl"}
{/if}

{* Blog Filter Button *}
{block name='frontend_blog_listing_filter_button'}
{/block}

{if $sBlogArticles}
	<div class="row h_list stories">
		{foreach from=$sBlogArticles item=sArticle key=key name="counter"}
			{if $sArticle.attribute.attribute2 && !$sArticle.attribute.attribute3}
				{include file="frontend/blog/top_blog_box.tpl" sArticle=$sArticle key=$key}
				{break}
			{/if}
		{/foreach}
	</div>

	<div class="row stories_list">
		{foreach from=$sBlogArticles item=sArticle key=key name="counter"}
			{if ! $sArticle.attribute.attribute2 && !$sArticle.attribute.attribute3}
				{include file="frontend/blog/blog_box.tpl" sArticle=$sArticle key=$key}
			{/if}
		{/foreach}
	</div>

	{* Paging *}
	{block name="frontend_listing_bottom_paging"}
		{if $sNumberPages > 1}
			{include file='frontend/blog/listing_actions.tpl'}
		{/if}
	{/block}
{/if}
