{extends file="frontend/index/index.tpl"}


{* Custom header *}
{block name='frontend_index_header'}
    {include file="frontend/index/header.tpl"}
{/block}


{* Back to the shop button *}
{block name='frontend_index_logo_trusted_shops' append}
    {if $theme.checkoutHeader}
        <a href="{url controller='index'}"
           class="btn is--small btn--back-top-shop is--icon-left"
           title="{"{s name='FinishButtonBackToShop' namespace='frontend/checkout/finish'}{/s}"|escape}">
            <i class="icon--arrow-left"></i>
            {s name="FinishButtonBackToShop" namespace="frontend/checkout/finish"}{/s}
        </a>
    {/if}
{/block}

	
{* Main content *}
{block name="frontend_index_content"}

	{include file="frontend/register/steps.tpl" sStepActive="paymentShipping"}

    <div class="content content--confirm product--table" data-ajax-shipping-payment="true">
        {include file="frontend/checkout/shipping_payment_core.tpl"}
    </div>
{/block}