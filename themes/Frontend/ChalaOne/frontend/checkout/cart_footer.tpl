{block name='frontend_checkout_cart_footer_element'}
    <div class="entries-border "></div>
    <div class="entries-article-summe col-md-12 nopadding hidden-xs">
        <div class="col-xs-6 col-sm-6 col-md-6 hidden-xs nopadding-left">
            {* Add product using a voucher *}
            {block name='frontend_checkout_cart_footer_add_voucher'}
                <form method="post"
                      action="{url action='addVoucher' sTargetAction=$sTargetAction}"
                      class="entries-forma col-xs-12 col-sm-12 col-md-12 nopadding-left">

                    {block name='frontend_checkout_cart_footer_add_voucher_field'}
                        <input type="text"
                               class="form-entries col-xs-7 col-ms-7 col-md-7" name="sVoucher"
                               placeholder="{"{s name='CheckoutFooterAddVoucherLabelInline'}{/s}"|escape}"/>
                    {/block}

                    {block name='frontend_checkout_cart_footer_add_voucher_button'}
                        <button type="submit"
                                class="col-xs-5 col-ms-5 col-md-5">
                            Einlösen
                        </button>
                    {/block}
                </form>
            {/block}
        </div>

        {block name='frontend_checkout_cart_footer_field_labels'}
            <div class="total-price col-xs-4 col-sm-4 col-md-3 nopadding float-right">
                <ul class="col-xs-12 col-sm-12 col-md-12 nopadding-right">

                    {* Basket sum *}
                    {block name='frontend_checkout_cart_footer_field_labels_sum'}

                        {block name='frontend_checkout_cart_footer_field_labels_sum_label'}
                            <li class="col-xs-7 col-sm-7 col-md-7 text-left nopadding-left">
                                <p>{s name="CartFooterLabelSum"}{/s}</p>
                            </li>
                        {/block}

                        {block name='frontend_checkout_cart_footer_field_labels_sum_value'}
                            <li class="price col-xs-5 col-sm-5 col-md-5 text-right">
                                <span>{$sDispatch.amount|currency}</span>
                            </li>
                        {/block}
                    {/block}
                </ul>

                {$voucherAmong = 0}
                {foreach $sBasket.content as $cartItem}
                    {* modus = 2 -> voucher *}
                    {if {$cartItem.modus = 2}}
                        {$voucherAmong = $voucherAmong + $cartItem.amount}
                    {/if}
                {/foreach}
                <ul class="col-xs-12 col-sm-12 col-md-12 nopadding-right">
                    <li class="col-xs-7 col-sm-7 col-md-7 text-left nopadding-left">
                        <p>Gutschein</p>
                    </li>
                    <li class="price col-xs-5 col-sm-5 col-md-5 text-right">
                        <span>{$voucherAmong|currency}</span>
                    </li>
                </ul>
            </div>
            <div class="entries-border full-width margin-bottom-20"></div>
            {* Total sum *}
            {block name='frontend_checkout_cart_footer_field_labels_total'}
                <div class="entries-article-total col-xs-12 col-sm-12 col-md-12 nopadding">
                <div class="total-price col-xs-4 col-sm-4 col-md-3 nopadding float-right">
                    <ul class="col-xs-12 col-sm-12 col-md-12 nopadding-right nomargin">

                        {block name='frontend_checkout_cart_footer_field_labels_total_label'}
                            <li class="col-xs-7 col-sm-7 col-md-7 text-left nopadding">
                                <p>{s name="CartFooterLabelTotal"}{/s}</p>
                            </li>
                        {/block}

                        {block name='frontend_checkout_cart_footer_field_labels_total_value'}
                            <li class="col-xs-5 col-sm-5 col-md-5 text-right">
                                <span>{$sBasket.Amount|currency}</span>
                            </li>
                        {/block}
                    </ul>
                </div>
            {/block}
            </div>
        {/block}
    </div>
    <div class="visible-xs">
        <div class="entries-borde visible-xs"></div>
        <div class="entries-article-summe col-xs-12 col-sm-12 visible-xs">
            <div class="col-xs-12 col-sm-12 nopadding-left nopadding-right">
                {* Add product using a voucher *}
                {block name='frontend_checkout_cart_footer_add_voucher'}
                    <form class="entries-forma col-xs-12 col-sm-12 col-md-12 nopadding-left nopadding-right">
                        {block name='frontend_checkout_cart_footer_add_voucher_field'}
                            <input type="text" value="Gutscheinnummer" class="form-entries col-xs-7 col-ms-7 col-md-7"
                                   name="sVoucher"
                                   placeholder="{"{s name='CheckoutFooterAddVoucherLabelInline'}{/s}"|escape}"/>
                        {/block}
                        {block name='frontend_checkout_cart_footer_add_voucher_button'}
                            <input type="submit" value="Einlösen" class="col-xs-5 col-ms-5 col-md-5">
                        {/block}
                    </form>
                {/block}
            </div>
        </div>
        <div class="entries-border-top"></div>
        <div class="total-price col-xs-12 col-sm-12 nopadding-right float-right visible-xs">
            {* Basket sum *}
            {block name='frontend_checkout_cart_footer_field_labels_sum'}
                <ul class="col-xs-12 col-sm-12 col-md-12 nopadding">
                    {block name='frontend_checkout_cart_footer_field_labels_sum_label'}
                        <li class="col-xs-7 col-sm-7 col-md-7 text-left nopadding-left">
                            <p>{s name="CartFooterLabelSum"}{/s}</p>
                        </li>
                    {/block}
                    {block name='frontend_checkout_cart_footer_field_labels_sum_value'}
                        <li class="price col-xs-5 col-sm-5 col-md-5 text-right">
                            <span>{$sDispatch.amount|currency}</span>
                        </li>
                    {/block}
                </ul>
                <ul class="col-xs-12 col-sm-12 col-md-12 nopadding">
                    <li class="col-xs-7 col-sm-7 col-md-7 text-left nopadding-left">
                        <p>Gutschein</p>
                    </li>
                    <li class="price col-xs-5 col-sm-5 col-md-5 text-right">
                        <span>{$voucherAmong|currency}</span>
                    </li>
                </ul>
            {/block}


            <ul class="col-xs-12 col-sm-12 col-md-12 nopadding">
                {* Total sum *}
                {block name='frontend_checkout_cart_footer_field_labels_total'}
                    <li class="col-xs-7 col-sm-7 col-md-7 text-left nopadding">
                        <p>{s name="CartFooterLabelTotal"}{/s}</p>
                    </li>
                {/block}
                {block name='frontend_checkout_cart_footer_field_labels_total_value'}
                    <li class="price col-xs-5 col-sm-5 col-md-5 text-right">
                        <span>{$sBasket.Amount|currency}</span>
                    </li>
                {/block}
            </ul>

        </div>
    </div>
{/block}
