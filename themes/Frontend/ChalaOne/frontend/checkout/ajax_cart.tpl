{block name='frontend_checkout_ajax_cart'}
    <div class="ajax--cart">
        <ul class="drop">
            <li>
                <p class="close_label">{if $sBasket.Quantity}{$sBasket.Quantity}{else}0{/if} {s name="AjaxCartArticlesInCart"}{/s}
                    <a href="#close-categories-menu" class="close close-cart close--off-canvas"></a>
                </p>

                {block name='frontend_checkout_ajax_cart_buttons_offcanvas'}
                    <div class="buttons--off-canvas hidden">
                        {block name='frontend_checkout_ajax_cart_buttons_offcanvas_inner'}
                            <a href="#close-categories-menu" class="close--off-canvas">
                                <i class="icon--arrow-left"></i>
                                {s name="AjaxCartContinueShopping"}{/s}
                            </a>
                        {/block}
                    </div>
                {/block}

                {block name='frontend_checkout_ajax_cart_alert_box'}
                    {if $basketInfoMessage}
                        <div class="alert is--info is--rounded is--hidden">
                            <div class="alert--icon">
                                <div class="icon--element icon--info"></div>
                            </div>
                            <div class="alert--content">{$basketInfoMessage}</div>
                        </div>
                    {else}
                        <div class="alert is--success is--rounded is--hidden">
                            <div class="alert--icon">
                                <div class="icon--element icon--check"></div>
                            </div>
                            <div class="alert--content">{s name="AjaxCartSuccessText" namespace="frontend/checkout/ajax_cart"}{/s}</div>
                        </div>
                    {/if}
                {/block}

                {block name='frontend_checkout_ajax_cart_item_container'}
                    <div class="items">
                        {block name='frontend_checkout_ajax_cart_item_container_inner'}
                            {if $sBasket.content}
                                {foreach $sBasket.content as $sBasketItem}
                                    {block name='frontend_checkout_ajax_cart_row'}
                                        <p>{$sBasketItem.articlename} <span>{$sBasketItem.amount|currency}</span></p>
                                    {/block}
                                {/foreach}
                            {else}
                                {block name='frontend_checkout_ajax_cart_empty'}
                                    <div class="cart--item is--empty">
                                        {block name='frontend_checkout_ajax_cart_empty_inner'}
                                            <span class="cart--empty-text">{s name='AjaxCartInfoEmpty'}{/s}</span>
                                        {/block}
                                    </div>
                                {/block}
                            {/if}
                        {/block}
                    </div>
                {/block}

                {block name='frontend_checkout_ajax_cart_prices_container'}
                    {if $sBasket.content}
                        {block name='frontend_checkout_ajax_cart_prices_container_inner'}
                            <p class="total">{s name='Total'}Gesamt{/s}<span>{$sBasket.Amount|currency}</span></p>
                        {/block}
                    {/if}
                {/block}

                {* Basket link *}
                {block name='frontend_checkout_ajax_cart_button_container'}
                    {block name='frontend_checkout_ajax_cart_button_container_inner'}
                        {if $sBasket}
                            {block name='frontend_checkout_ajax_cart_open_basket'}
                                {if $sBasket.Quantity}
                                    <a href="{url controller='checkout' action='cart'}" class="btn button--open-basket is--icon-right to_checkout" title="{"{s name='AjaxCartLinkBasket'}{/s}"|escape}">
                                        {s name='AjaxCartLinkBasket'}{/s}
                                    </a>
                                {else}
                                    <span class="btn no_product">{s name='AjaxCartLinkBasketNoArticles'}{/s}</span>
                                {/if}
                            {/block}
                        {/if}
                    {/block}
                {/block}
            </li>
        </ul>
    </div>
{/block}
