{namespace name="frontend/checkout/cart_item"}

<div class="table--tr block-group row--product{if $isLast} is--last-row{/if}">
    <div class="article-1 col-sm-12 col-md-12 nopadding nomargin hidden-xs">
        {if $sBasketItem.additional_details.sConfigurator}
            {$detailLink={url controller=detail sArticle=$sBasketItem.articleID number=$sBasketItem.ordernumber forceSecure}}
        {else}
            {$detailLink=$sBasketItem.linkDetails}
        {/if}

        {* Product information column *}
        {block name='frontend_checkout_cart_item_name'}
            {*<div class="column--product">*}
            <ul class="col-sm-5 col-md-6 nopadding nomargin">

                {* Product image *}
                {block name='frontend_checkout_cart_item_image'}
                    {block name="frontend_checkout_cart_item_image_container"}
                        {block name="frontend_checkout_cart_item_image_container_outer"}
                            <li class="col-xs-4 col-sm-2 col-md-2 nopadding">
                                {block name="frontend_checkout_cart_item_image_container_inner"}

                                    {$image = $sBasketItem.articleID|articleMainImage}
                                    {$desc = $sBasketItem.articlename|escape}

                                    {if $image.thumbnails.140x140}
                                        <a href="{$detailLink}" title="{$sBasketItem.articlename|strip_tags}"
                                           class="table--media-link"
                                                {if {config name=detailmodal} && {controllerAction|lower} === 'confirm'}
                                            data-modalbox="true"
                                            data-content="{url controller="detail" action="productQuickView" ordernumber="{$sBasketItem.ordernumber}" fullPath forceSecure}"
                                            data-mode="ajax"
                                            data-width="750"
                                            data-sizing="content"
                                            data-title="{$sBasketItem.articlename|strip_tags|escape}"
                                            data-updateImages="true"
                                                {/if}>

                                            {if $image.description}
                                                {$desc = $image.description|escape}
                                            {/if}

                                            <img src="{$image.thumbnails.140x140}" alt="{$desc}"
                                                 title="{$desc|truncate:160}" style="width:50px;"/>
                                        </a>
                                    {else}
                                        <img src="{link file='frontend/_public/src/img/no-picture.jpg'}" alt="{$desc}"
                                             title="{$desc|truncate:160}"/>
                                    {/if}
                                {/block}
                            </li>
                        {/block}
                    {/block}
                {/block}

                {* Product information *}
                {block name='frontend_checkout_cart_item_details'}
                    <li class="col-xs-8 col-sm-10 col-md-6 nopadding">
                        {* Product name *}
                        {block name='frontend_checkout_cart_item_details_title'}
                            <p class="navbar-text nomargin-left"><a class="content--title" href="{$detailLink}"
                                                                    title="{$sBasketItem.articlename|strip_tags|escape}"
                                        {if {config name=detailmodal} && {controllerAction|lower} === 'confirm'}
                                    data-modalbox="true"
                                    data-content="{url controller="detail" action="productQuickView" ordernumber="{$sBasketItem.ordernumber}" fullPath forceSecure}"
                                    data-mode="ajax"
                                    data-width="750"
                                    data-sizing="content"
                                    data-title="{$sBasketItem.articlename|strip_tags|escape}"
                                    data-updateImages="true"
                                        {/if}>
                                    {$sBasketItem.articlename|strip_tags|truncate:60}
                                </a></p>
                        {/block}
                    </li>
                {/block}
            </ul>
        {/block}

        {* Remove product from basket *}
        {block name='frontend_checkout_cart_item_delete_article'}
            <ul class="col-xs-12 col-sm-7 col-md-6 nopadding-left hidden-xs">
                <li class="col-xs-3 col-sm-3 col-md-3 col-lg-4 nopadding">
                    <form action="{url action='deleteArticle' sDelete=$sBasketItem.id sTargetAction=$sTargetAction}"
                          method="post">
                        <button type="submit"
                                class="btn is--small column--actions-link media-right col-xs-12  col-sm-12  col-md-12 col-lg-12 nopadding"
                                title="{"{s name='CartItemLinkDelete'}{/s}"|escape}">
                            Artikel entfernen
                        </button>
                    </form>
                </li>
            </ul>
        {/block}

        {* Product quantity *}
        {block name='frontend_checkout_cart_item_quantity'}

            {* Label *}
            {block name='frontend_checkout_cart_item_quantity_label'}
                <div class="column--label quantity--label">
                    {s name="CartColumnQuantity" namespace="frontend/checkout/cart_header"}{/s}
                </div>
            {/block}
            <li class="col-sm-3 col-md-3 col-lg-3 nopadding">
                <p class="navbar-text mobile-float-right media-right col-sm-12 col-md-12 col-lg-12">
                    {block name='frontend_checkout_cart_item_quantity_selection'}
                        {if !$sBasketItem.additional_details.laststock || ($sBasketItem.additional_details.laststock && $sBasketItem.additional_details.instock > 0)}
                            {$sBasketItem.quantity}
                        {else}
                            {s name="CartColumnQuantityEmpty" namespace="frontend/checkout/cart_item"}{/s}
                        {/if}
                    {/block}
                </p>
            </li>
        {/block}

        {* Product unit price *}
        {block name='frontend_checkout_cart_item_price'}
            {if !$sBasketItem.modus}
                {block name='frontend_checkout_cart_item_unit_price_label'}
                    <div class="column--label unit-price--label">
                        {s name="CartColumnPrice" namespace="frontend/checkout/cart_header"}{/s}
                    </div>
                {/block}
                <li class="price col-sm-3 col-md-3 col-lg-3 nopadding 3" style="right: 15px;">
                    <p class="navbar-text media-right col-sm-12 col-md-12 col-lg-12">{$sBasketItem.price|currency}{block name='frontend_checkout_cart_tax_symbol'}{s name="Star" namespace="frontend/listing/box_article"}{/s}{/block}
                    <p>
                </li>
            {/if}
        {/block}

        {* Product tax rate *}
        {block name='frontend_checkout_cart_item_tax_price'}{/block}

        {* Accumulated product price *}
        {block name='frontend_checkout_cart_item_total_sum'}
            {block name='frontend_checkout_cart_item_total_price_label'}
                <div class="column--label total-price--label">
                    {s name="CartColumnTotal" namespace="frontend/checkout/cart_header"}{/s}
                </div>
            {/block}
            <li class="price col-sm-3 col-md-3 col-lg-2 nopadding 1">
                <p class="navbar-text media-right col-sm-12 col-md-12 col-lg-12">{$sBasketItem.amount|currency}
                    {block name='frontend_checkout_cart_tax_symbol'}{s name="Star" namespace="frontend/listing/box_article"}{/s}{/block}
                <p>
            </li>
            </ul>
        {/block}
    </div>
</div>

<div class="entries-border visible-xs"></div>
<div class="article-1 col-xs-12 col-sm-12 col-md-12 nopadding visible-xs">

    <div class="col-xs-2 col-sm-2 nopadding">
        {block name="frontend_checkout_cart_item_image_container_inner"}

            {$image = $sBasketItem.articleID|articleMainImage}
            {$desc = $sBasketItem.articlename|escape}

            {if $image.thumbnails.140x140}
                <a href="{$detailLink}" title="{$sBasketItem.articlename|strip_tags}" class="table--media-link"
                        {if {config name=detailmodal} && {controllerAction|lower} === 'confirm'}
                    data-modalbox="true"
                    data-content="{url controller="detail" action="productQuickView" ordernumber="{$sBasketItem.ordernumber}" fullPath forceSecure}"
                    data-mode="ajax"
                    data-width="750"
                    data-sizing="content"
                    data-title="{$sBasketItem.articlename|strip_tags|escape}"
                    data-updateImages="true"
                        {/if}>

                    {if $image.description}
                        {$desc = $image.description|escape}
                    {/if}

                    <img src="{$image.thumbnails.140x140}" alt="{$desc}" title="{$desc|truncate:160}"
                         style="width:50px;"/>
                </a>
            {else}
                <img src="{link file='frontend/_public/src/img/no-picture.jpg'}" alt="{$desc}"
                     title="{$desc|truncate:160}"/>
            {/if}
        {/block}
    </div>
    <div class="col-xs-10 col-sm-10 nopadding">
        <div class="col-xs-12 col-sm-12 nopadding margin-bottom-10">
            <ul>
                <li class="col-xs-9 nopadding">
                    {* Product name *}
                    {block name='frontend_checkout_cart_item_details_title'}
                        <a class="content--title" href="{$detailLink}"
                           title="{$sBasketItem.articlename|strip_tags|escape}"
                                {if {config name=detailmodal} && {controllerAction|lower} === 'confirm'}
                            data-modalbox="true"
                            data-content="{url controller="detail" action="productQuickView" ordernumber="{$sBasketItem.ordernumber}" fullPath forceSecure}"
                            data-mode="ajax"
                            data-width="750"
                            data-sizing="content"
                            data-title="{$sBasketItem.articlename|strip_tags|escape}"
                            data-updateImages="true"
                                {/if}>
                            {$sBasketItem.articlename|strip_tags|truncate:60}
                        </a>
                    {/block}
                </li>

                <li class="col-xs-3 col-sm-3 nopadding">
                    {* Remove product from basket *}
                    {block name='frontend_checkout_cart_item_delete_article'}
                        <a class="button navbar-text media-right col-xs-12  col-sm-12 nopadding"
                           href="popup_thumbnail.html">X</a>
                    {/block}
                </li>
        </div>
        <div class="col-xs-12 nopadding">
            <ul class="nomargin col-xs-12 nopadding">
                {* Product quantity *}
                {block name='frontend_checkout_cart_header_quantity'}
                    <li class="col-xs-4 nopadding">Anzahl</li>
                {/block}
                {* Unit price *}
                {block name='frontend_checkout_cart_header_price'}
                    <li class="col-xs-4 nopadding text-align-right">{s name='CartColumnPrice'}{/s}</li>
                {/block}
                {* Accumulated product price *}
                {block name='frontend_checkout_cart_header_total'}
                    <li class="col-xs-4 nopadding text-align-right">{s name="CartColumnTotal"}{/s}</li>
                {/block}
            </ul>
            <ul class="nomargin col-xs-12 nopadding">
                <li class="col-xs-4 nopadding">
                    {block name='frontend_checkout_cart_item_quantity_selection'}
                        {if !$sBasketItem.additional_details.laststock || ($sBasketItem.additional_details.laststock && $sBasketItem.additional_details.instock > 0)}
                            <form name="basket_change_quantity212" method="post"
                                  action="{url action='changeQuantity' sTargetAction=$sTargetAction}">
                                <input name="sQuantity" data-auto-submit="true" type="text"
                                       placeholder="{if !$sBasketItem.additional_details.laststock || ($sBasketItem.additional_details.laststock && $sBasketItem.additional_details.instock > 0)}{$sBasketItem.quantity}{else}{s name="CartColumnQuantityEmpty" namespace="frontend/checkout/cart_item"}{/s}{/if}"
                                       class="navbar-text  media-right col-xs-12 nopadding float-left">
                                <input type="hidden" name="sArticle" value="212">
                                <input type="hidden" name="__csrf_token" value="NHW7MwIqcMGDKo5LaTWcsE0HFuKSJn">
                            </form>
                        {else}
                            {s name="CartColumnQuantityEmpty" namespace="frontend/checkout/cart_item"}{/s}
                        {/if}
                    {/block}
                </li>
                <li class="col-xs-4 nopadding text-align-right">
                    <p>{$sBasketItem.price|currency}{block name='frontend_checkout_cart_tax_symbol'}{s name="Star" namespace="frontend/listing/box_article"}{/s}{/block}</p>
                </li>
                <li class="col-xs-4 nopadding text-align-right">
                    <p>{$sBasketItem.amount|currency}{block name='frontend_checkout_cart_tax_symbol'}{s name="Star" namespace="frontend/listing/box_article"}{/s}{/block}</p>
                </li>
            </ul>
        </div>
    </div>

</div>
