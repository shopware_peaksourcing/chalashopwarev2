<div class="entries-article-head col-xs-12 col-sm-12 col-md-12 nopadding hidden-xs">
	{block name='frontend_checkout_cart_header_field_labels'}

		{* Product name and information *}
		{block name='frontend_checkout_cart_header_name'}
			<div class="entries-left col-xs-6 col-sm-6 col-md-7 nopadding">
				<span>{s name="CartColumnName"}{/s}</span>
			</div>
		{/block}

		{* Action column *}
		{block name='frontend_checkout_cart_header_actions'}
		{/block}
		<div class="entries-right col-xs-6 col-sm-6 col-md-5 nopadding-left hidden-xs">
			<ul class="col-xs-12 col-sm-12 col-md-12 col-lg-9 nopadding text-right float-right">
				{* Product quantity *}
				{block name='frontend_checkout_cart_header_quantity'}
					<li class="col-xs-4 col-sm-4 col-md-4 col-lg-4  nopadding">{s name="CartColumnQuantity"}{/s}</li>
				{/block}

				{* Unit price *}
				{block name='frontend_checkout_cart_header_price'}
					<li class="col-xs-4 col-sm-4 col-md-4 col-lg-4 nopadding">{s name='CartColumnPrice'}{/s}</li>
				{/block}

				{* Product tax rate *}
				{block name='frontend_checkout_cart_header_tax'}{/block}

				{* Accumulated product price *}
				{block name='frontend_checkout_cart_header_total'}
					<li class="col-xs-4 col-sm-4 col-md-4 col-lg-4 nopadding">{s name="CartColumnTotal"}{/s}</li>
				{/block}
			</ul>
		</div>
	{/block}
</div>
<div class="entries-border hidden-xs"></div>

<div class="entries-article-head col-xs-12 col-sm-12 col-md-12 visible-xs">
	{block name='frontend_checkout_cart_header_field_labels'}
		<div class="entries-left col-xs-6 col-sm-6 col-md-6 nopadding">
			<span>{if $sBasket.Quantity}{$sBasket.Quantity}{else}0{/if} {s name="CartColumnName"}{/s}</span>

		</div>
	{/block}
</div>
