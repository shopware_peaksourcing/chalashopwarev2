{block name='frontend_checkout_cart_footer_element'}
    <div class="entries-border"></div>
    <div class="entries-article-summe col-md-12 nopadding">
        {*<div class="basket--footer">*}
        {block name='frontend_checkout_cart_footer_field_labels'}
            {*<ul class="aggregation--list">*}
            <div class="total-price col-xs-4 col-sm-4 col-md-3 nopadding float-right">
                <ul class="col-xs-12 col-sm-12 col-md-12">

                    {* Basket sum *}
                    {block name='frontend_checkout_cart_footer_field_labels_sum'}
                        {*<li class="list--entry block-group entry--sum">*}

                        {block name='frontend_checkout_cart_footer_field_labels_sum_label'}
                            {*<div class="entry--label block">*}
                            <li class="col-xs-7 col-sm-7 col-md-7 text-left nopadding-left">
                                <p>{s name="CartFooterLabelSum"}{/s}</p>
                            </li>
                            {*</div>*}
                        {/block}

                        {block name='frontend_checkout_cart_footer_field_labels_sum_value'}
                            {*<div class="entry--value block">*}
                            <li class="price col-xs-5 col-sm-5 col-md-5 text-right">
                                <span>{$sDispatch.amount|currency}</span>
                            </li>
                            {*</div>*}
                        {/block}
                        {*</li>*}
                    {/block}
                </ul>

                {$voucherAmong = 0}
                {foreach $sBasket.content as $cartItem}
                    {* modus = 2 -> voucher *}
                    {if {$cartItem.modus = 2}}
                        {$voucherAmong = $voucherAmong + $cartItem.amount}
                    {/if}
                {/foreach}
                <ul class="col-xs-12 col-sm-12 col-md-12">
                    {*<li class="list--entry block-group entry--voucher">*}
                    <li class="col-xs-7 col-sm-7 col-md-7 text-left nopadding-left">
                        {*<div class="entry--label block">*}
                        <p>Gutschein</p>
                        {*</div>*}
                    </li>
                    <li class="price col-xs-5 col-sm-5 col-md-5 text-right">
                        {*<div class="entry--value block is--no-star">*}
                        <span>{$voucherAmong|currency}</span>
                        {*</div>*}
                    </li>
                    {*</li>*}
                </ul>
            </div>
            <div class="entries-border full-width margin-bottom-20"></div>
            {* Total sum *}
            {block name='frontend_checkout_cart_footer_field_labels_total'}
                <div class="entries-article-total col-xs-12 col-sm-12 col-md-12">
                <div class="total-price col-xs-4 col-sm-4 col-md-3 nopadding float-right">
                    <ul class="col-xs-12 col-sm-12 col-md-12 nopadding">
                        {*<li class="list--entry block-group entry--total">*}

                        {block name='frontend_checkout_cart_footer_field_labels_total_label'}
                            <li class="col-xs-7 col-sm-7 col-md-7 text-left nopadding">
                                {*<div class="entry--label block">*}
                                <p>{s name="CartFooterLabelTotal"}{/s}</p>
                                {*</div>*}
                            </li>
                        {/block}

                        {block name='frontend_checkout_cart_footer_field_labels_total_value'}
                            <li class="col-xs-5 col-sm-5 col-md-5 text-right">
                                {*<div class="entry--value block is--no-star">*}
                                <span>{$sBasket.Amount|currency}</span>
                                {*</div>*}
                            </li>
                        {/block}
                        {*</li>*}
                    </ul>
                </div>
            {/block}
            </div>
            {*</ul>*}
        {/block}

        {*</div>*}
    </div>
{/block}
