{extends file="frontend/index/index.tpl"}

{* Back to the shop button *}
{block name='frontend_index_logo_trusted_shops' append}
    {if $theme.checkoutHeader}
        <a href="{url controller='index'}"
           class="btn is--small btn--back-top-shop is--icon-left"
           title="{"{s name='FinishButtonBackToShop' namespace='frontend/checkout/finish'}{/s}"|escape}"
           xmlns="http://www.w3.org/1999/html">
            <i class="icon--arrow-left"></i>
            {s name="FinishButtonBackToShop" namespace="frontend/checkout/finish"}{/s}
        </a>
    {/if}
{/block}


{* Main content *}
{block name='frontend_index_content'}
    {include file="frontend/register/steps.tpl" sStepActive="finished"}
    <div class="container content confirm--content">

    {block name='frontend_checkout_confirm_product_table'}
        <div class="product--table{if {config name=BasketShippingInfo}} has--dispatch-info{/if} entries col-xs-12 col-sm-12 col-md-12">

            {* Deliveryfree dispatch notification *}
            {block name='frontend_checkout_cart_deliveryfree'}
                {if $sShippingcostsDifference}
                    {$shippingDifferenceContent="<strong>{s name='CartInfoFreeShipping'}{/s}</strong> {s name='CartInfoFreeShippingDifference'}{/s}"}
                    {include file="frontend/_includes/messages.tpl" type="warning" content="{$shippingDifferenceContent}"}
                {/if}
            {/block}

            {* Error messages *}
            {block name='frontend_checkout_cart_error_messages'}
                {include file="frontend/checkout/error_messages.tpl"}
            {/block}

            {* Product table content *}
            {block name='frontend_checkout_cart_panel'}
                <div class="panel has--border">
                    <div class="panel--body is--rounded">

                        {* Product table header *}
                        {block name='frontend_checkout_cart_cart_head'}
                            {include file="frontend/checkout/cart_header.tpl"}
                        {/block}

                        {* Basket items *}
                        {foreach $sBasket.content as $sBasketItem}
                            {block name='frontend_checkout_cart_item'}
                                <div class="entries-article-body col-xs-12 col-sm-12 col-md-12">
                                    {include file='frontend/checkout/cart_item.tpl' isLast=$sBasketItem@last}
                                </div>
                            {/block}
                        {/foreach}

                        {* Product table footer *}
                        {block name='frontend_checkout_cart_cart_footer'}
                            {include file="frontend/checkout/cart_footer_confirm.tpl"}
                        {/block}
                    </div>
                </div>
            {/block}

            {* Premium products *}
            {block name='frontend_checkout_cart_premium'}
                {if $sPremiums}
                    {* Actual listing *}
                    {block name='frontend_checkout_cart_premium_products'}
                        {include file='frontend/checkout/premiums.tpl'}
                    {/block}
                {/if}
            {/block}

        </div>
    {/block}

    {* Error messages *}
    {block name='frontend_checkout_confirm_error_messages'}
        {include file="frontend/checkout/error_messages.tpl"}
    {/block}

    {block name='frontend_checkout_confirm_form'}
    <form class="entries padding-top-bottom-30 col-xs-12 col-sm-12 col-md-12" method="post"
          action="{if $sPayment.embediframe || $sPayment.action}{url action='payment'}{else}{url action='finish'}{/if}">

        {block name='frontend_checkout_confirm_information_wrapper'}
            <div class="" data-panel-auto-resizer="true">

            {block name='frontend_checkout_confirm_information_addresses'}

                {if $activeBillingAddressId == $activeShippingAddressId}

                    {* Equal Billing & Shipping *}
                    {block name='frontend_checkout_confirm_information_addresses_equal'}
                        <div class="entries-container border-right-smaller col-xs-12 col-sm-12 col-md-4">

                        {block name='frontend_checkout_confirm_information_addresses_equal_panel'}

                            {block name='frontend_checkout_confirm_information_addresses_equal_panel_title'}
                                <h4 class="float-left">Rechnungsadresse</h4>
                            {/block}
                            {block name='frontend_checkout_confirm_information_addresses_equal_panel_shipping_change_address'}
                                <a href="{url controller=address action=edit id=$activeBillingAddressId sTarget=checkout sTargetAction=confirm}"
                                   class="andern top-12">
                                    {s name="ConfirmLinkChangePayment" namespace="frontend/checkout/confirm"}{/s}
                                </a>
                            {/block}

                            {block name='frontend_checkout_confirm_information_addresses_equal_panel_body'}

                                {block name='frontend_checkout_confirm_information_addresses_equal_panel_billing'}
                                    <div class="rechnungsadresse col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-top-15 billing--panel">
                                    {if $sUserData.billingaddress.company}
                                        <p>
                                            <span class="address--company is--bold">{$sUserData.billingaddress.company}</span>{if $sUserData.billingaddress.department}
                                        <br/>
                                            <span class="address--department is--bold">{$sUserData.billingaddress.department}</span>{/if}
                                        </p>
                                    {/if}
                                    <p>
                                        <span class="address--salutation">{$sUserData.billingaddress.salutation|salutation}</span>
                                        {if {config name="displayprofiletitle"}}
                                            <span class="address--title">{$sUserData.billingaddress.title}</span>
                                            <br/>
                                        {/if}
                                        <span class="address--firstname">{$sUserData.billingaddress.firstname}</span>
                                        <span class="address--lastname">{$sUserData.billingaddress.lastname}</span>
                                    </p>
                                    <p>
                                        <span class="address--street">{$sUserData.billingaddress.street}</span>
                                    </p>
                                    {if $sUserData.billingaddress.additional_address_line1}<p><span
                                                class="address--additional-one">{$sUserData.billingaddress.additional_address_line1}</span>
                                        </p>{/if}
                                    {if $sUserData.billingaddress.additional_address_line2}<p><span
                                                class="address--additional-two">{$sUserData.billingaddress.additional_address_line2}</span>
                                        </p>{/if}
                                    {if {config name=showZipBeforeCity}}
                                        <p><span class="address--zipcode">{$sUserData.billingaddress.zipcode}</span>
                                            <span class="address--city">{$sUserData.billingaddress.city}</span></p>
                                    {else}
                                        <p><span class="address--city">{$sUserData.billingaddress.city}</span> <span
                                                    class="address--zipcode">{$sUserData.billingaddress.zipcode}</span>
                                        </p>
                                    {/if}
                                    {if $sUserData.additional.state.name}<p><span
                                                class="address--statename">{$sUserData.additional.state.name}</span>
                                        </p>{/if}
                                    <p>
                                        <span class="address--countryname">{$sUserData.additional.country.countryname}</span>
                                    </p>
                                    {block name="frontend_checkout_confirm_information_addresses_equal_panel_billing_invalid_data"}
                                        {if $invalidBillingAddress}
                                            {include file='frontend/_includes/messages.tpl' type="warning" content="{s name='ConfirmAddressInvalidAddress'}{/s}"}
                                        {else}
                                            {block name="frontend_checkout_confirm_information_addresses_equal_panel_billing_set_as_default"}
                                                {if $activeBillingAddressId != $sUserData.additional.user.default_billing_address_id || $activeShippingAddressId != $sUserData.additional.user.default_shipping_address_id}
                                                    <div class="set-default">
                                                        <input type="checkbox" name="setAsDefaultAddress" id="set_as_default" value="1" /> <label for="set_as_default">{s name="ConfirmUseForFutureOrders"}{/s}</label>
                                                    </div>
                                                {/if}
                                            {/block}
                                        {/if}
                                    {/block}
                                    </div>
                                {/block}

                                {block name='frontend_checkout_confirm_information_addresses_equal_panel_shipping'}
                                    <div class="lieferadresse col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <h4 class="float-left">Lieferadresse</h4>
                                        {block name='frontend_checkout_confirm_information_addresses_equal_panel_shipping_select_address'}
                                            <a href="{url controller=address}"
                                                 class="andern top-12"
                                                 data-address-selection="true"
                                                 data-sessionKey="checkoutShippingAddressId"
                                                 data-id="{$activeShippingAddressId}"
                                                 title="{s name="ConfirmAddressChooseDifferentShippingAddress"}{/s}">
                                                  {s name="ConfirmLinkChangePayment" namespace="frontend/checkout/confirm"}{/s}
                            </a>
                            {/block}
                            <div class="rechnungsadresse col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-top-15">
                            <p>wie Rechnungsadresse</p>
                            </div>
                            {/block}

                            {/block}
                        {/block}
                    </div>
                    {/block}

                {else}

                    {* Separate Billing & Shipping *}
                    {block name='frontend_checkout_confirm_information_addresses_billing'}
                    <div class="information--panel-item information--panel-item-billing">
                        {* Billing address *}
                        {block name='frontend_checkout_confirm_information_addresses_billing_panel'}
                        <div class="panel has--border block information--panel billing--panel">

                            {* Headline *}
                            {block name='frontend_checkout_confirm_information_addresses_billing_panel_title'}
                            <div class="panel--title is--underline">
                                {s name="ConfirmHeaderBilling" namespace="frontend/checkout/confirm"}{/s}
                            </div>
                            {/block}

                            {* Content *}
                            {block name='frontend_checkout_confirm_information_addresses_billing_panel_body'}
                            <div class="panel--body is--wide">
                                {if $sUserData.billingaddress.company}
                                <span class="address--company is--bold">{$sUserData.billingaddress.company}</span>{if $sUserData.billingaddress.department}<br /><span class="address--department is--bold">{$sUserData.billingaddress.department}</span>{/if}
                                <br />
                                {/if}
                            <span class="address--salutation">{$sUserData.billingaddress.salutation|salutation}</span>
                                {if {config name="displayprofiletitle"}}
                                <span class="address--title">{$sUserData.billingaddress.title}</span><br/>
                                {/if}
                            <span class="address--firstname">{$sUserData.billingaddress.firstname}</span> <span class="address--lastname">{$sUserData.billingaddress.lastname}</span><br />
                            <span class="address--street">{$sUserData.billingaddress.street}</span><br />
                                {if $sUserData.billingaddress.additional_address_line1}<span class="address--additional-one">{$sUserData.billingaddress.additional_address_line1}</span><br />{/if}
                                {if $sUserData.billingaddress.additional_address_line2}<span class="address--additional-two">{$sUserData.billingaddress.additional_address_line2}</span><br />{/if}
                                {if {config name=showZipBeforeCity}}
                                <span class="address--zipcode">{$sUserData.billingaddress.zipcode}</span> <span class="address--city">{$sUserData.billingaddress.city}</span>
                                {else}
                                <span class="address--city">{$sUserData.billingaddress.city}</span> <span class="address--zipcode">{$sUserData.billingaddress.zipcode}</span>
                                {/if}<br />
                                {if $sUserData.additional.state.name}<span class="address--statename">{$sUserData.additional.state.name}</span><br />{/if}
                            <span class="address--countryname">{$sUserData.additional.country.countryname}</span>

                                {block name="frontend_checkout_confirm_information_addresses_billing_panel_body_invalid_data"}
                                    {if $invalidBillingAddress}
                                        {include file='frontend/_includes/messages.tpl' type="warning" content="{s name='ConfirmAddressInvalidBillingAddress'}{/s}"}
                                                        {else}
                                                            {block name="frontend_checkout_confirm_information_addresses_billing_panel_body_set_as_default"}
                                                                {if $activeBillingAddressId != $sUserData.additional.user.default_billing_address_id}
                                                                    <div class="set-default">
                                                                        <input type="checkbox" name="setAsDefaultBillingAddress" id="set_as_default_billing" value="1" /> <label for="set_as_default_billing">{s name="ConfirmUseForFutureOrders"}{/s}</label>
                                                                    </div>
                                                                {/if}
                                                            {/block}
                                                        {/if}
                                                    {/block}
                                                </div>
                                            {/block}

                                            {* Action buttons *}
                                            {block name="frontend_checkout_confirm_information_addresses_billing_panel_actions"}
                                                <div class="panel--actions is--wide">
                                                    {block name="frontend_checkout_confirm_information_addresses_billing_panel_actions_change"}
                                                        <div class="address--actions-change">
                                                            {block name="frontend_checkout_confirm_information_addresses_billing_panel_actions_change_address"}
                                                                <a href="{url controller=address action=edit id=$activeBillingAddressId sTarget=checkout sTargetAction=confirm}"
                                                                   data-address-editor="true"
                                                                   data-sessionKey="checkoutBillingAddressId"
                                                                   data-id="{$activeBillingAddressId}"
                                                                   data-title="{s name="ConfirmAddressSelectButton"}Change address{/s}"
                                                                   title="{s name="ConfirmAddressSelectButton"}Change address{/s}"
                                                                   class="btn">
                                                                    {s name="ConfirmAddressSelectButton"}Change address{/s}
                        </a>
                        {/block}
                        </div>
                        {/block}
                            {block name="frontend_checkout_confirm_information_addresses_billing_panel_actions_select_address"}
                            <a href="{url controller=address}"
                                                           data-address-selection="true"
                                                           data-sessionKey="checkoutBillingAddressId"
                                                           data-id="{$activeBillingAddressId}"
                                                           title="{s name="ConfirmAddressSelectLink"}{/s}">
                                                            {s name="ConfirmAddressSelectLink"}{/s}
                            </a>
                            {/block}
                        </div>
                        {/block}
                        </div>
                        {/block}
                    </div>
                    {/block}

                    {block name='frontend_checkout_confirm_information_addresses_shipping'}
                    <div class="information--panel-item information--panel-item-shipping">
                        {block name='frontend_checkout_confirm_information_addresses_shipping_panel'}
                        <div class="panel has--border block information--panel shipping--panel">

                            {* Headline *}
                            {block name='frontend_checkout_confirm_information_addresses_shipping_panel_title'}
                            <div class="panel--title is--underline">
                                {s name="ConfirmHeaderShipping" namespace="frontend/checkout/confirm"}{/s}
                            </div>
                            {/block}

                            {* Content *}
                            {block name='frontend_checkout_confirm_information_addresses_shipping_panel_body'}
                            <div class="panel--body is--wide">
                                {if $sUserData.shippingaddress.company}
                                <span class="address--company is--bold">{$sUserData.shippingaddress.company}</span>{if $sUserData.shippingaddress.department}<br /><span class="address--department is--bold">{$sUserData.shippingaddress.department}</span>{/if}
                                <br />
                                {/if}

                            <span class="address--salutation">{$sUserData.shippingaddress.salutation|salutation}</span>
                                {if {config name="displayprofiletitle"}}
                                <span class="address--title">{$sUserData.shippingaddress.title}</span><br/>
                                {/if}
                            <span class="address--firstname">{$sUserData.shippingaddress.firstname}</span> <span class="address--lastname">{$sUserData.shippingaddress.lastname}</span><br/>
                            <span class="address--street">{$sUserData.shippingaddress.street}</span><br />
                                {if $sUserData.shippingaddress.additional_address_line1}<span class="address--additional-one">{$sUserData.shippingaddress.additional_address_line1}</span><br />{/if}
                                {if $sUserData.shippingaddress.additional_address_line2}<span class="address--additional-one">{$sUserData.shippingaddress.additional_address_line2}</span><br />{/if}
                                {if {config name=showZipBeforeCity}}
                                <span class="address--zipcode">{$sUserData.shippingaddress.zipcode}</span> <span class="address--city">{$sUserData.shippingaddress.city}</span>
                                {else}
                                <span class="address--city">{$sUserData.shippingaddress.city}</span> <span class="address--zipcode">{$sUserData.shippingaddress.zipcode}</span>
                                {/if}<br />
                                {if $sUserData.additional.stateShipping.name}<span class="address--statename">{$sUserData.additional.stateShipping.name}</span><br />{/if}
                            <span class="address--countryname">{$sUserData.additional.countryShipping.countryname}</span>

                                {block name="frontend_checkout_confirm_information_addresses_shipping_panel_body_invalid_data"}
                                    {if $invalidShippingAddress}
                                        {include file='frontend/_includes/messages.tpl' type="warning" content="{s name='ConfirmAddressInvalidShippingAddress'}{/s}"}
                                                        {else}
                                                            {block name="frontend_checkout_confirm_information_addresses_shipping_panel_body_set_as_default"}
                                                                {if $activeShippingAddressId != $sUserData.additional.user.default_shipping_address_id}
                                                                    <div class="set-default">
                                                                        <input type="checkbox" name="setAsDefaultShippingAddress" id="set_as_default_shipping" value="1" /> <label for="set_as_default_shipping">{s name="ConfirmUseForFutureOrders"}{/s}</label>
                                                                    </div>
                                                                {/if}
                                                            {/block}
                                                        {/if}
                                                    {/block}
                                                </div>
                                            {/block}

                                            {* Action buttons *}
                                            {block name="frontend_checkout_confirm_information_addresses_shipping_panel_actions"}
                                                <div class="panel--actions is--wide">
                                                    {block name="frontend_checkout_confirm_information_addresses_shipping_panel_actions_change"}
                                                        <div class="address--actions-change">
                                                            {block name="frontend_checkout_confirm_information_addresses_shipping_panel_actions_change_address"}
                                                                <a href="{url controller=address action=edit id=$activeShippingAddressId sTarget=checkout sTargetAction=confirm}"
                                                                   title="{s name="ConfirmAddressSelectButton"}Change address{/s}"
                                                                   data-title="{s name="ConfirmAddressSelectButton"}Change address{/s}"
                                                                   data-address-editor="true"
                                                                   data-id="{$activeShippingAddressId}"
                                                                   data-sessionKey="checkoutShippingAddressId"
                                                                   class="btn">
                                                                    {s name="ConfirmAddressSelectButton"}Change address{/s}
                        </a>
                        {/block}
                        </div>
                        {/block}
                            {block name="frontend_checkout_confirm_information_addresses_shipping_panel_actions_select_address"}
                            <a href="{url controller=address}"
                                                           data-address-selection="true"
                                                           data-sessionKey="checkoutShippingAddressId"
                                                           data-id="{$activeShippingAddressId}"
                                                           title="{s name="ConfirmAddressSelectLink"}{/s}">
                                                            {s name="ConfirmAddressSelectLink"}{/s}
                            </a>
                            {/block}
                        </div>
                        {/block}
                        </div>
                        {/block}
                    </div>
                    {/block}
                {/if}
            {/block}

            {* Payment method *}
            {block name='frontend_checkout_confirm_information_payment'}
            <div class="entries-container col-xs-12 col-sm-12 col-md-4 border-right-smaller">
                {block name='frontend_checkout_confirm_left_payment_method_headline'}
                <h4 class="float-left">{s name="ConfirmInfoPaymentMethod" namespace="frontend/checkout/confirm"}{/s}</h4>
                {/block}
                {block name='frontend_checkout_confirm_left_payment_method_actions'}
                <a href="{url controller=checkout action=shippingPayment sTarget=checkout}" class="andern top-12 top-12 btn--change-shipping">
                                    {s name="ConfirmLinkChangePayment" namespace="frontend/checkout/confirm"}{/s}
                                </a>
                            {/block}

                            {block name='frontend_checkout_confirm_left_payment_method'}
                              <div class="rechnungsadresse col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-top-15">
                                <p class="payment--method-info">
                                  <span class="payment--description">{$sUserData.additional.payment.description}</span>
                                </p>
                                {if !$sUserData.additional.payment.esdactive && {config name="showEsd"}}
                                <p class="payment--confirm-esd">{s name="ConfirmInfoInstantDownload" namespace="frontend/checkout/confirm"}{/s}</p>
                                {/if}
                              </div>
                            {/block}
                        </div>
                        <div class="entries-container border-right-white-smaller col-xs-12 col-sm-12 col-md-4">
                            <h4 class="float-left">{s name="ConfirmHeadDispatch" namespace="frontend/checkout/confirm"}{/s}</h4>
                            <a href="{url controller=checkout action=shippingPayment sTarget=checkout}" class="andern top-12 btn--change-payment">
                                {s name="ConfirmLinkChangePayment" namespace="frontend/checkout/confirm"}{/s}
                            </a>

                            {block name='frontend_checkout_confirm_left_shipping_method'}
                              <div class="rechnungsadresse col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-top-15">
                                <p class="shipping--method-info">
                                  <span class="shipping--description" title="{$sDispatch.name}">{$sDispatch.name|truncate:25:"...":true}</span>
                                </p>
                              </div>
                            {/block}
                        </div>
                    {/block}
                </div>
            {/block}

        </form>

        <form class="payment-form entries padding-top-bottom-20 col-xs-12 col-sm-12 col-md-12" id="confirm--form" method="post" action="{if $sPayment.embediframe || $sPayment.action}{url action='payment'}{else}{url action='finish'}{/if}">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="entries-container bottom-0 col-xs-2 col-sm-2 col-md-2 col-lg-2 nopadding float-left">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <a href="{url controller=checkout action=cart}" class="bottom-button-white col-xs-12 col-sm-12 col-md-12 col-lg-12">Zurück</a>
                    </div>
                </div>
                <div class="entries-container col-xs-10 col-sm-10 col-md-10 col-lg-10 float-right">
					{block name='frontend_checkout_confirm_agb'}
						{if !{config name='IgnoreAGB'}}
							<div class="element-center margin-bottom-15">
							  {* Terms of service checkbox *}
							  {block name='frontend_checkout_confirm_agb_checkbox'}
								<input type="checkbox" required="required" aria-required="true" id="sAGB" name="sAGB"{if $sAGBChecked} checked="checked"{/if} class="entries-checkbox" />
							    <label for="sAGB"{if $sAGBError} class="has--error"{/if} data-modalbox="true" data-targetSelector="a" data-mode="ajax" data-height="500" data-width="750">{s name="ConfirmTerms"}{/s}</label>
							  {/block}
							</div>
                            <div class="confirmation-label">
                              {* AGB label *}
                                 {block name='frontend_checkout_confirm_agb_label'}
                                    <p>
                                        {s name="IHave"}Ich habe die{/s}
                                        <a href="{url controller=custom sCustom=4 forceSecure}" title="{s name="AGB"}AGB{/s}">
                                            <span style='text-decoration:underline;'>{s name='AGB'}AGB{/s}</span>
    </a>
        {s name="AgreeWithShopAndValidity"}Ihres Shops gelesen und bin mit deren Geltung einverstanden.{/s}
    </p>
    {/block}
    </div>
    {/if}
    {/block}
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 nopadding">
        {block name='frontend_checkout_cart_table_actions_bottom'}
            {block name="frontend_checkout_actions_confirm_bottom"}
                {if $sLaststock.hideBasket}
                    {block name='frontend_checkout_confirm_stockinfo'}
                        {include file="frontend/_includes/messages.tpl" type="error" content="{s name='ConfirmErrorStock'}{/s}"}
                                      {/block}
                                  {elseif ($invalidBillingAddress || $invalidShippingAddress)}
                                      {block name='frontend_checkout_confirm_addressinfo'}
                                          {include file="frontend/_includes/messages.tpl" type="error" content="{s name='ConfirmErrorInvalidAddress'}{/s}"}
                                      {/block}
                                  {else}
                                      {block name='frontend_checkout_confirm_submit'}
                                          {* Submit order button *}
                                          {if $sPayment.embediframe || $sPayment.action}
                                              <button type="submit" class="bottom-button col-xs-12 col-sm-12 col-md-12 col-lg-12 float-right" form="confirm--form" data-preloader-button="true">
                                                {s name='ConfirmDoPayment'}{/s}
                                              </button>
                                          {else}
                                              <button type="submit" class="bottom-button col-xs-12 col-sm-12 col-md-12 col-lg-12 float-right" form="confirm--form" data-preloader-button="true">
                                                {s name='ConfirmActionSubmit'}{/s}
                                              </button>
                                          {/if}
                                      {/block}
                                  {/if}
                            {/block}
						{/block}
					</div>
				</div>
			</div>
		</form>
    {/block}

    {* Additional feature which can be enabled / disabled in the base configuration *}
    {if {config name=commentvoucherarticle}||{config name=bonussystem} && {config name=bonus_system_active} && {config name=displaySlider}}
        {block name="frontend_checkout_confirm_additional_features"}
            <div class="panel has--border additional--features">
                {block name="frontend_checkout_confirm_additional_features_headline"}
                    <div class="panel--title is--underline">
                        {s name="ConfirmHeadlineAdditionalOptions"}{/s}
                    </div>
                {/block}

                {block name="frontend_checkout_confirm_additional_features_content"}
                    <div class="panel--body is--wide block-group">

                        {* Additional feature - Add voucher *}
                        {block name="frontend_checkout_confirm_additional_features_add_voucher"}
                            <div class="feature--group block">
                                <div class="feature--voucher">
                                    <form method="post" action="{url action='addVoucher' sTargetAction=$sTargetAction}" class="table--add-voucher add-voucher--form">
                                        {block name='frontend_checkout_table_footer_left_add_voucher_agb'}
                                            {if !{config name='IgnoreAGB'}}
                                                <input type="hidden" class="agb-checkbox" name="sAGB"
                                                       value="{if $sAGBChecked}1{else}0{/if}"/>
                                            {/if}
                                        {/block}

                                        {block name='frontend_checkout_confirm_add_voucher_field'}
                                            <input type="text" class="add-voucher--field block" name="sVoucher" placeholder="{"{s name='CheckoutFooterAddVoucherLabelInline' namespace='frontend/checkout/cart_footer'}{/s}"|escape}" />
{/block}

    {block name='frontend_checkout_confirm_add_voucher_button'}
    <button type="submit" class="add-voucher--button btn is--primary is--small block">
    <i class="icon--arrow-right"></i>
    </button>
    {/block}
</form>
</div>


    {* Additional feature - Add product using the sku *}
    {block name="frontend_checkout_confirm_additional_features_add_product"}
    <div class="feature--add-product">
    <form method="post" action="{url action='addArticle' sTargetAction=$sTargetAction}" class="table--add-product add-product--form block-group">

                                            {block name='frontend_checkout_confirm_add_product_field'}
                                                <input name="sAdd" class="add-product--field block" type="text" placeholder="{s name='CheckoutFooterAddProductPlaceholder' namespace='frontend/checkout/cart_footer_left'}{/s}" />
                                            {/block}

                                            {block name='frontend_checkout_confirm_add_product_button'}
                                                <button type="submit" class="add-product--button btn is--primary is--small block">
                                                    <i class="icon--arrow-right"></i>
                                                </button>
                                            {/block}
                                        </form>
                                    </div>
                                {/block}
                            </div>
                        {/block}

                        {* Additional customer comment for the order *}
                        {block name='frontend_checkout_confirm_comment'}
                            <div class="feature--user-comment block">
                                <textarea class="user-comment--field" rows="5" cols="20" placeholder="{s name="ConfirmPlaceholderComment" namespace="frontend/checkout/confirm"}{/s}" data-pseudo-text="true" data-selector=".user-comment--hidden">{$sComment|escape}</textarea>
</div>
{/block}
</div>
{/block}
</div>
{/block}
{/if}

    {* Premiums articles *}
    {block name='frontend_checkout_confirm_premiums'}
        {if $sPremiums && {config name=premiumarticles}}
            {include file='frontend/checkout/premiums.tpl'}
        {/if}
    {/block}

</div>
{/block}
