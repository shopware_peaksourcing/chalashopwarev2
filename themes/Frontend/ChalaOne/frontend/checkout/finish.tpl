{extends file="frontend/index/index.tpl"}

{* Main content *}
{block name="frontend_index_content"}
	<section class="checkout_finish">

		{* Finish teaser message *}
		{block name='frontend_checkout_finish_teaser'}
			<div class="element-center col-xs-12 col-sm-12 col-md-6 col-lg-6">

				{block name='frontend_checkout_finish_teaser_title'}
					<h1 class="panel--title teaser--title is--align-center">{s name="FinishHeaderThankYou"}{/s}!</h1>
				{/block}

				{block name='frontend_checkout_finish_teaser_content'}
            {if $confirmMailDeliveryFailed}
                {include file="frontend/_includes/messages.tpl" type="error" content="{s name="FinishInfoConfirmationMailFailed"}{/s}"}
            {/if}

            <div class="text-align-center col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-bottom-30">
                <p>{s name="FinishInfoConfirmationMail"}{/s}</p>
        				<p>Marcel@Challer.de!</p>
      			</div>

            {block name='frontend_checkout_finish_items'}
              {$image = $sBasket.content.0.articleID|articleMainImage}
              <div class="element-center col-xs-8 col-sm-5 col-md-5 col-lg-5 margin-bottom-30">
                <img src="{$image.thumbnails.360x240}" alt="">
              </div>
        		{/block}

						{block name='frontend_checkout_finish_teaser_actions'}
              <div class="element-center col-xs-12 col-sm-5 col-md-5 col-lg-5">
          			<a class="bottom-button-transparent col-xs-12 col-sm-12 col-md-12 col-lg-12 float-right" href="{url controller='index'}">Zurück zur Startseite</a>
          		</div>
						{/block}
				{/block}
			</div>
		{/block}

	</section>
{/block}
