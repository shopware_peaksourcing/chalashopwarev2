{extends file="frontend/index/index.tpl"}

{* Title *}
{block name='frontend_index_header_title'}
    {s name="RegisterTitle"}{/s} | {{config name=shopName}|escapeHtml}
{/block}

{* Back to the shop button *}
{block name='frontend_index_logo_trusted_shops' append}
    {if $theme.checkoutHeader && $sTarget != "account"}
        <a href="{url controller='index'}"
           class="btn is--small btn--back-top-shop is--icon-left"
           title="{"{s name='FinishButtonBackToShop' namespace='frontend/checkout/finish'}{/s}"|escape}">
            <i class="icon--arrow-left"></i>
            {s name="FinishButtonBackToShop" namespace="frontend/checkout/finish"}{/s}
        </a>
    {/if}
{/block}


{* Register content *}
{block name='frontend_index_content'}

    {* Step box *}
    {include file="frontend/register/steps.tpl" sStepActive="address"}
    <section id="payment_steps" class="section">
        <div class="container">
            <div class="entries padding-top-bottom-30 col-xs-12 col-sm-12 col-md-12">
                <div class="row">
                    {block name='frontend_register_index_registration'}
                        <div class="entries-container border-right col-xs-12 col-sm-12 col-md-4 {if $errors.occurred} is--collapsed{/if}"
                             id="registration" data-register="true">

                            {block name='frontend_register_index_dealer_register'}
                                {* Included for compatibility reasons *}
                            {/block}

                            {block name='frontend_register_index_cgroup_header'}
                                {if $register.personal.sValidation}
                                    {* Include information related to registration for other customergroups then guest, this block get overridden by b2b essentials plugin *}
                                    <div class="panel register--supplier">
                                        <h2 class="panel--title is--underline">{$sShopname|escapeHtml} {s name='RegisterHeadlineSupplier' namespace='frontend/register/index'}{/s}</h2>

                                        <div class="panel--body is--wide">
                                            <p class="is--bold">{s name='RegisterInfoSupplier3' namespace='frontend/register/index'}{/s}</p>

                                            <h3 class="is--bold">{s name='RegisterInfoSupplier4' namespace='frontend/register/index'}{/s}</h3>
                                            <p>{s name='RegisterInfoSupplier5' namespace='frontend/register/index'}{/s}</p>

                                            <h3 class="is--bold">{s name='RegisterInfoSupplier6' namespace='frontend/register/index'}{/s}</h3>
                                            <p>{s name='RegisterInfoSupplier7' namespace='frontend/register/index'}{/s}</p>
                                        </div>
                                    </div>
                                {/if}
                            {/block}

                            {block name='frontend_register_index_form'}
                                <form method="post"
                                      action="{url action=saveRegister sTarget='account' sTargetAction='index'}"
                                      class="panel register--form payment-form">
                                    {block name='frontend_register_index_form_personal_fieldset'}
                                        {include file="frontend/register/error_message.tpl" error_messages=$errors.personal}
                                        {include file="frontend/register/personal_fieldset.tpl" form_data=$register.personal error_flags=$errors.personal}
                                    {/block}

                                    {block name='frontend_register_index_form_billing_fieldset'}
                                        {include file="frontend/register/error_message.tpl" error_messages=$errors.billing}
                                        {include file="frontend/register/billing_fieldset.tpl" form_data=$register.billing error_flags=$errors.billing country_list=$countryList}
                                    {/block}

                                    {block name='frontend_register_index_form_shipping_fieldset'}
                                        {include file="frontend/register/error_message.tpl" error_messages=$errors.shipping}
                                        {include file="frontend/register/shipping_fieldset.tpl" form_data=$register.shipping error_flags=$errors.shipping country_list=$countryList}
                                    {/block}

                                    {* Privacy checkbox *}
                                    {if !$update}
                                        {if {config name=ACTDPRCHECK}}
                                            {block name='frontend_register_index_input_privacy'}
                                                <div class="register--privacy">
                                                    <input name="register[personal][dpacheckbox]" type="checkbox"
                                                           id="dpacheckbox"{if $form_data.dpacheckbox} checked="checked"{/if}
                                                           required="required" aria-required="true" value="1"
                                                           class="chkbox is--required"/>
                                                    <label for="dpacheckbox"
                                                           class="chklabel{if isset($errors.personal.dpacheckbox)} has--error{/if}">{s name='RegisterLabelDataCheckbox'}{/s}</label>
                                                </div>
                                            {/block}
                                        {/if}
                                    {/if}

                                    {block name='frontend_register_index_form_required'}
                                        {* Required fields hint *}
  								        {s name='RegisterPersonalRequiredText' namespace='frontend/register/personal_fieldset'}{/s}
                                    {/block}

                                    {block name='frontend_register_index_form_submit'}
                                        {* Submit button *}
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 nopadding register--action">
                                            <button type="submit" class="head-button register--submit"
                                                    name="Submit">{s name="RegisterIndexNewActionSubmit"}{/s} {*<i class="icon--arrow-right"></i>*}</button>
                                        </div>
                                        {* Skip login *}
                                        {if !$update}
                                            {block name='frontend_register_personal_fieldset_skip_login'}
                                                {if ($showNoAccount || $form_data.accountmode) && !$sEsd && !$form_data.sValidation && !{config name=NoAccountDisable}}
                                                    <div class="register--check is--hidden">
                                                        <input type="checkbox"
                                                               value="1"
                                                               id="register_personal_skipLogin"
                                                               name="register[personal][accountmode]"
                                                               class="register--checkbox chkbox"
                                                               {if $form_data.accountmode || $accountmode}checked="checked" {/if}/>

                                                        <label for="register_personal_skipLogin"
                                                               class="chklabel is--bold">{s name='RegisterLabelNoAccount'}{/s}</label>
                                                    </div>
                                                {else}
                                                    <input type="hidden"
                                                           value="0"
                                                           id="register_personal_skipLogin"
                                                           name="register[personal][accountmode]"
                                                           class="register--checkbox chkbox"
                                                           {if $form_data.accountmode || $accountmode}checked="checked" {/if}/>
                                                {/if}
                                            {/block}
                                        {/if}

                                    {/block}
                                </form>
                                <span class="registrierung col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <a href="#"
                                       onclick="$('.register--check input').trigger('click'); return false;">OHNE Registrierung fortfahren</a>
                                </span>
                            {/block}
                        </div>
                    {/block}

                    {* Register Login *}
                    {block name='frontend_register_index_login'}
                        <div class="entries-container col-xs-12 col-sm-12 col-md-4 border-right">
                            {include file="frontend/register/login.tpl"}
                        </div>
                    {/block}

                    {* Register advantages *}
                    {block name='frontend_register_index_advantages'}
                        <div class="entries-container border-right-white col-xs-12 col-sm-12 col-md-4">
                            <h4>Ohne Registrierung fortfahren</h4>
                            <p>Auch ohne zu Registrieren kannst Du bei uns einkaufen. Gib einfach im nächsten Schritt
                                Deine Daten ein. Bei dieser Variante kannst Du später nicht mehr auf Deine Daten
                                zugreifen.</p>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 nopadding">
                                <button class="head-button" onclick="$('.register--check input').trigger('click');">
                                    OHNE Registrierung fortfahren
                                </button>
                            </div>
                            {block name='frontend_index_content_advantages_list'}
                            {/block}
                        </div>
                    {/block}

                </div>
            </div>
        </div>
    </section>
{/block}
