{* Step box *}
<section id="steps" class="section">
	<div class="button-head">
		<div class="entries-article-head col-xs-12 col-sm-12 col-md-12">
			<div class="entries-button col-xs-12 col-sm-12 col-md-9 col-lg-9 nopadding-left">
				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 nopadding margin-r50">
					<a class="head-button button-active">Warenkorb</a>
				</div>
				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 nopadding button-left margin-r50">
					<a class="head-button">Zahlung und Adresse</a>
				</div>
				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 nopadding button-left">
					<a class="head-button-white">Bestellungsüberblick</a>
				</div>
			</div>
		</div>
	</div>
</section>
