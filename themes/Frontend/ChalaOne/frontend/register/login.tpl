{namespace name="frontend/account/login"}

{* Error messages *}
{block name='frontend_register_login_error_messages'}
	{if $sErrorMessages}
		<div class="account--error">
			{include file="frontend/register/error_message.tpl" error_messages=$sErrorMessages}
		</div>
	{/if}
{/block}

{* New customer *}
{block name='frontend_register_login_newcustomer'}
	{*<div class="register--new-customer">
		<a href="#registration"
		   class="new-customer-btn btn is--icon-right"
		   id="new-customer-action"
		   data-collapseTarget="#registration">
			{s name="LoginLinkRegister2"}{/s}
		</a>
	</div>*}
{/block}

{* Existing customer *}
{block name='frontend_register_login_customer'}

	<h4>Login</h4>
	{block name='frontend_register_login_form'}
		{if $sValidation}
			{$url = {url controller=account action=login sTarget=$sTarget sTargetAction=$sTargetAction sValidation=$sValidation} }
		{else}
			{$url = {url controller=account action=login sTarget=$sTarget sTargetAction=$sTargetAction} }
		{/if}

		<form name="sLogin" method="post" action="{$url}" class="payment-form">
			{if $sTarget}<input name="sTarget" type="hidden" value="{$sTarget|escape}" />{/if}

			{block name='frontend_register_login_description'}
				<!-- <div class="register--login-description">{s name="LoginHeaderFields"}{/s}</div> -->
			{/block}

			{block name='frontend_register_login_input_email'}
				<div class="register--login-email">
					<label for="{s name="LoginPlaceholderMail"}{/s}">{s name="LoginPlaceholderMail"}{/s}</label>
					<input name="email" placeholder="{s name="LoginPlaceholderMail"}{/s}" type="email" tabindex="1" value="{$sFormData.email|escape}" id="email" class="register--login-field{if $sErrorFlag.email} has--error{/if}" required="required" aria-required="true" />
				</div>
			{/block}

			{block name='frontend_register_login_input_password'}
				<div class="register--login-password">
					<label for="{s name="LoginPlaceholderPassword"}{/s}">{s name="LoginPlaceholderPassword"}{/s}</label>
					<input name="password" placeholder="{s name="LoginPlaceholderPassword"}{/s}" type="password" tabindex="2" id="passwort" class="register--login-field{if $sErrorFlag.password} has--error{/if}" required="required" aria-required="true" />
				</div>
			{/block}

			{block name='frontend_register_login_input_lostpassword'}
				<div class="register--login-lostpassword">
					<a href="{url controller=account action=password}" title="{"{s name="LoginLinkLostPassword"}{/s}"|escape}">
						{s name="LoginLinkLostPassword"}{/s}
					</a>
				</div>
			{/block}

			{block name='frontend_register_login_input_form_submit'}
				<div class="register--login-action col-xs-12 col-sm-12 col-md-12 col-lg-12 nopadding">
					<button type="submit" class="head-button register--login-btn" name="Submit">{s name="LoginLinkLogon"}{/s} {*<i class="icon--arrow-right"></i>*}</button>
				</div>
			{/block}
		</form>
	{/block}

{/block}
