var screenWidth = $(window).width();
var scrolltop = 0;
var comparisonSlider = false;

$(document).ready(function () {
    // Split configuration to name and description
    if ($('#filters_1 .radio_wrapper').length > 0) {
        $('#filters_1 .radio_wrapper').each(function () {
            var data = $(this).find('.option--label span').text();
            if (data.indexOf('|') >= 0) {
                arr = data.split('|');
            }
            $(this).find('.option--label span').empty().html("<p class='name'>" + arr[0] + "</p><p class='description'>" + arr[1] + "</p>");

        });
    }

    if ($('.articles .inner .text').find("p:first").length > 0) {
        $('.articles .inner .text').find("p:first").css({'margin-top': '25px', 'display': 'block'});
    }

    // Tailor Made Trigger Box
    if ($('.tailor_made_info').length > 0) {
        $('.tailor_made_trigger').click(function (event) {
            $(this).addClass('active');
            $('.tailor_made_info').toggleClass('hidden')
            $(this).removeClass('active');
            event.preventDefault();
        });
    }

    // Toggle User Login / Register Panels
    $('#user_nav a.toggle').click(function (event) {
        $('.login_panel').toggleClass('active');
        event.preventDefault();
    });

    // Toggle User Login / Register Panels
    // $('#user_nav .login_panel .close').click(function (event) {
    //     $('.login_panel').removeClass('active');
    //     $('.login .drop').removeClass('active');
    //     event.preventDefault();
    // });


    // Mobile User And Cart Menues
    $('#user').click(function (event) {
        if (screenWidth < 993) {
            $('.login .drop').addClass('active');
            $('.login_panel').addClass('active');
        }
        event.preventDefault();
    });
    if (screenWidth < 993) {
        $('#cart').click(function (event) {
            $(this).trigger('mouseenter');
            $('.cart .drop').addClass('active');
            event.preventDefault();
        });
    }
    /*      $('.container--ajax-cart .close_label .close-cart').click(function (event) {
     $('.drop.active').removeClass('active');
     event.preventDefault();
     });  */

    // Sliders
    setTimeout(function () {
        $('.slider').slick({
            lazyLoad: 'progressive',
            dots: false,
            arrows: true
        });
    }, 500);

    // Mobile Comparison Slider
    if ($('#product_comparison .products').length > 0 && screenWidth < 800) {
        mobileComaprisonSlider();
    }
    $('#product_comparison .top_mobile_nav a.prev').click(function (event) {
        $('#product_comparison .products').slick('slickPrev');
        event.preventDefault();
    });
    $('#product_comparison .top_mobile_nav a.next').click(function (event) {
        $('#product_comparison .products').slick('slickNext');
        event.preventDefault();
    });
    $('#product_comparison .bottom_mobile_nav a').click(function (event) {
        event.preventDefault();
        slideIndex = $(this).index();
        $('#product_comparison .products').slick('slickGoTo', parseInt(slideIndex));
    });

    // Highlight Model in List
    $('.item').hover(function () {
        $(this).siblings('.item').addClass('inactive');
    }, function () {
        $(this).siblings('.item').removeClass('inactive');
    });

    $('#leading_story .blog-slider').slick({
        lazyLoad: 'progressive',
        dots: false,
        arrows: false
    });

    // Blog Slider Navigation
    $('#leading_story .prev, #leading_story .next').click(function (event) {
        event.preventDefault();
        $('.blog-slider').slick($(this).data('slick'));
        if (screenWidth > 992) {
            var rightLabel = $('#leading_story .blog-slider .slick-active').next().find('h3').text();
            var leftLabel = $('#leading_story .blog-slider .slick-active').prev().find('h3').text();
            $('#leading_story .prev span').text(leftLabel);
            $('#leading_story .next span').text(rightLabel);
        }
    });

    // Video Fancybox
    $(".various").fancybox({
        maxWidth: 800,
        maxHeight: 600,
        fitToView: false,
        width: '70%',
        height: '70%',
        autoSize: false,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none'
    });
    // Image Gallery
    $('.slideshow').click(function (event) {
        event.preventDefault();
        $(".fancy-slide").fancybox({
            autoPlay: true,
            playSpeed: 3000
        }).trigger('click');
    });

    // Mobile Menu
    $('.navbar-toggle').click(function (event) {
        $('body').toggleClass('mobile_menu');
        event.preventDefault();
    });

    window.addEventListener("resize", function () {
        if (screenWidth > 992) {
            $('body').removeClass('mobile_menu');
        }
    }, false);


    if (screenWidth > 992) {
        $('body').removeClass('mobile_menu');
    }

    // Product Gallery
    $('#product_top .thumbs a').first().addClass('active');
    $('#product_top .thumbs a').click(function (event) {
        var item = $(this).attr('number');
        if (!$(this).hasClass('active')) {
            $('#product_top .thumbs a').removeClass('active');
            $(this).addClass('active');
            $('#product_top .preview img').removeClass('active');
            $('#product_top .preview img[number="' + item + '"]').addClass('active');
        }
        event.preventDefault();
    });

    // Product Material
    $('#product_top .material a').click(function (event) {
        if (!$(this).hasClass('active')) {
            var item = $(this).attr('id');
            $('#product_top .material a').removeClass('active');
            $(this).addClass('active');
            $('.product_info input[type="radio"]').removeAttr('checked');
            $('.product_info input[value="' + item + '"]').prop('checked', true);
        }
        event.preventDefault();
    });

    // Product Comparison P height
    if ($('#product_comparison .products').length > 0) {
        productComparisonP();
    }

    // FAQ
    $(".accordion .item").click(function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).children('p').slideUp();
        } else {
            $(this).siblings('.item').removeClass('active');
            $(this).siblings('.item').children('p').slideUp();
            $(this).addClass('active');
            $(this).children('p').slideDown();
        }
    });

    // Service FAQ anchors
    $('#service_faq .right a').click(function (event) {
        var offset = $($(this).attr('href')).offset().top;
        offset = offset - 90;
        $('html, body').animate({
            scrollTop: offset
        }, 500);
        event.preventDefault();
    });

    $('#service_faq #top a').click(function (event) {
        var offset = $($(this).attr('href')).offset().top;
        offset = offset - 90;
        if (screenWidth < 993) {
            offset = offset + 75;
        }
        $('html, body').animate({
            scrollTop: offset
        }, 500);
        event.preventDefault();
    });

    // Select JS
    if ($('select, input[type="radio"]').length > 0) {
        jcf.replaceAll();
    }

    // Product Configurator
    $('.radio_wrapper').click(function () {
        $(this).siblings().removeClass('active');
        $(this).addClass('active');
    });

    // position contact image in main menu
    $('#main_menu .navigation--list li').each(function () {
        var currentLi = $(this);
        if (currentLi.find(".contact-image").length > 0) {
            var dropHeight = currentLi.find('.drop').innerHeight();
            var imgHeight = currentLi.find('.contact-image').innerHeight();
            //console.log(currentLi);
            currentLi.find('.contact-image').css({
                top: (dropHeight - imgHeight) / 2,
                bottom: (dropHeight - imgHeight) / 2
            });
        }
    });

    $('input[type="text"]').focus(function () {
        var sd = $(this).attr('placeholder');

        if (sd != undefined) {
            $(this).before($('<label class="placeholder" style="display:none;">' + sd + '</label>').fadeIn());
            $(this).removeAttr('placeholder');
        }
    });
    $('textarea').focus(function () {
        var sd = $(this).attr('placeholder');

        if (sd != undefined) {
            $(this).before($('<label class="placeholder" style="display:none;">' + sd + '</label>').fadeIn());
            $(this).removeAttr('placeholder');
        }

    });
    $('input[type="email"]').focus(function () {
        var sd = $(this).attr('placeholder');

        if (sd != undefined) {
            $(this).before($('<label class="placeholder" style="display:none;">' + sd + '</label>').fadeIn());
            $(this).removeAttr('placeholder');
        }
    });
    while ($('#email-error').is(":visible") == true) {
        $(".placeholder").remove();
        console.log($('#email-error').is(":visible"));
    }
    console.log($('#email-error').is(":visible"));


    $('input[type="password"]').focus(function () {
        var sd = $(this).attr('placeholder');

        if (sd != undefined) {
            $(this).before($('<label class="placeholder" style="display:none;">' + sd + '</label>').fadeIn());
            $(this).removeAttr('placeholder');
        }
    });
}); // end: $(document).ready()

$(window).resize(function () {
    screenWidth = $(window).width();
    // Product Comparison P height
    productComparisonP();

    // Mobile Comparison Slider
    if (($('#product_comparison .products').length > 0)) {
        if (!comparisonSlider && (screenWidth < 993)) {
            mobileComaprisonSlider();
        }
        if (comparisonSlider && (screenWidth > 992)) {
            removeMobileComaprisonSlider();
        }
    }

});
end: $(window).resize()

$(window).scroll(function () {
    scrolltop = $(window).scrollTop();

    if (screenWidth > 992 && scrolltop > 5) {
        $('body').addClass('no_drop');
    } else {
        $('body').removeClass('no_drop');
    }
}); // end: $(window).scroll()

function productComparisonP() {
    setTimeout(function () {
        var item = 0;
        $('#product_comparison .products p').css('height', 'auto');
        $('#product_comparison .products p').each(function () {
            if ($(this).height() > item) {
                item = $(this).height();
            }
        });
        item = item + 10;
        $('#product_comparison .products p').css('height', item);
    }, 500);

}

function mobileComaprisonSlider() {
    setTimeout(function () {
        $('#product_comparison .products').slick({
            lazyLoad: 'progressive',
            dots: false,
            arrows: true
        });
    }, 500);
    comparisonSlider = true;
}

function removeMobileComaprisonSlider() {
    var slider = $('#product_comparison .products');
    slider.slick('unslick');
    comparisonSlider = false;
}