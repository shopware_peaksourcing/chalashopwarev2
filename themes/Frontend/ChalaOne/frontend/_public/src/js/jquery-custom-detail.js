(function($, window) {
    if($('.content--wrapper').children('div').hasClass('custom-detail')) {
        window.StateManager.removePlugin('.product--image-zoom', 'swImageZoom', 'xl');
    }
	window.StateManager.removePlugin('select:not([data-no-fancy-select="true"])', 'swSelectboxReplacement')
}(jQuery, window));