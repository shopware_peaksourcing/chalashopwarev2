{extends file="parent:frontend/home/index.tpl"}

{* Promotion *}
{block name='frontend_home_index_promotions'}
    {if $hasEmotion}
        {foreach $emotions as $emotion}
            {if $emotion.id != 17}
                {action module=widgets controller=emotion action=index emotionId=$emotion.id  controllerName=$Controller}
            {/if}
        {/foreach}
    {/if}
{/block}

{block name='frontend_index_content_main'}

    {* Content top container *}
    {block name="frontend_index_content_top"}{/block}

    {* Main content *}
    {block name='frontend_index_content_wrapper'}
        {block name='frontend_index_content'}{/block}
    {/block}

{/block}


{* Main content *}
{block name='frontend_index_content'}
{debug}
    {* Promotion *}
    {block name='frontend_home_index_promotions'}
        {if $hasEmotion}
            <div class="content--emotions">
                {foreach $emotions as $emotion}
                    {if $hasEscapedFragment}
                        {if 0|in_array:$emotion.devicesArray}
                            <div class="emotion--fragment">
                                {action module=widgets controller=emotion action=index emotionId=$emotion.id controllerName=$Controller}
                            </div>
                        {/if}
                    {else}
                        <div class="emotion--wrapper"
                             data-controllerUrl="{url module=widgets controller=emotion action=index emotionId=$emotion.id controllerName=$Controller}"
                             data-availableDevices="{$emotion.devices}">
                        </div>
                    {/if}
                {/foreach}
            </div>
        {/if}
    {/block}

    {block name='frontend_home_index_blog'}
        {* Blog Articles *}
        {if $sBlog.sArticles|@count}
            <div class="listing_box">
                <h2 class="headingbox_nobg largesize">{s name='WidgetsBlogHeadline'}{/s}:</h2>
                {foreach from=$sBlog.sArticles item=article key=key name="counter"}
                    {include file="frontend/blog/box.tpl" sArticle=$article key=$key homepage=true}
                {/foreach}
            </div>
        {/if}
    {/block}
    <section class="section narrow" id="leading_story">
        <div class="container">
            <div class="row">
                <h2>{s name="HomeSliderBlogTitle"}{/s}</h2>
            </div>
            <div class="row stories_nav top hidden-md hidden-lg">
                <a href="#" class="prev" data-slick="slickPrev"><span>vorherige</span></a>
                <a href="#" class="next" data-slick="slickNext"><span>nächste</span></a>
            </div>
            <div class="blog-slider row">
                {foreach $homeBlogArticlesSlider as $key => $entry name=ent}
                    <div class="row h_list {$entry.attribute.icon}">
                        {if $entry.media[0].media.path}
                            {$image = $entry.media[0].media.path}
                            <div class="col-xs-12 col-sm-12 col-md-7 {if $smarty.foreach.ent.iteration%2 > 0 }col-md-push-5{/if} col-lg-7 image">
                                <a class="blog--image"
                                   href="{url controller=blog action=detail sCategory=$entry.categoryId blogArticle=$entry.id}"
                                   title="{$entry.title|escape}">
                                    <img src="{$image}">
                                </a>
                            </div>
                        {/if}
                        <div class="col-xs-12 col-sm-12 col-md-5 {if $smarty.foreach.ent.iteration%2 > 0 }col-md-pull-7{/if} col-lg-5 text">
                            <h3>{$entry.title|truncate:40}</h3>
                            <p>
                                {if $entry.description}
                                    {$entry.description|unescape:"html"|truncate:340}
                                {/if}
                            </p>
                            <a href="{url controller=blog action=detail sCategory=$entry.categoryId blogArticle=$entry.id}">{s name='HomeBlogLink' namespace='frontend/home/index'}Gesamte Story lesen{/s}</a>
                        </div>
                    </div>
                {/foreach}
            </div>
            <div class="row stories_nav blog_home_nav">
                <div class="hidden-xs hidden-sm col-md-12 col-lg-12">
                    <a href="#" class="prev" data-slick="slickPrev">
						<span>
							{if $homeBlogArticles[1]}
                                {$homeBlogArticles[1].title|truncate:40}
                            {/if}
						</span>
                    </a>
                    <span class="separator">//</span>
                    <a href="#" class="next" data-slick="slickNext">
						<span>
							{if $homeBlogArticles[2]}
                                {$homeBlogArticles[2].title|truncate:40}
                            {/if}
						</span>
                    </a>
                </div>
            </div>
        </div>
    </section>
    {foreach $homeBlogArticlesSlider as $key => $entry name=ent}
        <section class="section narrow">
            <div class="container">
                <div class="row h_list {$entry.attribute.icon}">
                    {if $entry.media[0].media.path}
                        {$image = $entry.media[0].media.path}
                        <div class="col-xs-12 col-sm-12 col-md-7 {if $smarty.foreach.ent.iteration%2 > 0 }col-md-push-5{/if} col-lg-7 image">
                            <a class="blog--image"
                               href="{url controller=blog action=detail sCategory=$entry.categoryId blogArticle=$entry.id}"
                               title="{$entry.title|escape}">
                                <img src="{$image}">
                            </a>
                        </div>
                    {/if}
                    <div class="col-xs-12 col-sm-12 col-md-5 {if $smarty.foreach.ent.iteration%2 > 0 }col-md-pull-7{/if} col-lg-5 text">
                        <h3>{$entry.title|truncate:40}</h3>
                        <p>
                            {if $entry.description}
                                {$entry.description|unescape:"html"|truncate:340}
                            {/if}
                        </p>
                        <a href="{url controller=blog action=detail sCategory=$entry.categoryId blogArticle=$entry.id}">{s name='HomeBlogLink' namespace='frontend/home/index'}Gesamte Story lesen{/s}</a>
                    </div>
                </div>
            </div>
        </section>
    {/foreach}
    <!-- end: Home Runner -->

    <!-- start: Home Instagram -->
    <section id="instagram">
        <div class="container">
            <div class="wrapper">
                <div class="row top">
                    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                        <p>Follow us at <img srcset="/themes/Frontend/ChalaOne/frontend/_public/src/img/instagram.png">
                        </p>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <a href="https://www.instagram.com/explore/tags/chalasandals/?hl=en">#chalasandals</a>
                    </div>
                </div>
                <div class="row bottom">
                    {if $hasEmotion}
                        <div class="content--emotions">
                            <div class="emotion--fragment">
                                {action module=widgets controller=emotion action=index emotionId=17 controllerName=$Controller}
                            </div>
                        </div>
                    {/if}
                </div>
            </div>
        </div>
    </section>
    <!-- end: Home Instagram -->
{/block}
