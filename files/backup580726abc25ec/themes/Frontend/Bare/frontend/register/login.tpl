{namespace name="frontend/account/login"}
<div class="register--login content block">

	{* Error messages *}
	{block name='frontend_register_login_error_messages'}
		{if $sErrorMessages}
			<div class="account--error">
				{include file="frontend/register/error_message.tpl" error_messages=$sErrorMessages}
			</div>
		{/if}
	{/block}

	{* New customer *}
	{block name='frontend_account_login_new'}
	<div class="grid_10">
		<h2 class="headingbox_dark largesize">{s name="LoginHeaderNew"}{/s} {$sShopname|escapeHtml}</h2>
		<div class="inner_container">
			<p>{s name="LoginInfoNew"}{/s}</p>
			<form method="post" name="new_customer" class="new_customer_form" action="{url controller='register'}">
				{if !{config name=NoAccountDisable}}
					<div class="checkbox">
						<p>
							<input type="checkbox" class="chk_noaccount" name="skipLogin" value="1" />
							<strong>{s name="LoginLabelNoAccount"}{/s}</strong>
						</p>
					</div>
				{/if}
				<input type="submit" class="button-right large register_now" value="{s name='LoginLinkRegister'}{/s}" />
			</form>
		</div>
	</div>
	{/block}

	{* Existing customer *}
	{block name='frontend_account_login_customer'}
	<div class="grid_10">
    	<h2 class="headingbox_dark largesize">{s name="LoginHeaderExistingCustomer"}{/s}</h2>
    	<div class="inner_container">
	        <form name="sLogin" method="post" action="{url action=login}">
	            {if $sTarget}<input name="sTarget" type="hidden" value="{$sTarget|escape}" />{/if}
	            <fieldset>
	                <p>{s name="LoginHeaderFields"}{/s}</p>
	                <p>
	                    <label for="email">{s name='LoginLabelMail'}{/s}</label>
	                    <input name="email" type="email" tabindex="1" value="{$sFormData.email|escape}" id="email" class="text {if $sErrorFlag.email}instyle_error{/if}" />
	                </p>
	                <p class="none">
	                    <label for="passwort">{s name="LoginLabelPassword"}{/s}</label>
	                    <input name="password" type="password" tabindex="2" id="passwort" class="text {if $sErrorFlag.password}instyle_error{/if}" />
	                </p>
	            </fieldset>

	            <p class="password">
	    			<a href="{url action=password}" title="{"{s name='LoginLinkLostPassword'}{/s}"|escape}">
	    				{s name="LoginLinkLostPassword"}{/s}
	    			</a>
	    		</p>
	            <div class="action">
	           		<input class="button-middle small" type="submit" value="{s name='LoginLinkLogon'}{/s}" name="Submit"/>
	            </div>
	        </form>
    	</div>
    </div>
    {/block}
	
	<!-- 
	{* New customer *}
	{block name='frontend_register_login_newcustomer'}
		<div class="register--new-customer">
			<a href="#registration"
			   class="new-customer-btn btn is--icon-right"
			   id="new-customer-action"
			   data-collapseTarget="#registration">
				{s name="LoginLinkRegister2"}{/s}
			</a>
		</div>
	{/block}

	{* Existing customer *}
	{block name='frontend_register_login_customer'}
		<div class="register--existing-customer panel has--border is--rounded">

			<h2 class="panel--title is--underline">{s name="LoginHeaderExistingCustomer"}{/s}</h2>
			<div class="panel--body is--wide">
				{block name='frontend_register_login_form'}
					{if $sValidation}
						{$url = {url controller=account action=login sTarget=$sTarget sTargetAction=$sTargetAction sValidation=$sValidation} }
					{else}
						{$url = {url controller=account action=login sTarget=$sTarget sTargetAction=$sTargetAction} }
					{/if}

					<form name="sLogin" method="post" action="{$url}">
						{if $sTarget}<input name="sTarget" type="hidden" value="{$sTarget|escape}" />{/if}

						{block name='frontend_register_login_description'}
							<div class="register--login-description">{s name="LoginHeaderFields"}{/s}</div>
						{/block}

						{block name='frontend_register_login_input_email'}
							<div class="register--login-email">
								<input name="email" placeholder="{s name="LoginPlaceholderMail"}{/s}" type="email" tabindex="1" value="{$sFormData.email|escape}" id="email" class="register--login-field{if $sErrorFlag.email} has--error{/if}" />
							</div>
						{/block}

						{block name='frontend_register_login_input_password'}
							<div class="register--login-password">
								<input name="password" placeholder="{s name="LoginPlaceholderPassword"}{/s}" type="password" tabindex="2" id="passwort" class="register--login-field{if $sErrorFlag.password} has--error{/if}" />
							</div>
						{/block}

						{block name='frontend_register_login_input_lostpassword'}
							<div class="register--login-lostpassword">
								<a href="{url controller=account action=password}" title="{"{s name="LoginLinkLostPassword"}{/s}"|escape}">
									{s name="LoginLinkLostPassword"}{/s}
								</a>
							</div>
						{/block}

						{block name='frontend_register_login_input_form_submit'}
							<div class="register--login-action">
								<button type="submit" class="register--login-btn btn is--primary is--large is--icon-right" name="Submit">{s name="LoginLinkLogon"}{/s} <i class="icon--arrow-right"></i></button>
							</div>
						{/block}
					</form>
				{/block}
			</div>

		</div>
	{/block} -->
</div>